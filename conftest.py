import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--external-binaries",
        action="store_true",
        dest="binaries",
        default=False,
        help="Enable tests depending on external binaries",
    )


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "external_binaries: mark a test as requiring external binaries to run",
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--external-binaries"):
        return
    skip_binaries = pytest.mark.skip(reason="need --external-binaries option to run")
    for item in items:
        if "external_binaries" in item.keywords:
            item.add_marker(skip_binaries)
