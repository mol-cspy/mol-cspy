from setuptools.extension import Extension as Ext
from setuptools import find_namespace_packages
from numpy.distutils.core import setup, Extension as NumpyExt
from numpy import get_include


USE_CYTHON = True


def CythonExt(dst, srcs, ext=".pyx" if USE_CYTHON else ".cpp"):
    print(f'ext = {ext}')
    print(f'{[x + ext for x in srcs]}')
    print(f'get_include = {get_include()}')
    return Ext(
        dst, [x + ext for x in srcs], language="c++", include_dirs=[get_include()]
    )


ext_modules_cython = [
    CythonExt(
        "cspy.deprecated.core.models.point",
        ["cspy/deprecated/core/models/point"]
    ),
    CythonExt(
        "cspy.deprecated.core.models.cvector3d",
        ["cspy/deprecated/core/models/cvector3d"]
    ),
    CythonExt(
        "cspy.deprecated.core.models.quaternion",
        ["cspy/deprecated/core/models/quaternion"]
    ),
    CythonExt(
        "cspy.deprecated.core.models.lattice",
        ["cspy/deprecated/core/models/lattice"]
    ),
    CythonExt(
        "cspy.deprecated.core.models.cmatrix3d",
        ["cspy/deprecated/core/models/cmatrix3d"]
    ),
    CythonExt(
        "cspy.deprecated.core.tasks.internal.vector3d.cvector3dlist",
        ["cspy/deprecated/core/tasks/internal/vector3d/cvector3dlist"],
    ),
    CythonExt(
        "cspy.deprecated.core.models.chull",
        ["cspy/deprecated/core/models/chull"]
    ),
    CythonExt(
        "cspy.deprecated.core.tasks.internal.crystal.canalysis",
        ["cspy/deprecated/core/tasks/internal/crystal/canalysis"],
    ),
    CythonExt(
        "cspy.deprecated.core.tasks.internal.convex_hull.canalysis",
        ["cspy/deprecated/core/tasks/internal/convex_hull/canalysis"],
    ),
    #CythonExt("cspy.deprecated.spacegroupsfast", ["cspy/deprecated/spacegroupsfast"]),
    #ßCythonExt("cspy.deprecated.listmathfast", ["cspy/deprecated/listmathfast"]),
    CythonExt("cspy.db.clustering_loops", ["cspy/db/clustering_loops"]),
    CythonExt("cspy.sample.sobol", ["cspy/sample/sobol"]),
    CythonExt("cspy.sample.lds", ["cspy/sample/lds"]),
    Ext("cspy.pyAOMlite.anIres",
        sources=[
            "cspy/pyAOMlite/anIres.pyx",
            "cspy/pyAOMlite/anIres_functions.c"
        ]),
    Ext("cspy.pyAOMlite.aom_overlap",
        sources=[
            "cspy/pyAOMlite/aom_overlap.pyx",
            "cspy/pyAOMlite/mulliken_functions.c"
        ], include_dirs=[get_include()])
]

if USE_CYTHON:
    from Cython.Build import cythonize

    ext_modules_cython = cythonize(ext_modules_cython, compiler_directives={"language_level": "3"})


ext_modules = [
    NumpyExt(
        "cspy.deprecated.core.models.sobolf90",
        sources=["cspy/deprecated/core/models/sobol.f90"],
        language="f90",
    ),
    Ext("cspy.ml.distance._cdtw", sources=["cspy/ml/distance/_cdtw.c"], language="c"),
] + ext_modules_cython


setup(
    name="cspy",
    version="1.0.0",
    description="Crystal structure prediction using python",
    url="https://gitlab.com/GROUPNAME/molCSPy",
    keywords=[
        "crystal structure prediction",
        "crystal",
        "quantum chemistry",
        "job",
        "hpc",
        "high performance computing",
        "forcefield",
    ],
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 5 - Stable",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Chemistry",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    package_dir={"cspy": "cspy"},
    packages=find_namespace_packages(include="cspy.*"),
    package_data={
        "cspy": ["*.toml", "*.json"],
        "cspy.crystal": ["sgdata.json"],
        "cspy.sample": ["*.npz"],
        "cspy.templates": ["*.template"],
        "cspy.db.schemas": ["*.sql"],
        "progs": ["*"],
        "cspy.potentials": ["*.pots"],
    },
    install_requires=[
        "numpy",
        "scipy",
        "sphinx",
        "celery[redis]",
        "msgpack-numpy",
        "toml",
        "chainmap",
        "scikit-learn",
        "future",
        "paramiko",
        "pytest",
        "mock",
        "spglib",
        "psutil",
        "pymatgen",
        "sympy",
        "networkx",
        "tqdm",
        "dask",
        "distributed",
        "rtree",
        "pandas",
        "mpi4py"
    ],
    entry_points={
        "console_scripts": [
            "cspy-csp = cspy.apps.csp:main",
            "cspy-db = cspy.apps.db:main",
            "cspy-clg = cspy.apps.clg:main",
            "cspy-dma = cspy.apps.dma:main",
            "cspy-opt = cspy.apps.opt:main",
            "cspy-qrbh = cspy.apps.qr_bh:main",
            "cspy-threshold = cspy.apps.threshold:main",
            "cspy-reoptimize = cspy.distributed.reoptimization:main",
            "cspy-setup = cspy.apps.setup:main"
        ]
    },
    ext_modules=ext_modules,
    zip_safe=False,
    include_package_data=True,
)
