TITL unknown
CELL 0.7 5.640200 5.640200 5.640200 90.000000 90.000000 90.000000
LATT 4
SYMM -x,-y,+z
SYMM -x,-z,-y
SYMM -x,-z,+y
SYMM -x,+z,-y
SYMM -x,+z,+y
SYMM -x,+y,-z
SYMM -x,+y,+z
SYMM -y,-x,-z
SYMM -y,-x,+z
SYMM -y,-z,-x
SYMM -y,-z,+x
SYMM -y,+z,-x
SYMM -y,+z,+x
SYMM -y,+x,-z
SYMM -y,+x,+z
SYMM -z,-x,-y
SYMM -z,-x,+y
SYMM -z,-y,-x
SYMM -z,-y,+x
SYMM -z,+y,-x
SYMM -z,+y,+x
SYMM -z,+x,-y
SYMM -z,+x,+y
SFAC Na Cl
Cl1   2  0.000000000  0.000000000  0.000000000
Na1   1  0.500000000  0.500000000  0.500000000
