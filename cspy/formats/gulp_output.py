import logging
import numpy as np
from pathlib import Path
import re


LOG = logging.getLogger(__name__)


class GulpOutputException(Exception):
    """Generic exception for Dmacrys output failures.

    """


class GulpOutput:

    def __init__(self, contents, parse=True):
        """Gulp output class for parsing the GULP standard output
        file and converting data into a more useful form.

        Parameters
        ----------
        contents : string
            string containing the contents of the gulp standard out

        """
        self.contents = contents
        self._content_lines = self.contents.splitlines()
        self.parsed_contents = {}

        self.parsed = False

        if parse:
            self._parse()

    def _parse(self):
        from cspy.crystal import Crystal, AsymmetricUnit, SpaceGroup, UnitCell
        from cspy.chem.element import Element
        content_lines_iter = iter(self._content_lines)
        for line in content_lines_iter:
            if "Space group" in line:
                k, v = self._parse_single_line_block(line)
                self.parsed_contents["space group"] = v.strip()
            elif "Final Cartesian lattice vectors" in line:
                array = np.array(
                    [list(map(float, x.split())) for x in self._parse_table(content_lines_iter)]
                )
                self.parsed_contents["unit cell"] = UnitCell(array)
            elif "Final asymmetric unit coordinates" in line:
                for i in range(4):
                    next(content_lines_iter)
                elements = []
                positions = []
                for l in self._parse_table(content_lines_iter):
                    tokens = l.split()
                    elements.append(Element.from_string(tokens[1]))
                    positions.append((float(tokens[3]), float(tokens[4]), float(tokens[5])))
                self.parsed_contents["asymmetric unit"] = AsymmetricUnit(elements, positions)

    def _parse_table(self, content_lines_iter):
        reading_table = False
        next(content_lines_iter)

        lines = []
        for line in content_lines_iter:
            if not line.strip():
                break
            if line.startswith("----------------------"): continue
            else:
                lines.append(line.strip())
        return lines

    def _parse_single_line_block(self, line):
        return re.split('\s*[:=]\s*', line)


    @classmethod
    def from_file(cls, filename):
        return cls(Path(filename).read_text())
