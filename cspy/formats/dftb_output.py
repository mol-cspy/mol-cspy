import logging

LOG = logging.getLogger(__name__)


class DftbOutput:
    """A class for parsing output of a dftb+ calculation"""

    def __init__(self, contents: str) -> None:
        """Initialise a class for parsing output of a dftb+ calculation

        Args:
            contents (str): Contents that has been read from an output file
        """
        self.file_contents = contents.split("\n")

    def _valid(self, single_point: bool = False) -> bool:
        """Checks if the calculation is valid

        Returns:
            bool: True if SCF/geometry converged for single-point/geometry optimisation
                  else False
        """
        if single_point:
            return any("Total Energy:" in item for item in self.file_contents)
        else:
            return "Geometry converged" in self.file_contents

    def _first_match(
        self, pattern: str, contents: str, split_index: int, start: str
    ) -> str:
        """Looks for the first match to a given pattern

        Args:
            pattern (str): The pattern to search for.
            contents (str): The contents to search through.
            split_index (int): The index to return from a str.split() function.
            start (str): Start pattern matching from top or bottom of the file.

        Returns:
            str: A part of the match to the pattern found in the contents.
        """
        if start == "top":
            match = next((item for item in contents if pattern in item), None).split()[
                split_index
            ]
        else:
            match = next(
                (item for item in reversed(contents) if pattern in item), None
            ).split()[split_index]
        return match

    def _parse_file(self) -> dict:
        """Parser function to search for keywords in the output file

        Returns:
            dict: A dictionary containing matches to the initial & final energies, seed number
            and final geometry step
        """
        expressions = {
            "initial_energy": ["Total Energy:", 2, "top"],
            "final_energy": ["Total Energy:", 2, "bottom"],
            "seed": ["Chosen random seed:", -1, "top"],
            "final_geometry_step": ["***  Geometry step:", 3, "bottom"],
        }
        return {
            key: self._first_match(value[0], self.file_contents, value[1], value[2])
            for key, value in expressions.items()
        }
