from cspy.chem import Element
from cspy.chem.molecule import Molecule
from cspy.crystal import Crystal
import logging
from os.path import join
from typing import Union, List
import numpy as np

LOG = logging.getLogger(__name__)

_3ob_max_momenta_values = {
    "Br": "d",
    "C": "p",
    "Ca": "p",
    "Cl": "d",
    "F": "p",
    "H": "s",
    "I": "d",
    "K": "p",
    "Mg": "p",
    "N": "p",
    "Na": "p",
    "O": "p",
    "P": "d",
    "S": "d",
    "Zn": "d",
}

_3ob_hubbard_derivs = {
    "Br": -0.0573,
    "C": -0.1492,
    "Ca": -0.034,
    "Cl": -0.0697,
    "F": -0.1623,
    "H": -0.1857,
    "I": -0.0433,
    "K": -0.0339,
    "Mg": -0.02,
    "N": -0.1535,
    "Na": -0.0454,
    "O": -0.1575,
    "P": -0.14,
    "S": -0.11,
    "Zn": -0.03,
}

_3ob_dispersion_parameters = {
    # parameter order: a1, a2, s6, s8
    "3ob_standard": [0.746, 4.191, 1.0, 3.209],
    "3ob_brandenburg": [0.841, 3.834, 1.0, 0.0],
}


def kpoints_per_klength(
    klength: Union[float, np.ndarray], kspacing: float = 0.05
) -> int:
    """Calculate the number of k-points along a given vector

    Args:
        klength (float or ndarray): A cell vector norm
        kspacing (float, optional): Spacing between k-points. Defaults to 0.05 ang^-1.

    Returns:
        int: Number of k-points
    """
    nk_raw = max(1, ((klength / kspacing)))
    return int(np.ceil(nk_raw))


def calc_kpoint_grid(crystal: Crystal, kspacing: float = 0.05) -> List:
    """Generates a k-point grid

    Args:
        crystal (:obj: Crystal): Crystal for which a k-point grid is being generated
        kspacing (float, optional): Spacing between k-points. Defaults to 0.05 ang^-1.

    Returns:
        List: List of the number of k-points along each vector forming the grid
    """
    try:
        LOG.debug(
            f"unit cell lattice before niggli reduction - {crystal.unit_cell.lattice}"
        )
        crystal = crystal.standardized()
        LOG.debug(
            f"unit cell lattice after niggli reduction - {crystal.unit_cell.lattice}"
        )
    except:
        LOG.debug("Crystal standardization failed. Using original crystal")

    k_vec_norms = [
        np.linalg.norm(kvec) for kvec in crystal.unit_cell.reciprocal_lattice
    ]  # length of each cell vector
    nkvec = [
        kpoints_per_klength(knorm, kspacing) for knorm in k_vec_norms
    ]  # number of kpoints per vector length
    return nkvec


def write_gen_file(structure: Crystal, output_file: str) -> None:
    """Write the structure to a file in GEN format

    Args:
        structure (:obj: Crystal): Crystal structure as a Crystal object.
        output_file (str): File name for the output GEN file.

    Returns:
        Writes output file with no returning object
    """
    return structure.to_gen_file(output_file)


def create_geometry_section(hsd_file: str, gen_filename: str = "geometry.gen") -> None:
    """Writes the geometry section of the dftb+ hsd input file"""
    with open(hsd_file, "w") as f:
        f.writelines(
            """Geometry = GenFormat {{
    <<< {}
}}\n""".format(
                gen_filename
            )
        )


def create_driver_section(
    hsd_file: str,
    max_force: float,
    max_steps: int,
    out_prefix: str,
    alg: str = "LBFGS",
    lattice_opt: bool = False,
) -> None:
    """Writes the driver section of the dftb+ input file

    Args:
        hsd_file (str): File name for the dftb input hsd file
        max_force (float): The maximum force on atoms in order for convergence
        max_steps (int): The maximum allowed number of optimization steps
        out_prefix (str): The prefix to attach to output geometry files
        alg (str, optional): The optimizer algorithm to use. Defaults to "LBFGS".
        lattice_opt (bool, optional): Whether to optimize lattice parameters in addition to atomic positions. Defaults to False.
    """

    lat = "LatticeOpt = Yes" if lattice_opt else "#"

    with open(hsd_file, "a") as f:
        f.writelines(
            """Driver = GeometryOptimization {{
    Optimizer = {}{{}}
    MovedAtoms = 1:-1
    Convergence = {{GradElem = {}}}
    MaxSteps = {}
    OutputPrefix = {}
    {}
}}\n""".format(
                alg, max_force, max_steps, out_prefix, lat
            )
        )


def create_hamiltonian_section(
    hsd_file: str,
    skf_path: str,
    max_angular_momenta: dict,
    hubbard_derivs: dict,
    disp_coeff: List,
    kpoints: List,
    scc_tol: float,
    charge: int = 0,
) -> None:
    """Writes the Hamiltonian section of the dftb+ input file

    Args:
        hsd_file (str): File name of the input file
        skf_path (str): Path to the directory containing slater-koster files
        max_angular_momenta (dict):
        hubbard_derivs (dict):
        disp_coeff (List): Dispersion coefficients defined for the 3ob SK parameter set
        kpoints (List): Kpoints grid
        scc_tol (float): Tolerance for the SCC calculation
        charge (int, optional): Charge of the system. Defaults to 0.
    """

    if kpoints:
        shift_coeff = [0.5 if (float(item) % 2) == 0 else 0 for item in kpoints]
        kpoints_text = """KPointsAndWeights = SupercellFolding {{                           
               {} 0 0                              
               0 {} 0
               0 0 {}
               {} {} {}                            
           }} """.format(
            *kpoints, *shift_coeff
        )
    else:
        kpoints_text = "#"

    with open(hsd_file, "a") as f:
        f.writelines(
            """Hamiltonian = DFTB {{
    Scc = Yes
    SlaterKosterFiles = Type2FileNames {{
        Prefix = {}/
        Separator = "-"
        Suffix = ".skf"
    }}

    ThirdOrderFull = yes
    Charge = {}
    SCCTolerance = {}
    MaxSCCIterations = 100
    HCorrection = Damping{{
        Exponent = 4.0
    }}

    Dispersion = DftD3 {{
        Damping = BeckeJohnson {{
            a1 = {}
            a2 = {}
        }}
        s6 = {}
        s8 = {}
    }}

    {}

    MaxAngularMomentum {{\n""".format(
                skf_path, charge, scc_tol, *disp_coeff, kpoints_text
            )
        )

        for ele, value in max_angular_momenta.items():
            f.writelines("""         {} = {}\n""".format(ele, value))

        f.writelines(
            """     }
    HubbardDerivs {\n"""
        )

        for ele, value in hubbard_derivs.items():
            f.writelines("""        {} = {}\n""".format(ele, value))

        f.writelines("    }\n}\n")


def write_dftb_inputs(
    structure: Union[Crystal, Molecule], working_directory: str, settings: dict
) -> None:
    """
    Creates a GEN and HSD file input for a dftb+ calculation on a crystal
    or molecule object. Currently used by DftbMinimizer to create inputs before
    a calculation is performed.

    Args:
    structure (Crystal or Molecule): An input crystal or molecule
    working_directory (str): The working directory to write files to
    settings (dict): A dictionary of keyword settings

    e.g.
    settings = {
        "skf_set" = "3ob",
        "skf_path" = "User_Defined_PATH",
        "disp_coeff" = "3ob_standard",
        "groups" = 1,
        "max_force" = 0.00058, # units in Ha/Bohr - value equivalent to 0.03 eV/AA
        "alg" = "LBFGS",
        "output_prefix" = "geom.out",
        "kpoint_spacing" = 0.05,
        "scc_tolerance" = 1e-5,
        "single_point" = False,
        "lattice_and_atoms_opt" = False
    }
    """
    if isinstance(structure, Crystal):
        atom_types = set(
            Element[x].symbol for x in structure.unit_cell_atoms()["element"]
        )
        kpoints = calc_kpoint_grid(structure, kspacing=settings["kpoint_spacing"])
    else:
        atom_types = set(ele.symbol for ele in structure.elements)
        kpoints = None
        settings["lattice_and_atoms_opt"] = False
    dict_max_momenta = {ele: _3ob_max_momenta_values[ele] for ele in atom_types}
    dict_hubbard_derivs = {ele: _3ob_hubbard_derivs[ele] for ele in atom_types}
    skf_full_path = "".join((settings["skf_path"], settings["skf_set"]))
    LOG.info("Creating dftb_in.hsd file")
    output_file = join(working_directory, "dftb_in.hsd")
    output_geo_file = join(working_directory, "geometry.gen")
    write_gen_file(structure, output_geo_file)
    create_geometry_section(output_file, gen_filename=output_geo_file)

    if settings["single_point"]:
        pass
    else:
        create_driver_section(
            output_file,
            max_force=settings["max_force"],
            max_steps=settings["max_steps"],
            out_prefix=settings["output_prefix"],
            alg=settings["alg"],
            lattice_opt=settings["lattice_and_atoms_opt"],
        )

    create_hamiltonian_section(
        output_file,
        skf_path=skf_full_path,
        scc_tol=settings["scc_tol"],
        max_angular_momenta=dict_max_momenta,
        disp_coeff=_3ob_dispersion_parameters[settings["disp_coeff"]],
        hubbard_derivs=dict_hubbard_derivs,
        kpoints=kpoints,
        charge=settings["charge"],
    )
