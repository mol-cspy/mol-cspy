from os.path import exists


class DmainFile:
    contents = ""

    def __init__(self, filename=""):
        self.filename = filename
        self._parse()

    def _parse(self):

        if not exists(self.filename):
            return
        with open(self.filename) as f:
            self.contents = f.read()

    def _parse_contents(self):
        pass

    @classmethod
    def from_string(cls, s):
        obj = cls()
        obj.contents = s
        obj._parse_contents()

    def update_contents(args_dict):
        # TODO
        pass
        # Check for constant volume calculation
        if args_dict.get("constant_volume", False):
            self.contents.replace("CONP", "CONV")

        args_dict.get("spline", (2.0, 4.0))

        """
        if args_dict["spline_off"] or (
                args_dict["spline_on"] is not False):
            # Will override the default value
            if args_dict["spline_off"] and (
                    args_dict["spline_on"] is not False):
                crystal_logger.error(
                    "--spline_off and --spline_on present, pick one")
                raise CSPyException(
                    "--spline_off and --spline_on present, pick one")

            fr = open(dmainput_filename, "r+")
            dr = fr.readlines()
            fr.seek(0)
            if args_dict["spline_off"]:
                spline = False
            else:
                spline = True
            spline_replaced = False
            for i in dr:
                if 'SPLI' in i:
                    if spline is False:  # remove spline
                        crystal_logger.info(
                            "Removing spline from %s",
                            dmainput_filename
                        )
                    # keep spline
                    elif spline and args_dict["spline_on"] is None:
                        fr.write(i)
                        spline_replaced = True
                        crystal_logger.info(
                            "Keeping default spline in %s",
                            dmainput_filename
                        )
                    else:  # replace value of spline
                        fr.write(str(args_dict["spline_on"])+"\n")
                        spline_replaced = True
                        crystal_logger.info(
                            "Replacing default of %s with %s in %s",
                            i.strip(), args_dict["spline_on"],
                            dmainput_filename
                        )
                elif "POTE" in i:
                    if spline and not spline_replaced:
                        if args_dict["spline_on"] is None:
                            # Add default spline
                            spline_replaced = True
                            fr.write("SPLI 2.0 4.0\n" + i)
                            crystal_logger.info(
                                "Adding spline of SPLI 2.0 4.0 to %s",
                                dmainput_filename
                            )
                        else:  # Add spline
                            spline_replaced = True
                            fr.write(
                                str(args_dict["spline_on"]) +
                                "\n" + i
                            )
                        crystal_logger.info(
                            "Adding spline of %s to %s",
                            args_dict["spline_on"],
                            dmainput_filename
                        )
                    else:
                        fr.write(i)
                else:
                    fr.write(i)
            fr.truncate()
            fr.close()

        if args_dict["setup_off"] or dmacrys_args["setup_on"]:
            # Will override the default value
            if args_dict["setup_off"] and dmacrys_args["setup_on"]:
                crystal_logger.error(
                    "--setup_off and --setup_on present, pick one"
                )
                raise CSPyException(
                    "--setup_off and --setup_on present, pick one"
                )
            fr = open(dmainput_filename, "r+")
            dr = fr.readlines()
            fr.seek(0)
            for i in dr:
                if "PRIN" in i:
                    rstr = (
                        "MOLE " +
                        # Determine what to replace
                        i.split()[(i.split().index("MOLE")+1)]
                    )
                    if args_dict["setup_off"]:
                        # No setup
                        fr.write(i.replace(rstr, "MOLE 0"))
                    if args_dict["setup_on"]:
                        # Require setup
                        fr.write(i.replace(rstr, "MOLE 1"))
                else:
                    fr.write(i)
            fr.truncate()
            fr.close()

        if args_dict["set_real_space"]:
            # Will override the default real space component of the
            # direct lattice sum And set it so that it equals the
            # repulsion-dispersion cutoff Higher-multipole summation
            # cutoff
            fr = open(dmainput_filename, "r+")
            dr = fr.readlines()
            # Default values, if ACCM not in dmain these will be used
            ACCMAD = 1000000.0
            RLWGT = 1.0
            nbas = 0
            ACCM = []
            for i in range(0, len(dr)):
                # Get the real-space cutoff (in lattice units,
                # this is our target)
                if "CUTO" in dr[i]:
                    # RLSCAL = float(dr[i].split()[1])
                    CUTPOT = float(dr[i].split()[2])
                # Need VC so need the lattice parameters
                if "LATT" in dr[i]:
                    L = (
                        [float(x) for x in dr[i+1].split()] +
                        [float(x) for x in dr[i+2].split()] +
                        [float(x) for x in dr[i+3].split()]
                    )
                    X1 = L[1][1]*L[2][2] - L[2][1]*L[1][2]
                    X2 = L[2][1]*L[0][2] - L[0][1]*L[2][2]
                    X3 = L[0][1]*L[1][2] - L[1][1]*L[0][2]
                    # Equation dmacrys uses to calculate VC
                    # from latice.f90
                    VC = L[0][0]*X1 + L[1][0]*X2 + L[2][0]*X3
                # These will override our default values if present
                if "ACCM" in dr[i]:
                    if len(dr[i].split()) >= 1:
                        ACCMAD = float(dr[i].split()[1])
                    if len(dr[i].split()) >= 2:
                        RLWGT = float(dr[i].split()[2])
                    if len(dr[i].split()) >= 3:
                        # Not in use currently but
                        # for future reference
                        ACCM = (dr[i].split()[3::])
                # Need number of basis functions,
                # can count these statements to obtain it
                if "LEVEL" in dr[i] or "DUPL" in dr[i]:
                    nbas += 1
            # we want CUTLAT=CUTPOT by altering RLWGT
            DPI = 3.141592653589793  # From DMACRYS parmad.f90
            CUTLAT = CUTPOT  # We want these distances to be equal
            # The following is a rearrangement of the equation in
            # parmad.f90 of dmacrys for the calculation of the direct
            # lattice summation cutoff here, given the other parameters
            # we want to know the value of RLWGT which gives the
            # correct length
            ARG = np.sqrt(np.log(ACCMAD))
            RLWGT = np.power(
                ((VC*VC)/float(nbas)), 1.0/6.0
            ) * ARG/(np.sqrt(DPI)*CUTLAT)
            # write the new information, if the ACCM
            # statement is present, alter it, if not insert it
            fr.seek(0)
            ACCMREAD = False
            for i in dr:
                if "ACCM" in i:
                    fr.write(
                        "ACCM " + str(ACCMAD) +
                        " " + str(RLWGT) + " " +
                        str(" ".join(ACCM))+"\n"
                    )
                    ACCMREAD = True
                elif "SCAL" in i and not ACCMREAD:
                    fr.write(
                        "ACCM " + str(ACCMAD) +
                        " " + str(RLWGT) + " " +
                        str(" ".join(ACCM))+"\n"+str(i)
                    )
                else:
                    fr.write(str(i))
            fr.truncate()
            fr.close()

        if args_dict.get("lennard_jones", False):
            # if potential contains lj read terms,
            # else perform the fit
            lj_pot = args_dict["lennard_jones_potential"]
            if lj_pot is not None:
                from cspy.deprecated.dmacrys_utils import read_lj
                read_lj(
                    dmainput_filename,
                    potential_file=lj_pot
                )
            else:
                from cspy.deprecated.dmacrys_utils import buckingham_to_lj
                buckingham_to_lj(dmainput_filename)

        if args_dict['auto_neighbour_setting']:
            args_dict['cutm'] = math.ceil(
                self.max_atom_atom_distance()
            )
            args_dict['nbur'] = self.longest_min_connectivity()
        try:
            word_replace_in_file(
                dmainput_filename,
                "CUTM", "CUTM " + str(args_dict['cutm'])
            )
            word_replace_in_file(
                dmainput_filename,
                "NBUR", "NBUR " + str(args_dict['nbur'])
            )
        except Exception as exc:
            crystal_logger.error(
                "Can not open %s; no dmacrys can be run",
                dmainput_filename
            )
            raise CSPyException(
                "Can not open "+str(dmainput_filename) +
                "; no dmacrys can be run"
            )
        # If doing exact property calculation,
        # replace STAR PLUT with STAR PROP and
        # remove NOPR line in .dmain
        if args_dict['seig1']:
            word_replace_in_file(
                dmainput_filename,
                "NOPR", "SEIG 1\nNOPR"
            )

        if args_dict['exact_prop']:
            word_replace_in_file(
                dmainput_filename,
                "STAR PLUT", "STAR PROP"
            )
            with open(dmainput_filename) as dmainput:
                new_input = []
                for line in dmainput:
                    if line.strip() != "NOPR":
                        new_input.append(line)

            with open(dmainput_filename, 'w') as dmainput:
                dmainput.writelines(new_input)
        """
