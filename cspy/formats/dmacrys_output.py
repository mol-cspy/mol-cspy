import re
import logging
import math
import numpy as np


LOG = logging.getLogger(__name__)


class DmacrysOutputException(Exception):
    """Generic exception for Dmacrys output failures.

    """


class DmacrysOutput:

    def __init__(self, contents):
        """Dmacrys output class for parsing the dmacrys standard output
        file and converting data into a more useful form.

        Parameters
        ----------
        contents : string
            string containing the contents of the dmacrys standard out

        """
        self.contents = contents

    def dmaout_eigenvectors(self, crystal):
        """Parse the dmacrys output file, obtain phonon eigenvalues
        straight from the dmacrys output.

        Parameters
        ----------
        crystal : cspy.Crystal
            crystal structure obtained from the dmacrys phonon
            calculation

        Returns
        -------
        eigenvectors : numpy.array
            matrix of phonon eigenvectors

        """
        molecules = crystal.unit_cell_molecules()
        phonon_section = re.findall(
            "Zone Centre phonon eigenvectors(.*)Zone Centre Phonon Frequencies",
            self.contents, flags=re.DOTALL
        )[0]
        phonon_section = re.findall(
            " +[0-9]*" + (" +(-?[0-9]+[.][0-9]+)" * 6 + " *\n") * len(molecules),
            phonon_section
        )

        eigenvectors = []
        for eigenvector in phonon_section:
            eigenvectors.append([float(i) for i in eigenvector])

        return np.array(eigenvectors)

    def phonon_eigenvectors(self, crystal):
        """Returns eigenvectors with reordered coordinates of the
        molecules so that it is the same order as the molecule
        orderings in cspy.Crystal.

        Parameters
        ----------
        crystal : cspy.Crystal
            crystal structure obtained from the dmacrys phonon
            calculation

        Returns
        -------
        eigenvectors : numpy.array
            matrix of phonon eigenvectors

        """
        return self.dmaout_eigenvectors(crystal) @ self.reordering_matrix(crystal)

    def mass_and_moments(self):
        """Returns the masses and moments of inertia for each molecule.
        Masses are are duplicated three times for each xyz coordinate.

        Returns
        -------
        mass_and_moments : list
            an ordered list of masses and moments of inertia

        """
        mass_section = re.findall(
            " *Molecule No[.] +Centred at +Total Mass(.*)Molecule No[.] "
            "+Principal axes of inertia",
            self.contents, flags=re.DOTALL
        )[0]
        mass_section = re.findall(
            " +[0-9]+" + " +-?[0-9]+[.][0-9]+" * 3 + " +([0-9]+[.][0-9]+) *",
            mass_section
        )
        inertia_section = re.findall(
            "Molecule No[.] +Principal axes of inertia(.*)Maximum "
            "intramolecular distance",
            self.contents, flags=re.DOTALL
        )[0]
        inertia_section = re.findall(
            " +Ix = +([0-9]+[.][0-9]+E?[-+]?[0-9]*), "
            "Iy = +([0-9]+[.][0-9]+E?[-+]?[0-9]*), "
            "Iz = +([0-9]+[.][0-9]+E?[-+]?[0-9]*) *",
            inertia_section
        )

        mass_and_moments = []
        for i, moments in enumerate(inertia_section):
            mass_and_moments += [float(mass_section[i])] * 3
            mass_and_moments += [float(i) for i in moments]
        return np.array(mass_and_moments)

    def transform_cspy_global(self, crystal):
        """Generates a matrix that transforms the translation and
        infinitesimal rotations coordinates of the phonon ordered
        eigenvectors from dmacrys global axis for translation and
        local principle axes for rotations to cspys global axes frame.

        Parameters
        ----------
        crystal : cspy.Crystal
            crystal structure obtained from the dmacrys phonon
            calculation

        Returns
        -------
        numpy.array
            numpy array of the transformation matrix that transforms
            the infinitesimal rotation of the phonon eigenvectors to
            the global axes frame

        """
        axis_section = re.findall(
            " +Final lattice vectors are(.*) +Final basis positions are",
            self.contents, flags=re.DOTALL
        )[0]
        axis = np.array(re.findall(
            (" +(-?[0-9]+[.][0-9]+)" * 3 + " *\n") * 3, axis_section
        )[0]).reshape(3, 3).astype(float)
        dmacrys_direct = crystal.unit_cell.c * axis
        dmacrys_global_to_cspy_global \
            = np.linalg.inv(dmacrys_direct) @ crystal.unit_cell.direct

        dmacrys_local_to_cspy_global = []
        inertia_section = re.findall(
            "Molecule No[.] +Principal axes of inertia(.*)Maximum "
            "intramolecular distance",
            self.contents, flags=re.DOTALL
        )[0]
        inertia_section = re.findall(
            " +[0-9]+" + (" +(-?[0-9]+[.][0-9]+E?[-+]?[0-9]*)" * 3 + " *\n") * 3,
            inertia_section
        )

        molecules = crystal.unit_cell_molecules()
        for i, inertia_tensor in enumerate(inertia_section):
            dmacrys_local_to_cspy_global.append(
                np.array(inertia_tensor).reshape(3, 3).astype(float)
                @ molecules[i].axes()
            )

        transform_bmat = []
        for i, transform_mat in enumerate(dmacrys_local_to_cspy_global):
            bmat_row = []
            for j in range(len(dmacrys_local_to_cspy_global)):
                if i == j:
                    bmat_row.append(np.block([
                        [dmacrys_global_to_cspy_global, np.zeros((3, 3))],
                        [np.zeros((3, 3)), transform_mat]
                    ]))
                else:
                    bmat_row.append(np.zeros((6, 6)))
            transform_bmat.append(bmat_row)

        return np.block(transform_bmat) @ self.reordering_matrix(crystal)

    def reordering_matrix(self, crystal):
        """Match molecule orderings between dmacrys and CSPy to ensure
        each part of the eigenvector refer to the same molecule in
        both ordering systems.

        Parameters
        ----------
        crystal : cspy.Crystal
            crystal structure obtained from the dmacrys phonon
            calculation

        Returns
        -------
        reorder_matrix : numpy.array
            numpy array of a matrix that reorders the eigenvectors so
            that each part refers to molecules in the same order that
            CSPy molecules are listed in

        """
        com_section = re.findall(
            " *Final crystalographic positions of molecule centres of "
            "mass(.*)Direction cosines ",
            self.contents, flags=re.DOTALL
        )[0]
        com_section = re.findall(
            " +[0-9]+" + " +(-?[0-9]+[.][0-9]+)" * 3 + " *",
            com_section
        )

        molecules = crystal.unit_cell_molecules()
        reorder_matrix = []
        for com_i in com_section:
            com_i = np.array([float(i) for i in com_i])

            reorder_row = []
            for mol_j in molecules:

                com_ij = crystal.to_fractional(mol_j.center_of_mass) - com_i
                smallest_diff = crystal.to_cartesian(np.array([
                    k - math.floor(k)
                    if abs(k - math.floor(k)) < abs(k - math.ceil(k))
                    else k - math.ceil(k) for k in com_ij
                ]))

                if all([abs(k) < 1e-2 for k in smallest_diff]):
                    reorder_row.append(np.identity(6))
                else:
                    reorder_row.append(np.zeros((6, 6)))
            reorder_matrix.append(reorder_row)

        reorder_matrix = np.block(reorder_matrix)

        if np.sum(reorder_matrix) != 6 * len(molecules):
            raise DmacrysOutputException(
                "Failure to match molecules from dmacrys to CSPy could "
                "be that more than one molecules from dmacrys was "
                "matched to CSPy or that a molecules failed to match "
                "with any from dmacrys for some reason."
            )

        if np.trace(reorder_matrix) != 6 * len(molecules):
            LOG.warning(
                "Different order in molecules from dmacrys and CSPy "
                "was found, phonon eigenvectors will be reordered."
            )

        return reorder_matrix
