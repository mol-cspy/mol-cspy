import pytest
import numpy as np
import shutil
from unittest import TestCase
from cspy.crystal import Crystal
from cspy.ml.descriptors import PowderPattern
from cspy.executable.locations import PLATON_EXEC
from os.path import abspath, join, dirname
from tempfile import TemporaryDirectory
import os

EXAMPLES_DIR = abspath(join(dirname(__file__), "../../../examples"))
EXPERIMENTAL = join(EXAMPLES_DIR, "experimental_structures")
GENERATED = join(EXAMPLES_DIR, "generated_structures")


class PowderPatternTests(TestCase):
    def test_pp_from_crystal_object(self):
        """This test checks that a powder pattern can be created 
        from a crystal object using several different crystal 
        structure files.
        """
        for path in [EXPERIMENTAL, GENERATED]:
            for file in os.listdir(path):
                c = Crystal.load(join(path,file))
                pp = c.calculate_powder_pattern()
                x = pp.pattern
                assert x is not None
                assert pp.separation == 0.02
                assert pp.tt_range == (0,20)
    

    def test_pp_from_file(self):
        """This test checks that a powder pattern can be created 
        directly from a crystal structure file.
        """
        filename = join(EXPERIMENTAL, "ACETAC01.cif")
        with TemporaryDirectory() as working_directory:
            copied_file_path = join(working_directory,"ACETAC01.cif")
            shutil.copy(filename, copied_file_path)
            pp = PowderPattern.from_file(filename=copied_file_path,
                                         working_directory=working_directory)
            assert pp.pattern is not None
            assert pp.separation == 0.02
            assert pp.tt_range == (0,20)

            if PLATON_EXEC:
                expected_pp = np.genfromtxt(join(EXAMPLES_DIR, "ACETAC01/ACETAC01_platon_pp.csv"), 
                                            delimiter=',',
                                            skip_header=1)
            else:
                expected_pp = np.genfromtxt(join(EXAMPLES_DIR, "ACETAC01/ACETAC01_pmg_pp.csv"), 
                                            delimiter=',',
                                            skip_header=1)

            output_pp = np.array([item for item in zip(np.arange(0.02,20,0.02),pp.pattern)])
            assert expected_pp.all() == output_pp.all()
    
    def test_pp_same_structure(self):
        """This test checks that the powder pattern for structures
        that are the same but differ only by spacegroup setting are 
        identical.
        """
        pps = {}
        for setting in ['pccn', 'pnaa', 'pbnb']:
            filename = join(EXPERIMENTAL, f"ACEMID_{setting}_p1.cif") 
            with TemporaryDirectory() as working_directory:
                copied_file_path = join(working_directory,f"ACEMID_{setting}_p1.cif")
                shutil.copy(filename, copied_file_path)
                pp = PowderPattern.from_file(filename=copied_file_path,
                                            working_directory=working_directory)
                pps[setting] = pp

        patterns = [value.pattern for value in pps.values()]
        assert all(p.all() == patterns[0].all() for p in patterns)


    def test_pp_with_parameters(self):
        """This test checks that a powder pattern can be created 
        directly from a crystal structure file with non-default
        parameters.
        """
        filename = join(EXPERIMENTAL, "ACETAC01.cif")
        with TemporaryDirectory() as working_directory:
            copied_file_path = join(working_directory,"ACETAC01.cif")
            shutil.copy(filename, copied_file_path)
            pp = PowderPattern.from_file(filename=copied_file_path,
                                        two_theta_range=(0,40),
                                        separation=0.1,
                                        working_directory=working_directory,
                                        wavelength='Mo',
                                        )
            assert pp.pattern is not None
            assert pp.separation == 0.1
            assert pp.tt_range == (0,40)
            assert pp.wavelength == 'Mo' if PLATON_EXEC else 'MoKa'
    

    @pytest.mark.skipif(PLATON_EXEC == None, reason='Platon not found')
    def test_pp_using_platon(self):
        """This test checks that a powder pattern can be created 
        directly from a crystal structure file using PLATON. This
        test will be skipped if PLATON cannot be located. Different 
        wavelengths of the X-ray source are tested.
        """
        filename = join(EXPERIMENTAL, "ACETAC01.cif")
        with TemporaryDirectory() as working_directory:
            for wavelength in [None, 'Mo', 'Ag', 'Cu']:
                copied_file_path = join(working_directory,"ACETAC01.cif")
                shutil.copy(filename, copied_file_path)
                pp = PowderPattern.from_file(filename=copied_file_path,
                                            two_theta_range=(0,20),
                                            separation=0.02,
                                            wavelength=wavelength,
                                            working_directory=working_directory)
                assert pp.pattern is not None
                assert pp.separation == 0.02
                assert pp.tt_range == (0,20)
                assert pp.wavelength == wavelength


    def test_pp_using_pymatgen(self):
        """This test checks that a powder pattern can be created 
        directly from a crystal structure file using pymatgen.
        Different wavelengths of the X-ray source are tested
        including behaviour with unknown keywords.
        """
        filename = join(EXPERIMENTAL, "ACETAC01.cif")
        wavelength_dict = {
            'Mo':'MoKa',
            'Ag':'AgKa',
            'RandomString':'CuKa', # unkown keywords will default to CuKa
        }
        
        for wavelength in ['Mo', 'Ag', 'RandomString']:
            pp = PowderPattern.run_pymatgen(filename=filename,
                                            two_theta_range=(0,20),
                                            separation=0.02,
                                            wavelength=wavelength)
            assert pp.pattern is not None
            assert pp.separation == 0.02
            assert pp.tt_range == (0,20)
            assert pp.wavelength == wavelength_dict[wavelength]
