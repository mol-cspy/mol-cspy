import logging
import os
from copy import deepcopy
from collections import defaultdict
from scipy.spatial import cKDTree as KDTree
from scipy.spatial.distance import cdist
from scipy.sparse import csr_matrix
from scipy.sparse import dok_matrix
import numpy as np
from networkx import minimum_cycle_basis
from networkx.algorithms import isomorphism
from cspy.linalg.kabsch import reorient_points
from cspy.linalg.kabsch import rmsd_points
from typing import List, TypeVar
from .element import Element
import sys

# networkx.from_scipy_sparse_matrix is deprecated in new version
if sys.version_info.major > 2 and sys.version_info.minor >= 8:
    from networkx import from_scipy_sparse_array as from_scipy_sparse
else:
    from networkx import from_scipy_sparse_matrix as from_scipy_sparse

Self = TypeVar("Self", bound="Molecule")
BOHR_TO_ANGSTROM = 0.529177211  # https://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0


LOG = logging.getLogger(__name__)


def guess_hybridization_state(average_angle):
    if average_angle >= 155.0:
        return "sp"
    elif average_angle >= 115.0:
        return "sp3"
    else:
        return "sp2"


class Molecule:
    positions: np.ndarray
    elements: List
    labels: np.ndarray
    properties: dict

    def __init__(
        self,
        elements: List[Element],
        positions: np.ndarray,
        bonds: np.ndarray = None,
        labels: np.ndarray = None,
        **kwargs,
    ):
        self.positions = positions
        self.elements = elements
        self.properties = {}
        self.properties.update(kwargs)
        self.bonds = None

        if bonds is None:
            if kwargs.get("guess_bonds", False):
                self.guess_bonds()
        else:
            self.bonds = dok_matrix(bonds)

        if labels is None:
            self.assign_default_labels()
        else:
            self.labels = labels

    def reorder_atoms(self, new_order):
        self.positions = self.positions[new_order]
        self.labels = self.labels[new_order]
        self.elements = [self.elements[i] for i in new_order]

    def __iter__(self):
        for atom in zip(self.elements, self.positions):
            yield atom

    def __len__(self):
        return len(self.elements)

    @property
    def covalent_radii(self):
        return np.array([x.cov for x in self.elements])

    @property
    def vdw_radii(self):
        return np.array([x.vdw for x in self.elements])

    def neighcrys_bond_cutoffs(self, tolerance=0.1001):
        if self.bonds is None:
            self.guess_bonds()
        max_bond = {}
        for (i, j), v in self.bonds.items():
            el_i = self.elements[i].symbol
            el_j = self.elements[j].symbol
            key = tuple(sorted((el_i, el_j)))
            max_bond[key] = max(max_bond.get(key, 0.0), v)
        return {k: round(v + tolerance, 4) for k, v in max_bond.items()}

    def guess_bonds(self, tolerance=0.40):
        """Use geometric distances and covalent radii
        to determine bonding information for this molecule.
        Will set the bonds member.

        tolerance is default 0.4 angstroms, which is recommended
        by the CCDC
        """
        tree = KDTree(self.positions)
        covalent_radii = np.array([x.cov for x in self.elements])
        max_cov = np.max(covalent_radii)
        thresholds = (
            covalent_radii[:, np.newaxis] + covalent_radii[np.newaxis, :] + tolerance
        )
        max_distance = max_cov * 2 + tolerance
        dist = tree.sparse_distance_matrix(tree, max_distance=max_distance).toarray()
        mask = (dist > 0) & (dist < thresholds)
        self.bonds = np.zeros(dist.shape)
        self.bonds[mask] = dist[mask]
        self.bonds = dok_matrix(self.bonds)

    def bfs_order(self, start=0, directed=True, **kwargs):
        from scipy.sparse.csgraph import breadth_first_order

        if self.bonds is None:
            self.guess_bonds()
        return breadth_first_order(self.bonds, start, directed=directed, **kwargs)

    def dfs_order(self, start=0, directed=True, **kwargs):
        from scipy.sparse.csgraph import depth_first_order

        if self.bonds is None:
            self.guess_bonds()
        return depth_first_order(self.bonds, start, directed=directed, **kwargs)

    def bond_angle(self, idxs):
        """idxs a, b, c -> the bond angle between bonds ab and bc"""
        origin = self.positions[idxs[1]]
        neighbours = [idxs[0], idxs[2]]
        vecs = self.positions[neighbours, :] - origin
        vecs = vecs / np.linalg.norm(vecs, axis=1)[:, np.newaxis]
        dots = np.clip(np.dot(vecs, vecs.T), -1.0, 1.0)
        return np.degrees(np.arccos(dots[np.triu_indices(2, k=1)]))[0]

    def plane_normal(self, idxs):
        """idxs a, b, c -> the normal to the plane containg atoms a, b, c"""
        origin = self.positions[idxs[0]]
        neighbours = [idxs[1], idxs[2]]
        vecs = self.positions[neighbours, :] - origin
        return np.cross(vecs[0, :], vecs[1, :])

    def dihedral_angle(self, idxs):
        """a, b, c, d -> dihedral angle of planes through a, b, c and b, c, d"""
        n1 = self.plane_normal([idxs[0], idxs[1], idxs[2]])
        n2 = self.plane_normal([idxs[1], idxs[2], idxs[3]])
        n1n2 = np.linalg.norm(n1) * np.linalg.norm(n2)
        return np.degrees(np.arccos(np.dot(n1, n2) / n1n2))

    @property
    def rings(self):
        if not hasattr(self, "_rings"):
            if self.bonds is None:
                self.guess_bonds()
            self._rings = minimum_cycle_basis(
                from_scipy_sparse(self.bonds)
            )
        return self._rings

    def neighbouring_atoms(self, idx):
        if self.bonds is None:
            self.guess_bonds()
        return self.bonds[idx].nonzero()[1]

    def foreshortened_hydrogen_positions(self, reduction=0.1):
        positions = self.positions.copy()
        nums = self.atomic_numbers
        for i in range(positions.shape[0]):
            if nums[i] != 1:
                continue
            neighbours = self.neighbouring_atoms(i)
            j = neighbours[0]
            v_ij = self.positions[j] - self.positions[i]
            d = reduction * (v_ij / np.linalg.norm(v_ij))
            positions[i, :] += d
        return positions

    def ring_dihedral_angles(self):
        from cspy.linalg import plane_of_best_fit

        rings = self.rings
        angles = {}
        for a in range(len(rings)):
            n1, d1, rmsd1 = plane_of_best_fit(self.positions[rings[a]])
            for b in range(a + 1, len(rings)):
                n2, d2, rmsd2 = plane_of_best_fit(self.positions[rings[b]])
                angles[(a, b)] = np.degrees(np.arccos(np.vdot(n1, n2)))
        return angles

    def guess_hybridization_states(self):
        states = {}
        for i in range(len(self)):
            name = self.labels[i]
            origin = self.positions[i]
            neighbours = self.bonds[i].nonzero()[1]
            num_neighbours = len(neighbours)
            if num_neighbours < 2:
                continue
            vecs = self.positions[neighbours, :] - origin
            vecs = vecs / np.linalg.norm(vecs, axis=1)[:, np.newaxis]
            dots = np.dot(vecs, vecs.T)
            angles = np.degrees(np.arccos(dots[np.triu_indices(num_neighbours, k=1)]))
            average_angle = np.mean(angles)
            state = guess_hybridization_state(average_angle)
            states[name] = state
        return states

    def axes(self, method="nc", homogeneous=False):
        """ If `neighcrys_axis_atom` is defined in self.properties,
        will use these indices to generate the axis."""
        if method == "nc":
            if len(self) < 3:
                LOG.error("Invalid length of molecular positions: %d", len(self))
                raise NotImplementedError(
                    "molecular axes (method=nc) only implemented for n >= 3"
                )
            atom_a, atom_b, atom_c = self.properties.get(
                "neighcrys_axis_atom_idxs", (0, 1, 2)
            )
            LOG.debug("Neighcrys axis atom idxs: %s", (atom_a, atom_b, atom_c))
            x0 = self.positions[atom_b] - self.positions[atom_a]
            x0 /= np.linalg.norm(x0)
            x1 = self.positions[atom_a] - self.positions[atom_c]
            x1 /= np.linalg.norm(x1)
            x1 = x1 - np.vdot(x1, x0) * x0
            x1 /= np.linalg.norm(x1)
            x2 = np.cross(x0, x1)
            axes = np.array([x0, x1, x2])
            assert np.linalg.det(axes) > 0, "Coordinate system must be right-handed"
        elif method == "pca":
            axes, s, vh = np.linalg.svd((self.positions - self.center_of_mass).T)
        elif method == "moi":
            moi = np.zeros((3, 3))
            masses = np.asarray([x.mass for x in self.elements])
            positions = self.positions - self.center_of_mass
            for mass, pos in zip(masses, positions):
                moi[0][0] += mass * (pos[1]**2 + pos[2]**2)
                moi[1][1] += mass * (pos[0]**2 + pos[2]**2)
                moi[2][2] += mass * (pos[0]**2 + pos[1]**2)
                moi[0][1] -= mass * pos[0] * pos[1]
                moi[0][2] -= mass * pos[0] * pos[2]
                moi[1][2] -= mass * pos[1] * pos[2]
            moi = moi + moi.T - np.diag(np.diag(moi))
            axes = np.linalg.eigh(moi)[1].T
        else:
            raise ValueError(f"Unknown molecular axis method '{method}'")

        if homogeneous:
            transform = np.eye(4)
            transform[:3, :3] = axes
            translation = -np.dot(axes, self.center_of_mass)
            transform[:3, 3] = translation
            transform[np.abs(transform) < 1e-15] = 0
            return transform
        return axes

    def distance_to(self, other, method="centroid"):
        method = method.lower()
        if method == "centroid":
            return np.linalg.norm(self.centroid - other.centroid)
        elif method == "center_of_mass":
            return np.linalg.norm(self.center_of_mass - other.center_of_mass)
        elif method == "nearest_atom":
            return np.min(cdist(self.positions, other.positions))
        else:
            raise ValueError(f"Unknown method={method}")

    @property
    def homogeneous_positions(self):
        return np.c_[self.positions, np.ones(len(self))].T

    @property
    def atomic_numbers(self):
        return np.array([e.atomic_number for e in self.elements])

    def transform(
        self, transformation_matrix, translation=np.zeros(3), rotate_multipoles=False
    ):
        if transformation_matrix.shape[1] == 4:
            h = self.homogeneous_positions
            t = np.dot(transformation_matrix, h)
            self.positions = t.T[:, :3]
            if rotate_multipoles:
                raise NotImplementedError(
                    "Haven't implemented multipole rotation for homogeneous transforms"
                )
        else:
            self.positions = np.dot(
                self.positions + translation, transformation_matrix.T
            )
            if rotate_multipoles:
                self.properties["multipoles"] = [
                    m.transformed(transformation_matrix) for m in self.multipoles
                ]

    def translate(self, translation):
        self.positions += translation

    def translated(self, translation):
        import copy

        result = copy.deepcopy(self)
        result.positions += translation
        return result

    @property
    def is_connected(self):
        from scipy.sparse.csgraph import connected_components

        nfrag, _ = connected_components(self.bonds)
        return nfrag == 1

    def connected_fragments(self):
        from cspy.linalg import cartesian_product
        from scipy.sparse.csgraph import connected_components

        if self.bonds is None:
            self.guess_bonds()

        nfrag, labels = connected_components(self.bonds)
        molecules = []
        for frag in range(nfrag):
            atoms = np.where(labels == frag)[0]
            na = len(atoms)
            sqidx = cartesian_product(atoms, atoms)
            molecules.append(
                Molecule(
                    [self.elements[i] for i in atoms],
                    self.positions[atoms],
                    labels=self.labels[atoms],
                    bonds=self.bonds[sqidx[:, 0], sqidx[:, 1]].reshape(na, na),
                )
            )
        return molecules

    def assign_default_labels(self):
        counts = defaultdict(int)
        labels = []
        for el, _ in self:
            counts[el] += 1
            labels.append("{}{}".format(el.symbol, counts[el]))
        self.labels = np.asarray(labels)

    @property
    def centroid(self):
        return np.mean(self.positions, axis=0)

    @property
    def center_of_mass(self):
        masses = np.asarray([x.mass for x in self.elements])
        return np.sum(self.positions * masses[:, np.newaxis] / np.sum(masses), axis=0)

    @property
    def molecular_formula(self):
        from cspy.chem.element import chemical_formula

        return chemical_formula(self.elements, subscript=False)

    @property
    def multipoles(self):
        return self.properties.get("multipoles", None)

    def __repr__(self):
        return "<{}: {}({:.2f},{:.2f},{:.2f})>".format(
            self.__class__.__name__, self.molecular_formula, *self.center_of_mass
        )

    def reflect(self, plane):
        if plane == "xy":
            self.positions[:, 2] *= -1
            if "multipoles" in self.properties:
                self.properties["multipoles"] = [
                    x.reflected(plane) for x in self.multipoles
                ]
        else:
            raise NotImplementedError

    def positions_in_molecular_axis_frame(
        self, method="nc", foreshorten_hydrogens=None
    ):
        if method not in ("nc", "pca", "moi"):
            raise NotImplementedError("Only nc, pca, moi implemented")
        if len(self) == 1:
            return np.array([[0.0, 0.0, 0.0]])
        axis = self.axes(method=method)
        if foreshorten_hydrogens:
            positions = self.foreshortened_hydrogen_positions(
                reduction=foreshorten_hydrogens
            )
            return np.dot(positions - self.center_of_mass, axis.T)
        else:
            return np.dot(self.positions - self.center_of_mass, axis.T)

    @classmethod
    def group_atoms_by_element(cls, molecules):
        """Sort all arrays, grouped by element"""
        from cspy.util import grouped_ordering

        symbols = []
        atom_nums = [0]
        for mol in molecules:
            symbols += [i.symbol for i in mol.elements]
            atom_nums.append(atom_nums[-1] + len(mol))

        new_order = grouped_ordering(symbols)
        for n, mol in enumerate(molecules):

            temp = []
            for i in range(atom_nums[n], atom_nums[n + 1]):
                temp.append(new_order.index(i))
            sub_order = [None for _ in range(len(temp))]
            for i, j in enumerate(sorted(temp)):
                sub_order[temp.index(j)] = i
            sub_order = [sub_order.index(i) for i in range(len(sub_order))]

            mol.elements = [mol.elements[i] for i in sub_order]
            mol.positions = mol.positions[sub_order]
            mol.labels = np.array(mol.labels)[sub_order]
            for item in mol.properties.items():
                if isinstance(item, np.ndarray):
                    item = item[sub_order]
                elif isinstance(item, list) and len(item) == len(mol):
                    item = [item[i] for i in sub_order]
            if mol.bonds:
                mol.guess_bonds()

    def oriented(self, method="nc"):
        from copy import deepcopy

        result = deepcopy(self)
        result.positions = self.positions_in_molecular_axis_frame(method=method)
        return result

    def minimize_with_dftb(self, **kwargs) -> Self:
        """Optimizes the structure using dftb

        Returns:
            A minimized version of the molecule as a Molecule object
        """
        from cspy.minimize import dftb_calculator
        optimised_molecule = dftb_calculator(self, **kwargs)
        return optimised_molecule

    @classmethod
    def from_xyz_file(cls, filename, **kwargs):
        from cspy.formats.xyz import parse_xyz_file

        xyz_dict = parse_xyz_file(filename)
        return cls.from_xyz_dict(xyz_dict, **kwargs)

    @classmethod
    def from_xyz_string(cls, contents, **kwargs):
        from cspy.formats.xyz import parse_xyz_string

        xyz_dict = parse_xyz_string(contents)
        return cls.from_xyz_dict(xyz_dict, **kwargs)

    @classmethod
    def from_xyz_dict(cls, xyz_dict, **kwargs):
        elements = []
        positions = []
        for label, position in xyz_dict["atoms"]:
            elements.append(Element[label])
            positions.append(position)
        return cls(
            elements, np.asarray(positions), comment=xyz_dict["comment"], **kwargs
        )

    @classmethod
    def from_gaussian_optimization(cls, filename, **kwargs):
        from cspy.formats.gaussian import GaussianLogFile

        log_file = GaussianLogFile(filename)
        geometries = log_file.geometries
        if len(geometries["inp"]) > 0:
            geom = geometries["inp"][-1]
        else:
            LOG.info(
                "Could not find input orientation in %s, loading standard orientation instead",
                filename,
            )
            geom = geometries["std"][-1]
        return cls.from_arrays(
            elements=geom["elements"],
            positions=geom["positions"],
            scf_energy=log_file.final_energy,
            **kwargs,
        )

    @classmethod
    def from_fchk_file(cls, filename, **kwargs):
        from cspy.formats.gaussian import GaussianFchkFile

        fchk = GaussianFchkFile(filename, parse=True)
        elements = np.array(fchk["Atomic numbers"])
        positions = np.array(fchk["Current cartesian coordinates"]).reshape(
            elements.shape[0], 3
        )
        positions *= BOHR_TO_ANGSTROM
        return cls.from_arrays(elements=elements, positions=positions, **kwargs)

    @classmethod
    def from_gaussian_optimization_string(cls, contents, **kwargs):
        from cspy.formats.gaussian import GaussianLogFile

        log_file = GaussianLogFile.from_string(contents)
        geometries = log_file.geometries
        if len(geometries["inp"]) > 0:
            geom = geometries["inp"][-1]
        else:
            LOG.info(
                "Could not find input orientation in %s, loading standard orientation instead",
                log_file,
            )
            geom = geometries["std"][-1]
        return cls.from_arrays(
            elements=geom["elements"],
            positions=geom["positions"],
            scf_energy=log_file.final_energy,
            **kwargs,
        )

    @property
    def energy(self):
        return self.properties.get("scf_energy", np.nan)

    @classmethod
    def from_mol2_file(cls, filename, **kwargs):
        from cspy.formats.mol2 import parse_mol2_file

        atoms, bonds = parse_mol2_file(filename)
        positions = np.c_[atoms["x"], atoms["y"], atoms["z"]]
        elements = [Element[x].atomic_number for x in atoms["name"]]
        return cls.from_arrays(elements=elements, positions=positions, **kwargs)
    
    def to_gen_string(self) -> str:
        """Write the molecule to GEN format saving the contents as a string

        Returns:
            File contents of a GEN file as a string
        """
        gen_template = (
                "{num_atoms} C\n"
                "{elements}\n"
                "{coords}\n"
        )
        pos = self.positions
        ele = self.elements
        unique_ele = set(sorted(ele))
        index = np.linspace(1, len(ele), len(ele))
        ele_order_dict = {item:i+1 for i, item in enumerate(unique_ele)}
        temp_coords = np.column_stack(([ele_order_dict[item] for item in ele], pos))
        sorted_temp_coords = temp_coords[temp_coords[:,0].argsort()]
        genfile_coords ="\n".join("{:.10g} {:.10g} {:.10f} {:.10f} {:.10f}".format(*item)
                                  for item in np.column_stack((index, sorted_temp_coords)))
        input_contents = gen_template.format(
            num_atoms=len(ele),
            elements=" ".join(item.symbol for item in unique_ele),
            coords=genfile_coords,
            )
        return input_contents

    def to_gen_file(self, filename: str) -> None:
        """Write the molecule to a file in GEN format
        """
        with open(filename, "w") as f:
            f.write(self.to_gen_string())

    @classmethod
    def from_gen_file(cls, filename: str, **kwargs: dict) -> Self:
        """Initialize a molecule from a GEN file"""
        from cspy.util.path import Path
        p = Path(filename)
        return cls.from_gen_string(p.read_text(), **kwargs)
    
    @classmethod
    def from_gen_string(cls, file_content: str, **kwargs: dict) -> Self:
        """Initialize a molecule from a GEN string"""
        from cspy.formats.gen import parse_gen_file_content
        gen_dict = parse_gen_file_content(file_content)
        elements = gen_dict['elements']
        positions = gen_dict['positions']
        return cls(elements, np.asarray(positions), **kwargs)

    def save(self, filename: str) -> Self:
        extension_map = {
            ".xyz": self.to_xyz_file,
            ".gen": self.to_gen_file,
        }
        extension = os.path.splitext(filename)[-1].lower()
        return extension_map[extension](filename)

    @classmethod
    def load(cls, filename: str, **kwargs) -> Self:
        extension_map = {
            ".xyz": cls.from_xyz_file,
            ".log": cls.from_gaussian_optimization,
            ".mol2": cls.from_mol2_file,
            ".zmat": cls.from_zmatrix_file,
            ".fchk": cls.from_fchk_file,
            ".gen": cls.from_gen_file,
        }
        extension = os.path.splitext(filename)[-1].lower()
        return extension_map[extension](filename, **kwargs)

    def rmsd(self, other):
        return self.overlay(other)[2]

    def rmsd_with_reflection(self, other, method="fixed", plane="xy"):
        from cspy.linalg import rmsd_points

        if method != "fixed":
            raise NotImplementedError
        coords_self = self.positions.copy()
        coords_self -= np.mean(coords_self, axis=0)
        coords_other = other.positions.copy()
        coords_other -= np.mean(coords_other, axis=0)
        rms = rmsd_points(coords_self, coords_other)
        LOG.debug("RMSD without flip: %.5g", rms)
        planes = {"xy": 2, "xz": 1, "yz": 0}
        best_reflection = None
        if plane:
            reflection = planes[plane]
            coords_flipped = coords_self.copy()
            coords_flipped[:, reflection] *= -1
            rms2 = rmsd_points(coords_flipped, coords_other)
            LOG.debug("Flipping about '%s' plane, RMSD: %.5g", plane, rms2)
            if rms2 < rms:
                rms = rms2
                best_reflection = plane
        return rms, best_reflection

    def overlay(self, other, reorder_to="self"):
        """Overlay and reorder the other molecule onto this one.

        Parameters
        ----------
        other : cspy.chem.Molecule
            The other molecule to overlay onto this one.
        reorder_to : str, optional
            Reorder the atoms of the overlap to match self or other.

        Returns
        -------
            A copy with other molecule transformed to overlay with
            this molecule, a list of reordering indices used to
            reorder the other molecule and an rmsd of the overlay.
        """
        if reorder_to not in ["self", "other"]:
            raise ValueError(
                "reorder_to parameter should be \"self\" or \"other\""
            )

        overlayed = deepcopy(other)
        if self.bonds is None:
            self.guess_bonds()
        if other.bonds is None:
            other.guess_bonds()

        # create the graph matcher from the molecular connectivity
        graph_1 = from_scipy_sparse(csr_matrix(self.bonds))
        graph_2 = from_scipy_sparse(csr_matrix(other.bonds))
        if reorder_to == "self":
            gm = isomorphism.GraphMatcher(graph_1, graph_2)
        else:
            gm = isomorphism.GraphMatcher(graph_2, graph_1)

        if not np.array_equal(
                np.sort(self.atomic_numbers), np.sort(other.atomic_numbers)
        ) or not gm.is_isomorphic():
            raise NotImplementedError(
                "Overlaying two different molecules not supported."
            )

        pos_1 = self.positions - self.center_of_mass
        pos_2 = other.positions - other.center_of_mass
        num_1 = self.atomic_numbers
        num_2 = other.atomic_numbers

        # iterate through all graph mappings and find the ordering that
        # gives the lowest rmsd
        best_rmsd = np.inf
        best_order = None
        for mapping in gm.isomorphisms_iter():
            sorted_map = {i: mapping[i] for i in range(len(other))}
            order = list(sorted_map.values())
            if reorder_to == "self":
                rmsd = rmsd_points(pos_2[order], pos_1)
                equal = np.array_equal(num_1, num_2[order])
            else:
                rmsd = rmsd_points(pos_2, pos_1[order])
                equal = np.array_equal(num_1[order], num_2)
            if rmsd < best_rmsd and equal:
                best_rmsd = rmsd
                best_order = order

        if reorder_to == "self":
            overlayed.positions = reorient_points(pos_2[best_order], pos_1)
            overlayed.labels = overlayed.labels[best_order]
            overlayed.elements = [overlayed.elements[i] for i in best_order]
        else:
            overlayed.positions = reorient_points(pos_2, pos_1[best_order])
        overlayed.positions += self.center_of_mass
        return overlayed, best_order, best_rmsd

    def overlay_substructure(self, other, self_substructure,
        other_substructure=None,
    ):
        """Overlay substructure of other molecule onto substructure
        of this one. Basic implementation - could add reordering and 
        invariance to order of substructure lists.

        Parameters
        ----------
        other : cspy.chem.Molecule
            The other molecule to overlay onto this one.
        self_substructure : list of int
            The indices of the atoms of self that the substructure 
            of other will be overlayed onto.
        other_substructure : list of int
            The indices of the atoms of other to be overlayed on to 
            the substructure of self. As implemented, must be in 
            same order as self_substructure. If not given, assumed 
            to be the same indices as self (i.e. molecules have 
            same atom ordering - can do this with overlay())

        Returns
        -------
            A copy with other molecule transformed to overlay with the
            substructure of this molecule and an rmsd of the substructure 
            overlay.
        """
        from scipy.spatial.transform import Rotation

        if other_substructure is None:
            LOG.info("assuming substructure indices same in both molecules")
            other_substructure = self_substructure

        self_sub = Molecule(
            elements=np.array(self.elements)[self_substructure],
            positions=self.positions[self_substructure],
            labels=self.labels[self_substructure],
        )
        other_sub = Molecule(
            elements=np.array(other.elements)[other_substructure],
            positions=other.positions[other_substructure],
            labels=other.labels[other_substructure],
        )

        overlayed = deepcopy(other)
        if self_sub.bonds is None:
            self_sub.guess_bonds()
        if other_sub.bonds is None:
            other_sub.guess_bonds()

        # create the graph matcher from the molecular connectivity
        graph_1 = from_scipy_sparse(csr_matrix(self_sub.bonds))
        graph_2 = from_scipy_sparse(csr_matrix(other_sub.bonds))
        gm = isomorphism.GraphMatcher(graph_1, graph_2)

        if not np.array_equal(
                np.sort(self_sub.atomic_numbers), 
                np.sort(other_sub.atomic_numbers)
        ) or not gm.is_isomorphic():
            raise NotImplementedError(
                "Overlaying two different substructures not supported."
            )

        # PB - For some reason using the rotation matrix from 
        # cspy.linalg.kabsh didn't work here
        pos_1 = self_sub.positions - self_sub.centroid
        pos_2 = other_sub.positions - other_sub.centroid
        r, rmsd = Rotation.align_vectors(pos_1, pos_2)

        overlayed.positions -= other.centroid
        overlayed.positions = r.apply(overlayed.positions)
        overlayed.positions += other.centroid

        sub_diff = np.mean(self.positions[self_substructure], axis=0) - \
                    np.mean(overlayed.positions[other_substructure], axis=0)
        overlayed.positions += sub_diff
        
        return overlayed, rmsd

    def bond_path_separation(self):
        from scipy.sparse.csgraph import floyd_warshall
        from scipy.sparse import csr_matrix

        return floyd_warshall(csr_matrix(self.bonds.astype(bool))).astype(int)

    @property
    def bbox_corners(self):
        b_min = np.min(self.positions, axis=0)
        b_max = np.max(self.positions, axis=0)
        return b_min, b_max

    @property
    def bbox_size(self):
        if len(self) == 1:
            return np.array([self.elements[0].vdw] * 3)
        b_min, b_max = self.bbox_corners
        return np.abs(b_max - b_min)

    def to_xyz_string(self, header=True):
        if header:
            lines = [
                f"{len(self)}",
                self.properties.get("comment", self.molecular_formula),
            ]
        else:
            lines = []
        for el, (x, y, z) in zip(self.elements, self.positions):
            lines.append(f"{el} {x: 20.12f} {y: 20.12f} {z: 20.12f}")
        return "\n".join(lines)

    def to_turbomole_string(self):
        tmol_template = "$coord angs\n" "{coords}\n"
        el = [x.symbol for x in self.elements]
        pos = self.positions
        coords = "\n".join(
            f"    {x:10.6f} {y:10.6f} {z:10.6f} {e}" for (x, y, z), e in zip(pos, el)
        )
        return tmol_template.format(coords=coords)

    @classmethod
    def from_turbomole_string(cls, coord_content):
        """Initialize from an xtb coord string resulting from optimization"""
        data = {}
        sections = coord_content.split("$")
        for section in sections:
            if not section or section.startswith("end"):
                continue
            lines = section.strip().splitlines()
            label = lines[0].strip()
            data[label] = [x.strip() for x in lines[1:]]
        elements = []
        positions = []
        for line in data["coord"]:
            x, y, z, el = line.split()
            positions.append((float(x), float(y), float(z)))
            elements.append(Element[el])
        pos = np.array(positions) * 0.52917749
        return cls(elements, pos)

    @classmethod
    def from_zmatrix_file(cls, filename):
        from cspy.formats.zmatrix import parse_zmat_file

        return cls.from_zmatrix(parse_zmat_file(filename))

    @classmethod
    def from_zmatrix_string(cls, string, **kwargs):
        from cspy.formats.zmatrix import parse_zmat_string

        return cls.from_zmatrix(parse_zmat_string(string, **kwargs))

    @classmethod
    def from_zmatrix(cls, zmat):
        from scipy.spatial.transform import Rotation

        n = len(zmat)
        elements = []
        idxs = []
        angles = []
        dihedrals = []
        rs = []
        for x in zmat:
            elements.append(Element[x.element])
            rs.append((x.r if x.r is not None else 0.0))
            idxs.append((x.a if x.a else -1, x.b if x.b else -1, x.c if x.c else -1))
            angles.append(x.angle if x.angle is not None else 0.0)
            dihedrals.append(x.dihedral if x.dihedral is not None else 0.0)
        positions = np.zeros((n, 3))
        if n == 1:
            return cls(elements, positions)

        angles = np.radians(angles)
        dihedrals = np.radians(dihedrals)
        idxs = np.array(idxs) - 1
        rs = np.array(rs)

        # z-axis should be along vector from atoms 0 -> 1
        positions[1, 2] = rs[1]

        if n == 2:
            return cls(elements, positions)

        _, a, r, b, angle, _, _ = zmat[2]
        a, b = idxs[2, :2]
        v1 = positions[b, :] - positions[a, :]
        v1 /= np.linalg.norm(v1)
        d = r * v1
        # rotate about the y axis by angle (i.e. in the xz-plane)
        axis = np.cross(v1, (1, 0, 0))
        rot = Rotation.from_rotvec(axis * np.radians(angle))
        d = rot.apply(d)
        positions[2, :] = d + positions[a, :]

        if n == 3:
            return cls(elements, positions)

        cos_dihedrals = np.cos(dihedrals)
        sin_dihedrals = np.sin(dihedrals)
        sin_angles = np.sin(angles)
        cos_angles = np.cos(angles)
        for i in range(3, n):
            a, b, c = idxs[i]
            r = rs[i]
            v1 = positions[a] - positions[b]
            v2 = positions[a] - positions[c]
            n = np.cross(v1, v2)
            nn = np.cross(v1, n)
            n /= np.linalg.norm(n)
            nn /= np.linalg.norm(nn)
            n *= -sin_dihedrals[i]
            nn *= cos_dihedrals[i]
            v3 = n + nn
            v3 /= np.linalg.norm(v3)
            v3 *= r * sin_angles[i]
            v1 /= np.linalg.norm(v1)
            v1 *= r * cos_angles[i]
            positions[i, :] = positions[a] + v3 - v1
        return cls(elements, positions)

    @property
    def asym_symops(self):
        return self.properties.get("generator_symop", [16484] * len(self))

    def to_xyz_file(self, filename):
        with open(filename, "w") as f:
            f.write(self.to_xyz_string())

    def to_dash_data(self):
        """convert this to a dict representation useable by dash etc"""
        return [
            {"symbol": e.symbol, "x": xyz[0], "y": xyz[1], "z": xyz[2]}
            for e, xyz in zip(self.elements, self.positions)
        ]

    def to_zmatrix(self):
        from cspy.formats.zmatrix import to_zmat

        return to_zmat(self)

    def to_zmatrix_string(self):
        from cspy.formats.zmatrix import to_zmat_string

        return to_zmat_string(self)

    def to_zmatrix_file(self, filename):
        from cspy.util.path import Path

        Path(filename).write_text(self.to_zmatrix_string())

    def calculate_multipoles(self, **kwargs):
        from cspy.minimize.gaussian_minimizer import GaussianMinimizer
        from cspy.executable.gdma import Gdma
        from tempfile import TemporaryDirectory
        from cspy.util.path import Path
        from cspy.chem.multipole import DistributedMultipoles

        kwargs.pop("opt", False)
        with TemporaryDirectory() as tmpdirname:
            m = GaussianMinimizer(opt=False, **kwargs)
            fchk = Path(tmpdirname, self.molecular_formula + ".fchk").absolute()
            m.minimize_molecule(self, fchk_file=fchk)
            gdma = Gdma(fchk, **kwargs)
            gdma.timeout = 3600.0
            gdma.working_directory = tmpdirname
            gdma.run()
        mults = DistributedMultipoles.from_dma_string(gdma.punch_contents)
        self.properties["multipoles"] = mults.molecules[0].multipoles
        return mults

    @classmethod
    def from_arrays(cls, elements, positions, **kwargs):
        return cls([Element[x] for x in elements], np.array(positions), **kwargs)
