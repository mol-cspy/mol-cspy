#!/usr/bin/env python
import argparse
import logging
import sqlite3
import time
import numpy as np
import json
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib import cm
import seaborn as sns
from copy import deepcopy
from collections import namedtuple, defaultdict
from cspy.db import CspDataStore
from cspy.db.key import CspDatabaseId

LOG = logging.getLogger("CSPy")
#Read crystals from database file

Job = namedtuple("Job", "en_id trial_number density energy state min_id")
Min_Job = namedtuple("Min_Job", "min_id density energy mc_step uni_id")

class Min_Trial():
    """
    Recording energy stage for the connectivity graph; the min_energy is either
    set manually or the minimum energy of initial structures
    """
    def __init__(self, min_energy, increase=1):
        self.min_energy = min_energy
        self.increase = increase 

    def get_energy_from_state(self, state):
        return self.min_energy + self.increase * state

    def get_state_from_energy(self, energy):
        return int(np.ceil(float(energy - self.min_energy) / float(self.increase)))

    def get_ticks(self, state_max):
        ticks = []
        for i in range(state_max):
            ticks.append(self.min_energy + self.increase * i)
        return ticks

class Trial():
    """The trial information from table trial in databases"""
    def __init__(self, trajectory):
        self.traj = {}
        for items in trajectory:
            self.traj[items[0]] = items[1]

    def get_current_energy(self, step):
        for lid_step in sorted(self.traj, reverse=True):
            if step >= lid_step:
                return self.traj[lid_step]

class Trial_State():
    """Recoding states of trials and overlaps between trials"""
    def __init__(self, trial_numbers):
        self.trials = {}
        self.active_state = {}
        self.basin = {}
        for trial_number in trial_numbers:
            self.trials[trial_number] = trial_number
            self.basin[trial_number] = [trial_number]
            self.active_state[trial_number] = 0

    @property
    def active_trials(self):
        return self.active_state.keys()

    def get_overlap(self, trial_overlap):
        if len(trial_overlap) <= 1:
            return
        min_path = self.trials[min(trial_overlap, key=lambda x:self.trials[x])]
        for trial_i in sorted(trial_overlap):
            if self.trials[trial_i] <= min_path:
                min_path = self.trials[trial_i]
                continue
            self.trials[trial_i] = min_path
            self.trials[self.trials[trial_i]] = min_path
        self._cluster()
        self._update_basin()
        return

    def _update_basin(self):
        new_basin = {}
        for key, value in self.trials.items():
            if value in new_basin:
                new_basin[value].append(key)
            else:
                new_basin[value] = [key]
        self.basin = new_basin

    def _cluster(self):
        for trial_i in sorted(self.trials):
            self.trials[trial_i] = self.trials[self.trials[trial_i]]

    def update_active(self):
        new_active_state = {}
        for trial_number in self.trials.values():
            new_active_state[trial_number] = self.active_state[trial_number]
        self.active_state = new_active_state

    def update_state(self, trial_number, state):
        if trial_number not in self.active_state:
            LOG.error("Error: trial_number not active")
        self.active_state[trial_number] = state

class Range():
    """Generate x axis for connectivity graph"""
    def __init__(self, num_nodes):
        self.ranges = {0: (0, num_nodes)}
        self.overlap = {}
        self.new_overlap = {}
        pos_tmp = int(num_nodes / 2)
        self.thre_pos = {0: pos_tmp}

    def get_range(self, path, num):
        for old_path, block in self.overlap.items():
            if path in block:
                path_index = block.index(path)
                if path not in self.ranges:
                    start = self.ranges[path-1][0]
                else:
                    start = self.ranges[path][0]
                end = start + num

                if path_index < len(block) - 1:
                    later_path = block[path_index + 1]
                    if later_path not in self.ranges:
                        self.ranges[later_path] = (end, 0)
                    
                self.ranges[path] = (start, end)
                break

        return self.ranges[path]

    def get_thre_pos(self, path):
        if path not in self.thre_pos:
            num_sum = self.ranges[path][0] + self.ranges[path][1]
            self.thre_pos[path] = int(num_sum / 2)
        return self.thre_pos[path]

    def update_range(self, path, start, end):
        self.ranges[path] = (start, end)

    def get_overlap(self, path, overlap):
        if path not in self.overlap or self.overlap[path] != overlap:
            self.thre_pos = {}
            self.new_overlap[path] = sorted(overlap)

    def update_overlap(self):
        self.overlap = deepcopy(self.new_overlap)

def get_en_id(min_id):
    contents = min_id.split('-')
    if len(contents) == 8:
        name, task, sg, tn, tmp, ms, opt, _ = min_id.split('-')
        return '-'.join([name, task, sg, tn, tmp, ms, opt, "0"])
    elif len(contents) == 6:
        name, task, sg, tn, _, ms = min_id.split('-')
        return '-'.join([name, task, sg, tn, "0", ms])
    else:
        raise NotImplementedError("Incompalible id for connectivity graph")

def get_min_id(en_id):
    contents = en_id.split('-')
    if len(contents) == 8:
        name, task, sg, tn, tmp, ms, opt, _ = en_id.split('-')
        return '-'.join([name, task, sg, tn, tmp, ms, opt, str(max_min_step)])
    elif len(contents) == 6:
        name, task, sg, tn, _, ms = en_id.split('-')
        return '-'.join([name, task, sg, tn, str(max_min_step), ms])
    else:
        raise NotImplementedError("Incompalible id for connectivity graph")

def read_db(db_name, en_min=None, en_interval=None, up_limit=None):
    """Read crystals from database file"""
    ds = CspDataStore(db_name)
    global max_min_step
    max_min_step = ds.query(
        'select max(minimization_step) from trial_structure'
    ).fetchone()[0]

    id_tmp = ds.query("select id from crystal limit 1").fetchone()[0]
    if len(id_tmp.split('-')) == 8:
        en_min_query = "select energy from crystal where id like '%-{}-1-0-OPT-{}'"
    elif len(id_tmp.split('-')) == 6:
        en_min_query = "select energy from crystal where id like '%-{}-{}-0'"
    else:
        raise NotImplementedError("Incompalible id for connectivity graph")

    trials = {}
    trial_numbers = []
    en_mins = []
    for trial_number, metadata in ds.query(
        "select trial_number, metadata from trial"
    ).fetchall():
        meta = json.loads(metadata)
        if en_interval is None:
            en_interval = meta["increase"]
        en_mins.append(
            ds.query(en_min_query.format(trial_number, max_min_step)
            ).fetchone()[0]
        )
        trial_numbers.append(trial_number)
        trials[trial_number] = Trial(meta['trajectory'])

    #Generate the grid for connectivity graph
    if en_min is None:
        en_min = min(en_mins)
    min_trial = Min_Trial(en_min, increase=en_interval)

    #Set up limit for energy
    state_up = np.inf
    if up_limit is not None:
        state_up = min_trial.get_state_from_energy(up_limit)

    ini_jobs = {}
    min_jobs = {}
    ini_ids = {}
    trials_min_state = defaultdict(list)
    jobs = defaultdict(list)
    query_text = "select density, energy from crystal where id='{}'"
    for uni_id, trial_number, density, energy in ds.query(
        'select distinct(id), trial_number, density, energy from crystal '
        'join trial_structure using(id) '
        'join equivalent_to on crystal.id = equivalent_to.unique_id '
    ).fetchall():

        mc_step = int(uni_id.split('-')[5])
        thre_energy = trials[trial_number].get_current_energy(mc_step)

        #Remove all false minimization leads to uphill energy
        if energy > thre_energy:
            continue

        #Collect the lid energy from all duplicates
        valid = False
        ids = defaultdict(list)
        thre_tmp = [thre_energy]
        state = min_trial.get_state_from_energy(thre_energy)
        if state < state_up:
            valid = True
            ids[trial_number].append((uni_id, state, mc_step))
        for content in ds.query(
            'select equivalent_id from equivalent_to where unique_id="{}"'.format(uni_id)
        ):
            dup_id = content[0]
            if dup_id is not None:
                mc_step = int(dup_id.split('-')[5])
                trial_number = int(dup_id.split('-')[3])
                thre_energy = trials[trial_number].get_current_energy(mc_step)
                thre_tmp.append(thre_energy)
                state = min_trial.get_state_from_energy(thre_energy)
                if state < state_up:
                    valid = True
                    ids[trial_number].append((dup_id, state, mc_step))
        if not valid:
            continue

        #Get the earliest stage where the structure appears in each trajectory
        min_ids = []
        for trial_number, content in ids.items():
            min_id, state, mc_step = min(content, key=lambda x:x[2])
            min_ids.append((trial_number, min_id, state, mc_step))

        #Set the earliest stage over all trials as the unique_id
        _, min_uni_id, _, mc_step = min(min_ids, key=lambda x:x[3])
        min_jobs[min_uni_id] = Min_Job(
            min_id=min_uni_id,
            density=density,
            energy=energy,
            mc_step=mc_step,
            uni_id=uni_id,
        )

        #The earliest state in each trails need to be added to build connection
        for trial_number, min_id, state, mc_step in min_ids:
            en_id = get_en_id(min_id)
            if mc_step == 0:
                ini_ids[trial_number] = (min_uni_id, energy)
                continue
            trials_min_state[trial_number].append(state)
            den_tmp, en_tmp = ds.query(query_text.format(en_id)).fetchone()
            jobs[state].append(
                Job(
                    en_id=en_id,
                    trial_number=trial_number,
                    density=den_tmp,
                    energy=en_tmp,
                    state=state,
                    min_id=min_uni_id,
                )
            )

    #Add unminimized vertices for initial structures
    for trial_number in trials_min_state:
        min_id, energy= ini_ids[trial_number]
        state = min_trial.get_state_from_energy(energy) + 1
        jobs[state].append(
            Job(
                en_id=get_en_id(min_id),
                trial_number=trial_number,
                density=float("nan"),
                energy=float("nan"),
                state=state,
                min_id=min_id,
            )
        )

    return jobs, min_jobs, min_trial, trial_numbers

def conn(jobs, min_jobs, min_trial, trial_numbers):
    G = nx.Graph()
    trial_state = Trial_State(trial_numbers) 
    #Set the "unminimized" energy for plotting connectivity graph
    ts_energy = min_trial.increase / 3.0

    for state in sorted(jobs):
        thre_energy = min_trial.get_energy_from_state(state)
        #Back up trial information
        old_trials = {}
        for trial_number, path in trial_state.trials.items():
            if trial_number == path:
                old_trials[trial_number] = path

        #Set the overlap between trajectories when the same minimum is located again
        trials_tmp = defaultdict(list)
        for job in sorted(jobs[state], key=lambda x:x.trial_number):
            trials_tmp[job.min_id].append(job.trial_number)
            if job.min_id not in G:
                continue
            trial_numbers_occupied = [job.trial_number]
            ts_id = [x for x in G.neighbors(job.min_id)][0]
            for thre_id_tmp in G.neighbors(ts_id):
                if thre_id_tmp != job.min_id:
                    break
            trial_numbers_occupied.append(int(thre_id_tmp.split('-')[1]))
            trial_state.get_overlap(trial_numbers_occupied)

        for min_id, trial_number_list in trials_tmp.items():
            trial_number_list = list(set(trial_number_list))
            if len(trial_number_list) > 1:
                trial_state.get_overlap(trial_number_list)

        #Change the label that the current trajectory belongs to
        changed_path = {} 
        for path in trial_state.trials.values():
            changed_path[path] = [path]

        for trial_number, path in trial_state.trials.items():
            if trial_number in old_trials and path < old_trials[trial_number]:
                changed_path[path].append(trial_number)

        for job in jobs[state]:
            path = trial_state.trials[job.trial_number]

            thre_id = f"thre-{path}-{state}"
            if thre_id not in G:
                #Create new threshold vertice
                G.add_node(
                    thre_id, 
                    kind="thre", 
                    energy=thre_energy, 
                    state=state,
                    path=path,
                    covered=changed_path[path],
                    num_nodes=0,
                    num_initials=0,
                )
                for c_path in changed_path[path]:
                    if c_path not in trial_state.active_state:
                        continue
                    state_old = trial_state.active_state[c_path]
                    if state_old != 0:
                        thre_id_old = f"thre-{c_path}-{state_old}"
                        for state_tmp in range(state_old+1, state):
                            thre_id_tmp = f"thre-{c_path}-{state_tmp}"
                            #Construc dummy vertice for plotting
                            if thre_id_tmp not in G.nodes:
                                G.add_node(
                                    thre_id_tmp, 
                                    kind="fake_thre", 
                                    energy=min_trial.get_energy_from_state(state_tmp),
                                    state=state_tmp,
                                    path=c_path,
                                    num_nodes=G.nodes[thre_id_old]["num_nodes"],
                                    num_initials=G.nodes[thre_id_old]["num_initials"],
                                    covered=G.nodes[thre_id_old]["covered"],
                                )
                            thre_id_tmp1 = f"thre-{c_path}-{state_tmp-1}"
                            G.add_edge(thre_id_tmp, thre_id_tmp1, kind="thre")
                        thre_id1 = f"thre-{c_path}-{state-1}"
                        G.add_edge(thre_id, thre_id1, kind="thre")
                        G.nodes[thre_id]["num_nodes"] += G.nodes[thre_id_old]["num_nodes"]
                        G.nodes[thre_id]["num_initials"] += G.nodes[thre_id_old]["num_initials"]
                trial_state.update_state(path, state)

            if job.min_id in G:
                continue

            G.nodes[thre_id]["num_nodes"] += 1
            if min_jobs[job.min_id].mc_step == 0:
                G.nodes[thre_id]["num_initials"] += 1

            #Create new minimum and unminimized vertices
            G.add_node(
                job.en_id,
                kind="ts", 
                energy=thre_energy - ts_energy, 
            )
            min_job = min_jobs[job.min_id]
            G.add_node(
                min_job.min_id, 
                kind="min", 
                density=min_job.density,
                energy=min_job.energy, 
                thre_energy=thre_energy,
                num=G.nodes[thre_id]["num_nodes"],
                mc_step=min_job.mc_step,
                uni_id=min_job.uni_id,
            )
            G.add_edge(job.en_id, min_job.min_id, kind="quench", density=min_job.density)
            #G.add_edge(thre_id, min_job.min_id, kind="link")
            G.add_edge(job.en_id, thre_id, kind="perturb", density=min_job.density)

        trial_state.update_active()

    #Construct the top vertice to end up connection, but the top vertice is not connected
    max_state = max(jobs)
    end_state = max_state + 1
    thre_energy = min_trial.get_energy_from_state(end_state)
    path = min(trial_state.active_trials)
    thre_id = f"thre-{path}-{end_state}"
    G.add_node(
        thre_id, 
        kind="thre", 
        energy=thre_energy,
        state=end_state,
        path=path,
        covered=trial_state.active_trials,
        num_nodes=0,
        num_initials=0,
    )
    for active_trial, max_state_trial in trial_state.active_state.items():
        thre_id_old = f"thre-{active_trial}-{max_state_trial}"
        for state_tmp in range(max_state_trial+1, end_state):
            thre_id_tmp = f"thre-{active_trial}-{state_tmp}"
            G.add_node(
                thre_id_tmp, 
                kind="fake_thre", 
                energy=min_trial.get_energy_from_state(state_tmp),
                state=state_tmp,
                path=active_trial,
                num_nodes=G.nodes[thre_id_old]["num_nodes"],
                num_initials=G.nodes[thre_id_old]["num_initials"],
                covered=G.nodes[thre_id]["covered"],
            )
            thre_id_tmp1 = f"thre-{active_trial}-{state_tmp-1}"
            G.add_edge(thre_id_tmp, thre_id_tmp1, kind="thre")
        thre_id1 = f"thre-{active_trial}-{max_state}"
        G.nodes[thre_id]["num_nodes"] += G.nodes[thre_id_old]["num_nodes"]
        G.nodes[thre_id]["num_initials"] += G.nodes[thre_id_old]["num_initials"]
    num_nodes = G.nodes[thre_id]["num_nodes"]
    yticks = min_trial.get_ticks(end_state)

    return G, num_nodes, yticks

def dump_basin(G, out_name):
    #Get structures in each basin
    get_basin_list = []
    real_thre_list = []
    for node, node_data in G.nodes.data():
        if node_data["kind"] == "thre" or node_data["kind"] == "fake_thre":
            real_thre_list.append((node, node_data["energy"], node_data["path"]))
            if len(node_data["covered"])>1:
                get_basin_list.append((node_data["energy"], node_data["covered"]))

    path_divide = {}
    for energy, covered in sorted(get_basin_list, key=lambda x:x[0]):
        for path in covered:
            if path not in path_divide:
                path_divide[path] = energy

    print(path_divide)
    path_structure = defaultdict(list)
    for node, node_data in G.nodes.data():
        if node_data["kind"] == "thre":
            path = node_data["path"]
            if node_data["energy"] < path_divide[path] or\
                node_data["energy"] != node_data["energy"]:
                path_structure[path].append(node)
            else:
                path_structure[-1].append(node)

    #Write to file
    out_file = open(f"{out_name}.path", 'w')
    out_file.write(
        "{:<36} {:<36} {:<12} {:<12} {:<12} {:<12}\n".format(
            "min_id", "uni_id", "path", "density", "energy", "thre_energy"
        )
    )
    for path, nodes in path_structure.items():
        for node in nodes:
            for ts_node in G.neighbors(node):
                if G.nodes[ts_node]["kind"] != "ts":
                    continue
                min_id = get_min_id(ts_node)
                if min_id not in G.nodes:
                    continue
                energy = G.nodes[min_id]["energy"]
                density = G.nodes[min_id]["density"]
                uni_id = G.nodes[min_id]["uni_id"]
                thre_energy = G.nodes[min_id]["thre_energy"]
                out_file.write(
                    "{:<36} {:<36} {:<12} {:<12.6f} {:<12.6f} {:<12.6f}\n".format(
                        min_id, uni_id, path, density, energy, thre_energy
                    )
                )

    out_file.close()

    #for node, energy in real_thre_list:

def dump_branch(G, out_name):
    new_G = nx.Graph()
    thre = defaultdict(list)
    thre_list = []
    ts_list = []
    ini_list = []

    #Get connectivity graph with only initial structures from the whole connectivity graph.
    for node, node_data in G.nodes.data():
        if node_data["kind"] == "thre" or node_data["kind"] == "fake_thre":
            thre_list.append(node)
            state = node_data["state"]
            thre[state].append((node, node_data))
            new_G.add_nodes_from([(node, node_data)])

    for edge1, edge2, edge_data in G.edges.data():
        if edge_data["kind"] == "thre":
            new_G.add_edge(edge1, edge2, kind="thre")

    for node, node_data in G.nodes.data():
        if node_data["kind"] == "min" and node_data["mc_step"] == 0:
            ini_list.append(node)
            for ts_id in G.neighbors(node):
                ts_list.append(ts_id)

            new_G.add_nodes_from([
                (node, node_data),
                (ts_id, G.nodes[ts_id]),
            ])
            new_G.add_edge(node, ts_id, kind="quench")

            for thre_id in G.neighbors(ts_id):
                if G.nodes[thre_id]["kind"] == "min":
                    continue
                new_G.add_edge(ts_id, thre_id, kind="perturb")

    pos = {}
    r = Range(len(ini_list))
    for state in sorted(thre, reverse=True):
        for thre_id, thre_data in sorted(thre[state], key=lambda x:x[1]["path"]):
            path = thre_data["path"]
            start, end = r.get_range(path, thre_data["num_initials"])
            pos_thre = r.get_thre_pos(path)
            pos[thre_id] = [pos_thre, thre_data["energy"]]
            if new_G.nodes[thre_id]["kind"] == "fake_thre":
                continue
            for ts_id in new_G.neighbors(thre_id):
                if new_G.nodes[ts_id]["kind"] == "thre" or \
                    new_G.nodes[ts_id]["kind"] == "fake_thre" :
                    continue
                for min_id in new_G.neighbors(ts_id):
                    if new_G.nodes[min_id]["kind"] == "min":
                        break
                pos[ts_id] = [start, new_G.nodes[ts_id]["energy"]]
                pos[min_id] = [start, new_G.nodes[min_id]["energy"]]
            r.update_range(path, start, end)
            r.get_overlap(path, thre_data["covered"])
        r.update_overlap()

    for i in sorted(ini_list, key=lambda x:pos[x][0]):
        print(i, pos[i])

    fig, ax = plt.subplots()
    ax.set_ylabel("Energy (kJ/mol)", fontsize=10)
    nx.draw_networkx_nodes(new_G, pos=pos, nodelist=thre_list, node_size=0)
    #nx.draw_networkx_labels(new_G, pos=pos)
    nx.draw_networkx_nodes(new_G, pos=pos, nodelist=ts_list, node_size=0)
    nx.draw_networkx_nodes(new_G, pos=pos, nodelist=ini_list, node_color='orange', node_size=20)
    nx.draw_networkx_edges(new_G, pos=pos)
    ax.tick_params(left=True, labelleft=True, labelsize=10)
    ax.tick_params(bottom=True, labelbottom=False)
    plt.savefig(f"{out_name}_branch.png")

def output(G, num_nodes, yticks, out_name, cluster_file=None, plot_density=False):
    thre = defaultdict(list)
    thre_list = []
    ts_list = []
    min_list = []
    ini_list = []
    for node, node_data in G.nodes.data():
        if node_data["kind"] == "thre" or node_data["kind"] == "fake_thre":
            thre_list.append(node)
            state = node_data["state"]
            thre[state].append((node, node_data))
        elif node_data["kind"] == "ts":
            ts_list.append(node)
        elif node_data["mc_step"] == 0:
            ini_list.append(node)
        else:
            min_list.append(node)

    #Color the edge with information in cluster_file, used for clustering analysis
    if cluster_file is not None:
        basins = {}
        with open(cluster_file, "r") as f:
            lines = f.readlines()
        for line in lines[1:]:
            content = line.split()
            id = content[0]
            cluster = int(content[3])
            basins[id] = cluster

        edge_lists = defaultdict(list)
        for node, node_data in G.nodes.data():
            if node_data["kind"] != "min":
                continue
            if node_data["uni_id"] not in basins:
                basin = -1
            else:
                basin = basins[node_data["uni_id"]]
            ts_id = G.neighbors(node)
            for edge in G.edges(ts_id):
                edge_lists[basin].append(edge)
            #edge_lists[basin].append(G.edges(node))
    elif plot_density:
        edge_lists = defaultdict(list)
        densities = []
        for node_1, node_2, edge_data in G.edges.data():
            if edge_data["kind"] == "thre":
                edge_lists[-1].append((node_1, node_2))
            else:
                densities.append(edge_data["density"])
                edge_lists['d'].append(((node_1, node_2), edge_data["density"]))
        color_normalize = Normalize(vmin=min(densities), vmax=max(densities))
    else:
        edge_lists = {-1: G.edges()}
    
    edge_nums = list(edge_lists.keys())
    LOG.info("Number of edge lists %s", len(edge_nums))
    
    #Set x axis for each vertice to plot the graph
    pos = {}
    r = Range(num_nodes)
    for state in sorted(thre, reverse=True):
        for thre_id, thre_data in sorted(thre[state], key=lambda x:x[1]["path"]):
            path = thre_data["path"]
            start, end = r.get_range(path, thre_data["num_nodes"])
            pos_thre = r.get_thre_pos(path)
            pos[thre_id] = [pos_thre, thre_data["energy"]]
            if G.nodes[thre_id]["kind"] == "fake_thre":
                continue
            num_tmp = 0
            for i, ts_id in enumerate(G.neighbors(thre_id)):
                if G.nodes[ts_id]["kind"] == "thre" or G.nodes[ts_id]["kind"] == "fake_thre" :
                    continue
                num_tmp += 1

                if (i % 2 == 0) or (end <= (pos_thre + 1)):
                    pos_ts_min = start
                    start += 1
                elif (i % 2 == 1) or (start >= pos_thre):
                    end -= 1
                    pos_ts_min = end
                else:
                    LOG.error(
                        "Error: wrong position around thre, "
                        "thre %s, start %s, end %s", pos_thre, start, end)

                for min_id in G.neighbors(ts_id):
                    if G.nodes[min_id]["kind"] == "min":
                        break

                pos[ts_id] = [pos_ts_min, G.nodes[ts_id]["energy"]]
                pos[min_id] = [pos_ts_min, G.nodes[min_id]["energy"]]
            r.update_range(path, start, end)
            r.get_overlap(path, thre_data["covered"])
        r.update_overlap()

    for i in sorted(ini_list, key=lambda x:pos[x][0]):
        print(i, pos[i])

    fig, ax = plt.subplots()
    ax.set_ylabel("Energy (kJ/mol)", fontsize=30)
    fig.set_size_inches([24, 18])
    #ax.set_ylim([-160.0,-100.0])
    nx.draw_networkx_nodes(G, pos=pos, nodelist=thre_list, node_size=0)
    nx.draw_networkx_nodes(G, pos=pos, nodelist=ts_list, node_size=0)
    nx.draw_networkx_nodes(G, pos=pos, nodelist=min_list, node_size=30)
    nx.draw_networkx_nodes(G, pos=pos, nodelist=ini_list, node_color='orange', node_size=50)
    width = 1.5
    nx.draw_networkx_edges(G, pos=pos, edge_color='k', width=width)
    #nx.draw_networkx_labels(G, pos=pos)
    color_list = sns.color_palette(n_colors=len(edge_lists))
    for edge_i, edge_list in edge_lists.items():
        if edge_i == -1:
            nx.draw_networkx_edges(G, pos=pos, edgelist=edge_list, edge_color='k', width=width)
        elif edge_i == 'd':
            for edge, density in edge_list:
                norm_density = color_normalize(density)
                nx.draw_networkx_edges(
                    G, pos=pos, edgelist=[edge], 
                    edge_color=cm.coolwarm(norm_density), width=width
                )

            scalarmappaple = cm.ScalarMappable(norm=color_normalize, cmap=cm.coolwarm)
            scalarmappaple.set_array(densities)
            cb = plt.colorbar(scalarmappaple, shrink=0.75, pad=0.05)
            cb.ax.set_ylabel("Density (g c$m^{-3}$)", fontsize=30, rotation=270)
            cb.ax.yaxis.labelpad = 35
            cb.ax.tick_params(labelsize=30)
        else:
            nx.draw_networkx_edges(
                G, pos=pos, edgelist=edge_list, edge_color=color_list[edge_i],
                width=width
            )
    #nx.draw_networkx(G, pos=pos, with_labels=False, node_size=20)
    #plt.title("BZAMID sg 43 rare")
    ax.tick_params(left=True, labelleft=True, labelsize=30)
    ax.tick_params(bottom=True, labelbottom=True)
    
    #plt.yticks(yticks)
    #plt.show()
    #plt.legend(fontsize=30)
    #plt.colorbar()
    plt.savefig(f"{out_name}.png")

def main():
    parser = argparse.ArgumentParser(
        description="Get connectivity graph from threshold algorithm",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "input_db", 
        type=str,
        help="Input clustered database file"
    )
    parser.add_argument(
        "-c", 
        "--cluster-file", 
        type=str,
        default=None,
        help="Clustering file to be coloured on connectivity graph, currently "
            "hard coded as fourth row is label of cluster"
    )
    parser.add_argument(
        "-e", 
        "--en-min", 
        type=float,
        default=None,
        help="Minimum energy"
    )
    parser.add_argument(
        "--up-limit", 
        type=float,
        default=None,
        help="Up limit energy"
    )
    parser.add_argument(     
        "--interval", 
        type=float,
        default=None,
        help="Energy interval for connectivity graph"
    )
    parser.add_argument(     
        "--dump-basin", 
        action="store_true",
        default=False,
        help="Dump density-energy basins"
    )
    parser.add_argument(     
        "--dump-branch", 
        action="store_true",
        default=False,
        help="Dump main branch connecitivity graph"
    )
    parser.add_argument(     
        "--plot-density", 
        action="store_true",
        default=False,
        help="Plot density on the connecitivity graph"
    )
    parser.add_argument(
        "-o", 
        "--output", 
        type=str, 
        default=None,
        help="Name for the output file"
    )
    parser.add_argument(
        "-ll",
        "--log-level", 
        default="INFO",
        choices=[
            'DEBUG', 'INFO', 'WARNING',
           'ERROR', 'CRITICAL'
        ],
        help="Set the logging level"
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.log_level) 

    time_1 = time.time()
    LOG.info("Reading database %s", args.input_db)
    jobs, min_jobs, min_trial, trial_numbers = read_db(
        args.input_db, 
        args.en_min, 
        args.interval,
        args.up_limit,
    )
    graph, num_nodes, yticks = conn(jobs, min_jobs, min_trial, trial_numbers)
    time_run = time.time() - time_1

    if args.output is None:
        out_name = args.input_db.split('.')[0]
    else:
        out_name = args.output
    LOG.info("Analyze time %.3f writing to output name %s", time_run, out_name)
    if args.dump_basin:
        dump_basin(graph, out_name)
    output(graph, num_nodes, yticks, out_name, args.cluster_file, args.plot_density)
    if args.dump_branch:
        dump_branch(graph, out_name)

    return

if __name__ == "__main__":
    main()
