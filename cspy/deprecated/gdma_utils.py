from __future__ import print_function
import argparse
from cspy.executable.locations import GDMA_EXEC
from cspy.deprecated.cspy_exceptions import CSPyException
import logging

LOG = logging.getLogger(__name__)


def gdma_add_arguments(parser):
    parser.add_argument(
        "-ml", "--multipole_limit", type=int, help="Multipole limit", default=4
    )
    parser.add_argument(
        "-ms",
        "--multipole_switch",
        type=float,
        help="Multipole switch (0.0 original dma algorithm, "
        "4.0 is the suggested default for the new dma algorithm",
        default=4.0,
    )


def gdma_parse_args(args=None):
    gdma_parser = argparse.ArgumentParser(
        description="Arguments to control the GDMA execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    gdma_add_arguments(gdma_parser)
    gdma_args, unknown = gdma_parser.parse_known_args(args=args)
    return gdma_args


def add_gdma_argument_group(parser):
    gdma_parser = parser.add_argument_group(
        "GDMA Arguments", "Arguments to control the GDMA execution"
    )
    gdma_add_arguments(gdma_parser)


def gdma_for(
    gdma_exe=GDMA_EXEC, inputfile=None, units="ang", foreshorten_hydrogens="y"
):
    from subprocess import call
    from cspy.deprecated.listmath import list3norm, list3diff, list3multiply, list3add

    BOHR = 0.529177249e0
    ANG = 1.0e0
    if units.lower() == "bohr":
        FACTOR = BOHR
    elif units.lower() == "ang":
        FACTOR = ANG
    else:
        LOG.error("Units are wrong")
        raise CSPyException("Units are wrong")
    f = open("dmaX_tmp.dat", "w")
    in_file = open(inputfile, "r")
    for line in in_file.readlines():
        if line.lower().strip().startswith("switch"):
            switch = line.strip().split()[-1]
            f.write("SWITCH " + switch + "\n")
        elif line.strip() != "":
            f.write(line)

        if line.lower().strip().startswith("punch"):
            punchfile = line.strip().split()[-1]
    f.close()
    in_file.close()
    LOG.info("Punch file to be created: %s", punchfile)
    call(gdma_exe + " < dmaX_tmp.dat > /dev/null", shell=True)
    # Skiping the rest of the function if we are not making H foreshortening
    if foreshorten_hydrogens.lower() == "n":
        return
    pf = open(punchfile, "r")
    atoms = []
    for line in pf.readlines():
        if "rank" in line.strip().lower():
            atoms.append(line.strip().split())
    pf.close()
    #    print "Nuclei positions (BOHR)"
    #    for atom in atoms:
    #        print atom

    for atom in atoms:
        if atom[0].lower() == "h":
            avec = [float(a) for a in atom[1:4]]
            smallest = 999.9
            # Find shortest interatomic distance
            for otheratom in atoms:
                if otheratom[0].lower() != "h":
                    ovec = [float(x) for x in otheratom[1:4]]
                    dist = list3norm(avec, ovec)
                    if dist < smallest:
                        smallest = dist
                        svec = [float(x) for x in otheratom[1:4]]
            dvec = list3diff(avec, svec)
            fvec = list3add(
                svec, list3multiply(dvec, ((smallest - 0.1 / FACTOR) / smallest))
            )
            atom[1] = str(fvec[0])
            atom[2] = str(fvec[1])
            atom[3] = str(fvec[2])

    f = open("dmaX_tmp.dat", "r")
    fnew = open("dmaX_tmp2.dat", "w")
    for line in f.readlines():
        if line.lower().strip().startswith("switch"):
            fnew.write("SWITCH " + switch + "\n")
        elif line.lower().strip().startswith("mult"):
            fnew.write(line)
            fnew.write("DELETE ALL\n")
            for atom in atoms:
                if atom[0] != "X":
                    fnew.write(
                        "ADD "
                        + atom[0]
                        + " "
                        + atom[1]
                        + " "
                        + atom[2]
                        + " "
                        + atom[3]
                        + "\n"
                    )
            if float(switch) == 0.0:
                fnew.write("RADIUS H 0.558\n")
            else:
                fnew.write("RADIUS H 0.325\n")
        elif line.strip() != "":
            fnew.write(line)
    f.close()
    fnew.close()
    call(gdma_exe + " < dmaX_tmp2.dat > /dev/null", shell=True)


def run_gdma_for_ang(
    gdma_exe=GDMA_EXEC,
    identifier="struct",
    level="verbose",
    file="Test.FChk",
    title="Blank",
    switch=0.0,
    limit="4",
    dma_filename="struct.dma",
    foreshorten_hydrogens="y",
):
    # Sets up input file
    out = []
    out.append("Title " + title)
    out.append(level)
    #    out.append("DENSITY MP2")
    out.append("File " + file)
    out.append(" ")
    out.append("Angstrom")
    out.append("Multipoles")
    out.append("  SWITCH " + str(switch))
    out.append("  Limit " + str(limit))
    # out.append("  Radius C 0.65")
    # out.append("  Radius N 0.65")
    # out.append("  Radius O 0.65")
    # out.append("  Radius H 0.65")
    # out.append("  Radius S 0.65")
    # out.append("  Radius Cl 0.65")
    out.append("  Punch " + dma_filename)
    out.append("Start")
    out.append(" ")
    out.append("Finish")
    output_name = str(identifier) + ".data"
    output_f = open(output_name, "w")
    for line in out:
        output_f.write(line + "\n")
    output_f.close()

    f = open(file, "r")
    f.close()
    gdma_for(
        gdma_exe=gdma_exe,
        inputfile=output_name,
        units="ang",
        foreshorten_hydrogens=foreshorten_hydrogens,
    )

    return


if __name__ == "__main__":

    gdma_parser = argparse.ArgumentParser(
        description="Arguments to control the GDMA execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    gdma_add_arguments(gdma_parser)
    gdma_parser.add_argument("fchk_filename", type=str)
    gdma_parser.add_argument("dma_filename", type=str)
    gdma_parser.add_argument("-fs", "--foreshorten", type=str, default="n")

    args = gdma_parser.parse_args()

    run_gdma_for_ang(
        file=args.fchk_filename,
        dma_filename=args.dma_filename,
        foreshorten_hydrogens=args.foreshorten,
    )
