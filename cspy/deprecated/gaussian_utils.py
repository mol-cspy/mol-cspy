from __future__ import print_function


def gaussian_add_arguments(parser):
    parser.add_argument(
        "-np",
        "--nprocs",
        type=str,
        help="Number of processors to use in each Gaussian run",
        default="1",
    )
    parser.add_argument(
        "-fun",
        "--functional",
        type=str,
        help="DFT functional to use in each Gaussian run",
        default="B3LYP",
    )
    parser.add_argument(
        "-bas",
        "--basis_set",
        type=str,
        help="Basis set to use in each Gaussian run",
        default="6-311G**",
    )
    parser.add_argument(
        "-basfile",
        "--basis_file",
        type=str,
        help="File contains the basis set information that are not standard in Gaussian",
        default="",
    )
    parser.add_argument(
        "-mem",
        "--memory",
        type=str,
        help="Memory to use in each Gaussian run",
        default="1GB",
    )
    parser.add_argument(
        "--chelpg",
        action="store_true",
        help="Use CHelpG to calculate partial charges ",
        default=False,
    )
    parser.add_argument(
        "--gaussian_cleanup",
        action="store_true",
        help="Clean up gaussian output",
        default=False,
    )
    parser.add_argument(
        "-vol",
        "--molecular_volume",
        action="store_true",
        help="Calculate molecular volume",
        default=False,
    )
    parser.add_argument(
        "-pcm",
        "--polarizable_continuum",
        action="store_true",
        help="Use a Polarizable Continuum Model",
        default=False,
    )
    parser.add_argument(
        "-epcm",
        "--external_iteration_pcm",
        action="store_true",
        help="Use a Polarizable Continuum Model",
        default=False,
    )
    parser.add_argument(
        "-esp",
        "--dielectric_constant",
        type=str,
        help="Dielectric constant for Polarizable Continuum",
        default="3.0",
    )
    parser.add_argument(
        "-gopt",
        "--opt",
        type=str,
        help="Options within gaussian optimisation",
        default="ModRedundant,",
    )
    parser.add_argument("--iso", type=str, help="ISO value", default="")
    parser.add_argument(
        "--freq",
        type=str,
        default=False,
        help="Options for frequency, use a blank to turn on default",
    )

    parser.add_argument(
        "--gaussian_all_molecules",
        action="store_true",
        help="Run Gaussian on all molecules.",
        default=False,
    )
    parser.add_argument(
        "--additional_args",
        type=str,
        help="Additional arguments for gaussian",
        default="",
    )
    parser.add_argument(
        "--set_molecular_states",
        type=str,
        default=None,
        help="Set the charge and spin multiplicity for each molecule in the crystal e.g. '0,1 0,3 -1,1' would set the first molecule as a singlet, second as a triplet and third as a negatively charged singlet state, enter as a string",
    )
    sp = parser.add_mutually_exclusive_group()
    sp.add_argument(
        "--force_rerun",
        action="store_true",
        default=True,
        dest="force_rerun",
        help="Force re-running of Gaussian - NOTE: not widely used.",
    )
    sp.add_argument(
        "--no-force_rerun", action="store_false", default=True, dest="force_rerun"
    )


def gaussian_parse_args(args=None):
    import argparse

    gaussian_parser = argparse.ArgumentParser(
        description="Arguments to control the Gaussian execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    gaussian_add_arguments(gaussian_parser)
    gaussian_args, unknown = gaussian_parser.parse_known_args(args=args)
    return gaussian_args


def add_gaussian_argument_group(parser):
    gaussian_parser = parser.add_argument_group(
        "Gaussian Arguments", "Arguments to control the Gaussian execution"
    )
    gaussian_add_arguments(gaussian_parser)
