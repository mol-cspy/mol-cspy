from __future__ import print_function
from cspy.executable.locations import NEIGHCRYS_EXEC

from cspy.deprecated.file_utils import is_file_readable
from cspy.util import molecular_formula
from cspy.deprecated.cspy_exceptions import CSPyException, CSPyDebugException
import logging
import os
import numpy as np

neighcrys_logger = logging.getLogger("CSPy.neighcrys")


def neighcrys_add_arguments(parser):
    parser.add_argument(
        "-pf",
        "--potential_file",
        type=str,
        help="potentials file for DMACRYS",
        default="../w99_original.pots",
    )
    parser.add_argument(
        "-bf",
        "--bondlength_file",
        type=str,
        help="potentials file for DMACRYS",
        default="bond_cutoffs",
    )
    parser.add_argument(
        "-mf",
        "--multipole_file",
        type=str,
        help="multipole file for DMACRYS",
        default=None,
    )
    parser.add_argument(
        "-af", "--axis_file", type=str, help="axis file for DMACRYS", default=None
    )
    parser.add_argument(
        "-lf",
        "--lattice_factor",
        type=float,
        help="Expand lattice by this factor",
        default=1.0,
    )
    parser.add_argument(
        "--step_size", type=float, help="Optimisation step size", default=0.5
    )
    parser.add_argument(
        "--standardise_bonds", type=str, help="Standardise Bond Lengths", default="n"
    )
    parser.add_argument(
        "--intermolecular_distance",
        type=float,
        help="Max Inter-Molecular Distance",
        default=4.0,
    )
    parser.add_argument(
        "--max_iterations", type=int, help="Max Iterations", default=1000
    )
    parser.add_argument("--vdw_cutoff", type=float, help="VdW Cutoff", default=15.0)
    parser.add_argument(
        "--foreshorten_hydrogens", type=str, help="Foreshorten Hydrogens", default="y"
    )
    parser.add_argument("--wcal_ngcv", type=str, help="Turn WCAL NGCV on", default="n")
    parser.add_argument(
        "--force_field_type", type=str, help="Force field type, W, C, F", default="W"
    )
    parser.add_argument(
        "--neighcrys_input_mode",
        type=str,
        help="Neighcrys input mode: I or E",
        default="E",
        choices=["E", "I", "e", "i"],
    )
    parser.add_argument(
        "--max_search",
        type=int,
        help="Max search neighest neighbour unit cells for connectivity",
        default=3,
    )
    parser.add_argument(
        "--check-anisotropy",
        help="Turn off anisotropy check. Only for debugging purposes. Don't use it.",
        default=False,
    )
    parser.add_argument(
        "--custom_atoms_anisotropy",
        help="Create axis files for these atoms using neighcrys_utils routine. See info in subroutine",
        default="F,Cl,Br,I",
    )
    parser.add_argument(
        "--custom_labels",
        help="The names of these will determine forcefield parameters and other things",
        default=None,
    )
    parser.add_argument(
        "--pressure", type=str, help="Pressure for dmacrys run.", default=""
    )
    parser.add_argument(
        "--limi", type=float, help="Optimisation criteria", default=None
    )
    parser.add_argument(
        "--limg", type=float, help="Optimisation criteria", default=None
    )
    parser.add_argument(
        "--gdma_dist_tol", type=float, help="Optimisation criteria", default=1e-6
    )
    parser.add_argument(
        "--paste_coords",
        type=str,
        default="n",
        help="Paste in coordinates of a new molecular structure (y/n)",
    )
    parser.add_argument(
        "--paste_coords_file",
        type=str,
        help="File containing new molecular coordinates to paste in",
        default="",
    )
    parser.add_argument(
        "--raise_before_neighcrys_call",
        action="store_true",
        default=False,
        help="Raise a CSPyDebugException before neighcrys is called",
    )
    parser.add_argument(
        "--remove_symmetry_subgroup",
        type=str,
        nargs="*",
        help="Optimisation criteria",
        default=["0"],
    )
    parser.add_argument(
        "--dummy_atoms",
        action="store_true",
        help="Experimental: Add dummy atoms to molecules in order to run linear molecules (requires compatible neighcrys/dmacrys, removes dummy atoms from output res files)",
        default=False,
    )


def neighcrys_parse_args(args=None):
    import argparse

    neighcrys_parser = argparse.ArgumentParser(
        description="Arguments to control the Neighcrys execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    neighcrys_add_arguments(neighcrys_parser)
    neighcrys_args, unknown = neighcrys_parser.parse_known_args(args=args)
    return neighcrys_args


def add_neighcrys_argument_group(parser):
    neighcrys_parser = parser.add_argument_group(
        "Neighcrys Arguments", "Arguments to control the Neighcrys execution"
    )
    neighcrys_add_arguments(neighcrys_parser)


class NeighCrysInput:
    def __init__(self, changes=None):
        if changes is None:
            changes = {}
        # get defaults from neighcrys_args
        default_args = vars(neighcrys_parse_args([""]))

        # overide defaults with anything in changes
        extra_options = dict(list(default_args.items()) + list(changes.items()))

        # Options that are currently fixed
        self.options = {}
        self.options.update({"Input Mode": "I"})
        self.options.update({"Structure Filename": None})
        self.options.update({"Bond Centre Sites": "n"})
        self.options.update({"WCALNGCV": "n"})
        self.options.update({"Symmetry Subgroup": ["0"]})
        self.options.update({"Punch Exists": "n"})
        self.options.update({"Punch Filename": None})
        self.options.update({"Axis Exists": "n"})
        self.options.update({"Axis Filename": None})
        self.options.update({"Paste Co-ordinates": "n"})
        self.options.update({"Paste Co-ords File": None})

        # Update possible userdefined options, defaults taken from neighcrys_args function
        self.options_update(extra_options)

        #        self.options.update({"Step Size":"0.5"})
        #        self.options.update({"Max Iterations":"1000"})
        #        self.options.update({"Standardise Bond Lengths":"n"})
        #        self.options.update({"Max Inter-Molecular Distance":"4.0000"})
        #        self.options.update({"Bondlengths Filename":"cutoff"})
        #        self.options.update({"Potentials Exists":"y"})
        #        self.options.update({"Potentials Filename":"w99_original.pots"})
        #        self.options.update({"Forcefield":"W"})
        #        self.options.update({"Foreshorten Hydrogens":"y"})
        #        self.options.update({"VdW Cutoff":"15"})
        self._long_pots_warning = False
        self._long_axis_warning = False
        self._long_mult_warning = False
        self._long_labels_warning = (
            False  # Implement this at some point if needed - dhc
        )

        # get hold of debug options
        self.raise_before_neighcrys_call = extra_options["raise_before_neighcrys_call"]

        self.recalculate_input()
        return

    def options_update(self, changes):
        for key, value in changes.items():
            if key == "potential_file":
                if value not in ["", None, False]:
                    self.options.update({"Potentials Exists": "y"})
                    self.options.update({"Potentials Filename": value})
                else:
                    self.options.update({"Potentials Exists": "n"})
                    self.options.update({"Potentials Filename": ""})
            elif key == "bondlength_file":
                self.options.update({"Bondlengths Filename": value})
            elif key == "intermolecular_distance":
                self.options.update({"Max Inter-Molecular Distance": str(value)})
            elif key == "standardise_bonds":
                self.options.update({"Standardise Bond Lengths": value})
            elif key == "step_size":
                self.options.update({"Step Size": str(value)})
            elif key == "max_iterations":
                self.options.update({"Max Iterations": str(value)})
            elif key == "vdw_cutoff":
                self.options.update({"VdW Cutoff": str(value)})
            elif key == "force_field_type":
                self.options.update({"Forcefield": value})
            elif key == "custom_labels":
                self.options.update({"Custom Labels": value})
            elif key == "wcal_ngcv":
                self.options.update({"WCALNGCV": value})
            elif key == "foreshorten_hydrogens":
                self.options.update({"Foreshorten Hydrogens": value})
            elif key == "max_search":
                self.options.update({"Max Search": str(value)})
            elif key == "neighcrys_input_mode":
                self.options.update({"Input Mode": str(value)})
            elif key == "pressure":
                self.options.update({"Pressure": str(value)})
            elif key == "limi":
                self.options.update({"LIMI": value})
            elif key == "limg":
                self.options.update({"LIMG": value})
            elif key == "remove_symmetry_subgroup":
                self.options.update({"Symmetry Subgroup": list(map(str, value))})
            elif key == "paste_coords":
                self.options.update({"Paste Co-ordinates": value})
            elif key == "paste_coords_file":
                self.options.update({"Paste Co-ordinates File": value})

    def set_custom_labels(self, labels):
        self.options.update({"Custom Labels": labels})
        self.recalculate_input()
        return

    def set_structure_filename(self, filename):
        self.options.update({"Structure Filename": filename})
        self.recalculate_input()
        return

    def set_punch_filename(self, filename, exists="y"):
        self.options.update({"Punch Exists": exists})
        self.options.update({"Punch Filename": filename})
        self.recalculate_input()
        return

    def set_axis_filename(self, filename, exists="n"):
        self.options.update({"Axis Exists": exists})
        self.options.update({"Axis Filename": filename})
        self.recalculate_input()
        return

    def set_radiation_source(self, rad_src):
        if self.options["Standardise Bond Lengths"].lower() == "y":
            pass  # Maintain explicit choice to do this (default is n)
        elif rad_src.lower() in ["neutron", "csp"]:
            self.options.update({"Standardise Bond Lengths": "n"})
        elif rad_src.lower() in ["xray", "x-ray"]:
            self.options.update({"Standardise Bond Lengths": "y"})
        else:
            neighcrys_logger.error(
                "unknown rad_src values. given:"
                + rad_src
                + " require: csp,neutron,xray or x-ray"
            )
            raise CSPyException(
                "unknown rad_src values. given:"
                + rad_src
                + " require: csp,neutron,xray or x-ray"
            )
        return

    def recalculate_input(self):
        self.input_lines = []
        self.input_lines.append(self.options["Input Mode"])
        self.input_lines.append(self.options["Structure Filename"])
        self.input_lines.append(self.options["Bondlengths Filename"])
        self.input_lines.append(self.options["Max Inter-Molecular Distance"])
        self.input_lines.append(self.options["Bond Centre Sites"])
        self.input_lines.append(self.options["Standardise Bond Lengths"])
        if self.options["Input Mode"] == "E":
            self.input_lines.append(self.options["WCALNGCV"])
            self.input_lines.append(self.options["Step Size"])
            self.input_lines.append(self.options["Max Iterations"])
            self.input_lines.append(self.options["VdW Cutoff"])
            self.input_lines.append(self.options["Max Search"])
        self.input_lines.append(self.options["Forcefield"])
        if self.options["Forcefield"] == "C":
            if self.options["Custom Labels"]:
                self.input_lines.append(self.options["Custom Labels"])
            else:
                neighcrys_logger.info(
                    "If you give a custom forcefield, please tell neighcrys which labels you've used"
                )
                raise CSPyException(
                    "If you give a custom forcefield, please tell neighcrys which labels you've used"
                )
        self.input_lines.append(self.options["Foreshorten Hydrogens"])
        for subgroup in self.options["Symmetry Subgroup"]:
            self.input_lines.append(str(subgroup))
        if self.options["Symmetry Subgroup"][0] != str(0):
            self.input_lines.append(str(0))
        self.input_lines.append(self.options["Punch Exists"])
        if self.options["Punch Exists"] == "y":
            if len(self.options["Punch Filename"]) > 80:
                import os

                if not self._long_mult_warning:
                    self._long_mult_warning = True
                    neighcrys_logger.warning(
                        "Punch filename too long, symbolic linking to s.mult"
                    )
                os.symlink(self.options["Punch Filename"], "s.mult")
                self.input_lines.append("s.mult")
            else:
                self.input_lines.append(self.options["Punch Filename"])
        self.input_lines.append(self.options["Axis Exists"])
        if self.options["Axis Exists"] == "y":
            if len(self.options["Axis Filename"]) > 20:
                import os

                if not self._long_axis_warning:
                    self._long_axis_warning = True
                    neighcrys_logger.warning(
                        "Axis filename too long, symbolic linking to s.axis"
                    )
                try:
                    os.remove("s.axis")
                except Exception as exc:
                    pass
                os.symlink(self.options["Axis Filename"], "s.axis")
                self.input_lines.append("s.axis")
            else:
                self.input_lines.append(self.options["Axis Filename"])
        self.input_lines.append(self.options["Paste Co-ordinates"])
        # If pasting new molecular coordinates, copy these options but check that a file of coords is specified, else exception
        if self.options["Paste Co-ordinates"] == "y":
            if self.options["Paste Co-ordinates File"] not in ["", None, False]:
                self.input_lines.append(self.options["Paste Co-ordinates File"])
            else:
                neighcrys_logger.info(
                    "Paste coordinates requested but no molecular coordinates file specified!"
                )
                raise CSPyException(
                    "Paste coordinates requested but no molecular coordinates file specified!"
                )
        self.input_lines.append(self.options["Potentials Exists"])
        if self.options["Potentials Exists"] == "y":
            if len(self.options["Potentials Filename"]) > 20:
                import os

                if not self._long_pots_warning:
                    self._long_pots_warning = True
                    neighcrys_logger.info(
                        "Potential filename too long, symbolic linking to s.pots"
                    )
                try:
                    os.remove("s.pots")
                except Exception as exc:
                    pass
                os.symlink(self.options["Potentials Filename"], "s.pots")
                self.input_lines.append("s.pots")
            else:
                self.input_lines.append(self.options["Potentials Filename"])
        return

    def write_input_file(self, filename):
        self.input_filename = filename
        try:
            with open(self.input_filename, "w+") as input_file:
                for line in self.input_lines:
                    input_file.write(line + "\n")
        except IOError as exc:
            neighcrys_logger.error(
                "Neighcrys input file could not be opened for writing: %s",
                self.input_filename,
            )
            raise CSPyException(
                "Neighcrys input file could not be opened for writing: "
                + self.input_filename
            )

    def run_neighcrys(self, infile, TestOutput=False, neighcrysexec=NEIGHCRYS_EXEC):
        from subprocess import Popen
        import os

        f = open(infile, "r")
        o = open(infile + "_stdout", "w+")
        if self.raise_before_neighcrys_call:
            raise CSPyDebugException(
                "Debug flag read, raising before NEIGHCRYS_EXEC call"
            )
        try:
            neighrun = Popen([neighcrysexec], stdin=f, stdout=o)
        except OSError as exc:
            neighcrys_logger.error(
                "Problem running neighcrys executable:" + neighcrysexec
            )
            raise CSPyException("Problem running neighcrys executable:" + neighcrysexec)
        neighrun.wait()  # Needed for slow file system as full output may be buffered etc.
        f.close()
        o.close()
        if is_file_readable(open(infile).readlines()[1].strip() + ".dmain"):
            old_lines = open(
                open(infile).readlines()[1].strip() + ".dmain", "r"
            ).readlines()
            f = open(open(infile).readlines()[1].strip() + ".dmain", "w")
            for line in old_lines:
                f.write(line)
                if "STAR PLUT" == line.strip():
                    if self.options["Pressure"] != "":
                        f.write("PRES " + self.options["Pressure"] + "\n")
                    if self.options["LIMI"] is not None:
                        f.write("LIMI " + str(self.options["LIMI"]) + "\n")
                    if self.options["LIMG"] is not None:
                        f.write("LIMG " + str(self.options["LIMG"]) + "\n")
            f.close()

        if TestOutput:  # not tested, shouldn't be used yet
            fort21 = "./fort.21"
            if (
                os.path.exists(fort21)
                and os.path.isfile(fort21)
                and os.access(fort21, os.R_OK)
            ):
                neighcrys_logger.debug("File fort.21 exists and is readable by system")
            else:
                neighcrys_logger.debug("File fort.21 does not exist")
                raise Exception(
                    "I/O Error: File 'fort.21' does not exist. This is indicative of a failed run."
                )
            fort21_f = open(fort21, "r")
            basis = 0
            for line in fort21_f:
                if "basis N" in line:
                    basis = basis + 1
            if basis == 0:
                raise Exception(
                    "FileCheckError: There is an error in the file fort.21."
                )
            fort21_f.close()

        return

    def make_axis_file(self, struct_name, axis_filename, neighcrys_args={}):
        #        neighin=NeighCrysInput(dict({'Structure Filename':struct_name,"Axis Exists":"n",
        #                                  "Bondlengths Filename":"./cutoff"}.items()+neighcrys_args.items() ))
        self.options["Axis Exists"] = "n"
        self.recalculate_input()
        self.write_input_file(filename=struct_name + ".axis.neighcrysin")
        self.run_neighcrys(infile=struct_name + ".axis.neighcrysin")
        self.options["Axis Exists"] = "y"
        return

    def make_anisotropic_axis_file(
        self, crystal, struct_name, axis_filename, neighcrys_args={}
    ):
        """Makes an axis file. This function replaces the older 
           make_axis_file(). This function can handle monoatomic
           species, large Z' and will automatically generate
           anisotropic axes for all halogens.
        """
        neighcrys_logger.info("Attempting to autogenerate a suitable axis file.")
        self.options["Axis Exists"] = "n"
        self.recalculate_input()
        input_filename = struct_name + ".axis.neighcrysin"
        self.write_input_file(filename=input_filename)
        self.run_neighcrys(infile=input_filename)

        anis = False
        anis_atom_list = []  # List of strings, one for each anisotropic atom

        # Get NEIGHCRYS labelling scheme
        with open("fort.21", "r") as f:
            FORT21_CONTENTS = f.read().split("\n")
        # fort21 = f.read().split('\n')  # list of strings.
        # f.close()                      # close the file
        #        print neighcrys_args;exit()
        def defined_halogens(anisotropic_elements, token_atom):
            """Make ANIS axes for these elements- previously these were just Halogens, hence name is an artifact
               You may want to turn this on for terminal atoms (halogens, hydrogens) but will need modification for 
               polyvalent atoms. You can turn default atoms off if they are accounted for by neighcrys. token_atom provides dictionary"""
            custom_dict = {}
            for ele in anisotropic_elements.split(","):
                try:
                    custom_dict[token_atom.atomic_numbers[ele]] = ele
                except Exception as exc:
                    neighcrys_logger.warning(
                        "Anisotropic element error for " + str(ele)
                    )
            return custom_dict

        halo = defined_halogens(
            neighcrys_args["custom_atoms_anisotropy"],
            crystal.unique_molecule_list[0].atoms[0],
        )
        neighcrys_name = []

        def get_neighcryslabel(atom_label, inversionflag):
            label = ""
            fort21 = iter(FORT21_CONTENTS)
            for line in fort21:
                if "Equivalent basis atoms" in line:
                    neighcrys_logger.debug("Found equivalent atom list in fort.21")
                    for _ in range(4):  # Jump to the data part of fort.21
                        nline = next(fort21)
                    while True:  # loop over all atoms.
                        nline = next(fort21).strip()  # read the line

                        if nline == "":  # reached the end

                            neighcrys_logger.debug("Read all")

                            break

                        if atom_label == nline.split()[3]:
                            neighcrys_logger.debug("Found this line:")
                            neighcrys_logger.debug(str(nline))
                            if inversionflag == nline.split()[5]:
                                label = nline.split()[2][:10]

            return label

        fort21 = iter(FORT21_CONTENTS)
        for line in fort21:
            if "Equivalent basis atoms" in line:
                # print "Found equivalent atom list in fort.21"
                for _ in range(4):  # Jump to the data part of fort.21
                    nline = next(fort21)

                while True:  # loop over all atoms.
                    nline = next(fort21).strip()  # read the line

                    if nline == "":  # reached the end

                        # print "Read all basis atoms in fort.21"
                        break
                    snline = nline.split()  # make a list of strings of the line.
                    atom_number = int(snline[1])  # atom number for this basis atom
                    # neighcrys_name = snline[2] # neighcrys name
                    # input_name = snline[3]  # label in res file
                    if atom_number in halo:
                        anis = (
                            True  # We have halogens and should have an anisotropic axis
                        )
                        neighcrys_name += [snline[2]]  # neighcrys name of this atom.
                        anis_atom_list += [snline[3]]  # label in res file of this atom.

                break

        os.rename(struct_name.replace(".res", ".mols"), axis_filename)
        with open(axis_filename) as f:
            neighcrys_logger.debug("Axis file before modification:\n%s", f.read())

        if not os.path.exists(axis_filename):
            raise CSPyException("Neighcrys failed to create an axis file")

        if not anis:
            guests = 0
            for z_prime, fragment in enumerate(
                crystal.unique_molecule_list
            ):  # loop over asymmetric units
                # Get the connectivity matrix of this molecule
                conn = fragment.connectivity_matrix()
                neighcrys_logger.debug(
                    "Connectivity matrix of molecule " + str(z_prime) + " is:"
                )
                neighcrys_logger.debug(str(conn))
                # Get the number of monoatomic guests
                if len(fragment.atoms) == 1:
                    guests += 1
                    # print "Found a monoatomic thing."

            with open(struct_name.replace(".res", ".anis"), "r") as f:
                axis = f.read().split("\n")  # list of strings
            axis[0] = "MOLX " + str(len(crystal.unique_molecule_list) - guests)
            axis = "\n".join(axis)

            with open(axis_filename, "w") as f:
                f.write(axis)  # overwrite axis file.

            self.options["Axis Exists"] = "y"  # Found no halogens, so we quit.
            neighcrys_logger.info("Axis file created.")
            neighcrys_logger.debug("No halogens found, created axis file.")
            return

        neighcrys_name_set = set(neighcrys_name)  # remove duplicates by making it a set
        neighcrys_logger.debug(" ".join(neighcrys_name_set))
        anis_atom_set = set(anis_atom_list)
        axis_string = "\nANIS\n"
        neighcrys_logger.info(
            "Will make anisotropic axes for these atoms: %s", " ".join(anis_atom_set)
        )

        # Now use the crystal/Molecule/Atom classes to figure out connectivity
        # for Z'>1 fragment geometry and treat each separately.

        inversion = True
        # Negative lattice type tells presence of inversion
        if int(crystal.latt) < 0:
            neighcrys_logger.info("There are no inversions.")
            inversion = False

        neighcrys_logger.info("Crystal is Z'= %d", len(crystal.unique_molecule_list))
        guests = 0

        # loop over molecules in asymmetric unit
        for z_prime, fragment in enumerate(crystal.unique_molecule_list):
            # Get the connectivity matrix of this molecule
            conn = fragment.connectivity_matrix()
            neighcrys_logger.debug(
                "Connectivity of molecule %d is:\n%s",
                z_prime,
                "\n".join(map(str, conn)),
            )

            # Get the number of monoatomic guests

            if len(fragment.atoms) == 1:
                guests += 1
                # print "Found a monoatomic thing."

            for i, anisotropic_atom in enumerate(anis_atom_set):
                # loop over atoms in fragment
                for natom, atom in enumerate(fragment.atoms):
                    if atom.label == anisotropic_atom:
                        # We have a match.
                        neighcrys_logger.debug(
                            "Atom %s belongs to molecule %d", atom.label, z_prime
                        )
                        # Now find the first neighbour(s) of this atom.
                        neighcrys_logger.debug(str(conn[natom]))
                        neighcrys_logger.debug("It is bonded to: ")
                        # boolean list of
                        for n, neighbour in enumerate(conn[natom]):
                            if neighbour == 1:
                                neighcrys_logger.debug(str(fragment.atoms[n].label))
                                # Now find out what this little fellow is bonded to...
                                # print "   Which in turn is bonded to:"
                                for mm, second_neighbour in enumerate(conn[n]):
                                    if second_neighbour == 1:
                                        if atom.label != fragment.atoms[mm].label:
                                            neighcrys_logger.debug(
                                                "which is bonded to %s",
                                                fragment.atoms[mm].label,
                                            )
                                            neighcrys_logger.debug(
                                                "Getting Neighcrys label for %s",
                                                atom.label,
                                            )
                                            lbl = get_neighcryslabel(atom.label, "F")
                                            axis_string += lbl + "\n" + "Z LINE  "
                                            # now add Z LINE
                                            lbl = (
                                                get_neighcryslabel(
                                                    fragment.atoms[n].label, "F"
                                                )
                                                + " "
                                                + get_neighcryslabel(atom.label, "F")
                                                + " 1"
                                            )
                                            axis_string += lbl + "\n" + "X PLANE "

                                            # and add X PLANE
                                            lbl = (
                                                get_neighcryslabel(atom.label, "F")
                                                + " "
                                            )
                                            lbl += (
                                                get_neighcryslabel(
                                                    fragment.atoms[n].label, "F"
                                                )
                                                + " 1 "
                                            )
                                            lbl += (
                                                get_neighcryslabel(
                                                    fragment.atoms[mm].label, "F"
                                                )
                                                + " 2"
                                            )
                                            axis_string += lbl + "\n"

                                            # And for inversions...
                                            if inversion:
                                                # print "There is an inverted atom."
                                                lbl = get_neighcryslabel(
                                                    atom.label, "T"
                                                )
                                                neighcrys_logger.debug(
                                                    "Label for %s: %s", atom.label, lbl
                                                )
                                                axis_string += lbl + "\n" + "Z LINE  "
                                                # now add Z LINE
                                                lbl = (
                                                    get_neighcryslabel(
                                                        fragment.atoms[n].label, "T"
                                                    )
                                                    + " "
                                                    + get_neighcryslabel(
                                                        atom.label, "T"
                                                    )
                                                    + " 1"
                                                )
                                                neighcrys_logger.debug("Label: %s", lbl)
                                                axis_string += lbl + "\n" + "X PLANE "

                                                # and add X PLANE
                                                lbl = (
                                                    get_neighcryslabel(atom.label, "T")
                                                    + " "
                                                )
                                                lbl += (
                                                    get_neighcryslabel(
                                                        fragment.atoms[n].label, "T"
                                                    )
                                                    + " 1 "
                                                )
                                                lbl += (
                                                    get_neighcryslabel(
                                                        fragment.atoms[mm].label, "T"
                                                    )
                                                    + " 2"
                                                )
                                                axis_string += lbl + "\n"

                                            break  # We only need one second neighbour, so we quit here.
        # Open axis file and add anisotropic part.
        axis_string += "ENDS\nENDS"
        neighcrys_logger.debug("Anis axis string:\n'''\n%s\n'''", axis_string)

        # list of strings
        with open(axis_filename, "r") as f:
            axis = f.read().split("\n")

        # print "Old axis:"
        # print axis
        # Neighcrys sometimes gets MOLX wrong, it can't handle large Z'.
        # We replace MOLX *.
        # print "Number of monoatomic guests: ", guests
        if guests > 0:
            neighcrys_logger.info(
                "Found %d monoatomic guests, " "these will not be given axes.", guests
            )
        nmols = (
            len(crystal.unique_molecule_list) - guests
        )  # don't count monoatomic species.
        axis[0] = "MOLX " + str(nmols)
        axis[-2:] = ""  # delete ENDS line
        axis = "\n".join(axis)
        axis += axis_string  # append anisotropic axis stuff
        neighcrys_logger.debug("Final axis string:\n'''\n%s\n'''", axis_string)
        # print axis
        with open(axis_filename, "w") as f:
            f.write(axis)  # overwrite axis file.

        neighcrys_logger.info("Anisotropic axis file created.")
        self.options["Axis Exists"] = "y"
        return

    def get_neighcrys_labels(self):
        self.recalculate_input()
        struct_name = self.options["Structure Filename"]
        self.write_input_file(filename=struct_name + ".labels.neighcrysin")
        self.run_neighcrys(infile=struct_name + ".labels.neighcrysin")
        read = False
        labels = {}
        neighcrys_logger.info("reading fort21")
        lines = iter(open("fort.21", "r"))
        for line in lines:
            if read:
                if line.strip() == "":
                    break
                sl = line.split()
                labels[sl[3]] = sl[2][:10]
            if "Inequivalent basis atoms" in line:
                read = True
                [next(lines) for _ in range(4)]
        return labels


def create_geom_files(foreshorten_hydrogens):
    """Reads fort.21 and for every molecule it creates a geom file. Geom files are not created for monoatomic species."""
    import os

    #    print "Cheating to get geom file "; os.system("cp ../geom1 .");return
    # Gets Number of Symmetry Copies
    try:
        fort21 = open("fort.21", "r+")
    except IOError as exc:
        neighcrys_logger.error(
            "fort.21 could not be opened for reading. Problem occurs in folder: "
            + os.getcwd()
        )
        raise CSPyException(
            "fort.21 could not be opened for reading. Problem occurs in folder: "
            + os.getcwd()
        )
    else:

        NumberOfRepeats = None
        for line in fort21:
            if "GROUP MULTIPLICATION TABLE" in line:
                lin = next(fort21)
                NumberOfRepeats = lin.split()[-1]
                break
        neighcrys_logger.debug("Found NumberOfRepeats = %s", NumberOfRepeats)
        # Gets list of Inequiv Basis Atoms
        InequivBasisAtoms = []
        for line in fort21:
            if "Inequivalent basis atoms" in line:
                break
        atom = 0
        for line in fort21:
            if "atom" in line:
                atom += 1
            if atom > 1:
                break
            if len(line.split()) != 0:
                if line.split()[0] != "atom":
                    if line.split()[0] != "index":
                        InequivBasisAtoms.append(line.split())
        # print("Found "+str(len(InequivBasisAtoms))+" InequivBasisAtoms")

        InequivMolLabels = []
        #        print InequivMolLabels , InequivBasisAtoms
        for entry in InequivBasisAtoms:
            if entry[4] not in InequivMolLabels:
                InequivMolLabels.append(entry[4])
        # Monoatomic species are given molecule label 0, we remove these from the list so geom files
        # are not created for things like Xe.
        if "0" in InequivMolLabels:
            InequivMolLabels.remove("0")
        NumInequivMols = len(InequivMolLabels)
        InequivMols = []
        # print "Inequivalent molecule labels: ", InequivMolLabels
        if foreshorten_hydrogens.lower() == "y":
            search = "foreshortened hydrogens for molecule   " + "%2s"
        elif foreshorten_hydrogens.lower() == "n":
            search = " Atom positions in local axis system for molecule   " + "%2s"
        for label in InequivMolLabels:  # loop over molecule numbers.
            # print "Processing ", label
            search_string = search % (label)
            neighcrys_logger.info("Looking for: '%s'...", search_string)
            mol = []
            basis = 0
            for line in fort21:
                if search_string in line:
                    neighcrys_logger.info("found")
                    break
            for line in fort21:
                if "basis" in line:
                    basis += 1
                if basis > 1:
                    break
                if len(line.split()) != 0:
                    if line.split()[0] != "basis":
                        mol.append(line.strip())
            InequivMols.append(mol)
        fort21.close()
    # Write geom file
    n = 1
    while n <= NumInequivMols:  # for every molecule...
        name = "geom" + str(n)
        # print "Creating ", name
        geom = open(name, "w")  # make a geomfile
        # print "writing geom file:"
        for line in InequivMols[n - 1]:
            # print line
            geom.write(str(line) + "\n")
        geom.close()
        neighcrys_logger.info("Created " + str(name))
        n += 1
    return


def create_com_files_from_geom_files(
    gaussian_args={},
    Comment="Blank",
    Charge=0,
    Mult=1,
    ChkDir="./",
    allow_empty_geom=False,
):
    import os
    import glob
    from cspy.deprecated.gaussian_utils import gaussian_parse_args

    gaussian_default_args = vars(gaussian_parse_args())
    gaussian_default_args.update(gaussian_args)
    gaussian_args = gaussian_default_args

    # Gets Number of Symmetry Copies
    # Tests how many non empty geom files there are
    geoms = glob.glob("./geom*")
    neighcrys_logger.info("Creating Gaussian input files.")
    #    if os.path.getsize(f) == 0:
    #        raise Exception("Empty geom file encountered.  This suggests an error in the run.")
    NumGeoms = int(len(geoms))
    # Calculate number of inequivalent molecules
    InequivBasisAtoms = []
    try:
        fort21 = open("fort.21", "r")
    except IOError as exc:
        neighcrys_logger.error(
            "fort.21 could not be opened for reading. Problem occurs in folder: "
            + os.getcwd()
        )
        raise CSPyException(
            "fort.21 could not be opened for reading. Problem occurs in folder: "
            + os.getcwd()
        )
    else:
        for line in fort21:
            if "Inequivalent basis atoms" in line:
                break
        atom = 0
        for line in fort21:
            if "atom" in line:
                atom += 1
            if atom > 1:
                break
            if len(line.split()) != 0:
                if line.split()[0] != "atom":
                    if line.split()[0] != "index":
                        InequivBasisAtoms.append(line.split())
        fort21.close()

    InequivMolLabels = []
    for entry in InequivBasisAtoms:
        if entry[4] not in InequivMolLabels:
            InequivMolLabels.append(entry[4])
    NumInequivMols = len(InequivMolLabels)
    GeomFlag = False
    if NumInequivMols != NumGeoms:
        # This will happen if there are monoatomic species, it's not a problem.
        neighcrys_logger.warning(
            "Number of geoms is not equal to calculated number of molecules. Recreating geom files."
        )
        #        create_geom_files()
        GeomFlag = True

    # converting geom to com file
    if GeomFlag:
        geoms = glob.glob("./geom*")
    for geom_counter in range(len(geoms)):
        f = str("./geom") + str(geom_counter + 1)
        if os.path.getsize(f) == 0:
            # Test if any geom file is empty
            if allow_empty_geom:  # We may allow empty geom files.
                neighcrys_logger.info(
                    "WARNING! Encountered an empty geom file - will allow it."
                )
                neighcrys_logger.warning(
                    "Geom file "
                    + str(geom_counter)
                    + " is empty! Accepting it as an uncharged monoatomic thing. Will not make com file from it."
                )

            else:  # Or we don't allow it and exit.
                neighcrys_logger.info(
                    "Encountered an empty geom file, something has gone wrong!"
                )
                neighcrys_logger.error("Encountered empty geom file - exiting")
                raise Exception(
                    "Empty geom file encountered. This suggests an error in the run."
                )

        else:

            fname = "molecule_" + str(geom_counter + 1)
            geom_f = open(f, "r")
            com_f = open(fname + ".com", "w")
            com = []
            labels = []
            for line in geom_f:
                tmp = []
                lin = line.split()
                label = str(lin[1])
                labels.append(label)
                tmp.append(label.split("_")[0])
                tmp.append(lin[2:5])
                com.append(tmp)
            formula = molecular_formula([l.split("_")[0] for l in labels])

            com_f.write("%mem=" + str(gaussian_args["memory"]) + "\n")
            com_f.write("%chk=" + str(ChkDir) + str(fname) + ".chk" + "\n")
            com_f.write("%nprocs=" + str(gaussian_args["nprocs"]) + "\n")
            if gaussian_args["functional"] == "AM1":
                com_f.write(
                    "#" + str(gaussian_args["functional"]) + " NoSymm FChk" + "\n\n"
                )
            else:
                com_f.write(
                    "#"
                    + str(gaussian_args["functional"]).replace("GD3BJ", "")
                    + "/"
                    + str(gaussian_args["basis_set"])
                    + " NoSymm FChk density=current "
                    + "\n"
                )
            if "GD3BJ" in gaussian_args["functional"]:
                com_f.write("# EmpiricalDispersion=GD3BJ\n")
            if gaussian_args["chelpg"]:
                com_f.write("POP=CHelpG" + "\n")
            if gaussian_args["external_iteration_pcm"]:
                com_f.write("SCRF=(PCM,ExternalIteration,Read)" + "\n")
            elif gaussian_args["polarizable_continuum"]:
                com_f.write("SCRF=(PCM,Read)" + "\n")
            com_f.write("\n")
        if Comment == "Blank":
            com_f.write("refcode from crystal structure coordinates, for DMA\n\n")
        else:
            com_f.write(str(Comment) + "\n\n")
        #       try:
        #           if 'charge' in config[formula].keys():
        #               Charge = config[formula]['charge']

        #           if 'multiplicity' in config[formula].keys():
        #               Mult = config[formula]['multiplicity']
        #       except KeyError:
        #           pass

        com_f.write(str(Charge) + " " + str(Mult) + "\n")
        for atom in com:
            com_f.write(
                "%2s %10.6f %10.6f %10.6f\n"
                % (atom[0], float(atom[1][0]), float(atom[1][1]), float(atom[1][2]))
            )
        if (
            gaussian_args["polarizable_continuum"]
            or gaussian_args["external_iteration_pcm"]
        ):
            com_f.write("\n")
            com_f.write("EPS=" + str(gaussian_args["dielectric_constant"]) + "\n")
            com_f.write("\n")
        elif gaussian_args[
            "basis_file"
        ]:  # If ever need basis_file and polarizable continuum,, change this
            com_f.write("\n")
        else:
            com_f.write("\n\n\n")
        if gaussian_args["basis_file"]:
            #            try:
            bas_file = open(str(gaussian_args["basis_file"]), "r")
            for line in bas_file.readlines():
                com_f.write(line)
            bas_file.close()
            #            except Exception as exc:
            #                print gaussian_args['basis_file']
            #                raise CSPyException("Error reading "+gaussian_args['basis_file'])
            if (
                gaussian_args["polarizable_continuum"]
                or gaussian_args["external_iteration_pcm"]
            ):
                com_f.write("\n")
                com_f.write("EPS=" + str(gaussian_args["dielectric_constant"]) + "\n")
                com_f.write("\n")
        com_f.close()
    return


def run_gdma_neighcrys(
    geom_filename, dma_filename, multipole_filename, tol=1e-6, allow_forshortening=True
):
    import re
    from cspy.deprecated.listmath import list3norm

    dma = []
    geom = []
    with open(geom_filename, "r") as gf:
        geom_lines = gf.readlines()

    natoms = 0
    for line in geom_lines:
        geom.append(line.strip().split())
        if "X_" != geom[-1][1][0:2]:
            natoms += 1
        if "X_" == geom[-1][1][0:2]:
            dma.append(
                [
                    [geom[-1][1][0:2]] + geom[-1][2:5] + ["Rank", "0\n"],
                    ["    0.0000000000\n"],
                ]
            )
    natoms = len(geom)

    with open(dma_filename) as df:
        lines = df.readlines()

    lines = [x for x in lines if not (x[0] in ("!", "U")) and x.strip()]
    i = 0
    while i < len(lines):
        line = lines[i]
        tokens = line.strip().split()  # symbol, x, y, z, "RANK", rank
        try:
            rank = int(tokens[5])
        except Exception as e:
            neighcrys_logger.error(
                "Expecting 5 tokens in line:\n%s, found:\n%s. Whole file:\n%s",
                line,
                tokens,
                "".join(lines),
            )
            raise e
        tokens[-1] = tokens[-1] + "\n"
        nmoments = 0
        for x in range(rank + 1):
            nmoments += 2 * x + 1
        multipoles = []

        while nmoments > 0:
            i += 1
            line = lines[i]
            moments = line.strip().split()
            multipoles.append(line)
            nmoments -= len(moments)

        if "X_" not in tokens[0]:
            dma.append([tokens, multipoles])
        i += 1

    # Check the coordinates match
    geom_xyz = np.asarray([[float(x) for x in g[2:5]] for g in geom])
    dma_xyz = np.asarray([[float(x) for x in d[1:4]] for d, m in dma])
    if geom_xyz.shape != dma_xyz.shape:
        neighcrys_logger.debug(
            "the molecules being compared have differing numbers of atoms"
        )
        raise CSPyException(
            "the molecules being compared have differing numbers of atoms"
        )

    diff = np.linalg.norm(dma_xyz - geom_xyz, axis=1)
    neighcrys_logger.debug("Diff: %s", diff)

    coords_match = False
    (idxs,) = np.where(np.logical_or(diff < tol, np.abs(diff - 0.1) < 0.1))
    if len(idxs) == natoms:
        coords_match = True
    #   else:
    #       # Try mirroring each axis
    #       for ax in range(3):
    #           geom_xyz[:, ax] = -geom_xyz[:, ax]
    #           diff = np.linalg.norm(dma_xyz - geom_xyz, axis=1)
    #           neighcrys_logger.debug('Diff: (mirrored ax=%d) %s', ax, diff)
    #           idxs, = np.where(
    #               np.logical_or(diff < tol, np.abs(diff - 0.1) < 0.1)
    #           )
    #           if len(idxs) == natoms:
    #               coords_match = True
    #               break
    #           geom_xyz[:, ax] = -geom_xyz[:, ax]

    if not coords_match:
        neighcrys_logger.debug(
            "the coordinates of atoms %s differ more than the tolerance", idxs
        )
        raise CSPyException(
            "the coordinates of atoms {} differ " "more than the tolerance".format(idxs)
        )

    for i in range(natoms):
        # compare the labels
        if re.sub("_", "", geom[i][1][0:2]) != dma[i][0][0].split("_")[0][0:2]:
            neighcrys_logger.error(
                "if %s != %s",
                re.sub("_", "", geom[i][1][0:2]),
                dma[i][0][0].split("_")[0][0:2],
            )
            neighcrys_logger.error(
                "the types of atom {:d} do not match: ".format(i)
                + str(geom[i][1])
                + str(dma[i][0][0])
            )
            raise CSPyException(
                "the types of atom {:d} do not match: ".format(i)
                + str(geom[i][1])
                + str(dma[i][0][0])
            )

        # otherwise replace the label
        dma[i][0][0] = geom[i][1]

    with open(multipole_filename, "w") as pf:
        # now dump the datafile
        for tokens, multipoles in dma:
            pf.write(" ".join(tokens))
            pf.write("".join(multipoles))


def make_cutoff_file(self, filename="cutoff", tolerance=1.01):

    if not isinstance(filename, str):
        neighcrys_logger.warn("make_cutoff_file passed a filename that is not a string")
        filename = str(filename)

    try:
        cutoff_file = open(filename, "w+")
    except IOError as exc:
        neighcrys_logger.error(
            "cutoff file could not be opened for writing: " + filename
        )
        raise CSPyException("cutoff file could not be opened for writing: " + filename)
    # Create a dictionary of bond types, whose values are the cutoff length
    BTypeDict = {}
    for molecule in self.unique_molecule_list:
        for (i, bond) in enumerate(molecule.bond_keys()):
            BType = tuple(
                sorted([molecule.atoms[bond[0]].cl, molecule.atoms[bond[1]].cl])
            )
            BLength = float(molecule.bond_value(bond[0], bond[1]))
            if BType not in BTypeDict:
                BTypeDict[BType] = BLength
            elif BLength > BTypeDict[BType]:
                BTypeDict[BType] = BLength
    NTypes = []
    for molecule in self.unique_molecule_list:
        for atm in molecule.atoms:
            NTypes.append(atm.cl)
    NTypes = list(set(NTypes))
    neighcrys_logger.debug("Element types: %s", NTypes)
    for i in range(len(NTypes)):
        for j in range(len(NTypes)):
            BType = tuple(sorted([NTypes[i], NTypes[j]]))
            if BType not in BTypeDict:
                BTypeDict[BType] = 0.0
    neighcrys_logger.debug("Bond type dict: %s", BTypeDict)
    for keys in BTypeDict:
        # Apply tolerance to bond length values -- may use larger values for e.g. flexible systems
        cutoff_file.write(
            keys[0].ljust(2, "_")
            + " "
            + keys[1].ljust(2, "_")
            + " "
            + str(BTypeDict[keys] * tolerance)
            + "\n"
        )

    cutoff_file.write("ENDS")
    cutoff_file.close()


def create_invert_neighcrys_label(neighcrys_label):
    """Replaces last underscore with an i. Thats it."""
    neighcrys_inv = neighcrys_label[:-1] + "i"
    return neighcrys_inv


def check_potential(
    self, filename=None, pf=None
):  # types are the opls types, filename is the FIELD file, pf is the potential filename
    from cspy.deprecated.potentials import W99Potential

    labels = {}
    read = False
    neighcrys_logger.info("reading fort21")
    lines = iter(open("fort.21", "r"))
    for line in lines:
        if read:
            if line.strip() == "":
                break
            sl = line.split()
            labels[sl[3]] = sl[2][:10]
        if "Inequivalent basis atoms" in line:
            read = True
            [next(lines) for _ in range(4)]
    label_set = []
    for key in labels:
        label_set.append(str(labels[key])[0:4])
    p = open(pf, "r")
    terms = {}
    s = ""
    for line in p:
        if "BUCK" in line.upper() or "LENN" in line.upper():
            nl = line.strip().split()
            term = tuple(sorted([nl[1], nl[2]]))
            s += line
        elif "END" in line.upper():
            s += line
            terms[term] = s
            s = ""
        else:
            s += line
    p.close()
    lines = []
    label_set = list(set(label_set))  # keep unique entries
    lset = []
    for i in range(len(label_set)):
        for j in range(len(label_set)):
            lset.append(tuple(sorted([label_set[i], label_set[j]])))
    label_set = list(set(lset))
    for i in range(len(label_set)):
        if label_set[i] in terms:
            lines.append(terms[label_set[i]])
        else:
            neighcrys_logger.error(
                "Potential term "
                + str(label_set[i][0])
                + " "
                + str(label_set[j][1])
                + " does not exist in specified potential file ("
                + str(pf)
                + "), quitting"
            )
            raise CSPyException(
                "Potential term "
                + str(label_set[i][0])
                + " "
                + str(label_set[j][1])
                + " does not exist in specified potential file ("
                + str(pf)
                + "), quitting"
            )
    pfile = open(filename, "w")
    pfile.write("".join(lines))
    pfile.close()
    return
