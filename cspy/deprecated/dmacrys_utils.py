from __future__ import print_function
from cspy.executable.locations import DMACRYS_EXEC

from cspy.deprecated.cspy_exceptions import CSPyException, CSPyDebugException
import logging
import os
import sys
import time

dmacrys_logger = logging.getLogger("CSPy.dmacrys_utils")

_DMACRYS_TIMEOUT_DEFAULT = 3600.0


# Add new dmacrys arguments to this thing
def dmacrys_add_arguments(parser):
    parser.add_argument(
        "-ac",
        "--assisted_convergence",
        action="store_true",
        help="Use assisted convergence for DMACRYS",
        default=False,
    )
    parser.add_argument(
        "-cl",
        "--clean_level",
        type=int,
        help="Cleanup level for post DMACRYS. 0 = Keep all,"
        " 1: delete uneeded files (.nnl etc), "
        " 2: same as 1 plus dmain and dmaout,"
        " 3: delete all but fort.12 and fort.22 (valids),"
        " 4: clean up files before dmacrys, delete all"
        " but fort.12 and fort.22 (all).",
        default=1,
    )
    parser.add_argument(
        "-zl",
        "--zip_level",
        type=int,
        help="Zip level for post DMACRYS. "
        "0 = off, 1=folder, 2=folder+res+log,"
        " 3=all in structure list",
        default=1,
    )
    parser.add_argument(
        "-zu",
        "--zip_unpack",
        type=int,
        help="If 1 then try to unpack a zip " "file with the structures name.",
        default=1,
    )
    parser.add_argument(
        "-conv",
        "--constant_volume",
        action="store_true",
        help="constant volume optimisation",
        default=False,
    )
    parser.add_argument(
        "-dt",
        "--dmacrys_timeout",
        type=float,
        help="timeout (s) = factor * total walltime " " / total no. structures",
        default=_DMACRYS_TIMEOUT_DEFAULT,
    )
    parser.add_argument(
        "--remove_negative_eigens",
        action="store_true",
        help="Remove negative eigenvalues from dmaout",
        default=False,
    )
    parser.add_argument(
        "--cutm",
        type=float,
        default=20.0,
        help="set the CUTM value in the DMACRYS input file",
    )
    parser.add_argument(
        "--nbur",
        type=int,
        default=20,
        help="set the NBUR value in the DMACRYS input file",
    )
    parser.add_argument(
        "--auto_neighbour_setting",
        action="store_true",
        help="automatically calculate the NBUR and CUTM "
        "values for DMACRYS input file",
        default=False,
    )
    parser.add_argument(
        "--check_z_value",
        action="store_true",
        default=False,
        help="Check the Z value in ZERR line before " "writing .res file",
    )
    parser.add_argument(
        "--new_ac",
        action="store_true",
        default=False,
        help="newer version of assisted convergence",
    )
    parser.add_argument(
        "-puc",
        "--platon_unit_cell",
        action="store_true",
        help="Platon the cell before energy minimising",
        default=False,
    )
    parser.add_argument(
        "-lj",
        "--lennard_jones",
        action="store_true",
        help="Fit the Buckingham potential to an isotropic"
        "Lennard-Jones form (outputs LJFit.png to "
        "visualise the fit)",
        default=False,
    )
    parser.add_argument(
        "-ljp",
        "--lennard_jones_potential",
        help="File containing the LJ fit of the " "Buckingham potential",
        default=None,
    )
    parser.add_argument(
        "--seig1",
        action="store_true",
        default=False,
        help="Use SEIG 1 to search a negative " "eigenvalue for a lower energy minimum",
    )
    parser.add_argument(
        "--exact_prop",
        action="store_true",
        default=False,
        help="Perform an exact property calculation "
        "(replaces PLUT with PROP and removes "
        "NOPR from .dmain file).",
    )
    parser.add_argument(
        "--raise_before_dmacrys_call",
        action="store_true",
        help="Raise CSPyDebugException before DMACRYS call",
        default=False,
    )
    parser.add_argument(
        "--setup_off",
        action="store_true",
        help="Turn dmacrys setup off (override default)",
        default=False,
    )
    parser.add_argument(
        "--setup_on",
        action="store_true",
        help="Turn dmacrys setup on (override default)",
        default=False,
    )
    parser.add_argument(
        "--spline_off",
        action="store_true",
        help="Turn spline off (override default)",
        default=False,
    )
    parser.add_argument(
        "--spline_on",
        nargs="?",
        const=None,
        help="Turn spline on (if present, will "
        "leave the default else will use "
        "'SPLI 2.0 4.0'. Alternatively, "
        "if a string is given that will "
        "be used e.g. --spline_on 'SPLI 1.0 2.0')",
        default=False,
    )
    parser.add_argument(
        "--set_real_space",
        action="store_true",
        help="Computes the equivalent realspace "
        "cutoff to match repulsion-dispersion",
        default=False,
    )
    parser.add_argument(
        "--profiling",
        action="store_true",
        default=False,
        help="Record profiling information (gmon.out)" " for each dmacrys call",
    )


def dmacrys_parse_args(args=None):
    import argparse

    dmacrys_parser = argparse.ArgumentParser(
        description="Arguments to control the DMACRYS execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    dmacrys_add_arguments(dmacrys_parser)
    dmacrys_args, unknown = dmacrys_parser.parse_known_args(args=args)
    if dmacrys_args.profiling:
        from cspy.executable.locations import use_profiling_executables, DMACRYS_EXEC

        use_profiling_executables()
        dmacrys_logger.warn("Using profiling DMACRYS executable: %s", DMACRYS_EXEC)
        from cspy.executable.locations import DMACRYS_EXEC

        assert DMACRYS_EXEC.endswith("prof")
    return dmacrys_args


def add_dmacrys_argument_group(parser):
    dmacrys_parser = parser.add_argument_group(
        "DMACRYS Arguments", "Arguments to control the DMACRYS execution"
    )
    dmacrys_add_arguments(dmacrys_parser)


# All of these get_dmacrys_property things could be changed into the CSPData
# class-- this could be part of restructuring later e.g. could just store
# fort.12 as string, and then get anything wanted when needed?


def get_dmacrys_minimisation():
    """Reads whether dmacrys reached a valid or
       invalid minimisation- perhaps superfluous??"""
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        msg = "DMACRYS summary file could not be " "opened for reading: fort.12"
        dmacrys_logger.error(msg)
        raise CSPyException(msg)
    else:
        for line in fort12_file:
            if line.startswith(" Valid minimisation"):
                return "Valid"
            elif line.startswith(" Invalid mini"):
                return "Invalid"
    return None


def get_dmacrys_negative_rep(name):
    """gets negative_reps at source"""
    save_reps = False
    eigns = {}
    negative_rep = None
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        msg = "DMACRYS summary file could not be " "opened for reading: fort.12"
        dmacrys_logger.error(msg)
        raise CSPyException(msg)
    else:
        for line in fort12_file:
            if " Warning - Non-zero eigenvalues found" in line:
                dmaout_filename = name + ".res.dmaout"
                if not os.path.exists(dmaout_filename):
                    dmaout_filename = name + ".dmaout"
                with open(dmaout_filename) as dmaout_file:
                    for line in dmaout_file:
                        if "EIGENVALUES FOR REPRESENTATION" in line:
                            save_reps = True
                            rep_num = int(line.strip().split()[-1])
                            eigns[rep_num] = []
                            continue
                        if save_reps and line.strip() == "":
                            break
                        if save_reps:
                            eigns[rep_num] += map(float, line.strip().split())
                        for i in range(1, len(eigns) + 1):
                            if min(eigns[i]) < -1e-6:
                                # this rep needs to be removed
                                negative_rep = i
    return negative_rep


def get_dmacrys_f():
    """Reads whether dmacrys reached a valid
    or invalid minimisation- perhaps superfluous??"""
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        msg = "DMACRYS summary file could not be " "opened for reading: fort.12"
        dmacrys_logger.error(msg)
        raise CSPyException(msg)
    else:
        for line in fort12_file:
            if line.startswith(" F ="):
                try:
                    return float(line.split()[-1])
                except Exception as exc:
                    return None
    return None


def get_dmacrys_initial_energy():
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        msg = "DMACRYS summary file could not be " "opened for reading: fort.12"
        dmacrys_logger.error(msg)
        raise CSPyException(msg)
    else:
        for line in fort12_file:
            if line.startswith("Initial Lattice Energy"):
                try:
                    energy = float(line.strip().split()[3])
                except ValueError as exc:
                    dmacrys_logger.info(
                        "Problem with energy value in line: %s", line.strip()
                    )
                    dmacrys_logger.info(os.getcwd())
                    raise CSPyException(
                        "Problem with energy value in line: " + line.strip()
                    )
                fort12_file.close()
                return energy
        # if get here then not found energy
        dmacrys_logger.error("DMACRYS inital energy not found")
        return None


def get_dmacrys_energy():
    #    print "get_dmacrys_energy()"
    if not os.path.exists("fort.12"):
        dmacrys_logger.error(
            "dmacrys summary file could not be opened for reading: fort.12"
        )
        return None

    with open("fort.12") as f:
        lines = f.readlines()

    valid = False
    for line in lines:
        if "valid" in line.lower():
            valid = True
        if "final lattice energy" in line.lower():
            try:
                energy = float(line.strip().split()[3])
                dmacrys_logger.info("DMACRYS energy: %6.1f", energy)
            except ValueError as exc:
                dmacrys_logger.info(
                    "Problem with energy value in line: %s", line.strip()
                )
                dmacrys_logger.info(os.getcwd())
                raise CSPyException(
                    "Problem with energy value in line: " + line.strip()
                )
            if valid:
                return energy
    # if get here then not found energy
    # if not valid:#TEST
    #    dmacrys_logger.error("DMACRYS energy minimisation not valid")#TEST
    return None


def get_dmacrys_final_density():
    #    one day should just have one thing which returns dictionary of all data in the fort.12
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        dmacrys_logger.error(
            "dmacrys summary file could not be opened for reading: fort.12"
        )
        raise CSPyException(
            "DMACRYS summary file could not be opened for reading: fort.12"
        )
    else:
        valid = False
        for line in fort12_file:
            if line.startswith(" Valid"):
                valid = True
            if "Final   =>" in line and valid:
                try:
                    den = float(line.strip().split()[3])
                except ValueError as exc:
                    dmacrys_logger.info(
                        "Problem with energy value in line:", line.strip()
                    )
                    dmacrys_logger.info(os.getcwd())
                    raise CSPyException(
                        "Problem with energy value in line: " + line.strip()
                    )
                fort12_file.close()
                return den
        # if not valid:#TEST
        #    dmacrys_logger.error("DMACRYS energy minimisation not valid")#TEST
        return None


def get_dmacrys_initial_density():
    #    one day should just have one thing which returns dictionary of all data in the fort.12
    try:
        fort12_file = open("fort.12", "r")
    except IOError as exc:
        dmacrys_logger.error(
            "dmacrys summary file could not be opened for reading: fort.12"
        )
        raise CSPyException(
            "DMACRYS summary file could not be opened for reading: fort.12"
        )
    else:
        valid = False
        density = False
        for line in fort12_file:
            if line.startswith(" Valid"):
                valid = True
            if "density" in line:
                density = True
            if "   Initial =>" in line and valid and density == True:
                try:
                    den = float(line.strip().split()[3])
                except ValueError as exc:
                    dmacrys_logger.info(
                        "Problem with energy value in line:", line.strip()
                    )
                    dmacrys_logger.info(os.getcwd())
                    raise CSPyException(
                        "Problem with energy value in line: " + line.strip()
                    )
                fort12_file.close()
                return den
        if not valid:
            dmacrys_logger.error("DMACRYS energy minimisation not valid")
        return None


def buck_to_lj(A, B, C):
    import numpy
    from scipy.optimize import curve_fit

    def ljfit(a, b, c):
        test_points = 2001
        fit_points = 1001
        lower = 0.01
        upper = 6.01
        fitted = False
        # change the gradient based on whether we are fitting a purely repulsive potential
        if c == 0.0:
            gradient = -0.5
            fit_end = 6.0
            step = 1
            mult = 0.05
        else:
            step = 1
            mult = 0.0005
            min_r = numpy.linspace(6.0, 2.0, fit_points)
            buck_eq_min_pot, buck_eq_min_r = list(
                [sorted(zip(buckfunc(min_r, a, b, c), min_r))][0][0]
            )
        test_r = numpy.linspace(upper, lower, test_points)
        potential = buckfunc(test_r, a, b, c)
        attempts = 0
        tot_attempts = 100
        fitted = False
        co_a = 10.0
        co_b = 10.0
        while fitted == False and attempts < tot_attempts:
            if c != 0.0:
                fit_start = buck_eq_min_r - float(step) * mult
                fit_end = buck_eq_min_r + float(step) * mult
                if fit_start < 1.0:
                    fit_start = 1.0
                if fit_end > 6.0:
                    fit_end = 6.0
            elif c == 0.0:
                # count=0
                # while ((potential[count+1]-potential[count])/(test_r[count+1]-test_r[count]) > gradient) and (count < len(potential)-2): # while the gradient is greater than the target keep going
                #     count+=1
                # fit_start=upper-((upper-lower)/test_points)*float(count)
                fit_start = 2.25 + float(step) * mult
                fit_end = 6.0
            r = numpy.linspace(fit_start, fit_end, fit_points)
            buckpot = buckfunc(r, a, b, c)
            try:
                fitParams, fitCovariances = curve_fit(
                    ljfunc, r, buckpot, p0=(co_a, co_b)
                )
                lja, ljb = fitParams
            except Exception as exc:
                lja, ljb = -100.0, -100.0
            # the potential is allowed to go attractive at distances shorter than 0.85 Angstrom
            # we can use rcut (in towhee) to stop this being an issue (it will ignore short interactions)
            if ljfunc(0.01, lja, ljb) <= 0.0:

                dmacrys_logger.warning(
                    "Problem with the Lennard-Jones potential fit. Potential="
                    + str(ljfunc(0.01, lja, ljb))
                    + " @ r= 0.01"
                )
                dmacrys_logger.warning("Reducing the gradient and retrying")
                if c != 0.0:
                    step += 2  # widen the fit width used
                elif c == 0.0:
                    step += 2
                    gradient = gradient * 0.75  # change the maximum of the
                attempts += 1
            else:
                # check we don't have other problems
                if c == 0:  # the energy must always increase
                    buck_eq_r = 3.0
                else:
                    check_r = numpy.linspace(2.0, 6.0, 1000)
                    buck_eq_pot, buck_eq_r = list(
                        [sorted(zip(buckfunc(check_r, a, b, c), check_r))][0][0]
                    )

                check_r = numpy.linspace(buck_eq_r - 0.5, 0.01, 1000)
                # print check_r
                error = False
                for i in range(len(check_r) - 1):
                    if error == False:
                        if ljfunc(check_r[i], lja, ljb) > ljfunc(
                            check_r[i + 1], lja, ljb
                        ):
                            dmacrys_logger.info("Error")
                            error = True
                            break
                        else:
                            error = False
                if error == True:
                    fitted = False
                    attempts = 0
                    dmacrys_logger.info(lja, ljb, A, B, C)
                    mult = mult * 1.01
                    co_a = 15.0
                    co_b = 5.0
                else:
                    fitted = True

            if attempts >= tot_attempts:
                raise CSPyException(
                    "Problem with the Lennard-Jones fit. Potential remains attractive after 10 attempts."
                )

        if c != 0.0:
            buck_r = numpy.linspace(7.0, 2.0, 501)
            buck_eq_pot, buck_eq_r = list(
                [sorted(zip(buckfunc(buck_r, a, b, c), buck_r))][0][0]
            )
            lj_eq_pot, lj_eq_r = list(
                [sorted(zip(ljfunc(test_r, lja, ljb), test_r))][0][0]
            )
            dmacrys_logger.info(
                "Equilibrium distance (Buck, LJ, diff,%): "
                + str(buck_eq_r)
                + " "
                + str(lj_eq_r)
                + " "
                + str(lj_eq_r - buck_eq_r)
                + " "
                + str(100.0 * ((lj_eq_r - buck_eq_r) / buck_eq_r))
            )
            dmacrys_logger.info(
                "Equilibrium energy (Buck, LJ, diff, %): "
                + str(buck_eq_pot)
                + " "
                + str(lj_eq_pot)
                + " "
                + str(lj_eq_pot - buck_eq_pot)
                + " "
                + str(100.0 * ((lj_eq_pot - buck_eq_pot) / buck_eq_pot))
            )
        else:
            dmacrys_logger.info(
                "Repulsive potential- no measure of the fit implemented"
            )
        return lja, ljb

    lja, ljb = ljfit(A, B, C)
    # A/r^12-B/r^6
    # epsilon=ljb^2/(4*lja)
    # sigma=numpy.power(lja/ljb,(1.0/6.0))
    # epsilon=(numpy.power(ljb,2.0))/(4.0*lja)
    # sigma=numpy.power((lja/ljb),(1.0/6.0))
    # epsilon*[(rm/r)^12-2*(rm/r)^6]
    # rm=2^(1/6)*sigma => sigma=rm/(2^1/6)
    # epsilon=lja
    # sigma=ljb/numpy.power(2.0,(1.0/6.0))
    # print "e/s",lja,ljb,epsilon,sigma
    return lja, ljb


def buckingham_to_lj(dmain_file):
    try:
        dmain_in = open(dmain_file, "r")
    except IOError as exc:
        dmacrys_logger.error("dmacrys dmain file could not be opened for reading")
        raise CSPyException("DMACRYS dmain file could not be opened for reading")
    else:
        from scipy.optimize import curve_fit
        import numpy as np
        import shutil
        import matplotlib

        matplotlib.use("Agg")
        import matplotlib.pyplot as plt

        try:
            dmain_file2 = str(dmain_file + "2")
            dmain_out = open(dmain_file2, "w+")
        except IOError as exc:
            dmacrys_logger.error(
                "Lennard-Jones dmain file could not be opened for writing: "
                + str(dmain_file + "2")
            )
            raise CSPyException(
                "Neighcrys input file could not be opened for writing: "
                + str(dmain_file + "2")
            )
        else:
            cmap = plt.cm.get_cmap("Set1", 60)  # increase this if errors
            colors = cmap(np.arange(cmap.N))
            colorc = 0
            minp = 0.0
            begin = False
            end = False
            tmp_stop = False
            dmacrys_logger.info(
                "Lennard-Jones isotropic potential fitting, Buckingham potential file saved as "
                + str(dmain_file)
                + ".buck"
            )
            for line in dmain_in:
                if "BUCK" in line:
                    begin = True
                    end = False
                if "MOLE" in line:
                    end = True
                if begin == True and end == False:
                    if "BUCK" in line:
                        params = line.split()
                        dmain_out.write(
                            "LENN  "
                            + str("{:<8s}".format(params[1]))
                            + " "
                            + str("{:<8s}".format(params[2]))
                            + " 12 6\n"
                        )
                        dmacrys_logger.debug(
                            "Potential:"
                            + str("{:<8s}".format(params[1]))
                            + " "
                            + str("{:<8s}".format(params[2]))
                        )
                    if "ANIS" in line:
                        tmp_stop = True
                    if (
                        ("BUCK" not in line)
                        and ("ENDS" not in line)
                        and (tmp_stop != True)
                    ):
                        params = map(float, line.split())
                        a, b = buck_to_lj(params[0], params[1], params[2])
                        r = np.linspace(1.0, 6.0, 20)
                        plt.errorbar(
                            r,
                            buckfunc(r, params[0], params[1], params[2]),
                            fmt="ro",
                            yerr=0.0,
                            color=colors[colorc],
                        )
                        r = np.linspace(0.01, 6, 100)
                        ljpot = []
                        ljpot = ljfunc(r, a, b)
                        if min(ljpot) < minp:
                            minp = min(ljpot)
                        plt.plot(r, ljfunc(r, a, b), color=colors[colorc])
                        colorc += 1
                        dmain_out.write(
                            str("{:< 15.6F}".format(a))
                            + " "
                            + str("{:< 15.6F}".format(b))
                            + " "
                            + str("{:< 8.2F}".format(params[3]))
                            + " "
                            + str("{:< 8.2F}".format(params[4]))
                            + "\n"
                        )
                    if "ENDS" in line:
                        dmain_out.write(line)
                        tmp_stop = False
                else:
                    dmain_out.write(line)
        plt.ylabel("Energy", fontsize=16.0)
        plt.xlabel("R (Angstrom)", fontsize=16.0)
        plt.xlim(0.0, 6.0)  # potential won't go negative
        plt.ylim(minp - 0.001, 0.01)
        plt.savefig(
            "LJFit.png", bbox_inches=0, dpi=50
        )  # Fit of the Williams potential to be used
        dmain_in.close()
        dmain_out.close()
        buck_file = str(dmain_file + ".buck")
        shutil.move(dmain_file, buck_file)
        shutil.move(dmain_file2, dmain_file)
    return None


def run_dmacrys(
    dmacrys_exec=DMACRYS_EXEC,
    dmain="test.dmain",
    dmaout="test.dmaout",
    assisted_convergence=False,
    timeout=_DMACRYS_TIMEOUT_DEFAULT,
    dmacrys_args=None,
):

    from cspy.util import PopenTimeout
    from cspy.deprecated.file_utils import word_replace_in_file

    if dmacrys_args is None:
        dmacrys_args = vars(dmacrys_parse_args([]))

    # Run dmacrys using input file dmain and output results to dmaout
    input_file = open(dmain, "r")
    output_file = open(dmaout, "w")

    if dmacrys_args["raise_before_dmacrys_call"]:
        raise CSPyDebugException("Debug DMACRYS input files")
    s = time.time()
    status = PopenTimeout([dmacrys_exec], input_file, output_file).run(timeout)
    if status > 0:
        sys.stderr.write("Above error may be due to " + dmain)
        dmacrys_logger.warning(
            "DMACRYS quit due to an error after " + str(time.time() - s) + " s"
        )

    input_file.close()
    output_file.close()
    # Check if dmacrys summary file has been produced
    if not os.path.exists("fort.12"):
        with open(dmaout) as f:
            output_contents = f.readlines()[-5:]
        dmacrys_logger.debug(
            "DMACRYS summary file could not be opened for reading: fort.12\n"
            "last 5 lines of DMACRYS output:\n%s",
            "\n".join(output_contents),
        )
        raise CSPyException(
            "DMACRYS summary file 'fort.12' could not be opened for reading."
        )

    # For readability...
    if not assisted_convergence:
        return get_dmacrys_energy()

    dmacrys_logger.info("DMACRYS assisted convergence")

    # Assisted Convergence
    Valid = False
    ngcv = 0
    cycles_error = False
    while not Valid:
        try:
            with open("fort.12") as fort12:
                for line in fort12:
                    # Separate ifs out so we can detect other
                    # problems in future e.g. negative eigenvalues
                    sline = line.strip()
                    if sline.startswith("Valid minimisation"):
                        dmacrys_logger.info("Minimisation Complete")
                        Valid = True
                        return get_dmacrys_energy()
                    if sline.startswith("Invalid minimisation"):
                        dmacrys_logger.info(
                            "Invalid minimisation encountered.  Checking for cause"
                        )
                        break
                dmacrys_logger.info(line.strip())
        except IOError as exc:
            dmacrys_logger.error(
                "dmacrys summary file could not be opened for reading in ac: fort.12"
            )
            raise CSPyException(
                "DMACRYS summary file could not be opened for reading in ac: fort.12"
            )

        if sline == "Invalid minimisation - negative curvature":
            ngcv = ngcv + 1
            dmacrys_logger.info("Persistent negative curvature detected")
            if ngcv == 1:
                dmacrys_logger.info("Checking for WCALNGCV settings")
                dmain_f = open(dmain, "r")
                wcal_ngcv = False
                for line in dmain_f:
                    if "WCAL NGCV" in line:
                        wcal_ngcv = True
                dmain_f.close()
                if not wcal_ngcv:
                    dmacrys_logger.info("WCAL NGCV not detected. Inserting")
                    word_replace_in_file(dmain, "NOPR", "NOPR\nWCAL NGCV")
                else:
                    ngcv = ngcv + 1
                    dmacrys_logger.info("WCAL NGCV found, looking at step size")
            if dmacrys_args["new_ac"]:
                if ngcv >= 2 and ngcv < 5:
                    maxd = 0.5 * (2 ** (ngcv - 1))
                    dmacrys_logger.info("Increasing MAXD to " + str(maxd))
                    word_replace_in_file(dmain, "MAXD", "MAXD " + str(maxd))
            else:
                if ngcv >= 2 and ngcv < 10:
                    maxd = float(ngcv) * 0.5
                    dmacrys_logger.info("Increasing MAXD to " + str(maxd))
                    word_replace_in_file(dmain, "MAXD", "MAXD " + str(maxd))
            if ngcv == 10:
                dmacrys_logger.error(
                    "Structure not minimising using this method, check your input structure for errors"
                )
                raise CSPyException(
                    "Structure not minimising using this method, check your input structure for errors"
                )

        if sline == "Invalid minimisation - too many iteration cycles":
            if not cycles_error:
                cycles_error = True
                dmacrys_logger.info(
                    "Calculation hit cycle limit. Increasing cycle limit to 5000"
                )
                word_replace_in_file(dmain, "MAXI", "MAXI 5000", maxfound=1)
            else:
                dmacrys_logger.error(
                    "Iteration cycle limit does not converge with MAXI 5000. Please check your structure before resubmitting"
                )
                raise CSPyException(
                    "Iteration cycle limit does not converge with MAXI 5000. Please check your structure before resubmitting"
                )

        # Assisted convergence DMACRYS run
        dmacrys_logger.info("Assisted DMACRYS run")
        input_file = open(dmain, "r")
        output_file = open(dmaout, "w+")
        # remove fort.16 and fort.12 incase we timeout
        os.remove("fort.16")
        os.remove("fort.12")
        s = time.time()
        status = PopenTimeout([dmacrys_exec], input_file, output_file).run(timeout)
        if status > 0:
            sys.stderr.write("Above error may be due to " + dmain)
            dmacrys_logger.warning(
                "DMACRYS quit due to an error after " + str(time.time() - s) + " s"
            )
        #        dmacrys_process = Popen([dmacrys_exec],stdin=input_file,stdout=output_file)
        #        dmacrys_process.wait()
        input_file.close()
        output_file.close()

    return get_dmacrys_energy()


def buckfunc(r, a, b, c):
    import numpy

    return (a * numpy.exp(-r / b)) - (c / (numpy.power(r, 6.0)))


def ljfunc(r, A, B):
    import numpy

    return (A / numpy.power(r, 12.0)) - (B / numpy.power(r, 6.0))  # dmacrys lj form


def read_lj(dmain_file, potential_file=None):
    try:
        dmain_in = open(dmain_file, "r")
    except IOError as exc:
        dmacrys_logger.error("dmacrys dmain file could not be opened for reading")
        raise CSPyException("DMACRYS dmain file could not be opened for reading")
    try:
        pot = open(potential_file, "r")
    except IOError as exc:
        dmacrys_logger.error("potential file could not be opened for reading")
        raise CSPyException("read_lj potential file could not be opened for reading")
    else:
        import shutil

        try:
            dmain_file2 = str(dmain_file + "2")
            dmain_out = open(dmain_file2, "w+")
        except IOError as exc:
            dmacrys_logger.error(
                "Lennard-Jones dmain file could not be opened for writing: "
                + str(dmain_file + "2")
            )
            raise CSPyException(
                "Neighcrys input file could not be opened for writing: "
                + str(dmain_file + "2")
            )
        else:
            # read the lennard-jones terms into a dictionary
            # dictionary form Types, sub dict {powa,powb A, B, rmin, rmax}
            LJ = {}
            tmp_stop = False
            for line in pot:
                if "LENN" in line:
                    p = line.split()
                    label1 = p[1]
                    label2 = p[2]
                    LJ[label1, label2] = {"powerA": p[3], "powerB": p[4]}
                    LJ[label2, label1] = {"powerA": p[3], "powerB": p[4]}
                if "ANIS" in line:
                    tmp_stop = True
                if ("LENN" not in line) and ("ENDS" not in line) and (tmp_stop != True):
                    p = map(float, line.split())
                    powerA = LJ[label1, label2]["powerA"]
                    powerB = LJ[label1, label2]["powerB"]
                    LJ[label1, label2] = {
                        "powerA": powerA,
                        "powerB": powerB,
                        "A": p[0],
                        "B": p[1],
                        "rmin": p[2],
                        "rmax": p[3],
                    }
                    LJ[label2, label1] = {
                        "powerA": powerA,
                        "powerB": powerB,
                        "A": p[0],
                        "B": p[1],
                        "rmin": p[2],
                        "rmax": p[3],
                    }
                if "ENDS" in line:
                    del label1
                    del label2
                    tmp_stop = False
            pot.close()
            dmacrys_logger.info(LJ)
            begin = False
            end = False
            tmp_stop = False
            dmacrys_logger.info(
                "Lennard-Jones isotropic potential, Buckingham potential file saved as "
                + str(dmain_file)
                + ".buck"
            )
            for line in dmain_in:
                if "BUCK" in line:
                    begin = True
                    end = False
                if "MOLE" in line:
                    end = True
                if begin == True and end == False:
                    if "BUCK" in line:
                        p = line.split()
                        label1 = p[1]
                        label2 = p[2]
                        try:
                            powerA = LJ[label1, label2]["powerA"]
                            powerB = LJ[label1, label2]["powerB"]
                        except Exception as exc:
                            dmacrys_logger.info(label1, label2, LJ[label1, label2])
                            dmacrys_logger.error(label1, label2)
                            dmacrys_logger.error(
                                "Lennard-Jones parameters not in potential file: "
                                + str(potential_file)
                            )
                            raise CSPyException(
                                "Error in read_lj, check "
                                + str(potential_file)
                                + "contains all terms in "
                                + str(dmain_file)
                            )
                        dmain_out.write(
                            "LENN  "
                            + str("{:<8s}".format(label1))
                            + " "
                            + str("{:<8s}".format(label2))
                            + " "
                            + powerA
                            + " "
                            + powerB
                            + "\n"
                        )
                    if "ANIS" in line:
                        tmp_stop = True
                    if (
                        ("BUCK" not in line)
                        and ("ENDS" not in line)
                        and (tmp_stop != True)
                    ):
                        A = LJ[label1, label2]["A"]
                        B = LJ[label1, label2]["B"]
                        rmin = LJ[label1, label2]["rmin"]
                        rmax = LJ[label1, label2]["rmax"]
                        dmain_out.write(
                            str("{:< 15.6F}".format(A))
                            + " "
                            + str("{:< 15.6F}".format(B))
                            + " "
                            + str("{:< 8.2F}".format(rmin))
                            + " "
                            + str("{:< 8.2F}".format(rmax))
                            + "\n"
                        )
                    if "ENDS" in line:
                        dmain_out.write(line)
                        tmp_stop = False
                else:
                    dmain_out.write(line)
        dmain_in.close()
        dmain_out.close()
        buck_file = str(dmain_file + ".buck")
        shutil.move(dmain_file, buck_file)
        shutil.move(dmain_file2, dmain_file)
    return None
