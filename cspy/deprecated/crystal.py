from __future__ import print_function
import zipfile
import copy
import os
import logging
import math
import numpy as np
import itertools as it
from cspy.deprecated.cspy_exceptions import CSPyException
from collections import namedtuple
from cspy.deprecated.listmath import (
    list3normalize,
    list3multiply,
    list3cross,
    list3add,
    list3dot,
    list3vecmatrix,
    list3subtract,
    list3mag,
    rotation_by_q,
    list3zeros,
    list3_proj_pts_axis,
    list3delparallelvecs,
    list3matrixmatrix,
    list3perp_face_vecs,
    list3apply_sym_list,
    list3apply_sym_list_norm,
    list3apply_sym,
    list3make_all_translations,
    list3cross_combinations,
    list3cross_comb_tri,
    list3angle,
    list3required_dvec,
    list3find_overlap,
)

from cspy.deprecated.molecule import Atom
from cspy.deprecated.molecule import Molecule
from cspy.deprecated.voids import Voids
from cspy.deprecated.castep import make_castep_cell
from cspy.deprecated.molecule import Atom
from cspy.deprecated.molecule import Molecule
from cspy.deprecated.molecule import cluster_dimers
from cspy.deprecated.voids import Voids
from cspy.deprecated.castep import make_castep_cell
from cspy.util.path import Path
from cspy.formats import PminSave

crystal_logger = logging.getLogger("CSPy.crystal")


def same_value(v1, v2):
    return (v1 + 0.001 > v2) and (v1 - 0.001 < v2)


class Lattice:
    """
    This class describes a lattice.

    At its core the lattice is described by the lattice vectors
    (self.lattice_vectors) or the lattice parameters (self.lattice_parameters).

    Further features include characterisation techniques
    (determining fixed angles for given spacegroups etc).
    These are used in the structure generation algorithm,
    so look there when designing tests.

    """

    neighbour_cells = [
        (-1, -1, -1),
        (-1, -1, 0),
        (-1, -1, 1),
        (-1, 0, -1),
        (-1, 0, 0),
        (-1, 0, 1),
        (-1, 1, -1),
        (-1, 1, 0),
        (-1, 1, 1),
        (0, -1, -1),
        (0, -1, 0),
        (0, -1, 1),
        (0, 0, -1),
        (0, 0, 1),
        (0, 1, -1),
        (0, 1, 0),
        (0, 1, 1),
        (1, -1, -1),
        (1, -1, 0),
        (1, -1, 1),
        (1, 0, -1),
        (1, 0, 0),
        (1, 0, 1),
        (1, 1, -1),
        (1, 1, 0),
        (1, 1, 1),
    ]
    _RHOMBOHEDRAL = "Rhombohedral"
    _HEXAGONAL = "Hexagonal"
    _TRIGONAL_SETTING = _RHOMBOHEDRAL

    def __init__(self):
        self.lattice_type = 0
        self.lattice_parameters = np.zeros(6, dtype=np.float64)
        self._neighbour_cell_trans_list = []
        self.restricted_angles = [False] * 3
        self.unique_axis = False

    @classmethod
    def from_parameters(cls, parameters):
        l = cls()
        l.set_lattice_parameters(parameters)
        return l

    def angle_component_of_volume(self):
        """ Returns angle component of volume
        >>> l = Lattice()
        >>> l.set_lattice_parameters([10, 10, 10, 90, 90, 90])
        >>> l.angle_component_of_volume()
        1.0
        """
        alpha, beta, gamma = np.radians(self.lattice_parameters[3:])
        VStar = np.sqrt(
            1.0
            + 2.0 * np.cos(alpha) * np.cos(beta) * np.cos(gamma)
            - np.cos(alpha) ** 2
            - np.cos(beta) ** 2
            - np.cos(gamma) ** 2
        )
        return VStar

    def length_component_of_volume(self):
        """
        >>> l = Lattice()
        >>> l.set_lattice_parameters([10, 10, 10, 90.0, 90.0, 90.0])
        >>> l.length_component_of_volume() == 1000
        True
        """
        return (
            self.lattice_parameters[0]
            * self.lattice_parameters[1]
            * self.lattice_parameters[2]
        )

    def lattice_parameters_volume(self):
        """
        >>> l = Lattice()
        >>> l.set_lattice_parameters([5, 10, 10, 90.0, 90.0, 90.0])
        >>> l.lattice_parameters_volume() == 500.0
        True
        """

        """ Returns angle component of volume """
        return self.angle_component_of_volume() * self.length_component_of_volume()

    def neighbour_cell_trans(self):
        if len(self._neighbour_cell_trans_list) == 0:
            for trans in it.product(range(-1, 2), repeat=3):
                self._neighbour_cell_trans_list.append(
                    self.lattice.to_cartesian([float(x) for x in trans])
                )
        return self._neighbour_cell_trans_list

    def aniso_expand_lattice(self, factor=[1.0, 1.0, 1.0]):
        """
        >>> l = Lattice()
        >>> l.set_lattice_parameters([5, 5, 5, 90, 90, 90])
        >>> l.aniso_expand_lattice(factor=[2., 2., 1.])
        >>> np.allclose(l.lattice_parameters[:3], [10., 10.,  5.])
        True
        >>> np.allclose(l.direct_matrix(), np.array([[10.,  0.,  0.], [ 0., 10.,  0.], [ 0.,  0.,  5.]]))
        True
        >>> np.allclose(l.inverse_matrix(), np.array([[0.1, 0. , 0. ], [0. , 0.1, 0. ], [0. , 0. , 0.2]]))
        True
        """
        for i in range(3):
            self.lattice_vectors[i] = [factor[i] * x for x in self.lattice_vectors[i]]
        self.lattice_parameters[:3] = np.asarray(factor) * self.lattice_parameters[:3]
        self.calc_inv_lattice_vectors()

    def expand_lattice(self, factor):
        self.aniso_expand_lattice([factor] * 3)

    def lattice_type_dictionary(self, label):
        lattice_type_dictionary = {
            "Triclinic": 1,
            "Monoclinic": 2,
            "Orthorhombic": 3,
            "Tetragonal": 4,
            "Trigonal": 5,
            "Hexagonal": 6,
            "Cubic": 7,
        }
        return lattice_type_dictionary[label]

    def direct_matrix(self):
        """
        The direct matrix of the unit cell.
        Each row corresponds to a cell vector
        """
        return np.asarray(self.lattice_vectors)

    def reciprocal_matrix(self):
        return self.inverse_matrix().T

    def inverse_matrix(self):
        return np.asarray(self.inv_lattice_vectors)

    def to_cartesian(self, coords):
        return np.dot(coords, self.direct_matrix())

    def to_fractional(self, coords):
        return np.dot(coords, self.inverse_matrix())

    def determine_fixed_lengths(self, crystal, random_vector):
        """
        Method to determine the dimension of the random vector that take into
        account the search space of unit cell length, which are variables
        depending on the type of lattice system.

        Parameters:
        -----------
        crystal: A crystal structure object.
        random_vector: A random vector object.
        """
        # TODO: this needs to tidy up, why pass in a crystal object into a
        # method belong to a crystal class?
        if self.lattice_type == 0:
            raise CSPyException("Lattice type is zero, exiting.")
        if self.lattice_type in [1, 2, 3]:
            # Cases of TRICLINIC, MONOCLINIC and ORTHOROHOMBIC cells where all
            # three unit cell lengths could be varied independently. The cell
            # length search space in this case is three dimensional.
            random_vector.set_unit_cell_lengths([0.0] * 3)
            return
        if self.lattice_type == 7:
            # Case of CUBIC lattice, all three cell lengths are equal to each
            # other, the cell length search space is one dimensional.
            random_vector.set_unit_cell_lengths([0.0])
            return
        if self.lattice_type in [4, 6]:
            # cases for TETRAGONAL and HEXAGONAL lattices. Only two cell
            # lengths could be varied independently, so the search space
            # dimension for unit cell length is two.
            random_vector.set_unit_cell_lengths([0.0] * 2)
            self._determine_unique_axis_for_four_and_six(crystal, random_vector)
            return
        if self.lattice_type == 5:
            # case for TRIGONAL or RHOMBOHEDRAL lattice. All three cell lengths
            # were equivalent, search space dimension for unit cell length is
            # one.
            random_vector.set_unit_cell_lengths([0.0])
            return

        crystal_logger.info("Crashing in determine fixed lengths.")
        exit()

    def determine_fixed_angles(self, crystal, random_vector, predetermined_angles=None):
        """
        Method to determine the dimension of the random vector that take into
        account the search space of unit cell angles, which are variables
        depending on the type of lattice system.

        Parameters:
        -----------
        crystal: A crystal structure object.
        random_vector: A random vector object.
        predetermined_angles: If you have already fixed them, add this as
        90.,90.,90. for e.g.
        """

        if predetermined_angles:
            restricted_angles = []
            random_angles = []
            for ip, p in enumerate(predetermined_angles.split(",")):
                if p.replace(".", "").isdigit():
                    restricted_angles.append(float(p))
                else:
                    restricted_angles.append(False)
                    random_angles.append(0.0)
            self.restricted_angles = restricted_angles
            random_vector.set_unit_cell_angles(random_angles)
            return

        # TODO: this needs to tidy up, why pass in a crystal object into a
        # method belong to a crystal class?
        self.determine_restricted_angles(crystal, random_vector)

        if self.lattice_type == 0:
            raise CSPyException("Lattice type is zero, exiting.")

        if self.lattice_type == 1:
            # Case of Triclinice lattice, all three angles could be varied
            # independently, search space dimension is three.
            random_vector.set_unit_cell_angles([0.0] * 3)
            return

        if self.lattice_type == 2:
            # Case of Monoclinic lattice. Instead of work out the unique axis
            # directly from the given symmetry operators, we worked that out in
            # a brute force manner by testing all three directions along the
            # lattice vectors.  In standard orientation, beta is the
            # non-orthogonal angle, but if the orientation changes, this will
            # change correspondingly, and should reflect in the symmetry
            # operators.
            random_vector.set_unit_cell_angles([0.0])
            return

        if self.lattice_type in [3, 4, 7]:
            # Cases of Orthorhombic, Tetragonal and Cubic lattices, all three
            # angles were 90 degrees. So the cell angle search space is 0 and
            # all three angles should be restricted.
            random_vector.set_unit_cell_angles([])
            return

        if self.lattice_type == 5:
            # Cases of Trigonal lattice, all three cell angles were identical
            # and not orthogonal. The search space is one dimensional.  Only
            # working in rhomohedral settings
            if Lattice._TRIGONAL_SETTING == Lattice._RHOMBOHEDRAL:
                random_vector.set_unit_cell_angles([0.0])
            elif Lattice._TRIGONAL_SETTING == Lattice._HEXAGONAL:
                raise CSPyException(
                    "Cannot handle hexagonal setting for trigonal lattice"
                )
            else:
                raise CSPyException("Unknown setting for trigonal lattice")
            return

        if self.lattice_type == 6:
            # Case of hexagonal lattice. Need to work out first which one is
            # the long axis based on the lengths of the unit cells from the
            # random vectors, and then assign the cell angles accordingly.
            # nevertheless, all cell angles were of fixed values and the search
            # space for this is of zero dimension.
            random_vector.set_unit_cell_angles([])
            return
        # this statement may no longer be true
        crystal_logger.info("Error - check lattice.lattice_type or complete subroutine")
        crystal_logger.info("lattice.determine_fixed_angles for lattice types 5 and 6")
        exit()

    def _is_symmetry_operation_compatible_with_unique_axis_setting(
        self, crystal, test_lattice, pt1, pt2
    ):

        for op in crystal.spacegroup.operations:
            # test if this symmetry operation is a length-preserving operation
            # on this random vector, if so, this lead to a unique axis along
            # the current direction, otherwise, not a unique axis
            pt1_cart = self.lattice.to_cartesian(pt1)
            pt2_cart = self.lattice.to_cartesian(pt2)

            starting_vec_cart = pt1_cart - pt2_cart
            starting_vec_mag = np.linalg.norm(starting_vec_cart)
            starting_vec_mag = round(starting_vec_mag, 3)

            transformed_pt1_frac = op.apply_symmetry_op(pt1)
            transformed_pt2_frac = op.apply_symmetry_op(pt2)
            transformed_pt1_cart = self.lattice.to_cartesian(transformed_pt1_frac)
            transformed_pt2_cart = self.lattice.to_cartesian(transformed_pt2_frac)
            transformed_vec_cart = transformed_pt1_cart - transformed_pt2_cart
            transformed_vec_mag = np.linalg.norm(transformed_vec_cart)
            transformed_vec_mag = round(transformed_vec_mag, 3)

            if transformed_vec_mag != starting_vec_mag:
                unique_flag = False
            else:
                unique_flag = True

        return unique_flag

    def determine_unique_axis(self, crystal):
        def none(x, y):
            return None

        lat_func = {
            1: none,
            2: self._determine_unique_axis_for_monoclinic_system,
            3: none,
            4: self._determine_unique_axis_for_four_and_six,
            5: none,
            6: self._determine_unique_axis_for_four_and_six,
            7: none,
        }
        lat_func[self.lattice_type](crystal, None)

    def _determine_unique_axis_for_four_and_six(self, crystal, random_vector):
        # Basically the definition of unique axis would follow the definition
        # of symmetry operators.  Instead of working out directly from the
        # given symmetry operator, this routine use a brute force method to
        # find out symmetry operations that preseves the length of this random
        # vector and from which the unique axis was determined.
        pt1 = [0.234232, 0.865978, 0.145313]
        pt2 = [0.873548, 0.147889, 0.698323]
        test_lattice = Lattice()
        test_lens = [0.0] * 3
        test_angles = [90.0] * 3
        # permutations along three different spatial orientations.
        for i_len in range(3):
            # for the current axis, set the lattice vector along this direction
            # to be the shortest, but the other two the longest with identical
            # length.
            test_lens[i_len] = 5.0
            test_lens[(i_len + 1) % 3] = 10.0
            test_lens[(i_len + 2) % 3] = 10.0

            if self.lattice_type == 6:
                # for hexagonal system, angle index corresponding to the short
                # axis is 120, the others 90.
                test_angles[i_len] = 120.0
                test_angles[(i_len + 1) % 3] = 90.0
                test_angles[(i_len + 2) % 3] = 90.0

            # construct a dummy lattice based on the lattice parameter given.
            # Calculate lattice vectors and inverse lattice vectors.
            test_lattice.set_lattice_parameters(test_lens + test_angles)
            if self._is_symmetry_operation_compatible_with_unique_axis_setting(
                crystal, test_lattice, pt1, pt2
            ):
                self.unique_axis = i_len

    def _determine_unique_axis_for_monoclinic_system(self, crystal, random_vector):
        # Construct two random points that none of them lies on special
        # positions.
        pt1 = [0.234232, 0.865978, 0.145313]
        pt2 = [0.873548, 0.147889, 0.698323]
        test_lengths = [21.3, 12.3, 2.5]
        test_angles = [90.0, 90.0, 72.3]
        test_lattice = Lattice()
        for i_angle in range(3):
            test_lattice.set_lattice_parameters(
                test_lengths
                + [
                    test_angles[i_angle],
                    test_angles[(i_angle + 1) % 3],
                    test_angles[(i_angle + 2) % 3],
                ]
            )
            if self._is_symmetry_operation_compatible_with_unique_axis_setting(
                crystal, test_lattice, pt1, pt2
            ):
                self.unique_axis = i_angle
                break

    def determine_restricted_angles(self, crystal, random_vector):

        if self.lattice_type == 0:
            raise CSPyException("Lattice type is zero, exiting.")
            return

        if self.lattice_type == 1:
            self.restricted_angles = [False] * 3
            return

        if self.lattice_type == 2:
            self.restricted_angles = [90.0] * 3
            self._determine_unique_axis_for_monoclinic_system(crystal, random_vector)
            self.restricted_angles[2 - self.unique_axis] = False
            return

        if self.lattice_type in [3, 4, 7]:
            self.restricted_angles = [90.0] * 3
            return

        if self.lattice_type == 5:
            self.restricted_angles = [False] * 3
            return

        if self.lattice_type == 6:
            if self.unique_axis is None:
                self.determine_fixed_lengths(crystal, random_vector)
            if self.unique_axis is None:
                raise CSPyException("No unique axis for hexagonal system.")
            temp_v = [90.0] * 3
            temp_v[int(self.unique_axis)] = 120.0
            self.restricted_angles = temp_v
            return

    def calc_inv_lattice_vectors(self):
        a, b, c = self.lattice_parameters[:3]
        ca, cb, cg = np.cos(np.radians(self.lattice_parameters[3:]))
        sg = np.sin(np.radians(self.lattice_parameters[5]))
        v = self.lattice_volume()
        r = [
            [1 / a, 0.0, 0.0],
            [-cg / (a * sg), 1 / (b * sg), 0],
            [
                b * c * (ca * cg - cb) / v / sg,
                a * c * (cb * cg - ca) / v / sg,
                a * b * sg / v,
            ],
        ]
        i = np.array(r)
        self.inv_lattice_vectors = i.tolist()
        well_conditioned = np.allclose(
            np.dot(self.lattice_vectors, self.inv_lattice_vectors), np.eye(3), atol=1e-6
        )
        if not well_conditioned:
            crystal_logger.warn(
                "Ill conditioned lattice parameters: possible loss of precision"
            )
            crystal_logger.warn("D:\n%s", self.direct_matrix())
            crystal_logger.warn("I:\n%s", self.inverse_matrix())
            crystal_logger.warn(
                "D.I:\n%s", np.dot(self.direct_matrix(), self.inverse_matrix())
            )

    def frac_to_cart_hack(self, coords_in, lengths, angles):
        v = (
            1.0
            + 2.0 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])
            - math.cos(angles[0]) ** 2
            - math.cos(angles[1]) ** 2
            - math.cos(angles[2]) ** 2
        ) ** 0.5
        coords_out = [0.0] * 3

        coords_out[2] = (
            lengths[0] * coords_in[0]
            + lengths[1] * coords_in[1] * math.cos(angles[2])
            + lengths[2] * coords_in[2] * math.cos(angles[1])
        )

        coords_out[1] = lengths[1] * coords_in[1] * math.sin(angles[2]) + lengths[
            2
        ] * coords_in[2] * (
            math.cos(angles[0]) - math.cos(angles[1]) * math.cos(angles[2])
        ) / math.sin(
            angles[2]
        )

        coords_out[0] = lengths[2] * coords_in[2] * v / (math.sin(angles[2]))
        return coords_out

    def set_lattice_parameters(self, params):
        self.lattice_parameters = copy.deepcopy(params)
        a, b, c = params[:3]
        ca, cb, cg = np.cos(np.radians(params[3:]))
        sg = np.sin(np.radians(params[5]))
        v = self.lattice_volume()
        self.lattice_vectors = np.transpose(
            [
                [a, b * cg, c * cb],
                [0, b * sg, c * (ca - cb * cg) / sg],
                [0, 0, v / (a * b * sg)],
            ]
        ).tolist()
        r = [
            [1 / a, 0.0, 0.0],
            [-cg / (a * sg), 1 / (b * sg), 0],
            [
                b * c * (ca * cg - cb) / v / sg,
                a * c * (cb * cg - ca) / v / sg,
                a * b * sg / v,
            ],
        ]
        i = np.array(r)
        self.inv_lattice_vectors = i.tolist()

    def set_lattice_vectors(self, vectors):
        """
        This is performed by setting the lattice parameters
        based on the provided vectors, as that results in
        a consistent basis without inverting a matrix, and
        as the res file/cif output will be relying on these
        lengths/angles anyway.

        """
        self.lattice_vectors = vectors
        params = np.zeros(6)
        params[:3] = np.linalg.norm(self.lattice_vectors, axis=1)
        a, b, c = params[:3]
        u_a = vectors[0, :] / a
        u_b = vectors[1, :] / b
        u_c = vectors[2, :] / c
        alpha = np.arccos(np.clip(np.vdot(u_b, u_c), -1, 1))
        beta = np.arccos(np.clip(np.vdot(u_c, u_a), -1, 1))
        gamma = np.arccos(np.clip(np.vdot(u_a, u_b), -1, 1))
        params[3:] = np.degrees([alpha, beta, gamma])
        self.lattice_parameters = params
        self.inv_lattice_vectors = np.linalg.inv(self.lattice_vectors)

    def create_translations(self, depths, cutoff=None):
        if isinstance(depths, int):
            depths = [depths] * 3
        translations = []
        for ia in range(-1 * depths[0], depths[0] + 1):
            for ib in range(-1 * depths[1], depths[1] + 1):
                for ic in range(-1 * depths[2], depths[2] + 1):
                    trans = list3vecmatrix([ia, ib, ic], self.lattice_vectors)
                    if cutoff and list3mag(trans) < cutoff:
                        translations.append(trans)
                    else:
                        translations.append(trans)

        return translations

    def lattice_volume(self):
        a, b, c = self.lattice_parameters[:3]
        ca, cb, cg = np.cos(np.radians(self.lattice_parameters[3:]))
        sg = np.sin(np.radians(self.lattice_parameters[5]))
        return a * b * c * np.sqrt(1 - ca * ca - cb * cb - cg * cg + 2 * ca * cb * cg)

    def __repr__(self):
        return "Lattice({:.4f} {:.4f} {:.4f} {:.4f} {:.4f} {:.4f})".format(
            *self.lattice_parameters
        )


def frac_to_cart(coords_in, lengths, angles):
    # ANGLES MUST BE IN RADIANS, and as it says, coords_in are fractional (but
    # out are in angstrom)
    v = (
        1.0
        + 2.0 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])
        - math.cos(angles[0]) ** 2
        - math.cos(angles[1]) ** 2
        - math.cos(angles[2]) ** 2
    ) ** 0.5

    coords_out = [0.0] * 3
    coords_out[0] = (
        lengths[0] * coords_in[0]
        + lengths[1] * coords_in[1] * math.cos(angles[2])
        + lengths[2] * coords_in[2] * math.cos(angles[1])
    )
    coords_out[1] = lengths[1] * coords_in[1] * math.sin(angles[2]) + lengths[
        2
    ] * coords_in[2] * (
        math.cos(angles[0]) - math.cos(angles[1]) * math.cos(angles[2])
    ) / math.sin(
        angles[2]
    )
    coords_out[2] = lengths[2] * coords_in[2] * v / (math.sin(angles[2]))
    return coords_out


def cart_to_frac(coords_in, lengths, angles):
    v = (
        1.0
        + 2.0 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])
        - math.cos(angles[0]) ** 2
        - math.cos(angles[1]) ** 2
        - math.cos(angles[2]) ** 2
    ) ** 0.5

    coords_out = [0.0] * 3
    coords_out[0] = (1.0 / lengths[0]) * (
        coords_in[0]
        - coords_in[1] * math.cos(angles[2]) / math.sin(angles[2])
        + coords_in[2]
        * (math.cos(angles[2]) * math.cos(angles[0]) - math.cos(angles[1]))
        / (v * math.sin(angles[2]))
    )

    coords_out[1] = (1.0 / lengths[1]) * (
        coords_in[1] / math.sin(angles[2])
        + coords_in[2]
        * (math.cos(angles[1]) * math.cos(angles[2]) - math.cos(angles[0]))
        / (v * math.sin(angles[2]))
    )
    coords_out[2] = (coords_in[2] / lengths[2]) * (math.sin(angles[2]) / v)
    return coords_out


LengthBounds = namedtuple("LengthBounds", ["max", "min"])


class CrystalStructure(object):
    """
    The CrystalStructure class has routines for reading from SHELX files,
    writing SHELX and CIF files, performing DMACRYS calculations.

    It contains two main pieces of data: self.lattice and
    self.unique_molcule_list; these are a Lattice class and a list of instances
    of Molecules class that represents the asymmetric unit.

    There are many routines relating to generating structures e.g. expanding
    the lattice based on SAT.
    """

    def __init__(self, filename=None):
        self.lattice = Lattice()
        self.unique_molecule_list = []
        self.molecule_list = []
        self.name = ""
        self.titl = ""
        self.cell = ""
        self.wavelength = 1.000
        self.zerr = None
        self.latt = ""
        self.symm = []
        self.sfac = ""
        self.unit = None
        self.fvar = None
        self.filename = filename
        if filename:
            crystal_logger.debug("Initialising crystal from %s", filename)
            if filename.endswith(".cif"):
                crystal_logger.info("Using experimental cif file reader")
                self.init_from_cif_file(filename)
                self.name = filename.replace(".cif", "")
            elif filename.endswith(".xyz"):
                self.init_from_xyz_file(filename, titl=filename[:-4])
            else:
                self.init_from_res_file(filename)
                self.name = filename.replace(".res", "")
        self.set_radiation_source()
        self.energy = {"lattice": None}
        self.z = None
        self.z_prime = None
        self.mols_per_cell = None
        self.symm_ops_per_cell = None
        self.voids = Voids(parent=self)
        return

    @classmethod
    def from_res_string(cls, res_string):
        c = cls()
        c.init_from_res_string(res_string)
        return c

    @classmethod
    def from_pmin_save(cls, pmin_save_contents):
        crys = cls()
        save = PminSave(pmin_save_contents)
        a, b, c, ca, cb, cg = save.params
        crys.set_lattice_parameters(
            [
                a,
                b,
                c,
                np.degrees(np.arccos(ca)),
                np.degrees(np.arccos(cb)),
                np.degrees(np.arccos(cg)),
            ]
        )

        temp_molecule = Molecule()
        for label, idx, frac_coord in save.atoms:
            xyz = crys.lattice.to_cartesian(frac_coord)
            temp_molecule.add_atom(Atom(label, xyz))
            atom = temp_molecule.atoms[-1]
            atom.attributes["labelindex"] = int(idx)
        crys.populate_unique_molecule_list(temp_molecule)
        crys.update_sfac()
        return crys

    def get_read_only(self):
        return self

    def is_identical_twin(
        self,
        twin,
        compare_symmetry_copies=False,
        xyz_err=0.0001,
        ignore_space_group=False,
    ):
        """ Pass this another crystal, if tests are matched assume that they
        are the same crystal if not compare_symmetry_copies then the mols must
        be at same fractional coordinates The ignore_space_group option is for
        settings or space groups which are non-standard """

        if (
            not ignore_space_group
            and self.determine_space_group() != twin.determine_space_group()
        ):
            crystal_logger.info("Different space groups")
            return False
        # Check centres of mass (and moments inertia)??
        if compare_symmetry_copies:
            crystal_logger.info("ERROROROROR DO THIS LATER")
        for mol1 in self.unique_molecule_list:
            for at1 in mol1.atoms:
                matched_atom = False
                for mol2 in twin.unique_molecule_list:
                    for at2 in mol2.atoms:
                        if at2.clean_label() != at1.clean_label():
                            continue
                        if all(
                            [
                                at1.xyz[i] < at2.xyz[i] + xyz_err
                                and at1.xyz[i] > at2.xyz[i] - xyz_err
                                for i in range(3)
                            ]
                        ):
                            matched_atom = True
                            break
                    if matched_atom:
                        break
                if not matched_atom:
                    crystal_logger.info("Failed to find an atom")
                    return False
        return True

    def pmin_refinement_code(self):
        # TODO fix this for rhombohedral case, check angles
        if abs(int(self.latt)) == 1:
            return "111111"
        elif abs(int(self.latt)) == 2:
            return "111001"
        else:
            return "111000"

    def set_titl(self, base_name=None, sg_num=None, sob_id=None):
        titl_string = base_name + "_" + sg_num + "_" + sob_id
        for im, mol in enumerate(self.unique_molecule_list):
            if mol.history:
                titl_string += "mol" + str(im) + "(" + mol.history + ")"
        self.titl = titl_string
        return self.titl

    # should pass list of molecules???
    def populate_unique_molecule_list(self, molecule):
        self.unique_molecule_list = molecule.fragment_geometry()
        return len(self.unique_molecule_list)

    def recalculate_sfac(self):
        all_labels = [
            x.clean_label() for mol in self.unique_molecule_list for x in mol.atoms
        ]
        self.sfac = " ".join(set(all_labels))

    def atomic_numbers(self):
        nums = []
        for mol in self.unique_molecule_list:
            nums += [x.atomic_number for x in mol.atoms]
        return np.asarray(nums)

    def atomic_positions(self, frac=False):
        mol_xyz = []
        for mol in self.unique_molecule_list:
            mol_xyz.append(mol.xyz_asarray())
        coords = np.vstack(mol_xyz)
        if frac:
            coords = self.lattice.to_fractional(coords)
        return coords

    def set_atomic_positions(self, coords, frac=False):
        if frac:
            coords = self.lattice.to_cartesian(coords)
        i = 0
        for mol in self.unique_molecule_list:
            for atom in mol.atoms:
                atom.xyz = coords[i, :]
                i += 1

    def set_asymmetric_unit(self, molecules):
        self.unique_molecule_list = [copy.deepcopy(m) for m in molecules]
        self.recalculate_sfac()

    def add_molecules(self, molecules):
        self.set_asymmetric_unit(self.unique_molecule_list + molecules)

    def symmetry_operations(self):
        from cspy.deprecated.spacegroups import expand_symmetry_list, SymmetryOperation

        return [
            SymmetryOperation(s, None)
            for s in expand_symmetry_list(self.symm, int(self.latt))
        ]

    def calculate_symm_ops_per_cell(self):
        from cspy.deprecated.spacegroups import expand_symmetry_list

        self.symm_ops_per_cell = len(expand_symmetry_list(self.symm, int(self.latt)))
        return self.symm_ops_per_cell

    def set_z_prime(self, zp):
        self.z_prime = zp
        return self.z_prime

    def change_space_group(self, sg_index):
        from cspy.deprecated.space_groups_list import sg_symm_ele, crys_system
        from cspy.deprecated.spacegroups import sg

        self.latt = "-1"
        self.symm = sg_symm_ele[sg_index].rstrip("'] ").lstrip(" ['").split("', '")
        self.lattice.lattice_type = self.lattice.lattice_type_dictionary(
            crys_system[sg_index].replace(" ", "")
        )
        self.calculate_z()
        self.zerr = "{} 0.0 0.0 0.0 0.0 0.0 0.0".format(self.z)
        self.spacegroup = sg[sg_index]
        self.spacegroup_index = sg_index
        self.generate_unit_cell_molecules()
        self.calculate_symm_ops_per_cell()
        my_sfac = self.sfac.split()
        my_sfac_counter = [0] * len(self.sfac.split())
        for molecule in self.unique_molecule_list:
            for atom in molecule.atoms:
                for i, ms in enumerate(my_sfac):
                    if ms == atom.clean_label():
                        my_sfac_counter[i] += self.symm_ops_per_cell
        self.unit = " ".join(str(x) for x in my_sfac_counter)

    def determine_space_group(self):
        from cspy.deprecated.space_groups_list import sg_symm_ele
        from cspy.deprecated.spacegroups import (
            SpaceGroup,
            SymmetryOperation,
            expand_symmetry_list,
        )

        test_point1 = [0.23424, 0.12348, 0.98566]
        neighbour_cells = self.lattice.make_all_translations_frac(2)
        sg_ops = [
            SymmetryOperation(symm, lambda_func=None)
            for symm in expand_symmetry_list(self.symm, int(self.latt))
        ]
        self.spacegroup = SpaceGroup(sg_ops)
        my_spacegroup_list = copy.deepcopy(self.spacegroup.operations)
        test_points1 = []
        for op in my_spacegroup_list:
            test_points1.append(op.apply_symmetry_op(test_point1))
        sg_out = 0
        for sg_index in range(1, 230):
            correct_group = True
            if (
                len(sg_symm_ele[sg_index].split("',")) == len(test_points1)
                and "NULL" not in sg_symm_ele[sg_index].split("',")[0]
            ):
                correct_group = True
            else:
                continue
            self.change_space_group(sg_index)
            sg_ops = [
                SymmetryOperation(symm, lambda_func=None)
                for symm in expand_symmetry_list(self.symm, int(self.latt))
            ]
            self.spacegroup = SpaceGroup(sg_ops)
            my_spacegroup_list = copy.deepcopy(self.spacegroup.operations)
            test_points2 = []
            for op in my_spacegroup_list:
                test_points2.append(op.apply_symmetry_op(test_point1))
            for pt in test_points2:
                local_match = False
                for pt1 in test_points1:
                    for nv in neighbour_cells:
                        pt3 = np.array(pt1) + np.array(nv)
                        if (
                            round(pt[0], 4) == round(pt3[0], 4)
                            and round(pt[1], 4) == round(pt3[1], 4)
                            and round(pt[2], 4) == round(pt3[2], 4)
                        ):
                            local_match = True
                            break
                if local_match is False:
                    correct_group = False
                    break
            if correct_group:
                if sg_out > 0:
                    crystal_logger.info("Error in determine_space_group")
                sg_out = sg_index
        self.change_space_group(sg_out)
        return sg_out

    def factorize_out_latt(self):
        from cspy.deprecated.spacegroups import SpaceGroup
        from cspy.deprecated.spacegroups import SymmetryOperation, expand_symmetry_list

        test_point1 = [0.23424, 0.12348, 0.98566]
        sg_ops = [
            SymmetryOperation(symm, lambda_func=None)
            for symm in expand_symmetry_list(self.symm, int(self.latt))
        ]
        self.spacegroup = SpaceGroup(sg_ops)
        my_spacegroup_list = copy.deepcopy(self.spacegroup.operations)
        i1, i2 = 0, 0
        related_elements = []
        while i1 < len(my_spacegroup_list):
            test_point2 = my_spacegroup_list[i1].apply_symmetry_op(test_point1)
            i2 = i1 + 1
            while i2 < len(my_spacegroup_list):
                test_point3 = my_spacegroup_list[i2].apply_symmetry_op(test_point1)
                for nct in self.lattice.make_all_translations_frac(1):
                    test_point4 = np.array(test_point3) + np.array(nct)
                    if (
                        same_value(test_point2[0], -1.0 * test_point4[0])
                        and same_value(test_point2[1], -1.0 * test_point4[1])
                        and same_value(test_point2[2], -1.0 * test_point4[2])
                    ):
                        related_elements.append(self.spacegroup.operations[i2])
                        del my_spacegroup_list[i2]
                        continue
                i2 += 1
            i1 += 1
        if len(related_elements) == len(my_spacegroup_list):
            self.latt = "1"
            self.symm = [str(x) for x in my_spacegroup_list]
            sg_ops = [SymmetryOperation(symm, lambda_func=None) for symm in self.symm]
            self.spacegroup = SpaceGroup(sg_ops)
        else:
            self.latt = "-1"
        # 1 = P, 2 = I, 3 = rhombohedral obverse on hexagonal axes,
        #           4 = F, 5 = A, 6 = B, 7 = C. #NOTE WELL, 3 is particular
        pure_translations = {
            2: [[0.50000, 0.50000, 0.50000]],
            3: [[2.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0], [1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0]],
            4: [
                [0.50000, 0.50000, 0.00000],
                [0.50000, 0.00000, 0.50000],
                [0.00000, 0.50000, 0.50000],
            ],
            5: [[0.00000, 0.50000, 0.50000]],
            6: [[0.50000, 0.00000, 0.50000]],
            7: [[0.50000, 0.50000, 0.00000]],
        }
        for ipt in range(6):
            # Must do F before A,B,C as these are elements of {F}
            my_spacegroup_list = copy.deepcopy(self.spacegroup.operations)
            for each_pt in pure_translations[ipt + 2]:
                i1, i2 = 0, 0
                related_elements = []
                while i1 < len(my_spacegroup_list):
                    test_point2 = my_spacegroup_list[i1].apply_symmetry_op(test_point1)
                    i2 = i1 + 1
                    while i2 < len(my_spacegroup_list):
                        test_point3 = np.array(
                            my_spacegroup_list[i2].apply_symmetry_op(test_point1)
                        ) + np.array(each_pt)
                        for nct in self.lattice.make_all_translations_frac(1):
                            test_point4 = np.array(test_point3) + np.array(nct)
                            if (
                                same_value(test_point2[0], test_point4[0])
                                and same_value(test_point2[1], test_point4[1])
                                and same_value(test_point2[2], test_point4[2])
                            ):
                                related_elements.append(self.spacegroup.operations[i2])
                                del my_spacegroup_list[i2]
                                continue
                        i2 += 1
                    i1 += 1
            if len(my_spacegroup_list) * (len(pure_translations[ipt + 2]) + 1.0) == len(
                self.spacegroup.operations
            ):
                self.latt = str(int(self.latt) * (ipt + 2))
                self.symm = [str(x) for x in my_spacegroup_list]
                return

    def set_lattice_parameters(self, params):
        self.lattice.set_lattice_parameters(params)
        return

    def init_from_xyz_file(self, xyz_file, titl, others=[]):
        with open(xyz_file) as f:
            xyz_contents = f.read()

        other_xyz_contents = []
        crystal_logger.debug("xyz_contents:\n%s", xyz_contents)

        for other in others:
            with open(other) as f:
                other_xyz_contents.append(f.read())

        self.init_from_xyz_string(xyz_contents, titl, others=other_xyz_contents)

    def init_from_xyz_string(self, xyzstring, titl, others=[]):
        mols_xyz = [xyzstring]
        if others is not None:
            mols_xyz += others
        mols = []
        label_index = {}
        boxa, boxb, boxc = 0, 0, 0
        for i, xyz in enumerate(mols_xyz):
            m = Molecule()
            m.init_from_xyz_str(xyz)
            # no longer going to reorder the atoms
            # into the neighcrys order
            # m.order_atoms_for_neighcrys()
            mols.append(m)
            m_bbox = m.get_bounding_box_size() + 4.0
            boxa += m_bbox[0] + 3.0
            boxb += m_bbox[1]
            boxc += m_bbox[2]
            boxa, boxb, boxc = max(5.0, boxa), max(5.0, boxb), max(5.0, boxc)
            centroid = m.centroid()
            m.translate(
                [-centroid[0] + (boxa if i > 0 else 0.0), -centroid[1], -centroid[2]]
            )
            label_index.update({t: 0 for t in m.atom_types})

        self.name = titl
        resstring = (
            """TITL """
            + titl
            + """
CELL 1.0 %f %f %f 90.0 90.0 90.0
LATT -1
ZERR 1 0.0 0.0 0.0 0.0 0.0 0.0
SFAC """
            + (" ".join(label_index.keys()))
            + "\n"
        )
        resstring = resstring % tuple([boxa, boxb, boxc])

        b = Lattice()
        b.set_lattice_parameters([boxa, boxb, boxc, 90.0, 90.0, 90.0])

        for m, mol in enumerate(mols):
            xyz = mol.xyz_asarray()
            crystal_logger.debug("Molecule %d xyz:\n%s", m, xyz)
            abc = b.to_fractional(xyz)
            crystal_logger.debug("Molecule %d frac_xyz:\n%s", m, abc)
            labels = mol.cleaned_labels()

            for i, label in enumerate(labels):
                label_index[label] += 1
                label_index_str = str(label_index[label])
                mol.atoms[i].attributes["res_label"] = label + label_index_str
                sfac_index_str = str(list(mol.atom_types).index(label) + 1)
                resstring += "{}{} {} {:15.9f} {:15.9f} {:15.9f}\n".format(
                    label, label_index_str, sfac_index_str, *abc[i, :]
                )
        resstring += "END"
        self.init_from_res_string(resstring)

    def init_from_mult(self, mult_file, titl="titl"):
        self.name = titl
        a = Molecule()
        a.set_geometry_from_mult(mult_file)
        resstring = (
            """TITL """
            + titl
            + """
CELL 1.0 10.0 10.0 10.0 90.0 90.0 90.0
LATT -1
ZERR 1 0.0 0.0 0.0 0.0 0.0 0.0
SFAC """
            + (" ".join(a.atom_types))
            + "\n"
        )
        label_index = dict([(t, 0) for t in a.atom_types])
        b = Lattice()
        b.set_lattice_parameters([10.0, 10.0, 10.0, 90.0, 90.0, 90.0])
        for i, atom in enumerate(a.xyz_form(clean_labels=True)):
            xyz = b.to_fractional([float(x) for x in atom[1:4]])
            test = Atom(atom[0], atom[1:4])
            label_index[test.clean_label()] += 1
            label_index_str = str(label_index[test.clean_label()])
            a.atoms[i].attributes["res_label"] = test.clean_label() + label_index_str
            sfac_index_str = str(list(a.atom_types).index(test.clean_label()) + 1)
            resstring += (
                test.clean_label()
                + label_index_str
                + " "
                + sfac_index_str
                + " %15.9f %15.9f %15.9f " % tuple(xyz)
                + "\n"
            )
        resstring += "END"
        self.init_from_res_string(resstring)

    # return a crystal
    def set_geometry_from_mult_crystal(self, mult_file):
        mf = open(mult_file, "r")
        template_molecule = Molecule()
        box = 0.0  # place molecules in boxes along the x-axis
        for line in mf.readlines():
            if "Rank" in line and "_i" not in line:
                sxyz = line.split("_")[-1].split("Rank")[0].lstrip().rstrip()
                xyz = list(map(float, sxyz.split(" ")[0:3]))
                xyz[0] = xyz[0] + box * 20.0
                a = Atom(line.split("_")[0] + line.split("_")[2], xyz)
                template_molecule.add_atom(a)
            if "#ENDMOL" in line:
                self.unique_molecule_list.append(template_molecule)
                box += 1.0
                del template_molecule
                template_molecule = Molecule()
            if "_i" in line:
                del template_molecule
                break
        mf.close()

    def init_from_mult_crystal(self, mult_file, titl="titl"):
        self.name = titl
        a = CrystalStructure()
        a.set_geometry_from_mult_crystal(mult_file)
        atom_type_set = []
        for mol in a.unique_molecule_list:
            atom_type_set += mol.atom_types
        length = " " + str(
            float(len(a.unique_molecule_list)) * 40.0
        )  # place in boxes along x-axis

        resstring = (
            """TITL """
            + titl
            + """
CELL 1.0"""
            + length
            + """ 40.0 40.0 90.0 90.0 90.0
LATT -1
ZERR 1 0.0 0.0 0.0 0.0 0.0 0.0
SFAC """
            + (" ".join(set(atom_type_set)))
            + "\n"
        )
        for mol in a.unique_molecule_list:
            label_index = dict([(t, 0) for t in mol.atom_types])
        b = Lattice()
        b.set_lattice_parameters(
            [float(len(a.unique_molecule_list)) * 40.0, 40.0, 40.0, 90.0, 90.0, 90.0]
        )
        for mol in a.unique_molecule_list:
            for i, atom in enumerate(mol.xyz_form(clean_labels=True)):
                xyz = b.to_fractional([float(x) for x in atom[1:4]])
                test = Atom(atom[0], atom[1:4])
                label_index[test.clean_label()] += 1
                label_index_str = str(label_index[test.clean_label()])
                mol.atoms[i].attributes["res_label"] = (
                    test.clean_label() + label_index_str
                )
                sfac_index_str = str(list(mol.atom_types).index(test.clean_label()) + 1)
                resstring += (
                    test.clean_label()
                    + label_index_str
                    + " "
                    + sfac_index_str
                    + " %15.9f %15.9f %15.9f " % tuple(xyz)
                    + "\n"
                )
        resstring += "END"
        self.init_from_res_string(resstring)

    def init_from_fchk(self, fchk_input, titl=""):
        self.name = titl
        a = Molecule()
        a.init_from_fchk(fchk_input)
        resstring = (
            """TITL """
            + titl
            + """
CELL 1.0 10.0 10.0 10.0 90.0 90.0 90.0
LATT -1
ZERR 1 0.0 0.0 0.0 0.0 0.0 0.0
SFAC """
            + (" ".join(a.atom_types))
            + "\n"
        )
        label_index = dict([(t, 0) for t in a.atom_types])
        b = Lattice()
        b.set_lattice_parameters([10.0, 10.0, 10.0, 90.0, 90.0, 90.0])
        for i, atom in enumerate(a.xyz_form(clean_labels=True)):
            xyz = b.to_fractional([float(x) for x in atom[1:4]])
            test = Atom(atom[0], atom[1:4])
            label_index[test.clean_label()] += 1
            label_index_str = str(label_index[test.clean_label()])
            a.atoms[i].attributes["res_label"] = test.clean_label() + label_index_str
            sfac_index_str = str(list(a.atom_types).index(test.clean_label()) + 1)
            resstring += (
                test.clean_label()
                + label_index_str
                + " "
                + sfac_index_str
                + " %15.9f %15.9f %15.9f " % tuple(xyz)
                + "\n"
            )
        resstring += "END"
        self.init_from_res_string(resstring)

    def init_from_res_string(self, resstring, make_D_H=True):
        try:
            temp_molecule = Molecule()
            for i, line in enumerate(resstring.split("\n"), start=1):
                line = line.strip()
                ls = line.split()
                if line.startswith("END"):
                    break
                if line.startswith("CELL"):
                    self.set_lattice_parameters(list(map(float, ls[2:8])))
                    self.cell = line[4:].strip()
                    self.wavelength = float(ls[1])
                elif line.startswith("SFAC"):
                    if ("D" in line[4:].strip().split()) and make_D_H:
                        for item in line[4:].strip().split():
                            if item == "D":
                                crystal_logger.info("Swapping D to H in SFAC")
                                m = "H"
                            else:
                                m = item
                            try:
                                Atom(m, [0, 0, 0])
                                self.sfac += " " + m
                            except Exception as exc:
                                crystal_logger.warn(
                                    "Encountered exception on line %d:%s", i, exc
                                )
                    else:
                        for item in line[4:].strip().split():
                            try:
                                Atom(item, [0, 0, 0])
                                self.sfac += " " + item
                            except Exception as exc:
                                crystal_logger.warn(
                                    "Encountered exception on line %d:%s", i, exc
                                )

                elif line.startswith("SYMM"):
                    self.symm.append(line[4:].strip().replace(" ", ""))
                elif line.startswith("LATT"):
                    self.latt = line[4:].strip()
                elif line.startswith("TITLE"):
                    self.titl = line[5:].strip()
                elif line.startswith("TITL"):
                    self.titl = line[4:].strip()
                elif line.startswith("ZERR"):
                    self.zerr = line[4:].strip()
                elif line.startswith("UNIT"):
                    self.unit = line[4:].strip()
                elif line.startswith("FVAR"):
                    self.fvar = line[4:].strip()
                else:  # if none of these then must be an atom???
                    frac_coord = list(map(float, ls[2:5]))
                    if len(frac_coord) != 3:
                        err_str = "Invalid coordinates on line {}: {}".format(i, line)
                        raise ValueError(err_str)

                    xyz = self.lattice.to_cartesian(frac_coord)

                    if ("D" == ls[0][0]) and make_D_H:
                        m = ls
                        temp_molecule.add_atom(Atom("H" + m[0][1:], xyz))
                        crystal_logger.info("Changing deuterium to hydrogen.")
                    else:
                        temp_molecule.add_atom(Atom(ls[0], xyz))
                    atom = temp_molecule.atoms[-1]
                    atom.attributes["labelindex"] = int(ls[1])

            self.populate_unique_molecule_list(temp_molecule)
        except Exception as exc:
            raise CSPyException("Error loading structure string:\n" + resstring)

    def init_from_res_file(self, filename):

        crystal_logger.debug("Reading crystal structure from " + filename)
        try:
            with open(os.path.normpath(filename), "r") as res:
                self.init_from_res_string(res.read())
        except Exception as exc:
            crystal_logger.error(
                "init_from_res_file() can not open file: %s in folder %s",
                filename,
                os.getcwd(),
            )
            raise CSPyException(
                "init_from_res_file()"
                "can not open file: " + str(filename) + " in folder:" + str(os.getcwd())
            )

    def init_from_cif_string(self, cifstring):
        temp_molecule = Molecule()
        read_symm = False
        read_atom = False
        self.wavelength = 0.7
        frac_coords = []
        atom_prop = []

        def to_float(s):
            return float(s.split("(")[0])

        for line in cifstring.split("\n"):
            line = line.strip()
            ls = line.split()
            if line.startswith("END"):
                break
            if "_cell_length_a" in line:
                a = ls[-1]
            if "_cell_length_b" in line:
                b = ls[-1]
            if "_cell_length_c" in line:
                c = ls[-1]
            if "_cell_angle_alpha" in line:
                alpha = ls[-1]
            if "_cell_angle_beta" in line:
                beta = ls[-1]
            if "_cell_angle_gamma" in line:
                gamma = ls[-1]
            if read_symm and "_" in line:
                read_symm = False
            if read_symm:
                ls = line.replace("'", "").replace('"', "").replace(",", " ").split()
                symm = []
                for el in ls:
                    if "x" in el.lower() or "y" in el.lower() or "z" in el.lower():
                        symm.append(el)
                self.symm.append(",".join(symm).replace(" ", ""))
            if "_symmetry_equiv_pos_as_xyz" in line:
                read_symm = True
            if "_diffrn_radiation_wavelength" in line:
                self.wavelength = ls[-1]
            if "_atom_site" in line:
                read_atom = True
                atom_prop.append(ls)

            elif read_atom and "_" in line:
                read_atom = False
            elif read_atom and "END" in line:
                read_atom = False
            elif read_atom:
                for i in range(len(atom_prop)):
                    if "_atom_site_type_symbol" in atom_prop[i]:
                        isym = i
                    if "_atom_site_label" in atom_prop[i]:
                        ilab = i
                    if "_atom_site_fract_x" in atom_prop[i]:
                        ix = i
                    if "_atom_site_fract_y" in atom_prop[i]:
                        iy = i
                    if "_atom_site_fract_z" in atom_prop[i]:
                        iz = i
                if len(ls) != 0:
                    frac_coords.append(
                        [
                            ls[isym],
                            ls[ilab],
                            to_float(ls[ix]),
                            to_float(ls[iy]),
                            to_float(ls[iz]),
                        ]
                    )

        # Here we try and deal with the symmetry operations
        self.latt = "-1"
        self.update_sfac(retyped=True)
        self.fvar = ""
        self.unit = ""
        self.titl = "tmp"
        self.set_lattice_parameters(
            [to_float(x) for x in (a, b, c, alpha, beta, gamma)]
        )
        for i in range(len(frac_coords)):
            frac_coord = frac_coords[i][2::]
            xyz = self.lattice.to_cartesian(frac_coord)
            temp_molecule.add_atom(Atom(frac_coords[i][0], xyz))
        self.populate_unique_molecule_list(temp_molecule)
        self.zerr = str(len(self.unique_molecule_list))
        self.clean_symmetry_ops()

        # remove x,y,z
        symm = []
        for sym in self.symm:
            if sym != "+x,+y,+z" or sym != "x,y,z":
                symm.append(sym)
        self.symm = symm
        self.latt = "-1"
        self.lattice.lattice_type = 1
        # factorise out the lattice
        self.factorize_out_latt()
        self.clean_symmetry_ops()
        symm = []
        # remove x,y,z again or trying
        # to write a cif will fail
        for sym in self.symm:
            if sym != "+x,+y,+z" and sym != "x,y,z":
                symm.append(sym)
        self.symm = symm
        self.update_sfac(retyped=False)

    def init_from_cif_file(self, filename):
        crystal_logger.debug("Reading crystal structure from " + filename)
        try:
            with open(os.path.normpath(filename)) as cif:
                self.init_from_cif_string(cif.read())
        except Exception as exc:
            crystal_logger.error(
                "init_from_cif_file() can not open file: %s in folder %s",
                filename,
                os.getcwd(),
            )
            raise CSPyException(
                "init_from_cif_file()"
                "can not open file: " + str(filename) + " in folder:" + str(os.getcwd())
            )

    def init_from_g09_output(self, g09_filename, initial_geometry=True):
        test_molecule = Molecule()
        test_molecule.init_from_g09_output(
            g09_filename=g09_filename, initial_geometry=initial_geometry
        )
        if len(self.unique_molecule_list) > 0:
            crystal_logger.debug(
                "Adding molecule from Gaussian output"
                "to crystal with pre-existing molecule"
            )
        self.unique_molecule_list.append(copy.deepcopy(test_molecule))
        return

    def init_from_castep_geom_str(self, string):
        end_search = False
        entry_list = []
        entry = None
        for line in string.split("\n"):
            if "BEGIN" in line:
                end_search = True
            if "END" in line:
                end_search = False
                continue
            if end_search:
                continue
            if line.strip() == "":
                continue
            if "<--" not in line:
                # must be the line number
                if entry:
                    entry_list.append(entry)
                entry = {"energy": None, "lattice": [], "atoms": []}
            elif "<-- E" in line:
                entry["energy"] = list(map(float, line.split()[:-2]))
            elif "<-- h" in line:
                entry["lattice"].append(
                    list(map(lambda x: float(x) * 0.529177249, line.split()[:-2]))
                )
            elif "<-- R" in line:
                entry["atoms"].append(
                    [
                        line.split()[0],
                        list(map(lambda x: float(x) * 0.529177249, line.split()[2:5])),
                    ]
                )
        crystal_logger.debug(entry_list[-1])
        params = []
        params.append(np.linalg.norm(entry["lattice"][0]))
        params.append(np.linalg.norm(entry["lattice"][1]))
        params.append(np.linalg.norm(entry["lattice"][2]))
        params.append(
            np.degrees(np.arccos(np.vdot(entry["lattice"][1], entry["lattice"][2])))
        )
        params.append(
            np.degrees(np.arccos(np.vdot(entry["lattice"][0], entry["lattice"][2])))
        )
        params.append(
            np.degrees(np.arccos(np.vdot(entry["lattice"][0], entry["lattice"][1])))
        )
        crystal_logger.debug("Cell params: ".join(map(str, params)))
        m = Molecule()
        for atom in entry["atoms"]:
            m.add_atom(Atom(atom[0], atom[1]))
        resstring = (
            """TITL CASTEP
CELL 1.0 """
            + " ".join(map(str, params))
            + """
LATT -1
ZERR 1 0.0 0.0 0.0 0.0 0.0 0.0
SFAC """
            + (" ".join(m.atom_types))
            + "\n"
        )
        label_index = dict([(t, 0) for t in m.atom_types])
        b = Lattice()
        b.set_lattice_parameters(params)
        for i, atom in enumerate(m.xyz_form(clean_labels=True)):
            xyz = b.lattice.to_fractional([float(x) for x in atom[1:4]])
            test = Atom(atom[0], atom[1:4])
            label_index[test.clean_label()] += 1
            label_index_str = str(label_index[test.clean_label()])
            m.atoms[i].attributes["res_label"] = test.clean_label() + label_index_str
            sfac_index_str = str(list(m.atom_types).index(test.clean_label()) + 1)
            resstring += (
                test.clean_label()
                + label_index_str
                + " "
                + sfac_index_str
                + " %15.9f %15.9f %15.9f " % tuple(xyz)
                + "\n"
            )
        resstring += "END"
        self.init_from_res_string(resstring)

    def update_sfac(self, retyped=False, standard_order=False):
        sfac = []
        for molecule in self.unique_molecule_list:
            for atom in molecule.atoms:
                if retyped:
                    if atom.get_retyped_label() not in sfac:
                        sfac.append(atom.get_retyped_label())
                else:
                    if atom.clean_label() not in sfac:
                        sfac.append(atom.clean_label())
        if standard_order:
            # make fake molecule containing sfac atoms reorder
            mola = Molecule()
            for i in range(len(sfac)):
                mola.add_atom_from_str(str(sfac[i]) + " 0.0 0.0 0.0")
            mola.standard_atom_order()
            sfac = []
            for i in range(len(mola.atoms)):
                sfac.append(mola.atoms[i].cl)

        sfac_as_str = ""
        for atom_type in sfac:
            sfac_as_str += atom_type + "  "
        if retyped:
            self.sfac_typed = sfac_as_str
        else:
            self.sfac = sfac_as_str

    def max_atom_atom_distance(self):
        mx = 0.0
        for mol in self.unique_molecule_list:
            mx = max(mx, mol.max_atom_atom_distance())
        return mx

    def longest_min_connectivity(self):
        mx = 0
        for mol in self.unique_molecule_list:
            mx = max(mx, mol.longest_min_connectivity())
        return mx

    def remove_zprime_less_than_one(self):
        olatt = self.latt
        osymm = self.symm
        crystal_logger.debug("Generating symmetry operations")
        self.generate_symmetry_operation()
        self.clean_symmetry_ops()
        crystal_logger.debug(str(len(self.unique_molecule_list)))
        crystal_logger.debug("Generate unit cell molecules")
        self.generate_unit_cell_molecules()
        self.check_molecule_list()
        crystal_logger.debug("Found %d molecules in unit cell", len(self.molecule_list))
        crystal_logger.debug("Finding bonded molecules")
        self.find_bonded_molecules()
        crystal_logger.debug("Find operations that create bonds to asymm molecules")
        anyremoved = self.operations_bond_to_asymm()

        if not anyremoved:
            crystal_logger.debug("No symmetry operations were removed")
            self.latt = olatt
            self.symm = osymm

    def calculate_molecules_unit_cell(self):
        from cspy.deprecated.spacegroups import expand_symmetry_list

        try:
            self.mols_per_cell = (
                len(expand_symmetry_list(self.symm, int(self.latt))) * self.z_prime
            )
        except Exception as exc:
            if len(self.unique_molecule_list) == 0:
                self.populate_unique_molecule_list()
            self.mols_per_cell = len(
                expand_symmetry_list(self.symm, int(self.latt))
            ) * len(self.unique_molecule_list)
        return self.mols_per_cell

    def calculate_z(self, num_molecules_in_asymm_unit=1):
        """ Set and return the z value for a CrystalStucture
        NOTE DEFINITION OF Z. USE MOLECULES_PER_CELL ABOVE FOR STRUCTURE
        GENERATION - OTHER PEOPLE ARE WORKING ON SIMILAR PROBS,
        SO IMPROVE THIS LATER
        Now we can feed it a number of molecule in the asymmetric unit,
        to take care of zprime > 1
        """
        from cspy.deprecated.spacegroups import expand_symmetry_list

        self.z = (
            len(expand_symmetry_list(self.symm, int(self.latt)))
            * num_molecules_in_asymm_unit
        )
        return self.z

    def shift_molecules_to_unit_cell(self):
        crystal_logger.info("Shifting molecules to be between (0,0,0) and (1,1,1)")
        coords_list = []
        for mol in self.unique_molecule_list:
            cart_coords = mol.xyz_asarray()
            frac_coords = self.lattice.to_fractional(cart_coords)
            centre = frac_coords.mean(axis=0)
            shift = np.floor(centre)
            frac_coords = frac_coords - shift
            centre = frac_coords.mean(axis=0)
            coords_list.append(self.lattice.to_cartesian(frac_coords))
        coords = np.vstack(coords_list)
        self.set_atomic_positions(coords, frac=False)
        mol = Molecule()
        for a in range(len(self.unique_molecule_list)):
            for b in range(len(self.unique_molecule_list[a].atoms)):
                mol.add_atom(self.unique_molecule_list[a].atoms[b])
        del self.unique_molecule_list[:]
        self.unique_molecule_list = mol.fragment_geometry(lattice=self.lattice)

    def expand_to_full_unit_cell(self):
        """ This returns a primitive (P1) unit cell with ZERR
        equal to the number of symmetry operations.
         See expand_to_primitive_unit_cell() for correct ZERR.
        """
        crystal_logger.info("Generating symmetry operations")
        self.generate_symmetry_operation()
        crystal_logger.debug("Generate unit cell molecules")
        self.generate_unit_cell_molecules()
        # Not splitting up self.molecule and recombining any more
        #        use a list of unique molecules
        #        print "Combining molecules into asymmetric unit"
        #        for other_molecule in self.molecule_list[1:]:
        #            self.molecule = self.molecule.combine(other_molecule)
        self.unique_molecule_list = []
        for mol in self.molecule_list:
            self.unique_molecule_list.append(copy.deepcopy(mol))
        if self.zerr:
            self.zerr = (
                " "
                + str(len(self.spacegroup.operations))
                + " "
                + " ".join(self.zerr.split()[1:])
            )
        self.symm = []
        self.latt = "-1"

    def expand_to_primitive_unit_cell(self):
        """ Return a P1 unit cell with ZERR = no of formula units.
        See also expand_to_full_unit_cell().
        """
        crystal_logger.info("Generating primitive unit cell.")
        self.generate_symmetry_operation()
        crystal_logger.debug("Generate unit cell molecules")
        self.generate_unit_cell_molecules()
        self.unique_molecule_list = []
        for mol in self.molecule_list:
            self.unique_molecule_list.append(copy.deepcopy(mol))
        self.calculate_z()
        if self.zerr is not None:
            self.zerr = str(self.z) + " " + " ".join(self.zerr.split()[1:])
        else:
            self.zerr = str(self.z)
        self.symm = []
        self.latt = "-1"

    def remove_coincident_atoms(self):
        crystal_logger.info("Removing coincident atoms")
        for molecule in self.unique_molecule_list:
            molecule.remove_coincident_atoms()

    def generate_symmetry_operation(self):
        from cspy.deprecated.spacegroups import SymmetryOperation, SpaceGroup
        from cspy.deprecated.spacegroups import expand_symmetry_list, symm_op_clean_up

        self.clean_symmetry_ops()
        self.spacegroup = SpaceGroup(
            [
                SymmetryOperation(symm_op_clean_up(symm), lambda_func=None)
                for symm in expand_symmetry_list(self.symm, int(self.latt))
            ]
        )

    def optimise_exp_structure(self, gaussian_args):
        # function that optimises an experimental structure and
        # pastes this back into the cell
        a = []
        for i, mol in enumerate(self.unique_molecule_list):
            gas = copy.deepcopy(mol)
            mol.xyz_string_form_print(comment="original mol")
            gas.gaussian_calculation(
                filename="molecule_" + str(i) + ".com",
                calculate_normal_modes=False,
                force_rerun=False,
                geometry_optimisation=True,
                gaussian_args=gaussian_args,
            )
            gas.xyz_string_form_print(comment="gas mol ")
            if [x.clean_label() for x in gas.atoms] != [
                x.clean_label() for x in mol.atoms
            ]:
                gas.min_rms(mol, ndiv=8, anyatom=True)
            else:
                gas.min_rms(mol, ndiv=8)
            gas.xyz_string_form_print(comment="overlaid gas rms")
            a.append(copy.deepcopy(gas))

        crystal_logger.info("Combining molecules into asymmetric unit")
        # wipe clean list and re-populate
        # (done in 2 steps as I'm paranoid about memory and pointers)
        self.unique_molecule_list = []
        for mol in a:
            self.unique_molecule_list.append(copy.deepcopy(mol))

    def single_point(self, gaussian_args):
        # function that performs a single point energy calculation
        for i, mol in enumerate(self.unique_molecule_list):
            mol.xyz_string_form_print(comment="original mol")
            mol.gaussian_calculation(
                filename="molecule_" + str(i) + ".com",
                calculate_normal_modes=False,
                force_rerun=False,
                geometry_optimisation=False,
                gaussian_args=gaussian_args,
            )

    def generate_unit_cell_molecules(self):
        if not hasattr(self, "spacegroup"):
            self.generate_symmetry_operation()
        molecule_fractional = [[] for n in range(len(self.unique_molecule_list))]
        for idd, mole in enumerate(self.unique_molecule_list):
            for atom in mole.xyz_form(clean_labels=True):
                molecule_fractional[idd].append(
                    tuple([atom[0]])
                    + tuple(self.lattice.to_fractional([float(x) for x in atom[1:4]]))
                )
        # Create a set of molecules
        self.molecule_list = [
            Molecule()
            for n in range(
                len(self.spacegroup.operations) * len(self.unique_molecule_list)
            )
        ]
        # Create a dictionary that links a molecule to a
        # symmetry operation that created it
        self.mol_to_op = {}
        molecule_id = 0
        # Populate each molecule from and op and a asymm molecul
        for i, op in enumerate(self.spacegroup.operations):
            for molecule in molecule_fractional:
                for frac_coord in molecule:
                    xyz = self.lattice.to_cartesian(
                        op.apply_symmetry_op(frac_coord[1:4])
                    )
                    self.molecule_list[molecule_id].add_atom(Atom(frac_coord[0], xyz))
                self.mol_to_op[molecule_id] = [op.original_string_form, i]
                molecule_id += 1

        return

    def find_bonded_molecules(self):
        # create a list containing bonding information,
        # list what it is bonded to and using what translation
        bonding_info = [[n, [0.0, 0.0, 0.0]] for n in range(len(self.molecule_list))]

        for A in range(0, len(self.molecule_list) - 1, 1):
            # Store all indexes of potential bonds
            bonded_to = [bonding_info[A][0], A]
            for B in range(A + 1, len(self.molecule_list), 1):
                res = self.molecule_list[A].is_bonded_to(
                    self.molecule_list[B], self.lattice, check=True
                )
                if res[0]:
                    bonded_to += [bonding_info[B][0], B]
                    bonding_info[B][1] = res[1]
            # use the lowest index in bonded_to list to
            # label bonding_info index
            new_index = min(bonded_to)
            # Set anything found in bonded_to to the new index
            for i in range(len(bonding_info)):
                if bonding_info[i][0] in bonded_to:
                    bonding_info[i][0] = new_index
        for i in range(len(bonding_info)):
            crystal_logger.info(
                "Fragment %d bonds to molecule %s", i, str(bonding_info[i][0])
            )

        new_list = []
        done_list = []
        self.link_list = {}
        for i, n in enumerate(bonding_info):
            if n[0] in done_list:
                continue
            else:
                done_list.append(n[0])
            new_list.append(copy.deepcopy(self.molecule_list[i]))
            self.link_list[len(new_list) - 1] = [i]
            usedlist = []
            while True:
                none_found = True
                for j, m in enumerate(bonding_info[n[0] + 1 :], start=n[0] + 1):
                    if n[0] == m[0] and j not in usedlist:
                        res = new_list[-1].is_bonded_to(
                            self.molecule_list[j], self.lattice, check=True
                        )
                        if res[0]:
                            usedlist.append(j)
                            none_found = False
                            v = self.lattice.to_cartesian(res[1])
                            for a in self.molecule_list[j].atoms:
                                a.xyz = np.array(a.xyz) + np.array(v)
                            self.molecule_list[j]._min_max[0][0] += v[0]
                            self.molecule_list[j]._min_max[0][1] += v[0]
                            self.molecule_list[j]._min_max[1][0] += v[1]
                            self.molecule_list[j]._min_max[1][1] += v[1]
                            self.molecule_list[j]._min_max[2][0] += v[2]
                            self.molecule_list[j]._min_max[2][1] += v[2]
                            new_list[-1] = new_list[-1].combine(self.molecule_list[j])
                            self.link_list[len(new_list) - 1].append(j)
                if none_found:
                    break
        for mol in new_list:
            frac_c = self.lattice.to_fractional(mol.centroid())
            fc = [0, 0, 0]
            if frac_c[0] < 1e-6:
                fc[0] += 1
            if frac_c[1] < 1e-6:
                fc[1] += 1
            if frac_c[2] < 1e-6:
                fc[2] += 1
        self.asymm_full = new_list

    def operations_bond_to_asymm(self):
        from cspy.deprecated.spacegroups import symm_op_clean_up

        for idA, moleA in enumerate(self.unique_molecule_list):
            for idB, moleB in enumerate(self.asymm_full):
                if moleA.is_bonded_to(moleB, self.lattice, check=False)[0]:
                    self.unique_molecule_list[idA] = copy.deepcopy(self.asymm_full[idB])
                    break
            else:
                continue

        # get fractional co-ordinates
        molecule_fractional = []
        for idd, mole in enumerate(self.unique_molecule_list):
            molecule_fractional.append([])
            for atom in mole.xyz_form(clean_labels=True):
                molecule_fractional[idd].append(
                    [atom[0]]
                    + self.lattice.to_fractional([float(x) for x in atom[1:4]]).tolist()
                )
        # for operations
        self.symm = []
        d = {}
        for n in range(len(self.asymm_full)):
            d[n] = False
        anyremove = False
        for op in self.spacegroup.operations:
            for mol in molecule_fractional:
                no_c = not any([x[0].startswith("C") for x in mol])
                for atom in mol:
                    if atom[0].startswith("C") or no_c:
                        pass
                    else:
                        continue

                    smallmol = Molecule()

                    x = self.lattice.to_cartesian(
                        [float(x) for x in op.apply_symmetry_op(atom[1:4])]
                    )
                    smallmol.add_atom(Atom(atom[0], x))
                    #                    smallmol.xyz_string_form_print()

                    for i, m in enumerate(self.asymm_full):
                        res = smallmol.is_bonded_to(
                            m, self.lattice, fact=0.01, check=False
                        )
                        if res[0]:
                            if d[i]:
                                pass
                                anyremove = True
                                crystal_logger.info(
                                    "REMOVE: %s", symm_op_clean_up(str(op))
                                )
                            else:
                                self.symm.append(symm_op_clean_up(str(op)))
                                crystal_logger.info(
                                    "KEEP: %s", symm_op_clean_up(str(op))
                                )
                                d[i] = True
                            break
                    else:
                        continue
                    break
                else:
                    continue
                break
        self.zerr = " " + str(len(self.symm)) + " " + " ".join(self.zerr.split()[1:])
        self.symm = self.symm[1:]
        self.latt = "-1"
        return anyremove

    def remove_redundant_symmetry_operations(self):
        mollist = []
        for idA, mA in enumerate(self.unique_molecule_list):
            for idB, mB in enumerate(self.asymm_full):
                if mA.is_bonded_to(mB, self.lattice)[0]:
                    mollist += [[idA, idB]]

    def make_cluster(self, out_filename=None, expansion_level=7, min_size=25):
        from cspy.deprecated.crystal import CrystalStructure
        from cspy.deprecated.molecule import Molecule

        crystal_logger.info("make cluster")
        tmpcrys = copy.deepcopy(self)
        # need the P1 cell
        tmpcrys.expand_to_primitive_unit_cell()

        def intermolecular_distances(mol1, l1, mol2, l2, translation):
            """The next portion sets up the arrays needed to evaluate the
            overlap between the unitcell molecules and the periodic images
            atomsa is an array containing blocks of the coordinates in mol1
            with blocks of each atom of (mol2*translations) long la is the
            array containing the corresponding atom labels atomsb is an array
            containing blocks of the coordinates in mol2 with blocks of atoms
            of (translations) long lb is the array containing the corresponding
            atom labels"""

            la = np.repeat(l1, len(l2), axis=0)

            lb = np.ravel(np.tile(l2, (len(l1), 1)))

            laf = np.repeat(l1, len(l2) * len(translation), axis=0)

            lbf = np.ravel(
                np.tile(np.repeat(l2, len(translation), axis=0), (len(l1), 1))
            )

            atomsa = np.repeat(mol1, len(mol2) * len(translation), axis=0)

            atomsb = np.tile(np.repeat(mol2, len(translation), axis=0), (len(mol1), 1))

            translations = np.tile(np.matrix(translation), (len(mol2) * len(mol1), 1))
            # calculate x^2,y^2,z^2
            (x2, y2, z2) = np.split(
                np.square(np.subtract(atomsa, np.add(atomsb, translations))), 3, axis=1
            )
            # calculate the distance by r=sqrt(x^2+y^2+z^2)
            distance = np.ravel(np.sqrt(np.add(x2, np.add(y2, z2))))
            return la, lb, laf, lbf, distance, translations

        # take in the molecules 1 and 2 and a
        # translation and return a new single molecule
        def trans_mol(mole1, trans, origin):
            mole3 = Molecule()
            for at1 in mole1.atoms:
                at1.xyz = at1.xyz + (np.add(np.ravel(trans[0, :]), origin))
                mole3.add_atom(at1)
            return mole3

        mol1 = [list(tmpcrys.unique_molecule_list[0].centroid())]
        l1 = ["0"]  # reference molecule
        mol2 = []
        l2 = []  # comparison molecules
        for ia in range(len(tmpcrys.unique_molecule_list)):
            l2.append(ia)
            mol2.append(list(tmpcrys.unique_molecule_list[ia].centroid()))

        translation = np.array(
            list((tmpcrys.lattice.create_translations(expansion_level)))
        )
        la, lb, laf, lbf, distance, trans = intermolecular_distances(
            mol1, l1, mol2, l2, translation
        )  # molecule translations
        rcut = 10.0
        indices = []
        # we want a spherical cluster containing at least 50 molecules
        while len(indices) <= min_size:
            rcut += 1.0
            cutoff = rcut * np.ones(len(distance))
            indices = list(np.ravel(np.where(np.subtract(distance, cutoff) <= 0.0)))
        # new crystal for the spherical cluster
        cluster = CrystalStructure()
        cluster.titl = self.titl
        cluster.latt = "-1"
        cluster_length = 1000.0
        cluster.lattice.set_lattice_parameters(
            [cluster_length, cluster_length, cluster_length, 90.0, 90.0, 90.0]
        )  # Place cluster in large box
        origin = np.subtract(
            [cluster_length / 2.0] * 3, list(tmpcrys.unique_molecule_list[0].centroid())
        )  # set the origin of the cluster
        # create the cluster
        for i in range(len(indices)):
            mole = trans_mol(
                copy.deepcopy(tmpcrys.unique_molecule_list[lbf[indices[i]]]),
                trans[indices[i]],
                origin,
            )
            cluster.unique_molecule_list.append(mole)
        cluster.update_sfac(retyped=False)
        cluster.write_res_to_file(out_filename)
        crystal_logger.info("made cluster")

    def pbc_overlap(
        self, check_asymm=True, expand_uc=True, expansion_level=3, foreshortened=None
    ):
        """function to check if molecules within the unitcell
        overlap with those in periodic images"""
        import time

        tmp_crys = copy.deepcopy(self)
        start = time.time()
        if check_asymm:
            tmp_crys.check_asymmetric_unit()
        if expand_uc:
            tmp_crys.expand_to_primitive_unit_cell()
        # if the coordinates are foreshortened add 0.1
        # for each H in the bond
        if foreshortened:
            lengthen = 0.1
        else:
            lengthen = 0.0
        translation = np.array(
            list(
                filter(
                    lambda x: x != (0.0, 0.0, 0.0),
                    (tmp_crys.lattice.create_translations(expansion_level)),
                )
            )
        )  # Due to oblique cells we expand up to 5 cells

        def intermolecular_distances(mol1, l1, mol2, l2, translation):
            """The next portion sets up the arrays needed to evaluate
            the overlap between the unitcell molecules and the
            periodic images

            atomsa is an array containing blocks of the coordinates in mol1
                with blocks of each atom of (mol2*translations) long
            la is the array containing the corresponding atom labels

            atomsb is an array containing blocks of the coordinates in mol2
                with blocks of atoms of (translations) long
            lb is the array containing the corresponding atom labels"""

            la = np.repeat(l1, len(l2), axis=0)

            lb = np.ravel(np.tile(l2, (len(l1), 1)))

            laf = np.repeat(l1, len(l2) * len(translation), axis=0)

            lbf = np.ravel(
                np.tile(np.repeat(l2, len(translation), axis=0), (len(l1), 1))
            )

            atomsa = np.repeat(mol1, len(mol2) * len(translation), axis=0)

            atomsb = np.tile(np.repeat(mol2, len(translation), axis=0), (len(mol1), 1))

            translations = np.tile(np.matrix(translation), (len(mol2) * len(mol1), 1))
            # calculate x^2,y^2,z^2
            (x2, y2, z2) = np.split(
                np.square(np.subtract(atomsa, np.add(atomsb, translations))), 3, axis=1
            )

            # calculate the distance by r=sqrt(x^2+y^2+z^2)
            distance = np.ravel(np.sqrt(np.add(x2, np.add(y2, z2))))
            del x2, y2, z2, translations, atomsa, atomsb
            return la, lb, laf, lbf, distance

        def shortest_contacts(laf, lbf, distances):
            # full list of labels and the distances
            min_dist = np.amin(distances)
            max_diff = list(
                np.ravel(np.where((distances == min_dist)))
            )  # if molecules are bonded diff > 0.0
            short_contacts = {}
            for i in range(len(max_diff)):
                btype = " ".join(sorted([laf[max_diff[i]], lbf[max_diff[i]]]))
                if btype not in short_contacts:
                    short_contacts[btype] = distance[max_diff[i]]
            return short_contacts

        # unitcell overlap
        unitcell = np.array(
            list(
                filter(
                    lambda x: x == (0.0, 0.0, 0.0),
                    (tmp_crys.lattice.create_translations(1)),
                )
            )
        )
        # Form the initial arrays
        for a in range(len(tmp_crys.unique_molecule_list)):
            l1 = []
            l2 = []
            mol1 = []
            mol2 = []
            mola = tmp_crys.unique_molecule_list[a]
            atom_type_set = []  # Get the atom-types
            atom_type_set += mola.atom_types
            for atoma in mola.atoms:
                l1.append(atoma.cl)  # Get atom labels, for typing
                mol1.append(list(atoma.xyz))  # Get coordinates

            for b in range(len(tmp_crys.unique_molecule_list)):
                if a != b:
                    molb = tmp_crys.unique_molecule_list[b]
                    atom_type_set += molb.atom_types
                    for atomb in molb.atoms:
                        l2.append(atomb.cl)
                        mol2.append(list(atomb.xyz))

            la, lb, laf, lbf, distance = intermolecular_distances(
                mol1, l1, mol2, l2, unitcell
            )

        atom_type_set = set(atom_type_set)

        mol = Molecule()
        bonds = {}
        for at1 in atom_type_set:
            for at2 in atom_type_set:
                hcount = 0.0
                if at1 == "H":
                    hcount += 1.0
                if at2 == "H":
                    hcount += 1.0
                # to match fragment geometry we use a factor of
                # 1.1 in determining if things are bonded
                bonds[(at1, at2)] = (lengthen * hcount) + float(
                    mol.std_bond_length(at1, at2)
                )

        min_dist = np.amin(distance)
        if min_dist < bonds[min(bonds, key=bonds.get)]:
            short = shortest_contacts(laf, lbf, distance)
            crystal_logger.debug(
                "UC Overlap: shortest intermolecular " "distance %s (in %s seconds)",
                min_dist,
                time.time() - start,
            )
            del translation, bonds, la, lb, laf, lbf
            return True, short

        # pbc-overlap
        l1 = []
        l2 = []
        mol1 = []
        mol2 = []
        # Form the initial arrays
        atom_type_set = []  # Get the atom-types
        for mola in tmp_crys.unique_molecule_list:
            atom_type_set += mola.atom_types
            for atoma in mola.atoms:
                l1.append(atoma.cl)  # Get atom labels, for typing
                mol1.append(list(atoma.xyz))  # Get coordinates

        for molb in tmp_crys.unique_molecule_list:
            atom_type_set += molb.atom_types
            for atomb in molb.atoms:
                l2.append(atomb.cl)
                mol2.append(list(atomb.xyz))

        la, lb, laf, lbf, distance = intermolecular_distances(
            mol1, l1, mol2, l2, translation
        )
        atom_type_set = set(atom_type_set)

        mol = Molecule()
        bonds = {}
        for at1 in atom_type_set:
            for at2 in atom_type_set:
                hcount = 0.0
                if at1 == "H":
                    hcount += 1.0
                if at2 == "H":
                    hcount += 1.0
                # to match fragment geometry we use a
                # factor of 1.1 in determining if things are bonded
                bonds[(at1, at2)] = (lengthen * hcount) + float(
                    mol.std_bond_length(at1, at2)
                )

        min_dist = np.amin(distance)
        crystal_logger.debug("min_dist: %s", min_dist)
        if min_dist < bonds[min(bonds, key=bonds.get)]:
            short = shortest_contacts(laf, lbf, distance)
            crystal_logger.debug(
                "PBC Overlap: shortest intermolecular distance " "%s (in %s seconds)",
                min_dist,
                time.time() - start,
            )
            return True, short
            # Below will allow us to check if things are bonded
            # but is time consuming
        elif min_dist > bonds[max(bonds, key=bonds.get)]:
            short = shortest_contacts(laf, lbf, distance)
            crystal_logger.debug(
                "No PBC Overlap: shortest intermolecular "
                "distance %s (in %s seconds)",
                min_dist,
                time.time() - start,
            )
            return False, short
        else:
            # set to large positive as it must be a float
            # (for time) but will throw out an error if
            # a mistake is detected
            cutoff = 100.0 * np.ones(len(la))

            for at1 in atom_type_set:
                for at2 in atom_type_set:
                    # check the bond combinations
                    cutoff[np.where(np.logical_and(la == at1, lb == at2))] = float(
                        bonds[(at1, at2)]
                    )

            cutoff = np.repeat(
                cutoff, len(translation), axis=0
            )  # replicates the cutoff list for translations
            # if molecules are bonded diff > 0.0
            diff = np.subtract(cutoff, distance)

            max_diff = list(
                np.ravel(np.where((diff == np.amax(np.subtract(cutoff, distance)))))
            )  # if molecules are bonded diff > 0.0
            if diff[max_diff][0] > 10.0:
                crystal_logger.info("Error in crystal: pbc_overlap- check bond types")
            elif diff[max_diff][0] > 0.0:
                overlap = True
                short = {}
                for i in range(len(max_diff)):
                    btype = " ".join(sorted([laf[max_diff[i]], lbf[max_diff[i]]]))
                    if btype not in short:
                        short[btype] = distance[max_diff[i]]
                crystal_logger.debug(
                    "PBC Overlap: molecules overlap by %s" " (in %s seconds)",
                    diff[max_diff][0],
                    time.time() - start,
                )
            else:
                short = shortest_contacts(laf, lbf, distance)
                overlap = False
                crystal_logger.debug(
                    "No PBC Overlap: molecules do not " "overlap by %s (in %s seconds)",
                    diff[max_diff][0],
                    time.time() - start,
                )
            return overlap, short

    def res_list_form(self, factor=1.0, retyped=False):
        res_list = []
        res_list.append("TITL {} {}".format(self.name, self.titl))

        if factor != 1.0:
            cs = self.cell.split()
            cs[1:4] = map(lambda x: str(float(x) * factor), cs[1:4])
            res_list.append("CELL " + " ".join(cs))
            self.lattice.expand_lattice(factor)
        else:
            res_list.append(
                "CELL {} {} {} {} {} {} {}".format(
                    self.wavelength, *self.lattice.lattice_parameters
                )
            )

        if self.zerr:
            res_list.append("ZERR {}".format(self.zerr))
        res_list.append("LATT {}".format(self.latt))
        from cspy.deprecated.spacegroups import SymmetryOperation

        for sym in self.symm:
            sym_c = SymmetryOperation(sym, None)
            if sym.replace(" ", "").lower().replace("+", "") != "x,y,z":
                res_list += ["SYMM " + sym_c.string_fraction_form()]
        if retyped:
            sfac = self.sfac_typed
        else:
            sfac = self.sfac
        res_list += ["SFAC " + sfac]
        sfac_dict = {}
        label_index = {}
        for value, key in enumerate(sfac.strip().split(), start=1):
            sfac_dict[key] = value
            label_index[key] = 0
        if self.unit:
            res_list += ["UNIT " + self.unit]
        if self.fvar:
            res_list += ["FVAR " + self.fvar]
        self.atom_labels = []
        for molecule in self.unique_molecule_list:
            if retyped:
                for i, atom in enumerate(molecule.atoms):
                    xyz = self.lattice.to_fractional([float(x) for x in atom.xyz])
                    sfac_index_str = str(sfac_dict[atom.get_retyped_label()])
                    label_index[atom.get_retyped_label()] += 1
                    label_index_str = str(label_index[atom.get_retyped_label()])
                    molecule.atoms[i].attributes["res_label"] = (
                        atom.get_retyped_label() + label_index_str
                    )
                    res_list += [
                        atom.get_retyped_label()
                        + " "
                        + sfac_index_str
                        + " %15.9f %15.9f %15.9f " % tuple(xyz)
                    ]
                    self.atom_labels.append(atom.get_retyped_label() + label_index_str)
            else:
                for i, atom in enumerate(molecule.xyz_form(clean_labels=True)):
                    xyz = self.lattice.to_fractional([float(x) for x in atom[1:4]])
                    test = Atom(atom[0], atom[1:4])
                    sfac_index_str = str(sfac_dict[test.clean_label()])
                    label_index[test.clean_label()] += 1
                    label_index_str = str(label_index[test.clean_label()])
                    molecule.atoms[i].attributes["res_label"] = (
                        test.clean_label() + label_index_str
                    )
                    res_list += [
                        test.clean_label()
                        + label_index_str
                        + " "
                        + sfac_index_str
                        + " %15.9f %15.9f %15.9f " % tuple(xyz)
                    ]
                    self.atom_labels.append(test.clean_label() + label_index_str)
        res_list += ["END"]
        return res_list

    def res_string_form(self, factor=1.0, retyped=False):
        return "\n".join(self.res_list_form(factor, retyped))

    def write_res_to_file(self, filename, factor=1.0, retyped=False):
        try:
            f = open(filename, "w+")
        except IOError as exc:
            crystal_logger.error(
                "Can not open " + str(filename) + " to write crystal structure"
            )
            raise CSPyException(
                "Can not open " + str(filename) + " to write crystal structure"
            )
        f.write(self.res_string_form(factor, retyped))
        f.close()
        return

    def cif_list_form(self, name="", properties={}):
        from cspy.deprecated.spacegroups import symm_op_clean_up

        cif = [""]
        if name:
            cif += ["data_" + name]
        else:
            cif += ["data_" + self.name]
        for prop_name, prop_value in properties.items():
            cif.append("_{} {}".format(prop_name, prop_value))
        cif += ["loop_"]
        cif += ["_symmetry_equiv_pos_site_id"]
        cif += ["_symmetry_equiv_pos_as_xyz"]
        for i, op in enumerate(self.symmetry_operations(), 1):
            cif += [str(i) + " " + op.string_clean_form().replace(" ", "")]
        cif += ["_cell_length_a " + str(self.lattice.lattice_parameters[0])]
        cif += ["_cell_length_b " + str(self.lattice.lattice_parameters[1])]
        cif += ["_cell_length_c " + str(self.lattice.lattice_parameters[2])]
        cif += ["_cell_angle_alpha " + str(self.lattice.lattice_parameters[3])]
        cif += ["_cell_angle_beta " + str(self.lattice.lattice_parameters[4])]
        cif += ["_cell_angle_gamma " + str(self.lattice.lattice_parameters[5])]
        cif += ["_diffrn_radiation_wavelength " + str(0.7)]
        #        cif += ["_cell_volume"]
        cif += ["loop_"]
        cif += ["_atom_site_label"]
        cif += ["_atom_site_type_symbol"]
        cif += ["_atom_site_fract_x"]
        cif += ["_atom_site_fract_y"]
        cif += ["_atom_site_fract_z"]
        for molecule in self.unique_molecule_list:
            for atom in molecule.xyz_form(clean_labels=False):
                xyz = self.lattice.to_fractional([float(x) for x in atom[1:4]])
                test = Atom(atom[0], atom[1:4])
                cif += [
                    test.label
                    + " "
                    + test.clean_label()
                    + " %15.9f %15.9f %15.9f " % tuple(xyz)
                ]
        cif += [""]
        cif += ["#END"]
        return cif

    def cif_string_form(self, name="", properties={}):
        return "\n".join(self.cif_list_form(name, properties))

    def write_cif_to_file(self, filename, name=""):
        try:
            with open(filename, "w+") as f:
                f.write(self.cif_string_form(name))
        except IOError as exc:
            crystal_logger.error("Can not open %s to write crystal structure", filename)
            raise CSPyException(
                "Can not open " + str(filename) + " to write crystal structure"
            )

    def calc_void(self, probe_radius=1.2, grid=0.2):
        """Runs PLATON VOID calculation on the structure."""
        from subprocess import call
        from cspy.executable.locations import PLATON_EXEC

        crystal_logger.info("Performing void calculation with PLATON...")
        self.clean_symmetry_ops()
        plat = self.res_list_form()
        plat_cmd = "CALC VOID PROBE {} GRID {}\nEXIT"
        plat[-1] = plat_cmd.format(probe_radius, grid)
        f = open(self.name + "_void.res", "w+")
        f.write("\n".join(plat))
        f.close()
        call(
            PLATON_EXEC + " -o " + str(self.name) + "_void.res > /dev/null", shell=True
        )

    def void_centroids(self):
        """Returns a list of 3-lists with coordinates for void centroids"""
        import os

        def get_void_centroids_from_lis():
            f = open(self.name + "_void.lis", "r")
            lis = f.read().split("\n")
            f.close()
            # see if there were any voids.
            clis = []
            for line in lis:
                if ("Page" not in line) and (not line.startswith("=")):
                    clis.append(line)
            void_count = 0
            coords_list = []  # Initialise empty list to store coordinates
            coords = []
            void = 0
            for line in range(len(clis)):
                if clis[line].startswith("Area #GridPoint VolPerc"):
                    void = 1
                    offset = 2
                while void:
                    if clis[line + offset].startswith(" " + str(void) + " "):
                        # Need to store the coords in some way.
                        coords = [
                            clis[line + offset][35:41],
                            clis[line + offset][41:47],
                            clis[line + offset][47:53],
                        ]
                        coords_list.append(coords)
                        offset += 3
                        void_count += 1
                        void += 1
                    else:
                        void = 0

            return coords_list

        if os.path.exists(self.name + "_void.lis"):
            return get_void_centroids_from_lis()
        else:
            self.calc_void()
            return get_void_centroids_from_lis()

    def void_eigenvectors(self):
        """Returns a list of 3-lists with coordinates for void centroids"""
        import os

        def get_void_eigenvectors_from_lis():
            f = open(self.name + "_void.lis", "r")
            lis = f.read().split("\n")
            f.close()
            # see if there were any voids.
            clis = []
            for line in lis:
                if ("Page" not in line) and (not line.startswith("=")):
                    clis.append(line)
            void_count = 0
            eigen_list = []  # Initialise empty list to store eigenvectors
            void = 0
            for line in range(len(clis)):
                if clis[line].startswith("Area #GridPoint VolPerc"):
                    void = 1
                    offset = 2
                while void:
                    if clis[line + offset].startswith(" " + str(void) + " "):
                        eigen = [
                            [
                                float(clis[line + offset][56:62]),
                                float(clis[line + offset][62:68]),
                                float(clis[line + offset][68:74]),
                            ],
                            [
                                float(clis[line + offset + 1][56:62]),
                                float(clis[line + offset + 1][62:68]),
                                float(clis[line + offset + 1][68:74]),
                            ],
                            [
                                float(clis[line + offset + 2][56:62]),
                                float(clis[line + offset + 2][62:68]),
                                float(clis[line + offset + 2][68:74]),
                            ],
                        ]
                        eigen_list.append(eigen)
                        offset += 3
                        void_count += 1
                        void += 1
                    else:
                        void = 0

            return eigen_list

        if os.path.exists(self.name + "_void.lis"):
            return get_void_eigenvectors_from_lis()
        else:
            self.calc_void()
            return get_void_eigenvectors_from_lis()

    def void_volumes(self):
        """Returns a list of integers;
        the volume of the voids in cubic Angstrom."""
        # Currently can't calculate void volumes to greater precision.
        # print "This is void_volumes()"
        import os

        def get_void_volumes_from_lis():
            f = open(self.name + "_void.lis", "r")
            lis = f.read().split("\n")
            f.close()
            # see if there were any voids.
            clis = []
            for line in lis:
                if ("Page" not in line) and (not line.startswith("=")):
                    clis.append(line)
            void_count = 0
            vol = []
            vol_list = []
            coords_list = []  # Initialise empty list to store coordinates
            coords = []
            for line in range(len(clis)):
                if clis[line].startswith("Area #GridPoint VolPerc"):
                    void = 1
                    offset = 2
                    while void:
                        if clis[line + offset].startswith(" " + str(void) + " "):
                            # Need to store the coords in some way.
                            vol = int(clis[line + offset][20:26])
                            coords = [
                                clis[line + offset][35:41],
                                clis[line + offset][41:47],
                                clis[line + offset][47:53],
                            ]
                            coords_list.append(coords)
                            vol_list.append(vol)
                            offset += 3
                            void_count += 1
                            void += 1
                        else:
                            void = 0

            return vol_list

        if os.path.exists(self.name + "_void.lis"):
            return get_void_volumes_from_lis()
        else:
            self.calc_void()
            return get_void_volumes_from_lis()

    def overlay_other_molecule(self, other, rmsd_tolerance=0.1):
        """
        This is one of the subroutines which has only been written for one
        unique molecule. Extending it is possible, but may need things moved?
        """
        # need to find a inverted cell
        self.generate_unit_cell_molecules()
        mol_found = False
        for mol in self.molecule_list:
            # Note this subroutine really assumes one molecule
            mol.calculate_moments_inertia()
            other_mol = other.unique_molecule_list[0]
            other_mol.set_aligned_along_axes_at_origin(
                axes=mol.moments_inertia_eig_vecs
            )
            other_mol.min_rms(mol, ndiv=4, anyatom=True)
            rms_difference = other.unique_molecule_list[0].rms_difference(
                mol, anyatom=True
            )
            if rms_difference < rmsd_tolerance:
                self.unique_molecule_list = other.unique_molecule_list
                mol_found = True
                break

        if not mol_found:
            crystal_logger.info("RMSD COULD NOT BE MINIMISED")
            crystal_logger.info("Self res file:")
            crystal_logger.info(self.res_string_form())
            crystal_logger.info("Other res file:")
            crystal_logger.info(other.res_string_form())
            # Only using first item in lists- generalize this when needed
            other_mol = other.unique_molecule_list[0]
            self.unique_molecule_list[0].calculate_moments_inertia()
            other_mol.set_aligned_along_axes_at_origin(
                axes=self.unique_molecule_list[0].moments_inertia_eig_vecs
            )
            other_mol.min_rms(self.unique_molecule_list[0], ndiv=4, anyatom=True)
            crystal_logger.info(
                "RMSD to asymmetric unit: %s",
                other_mol.rms_difference(self.unique_molecule_list[0], anyatom=True),
            )
            other_mol.invert()
            other_mol.set_aligned_along_axes_at_origin(
                axes=self.unique_molecule_list[0].moments_inertia_eig_vecs
            )
            other_mol.min_rms(self.unique_molecule_list[0], ndiv=4, anyatom=True)
            crystal_logger.info(
                "RMSD to inversion of asymmetric unit: %s",
                other_mol.rms_difference(self.unique_molecule_list[0], anyatom=True),
            )
            raise CSPyException(
                "RMSD could not be minimised to any molecule" "in the reduced cell"
            )

    def list_of_zp1_crystals(self):
        # Takes an object of crystal class and returns a
        # list of Z'=1 crystals.
        # Used to split Z'>1 crystals into a set of Z'=1 crystals.
        # This was edited by dhc on 220114 -
        # Jonas feels that it may be old code, but left in in case
        list_of_crystals = [
            copy.deepcopy(self) for _ in range(len(self.unique_molecule_list))
        ]  # make a list of (identical) copies
        for i in range(len(self.unique_molecule_list)):
            list_of_crystals[i].unique_molecule_list = []
            # replace new assym unit with only one molecule
            # from orig assym unit
            list_of_crystals[i].unique_molecule_list.append(
                copy.deepcopy(self.unique_molecule_list[i])
            )
        return list_of_crystals

    def overlay_other_molecule2(self, other, rmsd_tolerance=0.1):
        # Function that takes two objects of crystal class
        # and tries to overlay the unit cells.
        # This works for Z'>1.
        n_self = len(self.unique_molecule_list)
        n_other = len(other.unique_molecule_list)
        # Make sure there are an equal number of
        # asymmetric units in the two crystals.
        if n_self != n_other:
            if n_other == 1:
                crystal_logger.warn(
                    "The two crystals have different numbers "
                    "of molecules in the asymmetric unit. (%s, %s)",
                    n_self,
                    n_other,
                )
                crystal_logger.warn(
                    "Trying to substitute asymmetric unit from other crystal "
                    "into all molecules in self"
                )
            else:
                raise CSPyException(
                    "Incompatible Z' in overlay_other_molecule2 ({}, {})".format(
                        n_self, n_other
                    )
                )

        new_molecule_list = []
        for i, mol1 in enumerate(self.unique_molecule_list):
            for j, mol2 in enumerate(other.unique_molecule_list):
                # if j has already been matched, skip it.
                crystal_logger.info("Trying A%s with B%s", i, j)
                mol1.calculate_moments_inertia()
                mol2.set_aligned_along_axes_at_origin(
                    axes=mol1.moments_inertia_eig_vecs
                )
                mol2.min_rms(mol1, ndiv=4, anyatom=True)
                rms_difference = mol2.rms_difference(mol1, anyatom=True)
                if rms_difference < rmsd_tolerance:
                    self.unique_molecule_list = other.unique_molecule_list
                    crystal_logger.info("Matched A%s with B%s", i, j)
                    new_molecule_list.append(copy.deepcopy(mol2))
                    break
            else:
                crystal_logger.error(
                    "Could not match structure A%d in " "with any of B", i
                )

        # Now rebuild self by combining the Z'=1 unit cells again
        del self.unique_molecule_list
        self.unique_molecule_list = new_molecule_list

    def overlay_other_molecule3(self, other, rmsd_tolerance=0.1):
        """ Function that takes two objects of crystal class
        and tries to overlay the unit cells.  This works for Z'>1.

        WARNING: currently the molecule overlay/rmsd is better than
        the old rmsd, but this function does not correctly produce
        an overlayed crystal structure: do not use
        """
        self_zp1_list = self.list_of_zp1_crystals()
        other_zp1_list = other.list_of_zp1_crystals()
        n_self = len(self_zp1_list)
        n_other = len(other_zp1_list)

        # Make sure there are an equal number of
        # connected fragments in the asymmetric units
        # in the two crystals.
        if n_self != n_other and n_other > 1:
            crystal_logger.warn(
                "The two crystals have different number of "
                "molecules in the assymmetric unit."
                " (%d != %d)",
                n_self,
                n_other,
            )
            raise CSPyException("Cannot overlay crystals with different Z'")

        matched_asymm_molecules = {}
        for i, crys in enumerate(self_zp1_list):
            mol = crys.unique_molecule_list[0]
            matched_asymm_molecules[i] = set()
            for j in range(n_other):
                # skip if j has already been matched
                if j in matched_asymm_molecules[i]:
                    continue

                other.unique_molecule_list[0].invert()
                other_mol = other_zp1_list[j].unique_molecule_list[0]
                if mol.get_rmsd(other_mol) > rmsd_tolerance:
                    other_mol.invert()
                    if mol.get_rmsd(other_mol) < rmsd_tolerance:
                        matched_asymm_molecules[i].add(j)
                        break
                else:
                    matched_asymm_molecules[i].add(j)
                    break
            else:
                crystal_logger.warn(
                    "No match for structure %d within rmsd=%2.1f", i, rmsd_tolerance
                )

        # Now rebuild self by combining the Z'=1 unit cells again
        self.unique_molecule_list = []

        for i, crys in enumerate(self_zp1_list):
            mol = crys.unique_molecule_list[0]
            if i in matched_asymm_molecules.keys():
                j = matched_asymm_molecules[i].pop()
                mol = other_zp1_list[j].unique_molecule_list[0]
            self.unique_molecule_list.append(copy.deepcopy(mol))

    def platon_cell(
        self,
        run_quiet=True,
        filename="plat.res",
        timeout=30.0,
        pbc=True,
        pbc_expansion_level=7,
        change_symm=False,
        substitute=False,
    ):
        from cspy.util import PopenTimeout
        from cspy.executable.locations import PLATON_EXEC
        from cspy.deprecated.space_groups_list import sg_names

        # use a copy so we can return the
        # original object if the routine fails
        plat = copy.deepcopy(self)
        plat.clean_symmetry_ops()
        plat.check_asymmetric_unit()
        if pbc:
            mol = Molecule()
            for a in range(len(plat.unique_molecule_list)):
                for b in range(len(plat.unique_molecule_list[a].atoms)):
                    mol.add_atom(plat.unique_molecule_list[a].atoms[b])
            del plat.unique_molecule_list[:]
            plat.unique_molecule_list = mol.fragment_geometry(
                lattice=plat.lattice, expansion_level=pbc_expansion_level
            )
        platon = []
        elecrys = copy.deepcopy(self)
        sg = False
        ele = {1: "C", 2: "Si", 3: "O", 4: "N", 5: "F", 6: "Cl"}
        atmmap = {}
        ats = 0
        count = 1
        countr = 1
        for a in range(len(elecrys.unique_molecule_list)):
            for b in range(len(elecrys.unique_molecule_list[a].atoms)):
                if elecrys.unique_molecule_list[a].atoms[b].cl != "C" or not substitute:
                    lab = elecrys.unique_molecule_list[a].atoms[b].cl + str(countr)
                    elecrys.unique_molecule_list[a].atoms[b].label = lab
                    atmmap[ats] = {
                        "label": lab,
                        "atype": elecrys.unique_molecule_list[a].atoms[b].cl,
                    }
                    ats += 1
                else:
                    if elecrys.unique_molecule_list[a].atoms[b].cl == "C":
                        lab = ele[count]
                        elecrys.unique_molecule_list[a].atoms[b].label = lab
                        atmmap[ats] = {
                            "label": lab + str(countr),
                            "atype": elecrys.unique_molecule_list[a].atoms[b].cl,
                        }
                        elecrys.unique_molecule_list[a].atoms[b].cl = lab
                    ats += 1
                    if count < len(ele):
                        count += 1
                    else:
                        count = 1
                countr += 1
        elecrys.update_sfac()
        elecrys.write_res_to_file("tmp" + filename)
        labels = CrystalStructure("tmp" + filename)
        for i in range(len(atmmap)):
            for a in range(len(labels.unique_molecule_list)):
                for b in range(len(labels.unique_molecule_list[a].atoms)):
                    if labels.unique_molecule_list[a].atoms[b].atom_id == i:
                        atmmap[i]["label"] = (
                            labels.unique_molecule_list[a].atoms[b].label
                        )
        platonres = elecrys.res_list_form()
        for word in platonres:
            if "SYMM" in word and (not sg) and (not change_symm):
                platon.append(
                    "SPGR " + (sg_names[self.determine_space_group()]).replace(" ", "")
                )
                platon.append(word)
                sg = True  # spacegroup set
            else:
                platon.append(word)

        platon[-1] = "CALC ADDSYMM EXACT SHELX"  # HELP"#+" EXIT"

        with open(filename, "w+") as f:
            f.write("\n".join(platon))

        crystal_logger.info("Running PLATON ADDSYMM routine")
        if not run_quiet:
            status = PopenTimeout([PLATON_EXEC, "-o", filename]).run(timeout)
        else:
            with open("/dev/null", "w") as o:
                status = PopenTimeout([PLATON_EXEC, "-o", filename], output_file=o).run(
                    timeout
                )
        if status != 0:
            crystal_logger.info(
                "Platon error, exit status not equal to 0, value=%s", status
            )
            return False
        elif status == 0:
            import os.path

            if os.path.isfile(filename.replace(".res", "_pl.res")):
                pl_file = filename.replace(".res", "_pl.res")
            else:
                pl_file = filename

            a = CrystalStructure(pl_file)

            file_list = [
                "plat.res",
                "plat.eld",
                "plat.lis",
                "platon.bin",
                "platon.def",
                "platon.pjn",
                "platon.sar",
                "platon.sum",
                "platon_new.res",
                "shelx.hkp",
                filename.replace(".res", ".lis"),
                pl_file.replace(".res", ".spf"),
                pl_file,
                filename,
                pl_file.replace(".res", ".eld"),
                pl_file.replace(".res", ".lis"),
            ]
            for f in file_list:
                try:
                    os.remove(f)
                except OSError as e:
                    pass
            # check the number of atoms matches (not always the case)
            orig_atoms = 0
            for i in range(len(plat.unique_molecule_list)):
                for j in range(len(plat.unique_molecule_list[i].atoms)):
                    orig_atoms += 1

            plat_atoms = 0
            for i in range(len(a.unique_molecule_list)):
                for j in range(len(a.unique_molecule_list[i].atoms)):
                    plat_atoms += 1
            if orig_atoms != plat_atoms:
                crystal_logger.info("Platon: inconsistent number of atoms")
                return False

            a.clean_symmetry_ops()
            molo = Molecule()
            a.check_asymmetric_unit()
            count = 0
            for i in range(len(atmmap)):
                for im in range(len(a.unique_molecule_list)):
                    for ia in range(len(a.unique_molecule_list[im].atoms)):
                        atom = a.unique_molecule_list[im].atoms[ia]
                        if atmmap[i]["label"].replace("X", "C") == atom.label:
                            count += 1
                            atom.label = atmmap[i]["atype"]
                            atom.cl = atmmap[i]["atype"]
                            atom.atom_id = i
                            molo.add_atom(atom)
            del a.unique_molecule_list[:]
            a.unique_molecule_list = molo.fragment_geometry(
                lattice=a.lattice, expansion_level=pbc_expansion_level
            )
            if len(a.unique_molecule_list) != len(plat.unique_molecule_list):
                crystal_logger.info("Platon: inconsistent number of molecules")
                return False
            a.update_sfac()
            a.write_res_to_file("ord" + str(filename))
            # overlay the original structure back onto the platoned file
            from cspy.deprecated.molecule import overlay_quaternion

            matches = [0] * len(plat.unique_molecule_list)
            for ia, mola in enumerate(a.unique_molecule_list):
                for ib, molb in enumerate(plat.unique_molecule_list):
                    na = len(a.unique_molecule_list[ia].atoms)
                    nb = len(plat.unique_molecule_list[ib].atoms)
                    if na == nb:
                        successout, inverted, rms, maxd = overlay_quaternion(
                            a.unique_molecule_list[ia],
                            plat.unique_molecule_list[ib],
                            overlay=True,
                            AllowInversion=True,
                            rmsd_tolerance=1e-2,
                            maxd_tolerance=1e-2,
                            heavy_atoms=False,
                            target="reference",
                        )
                        matches[ib] = -1
            if 0 in matches:
                crystal_logger.info("Platon: unmatched molecule during overlay")
                return False
            else:
                adict = a.__dict__
                for key in adict:
                    if key in [
                        "unique_molecule_list",
                        "name",
                        "filename",
                        "lattice",
                        "symm",
                        "latt",
                        "zerr",
                        "atom_labels",
                    ]:
                        setattr(self, key, adict[key])
        return True

    def set_radiation_source(self, source="neutron"):
        self.radiation_source = source

    def check_axis_and_potential_for_anisotropy(
        self, axis_filename=None, potential_filename=None
    ):
        axisf = open(axis_filename, "r")
        anis_atoms = []
        for line in axisf:
            if "ANIS" in line:
                while True:
                    nline = next(axisf)
                    if "END" in nline:
                        break
                    else:
                        anis_atoms.append(nline.strip())
                    next(axisf)
                    next(axisf)
                break
        from cspy.deprecated.potentials import W99Potential

        mypot = W99Potential()
        mypot.import_from_file(potential_filename)
        # Get NEIGHCRYS labelling scheme
        fort21 = open("fort.21", "r")
        for line in fort21:
            if "Equivalent basis atoms" in line:
                for _ in range(4):
                    nline = next(fort21)
                i = -1
                last_input_name = ""
                while True:
                    nline = next(fort21).strip()
                    if nline == "":
                        break
                    snline = nline.split()
                    neighcrys_name = snline[2]
                    input_name = snline[3]
                    if last_input_name != input_name:
                        find_first_inversion = True
                        i += 1
                        for im, molecule in enumerate(self.unique_molecule_list):
                            for i in range(molecule.num_atoms()):
                                if (
                                    molecule.atoms[i].attributes["res_label"]
                                    == input_name
                                ):
                                    break
                            atom = self.unique_molecule_list[im].atoms[i]
                            atom.attributes["neighcrys_label"] = neighcrys_name
                    elif snline[5] == "T" and find_first_inversion:
                        find_first_inversion = False
                        for im, molecule in enumerate(self.unique_molecule_list):
                            atom = self.unique_molecule_list[im].atoms[i]
                            atom.attributes[
                                "neighcrys_inversion_label"
                            ] = neighcrys_name
                    last_input_name = input_name[:]

                break
        # now check if we have anis in our
        # potential file for every atoms we have
        for molecule in self.unique_molecule_list:
            for atom in molecule.atoms:
                short_label = atom.attributes["neighcrys_label"][:4]
                long_label = atom.attributes["neighcrys_label"]
                inversion_long_label = atom.attributes.get("neighcrys_inversion_label")
                pp = mypot.w99_pots.get((short_label, short_label))
                # if short_label is in the potential file
                if pp:
                    # and it has anisotropic axis
                    if pp["S01"][0] or pp["S02"][0]:
                        # ensure that the axis file also
                        # contains anisotropic axis
                        if [label == long_label[:10] for label in anis_atoms]:
                            pass
                        else:
                            crystal_logger.error(
                                "%s has anisotropic potential but " "NO axis defined",
                                str(long_label),
                            )
                            raise CSPyException(
                                str(long_label) + " has anisotropic potential and but "
                                "NO axis defined"
                            )
                        if inversion_long_label:
                            # check that the inversion centre has
                            # also been defined
                            if [
                                label == inversion_long_label[:10]
                                for label in anis_atoms
                            ]:
                                pass
                            else:
                                crystal_logger.error(
                                    "%s has anisotropic potential but "
                                    "NO axis defined",
                                    inversion_long_label[:10],
                                )
                                raise CSPyException(
                                    str(inversion_long_label[:10])
                                    + " has anisotropic potential and but "
                                    "NO axis defined"
                                )
                else:
                    crystal_logger.error(
                        "Atom type '%s' does not exist " "in supplied potential: ",
                        short_label,
                    )
                    # this raise should probably come after
                    # checking all atom types
                    raise CSPyException(
                        "Atom type does not exist in supplied "
                        "potential " + short_label + " " + short_label
                    )

        for label in anis_atoms:
            short_label = label[:4]
            pp = mypot.w99_pots.get((short_label, short_label))
            # if short_label is in the potential file
            if pp:
                # and it has anisotropic axis
                if not pp["S01"][0] and not pp["S02"][0]:
                    crystal_logger.error(
                        "An atom (%s) has been defined as "
                        "anisotropic in the axis file, "
                        "but there is no anisotropic term "
                        "in the potential file ",
                        label,
                    )
                    raise CSPyException(
                        "An atom has been defined as "
                        "anisotropic in the axis file, "
                        "but there is no anisotropic term "
                        "in the potential file " + str(short_label)
                    )
            else:
                crystal_logger.error(
                    "Atom type '%s' does not exist " "in supplied potential: ",
                    short_label,
                )
                # this raise should probably come after
                # checking all atom types
                raise CSPyException(
                    "Atom type does not exist in supplied "
                    "potential " + short_label + " " + short_label
                )

    def add_dummy_atoms(self):
        for i in range(len(self.unique_molecule_list)):
            self.unique_molecule_list[i] = self.unique_molecule_list[
                i
            ].add_dummy_atoms()
        self.update_sfac()

    def energy_minimize(
        self,
        method="default",
        run_dir=".",
        neighcrys_args={},
        gaussian_args={},
        gdma_args={},
        dmacrys_args={},
    ):
        # standard modules
        import time
        import glob
        import shutil
        import zipfile

        # our modules
        from cspy.deprecated.neighcrys_utils import (
            NeighCrysInput,
            create_geom_files,
            create_com_files_from_geom_files,
            run_gdma_neighcrys,
            make_cutoff_file,
            neighcrys_parse_args,
        )
        from cspy.deprecated.dmacrys_utils import (
            run_dmacrys,
            get_dmacrys_energy,
            dmacrys_parse_args,
            get_dmacrys_minimisation,
            get_dmacrys_negative_rep,
        )
        from cspy.deprecated.gaussian_utils import gaussian_parse_args
        from cspy.deprecated.gdma_utils import run_gdma_for_ang, gdma_parse_args
        from cspy.deprecated.file_utils import ensure_path_exists, is_file_readable
        from cspy.deprecated.file_utils import word_replace_in_file
        from cspy.deprecated.cspy_utils import run_gaussian_all_unique_com_files
        from cspy.deprecated.cspy_exceptions import CSPyException

        # This is obsolete
        from cspy.deprecated.cspy_utils import run_gaussian_all_com_files
        from cspy.deprecated.csp_data import CSPData

        # raise exception here for testing of exit/submission JEC
        # Overide default arguments with anything that is passed in
        # This is a little dangerous because it updates the
        # dictionary passed in
        neighcrys_default_args = vars(neighcrys_parse_args([""]))
        neighcrys_default_args.update(neighcrys_args)
        neighcrys_args = neighcrys_default_args

        # This is a little dangerous because it updates
        # the dictionary passed in
        gaussian_default_args = vars(gaussian_parse_args([""]))
        gaussian_default_args.update(gaussian_args)
        gaussian_args = gaussian_default_args
        gaussian_args.update({"gaussian_cleanup": True})

        # This is a little dangerous because it updates the
        # dictionary passed in
        dmacrys_default_args = vars(dmacrys_parse_args([""]))
        dmacrys_default_args.update(dmacrys_args)
        dmacrys_args = dmacrys_default_args

        if dmacrys_args["profiling"]:
            from cspy.executable.locations import use_profiling_executables

            use_profiling_executables()

        # This is a little dangerous because it updates the
        # dictionary passed in
        gdma_default_args = vars(gdma_parse_args([""]))
        gdma_default_args.update(gdma_args)
        gdma_args = gdma_default_args

        start = time.time()

        self.data_summary = CSPData()
        # This file will be pickled and used for clustering and analysis
        self.data_summary.set_name(self.name)

        self.data_summary.set_initial_res_string(
            " ".join(self.res_string_form().replace("\n", "newline").split()).replace(
                "newline", "\n"
            )
        )

        if neighcrys_args["dummy_atoms"]:
            self.add_dummy_atoms()

        if self.zerr:
            self.data_summary.set_z(int(self.zerr.split()[0]))
        else:
            zprime = 1  # NOT ALWAYS, CHANGE THIS LATER
            z = len(self.symm) + 1
            if abs(int(self.latt)) == 2:
                z *= 2
            if abs(int(self.latt)) == 3:
                z *= 3
            if abs(int(self.latt)) == 4:
                z *= 4
            if abs(int(self.latt)) == 5:
                z *= 2
            if abs(int(self.latt)) == 6:
                z *= 2
            if abs(int(self.latt)) == 7:
                z *= 2
            if int(self.latt) > 0:
                z = z * 2
            self.data_summary.set_z(z * zprime)

        if method == "default":
            # This method creates a directory for the calculations
            # to be performed in.
            struct_dir = os.getcwd() + "/" + run_dir + "/" + self.name
            struct_full_filename = struct_dir + "/" + self.filename
            crystal_logger.info("Energy minimisation for %s", self.name)
            crystal_logger.debug("In folder: %s", struct_dir)
            crystal_logger.debug("Radiation source : %s", self.radiation_source)

            # Try to unpack a zip file
            if is_file_readable(self.name + ".zip") and dmacrys_args["zip_unpack"] == 1:
                crystal_logger.info("Checking for valid zipfile")
                try:
                    with zipfile.ZipFile(self.name + ".zip", "r") as z:
                        for f in z.infolist():
                            if not is_file_readable(f.filename):
                                z.extract(f)
                        crystal_logger.info("Zipfile valid")
                        z.close()
                except zipfile.BadZipfile as exc:
                    crystal_logger.warning("Zipfile possibly corrupt")
            # Moving to folder
            crystal_logger.debug("Moving to folder : " + str(struct_dir))
            ensure_path_exists(struct_dir)
            originaldir = os.getcwd()
            os.chdir(struct_dir)
            if len(self.filename) > 40:
                try:
                    os.remove("s.res")
                except OSError as e:
                    pass
                os.symlink(self.filename, "s.res")
                self.filename = "s.res"

            try:
                # Write .res file to run folder
                crystal_logger.debug("Create .res file in folder : " + str(struct_dir))
                if dmacrys_args["platon_unit_cell"]:
                    self.platon_cell()
                    self.data_summary.set_initial_uc_lengths(
                        [float(x) for x in self.cell.split()[1:4]]
                    )
                    self.data_summary.set_initial_uc_angles(
                        [float(x) for x in self.cell.split()[4:7]]
                    )

                if dmacrys_args["check_z_value"]:
                    crystal_logger.info("Checking ZERR line")
                    # Calculate a new z - number of molecules in the unit cell
                    # This version will not handle co-crystals properly
                    # z = num_symm_operators * num_molecules_in_asymmunit
                    n = len(self.unique_molecule_list)
                    self.calculate_z(num_molecules_in_asymm_unit=n)
                    # Set the new zerr variable for the res file writing
                    oldzerr = self.zerr
                    if self.zerr is None:
                        self.zerr = str(self.z)
                    elif len(self.zerr.split()) == 1:
                        self.zerr = str(self.z)
                    else:
                        self.zerr = " ".join([str(self.z)] + self.zerr.split()[1:])
                    if oldzerr != self.zerr:
                        crystal_logger.warning("Changed ZERR line to " + str(self.zerr))

                crystal_logger.info(
                    "%d atoms in molecule 0", len(self.unique_molecule_list[0].atoms)
                )
                self.write_res_to_file(
                    filename=struct_full_filename,
                    factor=neighcrys_args.get("Lattice Factor", 1.0),
                )

                # Test for potentials file.
                # Check if anything is supplied from neighcrys_args
                if is_file_readable(neighcrys_args["potential_file"]):
                    crystal_logger.debug(
                        "Potential file found:" + str(neighcrys_args["potential_file"])
                    )
                else:
                    crystal_logger.error(
                        "Potential file not found:"
                        + str(neighcrys_args["potential_file"])
                    )
                    raise CSPyException(
                        "Potential file not found:" + neighcrys_args["potential_file"]
                    )
                # Run neighcrys to set up for Gaussian
                crystal_logger.info("Running Neighcrys for pre-Gaussian setup")
                # Allow user defined options from neighcrys_args
                # to override these defaults
                struct_input = NeighCrysInput(neighcrys_args)
                struct_input.set_structure_filename(self.filename)
                if neighcrys_args["axis_file"]:
                    # If the user has specified an axis file - try to use it.
                    if is_file_readable(neighcrys_args["axis_file"]):
                        struct_input.set_axis_filename(
                            neighcrys_args["axis_file"], exists="y"
                        )
                    else:
                        crystal_logger.error(
                            "Supplied axis file '%s' does not exist."
                            "Please check your options.",
                            neighcrys_args["axis_file"],
                        )
                        raise CSPyException("Axis file does not exist.")
                else:
                    # The user has not specified an axis file
                    struct_input.set_axis_filename(self.filename + ".axis", exists="n")

                if neighcrys_args["multipole_file"] is not None:
                    struct_input.set_punch_filename(
                        neighcrys_args["multipole_file"], exists="n"
                    )
                else:
                    struct_input.set_punch_filename(self.filename + ".mult", exists="n")

                # Make cutoff file
                if is_file_readable(struct_input.options["Bondlengths Filename"]):
                    crystal_logger.debug("Bondlengths file exists")
                else:
                    crystal_logger.warning(
                        "Making bondlengths file: %s",
                        struct_input.options["Bondlengths Filename"],
                    )
                    make_cutoff_file(self, struct_input.options["Bondlengths Filename"])

                # Make sure the axis file for this structure exists,
                # if not make it. Currently using neighcrys.
                if is_file_readable(str(struct_input.options["Axis Filename"])):

                    if neighcrys_args["check_anisotropy"]:
                        struct_input.options["Axis Exists"] = "y"
                        crystal_logger.info(
                            "Checking anisotropy using in file: %s",
                            struct_input.options["Axis Filename"],
                        )

                else:
                    crystal_logger.warn(
                        "Axis file %s does not exist, generating one.",
                        struct_input.options["Axis Filename"],
                    )

                    if not struct_input.options["Axis Filename"]:
                        struct_input.options["Axis Filename"] = self.filename + ".axis"

                    struct_input.make_axis_file(
                        self.filename,
                        struct_dir + "/" + struct_input.options["Axis Filename"],
                        neighcrys_args=neighcrys_args,
                    )

                    struct_input.options["Axis Exists"] = "y"
                    crystal_logger.info(
                        "Using axis file: %s", struct_input.options["Axis Filename"]
                    )

                struct_input.recalculate_input()
                struct_input.write_input_file(
                    struct_dir + "/" + self.name + ".axis.neighcrysin"
                )
                struct_input.run_neighcrys(
                    infile=struct_dir + "/" + self.name + ".axis.neighcrysin"
                )

                # Check that the potential file and axis file are compatible
                # This works as an extra check that the autogenerated axis file
                # is ok and it forces the user to use an anisotropic potential
                # for halogens.  check all terms exist in the potential file
                # and write the necessary ones to a new file r.pots
                from cspy.deprecated.neighcrys_utils import check_potential

                check_potential(
                    self, filename="r.pots", pf=neighcrys_args["potential_file"]
                )
                struct_input.options.update({"Potentials Filename": "r.pots"})

                if neighcrys_args["check_anisotropy"]:
                    self.check_axis_and_potential_for_anisotropy(
                        axis_filename=struct_input.options["Axis Filename"],
                        potential_filename=neighcrys_args["potential_file"],
                    )

                struct_input.set_radiation_source(self.radiation_source)
                mf_e = is_file_readable(struct_input.options["Punch Filename"])
                if not mf_e:
                    crystal_logger.warn(
                        "Multipole file %s NOT READABLE",
                        struct_input.options["Punch Filename"],
                    )
                smart_mf = struct_input.options["Punch Filename"].endswith(".smult")

                smart_mf_e = (
                    is_file_readable(struct_input.options["Punch Filename"])
                    and smart_mf
                )
                if mf_e and not smart_mf:
                    crystal_logger.debug(
                        "Multipole file check: %s EXISTS",
                        struct_input.options["Punch Filename"],
                    )
                    # If potentials file already exists no
                    # need to run Gaussian
                    struct_input.options["Punch Exists"] = "y"
                else:
                    if not smart_mf:
                        crystal_logger.warn(
                            "Multipole file %s DOES NOT EXIST",
                            struct_input.options["Punch Filename"],
                        )
                    struct_input.options["Punch Exists"] = "n"
                    # Run neighcrys to generate molecule
                    # geometry in local axis frame defined by axis
                    struct_input.recalculate_input()
                    struct_input.write_input_file(
                        struct_dir + "/" + self.name + ".neighcrysin"
                    )
                    struct_input.run_neighcrys(
                        infile=struct_dir + "/" + self.name + ".neighcrysin"
                    )

                    # Use the neighcrys run to generate Gaussian input files
                    crystal_logger.debug(
                        "Create geometry files for each unique molecule"
                    )
                    create_geom_files(struct_input.options["Foreshorten Hydrogens"])
                    crystal_logger.debug(
                        "Create Gaussian input files " "for each unique molecule"
                    )
                    # PJB go into this function and check for
                    # gaussian_args['chelpg']
                    create_com_files_from_geom_files(gaussian_args=gaussian_args)

                    # Run Gaussian calculations if chk files are not present
                    start = time.time()

                    if not smart_mf_e:
                        if gaussian_args["gaussian_all_molecules"]:
                            crystal_logger.info("Running Gaussian on all molecules.")
                            run_gaussian_all_com_files()
                        else:
                            crystal_logger.info("Running Gaussian on unique molecules.")
                            run_gaussian_all_unique_com_files()
                        crystal_logger.info(
                            "Finished Gaussian calculations in " "%6.1f seconds",
                            time.time() - start,
                        )
                    else:
                        crystal_logger.info("Smart MF, skipping gaussian")
                    # Extract DMA information
                    crystal_logger.debug(
                        "Extracting DMA from Gaussian output using GDMA"
                    )

                    if not mf_e:
                        if gaussian_args["chelpg"]:
                            crystal_logger.info("Reading CHELPG charges")
                            punch_file = open(
                                struct_input.options["Punch Filename"], "w+"
                            )
                            for m, molfile in enumerate(glob.glob("molecule_*.log")):
                                if m != 0 and smart_mf:
                                    punch_file.write("#ENDMOL\n")
                                partcharges = Molecule().get_partial_charges(molfile)
                                geom_name = molfile.replace(
                                    "molecule_", "geom"
                                ).replace(".log", "")

                                with open(geom_name) as g:
                                    geom_lines = g.readlines()

                                for i, line in enumerate(geom_lines):
                                    crystal_logger.info(str(line), str(partcharges[i]))
                                    columns = line.strip().split()
                                    x_coord = columns[2].strip()
                                    y_coord = columns[3].strip()
                                    z_coord = columns[4].strip()
                                    labels = columns[1].strip()
                                    punch_file.write(
                                        str(labels)
                                        + " "
                                        + str(x_coord)
                                        + " "
                                        + str(y_coord)
                                        + " "
                                        + str(z_coord)
                                        + " Rank 0 \n"
                                    )
                                    punch_file.write(str(partcharges[i]) + "\n")
                            punch_file.close()
                        else:
                            # Get defaults from gdma_parse_args and combine
                            # this with this functions gdma_args variable
                            fchk_file_list = glob.glob("./molecule*.fchk")
                            mult_limit = gdma_args["multipole_limit"]
                            mult_switch = gdma_args["multipole_switch"]
                            fh = struct_input.options["Foreshorten Hydrogens"]
                            for fchk_file in fchk_file_list:
                                dma_file = fchk_file.replace("fchk", "dma")
                                run_gdma_for_ang(
                                    limit=mult_limit,
                                    switch=mult_switch,
                                    file=fchk_file,
                                    dma_filename=dma_file,
                                    foreshorten_hydrogens=fh,
                                )
                            try:
                                os.remove(struct_input.options["Punch Filename"])
                            except OSError as e:
                                pass

                            punch_file = open(
                                struct_input.options["Punch Filename"], "a+"
                            )
                            dma_file_list = glob.glob("./molecule*.dma")
                            for d, dma_file in enumerate(dma_file_list):
                                if d != 0 and smart_mf:
                                    punch_file.write("#ENDMOL\n")

                                run_gdma_neighcrys(
                                    dma_file.replace("molecule_", "geom").replace(
                                        ".dma", ""
                                    ),
                                    dma_filename=dma_file,
                                    multipole_filename=dma_file.replace("dma", "mult"),
                                    tol=neighcrys_args["gdma_dist_tol"],
                                )
                                mult_file = open(dma_file.replace("dma", "mult"), "r")
                                shutil.copyfileobj(mult_file, punch_file)
                                mult_file.close()
                            punch_file.close()
                            crystal_logger.debug(
                                "Wrote multipoles to %s",
                                struct_input.options["Punch Filename"],
                            )
                        # if doing smart multipoles and
                        # file didnt exist before (i.e. we just made it)
                        if smart_mf:
                            with open(
                                struct_input.options["Punch Filename"]
                            ) as punch_file:
                                contents = punch_file.read().split("#ENDMOL\n")

                            inverted_mults = []
                            for multipoleset in contents:
                                inv_mult_dict = Molecule.generate_inverted_multipoles_from_str(
                                    multipoleset
                                )
                                mol_mult_str = Molecule.multipole_file_str(
                                    inv_mult_dict
                                )
                                inverted_mults.append(mol_mult_str)
                            with open(
                                struct_input.options["Punch Filename"], "a"
                            ) as punch_file:
                                punch_file.write(
                                    "#ENDMOL\n" + ("#ENDMOL\n".join(inverted_mults))
                                )

                    if smart_mf:
                        with open(struct_input.options["Punch Filename"]) as f2:
                            mols = f2.read().split("#ENDMOL")

                        for i, mol in enumerate(mols):
                            crystal_logger.debug("mol %d =\n%s", i, mol)
                            if not mol.strip():
                                continue
                            with open("smart_{}.dma".format(i), "w") as f:
                                for line in mol.strip().split("\n"):
                                    if line.strip() != "":
                                        f.write(line + "\n")
                        geom_file_list = sorted(glob.glob("geom*"))
                        sdma_file_list = sorted(glob.glob("smart*.dma"))
                        for gfile in geom_file_list:
                            dma_file = gfile.replace("geom", "mol_") + ".dma"
                            for smfile in sdma_file_list:
                                crystal_logger.info(
                                    "Checking %s against %s", gfile, smfile
                                )
                                try:
                                    run_gdma_neighcrys(
                                        gfile,
                                        dma_filename=smfile,
                                        multipole_filename=dma_file,
                                    )

                                    Path(dma_file).append_to("smart.mult")

                                    crystal_logger.info("Successful Transplant")
                                    break
                                except CSPyException as exc:
                                    pass
                                except Exception as e:
                                    crystal_logger.exception(str(e))
                            else:
                                emsg = (
                                    "No matches in smart multipole file for" " geometry"
                                )
                                crystal_logger.error(emsg)

                                raise CSPyException(emsg)
                        struct_input.options["Punch Filename"] = "smart.mult"

                    struct_input.options["Punch Exists"] = "y"

                # Run neighcrys with DMA information
                crystal_logger.debug("Running Neighcrys for post-Gaussian setup")
                struct_input.recalculate_input()
                struct_input.write_input_file(
                    struct_dir + "/" + self.name + ".neighcrysin"
                )
                struct_input.run_neighcrys(
                    infile=struct_dir + "/" + self.name + ".neighcrysin"
                )
                # clean multipole files
                dmainput_filename = self.filename + ".dmain"
                if not os.path.exists(dmainput_filename):
                    dmainput_filename_new = self.filename.rstrip(".res") + ".dmain"
                    crystal_logger.warn(
                        "Could not find dmain file: %s, trying: %s",
                        dmainput_filename,
                        dmainput_filename_new,
                    )
                    dmainput_filename = dmainput_filename_new

                if dmacrys_args.get("clean_level", 0) >= 1:
                    patterns = ("*.dma", "*.com", "geom*", "*neighcrysi*")
                    for pattern in patterns:
                        for f in glob.glob(pattern):
                            crystal_logger.debug("Removing: " + str(f))
                            os.remove(f)

                # Check for constant volume calculation
                if dmacrys_args.get("constant_volume", False):
                    word_replace_in_file(dmainput_filename, "CONP", "CONV")

                if dmacrys_args["spline_off"] or (
                    dmacrys_args["spline_on"] is not False
                ):
                    # Will override the default value
                    if dmacrys_args["spline_off"] and (
                        dmacrys_args["spline_on"] is not False
                    ):
                        crystal_logger.error(
                            "--spline_off and --spline_on present, pick one"
                        )
                        raise CSPyException(
                            "--spline_off and --spline_on present, pick one"
                        )

                    fr = open(dmainput_filename, "r+")
                    dr = fr.readlines()
                    fr.seek(0)
                    if dmacrys_args["spline_off"]:
                        spline = False
                    else:
                        spline = True
                    spline_replaced = False
                    for i in dr:
                        if "SPLI" in i:
                            if spline is False:  # remove spline
                                crystal_logger.info(
                                    "Removing spline from %s", dmainput_filename
                                )
                            # keep spline
                            elif spline and dmacrys_args["spline_on"] is None:
                                fr.write(i)
                                spline_replaced = True
                                crystal_logger.info(
                                    "Keeping default spline in %s", dmainput_filename
                                )
                            else:  # replace value of spline
                                fr.write(str(dmacrys_args["spline_on"]) + "\n")
                                spline_replaced = True
                                crystal_logger.info(
                                    "Replacing default of %s with %s in %s",
                                    i.strip(),
                                    dmacrys_args["spline_on"],
                                    dmainput_filename,
                                )
                        elif "POTE" in i:
                            if spline and not spline_replaced:
                                if dmacrys_args["spline_on"] is None:
                                    # Add default spline
                                    spline_replaced = True
                                    fr.write("SPLI 2.0 4.0\n" + i)
                                    crystal_logger.info(
                                        "Adding spline of SPLI 2.0 4.0 to %s",
                                        dmainput_filename,
                                    )
                                else:  # Add spline
                                    spline_replaced = True
                                    fr.write(str(dmacrys_args["spline_on"]) + "\n" + i)
                                crystal_logger.info(
                                    "Adding spline of %s to %s",
                                    dmacrys_args["spline_on"],
                                    dmainput_filename,
                                )
                            else:
                                fr.write(i)
                        else:
                            fr.write(i)
                    fr.truncate()
                    fr.close()

                if dmacrys_args["setup_off"] or dmacrys_args["setup_on"]:
                    # Will override the default value
                    if dmacrys_args["setup_off"] and dmacrys_args["setup_on"]:
                        crystal_logger.error(
                            "--setup_off and --setup_on present, pick one"
                        )
                        raise CSPyException(
                            "--setup_off and --setup_on present, pick one"
                        )
                    fr = open(dmainput_filename, "r+")
                    dr = fr.readlines()
                    fr.seek(0)
                    for i in dr:
                        if "PRIN" in i:
                            rstr = (
                                "MOLE "
                                +
                                # Determine what to replace
                                i.split()[(i.split().index("MOLE") + 1)]
                            )
                            if dmacrys_args["setup_off"]:
                                # No setup
                                fr.write(i.replace(rstr, "MOLE 0"))
                            if dmacrys_args["setup_on"]:
                                # Require setup
                                fr.write(i.replace(rstr, "MOLE 1"))
                        else:
                            fr.write(i)
                    fr.truncate()
                    fr.close()

                if dmacrys_args["set_real_space"]:
                    # Will override the default real space component of the
                    # direct lattice sum And set it so that it equals the
                    # repulsion-dispersion cutoff Higher-multipole summation
                    # cutoff
                    fr = open(dmainput_filename, "r+")
                    dr = fr.readlines()
                    # Default values, if ACCM not in dmain these will be used
                    ACCMAD = 1000000.0
                    RLWGT = 1.0
                    nbas = 0
                    ACCM = []
                    for i in range(0, len(dr)):
                        # Get the real-space cutoff (in lattice units,
                        # this is our target)
                        if "CUTO" in dr[i]:
                            # RLSCAL = float(dr[i].split()[1])
                            CUTPOT = float(dr[i].split()[2])
                        # Need VC so need the lattice parameters
                        if "LATT" in dr[i]:
                            L = (
                                [float(x) for x in dr[i + 1].split()]
                                + [float(x) for x in dr[i + 2].split()]
                                + [float(x) for x in dr[i + 3].split()]
                            )
                            X1 = L[1][1] * L[2][2] - L[2][1] * L[1][2]
                            X2 = L[2][1] * L[0][2] - L[0][1] * L[2][2]
                            X3 = L[0][1] * L[1][2] - L[1][1] * L[0][2]
                            # Equation dmacrys uses to calculate VC
                            # from latice.f90
                            VC = L[0][0] * X1 + L[1][0] * X2 + L[2][0] * X3
                        # These will override our default values if present
                        if "ACCM" in dr[i]:
                            if len(dr[i].split()) >= 1:
                                ACCMAD = float(dr[i].split()[1])
                            if len(dr[i].split()) >= 2:
                                RLWGT = float(dr[i].split()[2])
                            if len(dr[i].split()) >= 3:
                                # Not in use currently but
                                # for future reference
                                ACCM = dr[i].split()[3::]
                        # Need number of basis functions,
                        # can count these statements to obtain it
                        if "LEVEL" in dr[i] or "DUPL" in dr[i]:
                            nbas += 1
                    # we want CUTLAT=CUTPOT by altering RLWGT
                    DPI = 3.141592653589793  # From DMACRYS parmad.f90
                    CUTLAT = CUTPOT  # We want these distances to be equal
                    # The following is a rearrangement of the equation in
                    # parmad.f90 of dmacrys for the calculation of the direct
                    # lattice summation cutoff here, given the other parameters
                    # we want to know the value of RLWGT which gives the
                    # correct length
                    ARG = np.sqrt(np.log(ACCMAD))
                    RLWGT = (
                        np.power(((VC * VC) / float(nbas)), 1.0 / 6.0)
                        * ARG
                        / (np.sqrt(DPI) * CUTLAT)
                    )
                    # write the new information, if the ACCM
                    # statement is present, alter it, if not insert it
                    fr.seek(0)
                    ACCMREAD = False
                    for i in dr:
                        if "ACCM" in i:
                            fr.write(
                                "ACCM "
                                + str(ACCMAD)
                                + " "
                                + str(RLWGT)
                                + " "
                                + str(" ".join(ACCM))
                                + "\n"
                            )
                            ACCMREAD = True
                        elif "SCAL" in i and not ACCMREAD:
                            fr.write(
                                "ACCM "
                                + str(ACCMAD)
                                + " "
                                + str(RLWGT)
                                + " "
                                + str(" ".join(ACCM))
                                + "\n"
                                + str(i)
                            )
                        else:
                            fr.write(str(i))
                    fr.truncate()
                    fr.close()

                if dmacrys_args.get("lennard_jones", False):
                    # if potential contains lj read terms,
                    # else perform the fit
                    lj_pot = dmacrys_args["lennard_jones_potential"]
                    if lj_pot is not None:
                        from cspy.deprecated.dmacrys_utils import read_lj

                        read_lj(dmainput_filename, potential_file=lj_pot)
                    else:
                        from cspy.deprecated.dmacrys_utils import buckingham_to_lj

                        buckingham_to_lj(dmainput_filename)

                if dmacrys_args["auto_neighbour_setting"]:
                    dmacrys_args["cutm"] = math.ceil(self.max_atom_atom_distance())
                    dmacrys_args["nbur"] = self.longest_min_connectivity()
                try:
                    word_replace_in_file(
                        dmainput_filename, "CUTM", "CUTM " + str(dmacrys_args["cutm"])
                    )
                    word_replace_in_file(
                        dmainput_filename, "NBUR", "NBUR " + str(dmacrys_args["nbur"])
                    )
                except Exception as exc:
                    crystal_logger.error(
                        "Can not open %s; no dmacrys can be run", dmainput_filename
                    )
                    raise CSPyException(
                        "Can not open "
                        + str(dmainput_filename)
                        + "; no dmacrys can be run"
                    )
                # If doing exact property calculation,
                # replace STAR PLUT with STAR PROP and
                # remove NOPR line in .dmain
                if dmacrys_args["seig1"]:
                    word_replace_in_file(dmainput_filename, "NOPR", "SEIG 1\nNOPR")

                if dmacrys_args["exact_prop"]:
                    word_replace_in_file(dmainput_filename, "STAR PLUT", "STAR PROP")
                    with open(dmainput_filename) as dmainput:
                        new_input = []
                        for line in dmainput:
                            if line.strip() != "NOPR":
                                new_input.append(line)

                    with open(dmainput_filename, "w") as dmainput:
                        dmainput.writelines(new_input)

                if dmacrys_args.get("clean_level", 0) >= 4:
                    crystal_logger.info("Cleaning files before minimization")
                    for pattern in (
                        "*.dma",
                        "geom*",
                        "molecule*com",
                        "*axis",
                        "*.mult",
                        "*.res",
                        "*.pots",
                        "*.nnl",
                    ):
                        filelist = glob.glob(pattern)
                        for f in filelist:
                            crystal_logger.debug("Removing: " + str(f))
                            try:
                                os.remove(f)
                            except OSError as e:
                                pass
                # Finally we can run dmacrys to do an energy minimisation
                crystal_logger.info("Started DMACRYS energy minimisation")
                dmaoutput_filename = dmainput_filename.rstrip("dmain") + "dmaout"
                # Check to see if the dmacrys summary
                # and final structures exist
                from cspy.executable.locations import DMACRYS_EXEC

                crystal_logger.info("DMACRYS_EXEC: %s", DMACRYS_EXEC)
                assisted = dmacrys_args["assisted_convergence"]
                if is_file_readable("fort.12") and is_file_readable("fort.16"):
                    self.energy["lattice"] = get_dmacrys_energy()
                else:
                    run_dmacrys(
                        dmacrys_exec=DMACRYS_EXEC,
                        dmain=dmainput_filename,
                        dmaout=dmaoutput_filename,
                        assisted_convergence=assisted,
                        timeout=dmacrys_args["dmacrys_timeout"],
                        dmacrys_args=dmacrys_args,
                    )
                    self.energy["lattice"] = get_dmacrys_energy()
                if dmacrys_args["remove_negative_eigens"]:
                    # Adding some comment to check branching worked ok
                    removeid = get_dmacrys_negative_rep(
                        name=self.filename.replace(".res", "")
                    )
                    crystal_logger.info(str(removeid))

                    if removeid is None:
                        crystal_logger.error("No negative eigenvalues found")
                        pass
                    else:
                        struct_input.options["Symmetry Subgroup"] = str(removeid)
                        crystal_logger.info(
                            "Running Neighcrys for symmetry removal " "of subgroup %s",
                            removeid,
                        )
                        struct_input.recalculate_input()
                        struct_input.write_input_file(
                            struct_dir + "/" + self.name + ".symm.neighcrysin"
                        )
                        struct_input.run_neighcrys(
                            infile=struct_dir + "/" + self.name + ".symm.neighcrysin"
                        )
                        crystal_logger.info("Running DMACRYS with symmetry removed")
                        run_dmacrys(
                            dmacrys_exec=DMACRYS_EXEC,
                            dmain=dmainput_filename,
                            dmaout=dmaoutput_filename,
                            assisted_convergence=assisted,
                            timeout=dmacrys_args["dmacrys_timeout"],
                        )
                        self.energy["lattice"] = get_dmacrys_energy()

                crystal_logger.info("Lattice energy: %s", self.energy["lattice"])
                optimised_crystal = CrystalStructure("fort.16")

                profile_contents = None
                if dmacrys_args["profiling"]:
                    crystal_logger.info("Reading profile from gmon.out")
                    if os.path.exists("gmon.out"):
                        with open("gmon.out", "rb") as f:
                            profile_contents = f.read()
                crystal_logger.info("Setting profile_contents")
                self.data_summary.set_profile_contents(profile_contents)

                if neighcrys_args["dummy_atoms"]:
                    mols = optimised_crystal.unique_molecule_list
                    for i in range(len(mols)):
                        mols[i] = mols[i].remove_dummy_atoms()
                    optimised_crystal.update_sfac()
                opt_res_str = optimised_crystal.res_string_form()
                self.data_summary.set_final_res_string(
                    " ".join(opt_res_str.replace("\n", "newline").split()).replace(
                        "newline", "\n"
                    )
                )
                with open("fort.12") as f:
                    fort12 = f.read()
                self.data_summary.set_fort12(
                    " ".join(fort12.replace("\n", "newline").split()).replace(
                        "newline", "\n"
                    )
                )
                with open("fort.22") as f:
                    fort22 = f.read()
                self.data_summary.set_fort22(
                    " ".join(fort22.replace("\n", "newline").split()).replace(
                        "newline", "\n"
                    )
                )
                self.data_summary.set_minimisation(get_dmacrys_minimisation())
                self.data_summary.set_negative_reps(
                    get_dmacrys_negative_rep(name=self.filename.replace(".res", ""))
                )

                crystal_logger.info(
                    "Finished DMACRYS energy minimisation in %6.1f seconds",
                    time.time() - start,
                )
                self.data_summary.set_time(time.time() - start)

                if gaussian_args["external_iteration_pcm"]:
                    self.data_summary.dielectric_constant = str(
                        gaussian_args["dielectric_constant"]
                    )
                    log_files = glob.glob("molecule*log")

                    epcm_energy = []
                    for log_file in sorted(log_files):
                        with open(log_file) as logf:
                            for line in logf:
                                if "<psi(f)|   H    |psi(f)>" in line:
                                    epcm_energy.append(line.split()[-1])
                    self.data_summary.epcm_energy = epcm_energy

                # Perform clean up of files here.
                # Aggressive == delete everything
                crystal_logger.info("Cleaning files")
                if dmacrys_args.get("clean_level", 0) == 99:
                    excluded = [
                        "fort.12",
                        "fort.16",
                        struct_input.options["Punch Filename"],
                        struct_input.options["Axis Filename"],
                        struct_input.options["Bondlengths Filename"],
                    ]
                # will also delete info for invalids
                elif dmacrys_args.get("clean_level", 0) == 5:
                    excluded = []
                # will also delete info for invalids
                elif dmacrys_args.get("clean_level", 0) == 4:
                    excluded = ["fort.12", "fort.22"]
                elif dmacrys_args.get("clean_level", 0) == 3:
                    excluded = ["fort.12", "fort.22"]
                elif dmacrys_args.get("clean_level", 0) == 2:
                    excluded = [
                        "fort.12",
                        "fort.16",
                        self.filename,
                        self.name + ".res",
                        struct_input.options["Punch Filename"],
                        struct_input.options["Axis Filename"],
                        struct_input.options["Bondlengths Filename"],
                    ] + glob.glob("molecule_*.log")
                elif dmacrys_args.get("clean_level", 0) == 1:
                    excluded = [
                        "fort.12",
                        "fort.16",
                        self.filename,
                        self.name + ".res",
                        struct_input.options["Punch Filename"],
                        struct_input.options["Axis Filename"],
                        struct_input.options["Bondlengths Filename"],
                        "fort.20",
                        dmainput_filename,
                        dmaoutput_filename,
                    ] + glob.glob("molecule_*.log")
                if dmacrys_args.get("clean_level", 0) > 0:
                    filelist = [f for f in os.listdir(".") if f not in excluded]
                    for f in filelist:
                        crystal_logger.debug("Removing: " + str(f))
                        try:
                            os.remove(f)
                        except OSError as e:
                            pass
            except Exception as e:
                crystal_logger.exception("Error in crystal.energy_minimise")
            finally:
                # will also delete info for invalids
                if dmacrys_args.get("clean_level", 0) == 5:
                    excluded = ["s.res"]
                    filelist = [f for f in os.listdir(".") if f not in excluded]
                    for f in filelist:
                        crystal_logger.debug("Removing: " + str(f))
                        try:
                            os.remove(f)
                        except OSError as e:
                            pass
                # will also delete info for invalids
                if dmacrys_args.get("clean_level", 0) == 4:
                    excluded = ["fort.12", "fort.22"]
                    filelist = [f for f in os.listdir(".") if f not in excluded]
                    for f in filelist:
                        crystal_logger.debug("Removing: " + str(f))
                        os.remove(f)
                self.data_summary.save_compressed_file("csp_data.p")
                os.chdir(originaldir)
                if dmacrys_args.get("clean_level", 0) == 5:
                    try:
                        shutil.move(
                            struct_dir + "/csp_data.p",
                            originaldir + "/" + self.name + ".p",
                        )
                        shutil.rmtree("./" + self.name)
                    except Exception as exc:
                        os.system("echo Fail > " + originaldir + "/" + self.name + ".p")
                elif dmacrys_args["zip_level"] >= 1:
                    crystal_logger.info("Zipping folder")
                    try:
                        os.remove(self.name + ".zip")
                    except Exception as exc:
                        pass
                    from cspy.deprecated.zipdir import ZipDir

                    ZipDir(self.name, self.name + ".zip")
                    shutil.rmtree("./" + self.name)

        return

    def dispersion_repulsion_energy(self, potential, cutoff, energy_function):
        """This gives the energies per mole (eV if Williams- and B is in
        Angstrom) As far as I (dhc) know, this isn't much used. it only has
        been changed to work for one molecule in the cell as of 220114
        """
        from cspy.deprecated.spacegroups import (
            SpaceGroup,
            SymmetryOperation,
            expand_symmetry_list,
        )

        er_energy = 0.0
        sg_ops = [
            SymmetryOperation(symm, lambda_func=None)
            for symm in expand_symmetry_list(self.symm, int(self.latt))
        ]
        self.spacegroup = SpaceGroup(sg_ops)
        # possible bug if haven't defined symmetry operations
        self.generate_symmetry_operation()
        self.generate_unit_cell_molecules()
        unit_cell_translations = self.lattice.create_translations(4)
        self.unique_molecule_list[0].set_atom_potential_labels("_W")
        self_centroid = self.unique_molecule_list[0].centroid()
        counter = 0
        for molecule in self.molecule_list:
            molecule.set_atom_potential_labels("_W")
            molecule_centroid = molecule.centroid()
            for idv in unit_cell_translations:
                sep = np.linalg.norm(self_centroid - (molecule_centroid + idv))
                if cutoff > sep > 0.00001:
                    molecule.displace_atoms(idv)
                    mol0 = self.unique_molecule_list[0]
                    if energy_function == "exp6":
                        er_energy += mol0.intermolecular_exp6_energy(
                            molecule, potential, 15.0
                        )
                        counter += 1
                    elif energy_function == "exp_rep":
                        er_energy += mol0.intermolecular_exp_rep_energy(
                            molecule, potential, 15.0
                        )
                    else:
                        crystal_logger.info("Error - no valid energy function supplied")
                    molecule.displace_atoms(-np.array(idv))
        crystal_logger.info("included %s", counter)
        return er_energy

    def make_box_cells(self, box_para):
        """
        Another subroutine which is not often used,
        and only works for 1 unique molecule per crystal
        """
        d2r = math.pi / 180.0
        if self.lattice.lattice_type not in [1, 2, 3]:
            crystal_logger.info("Can only do boxes for first three lattice types (atm)")
            exit()
        VStar = (
            1.0
            + 2.0
            * math.cos(self.lattice.lattice_parameters[3] * d2r)
            * math.cos(self.lattice.lattice_parameters[4] * d2r)
            * math.cos(self.lattice.lattice_parameters[5] * d2r)
            - math.cos(self.lattice.lattice_parameters[3] * d2r) ** 2
            - math.cos(self.lattice.lattice_parameters[4] * d2r) ** 2
            - math.cos(self.lattice.lattice_parameters[5] * d2r) ** 2
        ) ** 0.5
        box_factor = (
            box_para
            * self.z
            * self.unique_molecule_list[0].box_lengths[0]
            * self.unique_molecule_list[0].box_lengths[1]
            * self.unique_molecule_list[0].box_lengths[2]
            / VStar
        ) ** (1.0 / 3.0)
        box_cells = []
        box_cells.append([box_factor] * 3)
        t_list1 = np.array([1.0, 1.0, 2.0]) * (0.5) ** (1.0 / 3.0) * box_factor
        t_list2 = np.array([1.0, 2.0, 2.0]) * (1.0 / 4.0) ** (1.0 / 3.0) * box_factor
        t_list3 = np.array([1.0, 1.0, 4.0]) * (1.0 / 4.0) ** (1.0 / 3.0) * box_factor

        for i in range(3):
            box_cells.append([t_list1[i], t_list1[(i + 1) % 3], t_list1[(i + 2) % 3]])
            box_cells.append([t_list2[i], t_list2[(i + 1) % 3], t_list2[(i + 2) % 3]])
            box_cells.append([t_list3[i], t_list3[(i + 1) % 3], t_list3[(i + 2) % 3]])
        crystal_logger.info("some (but not many) trials")
        return box_cells

    def _determine_unique_rotations(self):
        """
        Method to determine which set of symmetry transformations in this space
        group lead to a unique rotation. By unique rotation, we mean that given
        an arbitrary vector in space, this symmetry operation will not generate
        a vector that is parallel or antiparallel to another vector generated
        from another symmetry operator operated on this arbitrary vector. This
        is used later to calculate what the MAXIMUM possible projected length
        of a given molecular shape on three lattice vector is respectively,
        taking into account all possible unique ways that a molecule could be
        rotated by the given symmetry operations in this space group.

        :return list self.unique_rotations: A list containing all the
            symmetry operators (see spacegroups.operations)
            which are unique rotations.
        """
        # construct two random test points that do not coincide with special
        # positions in the space group.  the coordinates were given in the
        # FRACTIONAL space!
        test_pt1 = [0.2, 0.6, -0.15]
        test_pt2 = [-0.32, 0.19, 0.56]

        _transformed_vectors = []
        self.unique_rotations = []
        for op in self.spacegroup.operations:
            # Apply this symmetry operator to generate a
            # new vector in fractional space.
            test_vec1 = np.array(op.apply_symmetry_op(test_pt1)) - np.array(
                op.apply_symmetry_op(test_pt2)
            )
            # Rounding up for numerical comparison purpose.
            test_vec2 = np.round(test_vec, 4)
            # Here check if the symmetry operation lead to a new unique vector
            # in fractional space. If so, this is a unique operation,
            # otherwise, this is a redundant symmetry operation.  this is done
            # by checking if the test vector is parallel to any other vectors
            # in the list _transformed_vectors
            unique = True
            if _transformed_vectors == []:
                _transformed_vectors.append(test_vec2)
                self.unique_rotations.append(op)
            else:
                for vector in _transformed_vectors:
                    # This is a test to make sure two vectors
                    # are not parallel or antiparallel to each other.
                    if np.linalg.norm(np.cross(test_vec2, vector)) <= 1e-4:
                        unique = False
                if unique:
                    _transformed_vectors.append(test_vec2)
                    self.unique_rotations.append(op)
        return self.unique_rotations

    def shadow_onto_axis_t(self, axis, set_vertices, set_radii):
        projected_max = [
            np.vdot(axis, x) + set_radii[i] for i, x in enumerate(set_vertices)
        ]
        projected_min = [
            np.vdot(axis, x) - set_radii[i] for i, x in enumerate(set_vertices)
        ]
        return max(projected_max), min(projected_min)

    def _determine_length_bounds(self):
        """
        This method applies all the unique rotations onto the molecules to be
        packed into the crystal, and find out what's the maximum and minimum
        lengths of projection of the molecular shape(s) along each lattice
        vector.  This will help to determine how long a unit cell length needs
        to be along a particular lattice vector.

        The idea here is that if this symmetry operator leads to a unique
        position in the lattice, then we will apply this operation to the
        molecule and generate a new one. This new molecule will have some
        position in the Cartesian space. We now project all the coordinates of
        this new molecules onto the three lattice vectors, this will update
        what are the maximum and minimum value of the projected length bounds
        on these three lattice vectors. Some buffer of atomic vdw radii was
        taken into account in calculating the bounds. This will give us some
        idea on roughly how big the unit cell needs to be.

        :return list length_bounds:
        """
        _min_bound_scaling = 0.75
        _max_bound_scaling = 0.75
        _projections = [[], [], []]
        # list holding the length bounds along three lattice vector,
        # each given as [min_bound, max_bound]
        self.length_bounds = [None] * 3

        for op in self.unique_rotations:
            temp_object = self.make_cartesian_op_matrix(op)
            cartesian_op_mats = temp_object[0]
            cartesian_op_trans = temp_object[1]
            for molecule in self.unique_molecule_list:
                # get a nested list of atomic vdw radii
                vertices_vdwr = molecule.flat_atom_vdw_radii_list()
                # get a nested list of atomic xyz coordinates
                atomic_positions = molecule.flat_atom_coord_list()
                transformed_coords = list3apply_sym_list(
                    atomic_positions, cartesian_op_mats, cartesian_op_trans
                )

                for ia, axis in enumerate(self.lattice.lattice_vectors):
                    normalized_axis = axis / np.linalg.norm(axis)
                    xtemp = self.shadow_onto_axis_t(
                        normalized_axis, transformed_coords, vertices_vdwr
                    )
                    temp2 = abs(xtemp[0] - xtemp[1])
                    _projections[ia].append(temp2)

        for ia in range(3):
            self.length_bounds[ia] = LengthBounds(
                max=max([i * _max_bound_scaling for i in _projections[ia]]),
                min=min([i * _min_bound_scaling for i in _projections[ia]]),
            )
            assert self.length_bounds[ia][0] >= self.length_bounds[ia][1]
        _projections = None
        return self.length_bounds

    def _determine_unique_point_bounds(self, unique_point_bounds, dummy_sym_op=None):
        # The functionality to impose dummy symmetry may be useful, but is not
        # a expected to be a standard feature. DHC 231115
        if dummy_sym_op:
            number_completely_unique = len(self.unique_molecule_list) / 2
            unique_pts = [[], [], []]
            for op in self.spacegroup.operations:
                test_vec1 = np.array(op.apply_symmetry_op([0.2, 0.6, -0.15])) - [
                    0.2,
                    0.6,
                    -0.15,
                ]
                for axis in range(3):
                    if not any(
                        [abs(test_vec1[axis] - x) < 0.001 for x in unique_pts[axis]]
                    ):
                        unique_pts[axis].append(test_vec1[axis])
                test_vec1 = np.array(
                    list3apply_sym(
                        op.apply_symmetry_op([0.2, 0.6, -0.15]),
                        dummy_sym_op[0],
                        dummy_sym_op[1],
                    )
                ) - [0.2, 0.6, -0.15]
                for axis in range(3):
                    if not any(
                        [abs(test_vec1[axis] - x) < 0.001 for x in unique_pts[axis]]
                    ):
                        unique_pts[axis].append(test_vec1[axis])
            self.unique_pts_on_axis = [
                float(len(x)) * number_completely_unique for x in unique_pts
            ]
            return self.unique_pts_on_axis
        if unique_point_bounds:
            # In practice, this needs to be understood with the help of the
            # group symmetry diagrams in the International Table of
            # Crystallography, and we need to basically work out how many
            # unique positions were allowed along each of the three axis in
            # fractional space. The method work by constructing a vector
            # between a given point and another one in the factional coordinate
            # space generated by the symmetry operator. We then look at each of
            # the three components of this vector, and see if that component
            # had been previously generated by another symmetry operation. If
            # not, this account for a unique point, otherwise, it is not. The
            # unique point bounds will count for how many unique molecules can
            # be placed along each axis (which is number of unique points times
            # the number of unique molecules.
            unique_pts = [[], [], []]
            for op in self.spacegroup.operations:
                test_vec1 = np.array(op.apply_symmetry_op([0.2, 0.6, -0.15])) - [
                    0.2,
                    0.6,
                    -0.15,
                ]
                for axis in range(3):
                    if not any(
                        [abs(test_vec1[axis] - x) < 0.001 for x in unique_pts[axis]]
                    ):
                        unique_pts[axis].append(test_vec1[axis])

            self.unique_pts_on_axis = [
                float(len(x)) * len(self.unique_molecule_list) for x in unique_pts
            ]
        else:
            # This is called with --old_cell_bounds option switched on
            # in the structure generator
            self.unique_pts_on_axis = [float(self.mols_per_cell) for _ in range(3)]
        return self.unique_pts_on_axis

    def fit_cell_parameters_target_volume(
        self,
        rand_vec,
        target_volume,
        sd_param=0.15,
        unique_point_bounds=True,
        dummy_sym_op=None,
    ):
        # {"Triclinic":1 , "Monoclinic":2 ,
        # "Orthorhombic":3 , "Tetragonal":4 ,
        # "Trigonal":5 , "Hexagonal":6 , "Cubic":7 }

        # TODO - why there are two identical scalings?
        # include inversion?? if need little speed ups, do this
        if not self.mols_per_cell:
            # At the beginning, we need to figure out in total, how many
            # molecules will be packed into the unit cell.  which at the moment
            # is defined by number of symmetry operators times the number of
            # unique molecules to be packed into the crystal. This will work
            # provided that we do not put molecules onto special symmetry
            # positions, so it's probably best described as the maximum
            # possible number of molecules that could be packed into a given
            # crystal system. So the way calculate_molecules_unit_cell() work
            # for structure generator here, would in principle needs to be
            # different from post-processing reduced cells. This needs further
            # tidy up!
            self.calculate_molecules_unit_cell()
        # This can actually be done at the start of structure generation,
        # before parallelization step because it is not random vector specific,
        # this could avoid doing repetitive work. For the moment leave it here
        # because don't know if server-client structure generation might be
        # affected.
        self._determine_unique_rotations()
        self._determine_length_bounds()
        self._determine_unique_point_bounds(unique_point_bounds, dummy_sym_op)
        self.generate_and_set_new_cell_parameters(
            rand_vec, target_volume, sd_param, unique_point_bounds
        )

    def new_cell_parameters(
        self, rand_vec, target_volume, sd_param=0.15, unique_point_bounds=True
    ):
        from scipy.stats import norm

        ######################################
        # This block starts to figure out the unit cell length,
        # which are broken down into specific crystal systems
        ######################################
        # We need to used the precent point function from scipy. In a
        # v-dimensional normal distribution function, a ppf(p) gives a point
        # t_v such that the corresponding probability for t_v is p, where
        # 0<p<1. By default, ppf function chooses a normal distribution
        # centered at 0 with standard deviation of 1. Given a sd_param, we
        # could adjust point t_v corrsponding to a normal distribution of
        # standard deviation of sd_param.  From here we know how to draw a
        # value of propbable cell length from a given normal distribution,
        # centered on a mean value, which can be determined by the target
        # volume parameters of the unit cell.
        #
        # The basic idea is to set 0,1 or 2 lattice lengths to be [min] +
        # bound_scaling(=0.75)*random*(N*[max]-[min]) angstroms, and then
        # another 1,2 or 3 random variates to ensure that the volume of the
        # cell relates to a normal variate with a mean relating to the
        # target_volume, and standard deviation of sd_param(=0.15) *[min].
        # Again, it is the volume of the cell, not the final lengths, which are
        # normally distributed. The different lattice types are accounted for,
        # and the indices run under modulo addition to scramble the order of
        # their assignment, and hence avoid systematic bias. [max] and [min]
        # are projections of the molecular shadowed onto the axes. The N can be
        # either the number of unique fractional positions generated by the
        # space group along each axis, or just the total number of molecules-
        # this is the point which needs to be tidied up ASAP.  Only the middle
        # 98% of Gaussian distribution used.
        temp_lengths = [0.0] * 3
        if self.lattice.lattice_type == 7:
            # Handling the CUBIC lattice, so all three unit
            # cell lengths were identical.
            # The mean value of the cell length could be worked
            # out from the target volume of the crystal.
            mean0 = target_volume ** (1.0 / 3.0)
            if rand_vec.unit_cell_lengths[0] > 0.99:
                temp_lengths[0] = (
                    norm.ppf(0.99) * sd_param * self.length_bounds[0][0] + mean0
                )
            elif rand_vec.unit_cell_lengths[0] < 0.01:
                temp_lengths[0] = (
                    norm.ppf(0.01) * sd_param * self.length_bounds[0][0] + mean0
                )
            else:
                temp_lengths[0] = (
                    norm.ppf(rand_vec.unit_cell_lengths[0])
                    * sd_param
                    * self.length_bounds[0][0]
                    + mean0
                )
            if temp_lengths[0] < self.length_bounds[0][1]:
                temp_lengths[0] = self.length_bounds[0][1]
            self.lattice.set_lattice_parameters(
                [temp_lengths[0]] * 3 + self.lattice.lattice_parameters[3:6]
            )
            return
        if self.lattice.lattice_type == 4:
            # Handling the TETRAGONAL lattice.
            if (rand_vec.seed % 2) == 0:
                temp_lengths[2] = self.length_bounds[self.lattice.unique_axis][
                    1
                ] + rand_vec.unit_cell_lengths[1] * (
                    self.mols_per_cell * self.length_bounds[self.lattice.unique_axis][0]
                    - self.length_bounds[self.lattice.unique_axis][1]
                )
                mean0 = (target_volume / temp_lengths[2]) ** (1.0 / 2.0)
                if rand_vec.unit_cell_lengths[0] > 0.99:
                    temp_lengths[0] = (
                        norm.ppf(0.99)
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                elif rand_vec.unit_cell_lengths[0] < 0.01:
                    temp_lengths[0] = (
                        norm.ppf(0.01)
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                else:
                    temp_lengths[0] = (
                        norm.ppf(rand_vec.unit_cell_lengths[0])
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                if (
                    temp_lengths[0]
                    < self.length_bounds[(self.lattice.unique_axis + 1) % 3][1]
                ):
                    temp_lengths[0] = self.length_bounds[
                        (self.lattice.unique_axis + 1) % 3
                    ][1]
                ordered_lens = [0.0] * 3
                ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
                ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
                ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
                self.lattice.set_lattice_parameters(
                    ordered_lens + self.lattice.lattice_parameters[3:6]
                )
            else:
                temp_lengths[0] = (
                    self.length_bounds[(self.lattice.unique_axis + 1) % 3][1]
                    + rand_vec.unit_cell_lengths[1]
                    * (
                        self.mols_per_cell
                        * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        - self.length_bounds[(self.lattice.unique_axis + 1) % 3][1]
                    )
                ) ** 0.5
                mean0 = target_volume / (temp_lengths[0] ** 2.0)
                if rand_vec.unit_cell_lengths[0] > 0.99:
                    temp_lengths[2] = (
                        norm.ppf(0.99)
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                elif rand_vec.unit_cell_lengths[0] < 0.01:
                    temp_lengths[2] = (
                        norm.ppf(0.01)
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                else:
                    temp_lengths[2] = (
                        norm.ppf(rand_vec.unit_cell_lengths[0])
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                if temp_lengths[2] < self.length_bounds[self.lattice.unique_axis][1]:
                    temp_lengths[2] = self.length_bounds[self.lattice.unique_axis][1]
                ordered_lens = [0.0] * 3
                ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
                ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
                ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
                self.lattice.set_lattice_parameters(
                    ordered_lens + self.lattice.lattice_parameters[3:6]
                )
            return
        if self.lattice.lattice_type == 6:
            # Handling the HEXAGONAL lattice
            d2r = math.pi / 180.0
            unique_pts = [[], [], []]
            for op in self.spacegroup.operations:
                test_vec1 = np.array(op.apply_symmetry_op([0.2, 0.6, -0.15])) - (
                    0.2,
                    0.6,
                    -0.15,
                )
                for axis in range(3):
                    if not any(
                        [
                            test_vec1[axis] < x + 0.001 and test_vec1[axis] > x - 0.001
                            for x in unique_pts[axis]
                        ]
                    ):
                        unique_pts[axis].append(test_vec1[axis])
            # MOVE ALL THIS CRAP TO THE TOP- dhc 131115
            if unique_point_bounds:
                self.unique_pts_on_axis = [float(len(x)) for x in unique_pts]
            else:
                self.unique_pts_on_axis = [self.mols_per_cell for _ in range(3)]
            #            for i in range(3):
            #                if self.unique_pts_on_axis[i] == 1.0:
            #                    self.unique_pts_on_axis[i] += 0.5
            VStar = (
                1.0
                + 2.0
                * math.cos(self.lattice.lattice_parameters[3] * d2r)
                * math.cos(self.lattice.lattice_parameters[4] * d2r)
                * math.cos(self.lattice.lattice_parameters[5] * d2r)
                - math.cos(self.lattice.lattice_parameters[3] * d2r) ** 2
                - math.cos(self.lattice.lattice_parameters[4] * d2r) ** 2
                - math.cos(self.lattice.lattice_parameters[5] * d2r) ** 2
            ) ** 0.5  # strictly always 0.86
            if rand_vec.seed % 2 == 1:
                #            if 1 == 1:
                temp_lengths[2] = self.length_bounds[self.lattice.unique_axis][
                    1
                ] + rand_vec.unit_cell_lengths[1] * (
                    self.unique_pts_on_axis[self.lattice.unique_axis]
                    * self.length_bounds[self.lattice.unique_axis][0]
                    - self.length_bounds[self.lattice.unique_axis][1]
                )
                #                print 1, temp_lengths[2]
                mean0 = (target_volume / (VStar * temp_lengths[2])) ** (1.0 / 2.0)
                if rand_vec.unit_cell_lengths[0] > 0.99:
                    temp_lengths[0] = (
                        norm.ppf(0.99)
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                elif rand_vec.unit_cell_lengths[0] < 0.01:
                    temp_lengths[0] = (
                        norm.ppf(0.01)
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                else:
                    temp_lengths[0] = (
                        norm.ppf(rand_vec.unit_cell_lengths[0])
                        * (
                            sd_param
                            * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                        )
                        ** 0.5
                        + mean0
                    )
                if (
                    temp_lengths[0]
                    < self.length_bounds[(self.lattice.unique_axis + 1) % 3][1]
                ):
                    temp_lengths[0] = self.length_bounds[
                        (self.lattice.unique_axis + 1) % 3
                    ][1]
                ordered_lens = [0.0] * 3
                ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
                ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
                ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
                #                print 1, ordered_lens, ordered_lens[0]*ordered_lens[1]*ordered_lens[2]
                self.lattice.set_lattice_parameters(
                    ordered_lens + self.lattice.lattice_parameters[3:6]
                )
            else:
                temp_lengths[0] = self.length_bounds[
                    (self.lattice.unique_axis + 1) % 3
                ][1] + rand_vec.unit_cell_lengths[1] * (
                    self.unique_pts_on_axis[(self.lattice.unique_axis + 1) % 3]
                    * self.length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                    - self.length_bounds[(self.lattice.unique_axis + 1) % 3][1]
                )  # **0.5
                if not unique_point_bounds:
                    temp_lengths[0] = temp_lengths[0] ** 0.5
                mean0 = target_volume / (VStar * (temp_lengths[0] ** 2.0))
                # this if statement is wrong. It is only included because the unique_point_bounds is new, and the tests are in 'old mode'
                # the reason that it is wrong is because we want the normal distribution of volumes, not lengths
                if rand_vec.unit_cell_lengths[0] > 0.99:
                    temp_lengths[2] = (
                        norm.ppf(0.99)
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                elif rand_vec.unit_cell_lengths[0] < 0.01:
                    temp_lengths[2] = (
                        norm.ppf(0.01)
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                else:
                    temp_lengths[2] = (
                        norm.ppf(rand_vec.unit_cell_lengths[0])
                        * (sd_param * self.length_bounds[self.lattice.unique_axis][0])
                        + mean0
                    )
                if temp_lengths[2] < self.length_bounds[self.lattice.unique_axis][1]:
                    temp_lengths[2] = self.length_bounds[self.lattice.unique_axis][1]
                ordered_lens = [0.0] * 3
                ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
                ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
                ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
                #                print 2, ordered_lens, ordered_lens[0]*ordered_lens[1]*ordered_lens[2]
                self.lattice.set_lattice_parameters(
                    ordered_lens + self.lattice.lattice_parameters[3:6]
                )
            #                print temp_lengths, mean0, self.length_bounds, self.unique_pts_on_axis, rand_vec.unit_cell_lengths, rand_vec.seed#;exit()

            return
        if self.lattice.lattice_type == 5:
            # Handle the TRIGONAL lattice
            d2r = math.pi / 180.0
            VStar = (
                1.0
                + 2.0
                * math.cos(self.lattice.lattice_parameters[3] * d2r)
                * math.cos(self.lattice.lattice_parameters[4] * d2r)
                * math.cos(self.lattice.lattice_parameters[5] * d2r)
                - math.cos(self.lattice.lattice_parameters[3] * d2r) ** 2
                - math.cos(self.lattice.lattice_parameters[4] * d2r) ** 2
                - math.cos(self.lattice.lattice_parameters[5] * d2r) ** 2
            ) ** 0.5
            mean0 = (target_volume / VStar) ** (1.0 / 3.0)
            if rand_vec.unit_cell_lengths[0] > 0.99:
                temp_lengths[0] = (
                    norm.ppf(0.99)
                    * (sd_param * self.length_bounds[0][0]) ** (1.0 / 3.0)
                    + mean0
                )
            elif rand_vec.unit_cell_lengths[0] < 0.01:
                temp_lengths[0] = (
                    norm.ppf(0.01)
                    * (sd_param * self.length_bounds[0][0]) ** (1.0 / 3.0)
                    + mean0
                )
            else:
                temp_lengths[0] = (
                    norm.ppf(rand_vec.unit_cell_lengths[0])
                    * (sd_param * self.length_bounds[0][0]) ** (1.0 / 3.0)
                    + mean0
                )
            if temp_lengths[0] < self.length_bounds[0][0]:
                temp_lengths[0] = self.length_bounds[0][0]
            self.lattice.set_lattice_parameters(
                [temp_lengths[0]] * 3 + self.lattice.lattice_parameters[3:6]
            )
            return

        # TODO - explain this section, not entirely clearly what's going on.
        d2r = math.pi / 180.0
        VStar = (
            1.0
            + 2.0
            * math.cos(self.lattice.lattice_parameters[3] * d2r)
            * math.cos(self.lattice.lattice_parameters[4] * d2r)
            * math.cos(self.lattice.lattice_parameters[5] * d2r)
            - math.cos(self.lattice.lattice_parameters[3] * d2r) ** 2
            - math.cos(self.lattice.lattice_parameters[4] * d2r) ** 2
            - math.cos(self.lattice.lattice_parameters[5] * d2r) ** 2
        ) ** 0.5
        indx2 = (rand_vec.seed - 1) % 3
        indx1 = (indx2 + 1 + (indx2) % 2) % 3
        indx0 = (indx2 + 2 - (indx2) % 2) % 3
        if self.mols_per_cell != 1:
            # hack dhc 200515
            #            temp_lengths [ indx2 ] = self.length_bounds[ indx2 ][1] + rand_vec.unit_cell_lengths[indx2] * ( self.mols_per_cell * self.length_bounds[indx2][0] - self.length_bounds[indx2][1] )
            #            temp_lengths [ indx1 ] = self.length_bounds[ indx1 ][1] + rand_vec.unit_cell_lengths[indx1] * ( self.mols_per_cell * self.length_bounds[indx1][0] - self.length_bounds[indx1][1] )
            temp_lengths[indx2] = self.length_bounds[indx2][
                1
            ] + rand_vec.unit_cell_lengths[indx2] * (
                self.unique_pts_on_axis[indx2] * self.length_bounds[indx2][0]
                - self.length_bounds[indx2][1]
            )
            temp_lengths[indx1] = self.length_bounds[indx1][
                1
            ] + rand_vec.unit_cell_lengths[indx1] * (
                self.unique_pts_on_axis[indx1] * self.length_bounds[indx1][0]
                - self.length_bounds[indx1][1]
            )
        else:
            temp_lengths[indx2] = self.length_bounds[indx2][
                1
            ] + rand_vec.unit_cell_lengths[indx2] * (
                1.5 * self.length_bounds[indx2][0] - self.length_bounds[indx2][1]
            )
            temp_lengths[indx1] = self.length_bounds[indx1][
                1
            ] + rand_vec.unit_cell_lengths[indx1] * (
                1.5 * self.length_bounds[indx1][0] - self.length_bounds[indx1][1]
            )
        mean0 = target_volume / (VStar * temp_lengths[indx2] * temp_lengths[indx1])  # +
        if rand_vec.unit_cell_lengths[indx0] > 0.99:
            temp_lengths[indx0] = (
                norm.ppf(0.99) * sd_param * self.length_bounds[indx0][0] + mean0
            )
        elif rand_vec.unit_cell_lengths[indx0] < 0.01:
            temp_lengths[indx0] = (
                norm.ppf(0.01) * sd_param * self.length_bounds[indx0][0] + mean0
            )
        else:
            temp_lengths[indx0] = (
                norm.ppf(rand_vec.unit_cell_lengths[indx0])
                * sd_param
                * self.length_bounds[indx0][0]
                + mean0
            )
        if temp_lengths[indx0] < self.length_bounds[indx0][1]:
            temp_lengths[indx0] = self.length_bounds[indx0][1]
        self.lattice.set_lattice_parameters(
            temp_lengths + self.lattice.lattice_parameters[3:6]
        )
        #
        #        print temp_lengths , self.length_bounds
        return

    def generate_and_set_new_cell_parameters(
        self, rand_vec, target_volume, sd_param=0.15, unique_point_bounds=True
    ):
        from scipy.stats import norm

        ######################################
        # This block starts to figure out the unit cell length, which are broken down into specific crystal systems
        ######################################
        # We need to used the precent point function from scipy. In a v-dimensional normal distribution function,
        # a ppf(p) gives a point t_v such that the corresponding probability for t_v is p, where 0<p<1. By default,
        # ppf function chooses a normal distribution centered at 0 with standard deviation of 1. Given a sd_param,
        # we could adjust point t_v corrsponding to a normal distribution of standard deviation of sd_param.
        # From here we know how to draw a value of propbable cell length from a given normal distribution, centered
        # on a mean value, which can be determined by the target volume parameters of the unit cell.
        #
        # The basic idea is to set 0,1 or 2 lattice lengths to be [min] + bound_scaling(=0.75)*random*(N*[max]-[min]) angstroms,
        # and then another 1,2 or 3 random variates to ensure that the volume of the cell relates to a normal variate with a mean
        # relating to the target_volume, and standard deviation of sd_param(=0.15) *[min]. Again, it is the volume of the cell,
        # not the final lengths, which are normally distributed. The different lattice types are accounted for, and the indices run
        # under modulo addition to scramble the order of their assignment, and hence avoid systematic bias. [max] and [min] are
        # projections of the molecular shadowed onto the axes. The N can be either the number of unique fractional positions
        # generated by the space group along each axis, or just the total number of molecules- this is the point which needs to be tidied up ASAP.
        # Only the middle 98% of Gaussian distribution used.
        temp_lengths = [0.0] * 3
        if self.lattice.lattice_type == 7:
            new_parameters = self.determine_lattice_type_7_parameters(
                rand_vec, self.length_bounds, target_volume, sd_param
            )
            self.lattice.set_lattice_parameters(new_parameters)
            return

        if self.lattice.lattice_type == 6:
            new_parameters = self.determine_lattice_type_6_parameters(
                rand_vec,
                self.unique_pts_on_axis,
                self.length_bounds,
                target_volume,
                sd_param,
                unique_point_bounds,
            )
            self.lattice.set_lattice_parameters(new_parameters)
            return

        if self.lattice.lattice_type == 5:
            new_parameters = self.determine_lattice_type_5_parameters(
                rand_vec,
                self.unique_pts_on_axis,
                self.length_bounds,
                target_volume,
                sd_param,
            )
            self.lattice.set_lattice_parameters(new_parameters)
            return

        if self.lattice.lattice_type == 4:
            new_parameters = self.determine_lattice_type_4_parameters(
                rand_vec,
                self.unique_pts_on_axis,
                self.length_bounds,
                target_volume,
                sd_param,
            )
            self.lattice.set_lattice_parameters(new_parameters)
            return

        if self.lattice.lattice_type in [1, 2, 3]:
            new_parameters = self.determine_lattice_type_1_2_3_parameters(
                rand_vec,
                self.unique_pts_on_axis,
                self.length_bounds,
                target_volume,
                sd_param,
            )
            self.lattice.set_lattice_parameters(new_parameters)
            return

        raise CSPyException("No lattice_type was set properly")

    def determine_lattice_type_7_parameters(
        self, rand_vec, length_bounds, target_volume, sd_param
    ):
        from scipy.stats import norm

        temp_lengths = [None, None, None]
        # Handling the CUBIC lattice, so all three unit cell lengths were identical.
        # The mean value of the cell length could be worked out from the tartget volume of the crystal.
        mean0 = target_volume ** (1.0 / 3.0)
        indx0 = 0
        ppf_input = rand_vec.unit_cell_lengths[indx0]
        ppf_input = min(ppf_input, 0.99)  # make ppf_input<=0.99
        ppf_input = max(ppf_input, 0.01)  # and ppf_input>=0.01
        length_indx = 0
        temp_lengths[indx0] = mean0 + norm.ppf(ppf_input) * (
            sd_param * length_bounds[length_indx][0]
        )
        temp_lengths[indx0] = max(temp_lengths[indx0], length_bounds[indx0][1])
        new_parameters = [temp_lengths[0]] * 3 + self.lattice.lattice_parameters[3:6]
        return new_parameters

    def determine_lattice_type_6_parameters(
        self,
        rand_vec,
        unique_pts_on_axis,
        length_bounds,
        target_volume,
        sd_param,
        unique_point_bounds,
    ):
        from scipy.stats import norm

        # Handling the HEXAGONAL lattice
        temp_lengths = [None, None, None]

        VStar = self.lattice.angle_component_of_volume()
        if rand_vec.seed % 2 == 1:
            temp_lengths[2] = length_bounds[self.lattice.unique_axis][
                1
            ] + rand_vec.unit_cell_lengths[1] * (
                unique_pts_on_axis[self.lattice.unique_axis]
                * length_bounds[self.lattice.unique_axis][0]
                - length_bounds[self.lattice.unique_axis][1]
            )
            mean0 = (target_volume / (VStar * temp_lengths[2])) ** (1.0 / 2.0)
            indx0 = 0
            ppf_input = rand_vec.unit_cell_lengths[indx0]
            ppf_input = min(ppf_input, 0.99)  # make ppf_input<=0.99
            ppf_input = max(ppf_input, 0.01)  # and ppf_input>=0.01
            length_indx = (self.lattice.unique_axis + 1) % 3
            temp_lengths[indx0] = mean0 + norm.ppf(ppf_input) * (
                sd_param * length_bounds[length_indx][0]
            ) ** (1.0 / 2.0)
            temp_lengths[indx0] = max(temp_lengths[indx0], length_bounds[indx0][1])
        else:
            temp_lengths[0] = length_bounds[(self.lattice.unique_axis + 1) % 3][
                1
            ] + rand_vec.unit_cell_lengths[1] * (
                unique_pts_on_axis[(self.lattice.unique_axis + 1) % 3]
                * length_bounds[(self.lattice.unique_axis + 1) % 3][0]
                - length_bounds[(self.lattice.unique_axis + 1) % 3][1]
            )  # **0.5
            if not unique_point_bounds:
                temp_lengths[0] = temp_lengths[0] ** 0.5
            mean0 = target_volume / (VStar * (temp_lengths[0] ** 2.0))
            # this if statement is wrong. It is only included because the unique_point_bounds is new, and the tests are in 'old mode'
            # the reason that it is wrong is because we want the normal distribution of volumes, not lengths
            if rand_vec.unit_cell_lengths[0] > 0.99:
                temp_lengths[2] = (
                    norm.ppf(0.99)
                    * (sd_param * length_bounds[self.lattice.unique_axis][0])
                    + mean0
                )
            elif rand_vec.unit_cell_lengths[0] < 0.01:
                temp_lengths[2] = (
                    norm.ppf(0.01)
                    * (sd_param * length_bounds[self.lattice.unique_axis][0])
                    + mean0
                )
            else:
                temp_lengths[2] = (
                    norm.ppf(rand_vec.unit_cell_lengths[0])
                    * (sd_param * length_bounds[self.lattice.unique_axis][0])
                    + mean0
                )
            if temp_lengths[2] < length_bounds[self.lattice.unique_axis][1]:
                temp_lengths[2] = length_bounds[self.lattice.unique_axis][1]
        ordered_lens = [0.0] * 3
        ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
        ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
        ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
        new_parameters = ordered_lens + self.lattice.lattice_parameters[3:6]
        return new_parameters

    def determine_lattice_type_5_parameters(
        self,
        rand_vec,
        unique_pts_on_axis,
        length_bounds,
        target_volume,
        sd_param,
        length_bound_index=0,
    ):
        from scipy.stats import norm

        temp_lengths = [None, None, None]
        VStar = self.lattice.angle_component_of_volume()

        mean0 = (target_volume / VStar) ** (1.0 / 3.0)
        indx0 = 0
        ppf_input = rand_vec.unit_cell_lengths[indx0]
        ppf_input = min(ppf_input, 0.99)  # make ppf_input<=0.99
        ppf_input = max(ppf_input, 0.01)  # and ppf_input>=0.01
        temp_lengths[indx0] = mean0 + norm.ppf(ppf_input) * (
            sd_param * length_bounds[indx0][0]
        ) ** (1.0 / 3.0)

        # TODO This line is incorrect, it should use length_bounds[indx0][1]
        temp_lengths[indx0] = max(
            temp_lengths[indx0], length_bounds[indx0][length_bound_index]
        )

        new_parameters = [temp_lengths[0]] * 3 + self.lattice.lattice_parameters[3:6]
        return new_parameters

    def determine_lattice_type_4_parameters(
        self, rand_vec, unique_pts_on_axis, length_bounds, target_volume, sd_param
    ):
        from scipy.stats import norm

        temp_lengths = [None, None, None]
        if (rand_vec.seed % 2) == 0:
            some_indx = self.lattice.unique_axis
            indx2 = 2
            indx0 = 0
            pow = 1.0
            ppf_pow = 0.5
            vol_pow = 1.0
            length_indx = (self.lattice.unique_axis + 1) % 3
        else:
            some_indx = (self.lattice.unique_axis + 1) % 3
            indx2 = 0
            indx0 = 2
            pow = 0.5
            ppf_pow = 1.0
            vol_pow = 2.0
            length_indx = self.lattice.unique_axis
        temp_lengths[indx2] = (
            length_bounds[some_indx][1]
            + rand_vec.unit_cell_lengths[1]
            * (
                self.mols_per_cell * length_bounds[some_indx][0]
                - length_bounds[some_indx][1]
            )
        ) ** pow

        mean0 = (target_volume / temp_lengths[indx2] ** vol_pow) ** ppf_pow
        ppf_input = rand_vec.unit_cell_lengths[0]
        ppf_input = min(ppf_input, 0.99)  # make ppf_input<=0.99
        ppf_input = max(ppf_input, 0.01)  # and ppf_input>=0.01
        temp_lengths[indx0] = (
            mean0
            + norm.ppf(ppf_input)
            * (sd_param * length_bounds[length_indx][0]) ** ppf_pow
        )
        temp_lengths[indx0] = max(temp_lengths[indx0], length_bounds[indx0][1])

        ordered_lens = [0.0] * 3
        ordered_lens[self.lattice.unique_axis] = temp_lengths[2]
        ordered_lens[(self.lattice.unique_axis + 1) % 3] = temp_lengths[0]
        ordered_lens[(self.lattice.unique_axis + 2) % 3] = temp_lengths[0]
        new_parameters = ordered_lens + self.lattice.lattice_parameters[3:6]
        return new_parameters

    def determine_lattice_type_1_2_3_parameters(
        self, rand_vec, unique_pts_on_axis, length_bounds, target_volume, sd_param
    ):
        """Calculate the new parameter lengths based of the bounds and number of molecules
      
        requires 
        rand_vec = RandomVector
        self.unique_pts_on_axis = [2,3,4]
        length_bounds = [[maxA,minA],[maxB,minB],[maxC,maxC]]
        target_volume = 1234.5
        sd_param = 0.2
        """
        from scipy.stats import norm

        temp_lengths = [None, None, None]
        VStar = self.lattice.angle_component_of_volume()

        # Indexing to swap order of lattice length assignments based on seed
        indx2 = (rand_vec.seed - 1) % 3
        indx1 = (indx2 + 1 + (indx2) % 2) % 3
        indx0 = (indx2 + 2 - (indx2) % 2) % 3

        # Compute lengths of cells based on self.unique_pts_on_axis[]
        if self.mols_per_cell == 1:
            # if only 1 molecule in the cell then add a little scaling
            unique_pts_on_axis[indx2] = 1.5
            unique_pts_on_axis[indx1] = 1.5
        temp_lengths[indx2] = length_bounds[indx2][1] + rand_vec.unit_cell_lengths[
            indx2
        ] * (
            unique_pts_on_axis[indx2] * length_bounds[indx2][0]
            - length_bounds[indx2][1]
        )
        temp_lengths[indx1] = length_bounds[indx1][1] + rand_vec.unit_cell_lengths[
            indx1
        ] * (
            unique_pts_on_axis[indx1] * length_bounds[indx1][0]
            - length_bounds[indx1][1]
        )

        # get mean of length for third axis length
        mean0 = target_volume / (VStar * temp_lengths[indx2] * temp_lengths[indx1])

        # Scale this value to gaussian distribution it with standard deviation of sd_param
        # need to make sure we dont do too close to the edges of Gaussian Distribution
        # currently only sample inside +-99%
        # ppf scales numbers between -inf and +inf. norm.ppf(0.5) = 0.0
        # This means norm.ppf(ppf_input)*sd_param has a largest abs value of 0.46526957480816816
        # So max_volume = mean + length_bound[indx0].max*0.5
        ppf_input = rand_vec.unit_cell_lengths[indx0]
        ppf_input = min(ppf_input, 0.99)  # make ppf_input<=0.99
        ppf_input = max(ppf_input, 0.01)  # and ppf_input>=0.01
        temp_lengths[indx0] = (
            mean0 + norm.ppf(ppf_input) * sd_param * length_bounds[indx0][0]
        )

        temp_lengths[indx0] = max(temp_lengths[indx0], length_bounds[indx0][1])
        return temp_lengths + self.lattice.lattice_parameters[3:6]

    def test_sat(self, sat_param1=0.99, neighbour_depth=3, pinch=0.0):
        return self.expand_sat(
            sat_param1=sat_param1,
            expand=False,
            neighbour_depth=neighbour_depth,
            pinch=pinch,
        )

    def expand_sat(self, sat_param1=0.99, expand=False, neighbour_depth=3, pinch=0.0):
        from cspy.deprecated.spacegroups import (
            SpaceGroup,
            SymmetryOperation,
            expand_symmetry_list,
        )

        """ This subroutine determines overlap, and expands the cell if needed. Returns True if there is overlap
            to debug, N.B. pinch, min_overlap -> tolerance in overlap
                           atoms vdw radii are finite
                           self.expand_lattice uses another tolerance """
        # note keep below line if args.spacegroup not set
        #        self.generate_symmetry_operation()
        # calculate vertices_vdwr once,, surely !
        list_of_ops = self.spacegroup.operations
        min_overlap = 0.05  # changed 240713, was 0.0001

        # The initial block will get hold of a bunch of objects before the actual SAT expansion was performed.
        #    This will allow, in principle, moving the actual SAT exapnd code into cython and speed up the process.
        if expand:
            saved_vectors = []
            saved_ops = []
            saved_trans = []
            saved_intermol_vec, saved_disp_vec = [0.0] * 3, [0.0] * 3

        # Things to be stored here include:
        # * vertices_mat:        A list of xyz for the vertices of the convex hulls for each unique molecule in the crystal.
        # * vertices_vdwr_mat:   A list of all the vdw radii for each atom in the molecule.
        # * vecs_perp_fac_mat:   A list of all vectors that are perpendicular to the faces of the molecular convex hull.
        # * centroids_mat:       A list of centroids for each unique molecule in the crystal.
        # * intermol_cutoff_mat: A list of averaged maximum molecular box lengths between any pair of unique molecules in the crystal.
        (
            vertices_mat,
            vertices_vdwr_mat,
            vecs_perp_fac_mat,
            centroids_mat,
            intermol_cutoff_mat,
        ) = ([], [], [], [], [])
        #        intermol_cutoff_mat = max ([ max(mol.box_lengths) for mol in self.unique_molecule_list  ])
        for im, molecule in enumerate(self.unique_molecule_list):
            vertices_mat.append(
                [v for v in self.unique_molecule_list[im].convex_hull_vertices]
            )
            # here just want to get hold of an atom object in which there is a vdw_radii dictionary for all the atoms. The actual loop will go through all the
            # convex hull indicies, which are just the cleaned atomic labels, from which the vdw radii can be retreived from the dictionary.
            vertices_vdwr_mat.append(
                [
                    self.unique_molecule_list[im].atoms[0].vdw_radii[v]
                    for v in self.unique_molecule_list[im].convex_hull_vertices_id
                ]
            )
            vecs_perp_fac_mat.append(
                list3perp_face_vecs(self.unique_molecule_list[im].convex_hull_faces)
            )
            centroids_mat.append(self.unique_molecule_list[im].centroid())
            intermol_cutoff_mat.append(
                [
                    0.5 * (max(molecule.box_lengths) + max(mol2.box_lengths))
                    for mol2 in self.unique_molecule_list
                ]
            )

        # get hold of all the rotation and translation operations in Cartesian space. Use a maximum up to neighbour_depth number of
        # unit cells (default to 3) to detect if two molecules in this supercell overlaps with each other.
        cartesian_op_mats, cartesian_op_trans = (
            [],
            [],
        )  # [ self.make_cartesian_op_matrix ( op )
        translations = list3make_all_translations(
            self.lattice.lattice_vectors, neighbour_depth
        )
        for op in list_of_ops:
            temp_object = self.make_cartesian_op_matrix(op)
            cartesian_op_mats.append(temp_object[0])
            cartesian_op_trans.append(temp_object[1])

        # This is the main loop to detect if there's any overlap between molecules, by looping through all symmetry operations to generate molecular pairs.
        for iop, op in enumerate(list_of_ops):
            for im1, molecule1 in enumerate(self.unique_molecule_list):
                # TODO- is this redundant from the preparation stage?
                # get hold of the vectors perpendicular to the convex hull faces for this molecule.
                vecs_perp_faces = list3perp_face_vecs(molecule1.convex_hull_faces)
                for im2, molecule2 in enumerate(self.unique_molecule_list):
                    # molecule 2 - do the symmetry transformations on the convex hull vertices, edges and faces. Basically could factorize out a function to
                    # apply symmetry operations on the convex hull as a whole.
                    vertices2t = list3apply_sym_list(
                        molecule2.convex_hull_vertices,
                        cartesian_op_mats[iop],
                        cartesian_op_trans[iop],
                    )
                    vecs_perp_faces2 = list3apply_sym_list_norm(
                        list3perp_face_vecs(molecule2.convex_hull_faces),
                        cartesian_op_mats[iop],
                        cartesian_op_trans[iop],
                    )
                    edges1 = [
                        list3subtract(edge[1], edge[0])
                        for edge in self.unique_molecule_list[im1].convex_hull_edges
                    ]
                    edges2 = [
                        list3subtract(
                            list3apply_sym(
                                edge[1], cartesian_op_mats[iop], cartesian_op_trans[iop]
                            ),
                            list3apply_sym(
                                edge[0], cartesian_op_mats[iop], cartesian_op_trans[iop]
                            ),
                        )
                        for edge in self.unique_molecule_list[im2].convex_hull_edges
                    ]
                    # Now need to work out the axis along which the separations will be performed, which are the cross products between edges of molecule 1 and transformed molecule 2
                    if im1 == im2:
                        vecs_edges = list3cross_comb_tri(edges1, edges2)
                    else:
                        vecs_edges = list3cross_combinations(edges1, edges2)

                    # For all the possible axes along which SAT expand could be performed, worked out which pair of two axes were almost parallel to each other
                    #  within the tolerance of sat_param1 (typically set to 0.99, means two vectors are almost parallel to each other by 99 %, which seems to work
                    #  fairly efficiently. The parallel duplicate of axis will be removed to improve numerical efficiency, such that expansion along same
                    #  axis will not be tried repetitively.
                    axis_list = list3delparallelvecs(
                        vecs_perp_faces + vecs_perp_faces2 + vecs_edges, sat_param1
                    )

                    axis_proj1 = [None for axis in axis_list]
                    axis_proj2 = [None for axis in axis_list]
                    for trans in translations:
                        # Initialize a big displacement vector, from which subsequent displacement vector will be compared to, in
                        # order to find the shortest displacement vector
                        disp_vec = [1000.0] * 3

                        # Basically no need to try separating two molecules that are completely on top of each other.
                        if (
                            op.original_string_form.replace(" ", "").lower() == "x,y,z"
                            and list3mag(trans) < 0.0001
                            and im1 == im2
                        ):
                            continue
                        # workout the intercentroid vector between these two molecules. As a first check, will see if these two centroids were separated far enough, defined by
                        # the corresponding intermol_cutoff_mat, if so, quit out from the loop since the two molecules were not too close to each other.
                        intermol_vec = list3subtract(
                            list3add(
                                trans,
                                list3apply_sym(
                                    centroids_mat[im2],
                                    cartesian_op_mats[iop],
                                    cartesian_op_trans[iop],
                                ),
                            ),
                            centroids_mat[im1],
                        )
                        if list3mag(intermol_vec) > intermol_cutoff_mat[im1][im2]:
                            continue
                        # Here is another loop that loops through all the possible axes along which expansion could be performed. The idea is to work out
                        # along which axis there is an overlap between the convex hull. And if there are more than one axis that such an overlap exists,
                        # which axis got the smallest overlap, and expansion will be tried along this axis, presumably with minimum effort.
                        # The overlap was found by projecting all the convex hull verticies onto the axis for each molecule and compare the extrema
                        # for the projections from each molecule. (very briefly)
                        for iax, axis in enumerate(
                            axis_list
                        ):  # axis must be normalized
                            if not axis_proj1[iax] or not axis_proj2[iax]:
                                axis_proj1[iax] = list3_proj_pts_axis(
                                    axis,
                                    molecule1.convex_hull_vertices,
                                    vertices_vdwr_mat[im1],
                                )
                                axis_proj2[iax] = list3_proj_pts_axis(
                                    axis, vertices2t, vertices_vdwr_mat[im2]
                                )
                            max1, min1 = axis_proj1[iax]
                            max2, min2 = axis_proj2[iax]
                            max2 += list3dot(trans, axis)
                            min2 += list3dot(trans, axis)
                            if expand:
                                disp_vec_temp = list3required_dvec(
                                    list3find_overlap(
                                        (max1 - pinch, min1 + pinch),
                                        (max2 - pinch, min2 + pinch),
                                    ),
                                    intermol_vec,
                                    axis,
                                )
                            else:
                                disp_vec_temp = list3multiply(
                                    axis,
                                    list3find_overlap(
                                        (max1 - pinch, min1 + pinch),
                                        (max2 - pinch, min2 + pinch),
                                    ),
                                )
                            if list3mag(disp_vec_temp) < list3mag(disp_vec):
                                disp_vec = disp_vec_temp
                                if list3mag(disp_vec) < min_overlap:
                                    break
                        # Basically only performe SAT if the length of the shortest overlap is larger than the minimum overlap threshold, otherwise,
                        # there is not much need to perform such a separation.

                        if list3mag(disp_vec) > min_overlap:
                            if expand == False:
                                return True
                            else:
                                if (
                                    list3mag(disp_vec) < list3mag(saved_disp_vec)
                                    or list3mag(saved_disp_vec) < min_overlap
                                ):
                                    saved_disp_vec = disp_vec
                                    saved_intermol_vec = intermol_vec
                                saved_vectors.append(disp_vec)
                                saved_ops.append(op)
                                saved_trans.append(trans)

        if expand == True and len(saved_vectors) > 0:
            # If we want to perform expansion and there is indeed an overlap vector found. it will now move the two molecules
            #   apart from each other and expand the lattice. This is only done once here. The controlling loop in the
            #   structure generator will call sat expand multiple time until a reasonable structrue can be achieved or otherwise
            #   discard the structure generated by this Sobol seed once for all.
            # see also :method expand_lattice(): for expanding the actual lattice, after pulling molecules apart from each other.
            disp_vec = sorted(saved_vectors, key=lambda x: list3mag(x))[0]
            op = saved_ops[saved_vectors.index(disp_vec)]
            trans = saved_trans[saved_vectors.index(disp_vec)]
            old_inv_vectors = self.lattice.inv_lattice_vectors
            self.expand_lattice(
                list3vecmatrix(saved_disp_vec, self.lattice.inv_lattice_vectors),
                list3vecmatrix(saved_intermol_vec, self.lattice.inv_lattice_vectors),
            )
            for im, molecule in enumerate(self.unique_molecule_list):
                self.move_all_objects_temp(
                    list3subtract(
                        list3vecmatrix(
                            list3vecmatrix(molecule.centroid(), old_inv_vectors),
                            self.lattice.lattice_vectors,
                        ),
                        molecule.centroid(),
                    ),
                    im,
                )
            return True
        return False

    def sort_ops(self, original_list, depth):
        """
        Only for 1 unique molecule
        """
        distances_list = []
        for op in original_list:
            distance = 1000.0
            for trans in self.lattice.create_translations(depth):
                if distance > list3mag(
                    list3subtract(
                        list3add(
                            trans,
                            self.cartesian_op(
                                op, self.unique_molecule_list[0].centroid()
                            ),
                        ),
                        self.unique_molecule_list[0].centroid(),
                    )
                ):
                    distance = list3mag(
                        list3subtract(
                            list3add(
                                trans,
                                self.cartesian_op(
                                    op, self.unique_molecule_list[0].centroid()
                                ),
                            ),
                            self.unique_molecule_list[0].centroid(),
                        )
                    )
            distances_list.append(distance)
        return [x for (y, x) in sorted(zip(distances_list, original_list))][::-1]

    def make_string_op_from_mat(self, mat, vec, swap_mat=None):
        """ This subroutine was intended to create symmetry strings with rotations amongst the original axes
        Such a thing is difficult and this in not complete. A better thing would be to get complete lists of 
        operators in all settings; the algebra to manipulate strings is harder to program than matrices
        """
        temp_string = []

        def test_value(number, var):
            if abs(number) < 0.1:
                return ""
            if var == "":
                return str(number)
            if round(abs(number), 2) == 1.00:
                if number > 0.0:
                    return "+" + var
                else:
                    return "-" + var
            if number > 0.0:
                return "+" + str(number) + "*" + var
            else:
                return str(number) + "*" + var

        def make_swap_d(mat):
            # the swapping dictionary is relevent if rotating axis frames
            swap_d = {}
            element_vec = ["x", "y", "z"]
            ts = []
            ts.append(
                test_value(mat[0][0], "x")
                + test_value(mat[0][1], "y")
                + test_value(mat[0][2], "z")
            )
            ts.append(
                test_value(mat[1][0], "x")
                + test_value(mat[1][1], "y")
                + test_value(mat[1][2], "z")
            )
            ts.append(
                test_value(mat[2][0], "x")
                + test_value(mat[2][1], "y")
                + test_value(mat[2][2], "z")
            )
            for its in range(len(ts)):
                if "-" in ts[its]:
                    swap_d[ts[its].replace("-", "")] = "-" + element_vec[its]
                    ts[its].replace("-", "")
                else:
                    swap_d[ts[its].replace("+", "")] = element_vec[its]
            return swap_d

        if swap_mat:
            change_dict = make_swap_d(swap_mat)  # { "x":"x", "y":"z", "z":"-y"}
        else:
            change_dict = {"x": "x", "y": "y", "z": "z"}
        temp_string.append(
            test_value(vec[0], "")
            + test_value(mat[0][0], change_dict["x"])
            + test_value(mat[0][1], change_dict["y"])
            + test_value(mat[0][2], change_dict["z"])
            .replace("--", "+")
            .replace("++", "+")
            .replace("-+", "-")
            .replace("+-", "-")
        )
        temp_string.append(
            test_value(vec[1], "")
            + test_value(mat[1][0], change_dict["x"])
            + test_value(mat[1][1], change_dict["y"])
            + test_value(mat[1][2], change_dict["z"])
            .replace("--", "+")
            .replace("++", "+")
            .replace("-+", "-")
            .replace("+-", "-")
        )
        temp_string.append(
            test_value(vec[2], "")
            + test_value(mat[2][0], change_dict["x"])
            + test_value(mat[2][1], change_dict["y"])
            + test_value(mat[2][2], change_dict["z"])
            .replace("--", "+")
            .replace("++", "+")
            .replace("-+", "-")
            .replace("+-", "-")
        )
        return ",".join(temp_string)

    def swap_op_string(self, op_string, axes_rot_mat):
        # what if it's mixing 1/2 x etc
        swap_dict = {}
        swap_dict["x"] = axes_rot_mat[0][0]

    def make_cartesian_op_matrix_frac(self, op):
        op_matrix = [[0.0] * 3] * 3
        pure_trans = op.apply_symmetry_op([0.0, 0.0, 0.0])
        op_matrix[0] = list3subtract(op.apply_symmetry_op([1.0, 0.0, 0.0]), pure_trans)
        op_matrix[1] = list3subtract(op.apply_symmetry_op([0.0, 1.0, 0.0]), pure_trans)
        op_matrix[2] = list3subtract(op.apply_symmetry_op([0.0, 0.0, 1.0]), pure_trans)
        return (op_matrix, pure_trans)

    def make_cartesian_op_matrix(self, op):
        """Acts on a cartesian vector and returns a cartesian vector"""
        temp_object = self.make_cartesian_op_matrix_frac(op)
        frac_op_matrix = temp_object[0]
        frac_trans_vec = temp_object[1]
        return (
            list3matrixmatrix(
                self.lattice.inv_lattice_vectors,
                list3matrixmatrix(frac_op_matrix, self.lattice.lattice_vectors),
            ),
            list3vecmatrix(frac_trans_vec, self.lattice.lattice_vectors),
        )

    def cartesian_op(self, op, cartesian_vec):
        return list3vecmatrix(
            op.apply_symmetry_op(
                list3vecmatrix(cartesian_vec, self.lattice.inv_lattice_vectors)
            ),
            self.lattice.lattice_vectors,
        )

    def move_all_objects(self, shift_vector):
        """
        Not used in current implementation- dhc - 220114 
        """

    def move_all_objects_temp(self, shift_vector, im):
        for i in range(len(self.unique_molecule_list[im].convex_hull_vertices)):
            self.unique_molecule_list[im].convex_hull_vertices[i] = list3add(
                self.unique_molecule_list[im].convex_hull_vertices[i], shift_vector
            )
        for ie, edge in enumerate(self.unique_molecule_list[im].convex_hull_edges):
            for i in range(2):
                self.unique_molecule_list[im].convex_hull_edges[ie][i] = list3add(
                    edge[i], shift_vector
                )
        for iface, face in enumerate(self.unique_molecule_list[im].convex_hull_faces):
            for i in range(3):
                self.unique_molecule_list[im].convex_hull_faces[iface][i] = list3add(
                    face[i], shift_vector
                )
        for ia, atom in enumerate(self.unique_molecule_list[im].atoms):
            self.unique_molecule_list[im].atoms[ia].xyz = list3add(
                atom.xyz, shift_vector
            )

    #        return #also not moving convex hull face or edge vectors-- shouldn't need to ??

    def expand_lattice(self, overlap_vector_frac, centroid_vector_frac):
        """ If the fractional displacement is below a tolerance, will not expand.
            This 0.1 -> 0.05 on 220514 dhc """
        new_ucl = [0.0] * 3
        for i in range(0, 3):
            if abs(centroid_vector_frac[i]) > 0.05:
                new_ucl[i] = (
                    self.lattice.lattice_parameters[i]
                    * (1.0 + abs(overlap_vector_frac[i] / centroid_vector_frac[i]))
                    + 0.001
                )
            else:
                new_ucl[i] = self.lattice.lattice_parameters[i]
        if self.lattice.lattice_type in [5, 7]:
            tlen = max(new_ucl)
            new_ucl = [tlen] * 3
        if self.lattice.lattice_type in [4, 6]:
            if (
                new_ucl[(self.lattice.unique_axis + 1) % 3]
                < new_ucl[(self.lattice.unique_axis + 2) % 3]
            ):
                new_ucl[(self.lattice.unique_axis + 1) % 3] = new_ucl[
                    (self.lattice.unique_axis + 2) % 3
                ]
            else:
                new_ucl[(self.lattice.unique_axis + 2) % 3] = new_ucl[
                    (self.lattice.unique_axis + 1) % 3
                ]
        self.lattice.set_lattice_parameters(
            new_ucl + self.lattice.lattice_parameters[3:6]
        )
        return new_ucl

    def required_displacement_vector(self, overlap, vector, axis):
        cos_rdv_theta = np.vdot(vector, axis) / (
            np.linalg.norm(vector) * np.linalg.norm(axis)
        )
        normalized_axis = axis / np.linalg.norm(axis)
        disp_vec = normalized_axis * (overlap / (cos_rdv_theta + 0.0000001))
        return disp_vec

    def find_overlap(self, EP1, EP2):
        if EP1[0] > EP2[0]:
            if EP1[1] < EP2[0]:
                if EP1[1] > EP2[1]:
                    return abs(EP2[0] - EP1[1])
                else:
                    return self.SmallerOv(EP1, EP2)
            else:
                return 0.0
        else:
            if EP1[0] > EP2[1]:
                if EP1[1] < EP2[1]:
                    return abs(EP1[0] - EP2[1])
                else:
                    return self.SmallerOv(EP1, EP2)
            else:
                return 0.0
        crystal_logger.info("Error in find_overlap")
        return

    def SmallerOv(self, EP1, EP2):
        # be wary of this thing
        if abs(EP1[0] - EP2[1]) <= abs(EP1[1] - EP2[0]):
            return abs(EP1[0] - EP2[1])
        else:
            return abs(EP1[1] - EP2[0])

    def move_all_molecules_into_uc(self):
        # this is copied from (check_asymmetric_unit) below- kept separate to avoid conflicts
        from math import floor

        for idd, mole in enumerate(self.unique_molecule_list):
            frac_centroid = self.lattice.to_fractional(mole.centroid)
            shift = -self.lattice.to_cartesian(np.floor(frac_centroid))
            self.unique_molecule_list[idd].displace_atoms(shift)
        return

    def check_molecule_list(self):
        """Translates molecules to the first periodic image. Works for Z'>1."""
        from math import floor

        crystal_logger.debug("len(self.molecule_list)=" + str(len(self.molecule_list)))
        frac_centroid = [[0.0, 0.0, 0.0] for _ in range(len(self.molecule_list))]
        crystal_logger.debug("frac_centroid=" + str(frac_centroid))
        for idd, mole in enumerate(self.molecule_list):
            crystal_logger.debug("Shifting coordinates for molecule " + str(idd))
            frac_centroid[idd] = self.lattice.to_fractional(mole.centroid())
            shift = -self.lattice.to_cartesian(np.floor(frac_centroid[idd]))
            self.molecule_list[idd].displace_atoms(shift)
        return

    def check_asymmetric_unit(self):
        """Translates molecules to the first periodic image. Works for Z'>1."""
        from math import floor

        crystal_logger.debug(
            "len(self.unique_molecule_list)=" + str(len(self.unique_molecule_list))
        )
        frac_centroid = [[0.0, 0.0, 0.0] for _ in range(len(self.unique_molecule_list))]
        crystal_logger.debug("frac_centroid=" + str(frac_centroid))
        for idd, mole in enumerate(self.unique_molecule_list):
            crystal_logger.debug("Shifting coordinates for molecule " + str(idd))
            frac_centroid[idd] = self.lattice.to_fractional(mole.centroid())
            shift = -self.lattice.to_cartesian(np.floor(frac_centroid[idd]))
            self.unique_molecule_list[idd].displace_atoms(shift)
        return

    def sort_atoms_in_molecules(self):
        """ Sorts the atoms in each molecule into the order of the first molecule."""
        if len(self.unique_molecule_list) == 1:
            return
        else:
            crystal_logger.info("Sorting atoms in molecules")
        # print self.unique_molecule_list[1].atoms[4].label
        for i, other in enumerate(self.unique_molecule_list[1:], 1):
            first = copy.deepcopy(self.unique_molecule_list[0])
            first.min_rms(other, anyatom=True)
            # print [a.label for a in self.unique_molecule_list[i].atoms]
            self.unique_molecule_list[i].reorder_to_nearest_other(first)
            # print [a.label for a in self.unique_molecule_list[i].atoms]
        # print self.unique_molecule_list[1].atoms[4].label

    def arrange_hydrogens(self):
        """Re-orders the atoms in the crystal so that the hydrogens are
        in the end. Needed for CrystalOptimiser. This works for Z'>1."""
        for idd in range(len(self.unique_molecule_list)):
            self.unique_molecule_list[idd].arrange_hydrogens()
            crystal_logger.debug("Arranging hydrogens in molecule " + str(idd))
        return

    def standard_atom_order(self):
        """Re-orders the atoms in the crystal so that the atoms are in a standard order.
        Needed for transferable smart.smult files."""
        for idd in range(len(self.unique_molecule_list)):
            self.unique_molecule_list[idd].standard_atom_order()
            crystal_logger.debug("Arranging atoms in molecule " + str(idd))
        self.update_sfac(standard_order=True)
        return

    def num_distinct_molecules(self, rms_tol=0.002, maxd_tol=0.005):
        """Returns the number of distinct molecules"""

        if len(self.unique_molecule_list) <= 1:
            return len(self.unique_molecule_list)

        num_distinct = len(self.unique_molecule_list)

        for ia, mola in enumerate(self.unique_molecule_list[:-1]):
            for ib, molb in enumerate(self.unique_molecule_list[ia + 1 :]):
                if mola.is_same(
                    molb,
                    allow_different_order=False,
                    rms_tol=rms_tol,
                    maxd_tol=maxd_tol,
                ):
                    num_distinct -= 1
                    break
            else:
                # Found a match, stop checking ia,molb
                continue
        return num_distinct

    def clean_symmetry_ops(self):
        """Re-writes symmetry operations to make them compatible
        with PLATON. This works for Z'>1."""
        from cspy.deprecated.spacegroups import symm_op_clean_up, SymmetryOperation

        for n, symm in enumerate(self.symm):
            self.symm[n] = symm_op_clean_up(
                SymmetryOperation(self.symm[n], None).string_fraction_form()
            )
        return

    def symmetry_operations(self):
        from cspy.deprecated.spacegroups import SymmetryOperation, expand_symmetry_list

        return [
            SymmetryOperation(s, None)
            for s in expand_symmetry_list(self.symm.copy(), int(self.latt))
        ]

    def hall_number(self):
        symops = {
            s.string_clean_form(): s.matrix_form() for s in self.symmetry_operations()
        }
        rotations = [s[0] for s in symops.values()]
        translations = [s[1] for s in symops.values()]
        from spglib import get_hall_number_from_symmetry

        n = get_hall_number_from_symmetry(rotations, translations)
        return n

    def standardize_cell(self, rmsd_tolerance=0.1):
        # 1. Store symmetry inf lost when expanding to full unit cell:
        symm = sorted([x.string_clean_form() for x in self.symmetry_operations()])
        latt = self.latt
        # 2. Expand to full unit cell and move all molecules to w/i UC.
        crystal_logger.info("Expanding to full unit cell (P1)")
        original_crystal = copy.deepcopy(self)
        self.expand_to_full_unit_cell()
        # 3. Now we interface with SPGLib. We provide it with 3 bits of
        #    info:
        #      a) col-major 3x3 matrix of lattice vectors
        #      b) Nx3 array of atomic fractional coordinates
        #      c) 1XN array of atomic numbers for constituent atoms
        crystal_logger.info("Reducing cell using spglib")
        from spglib import standardize_cell as scell, get_symmetry
        from cspy.deprecated.spacegroups import SymmetryOperation

        lattice = self.lattice.direct_matrix()
        lattice[np.abs(lattice) < (1e-8)] = 0
        crystal_logger.info(
            "Old lattice parameters:\nlengths: %s\nangles: %s",
            self.lattice.lattice_parameters[:3],
            self.lattice.lattice_parameters[3:],
        )

        pos = self.atomic_positions(frac=True)
        xyz = self.atomic_positions()
        nums = self.atomic_numbers()
        cell = scell((lattice, pos, nums), symprec=1e-08)
        #        from spglib import get_symmetry, get_spacegroup, get_symmetry_dataset
        #        dataset = get_symmetry_dataset(
        #            cell, symprec=1e-12,
        #            angle_tolerance=1e-5,
        #            hall_number=0,
        #        )
        #        uniques = np.unique(dataset['equivalent_atoms'])
        # 4. Having standardized the cell and carried out symmetry
        #    analysis, we reduce crystal back to its smallest unit.
        lattice, scaled_positions, numbers = cell
        self.lattice.set_lattice_vectors(lattice)
        crystal_logger.info(
            "New lattice parameters:\nlengths: %s\nangles: %s",
            self.lattice.lattice_parameters[:3],
            self.lattice.lattice_parameters[3:],
        )

        m = Molecule.from_arrays(numbers, self.lattice.to_cartesian(scaled_positions))

        self.unique_molecule_list = m.fragment_geometry(
            lattice=self.lattice, expansion_level=1
        )
        crystal_logger.debug(
            "Pasting original geometries from old unique molecule list"
        )

        from cspy.linalg.kabsch import iterative_closest_points

        rmsds = [1000000000 for m in original_crystal.unique_molecule_list]
        done = [-1 for m in original_crystal.unique_molecule_list]
        original_molecules = copy.deepcopy(original_crystal.unique_molecule_list)
        for i, ref in enumerate(original_molecules):
            ref_geom = ref.xyz_asarray()
            ref_numbers = np.asarray([x.atomic_number for x in ref.atoms])
            for j, mol in enumerate(self.unique_molecule_list):
                if j in done:
                    continue
                coords = mol.xyz_asarray()
                numbers = np.asarray([x.atomic_number for x in mol.atoms])
                if ref_geom.shape != coords.shape:
                    continue
                T, rmsd, idx = iterative_closest_points(
                    ref_geom, coords, tolerance=1e-5
                )
                if rmsd < rmsd_tolerance:
                    if rmsd > rmsds[i]:
                        continue
                    rmsds[i] = rmsd
                    done[i] = j
                    crystal_logger.debug(
                        "Found match %d, len=%d for %d", j, coords.shape[0], i
                    )

                    for k in range(coords.shape[0]):
                        ref.atoms[k].xyz = coords[idx[k], :]
                    continue
                else:
                    crystal_logger.debug(
                        "rmsd: failed for molecule %s %s = %s", i, j, rmsd
                    )

            if done[i] < 0:
                crystal_logger.error(
                    "No match for %s %s", i, [x.label for x in ref.atoms]
                )
            else:
                crystal_logger.debug("Found match for molecule %s", i)

        not_done = np.where(np.asarray(done) < 0)[0]
        if len(not_done) > 0:
            for x in not_done:
                mol = original_crystal.unique_molecule_list[x]
                crystal_logger.error(
                    "Could not match molecule: %s\n%s",
                    x,
                    "\n".join(mol.xyz_string_form()),
                )
            self = original_crystal
            return False
        else:
            self.unique_molecule_list = copy.deepcopy(
                original_crystal.unique_molecule_list
            )
            self.symm = original_crystal.symm
            self.latt = original_crystal.latt
            return True

    def krivy_gruber(
        self,
        max_it=10000,
        round_acc=5,
        change_symm_strings=False,
        quick_return=False,
        return_transformation_mat=False,
        enforce_setting=False,
    ):
        """see Krivy and Gruber, Acta Cryst 1976 A32 297 for explanations of step x etc
        this subroutine should reduce the cell parameters- symmetry operations within the new cell can only be accurately performed
        using matrices, as the algebra to implement string manipulation is not present (default is off)
        This returns the lattice parameters
        """
        from cspy.deprecated.spacegroups import (
            SpaceGroup,
            SymmetryOperation,
            expand_symmetry_list,
            symm_op_clean_up,
        )

        d2r = math.pi / 180.0

        def step(s):
            eq = s.eps_eq
            lt = s.eps_lt
            gt = s.eps_gt
            # A1
            if gt(s.a, s.b) or (eq(s.a, s.b) and gt(abs(s.d), abs(s.e))):
                s.a1_action()
            # A2
            if gt(s.b, s.c) or (eq(s.b, s.c) and gt(abs(s.e), abs(s.f))):
                s.a2_action()
                return True
            # A3
            if s.def_gt_0():
                s.a3_action()
            # A4
            else:
                s.a4_action()
            # A5
            if (
                gt(abs(s.d), s.b)
                or (eq(s.d, s.b) and lt(s.e + s.e, s.f))
                or (eq(s.d, -s.b) and lt(s.f, 0))
            ):
                s.a5_action()
                return True
            # A6
            if (
                gt(abs(s.e), s.a)
                or (eq(s.e, s.a) and lt(s.d + s.d, s.f))
                or (eq(s.e, -s.a) and lt(s.f, 0))
            ):
                s.a6_action()
                return True
            # A7
            if (
                gt(abs(s.f), s.a)
                or (eq(s.f, s.a) and lt(s.d + s.d, s.e))
                or (eq(s.f, -s.a) and lt(s.e, 0))
            ):
                s.a7_action()
                return True
            # A8
            if lt(s.d + s.e + s.f + s.a + s.b, 0) or (
                eq(s.d + s.e + s.f + s.a + s.b, 0)
                and gt(s.a + s.a + s.e + s.e + s.f, 0)
            ):
                s.a8_action()
                return True
            return False

        relative_epsilon = 1.0e-5

        class KG_Object:
            def __init__(self, max_iterations=100):
                #            self._r_inv = matrix.sqr((1,0,0,0,1,0,0,0,1))
                self._r_inv = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
                self._n_iterations = 0
                self._iteration_limit = max_iterations  # 100

            def _name(self):
                return "Krivy-Gruber"

            def eps_lt(self, x, y):
                return x < y - self.epsilon

            def eps_gt(self, x, y):
                return self.eps_lt(y, x)

            def eps_eq(self, x, y):
                return not (self.eps_lt(x, y) or self.eps_lt(y, x))

            def def_test(self):
                lt = self.eps_lt
                d, e, f = (self.d, self.e, self.f)
                n_zero = 0
                n_positive = 0
                if lt(0, d):
                    n_positive += 1
                elif not lt(d, 0):
                    n_zero += 1
                if lt(0, e):
                    n_positive += 1
                elif not lt(e, 0):
                    n_zero += 1
                if lt(0, f):
                    n_positive += 1
                elif not lt(f, 0):
                    n_zero += 1
                return n_zero, n_positive

            def def_gt_0(self):
                n_zero, n_positive = self.def_test()
                return n_positive == 3 or (n_zero == 0 and n_positive == 1)

            def type(self):
                n_zero, n_positive = self.def_test()
                if n_positive == 3:
                    return 1
                if n_positive == 0:
                    return 2
                return 0

            def a1_action(s):
                s.n1_action()

            def a2_action(s):
                s.n2_action()

            def a3_action(s):
                s.n3_true_action()

            def a4_action(s):
                s.n3_false_action()

            def a5_action(s):
                if s.d > 0:
                    s.cb_update((1, 0, 0, 0, 1, -1, 0, 0, 1))
                    s.c += s.b - s.d
                    s.d -= s.b + s.b
                    s.e -= s.f
                else:
                    s.cb_update((1, 0, 0, 0, 1, 1, 0, 0, 1))
                    s.c += s.b + s.d
                    s.d += s.b + s.b
                    s.e += s.f
                assert s.c > 0

            def a6_action(s):
                if s.e > 0:
                    s.cb_update((1, 0, -1, 0, 1, 0, 0, 0, 1))
                    s.c += s.a - s.e
                    s.d -= s.f
                    s.e -= s.a + s.a
                else:
                    s.cb_update((1, 0, 1, 0, 1, 0, 0, 0, 1))
                    s.c += s.a + s.e
                    s.d += s.f
                    s.e += s.a + s.a
                assert s.c > 0

            def a7_action(s):
                if s.f > 0:
                    s.cb_update((1, -1, 0, 0, 1, 0, 0, 0, 1))
                    s.b += s.a - s.f
                    s.d -= s.e
                    s.f -= s.a + s.a
                else:
                    s.cb_update((1, 1, 0, 0, 1, 0, 0, 0, 1))
                    s.b += s.a + s.f
                    s.d += s.e
                    s.f += s.a + s.a
                assert s.b > 0

            def a8_action(s):
                s.cb_update((1, 0, 1, 0, 1, 1, 0, 0, 1))
                s.c += s.a + s.b + s.d + s.e + s.f
                s.d += s.b + s.b + s.f
                s.e += s.a + s.a + s.f
                assert s.c > 0

            def cb_update(self, m_elems):
                if self._n_iterations == self._iteration_limit:
                    raise CSPyException(
                        "%s iteration limit exceeded (limit=%d)."
                        % (str(self._name()), str(self._iteration_limit))
                    )
                #                self._r_inv *= matrix.sqr(m_elems)
                self._r_inv = list3matrixmatrix(
                    self._r_inv, [m_elems[0:3], m_elems[3:6], m_elems[6:9]]
                )
                self._n_iterations += 1

            def n1_action(self):
                self.cb_update((0, -1, 0, -1, 0, 0, 0, 0, -1))
                self.a, self.b = self.b, self.a
                self.d, self.e = self.e, self.d

            def n2_action(self):
                self.cb_update((-1, 0, 0, 0, 0, -1, 0, -1, 0))
                self.b, self.c = self.c, self.b
                self.e, self.f = self.f, self.e

            def n3_true_action(self):
                lt = self.eps_lt
                i, j, k = 1, 1, 1
                if lt(self.d, 0):
                    i = -1
                if lt(self.e, 0):
                    j = -1
                if lt(self.f, 0):
                    k = -1
                self.cb_update((i, 0, 0, 0, j, 0, 0, 0, k))
                self.d = abs(self.d)
                self.e = abs(self.e)
                self.f = abs(self.f)

            def n3_false_action(self):
                lt = self.eps_lt
                gt = self.eps_gt
                f = [1, 1, 1]
                z = -1
                if gt(self.d, 0):
                    f[0] = -1
                elif not lt(self.d, 0):
                    z = 0
                if gt(self.e, 0):
                    f[1] = -1
                elif not lt(self.e, 0):
                    z = 1
                if gt(self.f, 0):
                    f[2] = -1
                elif not lt(self.f, 0):
                    z = 2
                if f[0] * f[1] * f[2] < 0:
                    assert z != -1
                    f[z] = -1
                self.cb_update((f[0], 0, 0, 0, f[1], 0, 0, 0, f[2]))
                self.d = -abs(self.d)
                self.e = -abs(self.e)
                self.f = -abs(self.f)

        kg = KG_Object(max_iterations=max_it)
        kg.a = self.lattice.lattice_parameters[0] ** 2.0
        kg.b = self.lattice.lattice_parameters[1] ** 2.0
        kg.c = self.lattice.lattice_parameters[2] ** 2.0
        kg.d = (
            2.0
            * self.lattice.lattice_parameters[1]
            * self.lattice.lattice_parameters[2]
            * math.cos(self.lattice.lattice_parameters[3] * d2r)
        )
        kg.e = (
            2.0
            * self.lattice.lattice_parameters[0]
            * self.lattice.lattice_parameters[2]
            * math.cos(self.lattice.lattice_parameters[4] * d2r)
        )
        kg.f = (
            2.0
            * self.lattice.lattice_parameters[1]
            * self.lattice.lattice_parameters[0]
            * math.cos(self.lattice.lattice_parameters[5] * d2r)
        )
        kg.epsilon = self.lattice.lattice_volume() ** (1 / 3.0) * relative_epsilon
        while step(kg):
            pass
        if quick_return:
            return [
                kg.a ** 0.5,
                kg.b ** 0.5,
                kg.c ** 0.5,
                math.acos(kg.d / (2.0 * (kg.b ** 0.5) * (kg.c ** 0.5))) / d2r,
                math.acos(kg.e / (2.0 * (kg.a ** 0.5) * (kg.c ** 0.5))) / d2r,
                math.acos(kg.f / (2.0 * (kg.a ** 0.5) * (kg.b ** 0.5))) / d2r,
            ]
        # AT THIS STAGE, KEEP TO QUICK RETURN EQUALS TRUE- get strings and so on later- dhc 120314
        C_mat = kg._r_inv
        #        print C_mat
        import numpy
        from numpy import linalg as LA

        np = numpy
        #        C_mat = np.linalg.inv(kg._r_inv)

        # are these an acceptable group? Note the self._is_symmetry... thing may not be what is wanted
        def acceptable_group(ops, lattice, eta=1.0e-6):
            # ops are spacegroup.operations
            v1 = np.array([0.2, -1.2, 3.5])
            v2 = np.array([3.2, 1.2, -1.1])
            vdiff = np.linalg.norm(v1 - v2)
            invLattice = np.linalg.inv(lattice)
            for op in ops:
                frac_op_matrix, frac_trans_vec = self.make_cartesian_op_matrix_frac(op)
                cart_op = (
                    np.dot(invLattice, np.dot(frac_op_matrix, lattice)),
                    np.dot(frac_trans_vec, lattice),
                )
                if (
                    not abs(
                        vdiff
                        - np.linalg.norm(
                            np.dot(v1, cart_op[0]) - np.dot(v2, cart_op[0])
                        )
                    )
                    < eta
                ):
                    return False
            return True

        def calculate_lattice_parameters(lattice):
            return [
                np.dot(lattice[0], lattice[0]) ** 0.5,
                np.dot(lattice[1], lattice[1]) ** 0.5,
                np.dot(lattice[2], lattice[2]) ** 0.5,
                math.acos(
                    np.dot(lattice[1], lattice[2])
                    / (np.linalg.norm(lattice[1]) * np.linalg.norm(lattice[2]))
                )
                / d2r,
                math.acos(
                    np.dot(lattice[0], lattice[2])
                    / (np.linalg.norm(lattice[0]) * np.linalg.norm(lattice[2]))
                )
                / d2r,
                math.acos(
                    np.dot(lattice[0], lattice[1])
                    / (np.linalg.norm(lattice[0]) * np.linalg.norm(lattice[1]))
                )
                / d2r,
            ]

        def same_ordered_lattice_parameters(kg1, kg2, eta=0.001):
            kg1 = sorted(kg1)
            kg2 = sorted(kg2)
            #            print kg1, kg2
            return all([abs(kg1[i] - kg2[i]) < eta for i in range(6)])

        if enforce_setting:

            self.determine_space_group()
            potential_twists = [
                numpy.array(((0.0, 1.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, 1.0))),
                numpy.array(((0.0, 0.0, -1.0), (0.0, 1.0, 0.0), (1.0, 0.0, 0.0))),
                numpy.array(((-1.0, 0.0, 0.0), (0.0, 0.0, 1.0), (0.0, 1.0, 0.0))),
                numpy.array(((1.0, 0.0, 0.0), (0.0, 1.0, 0.0), (0.0, 0.0, 1.0))),
                numpy.array(((0.0, 0, 1.0), (1.0, 0.0, 0.0), (0.0, 1.0, 0.0))),
                numpy.array(((0.0, 1, 0.0), (0.0, 0.0, 1.0), (1.0, 0.0, 0.0))),
            ]

            kg_parameters = [
                kg.a ** 0.5,
                kg.b ** 0.5,
                kg.c ** 0.5,
                math.acos(kg.d / (2.0 * (kg.b ** 0.5) * (kg.c ** 0.5))) / d2r,
                math.acos(kg.e / (2.0 * (kg.a ** 0.5) * (kg.c ** 0.5))) / d2r,
                math.acos(kg.f / (2.0 * (kg.a ** 0.5) * (kg.b ** 0.5))) / d2r,
            ]

            for indxp, p in enumerate(potential_twists):
                #                 p = np.transpose(p)
                if acceptable_group(
                    [op for op in self.spacegroup.operations],
                    np.dot(
                        np.transpose(np.dot(C_mat, p)), self.lattice.lattice_vectors
                    ),
                ) and same_ordered_lattice_parameters(
                    kg_parameters,
                    calculate_lattice_parameters(
                        np.dot(
                            np.transpose(np.dot(C_mat, p)), self.lattice.lattice_vectors
                        )
                    ),
                ):
                    C_mat = np.dot(C_mat, p)
                    break
            else:
                raise CSPyException(
                    "Could not find an acceptable setting of the space group"
                )
            self.factorize_out_latt()
        a = numpy.array(
            list3matrixmatrix(
                (
                    (C_mat[0][0], C_mat[1][0], C_mat[2][0]),
                    (C_mat[0][1], C_mat[1][1], C_mat[2][1]),
                    (C_mat[0][2], C_mat[1][2], C_mat[2][2]),
                ),
                self.lattice.lattice_vectors,
            )
        )
        ainv = LA.inv(a)
        intermediate_inv_lattice_vectors = ainv.tolist()
        self.lattice.set_lattice_parameters(
            [
                np.dot(a[0], a[0]) ** 0.5,
                np.dot(a[1], a[1]) ** 0.5,
                np.dot(a[2], a[2]) ** 0.5,
                math.acos(
                    np.dot(a[1], a[2]) / (np.linalg.norm(a[1]) * np.linalg.norm(a[2]))
                )
                / d2r,
                math.acos(
                    np.dot(a[0], a[2]) / (np.linalg.norm(a[0]) * np.linalg.norm(a[2]))
                )
                / d2r,
                math.acos(
                    np.dot(a[0], a[1]) / (np.linalg.norm(a[0]) * np.linalg.norm(a[1]))
                )
                / d2r,
            ]
        )
        rot_mat = list3matrixmatrix(
            intermediate_inv_lattice_vectors, self.lattice.lattice_vectors
        )
        if len(self.unique_molecule_list) == 0:
            crystal_logger.info(
                "Not populated molecule list-- must do this to use full functionality of crystal structures"
            )
            self.populate_unique_molecule_list()
        for im, mol in enumerate(self.unique_molecule_list):
            # Rotate atoms into new frame, which is aligned with C vector along cartesian Z
            for ia, atom in enumerate(self.unique_molecule_list[im].atoms):
                self.unique_molecule_list[im].atoms[ia].xyz = list3vecmatrix(
                    atom.xyz, rot_mat
                )
        # Move molecules so that they all fit in unit cell (0-1)
        self.move_all_molecules_into_uc()
        if return_transformation_mat:
            return rot_mat
        return self.lattice.lattice_parameters  # niggli_obs

    def make_estimate_partial_charge_file(
        self,
        data_pickle=None,
        displacements=None,
        atom_index_dict=None,
        punch_name=None,
        predictors=None,
        fixed_charge_filename=None,
    ):
        def re_summed_vector(vec_in, sum_target):
            sum_in = (sum(vec_in) - sum_target) / float(len(vec_in))
            return [x - sum_in for x in vec_in]

        estimated_charges = []
        label_charges = {}
        label = "DUMMY"
        if fixed_charge_filename is not None:
            f = open(fixed_charge_filename, "r")
            for line in f:
                if line.strip() == "":
                    continue
                try:
                    label_charges[label] = float(line.strip())
                except ValueError as exc:
                    label = line.strip().split()[0]
            f.close()
            for mol in self.unique_molecule_list:
                for atom in mol.atoms:
                    label = atom.custom_label
                    estimated_charges.append(label_charges[label])
        else:
            for mol in self.unique_molecule_list:
                for atom in mol.atoms:
                    #                    estimated_charges.append(predictors[atom.label].predict(np.array(displacements), eval_MSE=False))
                    if isinstance(predictors, dict):
                        estimated_charges.append(
                            predictors[atom.label].predict(
                                np.array(displacements), eval_MSE=False
                            )
                        )
                    else:
                        estimated_charges.append(
                            predictors[atom_index_dict[atom.label]].predict(
                                np.array(displacements), eval_MSE=False
                            )
                        )

        partial_charges = re_summed_vector(estimated_charges, 0.0)
        #        punch_file = open(struct_input.options['Punch Filename'], 'w+')
        punch_file = open(punch_name, "w+")
        i = 0
        for mol in self.unique_molecule_list:
            for atom in mol.atoms:
                #                print "potential_label", atom.potential_label;exit()
                #                punch_file.write(atom.label + ' ' + str(atom.xyz[0]) + ' ' + str(atom.xyz[1]) +
                punch_file.write(
                    atom.custom_label
                    + " "
                    + str(atom.xyz[0])
                    + " "
                    + str(atom.xyz[1])
                    + " "
                    + str(atom.xyz[2])
                    + " Rank 0 \n"
                )
                punch_file.write(str(partial_charges[i]) + "\n")
                i += 1
        punch_file.close()

    def generate_supercell(self, size, cutoff=None, label_mols=False):
        """Generates a supercell containing (2*size + 1) x (2*size + 1)
        x (2*size + 1) copies of the unit cell
        """
        supercell = []
        for i, molecule in enumerate(self.unique_molecule_list):

            # label the molecules so we know which molecule the
            # molecule was copied from
            if label_mols:
                molecule.name = str(i)

            for trans in self.lattice.create_translations(size, cutoff):
                mol_copy = copy.deepcopy(molecule)
                for atom in mol_copy.atoms:
                    for coord in [0, 1, 2]:
                        atom.xyz[coord] = atom.xyz[coord] + trans[coord]
                supercell.append(mol_copy)

        return supercell

    def get_dimers(self, buffer=0.0, eps=0.000001, cutoff=25):
        """For each molecule in the unit cell look for other molecules
        in close contact and return a list of dimers. Molecules are
        defined to be in close contact when any atom-atom distance
        between the two molecules are less then the sum of the vdw
        radius of the two atoms plus a specified buffer.
        """
        self.expand_to_full_unit_cell()
        self.check_asymmetric_unit()

        # workout of what the dimensions of the supercell will need to
        # be to ensure all unit cells within the cutoff + buffer from
        # the center of the first unit cell are included
        cutoff += buffer

        mag_a = list3mag(self.lattice.lattice_vectors[0])
        mag_b = list3mag(self.lattice.lattice_vectors[1])
        mag_c = list3mag(self.lattice.lattice_vectors[2])

        area_ab = list3mag(
            list3cross(self.lattice.lattice_vectors[0], self.lattice.lattice_vectors[1])
        )
        area_ac = list3mag(
            list3cross(self.lattice.lattice_vectors[0], self.lattice.lattice_vectors[2])
        )
        area_bc = list3mag(
            list3cross(self.lattice.lattice_vectors[1], self.lattice.lattice_vectors[2])
        )

        depth_a = int(math.ceil((cutoff / min(area_ab / mag_b, area_ac / mag_c)) - 0.5))
        depth_b = int(math.ceil((cutoff / min(area_ab / mag_a, area_bc / mag_c)) - 0.5))
        depth_c = int(math.ceil((cutoff / min(area_ac / mag_a, area_bc / mag_b)) - 0.5))

        supercell = self.generate_supercell(
            [depth_a, depth_b, depth_c], cutoff, label_mols=True
        )

        dimers = []
        for mol_i, mol_j in it.product(self.unique_molecule_list, supercell):

            # continue as they are the same molecule
            if list3norm(mol_i.centroid(), mol_j.centroid()) < eps:
                continue

            for atom_i, atom_j in it.product(mol_i.atoms, mol_j.atoms):
                xyzdist = list3norm(atom_i.xyz, atom_j.xyz)
                vdwdist = atom_i.VdW_radius() + atom_j.VdW_radius() + buffer

                if xyzdist < vdwdist:
                    dimers.append([copy.deepcopy(mol_i), copy.deepcopy(mol_j)])
                    break

        return dimers

    def inflate_cell(self, inflation_factor=1.0):
        """
        Expands the lattice by inflation_factor (isotropic)
        Rescales the positions of the centroid for each molecule 
        in unique_molecule_list accordingly
        (does *not* distort the molecules)
        """
        self.aniso_inflate_cell([inflation_factor] * 3)

    def aniso_inflate_cell(self, inflation_factor=[1.0, 1.0, 1.0]):
        """
        Expands the lattice by an anisotropic inflation tensor.
        Rescales the positions of the centroid for each molecule 
        in unique_molecule_list accordingly
        (does *not* distort the molecules)
        """
        from copy import deepcopy
        from cspy.deprecated.listmath import list3diff

        self.expand_to_full_unit_cell()
        for molecule in self.unique_molecule_list:
            scaled_centroid = list(deepcopy(molecule.centroid()))
            for coord in [0, 1, 2]:
                scaled_centroid[coord] *= inflation_factor[coord]
            xyzdisplacement = list3diff(scaled_centroid, list(molecule.centroid()))
            for atom in molecule.atoms:
                for coord in [0, 1, 2]:
                    atom.xyz[coord] += xyzdisplacement[coord]
        self.lattice.aniso_expand_lattice(inflation_factor)

    def insert_guest(
        self,
        guests,
        num_insertions=1,
        max_insertions=100,
        use_void_centroid=False,
        use_void_shape=False,
    ):
        """
        Insertion of a collection of guest molecules in a host structure
        """
        from copy import deepcopy

        # Initialization
        successful_insertions = 0
        tried_insertions = 0
        guest_host_structures = []
        seed = np.random.randint(0, 100)
        if use_void_centroid:
            if len(guests) > len(self.void_centroids()):
                err_msg = "Trying to insert more guests than the number of available void positions"
                crystal_logger.error(str(err_msg))
                raise CSPyException(err_msg)
        # Main loop
        while (
            successful_insertions < num_insertions and tried_insertions < max_insertions
        ):
            initial_crystal = deepcopy(self)
            for guest in guests:
                insert_ok = False
                while (not insert_ok) and (tried_insertions < max_insertions):
                    (
                        insert_ok,
                        new_crystal,
                        seed,
                    ) = initial_crystal.simple_insertion_single_guest(
                        guest, seed, use_void_centroid, use_void_shape
                    )
                    if insert_ok:
                        initial_crystal = deepcopy(new_crystal)
                    tried_insertions += 1
                    crystal_logger.info("Try #%d complete", tried_insertions)
            if insert_ok:
                guest_host_structures.append(initial_crystal)
                successful_insertions += 1
                crystal_logger.info(
                    "Found a total of %d guest-host structures", successful_insertions
                )
        # Output
        crystal_logger.info("Made a total %d tries", tried_insertions)
        return guest_host_structures

    def simple_insertion_single_guest(
        self,
        guest,
        seed=np.random.randint(0, 100),
        use_void_centroid=False,
        use_void_shape=False,
    ):
        from copy import deepcopy
        from sobolf90 import i4_sobol
        from cspy.deprecated.listmath import make_quat_rot1
        from np.random import randint, random_sample
        from math import floor, pi

        supercell = self.generate_supercell(1)
        sobol_seed = seed
        new_guest = deepcopy(guest)

        def fully_random_position(sobol_seed):
            rand_num = np.asfortranarray(np.zeros(6), dtype=np.float32)
            rand_num, sobol_seed = i4_sobol(sobol_seed, rand_num, dim_num=6)
            rand_num = np.ascontiguousarray(rand_num, dtype=np.float32)
            rand_vec = [0.0, 0.0, 0.0]
            for coord in [0, 1, 2]:
                rand_vec[coord] += rand_num[0] * self.lattice.lattice_vectors[0][coord]
                rand_vec[coord] += rand_num[1] * self.lattice.lattice_vectors[1][coord]
                rand_vec[coord] += rand_num[2] * self.lattice.lattice_vectors[2][coord]
            rand_quat = make_quat_rot1([rand_num[3], rand_num[4], rand_num[5]])
            new_guest.rotate_atoms(rand_quat)
            for atom in new_guest.atoms:
                for coord in [0, 1, 2]:
                    atom.xyz[coord] += rand_vec[coord]
            return sobol_seed

        def fully_random_orientation(sobol_seed):
            rand_num = np.asfortranarray(np.zeros(3), dtype=np.float32)
            rand_num, sobol_seed = i4_sobol(sobol_seed, rand_num, dim_num=3)
            rand_num = np.ascontiguousarray(rand_num, dtype=np.float32)
            rand_index = randint(len(self.void_centroids()))
            void_centroid = self.void_centroids()[rand_index]
            centroid_frac = [0.0, 0.0, 0.0]
            for coord in [0, 1, 2]:
                centroid_frac[coord] = float(void_centroid[coord]) - floor(
                    float(void_centroid[coord])
                )
            centroid_vec = [0.0, 0.0, 0.0]
            for coord in [0, 1, 2]:
                centroid_vec[coord] += (
                    centroid_frac[0] * self.lattice.lattice_vectors[0][coord]
                )
                centroid_vec[coord] += (
                    centroid_frac[1] * self.lattice.lattice_vectors[1][coord]
                )
                centroid_vec[coord] += (
                    centroid_frac[2] * self.lattice.lattice_vectors[2][coord]
                )
            rand_quat = make_quat_rot1([rand_num[0], rand_num[1], rand_num[2]])
            new_guest.rotate_atoms(rand_quat)
            for atom in new_guest.atoms:
                for coord in [0, 1, 2]:
                    atom.xyz[coord] += centroid_vec[coord]
            return sobol_seed

        def oriented_random_rotation(sobol_seed):
            from cspy.deprecated.listmath import (
                rot_vec_around_dir,
                get_alignment_matrix,
            )

            rand_angle = np.asfortranarray(np.zeros(1), dtype=np.float32)
            rand_angle, sobol_seed = i4_sobol(sobol_seed, rand_angle, dim_num=1)
            rand_angle = np.ascontiguousarray(rand_angle, dtype=np.float32)
            rand_angle = random_sample() * (2 * pi)
            rand_index = randint(len(self.void_centroids()))
            void_centroid = self.void_centroids()[rand_index]
            centroid_frac = [0.0, 0.0, 0.0]
            for coord in [0, 1, 2]:
                centroid_frac[coord] = float(void_centroid[coord]) - floor(
                    float(void_centroid[coord])
                )
            centroid_vec = [0.0, 0.0, 0.0]
            for coord in [0, 1, 2]:
                centroid_vec[coord] += (
                    centroid_frac[0] * self.lattice.lattice_vectors[0][coord]
                )
                centroid_vec[coord] += (
                    centroid_frac[1] * self.lattice.lattice_vectors[1][coord]
                )
                centroid_vec[coord] += (
                    centroid_frac[2] * self.lattice.lattice_vectors[2][coord]
                )
            new_guest.calculate_moments_inertia()
            guest_axis = new_guest.moments_inertia_eig_vecs[0]
            void_eigenvector = self.void_eigenvectors()[rand_index][0]
            m = get_alignment_matrix(guest_axis, void_eigenvector)
            for atom in new_guest.atoms:
                atom.xyz = list(np.dot(m, np.array(atom.xyz)))
            for atom in new_guest.atoms:
                atom.xyz = list(
                    rot_vec_around_dir(atom.xyz, void_eigenvector, rand_angle)
                )
            for atom in new_guest.atoms:
                for coord in [0, 1, 2]:
                    atom.xyz[coord] += centroid_vec[coord]
            return sobol_seed

        # Pick random position/orientation of the guest molecule
        overlap = False
        if use_void_centroid:
            if len(self.void_centroids()) == 0:
                err_msg = (
                    "PLATON could not find any voids in the structure\n"
                    "Try to modify the probe radius (--probe_radius)"
                )
                crystal_logger.error(str(err_msg))
                raise CSPyException(str(err_msg))
            if use_void_shape:
                sobol_seed = oriented_random_rotation(sobol_seed)
            else:
                sobol_seed = fully_random_orientation(sobol_seed)
        else:
            sobol_seed = fully_random_position(sobol_seed)
        # Look for a possible overlap between the guest and the crystal
        supercell_molecule_index = 0
        supercell_molecule_index_max = len(supercell)
        while (not overlap) and (
            supercell_molecule_index < supercell_molecule_index_max
        ):
            molecule = supercell[supercell_molecule_index]
            guest_atom_index = 0
            guest_atom_index_max = len(new_guest.atoms)
            while (not overlap) and (guest_atom_index < guest_atom_index_max):
                guest_atom = new_guest.atoms[guest_atom_index]
                molecule_atom_index = 0
                molecule_atom_index_max = len(molecule.atoms)
                while (not overlap) and (molecule_atom_index < molecule_atom_index_max):
                    molecule_atom = molecule.atoms[molecule_atom_index]
                    xyzdist = list3norm(guest_atom.xyz, molecule_atom.xyz)
                    VdWdist = guest_atom.VdW_radius() + molecule_atom.VdW_radius()
                    if xyzdist < VdWdist:
                        overlap = True
                    molecule_atom_index += 1
                molecule_atom_index = 0
                guest_atom_index += 1
            guest_atom_index = 0
            supercell_molecule_index += 1
        # No overlap => keep guest-host structure
        new_crystal = deepcopy(self)
        if not overlap:
            new_crystal.unique_molecule_list.append(new_guest)
        return (not overlap, new_crystal, sobol_seed)

    def orient_to_miller_plane(self, miller_indicies):
        crystal_logger.info(list(map(str, miller_indicies)))
        crystal_logger.info(str(self.lattice.lattice_volume()))
        self.expand_to_full_unit_cell()
        self.check_asymmetric_unit()
        crystal_logger.info("Show[")
        crystal_logger.info('Graphics3D[Text["O",{ 0, 0 , 0 }]],')
        crystal_logger.info('Graphics3D[Text["X",{ 200, 0 , 0 }]],')
        crystal_logger.info('Graphics3D[Text["Y",{ 0, 200, 0 }]],')
        crystal_logger.info('Graphics3D[Text["Z",{ 0, 0, 200}]],')
        for v in self.lattice.lattice_vectors:
            crystal_logger.info(
                "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v, 100.0)
            )
        crystal_logger.info('ImportString["')
        for i, mol in enumerate(self.unique_molecule_list):
            for j, atom in enumerate(mol.atoms):
                crystal_logger.info(
                    atom.clean_label(),
                    str(atom.xyz[0]),
                    str(atom.xyz[1]),
                    str(atom.xyz[2]),
                )
        crystal_logger.info('","XYZ"]]')

        xy_i = []  # which lattice vectors will form in xy plane
        new_vecs = [[], [], []]
        if miller_indicies.count(0) == 2:
            # print "special case of 2 zeros"
            for i, ind in enumerate(miller_indicies):
                if ind == 0:
                    xy_i += [i]
            new_vecs[0] = self.lattice.lattice_vectors[xy_i[0]]
            new_vecs[1] = self.lattice.lattice_vectors[xy_i[1]]
            new_vecs[2] = self.lattice.lattice_vectors[
                [i for i, ind in enumerate(miller_indicies) if ind != 0][0]
            ]
        elif miller_indicies.count(0) == 1:
            # print "special case of 1 zeros"
            # take last valued index as the z direction
            xy_i = miller_indicies.index(0)
            # print xy_i
            new_vec = [0, 0, 0]
            commonf = 1
            for ind in miller_indicies:
                if ind == 0:
                    continue
                commonf = commonf * ind
            for i, ind in enumerate(miller_indicies):
                if ind == 0:
                    continue
                new_vec = list3add(
                    list3multiply(self.lattice.lattice_vectors[i], commonf / ind),
                    new_vec,
                )
            new_vecs[0] = self.lattice.lattice_vectors[xy_i]
            new_vecs[1] = new_vec
            new_vecs[2] = self.lattice.lattice_vectors[
                [i for i, ind in enumerate(miller_indicies) if ind != 0][-1]
            ]
        else:
            # print "Hardest case of no zeros"
            new_vec = [0, 0, 0]
            commonf = 1
            for i, ind in enumerate(miller_indicies):
                if i == 2:
                    continue
                commonf = commonf * ind
            neg = -1
            for i, ind in enumerate(miller_indicies):
                if i == 2:
                    continue
                new_vec = list3add(
                    list3multiply(self.lattice.lattice_vectors[i], neg * commonf / ind),
                    new_vec,
                )
                neg = 1
            new_vecs[0] = copy.deepcopy(new_vec)
            new_vec = [0, 0, 0]
            commonf = 1
            for i, ind in enumerate(miller_indicies):
                if i == 1:
                    continue
                commonf = commonf * ind
            neg = -1
            for i, ind in enumerate(miller_indicies):
                if i == 1:
                    continue
                new_vec = list3add(
                    list3multiply(self.lattice.lattice_vectors[i], neg * commonf / ind),
                    new_vec,
                )
                neg = 1
            new_vecs[1] = copy.deepcopy(new_vec)
            new_vec = [0, 0, 0]
            commonf = 1
            for i, ind in enumerate(miller_indicies):
                if i == 1:
                    continue
                commonf = commonf * ind
            neg = 1
            for i, ind in enumerate(miller_indicies):
                if i == 1:
                    continue
                # print neg*commonf/ind, neg, commonf, ind
                new_vec = list3add(
                    list3multiply(self.lattice.lattice_vectors[i], neg * commonf / ind),
                    new_vec,
                )
                neg = 1
            new_vecs[2] = copy.deepcopy(new_vec)
            # new_vecs[2] = copy.deepcopy(list3multiply(self.lattice.lattice_vectors[2], miller_indicies[2])
            # for v in new_vecs:
            # print "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v,100.)

            pass

        self.lattice.lattice_vectors = new_vecs
        self.lattice.calc_inv_lattice_vectors()
        # Rotate entire system so that self.lattice.lattice_vectors[0] point in x direction
        # 1) rotate about y, then z
        xang = list3angle(
            [
                self.lattice.lattice_vectors[0][0],
                0.0,
                self.lattice.lattice_vectors[0][2],
            ],
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
        )
        # print xang
        if self.lattice.lattice_vectors[0][2] < 0:
            xang = -xang
        for i, vec in enumerate(self.lattice.lattice_vectors):
            self.lattice.lattice_vectors[i] = rotation_by_q(vec, xang, [0.0, 1.0, 0.0])
        for i, mol in enumerate(self.unique_molecule_list):
            for j, atom in enumerate(mol.atoms):
                self.unique_molecule_list[i].atoms[j].xyz = rotation_by_q(
                    atom.xyz, xang, [0.0, 1.0, 0.0]
                )
        # for v in self.lattice.lattice_vectors:
        #    print "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v,100.)

        xang = list3angle(
            [
                self.lattice.lattice_vectors[0][0],
                self.lattice.lattice_vectors[0][1],
                0.0,
            ],
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
        )
        # print xang
        if self.lattice.lattice_vectors[0][1] > 0:
            xang = -xang
        for i, vec in enumerate(self.lattice.lattice_vectors):
            self.lattice.lattice_vectors[i] = rotation_by_q(vec, xang, [0.0, 0.0, 1.0])
        for i, mol in enumerate(self.unique_molecule_list):
            for j, atom in enumerate(mol.atoms):
                self.unique_molecule_list[i].atoms[j].xyz = rotation_by_q(
                    atom.xyz, xang, [0.0, 0.0, 1.0]
                )
        # for v in self.lattice.lattice_vectors:
        #    print "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v,100.)

        xang = list3angle(
            [
                0.0,
                self.lattice.lattice_vectors[1][1],
                self.lattice.lattice_vectors[1][2],
            ],
            [0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0],
        )
        if self.lattice.lattice_vectors[1][2] > 0:
            xang = -xang
        # print xang
        for i, vec in enumerate(self.lattice.lattice_vectors):
            # print self.lattice.lattice_vectors[i], rotation_by_q(vec, xang, [1.,0.,0.])
            self.lattice.lattice_vectors[i] = rotation_by_q(vec, xang, [1.0, 0.0, 0.0])
        for i, mol in enumerate(self.unique_molecule_list):
            for j, atom in enumerate(mol.atoms):
                self.unique_molecule_list[i].atoms[j].xyz = rotation_by_q(
                    atom.xyz, xang, [1.0, 0.0, 0.0]
                )
                # print self.unique_molecule_list[i].atoms[j]
        # for v in self.lattice.lattice_vectors:
        #    print "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v,100.)
        if self.lattice.lattice_vectors[2][2] > 0:
            xang = math.pi
            for i, vec in enumerate(self.lattice.lattice_vectors):
                # print self.lattice.lattice_vectors[i], rotation_by_q(vec, xang, [1.,0.,0.])
                self.lattice.lattice_vectors[i] = rotation_by_q(
                    vec, xang, [1.0, 0.0, 0.0]
                )
            for i, mol in enumerate(self.unique_molecule_list):
                for j, atom in enumerate(mol.atoms):
                    self.unique_molecule_list[i].atoms[j].xyz = rotation_by_q(
                        atom.xyz, xang, [1.0, 0.0, 0.0]
                    )
        # print self.lattice.lattice_vectors
        # print self.lattice.lattice_volume()
        self.lattice.calc_inv_lattice_vectors()
        # self.check_asymmetric_unit()
        crystal_logger.info("Show[")
        crystal_logger.info('Graphics3D[Text["O",{ 0, 0 , 0 }]],')
        crystal_logger.info('Graphics3D[Text["X",{ 200, 0 , 0 }]],')
        crystal_logger.info('Graphics3D[Text["Y",{ 0, 200, 0 }]],')
        crystal_logger.info('Graphics3D[Text["Z",{ 0, 0, 200 }]],')
        for v in self.lattice.lattice_vectors:
            crystal_logger.info(
                "Graphics3D[Arrow[{{0,0,0},{ %f, %f, %f }}]]," % list3multiply(v, 100.0)
            )
        crystal_logger.info('ImportString["')
        for i, mol in enumerate(self.unique_molecule_list):
            for j, atom in enumerate(mol.atoms):
                crystal_logger.info(
                    atom.clean_label(),
                    str(atom.xyz[0]),
                    str(atom.xyz[1]),
                    str(atom.xyz[2]),
                )
        crystal_logger.info('","XYZ"]]')

    def hexagonal_to_rhombohedral_setting(self, sg=None):
        return self.rhombohedral_to_hexagonal_setting(sg=sg, reverse_direction=True)

    def rhombohedral_to_hexagonal_setting(self, sg=None, reverse_direction=False):
        """ Specify a space group, and self == a rhombohedral cell (1 variable angle and 1 variable length)
            This returns a hexagonal cell (angles 2pi/3 and pi/3)- N.B. add more space groups when needed  
            also note- this subroutine is intended to be used before writing res files- if you want to use 
            in other ways, please test for latent problems
            dhc 270215 """
        from cspy.deprecated.listmath import list3angle
        from cspy.deprecated.spacegroups import expand_symmetry_list

        available_space_group_operators = {
            146: ["-y,x-y,z", "-x+y,-x,z"],
            148: ["-y,x-y,z", "-x+y,-x,z"],
            155: ["-y,x-y,z", "-x+y,-x,z", "y,x,-z", "x-y,-y,-z", "-x,-x+y,-z"],
            160: ["-y,x-y,z", "-x+y,-x,z", "-y,-x,z", "-x+y,y,z", "x,x-y,z"],
            161: [
                "-y,x-y,z",
                "-x+y,-x,z",
                "-y,-x,0.5+z",
                "-x+y,y,0.5+z",
                "x,x-y,0.5+z",
            ],
            166: [
                "-y,x-y,z",
                "-x+y,-x,z",
                "-y,-x,z",
                "-x+y,y,z",
                "x,x-y,z",
                "-x,-y,-z",
                "y,-x+y,-z",
                "x-y,x,-z",
                "y,x,-z",
                "x-y,-y,-z",
                "-x,-x+y,-z",
            ],
            167: [
                "-y,x-y,z",
                "-x+y,-x,z",
                "-y,-x,0.5+z",
                "-x+y,y,0.5+z",
                "x,x-y,0.5+z",
            ],
        }
        reverse_space_group_operators = {146: ["z,x,y", "y,z,x"]}
        available_latt_types = {
            146: "-3",
            148: "3",
            155: "-3",
            160: "-3",
            161: "-3",
            166: "3",
            167: "3",
        }
        reverse_latt_types = {146: "-1"}
        crystal_logger.info(str(sg), str(reverse_direction))
        if (not reverse_direction and sg not in available_space_group_operators) or (
            reverse_direction and sg not in reverse_space_group_operators
        ):
            raise CSPyException(
                "Space group %s is not available in hex setting. It's easy to add!"
                % (sg)
            )

        C_mat = ((1.0, -1.0, 0.0), (-1.0, 0.0, 1.0), (-1.0, -1.0, -1.0))
        if reverse_direction:
            C_mat = np.linalg.inv(C_mat)

        hexA, hexB, hexC = list3matrixmatrix(C_mat, self.lattice.lattice_vectors)
        self.set_lattice_parameters(
            [
                list3mag(hexA),
                list3mag(hexB),
                list3mag(hexC),
                list3angle(hexB, [0.0, 0.0, 0.0], hexC) * 180.0 / math.pi,
                list3angle(hexA, [0.0, 0.0, 0.0], hexC) * 180.0 / math.pi,
                list3angle(hexA, [0.0, 0.0, 0.0], hexB) * 180.0 / math.pi,
            ]
        )

        for a in range(3):
            for b in range(3):
                if np.isclose(
                    self.lattice.lattice_parameters[a],
                    self.lattice.lattice_parameters[b],
                    rtol=1e-4,
                    atol=1e-4,
                ):
                    self.lattice.lattice_parameters[
                        b
                    ] = self.lattice.lattice_parameters[a]
        for a in range(3, 6):
            if np.isclose(
                90.0, self.lattice.lattice_parameters[a], rtol=1e-4, atol=1e-4
            ):
                self.lattice.lattice_parameters[a] = 90.0
            if np.isclose(
                120.0, self.lattice.lattice_parameters[a], rtol=1e-4, atol=1e-4
            ):
                self.lattice.lattice_parameters[a] = 120.0

        intermediate_inv_lattice_vectors = np.linalg.inv(
            np.array([hexA, hexB, hexC])
        ).tolist()
        rot_mat = list3matrixmatrix(
            intermediate_inv_lattice_vectors, self.lattice.lattice_vectors
        )

        for im, mol in enumerate(self.unique_molecule_list):
            for ia, atom in enumerate(self.unique_molecule_list[im].atoms):
                self.unique_molecule_list[im].atoms[ia].xyz = list3vecmatrix(
                    atom.xyz, rot_mat
                )

        # Note that you could add all the symmetry elements to available...operators, and then factorize out latt, but this is easier
        if reverse_direction:
            self.symm = reverse_space_group_operators[sg]
            self.latt = reverse_latt_types[sg]
        else:
            self.symm = available_space_group_operators[sg]
            self.latt = available_latt_types[sg]
        # self.factorize_out_latt()
        self.zerr = (
            str(
                len(self.unique_molecule_list)
                * len(expand_symmetry_list(self.symm, int(self.latt)))
            )
            + " 0 0 0"
        )
        return self.lattice.lattice_parameters


def main():
    import argparse

    from cspy.deprecated.logging_utils import setup_cspy_logger
    from cspy.deprecated.gaussian_utils import (
        add_gaussian_argument_group,
        gaussian_parse_args,
    )

    import sys

    crystal_logger = logging.getLogger("CSPy.crystal")

    parser = argparse.ArgumentParser(
        description="Crystal modification utility",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("filename", type=str, help="Structure filename")
    parser.add_argument(
        "--log-level",
        type=str,
        choices=("INFO", "DEBUG", "WARN", "ERROR"),
        default="INFO",
        help="Level of verbosity for debugging",
    )
    parser.add_argument(
        "-dbg", "--debug_filename", type=str, default=None, help="Debugging output"
    )
    parser.add_argument(
        "--zprime_expand",
        action="store_true",
        help="Create a structure with z'>=1 symmetry",
        default=False,
    )
    parser.add_argument(
        "--full_cell", action="store_true", help="Remove z'<1 symmetry", default=False
    )
    parser.add_argument(
        "--make_primitive",
        action="store_true",
        help="Return primitive (P1) unit cell with ZERR = no of formula units in cell.",
        default=False,
    )
    parser.add_argument(
        "--remove_coincident_atoms",
        action="store_true",
        help="Remove z'<1 symmetry",
        default=False,
    )
    parser.add_argument(
        "--res_print",
        action="store_true",
        help="Print res file after each program",
        default=False,
    )
    parser.add_argument(
        "--res_output_filename", type=str, help="write res file to this", default=""
    )
    parser.add_argument(
        "--xyz_output_filename", type=str, help="write res file to this", default=""
    )
    parser.add_argument(
        "--cif_print",
        action="store_true",
        help="Print res file after each program",
        default=False,
    )
    parser.add_argument(
        "--cif_output_filename", type=str, help="write res file to this", default=""
    )
    parser.add_argument("--cif_name", type=str, help="cif name for data", default="")
    parser.add_argument(
        "--optimise_exp_structure",
        action="store_true",
        help="optimise experimental structure and place back into cell",
        default=False,
    )
    parser.add_argument(
        "--check_asymm",
        action="store_true",
        help="Ensure fractional co-ordinates of the centroid of the asymmetric unit molecules are in region 0.0 to 1.0",
        default=False,
    )
    parser.add_argument(
        "--arrange_hydrogens",
        action="store_true",
        help="Print res file with hydrogens at the end",
        default=False,
    )
    parser.add_argument(
        "--standard_atom_order",
        action="store_true",
        help="Arrange the atoms in the molecule into a preset element order",
        default=False,
    )
    parser.add_argument(
        "--connectivity_info",
        action="store_true",
        help="Print res file after each program",
        default=False,
    )
    parser.add_argument(
        "--clean_symm",
        action="store_true",
        help="Rewrites symmetry operations to standard 1/4+X format for compatibility with external programs.",
        default=False,
    )
    parser.add_argument(
        "--factorize_latt",
        action="store_true",
        help="Rewrites symmetry operations and LATT for compatibility with external programs.",
        default=False,
    )
    parser.add_argument(
        "--embe_gen",
        action="store_true",
        help="Generate EMBE input file",
        default=False,
    )
    parser.add_argument(
        "--platon", action="store_true", help="Run platon on res", default=""
    )
    parser.add_argument(
        "--apply_pbc", action="store_true", help="Apply pbc to crystal", default=False
    )
    parser.add_argument(
        "--pbc_expansion_level",
        type=int,
        help="Number of neighbouring cells to check",
        default=7,
    )
    parser.add_argument(
        "--niggli",
        action="store_true",
        help="WARNING! Experimental feature! Reduce cell parameters to Niggli cell.",
        default=False,
    )
    parser.add_argument(
        "--simple_molecule_overlay",
        action="store_true",
        help="WARNING! Experimental feature! Overlay the molecule from other to the main structure",
        default=False,
    )
    parser.add_argument(
        "--other",
        type=str,
        nargs="*",
        help="WARNING! Experimental feature! Other crystal",
        default=[],
    )
    parser.add_argument(
        "-rms",
        "--rmsd_tolerance",
        type=float,
        default=0.1,
        help="WARNING! Experimental feature! Other crystal",
    )
    parser.add_argument(
        "-maxd",
        "--maxd_tolerance",
        type=float,
        default=0.005,
        help="WARNING! Experimental feature! Other crystal",
    )
    parser.add_argument(
        "--match_structure_to_template",
        type=str,
        help="applies symmetry operations to molecule, such that it matches the arrangement in a template (important to match .mult files)",
        default="",
    )
    add_gaussian_argument_group(parser)
    parser.add_argument(
        "--dimer_extract_all",
        action="store_true",
        help="Creates xyz files for all dimers consisting of monomers in close contact",
    )
    parser.add_argument(
        "--dimer_extract_uni",
        action="store_true",
        help="Creates xyz files for all unique dimers consisting of monomers in close contact",
    )
    parser.add_argument(
        "--dimer_buffer",
        type=float,
        default=1.5,
        help="Buffer to be added to VDW radius when determing if monomers are in close contact when extracting dimers",
    )
    parser.add_argument(
        "--dimer_cutoff",
        type=float,
        default=25.0,
        help="Cutoff used when creating a supercell cluster for close contact dimer extraction",
    )
    parser.add_argument(
        "--kabsch_rmsd",
        type=float,
        default=0.001,
        help="RMSD threshold for kabsch algorithm",
    )
    parser.add_argument("--typing", type=str, help="")
    parser.add_argument(
        "--orient_miller", type=str, help="Miller index for new orientation"
    )
    parser.add_argument(
        "--make_castep_cell",
        action="store_true",
        default=False,
        help="Make a basic .cell file",
    )
    parser.add_argument(
        "--calc_void", action="store_true", default=False, help="Perform void analysis"
    )
    parser.add_argument(
        "--make_D_H",
        action="store_true",
        default="",
        help="Replace any Deuterium with Hydrogen.",
    )
    parser.add_argument(
        "--sort_atoms",
        action="store_true",
        default=False,
        help="Sort the atoms in each molecule into the same order.",
    )
    parser.add_argument(
        "--find-redundant-conformers",
        action="store_true",
        default=False,
        help="Find only unique conformers in set",
    )

    # Alternative insertion procedure-related keywords
    parser.add_argument(
        "--guest",
        nargs="+",
        type=str,
        default=None,
        help="Path(s) to the guest molecule(s)",
    )
    parser.add_argument(
        "--inflation_factor",
        type=float,
        default=1.0,
        help="Inflation factor used in the " "'alternative_insertion' procedure",
    )
    parser.add_argument(
        "--inflation_tensor",
        nargs="+",
        type=float,
        default=[1.0, 1.0, 1.0],
        help="Inflation tensor used in the " "'alternative_insertion' procedure",
    )
    parser.add_argument(
        "--num_insertions",
        type=int,
        default=1,
        help="Number of generated structures in the "
        "'alternative_insertion' procedure",
    )
    parser.add_argument(
        "--max_insertions",
        type=int,
        default=100,
        help="Maximum number of tries in the " "'alternative_insertion' procedure",
    )
    parser.add_argument(
        "--multiple_insertions",
        type=int,
        default=1,
        help="Insert multiple occurrences of the same guest "
        "in a crystal during the 'alternative_insertion' "
        "procedure",
    )
    parser.add_argument(
        "--use_void_centroid",
        action="store_true",
        default=False,
        help="Put the guests at the centroid of the voids",
    )
    parser.add_argument(
        "--use_void_shape",
        action="store_true",
        default=False,
        help="Align the guests along the voids",
    )
    parser.add_argument(
        "--probe_radius",
        type=float,
        default=1.2,
        help="Probe radius used to calculate the void space " "of the structure",
    )
    parser.add_argument(
        "--void_grid",
        type=float,
        default=0.2,
        help="Grid used to calculate the void space of the " "structure (in Angstrom)",
    )
    parser.add_argument(
        "--generate_cocrystal",
        action="store_true",
        default=False,
        help="Generates a collection of cocrystals; "
        "equivalent to --insert_guest --use_void_centroid "
        "--use_void_shape",
    )
    parser.add_argument(
        "--insert_guest",
        action="store_true",
        default=False,
        help="Insertion of a guest molecule; use the "
        "--guest keyword to specify the path to the guest",
    )
    parser.add_argument(
        "--report",
        action="store_true",
        default=False,
        help="Reports crystal information",
    )
    parser.add_argument(
        "--check_same_molecules",
        action="store_true",
        default=False,
        help="checks list of molecules to indent....",
    )
    parser.add_argument(
        "--rhom_to_hex",
        type=int,
        default=None,
        help="Give the space group as argument, and this transforms the setting. Only some implemented",
    )
    parser.add_argument(
        "--hex_to_rhom",
        type=int,
        default=None,
        help="Give the space group as argument, and this transforms the setting. Only some implemented",
    )

    args = parser.parse_args()

    crystal_logger = logging.getLogger("CSPy.crystal")
    logger = setup_cspy_logger(args.debug_filename, level=args.log_level)

    if args.filename.lower().endswith(".xyz"):
        crystal = CrystalStructure()
        oxyz = [open(o).read() for o in args.other]
        crystal.init_from_xyz_string(
            open(args.filename).read(), args.filename, others=oxyz
        )
    elif args.filename.lower().endswith(".fchk"):
        crystal = CrystalStructure()
        crystal.init_from_fchk(args.filename)
    elif args.filename.lower().endswith(".geom"):
        crystal = CrystalStructure()
        crystal.init_from_castep_geom_str(open(args.filename).read())
    else:
        crystal = CrystalStructure(args.filename)

    if args.apply_pbc:
        mol = Molecule()
        for a in range(len(crystal.unique_molecule_list)):
            for b in range(len(crystal.unique_molecule_list[a].atoms)):
                mol.add_atom(crystal.unique_molecule_list[a].atoms[b])
        del crystal.unique_molecule_list[:]
        crystal.unique_molecule_list = mol.fragment_geometry(
            lattice=crystal.lattice, expansion_level=args.pbc_expansion_level
        )

    if args.set_molecular_states is not None:
        states = args.set_molecular_states.split(" ")
        if len(crystal.unique_molecule_list) != len(states):
            err_msg = "The number of molecules in the crystal is not equal to the number of molecular states specified; check it is a sequence of space separated tuples. Quitting."
            crystal_logger.error(err_msg)
            raise CSPyException(err_msg)
            exit()

        for i in range(len(states)):
            s = states[i].split(",")
            crystal.unique_molecule_list[i].charge = s[0]
            crystal.unique_molecule_list[i].spin_multiplicity = s[1]

    if (
        args.match_structure_to_template
    ):  # Like other subroutines, this only works for one unique molecule at this stage
        if ".mult" in args.match_structure_to_template.lower():
            template_molecule = Molecule()
            template_molecule.set_geometry_from_mult(args.match_structure_to_template)
        elif ".res" in args.match_structure_to_template.lower():
            template_xstal = CrystalStructure(args.match_structure_to_template)
            template_molecule = copy.deepcopy(template_xstal.unique_molecule_list[0])
        else:
            raise CSPyException(
                "Must pass .res or .mult to match_structure_to_template"
            )
        crystal.change_space_group(crystal.determine_space_group())
        xstal2 = copy.deepcopy(crystal)
        align_method = "atom_vectors"
        if not xstal2.unique_molecule_list[0].match_atom_labelling_with_template(
            template_molecule, align_method
        ):
            for iatom, atom in enumerate(xstal2.unique_molecule_list[0].atoms):
                xstal2.unique_molecule_list[0].atoms[iatom].xyz = list3multiply(
                    atom.xyz, -1.0
                )
            if xstal2.unique_molecule_list[0].match_atom_labelling_with_template(
                template_molecule, align_method
            ):
                for iatom, atom in enumerate(crystal.unique_molecule_list[0].atoms):
                    crystal.unique_molecule_list[0].atoms[iatom].xyz = list3multiply(
                        atom.xyz, -1.0
                    )
                crystal_logger.info("Inverse is the match")
            else:
                crystal_logger.info("Could match neither original nor inverse")
    #        print "Hacking inverse"
    #        for iatom,atom in enumerate(crystal.molecule.atoms):
    #            crystal.molecule.atoms[iatom].xyz = list3multiply(atom.xyz,-1.0)

    if args.connectivity_info:
        crystal_logger.info(
            "Longest minimum connectivity (NBUR): ", crystal.longest_min_connectivity()
        )
        crystal_logger.info(
            "Longest atom-atom intermolecular contact (CUTM): ",
            crystal.max_atom_atom_distance(),
        )

    if args.res_print:
        crystal_logger.info("Initial Structure")
        crystal_logger.info(crystal.res_string_form())

    if args.cif_print:
        crystal_logger.info("Initial Structure")
        crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.check_asymm:
        crystal.check_asymmetric_unit()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.zprime_expand:
        crystal.remove_zprime_less_than_one()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.full_cell:
        crystal.expand_to_full_unit_cell()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.remove_coincident_atoms:
        crystal.remove_coincident_atoms()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.sort_atoms:
        crystal.sort_atoms_in_molecules()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.arrange_hydrogens:
        crystal.arrange_hydrogens()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.standard_atom_order:
        crystal.standard_atom_order()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.optimise_exp_structure:
        crystal.optimise_exp_structure(vars(gaussian_parse_args()))
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.molecular_volume:
        crystal.single_point(vars(gaussian_parse_args()))
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.clean_symm:
        crystal.clean_symmetry_ops()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.factorize_latt:
        crystal.factorize_out_latt()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.make_primitive:
        crystal.expand_to_primitive_unit_cell()
        crystal.shift_molecules_to_unit_cell()
        crystal.remove_coincident_atoms()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.calc_void:
        crystal.calc_void(args.probe_radius)
        crystal_logger.info("Void volumes:", crystal.void_volumes())
        crystal_logger.info("At these positions:", crystal.void_centroids())

    if args.embe_gen:
        crystal.embe_energy()

    if args.platon:
        crystal.platon_cell()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.niggli:
        crystal.standardize_cell()
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.simple_molecule_overlay:
        other = CrystalStructure(args.other[0])
        crystal.overlay_other_molecule2(other, rmsd_tolerance=args.rmsd_tolerance)
        if args.res_print:
            crystal_logger.info(crystal.res_string_form())
        if args.cif_print:
            crystal_logger.info(crystal.cif_string_form(args.cif_name))

    if args.dimer_extract_all or args.dimer_extract_uni:
        # Generates a series of *.xyz structures corresponding to close
        # contact dimers in a crystal

        dimer_groups = cluster_dimers(
            crystal.get_dimers(buffer=args.dimer_buffer, cutoff=args.dimer_cutoff),
            kabsch_rmsd=args.kabsch_rmsd,
        )

        crystal_logger.info("Found {} unique dimers".format(len(dimer_groups)))

        crystal_file_stem = args.filename.split(os.sep)[-1].split(".")[0]
        if args.dimer_extract_all:
            zip_file_name = crystal_file_stem + "_all_dimers.zip"
        else:
            zip_file_name = crystal_file_stem + "_uni_dimers.zip"

        idx = 0
        for i, dimer_group in enumerate(dimer_groups):
            for dimer in dimer_group:
                # add extra labels so that we know which atom belong to
                # which molecule as well as the site that the molecule
                # is in, not strictly following xyz file format but
                # should be more useful for everyone
                if args.dimer_extract_all:
                    labels_0 = "(Fragment=0, Site={})".format(dimer[0].name)
                    labels_1 = "(Fragment=1, Site={})".format(dimer[1].name)
                else:
                    labels_0 = "(Fragment=0)"
                    labels_1 = "(Fragment=1)"

                for atom in dimer[0].atoms:
                    atom.label += labels_0
                for atom in dimer[1].atoms:
                    atom.label += labels_1

                with zipfile.ZipFile(zip_file_name, "a") as xyz_zip:
                    output_filename = crystal_file_stem + "_" + str(idx) + ".xyz"
                    cmt = "Group #{} Dimer #{} extracted from {}".format(
                        i, idx, args.filename
                    )
                    dimer[0].fuse_with(dimer[1]).xyz_print_to_file(
                        filename=output_filename, comment=cmt
                    )

                    xyz_zip.write(output_filename)
                    os.remove(output_filename)

                if args.dimer_extract_all:
                    idx += 1
                else:
                    idx += len(dimer_group)
                    break

    if args.typing:
        from cspy.deprecated.molecule_typing import Typing_rules

        rulename = args.typing
        myrules = Typing_rules()
        myrules.init_from_file(rulename)
        crystal.expand_to_full_unit_cell()
        for molecule in crystal.unique_molecule_list:
            molecule.set_retyped_labels(myrules)
        crystal.update_sfac(retyped=True)
        filename = args.filename.split(os.sep)[-1].split(".")[0] + "_typed.res"
        crystal.write_res_to_file(filename, retyped=True)

    if args.make_castep_cell:
        make_castep_cell(crystal)

    if args.orient_miller:
        crystal.orient_to_miller_plane(list(map(int, args.orient_miller.split(","))))

    if args.report:
        crystal_logger.info(
            "Number of molecules in asymmetic unit = %d",
            len(crystal.unique_molecule_list),
        )
        crystal_logger.info(
            "Number of distinct molecules = %d",
            crystal.num_distinct_molecules(
                rms_tol=args.rmsd_tolerance, maxd_tol=args.maxd_tolerance
            ),
        )

    if args.check_same_molecules:
        uniq_res = list(set([args.filename] + args.other))
        for o in [args.filename] + uniq_res:
            ocrystal = CrystalStructure(o)
            if 1 == len(ocrystal.unique_molecule_list):
                base_crystal = CrystalStructure(o)
                base_name = o
                crystal_logger.info(
                    "Using " + str(o) + " as our base crystal as this is zprime 1"
                )
                break
        base_molecule = base_crystal.unique_molecule_list[0]
        for o in uniq_res:
            ocrystal = CrystalStructure(o)
            crystal_logger.info(
                "Number of molecules in asymmetic unit = %d",
                len(ocrystal.unique_molecule_list),
            )
            crystal_logger.info(
                "Number of distinct molecules = %d",
                ocrystal.num_distinct_molecules(
                    rms_tol=args.rmsd_tolerance, maxd_tol=args.maxd_tolerance
                ),
            )
            for i, other_mol in enumerate(ocrystal.unique_molecule_list):
                same = base_molecule.is_same(
                    other_mol, rms_tol=args.rmsd_tolerance, maxd_tol=args.maxd_tolerance
                )
                if not same:
                    crystal_logger.warn(
                        "DIFFERENT %s[%d] is inconsistent with %s", o, i, base_name
                    )
                else:
                    crystal_logger.info(
                        "SAME: %s[%d] is consistent with %s", o, i, base_name
                    )

    if args.find_redundant_conformers:
        from collections import defaultdict
        import time

        unique_res_files = set([args.filename] + args.other)
        crystals = {x: CrystalStructure(x) for x in unique_res_files}
        crystal_logger.warn("This code has only been designed for Z'==1")
        duplicates = defaultdict(list)
        while crystals:
            name, ref = crystals.popitem()
            crystal_logger.info("Reference crystal: %s", name)
            ref_mol = ref.unique_molecule_list[0]
            other_crystals = crystals.copy()
            while other_crystals:
                other_name, other = other_crystals.popitem()
                t = time.time()
                for i, other_mol in enumerate(other.unique_molecule_list):
                    same = ref_mol.is_same(
                        other_mol,
                        rms_tol=args.rmsd_tolerance,
                        maxd_tol=args.maxd_tolerance,
                    )
                    if same:
                        duplicates[name] += other_name
                        crystal_logger.info(
                            "(%s, %s) match within RMSD", name, other_name
                        )
                        del crystals[other_name]
                        del other_crystals[other_name]
                crystal_logger.debug(
                    "Time taken for mol comparison: %s s", time.time() - t
                )
        crystal_logger.info("Duplicates: %s", duplicates)

    if args.generate_cocrystal:
        """
        Shortcut command for the deterministic insertion of guests
        """
        args.insert_guest = True
        args.use_void_centroid = True
        args.use_void_shape = True

    if args.insert_guest:
        """
        Generates a series of *.res files corresponding to the insertion of
        a guest (*.xyz format) in a crystal
        
        Use:
        >>> crystal.py my_crystal.res --insert_guest --guest my_guest.xyz
        
        Options:
        * --guest
        Description: path to the guest(s) to insert
        Format: string or n*string (default = None)
        
        * --use_void_centroid
        Description: guests are positioned at the centroids of the voids
        Format: no argument (default = False)
        
        * --use_void_shape
        Description: guests are oriented according to the shape of the voids
        Format: no argument (default = False)
        
        * --probe_radius
        Description: radius of the probe used during the void calculation
        Format: float (default = 1.2)
        
        * --void_grid
        Description: grid used during the void calculation
        Format: float (default = 0.2)
        
        * --num_insertions
        Description: desired number of structures to generate
        Format: integer (default = 1)
        
        * --max_insertions
        Description: maximum number of tries in the generation procedure
        Format: integer (default = 100)
        
        * --inflation_factor
        Description: inflation factor for the initial unit cell
        Format: float (default = 1.)
        
        * --inflation_tensor
        Description: inflation factors along each unit cell vector
        Format: 3*float (default = 1. 1. 1.)
        Nota: has the priority over --inflation_factor
        
        * --multiple_insertions
        Description: multiple insertions of the same guest
        Format: integer (default = 1))
        """
        # Deals with the guests (unique guest or list of guests)
        if not args.guest:
            err_msg = "No guest molecule has been specified! Use the --guest keyword"
            crystal_logger.error(err_msg)
            raise CSPyException(err_msg)
        if len(args.guest) == 1:
            guest_paths = [args.guest[0]] * args.multiple_insertions
        else:
            guest_paths = args.guest
        guests = []
        for guest in guest_paths:
            if guest[-4:] == ".xyz":
                new_guest = Molecule()
                new_guest.init_from_xyz(guest)
                new_guest.name = guest.split(os.sep)[-1]
            else:
                err_msg = "Provide a .xyz file for the guest molecule(s)"
                crystal_logger.error(err_msg)
                raise CSPyException(err_msg)
            new_guest_centroid_coord = new_guest.centroid()
            for atom in new_guest.atoms:
                for coord in [0, 1, 2]:
                    atom.xyz[coord] -= new_guest_centroid_coord[coord]
            guests.append(new_guest)
        # Calculates the voids, if needed
        if args.use_void_centroid:
            crystal.calc_void(args.probe_radius, args.void_grid)
        # Deals with the (an)isotropic inflation of the cell
        if args.inflation_tensor != [1.0, 1.0, 1.0]:
            inflation_tensor = args.inflation_tensor
        else:
            inflation_tensor = [args.inflation_factor] * 3
        crystal.aniso_inflate_cell(inflation_tensor)
        crystal.check_asymmetric_unit()
        # Calls the main routine
        insertion_header = (
            "Attempting to insert guest molecules\n" "Options:\n" "- Guests:\n"
        )
        for guest_path in guest_paths:
            insertion_header += guest_path.split(os.sep)[-1] + "\n"
        insertion_header += (
            "- Number of structures to generate: {}\n"
            "- Use voids?\n"
            "      centroids: {}\n"
            "         shapes: {}\n"
            "   probe radius: {}\n"
            "           grid: {}\n"
            "Inflation tensor of the cell: {}\n"
        )
        print(
            insertion_header.format(
                args.num_insertions,
                args.use_void_centroid,
                args.use_void_shape,
                args.probe_radius,
                args.void_grid,
                inflation_tensor,
            )
        )
        insertion_compounds = crystal.insert_guest(
            guests,
            args.num_insertions,
            args.max_insertions,
            args.use_void_centroid,
            args.use_void_shape,
        )
        print("Generated {} structures".format(len(insertion_compounds)))
        # Writes out the generated structures
        count = 0
        if args.res_print or args.cif_print:
            for compound in insertion_compounds:
                if args.res_print:
                    new_filename = (
                        args.filename.split(os.sep)[-1].split(".")[0]
                        + "_"
                        + str(count)
                        + ".res"
                    )
                    compound.write_res_to_file(new_filename)
                else:
                    new_filename = (
                        args.filename.split(os.sep)[-1].split(".")[0]
                        + "_"
                        + str(count)
                        + ".cif"
                    )
                    compound.write_cif_to_file(new_filename)
                count += 1

    if args.rhom_to_hex:
        # This doesn't automatically work out the space group index, as the reverse transformation may not be available
        crystal.rhombohedral_to_hexagonal_setting(sg=args.rhom_to_hex)

    if args.hex_to_rhom:
        # This doesn't automatically work out the space group index, as the reverse transformation may not be available
        crystal.rhombohedral_to_hexagonal_setting(
            sg=args.hex_to_rhom, reverse_direction=True
        )
    ## DON'T ADD ANY NEW FUNCTIONALITY BELOW
    if args.xyz_output_filename != "":
        crystal.unique_molecule_list[0].xyz_print_to_file(args.xyz_output_filename)

    if args.res_output_filename != "":
        crystal.write_res_to_file(args.res_output_filename)

    if args.cif_output_filename != "":
        crystal.write_cif_to_file(args.cif_output_filename, args.cif_name)


if __name__ == "__main__":
    main()
