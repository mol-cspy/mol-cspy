from cspy.deprecated.cspy_exceptions import CSPyException


class Job(object):
    def __init__(self):
        pass

    @property
    def nprocs(self):
        return str(self._nprocs)

    @nprocs.setter
    def nprocs(self, value):
        if isinstance(value, int):
            self._nprocs = value
        else:
            raise CSPyException("Number of cores must be an integer!")

    @property
    def input_file_name(self):
        # for job calling external code that needs an input file, store the
        # file name
        if hasattr(self, "_input_file_name"):
            return self._input_file_name
        else:
            raise CSPyException("No input specified")

    @input_file_name.setter
    def input_file_name(self, value):
        self._input_file_name = value

    @property
    def execution_directory(self):
        if hasattr(self, "_execution_directory"):
            return self._execution_directory
        else:
            # by default, gaussian calculation will be run inside the current
            # working directory
            import os

            return os.getcwd()

    @execution_directory.setter
    def execution_directory(self, value):
        self._execution_directory = value

    @property
    def molecule(self):
        return self._molecule

    @molecule.setter
    def molecule(self, value):
        from cspy.deprecated.core.models.molecule import Molecule

        assert isinstance(value, Molecule)
        self._molecule = value


class GaussianJob(Job):
    def __init__(self):
        pass

    @property
    def input_file_name(self):
        # for job calling external code that needs an
        # input file, store the file name
        if hasattr(self, "_input_file_name"):
            return self._input_file_name
        else:
            return "cspy_mol.com"

    @input_file_name.setter
    def input_file_name(self, value):
        self._input_file_name = value

    @property
    def functional(self):
        return self._functional

    @functional.setter
    def functional(self, value):
        self._functional = value

    @property
    def basis_set(self):
        return self._basis_set

    @basis_set.setter
    def basis_set(self, value):
        self._basis_set = value

    @property
    def memory(self):
        return str(self._memory)

    @memory.setter
    def memory(self, value):
        self._memory = value

    @property
    def chelpg(self):
        if hasattr(self, "_chelpg"):
            return self._chelpg
        else:
            return False  # default option turn it off

    @chelpg.setter
    def chelpg(self, value):
        assert isinstance(value, bool)
        self._chelpg = value

    @property
    def molecular_volume(self):
        # option specify if molecular volume will be calculated
        if hasattr(self, "_molecular_volume"):
            return self._molecular_volume
        else:
            return False  # default option turn it off

    @molecular_volume.setter
    def molecular_volume(self, value):
        assert isinstance(value, bool)
        self._molecular_volume = value

    @property
    def polarizable_continuum(self):
        # option to use polarizable continuum
        if hasattr(self, "_polarizable_continuum"):
            return self._polarizable_continuum
        else:
            return False  # default option turn it off

    @polarizable_continuum.setter
    def polarizable_continuum(self, value):
        assert isinstance(value, bool)
        self._polarizable_continuum = value

    @property
    def external_iteration_pcm(self):
        # option to use external polarizable continuum
        if hasattr(self, "_external_iteration_pcm"):
            return self._external_iteration_pcm
        else:
            return False  # default option turn it off

    @external_iteration_pcm.setter
    def external_iteration_pcm(self, value):
        assert isinstance(value, bool)
        self._external_iteration_pcm = value

    @property
    def dielectric_constant(self):
        # return the value of dielectric constant for pcm calculations
        # default value is set to 3.0
        if hasattr(self, "_dielectric_constant"):
            return self._dielectric_constant
        else:
            return 3.0

    @dielectric_constant.setter
    def dielectric_constant(self, value):
        self._dielectric_constant = value

    @property
    def perform_geometry_optimisation(self):
        if hasattr(self, "_perform_geometry_optimisation"):
            assert isinstance(self._perform_geometry_optimisation, bool)
            return self._perform_geometry_optimisation
        else:
            return False  # default not to perform geometry optimisation

    @perform_geometry_optimisation.setter
    def perform_geometry_optimisation(self, value):
        assert isinstance(value, bool)
        self._perform_geometry_optimisation = value

    @property
    def geometry_optimisation(self):
        if hasattr(self, "_geometry_optimisation"):
            return self._geometry_optimisation
        else:
            return "ModRedundant,"  # default option for geometry optimization,
            # use ModRedundant

    @geometry_optimisation.setter
    def geometry_optimisation(self, value):
        self._geometry_optimisation = value

    @property
    def frequency_calculation(self):
        if hasattr(self, "_frequency_calculation"):
            return self._frequency_calculation
        else:
            return False

    @frequency_calculation.setter
    def frequency_calculation(self, value):
        self._frequency_calculation = value

    @property
    def normal_mode_calculation(self):
        if hasattr(self, "_normal_mode_calculation"):
            return self._normal_mode_calculation
        else:
            return False

    @normal_mode_calculation.setter
    def normal_mode_calculation(self, value):
        assert isinstance(value, bool)
        self._normal_mode_calculation = value

    @property
    def additional_args(self):
        if hasattr(self, "_additional_args"):
            return self._additional_args
        else:
            return ""

    @additional_args.setter
    def additional_args(self, value):
        self._additional_args = value

    @property
    def isotope_mass(self):
        # For normal mode calculation, the isotope mass needs to be specified
        # for our code.
        if hasattr(self, "_isotope_mass"):
            return self._isotope_mass
        else:
            return ""  # should there be a proper default value?

    @isotope_mass.setter
    def isotope_mass(self, value):
        self._isotope_mass = value

    @property
    def save_check_point(self):
        # options for determining whether a checkpoint file will be created
        if hasattr(self, "_save_check_point"):
            return self._save_check_point
        else:
            return True

    @save_check_point.setter
    def save_check_point(self, value):
        assert isinstance(value, bool)
        self._save_check_point = value

    @property
    def checkpoint_filename(self):
        # check point files will be named after the input filename
        # thus no explicit setter is provided in the code!
        return self.input_file_name.split(".")[0]

    @property
    def comment(self):
        if hasattr(self, "_comment"):
            return self._comment
        else:
            return "Gaussian calculation for CSPy"

    @comment.setter
    def comment(self, value):
        self._comment = str(value)

    @property
    def charge(self):
        if hasattr(self, "_charge"):
            return self._charge
        else:
            return 0  # default we only deal with neutral system

    @charge.setter
    def charge(self, value):
        self.charge = int(value)

    @property
    def multiplicity(self):
        if hasattr(self, "_multiplicity"):
            return self._multiplicity
        else:
            return 1  # default value for neutral system

    @multiplicity.setter
    def multiplicity(self, value):
        self.multiplicity = value
