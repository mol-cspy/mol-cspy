from cspy.deprecated.core.io.gaussian.io import FileReader
from cspy.deprecated.core.io.gaussian.jobs import GaussianJob
from cspy.deprecated.core.models.molecule import Molecule
from cspy.deprecated.core.models.atom import Atom
import re
import math
import logging

gaussian_reader_log = logging.getLogger("CSPy.dao.gaussianOutput")


class GaussianLogFileReader(FileReader):
    def __init__(self, job=None, input_location=None, file_content=None):
        super(self.__class__, self).__init__(
            job=job, input_location=input_location, file_content=file_content
        )

    def _initialize_from_job_model(self):
        assert isinstance(self.job, GaussianJob)
        self.input_location = (
            self.job.execution_directory + "/" + self.job.input_file_name
        )
        self.input_location = self.input_location.replace(".com", ".log")
        self._initialize_from_input_location()

    def read_in(self):
        # This is the method that will be called internally from our code,
        # which assumes a job model is passed in, based on options in job,
        # relavant information will be parsed out accordingly.
        self._get_total_energy()
        self._get_optimized_structure()
        self.store_partial_charges()

    def _get_line_matching(self, pattern):
        keep_reading = True
        content = iter(self.file_content)
        while keep_reading:
            line = next(content)
            matched_line = re.match(pattern, line, re.M | re.I)
            if matched_line:
                keep_reading = False
        return line

    def _get_all_lines_matching(self, pattern):
        all_lines = []
        line_number = 0
        for line in self.file_content:
            if re.match(pattern, line, re.M | re.I):
                all_lines.append([line, line_number])
            line_number += 1
        return all_lines

    def get_termination_status(self):
        all_matched_lines = self._get_all_lines_matching(
            " Normal termination of Gaussian"
        )
        return len(all_matched_lines) > 0

    def _get_total_energy(self):
        if self.job.perform_geometry_optimisation is True:
            # read in both the initial and final energy for the molecule Note
            # that if external iteraction PCM option was switched on, only the
            # final energy will be read in
            if self.job.external_iteration_pcm is not True:
                self.job.molecule.initial_energy = self.get_initial_energy()
                self.job.molecule.energy = self.get_final_energy()
                gaussian_reader_log.info(
                    "Geometry optimisation, energy changed from %s to %s a.u",
                    str(self.job.molecule.initial_energy),
                    str(self.job.molecule.energy),
                )
            else:
                self.job.molecule.energy = self.get_final_energy(external_pcm=True)
                gaussian_reader_log.info(
                    "Single point energy from log file: %s",
                    str(self.job.molecule.energy) + " a.u.",
                )
        else:
            self.job.molecule.energy = self.get_final_energy(
                external_pcm=self.job.external_iteration_pcm
            )

        if self.job.molecule.energy is None:
            raise Exception("No SCF energy in gaussian log file")

    def get_final_energy(self, external_pcm=False):
        # this is the method to be called from outside, say when only a log
        # file was given and one would want to extract the final energy from
        # the output file. Regardless if a geometry optimization had been
        # requested, this is the last energy value that got splitted out from
        # the log file the user who uses this function needs to be sure what
        # they are requesting, for the moment.
        all_matched_lines = self._get_all_lines_matching(
            self._target_string(external_pcm=external_pcm)
        )
        return float(all_matched_lines[-1][0].split()[4])

    def get_initial_energy(self, external_pcm=False):
        all_matched_lines = self._get_all_lines_matching(
            self._target_string(external_pcm=external_pcm)
        )
        return float(all_matched_lines[0][0].split()[4])

    def _target_string(self, external_pcm=False):
        if external_pcm:
            return " <psi(f)|   H    |psi(f)>"
        else:
            return " SCF Done"

    def _get_optimized_structure(self):
        # this is just a wrapper to put a list of xyz tuples into the proper
        # domain model A bit of trade-off of performing the loop one extra time
        # over all atoms in the molecule, but hopefully gaining a bit of code
        # reusability
        if self.job.perform_geometry_optimisation is True:
            optimized_coordinates = self.get_optimized_structure()
            assert len(optimized_coordinates) == len(self.job.molecule.atoms)
            for i in range(len(optimized_coordinates)):
                self.job.molecule.atoms[i].xyz = optimized_coordinates[i]

    def get_optimized_structure(self):
        # method to be called from outside to read in the optimized coordinates
        matched_lines = self._get_all_lines_matching(
            "                          Input orientation:"
        )
        starting_line_for_the_last_block = matched_lines[-1][1]
        structure_start = starting_line_for_the_last_block + 4
        line_counter = 0
        # to store a list of tuples for the coordinates, probably need to store
        # atomic symbols as well
        optimized_coordinates = []

        gaussian_reader_log.debug(
            "read in optimized coordinates from line %s", str(structure_start)
        )

        for line in self.file_content:
            if line_counter > structure_start:
                if "----" not in line:
                    gaussian_reader_log.debug(line)
                    this_xyz = (
                        float(line.split()[3]),
                        float(line.split()[4]),
                        float(line.split()[5]),
                    )
                    optimized_coordinates.append(this_xyz)
                else:
                    break
            line_counter += 1
        return optimized_coordinates

    def get_number_of_atoms(self):
        return len(self.get_optimized_structure())

    def get_atoms(self):
        # get all the atoms out from the log file
        raise NotImplementedError

    def _get_partial_charges(self):
        return
        # get the partial charge from log file and returns a LIST of partial
        # charges.
        partial_charges = []
        i = 0
        counter = 0
        find_str = " Fitting point charges to electrostatic potential"
        for line in self.file_content:
            if line.startswith(find_str):
                this_partial_charges = []
                i += 1
                counter += 1
            if i >= 5:
                if "-----" in line or "Sum of ESP charges" in line:
                    partial_charges.append(this_partial_charges)
                    i = 0
                    continue
                this_partial_charges.append(float(line.split()[2]))
            if i > 0:
                i += 1

        if len(partial_charges) == 0:
            raise Exception("Could not extract partial charges from log file!")

        return partial_charges[-1]

    def store_partial_charges(self):
        self.job.molecule.chelpg = self._get_partial_charges()


class GaussianFchkFileReader(FileReader):
    def __init__(self, job=None, input_location=None, file_content=None):
        try:
            assert job is not None
            self.job = job
            self._initialize_from_job_model()
        except AssertionError:
            try:
                assert input_location is not None
                self.input_location = input_location
                self._initialize_from_input_location()
            except AssertionError:
                try:
                    assert file_content is not None
                    self._initialize_from_file_content(file_content)
                except AssertionError:
                    raise Exception(
                        "Nothing has been passed in to properly"
                        "initialized the reader!"
                    )

    def _initialize_from_job_model(self):
        assert isinstance(self.job, GaussianJob)
        self.input_location = (
            self.job.execution_directory + "/" + self.job.input_file_name
        )
        self.input_location = self.input_location.replace(".com", ".fchk")
        self._initialize_from_input_location()

    def get_molecule(self):
        """
        Read in the molecular coordinates from the formatted checkpoint file.
        """
        pass

    def get_atom_identities(self):
        content = iter(self.file_content)
        for line in content:
            if line.startswith("Atomic numbers"):
                # read in total number of atoms in formatted check
                # point file with the line
                # Atomic numbers                             I   N=          26
                sl = line.strip().split()
                natoms = int(sl[-1])
                # count how many lines in the file contains the atomic numbers
                nlines = int(math.ceil(float(natoms) / 6.0))

                vals = []
                for _ in range(nlines):
                    line = next(content)
                    vals += map(float, line.strip().split())

                    # put in a list of atom models with zero coordinates
                    atoms = []
                    for n in range(natoms):
                        atoms.append(Atom(vals[n], [0.0, 0.0, 0.0]))
        return atoms

    def get_cartesian_coordinates(self):
        atoms = self.get_atom_identities()
        content = iter(self.file_content)
        for line in content:
            if line.startswith("Current cartesian coordinates"):
                sl = line.strip().split()
                nl = int(sl[-1])
                nlines = int(math.ceil(float(nl) / 5.0))
                vals = []
                for _ in range(nlines):
                    line = next(content)
                    vals += map(float, line.strip().split())

                for n in range(len(atoms)):
                    atoms[n].xyz[0] = vals[n * 3 + 0] * 0.529177249
                    atoms[n].xyz[1] = vals[n * 3 + 1] * 0.529177249
                    atoms[n].xyz[2] = vals[n * 3 + 2] * 0.529177249
        self._populate_molecule_with_atoms(atoms)
        return atoms

    def _populate_molecule_with_atoms(self, atoms):
        if hasattr(self, "job"):
            self.job.molecule = Molecule()
            for atom in atoms:
                self.job.molecule.add_atom(atom)

    def get_cartesian_normal_modes(self):
        # just parse all the cartesian normal modes in here as a list of
        # floats, no data processing, in terms of packaging up into a proper
        # matrix is carried out here.
        normal_modes = self.grab_block_data("Vib-Modes")
        self._populate_molecule_with_normal_modes(normal_modes)
        return normal_modes

    def _populate_molecule_with_normal_modes(self, normal_modes):
        if not hasattr(self, "job"):
            return
        natoms = self.job.molecule.num_atoms()
        self.job.molecule.cart_force_matrix = [
            [[] for _ in range(natoms * 3)] for _ in range(natoms * 3)
        ]
        for m in range(natoms * 3):
            for a in range(m, natoms * 3):
                ind = m * natoms + a
                self.job.molecule.cart_force_matrix[m][a] = normal_modes[ind]
                self.job.molecule.cart_force_matrix[a][m] = normal_modes[ind]

    def get_cartesian_force_constants(self):
        force_constants = self.grab_block_data("Cartesian Force Constants")
        self._populate_molecule_with_force_constants(force_constants)
        return force_constants

    def _populate_molecule_with_force_constants(self, force_constants):
        if not hasattr(self, "job"):
            return
        natoms = self.job.molecule.num_atoms()
        self.job.molecule.cart_dof_matrix = [
            [[[] for _ in range(3)] for _ in range(natoms)]
            for _ in range(self.job.molecule.num_int_dof())
        ]
        i = 0
        for m in range(self.job.molecule.num_int_dof()):
            for a in range(natoms):
                for x in range(3):
                    # TODO: Fix this undefined reference to raw
                    self.job.molecule.cart_dof_matrix[m][a][x] = raw[i]
                    i += 1

    def grab_block_data(self, block_name):
        save_force = False
        raw_data = []
        for line in self.file_content:
            if save_force:
                try:
                    for c in line.strip().split():
                        raw_data.append(float(c))
                except Exception:
                    # in case hit the next block of content in check point
                    # file, dont read anymore
                    break
            if block_name in line:
                save_force = True
        return raw_data
