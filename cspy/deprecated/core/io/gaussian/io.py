import os.path


class FileWriter(object):
    def __init__(self):
        pass


class FileReader(object):
    def __init__(self, job=None, input_location=None, file_content=None):
        try:
            assert job is not None
            self.job = job
            self._initialize_from_job_model()
        except AssertionError:
            try:
                assert input_location is not None
                self.input_location = input_location
                self._initialize_from_input_location()
            except AssertionError:
                assert file_content is not None
                self._initialize_from_file_content(file_content)

    def _initialize_from_input_location(self):
        if os.path.isfile(self.input_location) is not True:
            raise Exception("File " + self.input_location + " not found!")

        self._initialize_from_file_content(open(self.input_location, "r").read())

    def _initialize_from_file_content(self, file_content):
        if isinstance(file_content, str):
            self.file_content = file_content.split("\n")
        else:
            assert isinstance(file_content, list)
            self.file_content = file_content
