from cspy.deprecated.core.io.gaussian.io import FileWriter
from cspy.deprecated.core.io.gaussian.jobs import GaussianJob
from cspy.deprecated.core.models.molecule import Molecule


class GaussianInputFileWriter(FileWriter):
    def __init__(self, job):
        assert isinstance(job, GaussianJob)

        self.job = job
        self.header = ""
        self.file_content = ""

        # semiempirical methods available to Gaussian
        self._semiempirical_methods = ["am1"]

    def write_out(self):
        self.build_file_content()

        input_file_name = self.job.execution_directory + "/" + self.job.input_file_name
        input_file = open(input_file_name, "w")
        input_file.write(self.file_content)
        input_file.close()

    def build_file_content(self):
        self.file_content += self.build_header()
        self.write_atoms()

        if self.job.normal_mode_calculation is True:
            self.file_content += self._build_header_for_normal_mode_calculation()

        self.file_content += "\n"

    def build_header(self):
        self._build_memory_string()
        self._build_number_of_processors_string()
        self._build_check_point_file_string()
        self._build_method_string()
        self._build_symmetry_string()
        self._build_check_point_string()
        self._build_geometry_optimisation_string()
        self._build_molecular_volume_string()
        self._build_frequency_calculation_string()
        self._build_pop_string()
        self._build_additional_args_string()

        self._build_comment_string()

        self._build_charge_and_multiplicity_string()
        return self.header

    def _build_header_for_normal_mode_calculation(self):
        self.header = "--Link1--\n"
        self._build_memory_string()
        self._build_number_of_processors_string()
        self._build_check_point_file_string()
        self._build_method_string()
        self._build_symmetry_string()
        self._build_check_point_string()
        if self.job.frequency_calculation is not False:
            self.header += "Freq=(" + self.job.frequency_calculation + ") "
        self.header += "Geom=Check "
        self._build_additional_args_string()

        self.job.comment = "frequency calculation"
        self._build_comment_string()

        self._build_charge_and_multiplicity_string()
        return self.header

    def _build_memory_string(self):
        self.header += "%mem=" + self.job.memory + "\n"

    def _build_number_of_processors_string(self):
        self.header += "%nprocshared=" + self.job.nprocs + "\n"

    def _build_check_point_file_string(self):
        if self.job.save_check_point:
            self.header += "%chk=" + self.job.checkpoint_filename + ".chk\n"

    def _build_method_string(self):
        if self.job.functional.lower() in self._semiempirical_methods:
            self.header += "#" + self.job.functional + " "
        else:
            functional_name = self.job.functional.strip().replace("GD3BJ", "").strip()
            self.header += "#" + functional_name + "/" + self.job.basis_set + " "
            if "GD3BJ" in self.job.functional:
                self.header += "EmpiricalDispersion=GD3BJ "

    def _build_symmetry_string(self):
        # symmetry is turned off by default
        self.header += "NoSymmetry "

    def _build_check_point_string(self):
        if self.job.save_check_point:
            self.header += "FChk "

    def _build_geometry_optimisation_string(self):
        if self.job.perform_geometry_optimisation:
            self.header += "Opt=(" + self.job.geometry_optimisation + ") "

    def _build_molecular_volume_string(self):
        # our default option for volume
        if self.job.molecular_volume:
            self.header += "Volume=Tight "

    def _build_frequency_calculation_string(self):
        # as on 27/02/2015, the default value for frequency_calculation is
        # False, and was turned on if a string is set for this variable, so we
        # need to check this is not False rather than checking this is True.
        # TODO: Migrate this to default being None or something more sane.
        if (self.job.frequency_calculation is not False) and (
            self.job.normal_mode_calculation is not True
        ):
            self.header += "Freq=(" + self.job.frequency_calculation + ") "

    def _build_pop_string(self):
        if self.job.chelpg:
            self.header += "POP=(CHELPG)"

    def _build_additional_args_string(self):
        self.header += self.job.additional_args

    def _build_comment_string(self):
        self.header += "\n\n" + self.job.comment + "\n\n"

    def _build_charge_and_multiplicity_string(self):
        self.header += str(self.job.charge) + " " + str(self.job.multiplicity) + "\n"

    def write_atoms(self):
        # make sure there is an instance of molecule to work on
        assert isinstance(self.job.molecule, Molecule)
        # if self.job.molecule.coord_type == "ZMATRIX":
        #   raise notImplementedError
        # else:
        self._write_cartesian_coordinates()

    def _write_cartesian_coordinates(self):
        for atom in self.job.molecule.atoms:
            self.file_content += atom.element.symbol + " " + str(atom.xyz) + "\n"
        self.file_content += "\n\n"
        return self.file_content
