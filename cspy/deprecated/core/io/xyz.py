"""
Module for reading and writing XYZ files (files containing molecular
geometries as XYZ coordinates, which may or may not be preceded by
the number of atoms and a comment line).

This uses the generic FileReader template as a parent class.

For simple **reading** of an XYZ file on disk to a Molecule object, use:

.. code-block:: python

    my_molecule = load_molecule('./file.xyz')

For simple **writing** of an XYZ file for a specified Molecule, use:

.. code-block:: python

    write_molecule(my_molecule,'./newfile.xyz')

Details
-------

An XYZ file can be **read** from either a file or a string, by
instantiating the XYZFileReader class appropriately.
It is the action of the read() method that actually creates the molecule:

.. code-block:: python

    file_molecule = XYZFileReader(input_location='directory/geom.xyz').read()

    string_molecule = XYZFileReader(file_content=my_xyz_string).read()


An XYZ file can be **written** to a file, by instantiating the XYZFileWriter
class. The writer is instantiated with the output location, and the write()
method is then called with the molecule as argument.

.. code-block:: python

    writer = XYZFileWriter(output_location='directory/geom.xyz', \
            file_content='')
    writer.write(my_molecule)

Internally, the molecule's XYZ string representation is generated.
This can be accessed explictly through the writer's method:

.. code-block:: python

    writer.xyz_string_form(my_molecule)

"""
from cspy.deprecated.core.io.abstract_io import FileReader, FileWriter
from cspy.deprecated.core.models import Atom, Molecule, Vector3D


def load_molecule(file_location):
    """
    Shortcut function to get a molecule from an XYZ file.

    :param file_location: the location of the .xyz file
    :return: A Molecule object
    :rtype: :class:`.Molecule`
    """
    molecule = XYZFileReader(file_location).read()
    return molecule


def write_molecule(molecule, file_location):
    """
    Shortcut function to write a molcule to an XYZ file.

    :param molecule: the molecule to write out
    :type molecule: :class:`.Molecule`
    :param file_location: the intended path to the .xyz file
    """
    writer = XYZFileWriter(output_location=file_location, file_content="")
    writer.write(molecule)
    return


def xyz_molecule(molecule):
    """
    Shortcut function to write a molcule to an XYZ file.

    :param molecule: the molecule to write out
    :type molecule: :class:`.Molecule`
    :param file_location: the intended path to the .xyz file
    """
    writer = XYZFileWriter(output_location="", file_content="")
    return writer.xyz_string_form(molecule)


class XYZFileReader(FileReader):
    """
    Class that provides methods for parsing an XYZ (.xyz)file.
    """

    def __init__(self, input_location=None, file_content=None):
        super(self.__class__, self).__init__(
            input_location=input_location, file_content=file_content
        )

    def read(self):
        """
        Method for parsing the content of an XYZ file to create a
        Molecule object, after having initialised an instance of the
        XYZFileReader class.

        :return: The resulting Molecule object as defined by the XYZ file.
        :rtype: :class:`.Molecule`
        """
        read_comment = False
        number_of_atoms = False
        atoms = []
        for i, _line in enumerate(self.file_content):
            line = _line.strip()
            split_line = line.split()
            if read_comment:
                read_comment = False
                continue
            elif i == 0:
                try:
                    number_of_atoms = int(line)
                    read_comment = True
                    continue
                except ValueError:
                    pass
            elif line == "":
                break
            if len(split_line) != 4:
                raise AssertionError(
                    "Lines in XYZ file should have exactly"
                    "4 fields: label and 3 coords."
                )
            atom_label = split_line[0]
            atom_coords = Vector3D(*map(float, split_line[1:4]))
            atom = Atom(label=atom_label, xyz=atom_coords)
            atoms.append(atom)

        if number_of_atoms and number_of_atoms != len(atoms):
            raise AssertionError(
                "Number of atom entries in XYZ file does not"
                "match number on first line."
            )

        molecule = Molecule(atoms=atoms)
        return molecule


class XYZFileWriter(FileWriter):
    line_format = "{label:<10}    {x:<16}    {y:<16}    {z:<16}"

    def __init__(self, output_location, file_content):
        super(self.__class__, self).__init__(output_location, file_content)

    def write(self, molecule):
        """
        Method to write a Molecule to an XYZ file on disk at the
        location specified.

        :param molecule: The molecule to write out to file.
        :type: :class:`.Molecule`
        """

        with open(self.output_location, "w+") as f:
            f.write(self.xyz_string_form(molecule))

    def xyz_string_form(self, molecule):
        """
        Obtain the XYZ string form of a specified molecule.

        :param molecule: The molecule to write an XYZ string for.
        :type: :class:`.Molecule`
        :return: xyz_string -- The newline-separated string of the
            specified molecule in XYZ form.
        :rtype: string
        """
        xyz_string = []

        xyz_string += [str(len(molecule.atoms))]

        if molecule.name is not None:
            xyz_string += [molecule.name]
        else:
            xyz_string += ["XYZ string form"]

        for atom in molecule.atoms:
            atom_line = self.line_format.format(
                label=atom.label, x=atom.xyz.x, y=atom.xyz.y, z=atom.xyz.z
            )
            xyz_string += [atom_line.strip()]

        return "\n".join(xyz_string)
