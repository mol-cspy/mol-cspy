"""
Module containing objects to perform I/O operations for SHLEX files that store
crystallography informations.

Reading in a res file into a crystal object is done via a two stage approach.
In the first stage, the res file from a disk location is read in into a
ResFileContent object, which subsequently converted into an interal crystal
object using a mapper, like so:

.. code-block:: python

    res_content = ResFileReader('directory/test.res').read()
    crystal = ResCrystalMapper().map_to_object(res_content)

To write the crystal out to a res file at a specific file location, similar
logic follows, where a crystal object must be first converted to a
ResFileContent object, before calling the writer to write the file out:

.. code-block:: python

    file_content = ResCrystalMapper().map_to_content(crystal)
    writer = ResFileWriter(file_content=file_content, \
            output_location='./this.res')
    writer.write()

For normal user, the following wrapper functions were also provided, to achieve
the above functionalities by just writing one line of code:

Getting a crystal from res file on disk:

.. code-block:: python

    crystal = load_crystal('./this.res')

Writing a crystal to a res file on disk:

.. code-block:: python

    write_crystal(crystal,'./this.res')

"""
from collections import namedtuple
from cspy.deprecated.core.tasks.internal.cell.lattice_centering import factor_out_lattice_type
from cspy.deprecated.core.models import (
    Lattice,
    Molecule,
    Crystal,
    Atom,
    SymmetryOperation,
    SpaceGroup,
    FractionalCoordinates,
)
from cspy.deprecated.core.io.abstract_io import FileReader, FileWriter, ObjectMapper
from cspy.deprecated.core.models.space_group import StandardSpaceGroup

__res_template_fields = [
    "TITL",
    "CELL",
    "LATT",
    "SFAC",
    "SYMM",
    "ZERR",
    "UNIT",
    "FVAR",
    "atoms",
]
ResFileContent = namedtuple("ResFileContent", __res_template_fields)
"""
A named turple that defines the content of a res file, which will be used to
store the content retrieved from a SHELX file without explictly interpreting
its content
"""


def load_crystal(file_location):
    """
    Wrapper function to get a crystal from a res file.

    :param file_location: the location of the .res file
    :return crystal: A fully constructed crystal object.
    :rtype: :class:`.Crystal`
    """
    res_content = ResFileReader(file_location).read()
    crystal = ResCrystalMapper().map_to_object(res_content)
    return crystal


def res_string_crystal(crystal):
    """
    Wrapper function to write a crystal to a res file on disc.

    :param crystal: A :class:`.Crystal` object
    :param file_location: Disk location to which the res file will
    be written to.
    """
    file_content = ResCrystalMapper().map_to_content(crystal)
    writer = ResFileWriter(file_content=file_content, output_location="")
    return writer.res_string_form


def write_crystal(crystal, file_location):
    """
    Wrapper function to write a crystal to a res file on disc.

    :param crystal: A :class:`.Crystal` object
    :param file_location: Disk location to which the res file will
    be written to.
    """
    file_content = ResCrystalMapper().map_to_content(crystal)
    writer = ResFileWriter(file_content=file_content, output_location=file_location)
    writer.write()


class ResFileReader(FileReader):
    """
    Class that provides methods for parsing the content of SHELX (.res) file.
    """

    def __init__(self, input_location=None, file_content=None):
        super(self.__class__, self).__init__(
            input_location=input_location, file_content=file_content
        )

    def read(self):
        """
        Method that parse the content of a res file and construct a crystal
        from it.

        :return file_content: Content of the res file stored in the res file
        content object.
        :rtype: :class:`.res_file_content`
        """
        cell = None
        sfac = None
        symm = []
        atoms = []
        latt = None
        titl = None
        zerr = None
        unit = None
        fvar = None

        for line in self.file_content:
            if line.startswith("END"):
                break
            elif line.startswith("CELL"):
                cell = line[4:].strip()
            elif line.startswith("SFAC"):
                sfac = line[4:].strip()
            elif line.startswith("SYMM"):
                symm.append(line[4:].strip().replace(" ", ""))
            elif line.startswith("LATT"):
                latt = line[4:].strip()
            elif line.startswith("TITL"):
                titl = line[4:].strip()
            elif line.startswith("ZERR"):
                zerr = line[4:].strip()
            elif line.startswith("UNIT"):
                unit = line[4:].strip()
            elif line.startswith("FVAR"):
                fvar = line[4:].strip()
            else:
                atoms.append(line)
        # TODO: change this to dict unpacking
        return ResFileContent(
            TITL=titl,
            CELL=cell,
            LATT=latt,
            SFAC=sfac,
            SYMM=symm,
            ZERR=zerr,
            UNIT=unit,
            FVAR=fvar,
            atoms=atoms,
        )


class ResFileWriter(FileWriter):
    """
    Class that provides methods for writing out the content of SHELX (.res)
    file.
    """

    def __init__(self, output_location, file_content):
        super(self.__class__, self).__init__(output_location, file_content)

    def write(self):
        """
        Method to write the res file content to a res file on the location
        specified.
        """
        try:
            f = open(self.output_location, "w+")
        except IOError:
            raise Exception(
                "Can not open " + self.output_location + " to write crystal structure"
            )
        f.write(self.res_string_form)
        f.close()

    @property
    def res_string_form(self):
        """
        Given a res file content stored in the :class:`.res_file_content`
        object, convert it to a string form, with line breaks so it could be
        written out.
        """
        res_list = []

        if self.file_content.TITL is None:
            titl = "cspy_crystal"
        else:
            titl = self.file_content.TITL
        res_list += ["TITL " + titl]

        res_list += ["CELL " + " ".join(self.file_content.CELL)]

        if self.file_content.ZERR:
            res_list += ["ZERR " + str(self.file_content.ZERR)]

        res_list += ["LATT " + str(self.file_content.LATT)]

        for sym in self.file_content.SYMM:
            res_list += ["SYMM " + sym]

        res_list += ["SFAC " + " ".join(self.file_content.SFAC)]

        if self.file_content.UNIT:
            res_list += ["UNIT " + self.file_content.UNIT]

        if self.file_content.FVAR:
            res_list += ["FVAR " + self.file_content.FVAR]

        for atom in self.file_content.atoms:
            res_list += [atom]

        res_list += ["END"]
        return "\n".join(res_list)


class ResCrystalMapper(object):
    """
    Class that provides methods for converting between a turple of
    :class:`.ResFileContent` and an actual :class:`.Crystal` object.
    """

    def map_to_object(self, content):
        """
        Convert content in the turple of :class:`.ResFileContent` to a
        :class:`.Crystal` object.

        :param content: The raw file content from a res file.
        :type: :class:`.res_file_cotnent`
        :return: a fully constructed crystal object.
        :rtype: :class:`.Crystal`
        """
        self.content = content
        self.__construct_lattice()
        self.__construct_atoms()
        self.__construct_molecule()
        self.__construct_space_group()

        crystal = Crystal(
            asymmetric_unit=[self.__molecule],
            lattice=self.__lattice,
            space_group=self.__sg,
        )
        self.__molecule._crystal = crystal

        return crystal

    def __construct_lattice(self):
        """
        Method to construct a lattice from lattice parameters stored in the
        file content.

        :return: A fully constructed lattice.
        :rtype: :class:`.Lattice`
        """
        params = [float(x) for x in self.content.CELL.split()]
        self.__lattice = Lattice(
            a=params[-6],
            b=params[-5],
            c=params[-4],
            alpha=params[-3],
            beta=params[-2],
            gamma=params[-1],
        )

    def __construct_atoms(self):
        """
        Method to construct a list of atoms based on the lines of fractional
        coordinates stored in the file content.

        :return: A list of atom objects
        :rtype: list
        """
        self.__atoms = [
            self.__construct_atom(i) for i in range(len(self.content.atoms))
        ]

    def __construct_atom(self, index):
        """
        Convert a specific line of fractional coordinates in the file content
        to an Atom object.

        :param index: the index of atom to be converted in the lines of
        fractional coordinates stored in the file content.
        :return: A fully constructed atom object.
        :rtype: :class:`.Atom`
        """
        splitted = self.content.atoms[index].split()

        # first read in the fractional coordinates and convert it to Cartesian
        # coordinates
        fractional_coords = FractionalCoordinates(*map(float, splitted[2:5]))
        cartesian_coords = fractional_coords.to_cartesian_coords(self.__lattice)

        return Atom(label=splitted[0], xyz=cartesian_coords)

    def __construct_molecule(self):
        """
        Method to construct a molecule from the atomic coordinates stored in
        the file content. The molecule is simply a placeholder and no complex
        manipulation will be performed on it.

        :return: A molecule.
        :rtype: :class:`.Molecule`
        """
        self.__molecule = Molecule(atoms=self.__atoms)
        for atom in self.__atoms:
            atom._molecule = self.__molecule

    def __construct_space_group(self):
        """
        Method to construct a space group object based on the SYMM strings
        stored in the file content.

        Here, we get hold of all the symmetry elements for this space group
        based on the lattice type.  This needs to be done here before the full
        crystal is constructed bacause LATT type is not explicitly stored by
        the crystal object.

        :return: A space group
        :rtype: :class:`.SpaceGroup`
        """
        from cspy.deprecated.core.models.space_group import expand_symmetry_list

        full_symm_list = expand_symmetry_list(self.content.SYMM, int(self.content.LATT))
        symops = [SymmetryOperation(sym_op_string) for sym_op_string in full_symm_list]
        self.__sg = SpaceGroup(symops)

    def map_to_content(self, crystal):
        """
        Convert content an object of :class:`.Crystal` to a
        :class:`.ResFileContent` object.

        :param crystal: A crystal object.
        :type: :class:`.Crystal`
        :return: Content of res file to be written out to disc.
        :rtype: :class:`.ResFileContent`
        """
        self.crystal = crystal

        # where to get these information?
        TITL = None
        ZERR = None
        UNIT = None
        FVAR = None
        # information stored intrinsically in a crystal object

        # instead of using set, do it this way tend to preserve the order in
        # which each element appears in the res file.
        SFAC = []
        for mol in self.crystal.asymmetric_unit:
            for atom in mol.atoms:
                if atom.element.symbol not in SFAC:
                    SFAC.append(atom.element.symbol)

        CELL = [
            "1.0",
            str(round(self.crystal.lattice.a, 7)),
            str(round(self.crystal.lattice.b, 7)),
            str(round(self.crystal.lattice.c, 7)),
            str(round(self.crystal.lattice.alpha, 6)),
            str(round(self.crystal.lattice.beta, 6)),
            str(round(self.crystal.lattice.gamma, 6)),
        ]

        # factor out the lattice types before the symmetry operators for a
        # crystal system is written out.
        if isinstance(self.crystal.space_group, StandardSpaceGroup):
            LATT = self.crystal.space_group.lattice_centering.index
            if not self.crystal.space_group.inversion:
                LATT *= -1
            SYMM = [op.operation_string for op in self.crystal.space_group.symmetry]
        else:
            space_group, LATT = factor_out_lattice_type(self.crystal.space_group)
            SYMM = space_group.symmetry_list_for_res_content
            SYMM.remove("x,y,z")

        # Set up the dictionaries for figuring out the atom indicies, needed
        # for writing out the res fil.  SFAC gives the index of the type of
        # atoms in the molecule label_index gives the index of a specific type
        # of atom in the molecule, e.g. C1, C2, C3, etc.
        sfac_dict = {}
        symbol_index = {}
        for value, key in enumerate(SFAC):
            sfac_dict[key] = int(value + 1)
            symbol_index[key] = 0

        atoms = []
        for mol in self.crystal.asymmetric_unit:
            for atom in mol.atoms:
                symbol_index[atom.element.symbol] += 1
                this_atom_string = (
                    atom.element.symbol
                    + str(symbol_index[atom.element.symbol])
                    + " "
                    + str(sfac_dict[atom.element.symbol])
                    + " "
                )
                this_atom_string += " %15.9f" % atom.fractional_coordinates.x
                this_atom_string += " %15.9f" % atom.fractional_coordinates.y
                this_atom_string += " %15.9f" % atom.fractional_coordinates.z
                atoms.append(this_atom_string)

        return ResFileContent(
            TITL=TITL,
            CELL=CELL,
            LATT=LATT,
            SFAC=SFAC,
            SYMM=SYMM,
            ZERR=ZERR,
            UNIT=UNIT,
            FVAR=FVAR,
            atoms=atoms,
        )


ObjectMapper.register(ResCrystalMapper)
