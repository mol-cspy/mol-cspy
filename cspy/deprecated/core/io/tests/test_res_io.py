import logging
import unittest

from cspy.deprecated.core.io.res import (
    ResFileContent,
    ResFileReader,
    ResCrystalMapper,
    ResFileWriter,
)
from cspy.deprecated.core.models import Matrix3D, Vector3D

LOG = logging.getLogger(__name__)
# Need to have backslash to prevent additional newline
res_string = """\
TITL s.res.dmain xxiiB1opt_76_9507_primary_primary_optg_secondary\
 fort.16 xxiiB1
CELL 1.0   10.2468   10.2468    9.8045   90.0000   90.0000   90.0000
ZERR    4 0.0 0.0 0.0 0.0 0.0 0.0
LATT  -1
SYMM -Y , +X , +Z +1/4
SYMM -X , -Y , +Z +1/2
SYMM +Y , -X , +Z +3/4
SFAC C N S
C1          1   0.29278544   0.00047599   0.28603858
C2          1   0.35338171  -0.09805504   0.21877015
C3          1   0.34454043  -0.25101289   0.44577749
C4          1   0.28119997  -0.14650861   0.51560229
C5          1   0.29955411   0.13127047   0.23894216
C6          1   0.42640707  -0.07379276   0.09826200
C7          1   0.29252036  -0.16390376   0.65445226
C8          1   0.24476191  -0.08303474   0.75948229
N1          2   0.30152666   0.23862154   0.20293913
N2          2   0.48533088  -0.05810142  -0.00085105
N3          2   0.39925563  -0.34125846   0.52060132
N4          2   0.20618055  -0.01759031   0.84657556
S1          3   0.37675862  -0.30887685   0.68553339
S2          3   0.34816352  -0.26596554   0.26530954
S3          3   0.19534231  -0.01899653   0.43535680
END
"""
res_content = ResFileContent(
    TITL="s.res.dmain xxiiB1opt_76_9507_primary_primary_optg_secondary"
    " fort.16 xxiiB1",
    CELL="1.0   10.2468   10.2468    9.8045   90.0000   90.0000   90.0000",
    LATT="-1",
    SFAC="C N S",
    SYMM=["-Y,+X,+Z+1/4", "-X,-Y,+Z+1/2", "+Y,-X,+Z+3/4"],
    ZERR="4 0.0 0.0 0.0 0.0 0.0 0.0",
    UNIT=None,
    FVAR=None,
    atoms=[
        "C1          1   0.29278544   0.00047599   0.28603858",
        "C2          1   0.35338171  -0.09805504   0.21877015",
        "C3          1   0.34454043  -0.25101289   0.44577749",
        "C4          1   0.28119997  -0.14650861   0.51560229",
        "C5          1   0.29955411   0.13127047   0.23894216",
        "C6          1   0.42640707  -0.07379276   0.09826200",
        "C7          1   0.29252036  -0.16390376   0.65445226",
        "C8          1   0.24476191  -0.08303474   0.75948229",
        "N1          2   0.30152666   0.23862154   0.20293913",
        "N2          2   0.48533088  -0.05810142  -0.00085105",
        "N3          2   0.39925563  -0.34125846   0.52060132",
        "N4          2   0.20618055  -0.01759031   0.84657556",
        "S1          3   0.37675862  -0.30887685   0.68553339",
        "S2          3   0.34816352  -0.26596554   0.26530954",
        "S3          3   0.19534231  -0.01899653   0.43535680",
    ],
)


class TestResFileReader(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_reader_initialization(self):
        reader = ResFileReader(file_content=res_string)
        self.assertTrue(isinstance(reader.file_content, list))

    def test_read_output(self):
        reader = ResFileReader(file_content=res_string)
        output = reader.read()
        self.assertTrue(isinstance(output, ResFileContent))
        self.assertEqual(output.TITL, res_content.TITL)
        self.assertEqual(output.CELL, res_content.CELL)
        self.assertEqual(output.LATT, res_content.LATT)
        self.assertEqual(output.SFAC, res_content.SFAC)
        self.assertEqual(output.SYMM, res_content.SYMM)
        self.assertEqual(output.ZERR, res_content.ZERR)

        self.assertEqual(output.UNIT, res_content.UNIT)
        self.assertEqual(output.FVAR, res_content.FVAR)
        LOG.debug("output.atoms: %s", output.atoms)
        self.assertEqual(len(output.atoms), 15)
        self.assertEqual(output.atoms, res_content.atoms)


class TestResCrystalMapper(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_crystal_attributes_set_correctly(self):
        crystal = ResCrystalMapper().map_to_object(res_content)
        self.assertEqual(crystal.lattice.a, 10.2468)
        self.assertEqual(crystal.lattice.b, 10.2468)
        self.assertEqual(crystal.lattice.c, 9.8045)
        self.assertEqual(crystal.lattice.alpha, 90.0000)
        self.assertEqual(crystal.lattice.beta, 90.0000)
        self.assertEqual(crystal.lattice.gamma, 90.0000)
        self.assertEqual(len(crystal.asymmetric_unit), 1)
        self.assertEqual(len(crystal.asymmetric_unit[0].atoms), 15)
        lattice_vectors = Matrix3D(
            Vector3D(10.2468, 0, 0), Vector3D(0, 10.2468, 0), Vector3D(0, 0, 9.8045)
        )
        for i in range(len(lattice_vectors)):
            for j in range(len(lattice_vectors[i])):
                self.assertAlmostEqual(
                    crystal.lattice.lattice_vectors[i][j], lattice_vectors[i][j]
                )

        self.assertEqual(len(crystal.space_group.full_symmetry), 4)
        sym_op_strings = ["x,y,z", "-y,+x,+z+1/4", "-x,-y,+z+1/2", "+y,-x,+z+3/4"]
        for i in range(len(crystal.space_group.full_symmetry)):
            self.assertEqual(
                crystal.space_group.full_symmetry[i].original_string_form,
                sym_op_strings[i],
            )

        __frac_coords = [
            [0.29278544, 0.00047599, 0.28603858],
            [0.35338171, -0.09805504, 0.21877015],
            [0.34454043, -0.25101289, 0.44577749],
            [0.28119997, -0.14650861, 0.51560229],
            [0.29955411, 0.13127047, 0.23894216],
            [0.42640707, -0.07379276, 0.09826200],
            [0.29252036, -0.16390376, 0.65445226],
            [0.24476191, -0.08303474, 0.75948229],
            [0.30152666, 0.23862154, 0.20293913],
            [0.48533088, -0.05810142, -0.00085105],
            [0.39925563, -0.34125846, 0.52060132],
            [0.20618055, -0.01759031, 0.84657556],
            [0.37675862, -0.30887685, 0.68553339],
            [0.34816352, -0.26596554, 0.26530954],
            [0.19534231, -0.01899653, 0.43535680],
        ]

        for i, atom in enumerate(crystal.asymmetric_unit[0].atoms):
            self.assertAlmostEqual(atom.xyz[0], __frac_coords[i][0] * 10.2468)
            self.assertAlmostEqual(atom.xyz[1], __frac_coords[i][1] * 10.2468)
            self.assertAlmostEqual(atom.xyz[2], __frac_coords[i][2] * 9.8045)

    def test_map_crystal_to_res_content(self):
        crystal = ResCrystalMapper().map_to_object(res_content)
        file_content = ResCrystalMapper().map_to_content(crystal)
        self.assertEqual(file_content.TITL, None)
        self.assertEqual(file_content.ZERR, None)
        self.assertEqual(file_content.UNIT, None)
        self.assertEqual(file_content.FVAR, None)
        self.assertEqual(file_content.SFAC, ["C", "N", "S"])
        self.assertEqual(
            file_content.CELL,
            ["1.0", "10.2468", "10.2468", "9.8045", "90.0", "90.0", "90.0"],
        )
        self.assertEqual(file_content.LATT, -1)
        self.assertEqual(
            file_content.SYMM, ["-y,+x,+z+1/4", "-x,-y,+z+1/2", "+y,-x,+z+3/4"]
        )
        self.assertEqual(
            file_content.atoms,
            [
                "C1 1      0.292785440     0.000475990     0.286038580",
                "C2 1      0.353381710    -0.098055040     0.218770150",
                "C3 1      0.344540430    -0.251012890     0.445777490",
                "C4 1      0.281199970    -0.146508610     0.515602290",
                "C5 1      0.299554110     0.131270470     0.238942160",
                "C6 1      0.426407070    -0.073792760     0.098262000",
                "C7 1      0.292520360    -0.163903760     0.654452260",
                "C8 1      0.244761910    -0.083034740     0.759482290",
                "N1 2      0.301526660     0.238621540     0.202939130",
                "N2 2      0.485330880    -0.058101420    -0.000851050",
                "N3 2      0.399255630    -0.341258460     0.520601320",
                "N4 2      0.206180550    -0.017590310     0.846575560",
                "S1 3      0.376758620    -0.308876850     0.685533390",
                "S2 3      0.348163520    -0.265965540     0.265309540",
                "S3 3      0.195342310    -0.018996530     0.435356800",
            ],
        )


class TestResFileWriter(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_correct_res_list_form(self):
        crystal = ResCrystalMapper().map_to_object(res_content)
        file_content = ResCrystalMapper().map_to_content(crystal)
        writer = ResFileWriter(file_content=file_content, output_location="")
        self.assertEqual(
            writer.res_string_form.split("\n"),
            [
                "TITL cspy_crystal",
                "CELL 1.0 10.2468 10.2468 9.8045 90.0 90.0 90.0",
                "LATT -1",
                "SYMM -y,+x,+z+1/4",
                "SYMM -x,-y,+z+1/2",
                "SYMM +y,-x,+z+3/4",
                "SFAC C N S",
                "C1 1      0.292785440     0.000475990     0.286038580",
                "C2 1      0.353381710    -0.098055040     0.218770150",
                "C3 1      0.344540430    -0.251012890     0.445777490",
                "C4 1      0.281199970    -0.146508610     0.515602290",
                "C5 1      0.299554110     0.131270470     0.238942160",
                "C6 1      0.426407070    -0.073792760     0.098262000",
                "C7 1      0.292520360    -0.163903760     0.654452260",
                "C8 1      0.244761910    -0.083034740     0.759482290",
                "N1 2      0.301526660     0.238621540     0.202939130",
                "N2 2      0.485330880    -0.058101420    -0.000851050",
                "N3 2      0.399255630    -0.341258460     0.520601320",
                "N4 2      0.206180550    -0.017590310     0.846575560",
                "S1 3      0.376758620    -0.308876850     0.685533390",
                "S2 3      0.348163520    -0.265965540     0.265309540",
                "S3 3      0.195342310    -0.018996530     0.435356800",
                "END",
            ],
        )


if __name__ == "__main__":
    unittest.main()
