import unittest
import numpy
import os

from cspy.deprecated.core.io.xyz import XYZFileReader, XYZFileWriter
from cspy.deprecated.core.models import Vector3D, Atom, Molecule

reference_string = """31
comment line
Cl1        17.316686954      5.318958438      6.633051174
Cl2        19.129721605      0.317151358      7.531321798
O1         10.201691639      1.730919861      8.367608883
H1          9.376442848      1.229490056      8.438736355
O2          9.626094277      1.535659056      6.201002367
N1         13.424911170      3.796852815      7.041320994
N2         12.437909548      3.143261836      7.679637975
C1         10.390886562      1.940465547      7.047857809
C2         11.609899034      2.698027412      6.740644734
C3         12.074277696      3.054267376      5.433661525
C4         11.631403090      2.858823794      4.116366271
H4         10.711263172      2.322327038      3.930321674
C5         12.407961494      3.366985025      3.091131258
H5         12.094013048      3.229764880      2.063370031
C6         13.610675271      4.061158421      3.350661834
H6         14.192517388      4.435782987      2.516854003
C7         14.067127336      4.271251030      4.639935866
H7         14.994164681      4.794631028      4.834589997
C8         13.273790120      3.761020134      5.674003718
C9         14.566165496      4.298945629      7.787047571
H9A        14.227077506      4.421163497      8.815550759
H9B        14.833482124      5.279563285      7.398172424
C10        15.738424090      3.342866894      7.721439588
C11        16.992823539      3.688351918      7.219181267
C12        18.040933894      2.774039001      7.156542347
H12        19.002761690      3.068226476      6.762405187
C13        17.820669168      1.480628059      7.606115186
C14        16.583601644      1.092722877      8.111847323
H14        16.429517841      0.079460048      8.456551341
C15        15.560486340      2.027059472      8.162778969
H15        14.587329211      1.738510317      8.543142346
"""
reference_atoms = [
    Atom("Cl1", Vector3D(17.316686954, 5.318958438, 6.633051174)),
    Atom("Cl2", Vector3D(19.129721605, 0.317151358, 7.531321798)),
    Atom("O1", Vector3D(10.201691639, 1.730919861, 8.367608883)),
    Atom("H1", Vector3D(9.376442848, 1.229490056, 8.438736355)),
    Atom("O2", Vector3D(9.626094277, 1.535659056, 6.201002367)),
    Atom("N1", Vector3D(13.424911170, 3.796852815, 7.041320994)),
    Atom("N2", Vector3D(12.437909548, 3.143261836, 7.679637975)),
    Atom("C1", Vector3D(10.390886562, 1.940465547, 7.047857809)),
    Atom("C2", Vector3D(11.609899034, 2.698027412, 6.740644734)),
    Atom("C3", Vector3D(12.074277696, 3.054267376, 5.433661525)),
    Atom("C4", Vector3D(11.631403090, 2.858823794, 4.116366271)),
    Atom("H4", Vector3D(10.711263172, 2.322327038, 3.930321674)),
    Atom("C5", Vector3D(12.407961494, 3.366985025, 3.091131258)),
    Atom("H5", Vector3D(12.094013048, 3.229764880, 2.063370031)),
    Atom("C6", Vector3D(13.610675271, 4.061158421, 3.350661834)),
    Atom("H6", Vector3D(14.192517388, 4.435782987, 2.516854003)),
    Atom("C7", Vector3D(14.067127336, 4.271251030, 4.639935866)),
    Atom("H7", Vector3D(14.994164681, 4.794631028, 4.834589997)),
    Atom("C8", Vector3D(13.273790120, 3.761020134, 5.674003718)),
    Atom("C9", Vector3D(14.566165496, 4.298945629, 7.787047571)),
    Atom("H9A", Vector3D(14.227077506, 4.421163497, 8.815550759)),
    Atom("H9B", Vector3D(14.833482124, 5.279563285, 7.398172424)),
    Atom("C10", Vector3D(15.738424090, 3.342866894, 7.721439588)),
    Atom("C11", Vector3D(16.992823539, 3.688351918, 7.219181267)),
    Atom("C12", Vector3D(18.040933894, 2.774039001, 7.156542347)),
    Atom("H12", Vector3D(19.002761690, 3.068226476, 6.762405187)),
    Atom("C13", Vector3D(17.820669168, 1.480628059, 7.606115186)),
    Atom("C14", Vector3D(16.583601644, 1.092722877, 8.111847323)),
    Atom("H14", Vector3D(16.429517841, 0.079460048, 8.456551341)),
    Atom("C15", Vector3D(15.560486340, 2.027059472, 8.162778969)),
    Atom("H15", Vector3D(14.587329211, 1.738510317, 8.543142346)),
]
output_ref_string = "\n".join(
    [
        "31",
        "XYZ string form",
        "Cl1           17.316686954        5.318958438         6.633051174",
        "Cl2           19.129721605        0.317151358         7.531321798",
        "O1            10.201691639        1.730919861         8.367608883",
        "H1            9.376442848         1.229490056         8.438736355",
        "O2            9.626094277         1.535659056         6.201002367",
        "N1            13.42491117         3.796852815         7.041320994",
        "N2            12.437909548        3.143261836         7.679637975",
        "C1            10.390886562        1.940465547         7.047857809",
        "C2            11.609899034        2.698027412         6.740644734",
        "C3            12.074277696        3.054267376         5.433661525",
        "C4            11.63140309         2.858823794         4.116366271",
        "H4            10.711263172        2.322327038         3.930321674",
        "C5            12.407961494        3.366985025         3.091131258",
        "H5            12.094013048        3.22976488          2.063370031",
        "C6            13.610675271        4.061158421         3.350661834",
        "H6            14.192517388        4.435782987         2.516854003",
        "C7            14.067127336        4.27125103          4.639935866",
        "H7            14.994164681        4.794631028         4.834589997",
        "C8            13.27379012         3.761020134         5.674003718",
        "C9            14.566165496        4.298945629         7.787047571",
        "H9A           14.227077506        4.421163497         8.815550759",
        "H9B           14.833482124        5.279563285         7.398172424",
        "C10           15.73842409         3.342866894         7.721439588",
        "C11           16.992823539        3.688351918         7.219181267",
        "C12           18.040933894        2.774039001         7.156542347",
        "H12           19.00276169         3.068226476         6.762405187",
        "C13           17.820669168        1.480628059         7.606115186",
        "C14           16.583601644        1.092722877         8.111847323",
        "H14           16.429517841        0.079460048         8.456551341",
        "C15           15.56048634         2.027059472         8.162778969",
        "H15           14.587329211        1.738510317         8.543142346",
    ]
)


class TestXYZFileReader(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_reader_initialization(self):
        reader = XYZFileReader(file_content=reference_string)
        self.assertTrue(isinstance(reader.file_content, list))

    def test_read_output(self):
        reader = XYZFileReader(file_content=reference_string)
        output = reader.read()
        self.assertTrue(isinstance(output, Molecule))
        self.assertEqual(len(output.atoms), 31)
        for reference_atom, output_atom in zip(reference_atoms, output.atoms):
            self.assertEqual(reference_atom.label, output_atom.label)
            numpy.testing.assert_array_almost_equal(reference_atom.xyz, output_atom.xyz)

    def test_bad_comment(self):
        bad_xyz = """2
        H 0 1 2
        H 3 4 5"""
        reader = XYZFileReader(file_content=bad_xyz)
        with self.assertRaises(AssertionError):
            output = reader.read()  # noqa

    def test_missing_label(self):
        bad_xyz = """2
        H 0 1 2
        3 4 5"""
        reader = XYZFileReader(file_content=bad_xyz)
        with self.assertRaises(AssertionError):
            output = reader.read()  # noqa

    def test_extra_field(self):
        bad_xyz = """2
        H 0 1 2
        H 3 4 5 6"""
        reader = XYZFileReader(file_content=bad_xyz)
        with self.assertRaises(AssertionError):
            output = reader.read()  # noqa


class TestXYZFileWriter(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        if os.path.exists("test.xyz"):
            os.remove("test.xyz")
        return

    def test_writer_xyz_string(self):
        molecule = Molecule(atoms=reference_atoms)
        xyz_string = XYZFileWriter(output_location="", file_content="").xyz_string_form(
            molecule
        )
        self.maxDiff = None
        self.assertEqual(xyz_string.split("\n"), output_ref_string.split("\n"))

    def test_writer_xyz_file(self):
        molecule = Molecule(atoms=reference_atoms)
        xyz_string = XYZFileWriter(  # noqa
            output_location="test.xyz", file_content=""
        ).write(molecule)
        assert os.path.exists("test.xyz")
        with open("test.xyz", "r") as f:
            output = f.read()
            self.assertEqual(output, output_ref_string)


if __name__ == "__main__":
    unittest.main()
