TITL PAXSAK PAXSAK PAXSAK
CELL  0.7 6.5511 6.9192 8.0265 93.73 102.78 91.769
ZERR 2 0.0011 0.0012 0.0017 0.0010 0.0010 0.0010
LATT 1
SFAC  C H N O
UNIT 16 18 2 2
FVAR 1.00
C1 1     0.128544735     0.609416145     0.720001427 
C2 1     0.479361893     0.516098921     0.715800342 
C3 1     0.318932556     0.645336913     0.673070989 
C4 1     0.448940123     0.353941520     0.802661860 
C5 1     0.207558303     0.899573774     0.470908537 
C6 1     0.099281596     0.444289843     0.803983104 
C7 1     0.258130875     0.313972018     0.847668208 
C8 1     0.227337190     0.139355060     0.944083290 
H1 2     0.503383596     0.859437820     0.596911270 
H2 2     0.004785976     0.711060978     0.693650664 
H3 2     0.627158660     0.542217993     0.678979274 
H4 2     0.575470153     0.255481354     0.834690795 
H5 2     0.048966267     0.830232223     0.449981459 
H6 2    -0.049880989     0.418116748     0.838587437 
H7 2     0.061708278     0.093387093     0.921724297 
H8 2     0.284679334     0.170646634     1.082617563 
H9 2     0.312376215     0.016582726     0.905867920 
N1 3     0.353820477     0.809005783     0.585506162 
O1 4     0.248004759     1.040226455     0.398525407 
END