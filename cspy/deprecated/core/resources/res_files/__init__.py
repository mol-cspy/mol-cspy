"""
A selection of CCDC structures optimized at the B3LYP-GD3BJ/6-31G(d,p)
level of theory with the following com file header:

'%mem=1GB
%nprocshared=8
%chk=molecule_0
#B3LYP/6-31G(d,p) FChk NoSymmetry Opt=(ModRedundant,MaxCycle=500,MaxStep=29) \
    Integral(Grid=UltraFine) GEOM(PrintInputOrient) \
    EmpiricalDispersion=GD3BJ SCF=(MaxCycle=65,tight)

Gaussian Calculation

0 1'

Structures generated with the following command:

python ~/CSPy_blind_test/cspy/crystal.py ${NAME}.res --optimise_exp_structure \
    -gopt "ModRedundant,MaxCycle=500,MaxStep=29" --additional_args \
    "Integral(Grid=UltraFine) GEOM(PrintInputOrient) EmpiricalDispersion=GD3BJ\
        SCF=(MaxCycle=65,tight)" -bas "6-31G(d,p)"\
    -fun B3LYP --res_output_filename ${NAME}opt.res -np 8
"""
