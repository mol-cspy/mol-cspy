from cspy.deprecated.core.models import Crystal, Molecule, Atom, Vector3D, Lattice
from cspy.deprecated.core.resources.crystallographic_space_groups import (
    CrystallographicSpaceGroups,
)


AABHTZ = Crystal(
    asymmetric_unit=[
        Molecule(
            [
                Atom("Cl", Vector3D(-3.672011, 0.794130, 0.703424)),
                Atom("Cl", Vector3D(-6.165445, -3.843340, 2.167950)),
                Atom("C", Vector3D(-4.922134, -0.093188, 1.529499)),
                Atom("C", Vector3D(-5.842684, 0.639019, 2.270049)),
                Atom("C", Vector3D(-6.814849, -0.015607, 2.972147)),
                Atom("C", Vector3D(-6.873575, -1.397917, 2.948279)),
                Atom("C", Vector3D(-5.962153, -2.115550, 2.184524)),
                Atom("C", Vector3D(-4.945151, -1.491127, 1.445963)),
                Atom("C", Vector3D(-3.946263, -2.167189, 0.587402)),
                Atom("N", Vector3D(-3.520581, -3.326939, 0.895688)),
                Atom("N", Vector3D(-2.547213, -3.901826, 0.065635)),
                Atom("C", Vector3D(-2.127396, -3.359001, -1.161544)),
                Atom("N", Vector3D(-2.870984, -3.230996, -2.218999)),
                Atom("N", Vector3D(-2.064454, -2.653189, -3.210820)),
                Atom("C", Vector3D(-0.899225, -2.463904, -2.691705)),
                Atom("N", Vector3D(-0.872170, -2.891134, -1.394913)),
                Atom("N", Vector3D(0.161958, -2.740134, -0.491269)),
                Atom("C", Vector3D(0.007793, -1.798001, 0.493258)),
                Atom("C", Vector3D(1.126483, -1.740381, 1.468504)),
                Atom("O", Vector3D(-0.966549, -1.070019, 0.525744)),
                Atom("C", Vector3D(-2.015356, -5.115695, 0.476021)),
                Atom("C", Vector3D(-2.591052, -5.741894, 1.689277)),
                Atom("O", Vector3D(-1.113293, -5.597075, -0.196243)),
                Atom("H", Vector3D(-5.772884, 1.633167, 2.300546)),
                Atom("H", Vector3D(-7.457111, 0.476851, 3.520432)),
                Atom("H", Vector3D(-7.564376, -1.921010, 3.513803)),
                Atom("H", Vector3D(-3.590775, -1.719331, -0.212154)),
                Atom("H", Vector3D(-0.145202, -2.027951, -3.109384)),
                Atom("H", Vector3D(0.761794, -3.351387, -0.377900)),
                Atom("H", Vector3D(1.478868, -0.828033, 1.438670)),
                Atom("H", Vector3D(1.863513, -2.423085, 1.253035)),
                Atom("H", Vector3D(0.844696, -1.799233, 2.207729)),
                Atom("H", Vector3D(-2.243774, -6.690614, 1.823199)),
                Atom("H", Vector3D(-2.351408, -5.399382, 2.479551)),
                Atom("H", Vector3D(-3.537833, -5.877199, 1.611045)),
            ]
        )
    ],
    lattice=Lattice(11.372, 10.272, 7.359, 108.75, 71.07, 96.16),
    space_group=CrystallographicSpaceGroups.get(2),
)
