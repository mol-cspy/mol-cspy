from __future__ import division, absolute_import, print_function

from fractions import Fraction

from cspy.deprecated.core.models.space_group_operation import SpaceGroupOperation
from cspy.deprecated.core.models.cvector3d import cVector3D


class LinkedDimension(object):
    def __init__(self, linked_index):
        self.linked_index = linked_index


class MustNotBe(object):
    def __init__(self, index, value):
        self.index = index
        self.value = value


class MustBe(object):
    def __init__(self, index, value):
        self.index = index
        self.value = value


class CrystalSystem(object):
    def __init__(self, index, a, b, c, alpha, beta, gamma):
        self.index = index
        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

    def get_defined_index(self, index):
        if index == 0:
            return self.a
        if index == 1:
            return self.b
        if index == 2:
            return self.c
        if index == 3:
            return self.alpha
        if index == 4:
            return self.beta
        if index == 5:
            return self.gamma

    def is_index_restricted(self, index):
        p = self.get_real_index(index)
        return isinstance(p, MustBe)

    def get_real_index(self, index):
        lpi = self.get_defined_index(index)
        if isinstance(lpi, LinkedDimension):
            return self.get_real_index(lpi.linked_index)
        else:
            return lpi

    def set_lattice_parameter(self, lattice, index, value):
        _index = self.get_real_index(index)
        if isinstance(_index, MustBe):
            assert _index.value == value
            lattice.set_by_index(_index.index, value)
        elif isinstance(_index, MustNotBe):
            assert _index.value != value
            lattice.set_by_index(_index.index, value)
        else:
            for i, x in enumerate(
                [self.a, self.b, self.c, self.alpha, self.beta, self.gamma]
            ):
                if x == _index or (
                    isinstance(x, LinkedDimension) and x.linked_index == index
                ):
                    lattice.set_by_index(i, value)

    def get_lattice_parameter(self, lattice, index):
        _index = self.get_real_index(index)
        return lattice.parameters[_index]

    def get_linked_index(self, index):
        return [
            i
            for i, x in enumerate([self.a, self.b, self.c])
            if isinstance(x, LinkedDimension) and x.linked_index == index or x == index
        ]

    def get_number_of_linked_parameters(self, index):
        return 1 + sum(
            [
                1
                for x in [self.a, self.b, self.c]
                if isinstance(x, LinkedDimension) and x.linked_index == index
            ]
        )

    @property
    def variable_lengths(self):
        return [
            i
            for i, x in enumerate([self.a, self.b, self.c])
            if not isinstance(x, LinkedDimension)
        ]

    @property
    def num_variable_lengths(self):
        return len(
            [
                None
                for x in [self.a, self.b, self.c]
                if not isinstance(x, LinkedDimension)
            ]
        )

    @property
    def num_variable_angles(self):
        return len(
            [
                None
                for x in [self.alpha, self.beta, self.gamma]
                if not isinstance(x, LinkedDimension) and not isinstance(x, MustBe)
            ]
        )

    def __repr__(self):
        return "{}: a={} b={} c={} alpha={} beta={} gamma={}>".format(
            self.__class__, self.a, self.b, self.c, self.alpha, self.beta, self.gamma
        )


class CrystalSystems(object):
    TRICLINIC = CrystalSystem(1, 0, 1, 2, 3, 4, 5)
    MONOCLINIC = CrystalSystem(
        2, 0, 1, 2, MustBe(3, 90), MustNotBe(4, 90), MustBe(5, 90)
    )
    ORTHORHOMBIC = CrystalSystem(
        3, 0, 1, 2, MustBe(3, 90), MustBe(4, 90), MustBe(5, 90)
    )
    TETRAGONAL = CrystalSystem(
        4, 0, LinkedDimension(0), 2, MustBe(3, 90), MustBe(4, 90), MustBe(5, 90)
    )
    # Rhobohedral axes
    TRIGONAL = CrystalSystem(
        5,
        0,
        LinkedDimension(0),
        LinkedDimension(0),
        MustNotBe(3, 90),
        LinkedDimension(3),
        LinkedDimension(3),
    )
    # Hexagonal axes
    # TRIGONAL = CrystalSystem(5, 0, LinkedDimension(0), 2, MustBe(3, 90),
    # MustBe(4, 90), MustBe(5, 120))
    HEXAGONAL = CrystalSystem(
        6, 0, LinkedDimension(0), 2, MustBe(3, 90), MustBe(4, 90), MustBe(5, 120)
    )
    CUBIC = CrystalSystem(
        7,
        0,
        LinkedDimension(0),
        LinkedDimension(0),
        MustBe(3, 90),
        MustBe(4, 90),
        MustBe(5, 90),
    )


class Centering(object):
    def __init__(self, index, letter, name, additional_lattice_points):
        self.index = index
        self.letter = letter
        self.name = name
        self.additional_lattice_points = additional_lattice_points

    def transform(self, op):
        additional_ops = []
        for point in self.additional_lattice_points:

            def func(x):
                return "{0}+{3},{1}+{4},{2}+{5}".format(*(x.split(",") + list(point)))

            additional_ops.append(op.__class__(func(op.operation_string)))
        return additional_ops


class LatticeCentering(object):
    P = Centering(1, "P", "Primitive", [])
    I = Centering(2, "I", "Body-Centered", [(0.5, 0.5, 0.5)])  # noqa: E741
    # Hexagonal axes system
    H = Centering(
        3,
        "H",
        "Hexagonal",
        [(Fraction(2, 3), Fraction(1, 3), 0.0), (Fraction(1, 3), Fraction(2, 3), 0.0)],
    )
    R = Centering(
        1,
        "R",
        "Rhombohedral",
        [
            (Fraction(2, 3), Fraction(1, 3), Fraction(1, 3)),
            (Fraction(1, 3), Fraction(2, 3), Fraction(2, 3)),
        ],
    )
    F = Centering(
        4, "F", "Face-Centered", [(0.0, 0.5, 0.5), (0.5, 0.0, 0.5), (0.5, 0.5, 0.0)]
    )
    A = Centering(5, "A", "Base-Centered A", [(0.0, 0.5, 0.5)])
    B = Centering(6, "B", "Base-Centered B", [(0.5, 0.0, 0.5)])
    C = Centering(7, "C", "Base-Centered C", [(0.5, 0.5, 0.0)])


class Inversion(object):
    YES = True
    NO = False
    UNKNOWN = None


class StandardSpaceGroup(object):
    def __init__(
        self,
        index,
        name,
        crystal_system,
        lattice_centering,
        inversion,
        symmetry,
        asymmetric_unit,
        unique_axis,
    ):
        self.index = index
        self.name = name
        self.crystal_system = crystal_system
        self.lattice_centering = lattice_centering
        self.inversion = inversion
        self.symmetry = symmetry
        self.asymmetric_unit = asymmetric_unit
        self.unique_axis = unique_axis
        self.non_centering_symmetry = []
        self.compute_non_centering_symmetry()
        self.full_symmetry = []
        self.compute_full_symmetry()

    @property
    def identity(self):
        return SpaceGroupOperation("x,y,z")

    def compute_non_centering_symmetry(self):
        for op in [self.identity] + self.symmetry:
            self.non_centering_symmetry.append(op)

        if self.inversion:
            for i in range(len(self.non_centering_symmetry)):
                op = self.non_centering_symmetry[i]
                self.non_centering_symmetry.append(op.inversion())

    def compute_full_symmetry(self):
        self.full_symmetry = [op for op in self.non_centering_symmetry]
        if self.lattice_centering:
            for i in range(len(self.full_symmetry)):
                op = self.full_symmetry[i]
                centering_ops = self.lattice_centering.transform(op)
                self.full_symmetry += centering_ops

    def symmetry_unique_points(self, test_site=cVector3D(0.2, 0.6, -0.15)):
        """
        Returns number of unique points along each axis

        In practice, this needs to be understood with the help of the group
        symmetry diagrams in the International Table of Crystallography, and we
        need to basically work out how many unique positions were allowed along
        each of the three axis in fractional space. The method works by
        constructing a vector between a given point and another one in the
        factional coordinate space generated by the symmetry operator. We then
        look at each of the three components of this vector, and see if that
        component had been previously generated by another symmetry operation.
        If not, this accounts for a unique point, otherwise, it is not. The
        unique point bounds will count how many unique molecules can be placed
        along each axis (which is number of unique points times the number of
        unique molecules.

        :return: number of unique point along each axis
        :rtype: list of ints

        """

        unique_points = [[], [], []]
        for sym_op in self.full_symmetry:
            r = sym_op.apply(test_site) - test_site
            for axis in range(3):
                if not any([abs(r[axis] - x) < 0.001 for x in unique_points[axis]]):
                    unique_points[axis].append(r[axis])

        return [len(x) for x in unique_points]


class Symmetry(object):
    NONE = []

    @staticmethod
    def get(value):
        if value in ("", "UNKNOWN"):
            return []
        return [SpaceGroupOperation(v) for v in value.split(";")]


class AsymmetricUnit(object):
    UNKNOWN = [[0, 1.00], [0, 1.00], [0, 1.00]]
    FULL = [[0, 1.00], [0, 1.00], [0, 1.00]]
    HALF_X = [[0, 0.50], [0, 1.00], [0, 1.00]]
    HALF_Y = [[0, 1.00], [0, 0.50], [0, 1.00]]
    HALF_Z = [[0, 1.00], [0, 1.00], [0, 0.50]]
    QUART_Y = [[0, 1.00], [0, 0.25], [0, 1.00]]
    HALF_X_QUART_Y = [[0, 0.50], [0, 0.25], [0, 1.00]]
    HALF_XZ = [[0, 0.50], [0, 1.00], [0, 0.50]]
    HALF_XY = [[0, 0.50], [0, 0.50], [0, 1.00]]
    EIGHT_Z = [[0, 1.00], [0, 1.00], [0, 0.125]]


class UniqueAxis(object):
    UNKNOWN = -1
    NA = -1
    X = 0
    Y = 1
    Z = 2


class SpaceGroup(object):
    def __init__(self, symmetry_operations):
        assert isinstance(symmetry_operations, list)
        if isinstance(symmetry_operations[0], SpaceGroupOperation):
            _symmetry_operations = symmetry_operations
        else:
            _symmetry_operations = [
                SpaceGroupOperation(op) for op in symmetry_operations
            ]
        self.symmetry_operations = _symmetry_operations

    @property
    def full_symmetry(self):
        return self.symmetry_operations

    @property
    def symmetry_list_for_res_content(self):
        symm = []
        for sym_op in self.symmetry_operations:
            cleaned = str(sym_op).replace(" ", "").lower().replace("+", "")
            if cleaned != "x,y,z":
                symm.append(sym_op.original_string_form)
        return symm

    def symmetry_unique_points(self):
        """
        Returns number of unique points along each axis

        In practice, this needs to be understood with the help of the group
        symmetry diagrams in the International Table of Crystallography, and we
        need to basically work out how many unique positions were allowed along
        each of the three axis in fractional space. The method works by
        constructing a vector between a given point and another one in the
        factional coordinate space generated by the symmetry operator. We then
        look at each of the three components of this vector, and see if that
        component had been previously generated by another symmetry operation.
        If not, this accounts for a unique point, otherwise, it is not. The
        unique point bounds will count how many unique molecules can be placed
        along each axis (which is number of unique points times the number of
        unique molecules.

        :return: number of unique point along each axis
        :rtype: list of ints

        """

        unique_points = [[], [], []]
        non_special_site = cVector3D(0.2, 0.6, -0.15)
        for sym_op in self.symmetry_operations:
            r = sym_op.apply(non_special_site) - non_special_site
            for axis in range(3):
                if not any([abs(r[axis] - x) < 0.001 for x in unique_points[axis]]):
                    unique_points[axis].append(r[axis])
        return [len(x) for x in unique_points]


# TODO: tidy this up later, not sure if this should be here? (JY)


def expand_symmetry_list(symm_list_input, lattice_type):
    # 1=P, 2=I, 3=rhombohedral obverse on hexagonal axes, 4=F, 5=A, 6=B, 7=C.
    import copy

    symm_list_in = [
        op.replace(" ", "").lower()
        for op in symm_list_input
        if op.replace(" ", "").lower() != "x,y,z"
    ]
    symm_list_in = ["x,y,z"] + symm_list_in
    symm_list = copy.deepcopy(symm_list_in)

    if abs(lattice_type) == 2:

        def func(x):
            return "%s+1/2,%s+1/2,%s+1/2" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

    if abs(lattice_type) == 3:

        def func(x):
            return "%s+2/3,%s+1/3,%s+1/3" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

        def func(x):
            return "%s+1/3,%s+2/3,%s+2/3" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

    if abs(lattice_type) in [4, 5]:

        def func(x):
            return "%s,%s+1/2,%s+1/2" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

    if abs(lattice_type) in [4, 6]:

        def func(x):
            return "%s+1/2,%s,%s+1/2" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

    if abs(lattice_type) in [4, 7]:

        def func(x):
            return "%s+1/2,%s+1/2,%s" % tuple(x.split(","))

        symm_list += map(func, symm_list_in)

    if lattice_type > 0:

        def func(x):
            return "-1*(%s),-1*(%s),-1*(%s)" % tuple(x.split(","))

        symm_list += copy.deepcopy(map(func, symm_list))

    return symm_list
