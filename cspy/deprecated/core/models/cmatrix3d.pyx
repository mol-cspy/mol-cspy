from cpython cimport Py_buffer
from cspy.deprecated.core.models.cvector3d cimport cVector3D, Points, Point
import cython

cdef class cMatrix3D:
    def __init__(self, cVector3D row1, cVector3D row2, cVector3D row3):
        self.data[0][0] = row1.xyz[0]
        self.data[0][1] = row1.xyz[1]
        self.data[0][2] = row1.xyz[2]
        self.data[1][0] = row2.xyz[0]
        self.data[1][1] = row2.xyz[1]
        self.data[1][2] = row2.xyz[2]
        self.data[2][0] = row3.xyz[0]
        self.data[2][1] = row3.xyz[1]
        self.data[2][2] = row3.xyz[2]

    def __reduce__(self):
        return (self.__class__, (self[0], self[1], self[2]))

    def __getitem__(self, x):
        return cVector3D(self.data[x][0], self.data[x][1], self.data[x][2])

    def __str__(self):
        return '(({}),({}),({}))'.format(self[0], self[1], self[2])

    def g(self, x, y):
        return self.data[x*3+y]

    def __len__(self):
        return 3
    def inverse(self):
        cdef double det = self.data[0][0] * (self.data[1][1] * self.data[2][2] - self.data[2][1] * self.data[1][2]) -\
                          self.data[0][1] * (self.data[1][0] * self.data[2][2] - self.data[1][2] * self.data[2][0]) +\
                          self.data[0][2] * (self.data[1][0] * self.data[2][1] - self.data[1][1] * self.data[2][0])
        
        cdef double invdet = 1. / det
        
        cdef cMatrix3D minv = cMatrix3D(cVector3D(0,0,0),cVector3D(0,0,0),cVector3D(0,0,0))
        minv.data[0][0] = (self.data[1][1] * self.data[2][2] - self.data[2][1] * self.data[1][2]) * invdet;
        minv.data[0][1] = (self.data[0][2] * self.data[2][1] - self.data[0][1] * self.data[2][2]) * invdet;
        minv.data[0][2] = (self.data[0][1] * self.data[1][2] - self.data[0][2] * self.data[1][1]) * invdet;
        minv.data[1][0] = (self.data[1][2] * self.data[2][0] - self.data[1][0] * self.data[2][2]) * invdet;
        minv.data[1][1] = (self.data[0][0] * self.data[2][2] - self.data[0][2] * self.data[2][0]) * invdet;
        minv.data[1][2] = (self.data[1][0] * self.data[0][2] - self.data[0][0] * self.data[1][2]) * invdet;
        minv.data[2][0] = (self.data[1][0] * self.data[2][1] - self.data[2][0] * self.data[1][1]) * invdet;
        minv.data[2][1] = (self.data[2][0] * self.data[0][1] - self.data[0][0] * self.data[2][1]) * invdet;
        minv.data[2][2] = (self.data[0][0] * self.data[1][1] - self.data[1][0] * self.data[0][1]) * invdet;

        return minv

    cpdef mat_mat_mul(self, n):
        nT = n.transpose()
        A = cVector3D(self[0].dot(nT[0]), self[0].dot(nT[1]), self[0].dot(nT[2]))
        B = cVector3D(self[1].dot(nT[0]), self[1].dot(nT[1]), self[1].dot(nT[2]))
        C = cVector3D(self[2].dot(nT[0]), self[2].dot(nT[1]), self[2].dot(nT[2]))
        return cMatrix3D(A, B, C)

    cpdef transpose(self):
        return cMatrix3D(cVector3D(self.data[0][0], self.data[1][0], self.data[2][0]),
                         cVector3D(self.data[0][1], self.data[1][1], self.data[2][1]),
                         cVector3D(self.data[0][2], self.data[1][2], self.data[2][2]))
    @classmethod
    def identity(cls, diag=1.0):
        return cls(cVector3D(diag, 0., 0.),
                   cVector3D(0., diag, 0.),
                   cVector3D(0., 0., diag))

    @classmethod
    def zeros(cls):
        return cls.identity(diag=0.)

    def __richcmp__(self, other, op):
        if op == 2:
            return self[0] == other[0] and \
                   self[1] == other[1] and \
                   self[2] == other[2]

    def __iter__(self):
        self.index = -1
        return self

    def __next__(self):
        if self.index == 2:
            raise StopIteration
        self.index += 1
        return self[self.index]

    #def __setitem__(self, int x, double value):
    #    if x == 0:
    #        self.x = value
    #    elif x == 1:
    #        self.y = value
    #    elif x == 2:
    #        self.z = value

@cython.boundscheck(False)
@cython.wraparound(False)
cpdef Points cneighbouring_translations_points(Py_ssize_t n, cMatrix3D m):
    cdef Points trans
    cdef Point p
    cdef Py_ssize_t i, j, k, ind
    cdef double x,y,z
    cdef cVector3D tmp, ipj, ivv, ijj, ikk, iv, ij, ik
    iv = cVector3D.__new__(cVector3D)
    ij = cVector3D.__new__(cVector3D)
    ik = cVector3D.__new__(cVector3D)
    iv.data[0] = 1
    ij.data[1] = 1
    ik.data[2] = 1
    ind = 0
    trans=Points.__new__(Points)
    p.x = 0
    p.y = 0
    p.z = 0
    trans.vs.push_back(p)
    trans.N = 1
    iv.i_vec_mat_mul(m) 
    ij.i_vec_mat_mul(m) 
    ik.i_vec_mat_mul(m) 
    for i in range(-n,n+1): 
        x = i
        ivv = iv.dmul(x)
        for j in range(-n,n+1): 
            y = j
            ijj = ij.dmul(y)
            ipj = ijj.add(ivv)
            for k in range(-n,n+1): 
                if i == 0 and j == 0 and k == 0:
                    continue
                z = k
                ikk = ik.dmul(z)
                tmp = ipj.add(ikk)
                p.x = tmp.xyz[0]
                p.y = tmp.xyz[1]
                p.z = tmp.xyz[2]
                trans.vs.push_back(p)
                trans.N += 1
                #(<cVector3D> trans[ind]).xyz[0] = x
                #(<cVector3D> trans[ind]).xyz[1] = y
                #(<cVector3D> trans[ind]).xyz[2] = z
                #(<cVector3D> trans[ind]).i_vec_mat_mul(m)
                ind+=1
    return trans


