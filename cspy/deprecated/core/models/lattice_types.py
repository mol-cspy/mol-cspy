"""
Data on lattice types
=====================
"""

num_variable_lengths = {
    "Triclinic": 3,
    "Monoclinic": 3,
    "Orthorhombic": 3,
    "Tetragonal": 2,
    "Trigonal": 1,
    "Hexagonal": 2,
    "Cubic": 1,
}
num_variable_lengths = {1: 3, 2: 3, 3: 3, 4: 2, 5: 1, 6: 2, 7: 1}
num_variable_angles = {
    "Triclinic": 3,
    "Monoclinic": 1,
    "Orthorhombic": 0,
    "Tetragonal": 0,
    "Trigonal": 1,
    "Hexagonal": 0,
    "Cubic": 0,
}
num_variable_angles = {1: 3, 2: 1, 3: 0, 4: 0, 5: 1, 6: 0, 7: 0}
