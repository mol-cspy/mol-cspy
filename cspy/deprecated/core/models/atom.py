"""
The Atom Class
==============

"""
from __future__ import division, absolute_import, print_function
import re
from cspy.deprecated.core.models.element import *
from cspy.deprecated.core.models.vector import CartesianCoordinates
from cspy.deprecated.core.models import Vector3D


class Atom(object):
    """
    An atom object. Initialization

    :param label: atomic label
    :type label: string
    :param xyz: atomic coordinates in cartesian space
    :type xyz: :class:`.CartesianCoordinates`
    :param molecule: molecule that the molecule is associated with
    :type molecule: :class:`Molecule` or None
    :param element: properties of the element associated with the atomic label
    :type element: named tuple
    """

    def __init__(self, label, xyz, molecule=None):
        """
        Initialise an atom
        """
        self.label = label
        if isinstance(xyz, tuple):
            self.xyz = Vector3D(*xyz)
        else:
            self.xyz = xyz
        self._molecule = molecule
        self.__set_element()

    def copy(self):
        return self.__class__(self.label, self.xyz.copy())

    def __reduce__(self):
        return (self.__class__, (self.label, self.xyz.copy()))

    @property
    def fractional_coordinates(self):
        """
        Convert the atomic fractional coordinates to cartesian coordinates
        """
        return self.xyz.vec_mat_mul(self._get_lattice().inv_vectors)
        return CartesianCoordinates(*self.xyz).to_fractional_coords(self._get_lattice())

    def _get_lattice(self):
        """
        Retrieve the crystal lattice information from the molecule class
        """
        return self._molecule._crystal.lattice

    def __set_element(self):
        """
        Set the atoms elemental properties based on the original label
        Returned as a named tuple currently containing:
        :param symbol: atomic symbol
        :type symbol: string
        :param atomic_number: atomic number
        :type atomic_number: int
        :type vdw_radius: the vdw radius
        :param atomic_number: float
        """
        _symbol = "".join(c for c in self.label.strip()[:2] if c.isalpha() is True)
        self.element = element_dict[_symbol]
