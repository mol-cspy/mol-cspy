from __future__ import division, absolute_import, print_function
from collections import namedtuple

VdWRadius = namedtuple("VdWRadius", ["atom", "radius"])

vdw_radii = (
    VdWRadius("H", 1.09),
    VdWRadius("He", 1.40),
    VdWRadius("Li", 1.82),
    VdWRadius("Be", 2.00),
    VdWRadius("B", 2.00),
    VdWRadius("C", 1.70),
    VdWRadius("N", 1.55),
    VdWRadius("O", 1.52),
    VdWRadius("F", 1.47),
    VdWRadius("Ne", 1.54),
    VdWRadius("Na", 2.27),
    VdWRadius("Mg", 1.73),
    VdWRadius("Al", 2.00),
    VdWRadius("Si", 2.10),
    VdWRadius("P", 1.80),
    VdWRadius("S", 1.80),
    VdWRadius("Cl", 2.00),
    VdWRadius("Ar", 1.88),
    VdWRadius("Br", 1.85),
    VdWRadius("Xe", 2.16),
    VdWRadius("I", 1.98),
)
