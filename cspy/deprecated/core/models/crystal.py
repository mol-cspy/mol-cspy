""" Crystals """
from __future__ import division, absolute_import, print_function
from cspy.deprecated.core.models.space_group import StandardSpaceGroup
from cspy.deprecated.core.resources.crystallographic_space_groups import (
    CrystallographicSpaceGroups,
)


class Crystal(object):
    """ The Crystal structure class

    The Crystal has a lattice, an asymmetric_unit and a space_group.  It is a
    central container for CSPy

    :param lattice: The lattice defining the cell dimensions :type lattice:
    :class:.Lattice :param asymmetric_unit: A list of molecules in the
    asymmetric unit (symmetry independant) :type asymmetric_unit: list of
    :class:.Molecule :param space_group: The symmetry of the crystal :type
    space_group: :class:`.SpaceGroup`
    """

    def __init__(self, lattice, asymmetric_unit, space_group):
        self.lattice = lattice

        self.asymmetric_unit = asymmetric_unit

        # auto set up the reverse link
        for molecule in self.asymmetric_unit:
            molecule._crystal = self
        self.lattice._crystal = self

        if isinstance(space_group, int):
            space_group = CrystallographicSpaceGroups.get(space_group)
        self.space_group = space_group

    def copy(self):
        return self.__class__(
            self.lattice.copy(),
            [m.copy() for m in self.asymmetric_unit],
            self.space_group,
        )

    def __reduce__(self):
        if isinstance(self.space_group, StandardSpaceGroup):
            _space_group = self.space_group.index
        else:
            _space_group = self.space_group
        return (self.__class__, (self.lattice, self.asymmetric_unit, _space_group))
