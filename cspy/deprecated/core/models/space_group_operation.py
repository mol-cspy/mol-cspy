from __future__ import division, absolute_import, print_function
from cspy.deprecated.core.models import Vector3D, Matrix3D


class SpaceGroupOperation(object):
    """ Infrastructure for symmetry operations for space groups

    Container controlling the applications of symmetry operations.
    A function is normally given, but in its absence the string will be used to evaluate.

    :param operation_string: string form of the space group operations e.g. "x,y,z+1/2"
    :type operation_string: :class:string
    :param operation_function: function that applies symmetry operation based on 3 inputs
    :type operation_function: None or function
    """

    def __init__(self, operation_string, operation_function=None):
        self.operation_string = operation_string

        if operation_function is None:
            s = prepare_operation(self.operation_string)
            namespace = {"Vector3D": Vector3D}
            exec("operation_function = lambda x,y,z: Vector3D(" + s + ")", namespace)
            operation_function = namespace["operation_function"]
        self.operation_function = operation_function
        self.transF = self.operation_function(0.0, 0.0, 0.0)
        self.F = Matrix3D(
            self.operation_function(1.0, 0.0, 0.0) - self.transF,
            self.operation_function(0.0, 1.0, 0.0) - self.transF,
            self.operation_function(0.0, 0.0, 1.0) - self.transF,
        )

    def __reduce__(self):
        return (self.__class__, (self.operation_string,))

    def apply(self, obj):
        from cspy.deprecated.core.models.convex_hull import Hull

        if isinstance(obj, Hull):
            return self.apply_to_hull(obj)
        else:
            return self.apply_vector3d(obj)

    def apply_vector3d(self, v3d):
        """ Return the new position after operation applied

        :param v3d: input vector
        :type v3d: :class:`.Vector3D` or list or tuple of length 3

        :rtype: :class:`.Vector3D`
        """
        return v3d.vec_mat_mul(self.F) + self.transF

    def apply_to_hull(self, hull):
        """ Return the new position after operation applied

        :param v3d: input vector
        :type v3d: :class:`.Vector3D` or list or tuple of length 3

        :rtype: :class:`.Vector3D`
        """
        for i, vertex in enumerate(hull.vertices):
            hull.vertices[i].v = self.apply(vertex.v)

    def is_identity(self):
        return self.operation_string in ["x,y,z", "x+0,y+0,z+0", "x+0.0,y+0.0,z+0.0"]

    @property
    def original_string_form(self):
        return self.operation_string

    def inversion(self):
        def func(x):
            return "-1*(%s),-1*(%s),-1*(%s)" % tuple(x.split(","))

        return self.__class__(func(self.operation_string))

    def lattice_specific_symmetry_operation(self, lattice):
        invL = lattice.inv_lattice_vectors
        L = lattice.lattice_vectors
        return invL.mat_mat_mul(self.F.mat_mat_mul(L)), self.transF.vec_mat_mul(L)


def prepare_operation(s):
    """ Cleans up a string of a symmetry operation to be used in eval or exec

    :param s: Input string e.g. "x,y,z+1/2"
    :type s: string

    :rtype: string
    """
    tmp = s.replace("1/4", "1.0/4.0")
    tmp = tmp.replace("1/2", "1.0/2.0")
    tmp = tmp.replace("3/4", "3.0/4.0")
    tmp = tmp.replace("1/3", "1.0/3.0")
    tmp = tmp.replace("2/3", "2.0/3.0")
    tmp = tmp.replace("1/6", "1.0/6.0")
    tmp = tmp.replace("5/6", "5.0/6.0")
    return tmp.replace(" ", "").lower()
