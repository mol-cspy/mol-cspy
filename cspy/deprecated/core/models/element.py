from collections import namedtuple

Element = namedtuple("Element", ["symbol", "atomic_number", "vdw_radius"])

vdw_radii = {
    "H": 1.09,
    "He": 1.40,
    "Li": 1.82,
    "Be": 2.00,
    "B": 2.00,
    "C": 1.70,
    "N": 1.55,
    "O": 1.52,
    "F": 1.47,
    "Ne": 1.54,
    "Na": 2.27,
    "Mg": 1.73,
    "Al": 2.00,
    "Si": 2.10,
    "P": 1.80,
    "S": 1.80,
    "Cl": 2.0,
    "Ar": 1.88,
    "Br": 1.85,
    "Xe": 2.16,
    "I": 1.98,
}

atomic_numbers = {
    "H": 1,
    "He": 2,
    "Li": 3,
    "Be": 4,
    "B": 5,
    "C": 6,
    "N": 7,
    "O": 8,
    "F": 9,
    "Ne": 10,
    "Na": 11,
    "Mg": 12,
    "Al": 13,
    "Si": 14,
    "P": 15,
    "S": 16,
    "Cl": 17,
    "Ar": 18,
    "K": 19,
    "Ca": 20,
    "Sc": 21,
    "Ti": 22,
    "V": 23,
    "Cr": 24,
    "Mn": 25,
    "Fe": 26,
    "Co": 27,
    "Ni": 28,
    "Cu": 29,
    "Zn": 30,
    "Br": 35,
    "Xe": 54,
    "I": 53,
}

element_dict = {
    key: Element(
        symbol=key, atomic_number=atomic_numbers[key], vdw_radius=vdw_radii[key]
    )
    for key, v in vdw_radii.items()
}

_max_covalent_by_symbol = {
    ("C", "H"): 1.28,
    ("C", "C"): 1.65,
    ("C", "N"): 1.55,
    ("C", "O"): 1.55,
    ("C", "F"): 1.45,
    ("C", "S"): 1.90,
    ("C", "Cl"): 1.85,
    ("C", "Br"): 1.95,
    ("N", "H"): 1.20,
    ("N", "N"): 1.55,
    ("N", "O"): 1.55,
    ("N", "S"): 1.70,
    ("N", "Hg"): 2.80,
    ("O", "H"): 1.3,
    ("O", "O"): 1.70,
    ("O", "S"): 1.50,
    ("B", "F"): 1.45,
    ("B", "C"): 1.65,
    ("I", "Hg"): 2.80,
    ("Br", "Hg"): 2.50,
    ("S", "S"): 2.5,
    ("H", "H"): 0.85,
    ("C", "I"): 2.2,
    ("S", "Cl"): 1.7,
    ("S", "F"): 1.6,
    ("Cl", "H"): 1.35,
    ("H", "F"): 1.00,
    ("O", "F"): 1.50,
    ("N", "F"): 1.45,
    ("F", "F"): 1.50,
}
for k, v in list(_max_covalent_by_symbol.items()):
    _max_covalent_by_symbol[(k[1], k[0])] = v
