from __future__ import division, absolute_import, print_function
import numpy


class Molecule(object):
    def __init__(self, atoms=None, crystal=None):
        self.name = None
        if atoms is None:
            atoms = []
        self.atoms = atoms
        for atom in self.atoms:
            atom._molecule = self
        self._crystal = crystal

    def copy(self):
        return self.__class__([a.copy() for a in self.atoms])

    def __reduce__(self):
        return (self.__class__, (self.atoms,))

    def as_np_array(self):
        return numpy.array(
            [[atom.xyz.x, atom.xyz.y, atom.xyz.z] for atom in self.atoms]
        )
