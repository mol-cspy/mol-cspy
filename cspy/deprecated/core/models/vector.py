from __future__ import division, absolute_import, print_function
from cspy.deprecated.core.models.cvector3d import cVector3D as Vector3D


class CartesianCoordinates(Vector3D):
    """
    Class that defines an object of Cartesian coordinates.
    """

    def to_fractional_coords(self, lattice):
        """
        Convert the Cartesian coordinates into a fractional coordinates

        :param lattice: The Lattice in which the fractional coordinates will be
        defined.
        :type: :class:`.Lattice`
        :return:  Converted fractional coordinates
        :rtype: :class:`.FractionalCoordinates`
        """
        return self.vec_mat_mul(lattice.inv_lattice_vectors)


class FractionalCoordinates(Vector3D):
    """
    Class that defines an object of fractional coordinates.
    """

    def to_cartesian_coords(self, lattice):
        """
        Convert the fractional coordinates into a Cartesian coordinates

        :param lattice: The Lattice in which the fractional coordinates were
        defined.
        :type: :class:`.Lattice`
        :return:  Converted Carteisan coordinates
        :rtype: :class:`.CartesianCoordinates`
        """
        return self.vec_mat_mul(lattice.lattice_vectors)
