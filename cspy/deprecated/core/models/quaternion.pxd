cdef class Quaternion:
    cdef double data[4]
    cpdef Quaternion conjugation(Quaternion self, Quaternion other)
    cpdef Quaternion inverse(self)
    cpdef rotate_vector(self, vector)
