# from cspy.core.tasks.internal.sobol.sobol_lib import i4_sobol
from cspy.sample.sobol import sobol_vector


class SobolSequence(object):
    def __init__(self, ndim):
        self.ndim = ndim

    def __getitem__(self, index):
        vector = sobol_vector(index, self.ndim)
        # vector, next_seed = i4_sobol(self.ndim, index)
        return vector
