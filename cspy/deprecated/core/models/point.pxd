cdef struct Sphere:
    double x
    double y
    double z
    double radius

cdef struct Point:
    double x
    double y
    double z
