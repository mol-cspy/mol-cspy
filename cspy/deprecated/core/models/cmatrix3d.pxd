cdef class cMatrix3D:
    cdef Py_ssize_t shape[2]
    cdef Py_ssize_t strides[2]
    cdef double data[3][3]
    cdef int index
    cpdef transpose(self)
    cpdef mat_mat_mul(self, n)
