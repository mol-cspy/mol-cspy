from cspy.deprecated.core.models.point cimport Point, Sphere
from libcpp cimport bool
from libcpp.vector cimport vector
cdef class cVector3D:
    cdef Py_ssize_t shape[1]
    cdef Py_ssize_t strides[1]
    cdef double *xyz
    cdef double data[3]
    cdef int index
    cdef bool is_parallel(cVector3D self, cVector3D other, double threshold)
    cpdef cVector3D copy(cVector3D self)
    cpdef void copy_from(cVector3D self, cVector3D other)
    cpdef ineg(cVector3D self)
    cpdef dot(cVector3D self, cVector3D other)
    cdef double cdot(cVector3D self, cVector3D other)
    cdef void i_dmul(cVector3D self, double other)
    cdef cVector3D dmul(cVector3D self, double other)
    cdef cVector3D add(cVector3D self, cVector3D other)
    cpdef cVector3D sub(self, cVector3D other)
    cpdef void i_sub(self, cVector3D other)
    cpdef double magnitude(cVector3D self)
    cpdef cVector3D normalized(cVector3D self)
    cpdef cVector3D i_normalized(cVector3D self)
    cpdef cross(cVector3D self, cVector3D other)
    cpdef cVector3D i_cross(cVector3D self, cVector3D other)
    cpdef rotated(cVector3D self, double angle, cVector3D about)
    cpdef i_rotated(cVector3D self, double angle, cVector3D about)
    cpdef is_collinear(cVector3D self, cVector3D a, cVector3D b)
    cpdef is_in_plane_func(cVector3D self, cVector3D a, cVector3D b, cVector3D c)
    cpdef is_in_plane(cVector3D self, cVector3D a, cVector3D b, cVector3D c)
    cpdef element_mul(cVector3D self, cVector3D other)
    cpdef i_element_mul(cVector3D self, cVector3D other)
    cpdef cVector3D vec_mat_mul(cVector3D self, m)
    cpdef i_vec_mat_mul(cVector3D self, m)
    cpdef bool is_close(cVector3D self, cVector3D other, double threshold)

cdef class cSphere(cVector3D):
    cdef public double radius
#    cpdef cSphere copy(cSphere self)
    
 
cpdef minmax_cVector3D_onto_axis(list vectors, cVector3D axis)
cdef double ccdot(double[::1] a, double[::1] b)

cdef class Points:
    cdef vector[Point] vs
    #cdef Pool mem
    cdef int N
    #cdef Point* vs


cdef class Spheres:
    cdef vector[Sphere] vs
    cdef int N
    cdef SphereProjection projection(self, cVector3D vector)

cdef struct SphereProjection:
     double mini
     double maxi
