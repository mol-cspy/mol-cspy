import cython
from cspy.deprecated.core.models.cvector3d cimport cVector3D
from cspy.deprecated.core.models.cvector3d cimport ccdot
from cspy.deprecated.core.models.quaternion cimport Quaternion
cimport numpy as np

cpdef ConvexHull build_convex_hull(hull):
    cdef ConvexHull ch
    for vertex in hull.vertices:
        ch.vertices.push_back(vertex.v.as_Sphere())
    for face in hull.np_faces:
        ch.faces.push_back(face.as_Point())
    for edge in hull.edges:
        ch.edges.push_back(edge.vec().as_Point())
    ch.centre = hull.centroid.as_Sphere()
    ch.centre.radius = hull.max_r
    return ch

cdef class cHull:
    def __init__(self, double[:,::1] vertices_v):
        self.vertices_v = vertices_v

    def __reduce__(self):
        return (self.__class__, (self.vertices_v, self.faces_norm, self.edges_vec, self.centroid))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dot_list(self, list verts):
        cdef cVector3D vi=cVector3D.__new__(cVector3D), vj=cVector3D.__new__(cVector3D)
        cdef Py_ssize_t i,j,k
        cdef double r
        k = self.vertices_v.shape[0]
        for i in range(k-1):
            for j in range(i+1, k):
                vi = verts[i]
                vj = verts[j]
                #vi.xyz = &(<np.ndarray> self.vertices_v)[i,0]
                #vj.xyz = &(<np.ndarray> self.vertices_v)[j,0]
                r += vi.dot(vj)
        return r

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def dot_array(self, double[:,::1] verts not None):
        cdef cVector3D vi=cVector3D.__new__(cVector3D), vj=cVector3D.__new__(cVector3D)
        #cdef cVector3D vi=cVector3D(0,0,0), vj=cVector3D(0,0,0)
        cdef int i,j,k
        cdef double r
        k = self.vertices_v.shape[0]
        for i in range(k-1):
            #vi.xyz = &verts[i,0]
            for j in range(i+1, k):
                #vj.xyz = &verts[j,0]
                #r += verts[i,0]*verts[j,0]+verts[i,1]*verts[j,1]+verts[i,2]*verts[j,2]
                r += cccdot(&verts[i,0], &verts[j,0])
                ##r += ccdot(verts, verts, i ,j)
                #vi.xyz = &verts[i,0]
                #vj.xyz = &verts[j,0]
                ####vi.xyz = &(<np.ndarray> self.vertices_v)[i,0]
                ####vj.xyz = &(<np.ndarray> self.vertices_v)[j,0]
                #r+=vi.cdot(vj)
        return r
#
#    cpdef translate(self, cVector3D vector):
#        self.centroid += vector
#        for i in range(len(self.vertices_v)):
#            self.vertices_v[i] += vector
#
#    cpdef rotate_by_quaternion(self, Quaternion rotation_quaternion, cVector3D origin): 
#        self.translate(-1*origin) 
#        rotation_quaternion.rotate_vector(self.centroid)
#        for i in range(len(self.vertices_v)):
#            rotation_quaternion.rotate_vector(self.vertices_v[i]) 
#        self.translate(origin) 

@cython.boundscheck(False)
@cython.wraparound(False)
cdef double cccdot(double a[3], double b[3]):
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]
