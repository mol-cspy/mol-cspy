from __future__ import division, absolute_import, print_function
from unittest import TestCase
import mock
import copy
import pickle

from cspy.deprecated.core.models import Atom, Vector3D  # , Matrix3D


class AtomTestCase(TestCase):
    def test_init(self):
        a = Atom("H", Vector3D(0, 0, 0))
        m = mock.Mock()
        a = Atom("H1", Vector3D(0, 0, 0), molecule=m)

    def test_pickle(self):
        m = mock.Mock()
        a = Atom("H1", Vector3D(0, 0, 0), molecule=m)
        a2 = pickle.loads(pickle.dumps(a))
        a3 = copy.deepcopy(a)

    # def test_fractional_coordinates(self):
    #    inv_vec = Matrix3D(Vector3D(1,0,0),Vector3D(0,1,0),Vector3D(0,0,1))
    #    with mock.patch.object(Atom, '_get_inv_vectors', return_value=inv_vec):
    #        a = Atom('H', (0,0,0))
    #        self.assertEqual(a.fractional_coordinates, (0,0,0))
