import unittest
import pickle
import numpy
from cspy.deprecated.core.models import Lattice, Vector3D


class LatticeTestCase(unittest.TestCase):
    def test_init(self):
        Lattice(1, 1, 1, 90, 90, 90)

    def test_lattice_pickle(self):
        l = Lattice(1, 1, 1, 90, 90, 90)
        l2 = pickle.loads(pickle.dumps(l))

    def test_angle_component_of_volume(self):
        l = Lattice(1, 1, 1, 90, 90, 90)
        self.assertEqual(l.angle_component_of_volume, 1.0)

    def test_lattice_vectors(self):
        unit_lv = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
        l = Lattice(1, 1, 1, 90, 90, 90)
        lv = l.lattice_vectors
        numpy.testing.assert_almost_equal(lv, unit_lv)

    def test_inv_lattice_vectors(self):
        unit_ilv = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
        l = Lattice(1, 1, 1, 90, 90, 90)
        ilv = l.inv_lattice_vectors
        numpy.testing.assert_almost_equal(ilv, unit_ilv)

    def test_anisotropic_grid(self):
        grid = Lattice.anisotropic_grid(1, 2, 3)
        self.assertTrue(len(grid), 3 * 5 * 7)
        out_grid = [
            [-1, -2, -3],
            [-1, -2, -2],
            [-1, -2, -1],
            [-1, -2, 0],
            [-1, -2, 1],
            [-1, -2, 2],
            [-1, -2, 3],
            [-1, -1, -3],
            [-1, -1, -2],
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, -1, 1],
            [-1, -1, 2],
            [-1, -1, 3],
            [-1, 0, -3],
            [-1, 0, -2],
            [-1, 0, -1],
            [-1, 0, 0],
            [-1, 0, 1],
            [-1, 0, 2],
            [-1, 0, 3],
            [-1, 1, -3],
            [-1, 1, -2],
            [-1, 1, -1],
            [-1, 1, 0],
            [-1, 1, 1],
            [-1, 1, 2],
            [-1, 1, 3],
            [-1, 2, -3],
            [-1, 2, -2],
            [-1, 2, -1],
            [-1, 2, 0],
            [-1, 2, 1],
            [-1, 2, 2],
            [-1, 2, 3],
            [0, -2, -3],
            [0, -2, -2],
            [0, -2, -1],
            [0, -2, 0],
            [0, -2, 1],
            [0, -2, 2],
            [0, -2, 3],
            [0, -1, -3],
            [0, -1, -2],
            [0, -1, -1],
            [0, -1, 0],
            [0, -1, 1],
            [0, -1, 2],
            [0, -1, 3],
            [0, 0, -3],
            [0, 0, -2],
            [0, 0, -1],
            [0, 0, 0],
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, -3],
            [0, 1, -2],
            [0, 1, -1],
            [0, 1, 0],
            [0, 1, 1],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, -3],
            [0, 2, -2],
            [0, 2, -1],
            [0, 2, 0],
            [0, 2, 1],
            [0, 2, 2],
            [0, 2, 3],
            [1, -2, -3],
            [1, -2, -2],
            [1, -2, -1],
            [1, -2, 0],
            [1, -2, 1],
            [1, -2, 2],
            [1, -2, 3],
            [1, -1, -3],
            [1, -1, -2],
            [1, -1, -1],
            [1, -1, 0],
            [1, -1, 1],
            [1, -1, 2],
            [1, -1, 3],
            [1, 0, -3],
            [1, 0, -2],
            [1, 0, -1],
            [1, 0, 0],
            [1, 0, 1],
            [1, 0, 2],
            [1, 0, 3],
            [1, 1, -3],
            [1, 1, -2],
            [1, 1, -1],
            [1, 1, 0],
            [1, 1, 1],
            [1, 1, 2],
            [1, 1, 3],
            [1, 2, -3],
            [1, 2, -2],
            [1, 2, -1],
            [1, 2, 0],
            [1, 2, 1],
            [1, 2, 2],
            [1, 2, 3],
        ]
        for i in range(len(out_grid)):
            self.assertTrue(grid[i] == Vector3D(*out_grid[i]))

    def test_isotropic_grid(self):
        grid = Lattice.isotropic_grid(1)
        self.assertTrue(len(grid), 3 * 3 * 3)
        out_grid = [
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, -1, 1],
            [-1, 0, -1],
            [-1, 0, 0],
            [-1, 0, 1],
            [-1, 1, -1],
            [-1, 1, 0],
            [-1, 1, 1],
            [0, -1, -1],
            [0, -1, 0],
            [0, -1, 1],
            [0, 0, -1],
            [0, 0, 0],
            [0, 0, 1],
            [0, 1, -1],
            [0, 1, 0],
            [0, 1, 1],
            [1, -1, -1],
            [1, -1, 0],
            [1, -1, 1],
            [1, 0, -1],
            [1, 0, 0],
            [1, 0, 1],
            [1, 1, -1],
            [1, 1, 0],
            [1, 1, 1],
        ]
        for i in range(len(out_grid)):
            self.assertTrue(grid[i] == Vector3D(*out_grid[i]))
