import unittest
import numpy

from cspy.deprecated.core.models import Vector3D, Matrix3D, Lattice
from cspy.deprecated.core.models.space_group_operation import SpaceGroupOperation


class LatticeSpecificOperationTestCase(unittest.TestCase):
    def test_latt_symm_op(self):
        lat = Lattice(2, 3, 4, 90, 90, 90)
        op = SpaceGroupOperation("x,y,z")
        Rmat, Tmat = op.lattice_specific_symmetry_operation(lat)
        numpy.testing.assert_array_almost_equal(Rmat, Matrix3D.identity())
        numpy.testing.assert_array_almost_equal(Tmat, Vector3D(0.0, 0.0, 0.0))

        op = SpaceGroupOperation("x+0.5,y+0.25,z")
        Rmat, Tmat = op.lattice_specific_symmetry_operation(lat)
        numpy.testing.assert_array_almost_equal(Rmat, Matrix3D.identity())
        numpy.testing.assert_array_almost_equal(
            Tmat, Vector3D(2 * 0.5, 3 * 0.25, 4 * 0)
        )

        op = SpaceGroupOperation("-x,-y,-z")
        Rmat, Tmat = op.lattice_specific_symmetry_operation(lat)
        numpy.testing.assert_array_almost_equal(Rmat, Matrix3D.identity(diag=-1))
        numpy.testing.assert_array_almost_equal(Tmat, Vector3D(0.0, 0.0, 0.0))
