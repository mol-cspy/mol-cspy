import unittest

import numpy
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.lattice import Lattice


class LatticeTestCase(unittest.TestCase):
    def test_init(self):
        Lattice(1, 1, 1, 90, 90, 90)

    def test_angle_component_of_volume(self):
        l = Lattice(1, 1, 1, 90, 90, 90)
        self.assertEqual(l.angle_component_of_volume, 1.0)

    def test_lattice_vectors(self):
        unit_lv = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
        l = Lattice(1, 1, 1, 90, 90, 90)
        lv = l.vectors
        numpy.testing.assert_almost_equal(lv, unit_lv)

    def test_inv_lattice_vectors(self):
        unit_ilv = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
        l = Lattice(1, 1, 1, 90, 90, 90)
        ilv = l.inv_vectors
        numpy.testing.assert_almost_equal(ilv, unit_ilv)
