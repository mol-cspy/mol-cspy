import unittest

from cspy.deprecated.core.models.cvector3d import cSphere, cVector3D


class cSphereTestCase(unittest.TestCase):
    def test_init(self):
        s = cSphere(0, 0, 0, 5)
        s.dot(cSphere(1, 2, 3, 4))

    def test_add(self):
        s = cSphere(0, 0, 0, 5)
        t = cVector3D(1, 2, 3)
        self.assertTrue(isinstance(s + t, cSphere))

    def test_copy(self):
        s = cSphere(0, 0, 0, 5)
        s2 = s.copy()
        self.assertTrue(isinstance(s2, cSphere))
