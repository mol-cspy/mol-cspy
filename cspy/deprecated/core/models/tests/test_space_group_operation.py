import unittest

from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.space_group_operation import SpaceGroupOperation


class SpaceGroupOperationTestCase(unittest.TestCase):
    def test_init(self):
        sgo = SpaceGroupOperation("x,y,z")
        r = sgo.apply(Vector3D(1, 2, 3))
        self.assertEqual(r, Vector3D(1, 2, 3))

    def test_operations(self):
        sgo = SpaceGroupOperation("x+0.5,y-1/2,z")
        r = sgo.apply(Vector3D(1, 2, 3))
        self.assertEqual(r, Vector3D(1.5, 1.5, 3))
