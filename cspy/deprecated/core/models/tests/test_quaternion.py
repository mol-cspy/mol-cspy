"""
Quaternion Class Test
======
"""

import unittest
from cspy.deprecated.core.models.quaternion import Quaternion


class QuaternionTestCase(unittest.TestCase):
    def test___mul__(self):
        """
        Quaternion Multiplication
        a=Quaternion(1.0, 0.0, 1.0, 0.0);b=Quaternion(1.0, 0.5, 0.5, 0.75)
        Assert that a*b=Quaternion(0.5, 1.25, 1.5, 0.25)
        """
        result = Quaternion(0.5, 1.25, 1.5, 0.25)
        a = Quaternion(1.0, 0.0, 1.0, 0.0)
        b = Quaternion(1.0, 0.5, 0.5, 0.75)
        self.assertEqual(a * b, result)

    def test_conjugation(self):
        """
        Quaternion Conjugation
        a=Quaternion(1.0, 2.0, 3.0, 4.0);b=Quaternion(4.0, 3.0, 2.0, 1.0)
        Assert that a.conjugation(b)=Quaternion(120.0,-30.0,60.0,90.0)

        """
        result = Quaternion(120.0, -30.0, 60.0, 90.0)
        a = Quaternion(1.0, 2.0, 3.0, 4.0)
        b = Quaternion(4.0, 3.0, 2.0, 1.0)
        self.assertEqual(a.conjugation(b), result)

    def test_inverse(self):
        """
        Quaternion Inverse
        a=Quaternion(1.0,2.0,3.0,4.0)
        assert that a.inverse=Quaternion(1.0,-2.0,-3.0,-4.0)

        """
        result = Quaternion(1.0, -2.0, -3.0, -4.0)
        a = Quaternion(1.0, 2.0, 3.0, 4.0)
        self.assertEqual(a.inverse(), result)

    def test_from_zero_to_one_vector(self):
        """
        Apply Shoemaker equations to map a vector (in range 0..1) to a point on a sphere
        a=(0.5,0.5,0.5)
        assert that Quaternion.from_zero_to_one_vector(a)=Quaternion(8.659560562354934e-17,-2.0**-0.5,8.659560562354934e-17,-2.0**-0.5)
        a=(0.0,0.75,0.75)
        assert that Quaternion.from_zero_to_one_vector(a)=Quaternion(-1.0,-1.8369701987210297e-16,-0.0,-0.0)

        """
        result = Quaternion(
            8.659560562354934e-17, -(2.0 ** -0.5), 8.659560562354934e-17, -(2.0 ** -0.5)
        )
        a = (0.5, 0.5, 0.5)
        self.assertEqual(Quaternion.from_zero_to_one_vector(a), result)
        result = Quaternion(-1.0, -1.8369701987210297e-16, -0.0, -0.0)
        a = (0.0, 0.75, 0.75)
        self.assertEqual(Quaternion.from_zero_to_one_vector(a), result)
