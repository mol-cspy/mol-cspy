import unittest
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.space_group import SpaceGroup
from cspy.deprecated.core.resources.crystallographic_space_groups import (
    CrystallographicSpaceGroups,
)


class SpaceGroupTestCase(unittest.TestCase):
    def test_init(self):
        sg = SpaceGroup(["x,y,z", "-x,-y,-z"])
        unique_points = sg.symmetry_unique_points()
        self.assertEqual(unique_points, [2, 2, 2])

    def test_standard_space_groups(self):
        sg = CrystallographicSpaceGroups.get(1)
        self.assertEqual(len(sg.symmetry), 0)
        self.assertEqual(len(sg.full_symmetry), 1)
        self.assertTrue(sg.full_symmetry[0].is_identity())
        self.assertEqual(sg.crystal_system.index, 1)

        sg = CrystallographicSpaceGroups.get(2)
        self.assertEqual(len(sg.symmetry), 0)
        self.assertEqual(len(sg.non_centering_symmetry), 2)
        self.assertEqual(len(sg.full_symmetry), 2)
        test_v = Vector3D(0.1, 0.2, 0.3)
        self.assertEqual(sg.full_symmetry[1].apply(test_v), -test_v)
        self.assertTrue(sg.full_symmetry[0].is_identity())

        sg = CrystallographicSpaceGroups.get(5)
        self.assertEqual(len(sg.symmetry), 1)
        self.assertEqual(len(sg.non_centering_symmetry), 2)
        self.assertEqual(len(sg.full_symmetry), 4)
        self.assertEqual(
            sg.full_symmetry[0].apply(test_v), test_v.element_mul(Vector3D(1, 1, 1))
        )
        self.assertEqual(
            sg.full_symmetry[1].apply(test_v), test_v.element_mul(Vector3D(-1, 1, -1))
        )
        self.assertEqual(
            sg.full_symmetry[2].apply(test_v),
            test_v.element_mul(Vector3D(1, 1, 1)) + Vector3D(0.5, 0.5, 0.0),
        )
        self.assertEqual(
            sg.full_symmetry[3].apply(test_v),
            test_v.element_mul(Vector3D(-1, 1, -1)) + Vector3D(0.5, 0.5, 0.0),
        )
        self.assertTrue(sg.full_symmetry[0].is_identity())

        sg = CrystallographicSpaceGroups.get(14)
        self.assertEqual(len(sg.symmetry), 1)
        self.assertEqual(len(sg.non_centering_symmetry), 4)
        self.assertEqual(len(sg.full_symmetry), 4)
        self.assertTrue(sg.full_symmetry[0].is_identity())
