import unittest

from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.convex_hull import Hull


class ConvexHullTestCase(unittest.TestCase):
    def test_init(self):
        simple = [
            Vector3D(0, 0, 0),
            Vector3D(0, 0, 1),
            Vector3D(0, 1, 0),
            Vector3D(1, 0, 0),
        ]
        hull = Hull(simple)

    def test_flat_sites(self):
        simple = [Vector3D(0, 0, 0), Vector3D(0, 1, 0), Vector3D(1, 0, 0)]
        with self.assertRaises(Exception):
            hull = Hull(simple)
