import unittest

import math
import numpy
import copy
from cspy.deprecated.core.models import Vector3D, Matrix3D
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import centroid, is_list_planar
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import inertia_tensor, moment_of_inertia
from cspy.deprecated.core.models.cvector3d import cVector3D


class cVector3DMathTestCase(unittest.TestCase):
    def test_deepcopy(self):
        x = cVector3D(1.0, 0.0, 0.0)
        y = cVector3D(0.0, 1.0, 0.0)
        z = copy.deepcopy(x)
        self.assertTrue(x is not z)
        self.assertTrue(x == z)

    def test_rotated(self):
        r = cVector3D(1.0, 0.0, 0.0).i_rotated(
            math.pi * 90.0 / 180.0, cVector3D(0.0, 0.0, 1.0)
        )
        numpy.testing.assert_array_almost_equal(r, cVector3D(0.0, 1.0, 0.0))

    def test_dot(self):
        a = cVector3D(1, 0, 0)
        b = cVector3D(2, 0, 0)
        d = a.dot(b)
        self.assertEqual(d, 2)
        c = cVector3D(-1, 0, 0)
        d = (a - c).dot(b - c)
        self.assertEqual(d, 6)

    def test_cross(self):
        a = cVector3D(1, 0, 0)
        b = cVector3D(2, 0, 0)
        c = a.cross(b)
        self.assertEqual(c, cVector3D(0, 0, 0))

        o = cVector3D(1, -1, 0)
        c = (a - o).cross(b - o)
        self.assertEqual(c, cVector3D(0, 0, -1))

        c = (b - o).cross(a - o)
        self.assertEqual(c, cVector3D(0, 0, +1))

    def test_centroid(self):
        a = [cVector3D(1.0, 0, 0), cVector3D(2.0, 0, 0)]
        self.assertEqual(centroid(a), cVector3D(1.5, 0, 0))

    def test_is_collinear(self):
        a = cVector3D(1.0, 0, 0)
        b = cVector3D(2.0, 0, 0)
        c = cVector3D(3.0, 0, 0)
        self.assertTrue(a.is_collinear(b, c))
        self.assertTrue(a.is_collinear(b, c + cVector3D(0.01, 0, 0)))
        self.assertFalse(a.is_collinear(b, c + cVector3D(0, 0.01, 0)))

    def test_is_planar(self):
        a = cVector3D(1.0, 0, 0)
        b = cVector3D(2.0, 0, 0)
        c = cVector3D(3.0, 1, 0)
        d = cVector3D(4.0, 1, 0)
        self.assertEqual(a.is_in_plane_func(b, c, d), 0)
        self.assertTrue(a.is_in_plane(b, c, d))

        d = cVector3D(4.0, 0, 0)
        self.assertTrue(a.is_in_plane(b, c, d))

        d = cVector3D(4.0, 0, 1)
        self.assertFalse(a.is_in_plane(b, c, d))

    def test_is_list_planar(self):
        a = [
            Vector3D(1.0, 0, 0),
            Vector3D(2.0, 0, 0),
            Vector3D(3.0, 1, 0),
            Vector3D(4.0, 1, 0),
        ]
        self.assertTrue(is_list_planar(a))
        a += [Vector3D(123, 456, 0)]
        self.assertTrue(is_list_planar(a))

        a += [Vector3D(123, 456, 789)]
        self.assertFalse(is_list_planar(a))

    def test_inertia_tensor(self):
        a = [
            Vector3D(1.0, 0, 0),
            Vector3D(2.0, 0, 0),
            Vector3D(3.0, 1.0, 0),
            Vector3D(4.0, 1.0, 0),
        ]
        it = inertia_tensor(a)
        numpy.testing.assert_array_almost_equal(it[0], cVector3D(2.0, -7.0, 0.0))
        numpy.testing.assert_array_almost_equal(it[1], cVector3D(-7.0, 30.0, 0.0))
        numpy.testing.assert_array_almost_equal(it[2], cVector3D(0.0, 0.0, 32.0))

    def test_moment_of_inertia(self):
        a = [
            Vector3D(1.0, 0, 0),
            Vector3D(2.0, 0, 0),
            Vector3D(3.0, 1.0, 0),
            Vector3D(4.0, 1.0, 0),
        ]
        moi = moment_of_inertia(a)
        numpy.testing.assert_array_almost_equal(
            cVector3D(*moi[0]), cVector3D(-0.97324899, -0.22975292, 0.0)
        )
        numpy.testing.assert_array_almost_equal(
            cVector3D(*moi[1]), cVector3D(0.22975292, -0.97324899, 0.0)
        )
        numpy.testing.assert_array_almost_equal(
            cVector3D(*moi[2]), cVector3D(0.0, 0.0, 1.0)
        )

        moi = moment_of_inertia(a, origin=centroid(a))
        numpy.testing.assert_array_almost_equal(
            moi[0], cVector3D(-0.92387953, -0.38268343, 0.0)
        )
        numpy.testing.assert_array_almost_equal(
            moi[1], cVector3D(0.38268343, -0.92387953, 0.0)
        )
        numpy.testing.assert_array_almost_equal(moi[2], cVector3D(0.0, 0.0, 1.0))
