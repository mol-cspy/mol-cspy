cimport numpy as np
from libcpp.vector cimport vector
from cspy.deprecated.core.models.cvector3d cimport cVector3D
from cspy.deprecated.core.models.quaternion cimport Quaternion
from cspy.deprecated.core.models.cvector3d cimport Point, Sphere

cdef struct ConvexHull:
    vector[Sphere] vertices
    vector[Point] faces
    vector[Point] edges
    Sphere centre

cdef class cHull:
    cdef double[:,::1] vertices_v
#    cdef cVector3D centroid
#    cdef list faces_norm
#    cdef list edges_vec
#    cpdef translate(self, cVector3D vector)
#    cpdef rotate_by_quaternion(self, Quaternion rotation_quaternion, cVector3D origin)
