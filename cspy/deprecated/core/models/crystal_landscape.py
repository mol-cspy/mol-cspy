import logging

LOG = logging.getLogger("cspy.deprecated.core.models.crystal_landscape")


class CrystalLandscape(object):
    def __init__(self):
        self.accepted_objs = []
        self.rejected_objs = []

    @property
    def n_accepted(self):
        return len(self.accepted_objs)

    @property
    def n_rejected(self):
        return len(self.rejected_objs)

    @property
    def n_trials(self):
        return len(self.accepted_objs) + len(self.rejected_objs)

    def summary_dict(self):
        rate = 0.0
        if self.n_trials > 0:
            rate = 100.0 * self.n_accepted / self.n_trials
        result = {
            "Results": {
                "Accepted": self.n_accepted,
                "Rejected": self.n_rejected,
                "Success rate": f"{rate:.02f}%",
            }
        }
        return result

    def print_summary(self, printer=LOG.info):
        stats = self.summary_dict()
        for stat, value in stats["Results"].items():
            printer(f"{stat}: {value}")
