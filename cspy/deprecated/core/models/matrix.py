from __future__ import division, absolute_import, print_function

# from collections import namedtuple

from cspy.deprecated.core.models.vector import Vector3D
from cspy.deprecated.core.models.cmatrix3d import cMatrix3D as Matrix3D


# Matrix3D = namedtuple('Matrix3D', ['row0', 'row1', 'row2'])


# Deprecated pure python implementation of 3D Matrix
class Matrix3D(Matrix3D):
    @classmethod
    def identity(cls, diag=1.0):
        return cls(
            Vector3D(diag, 0.0, 0.0), Vector3D(0.0, diag, 0.0), Vector3D(0.0, 0.0, diag)
        )

    @classmethod
    def zeros(cls):
        return cls.identity(diag=0.0)

    def mat_mat_mul(self, n):
        nT = n.transpose()
        A = Vector3D(self[0].dot(nT[0]), self[0].dot(nT[1]), self[0].dot(nT[2]))
        B = Vector3D(self[1].dot(nT[0]), self[1].dot(nT[1]), self[1].dot(nT[2]))
        C = Vector3D(self[2].dot(nT[0]), self[2].dot(nT[1]), self[2].dot(nT[2]))
        return Matrix3D(A, B, C)

    def transpose(self):
        return Matrix3D(
            Vector3D(self[0][0], self[1][0], self[2][0]),
            Vector3D(self[0][1], self[1][1], self[2][1]),
            Vector3D(self[0][2], self[1][2], self[2][2]),
        )
