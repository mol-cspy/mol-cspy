import itertools
from collections import namedtuple

crystal_search_parameters = (
    "positions orientations unit_cell_lengths unit_cell_angles flex_dfs"
)
CrystalSearchDimension = namedtuple("CrystalSearchDimension", crystal_search_parameters)
CrystalSearchPoint = namedtuple(
    "CrystalSearchPoint", crystal_search_parameters + " seed"
)
CrystalSearchPoint.__new__.__defaults__ = (None,)


def make_search_dimensions(crystal):
    dims = {}
    dims["positions"] = 3 * len(crystal.asymmetric_unit)
    dims["orientations"] = 3 * len(
        [m for m in crystal.asymmetric_unit if len(m.atoms) > 1]
    )
    dims["unit_cell_lengths"] = crystal.space_group.crystal_system.num_variable_lengths
    dims["unit_cell_angles"] = crystal.space_group.crystal_system.num_variable_angles
    dims["flex_dfs"] = 0
    return CrystalSearchDimension(**dims)


def uneven_chunker(iterable, chunk_list):
    group_maker = iter(iterable)
    for chunk_size in chunk_list:
        yield list(itertools.islice(group_maker, chunk_size))


class CrystalVectorMapper(object):
    def __init__(self, model, *args, **kwargs):
        self.component_ndim = make_search_dimensions(model)
        self.search_point_classmethod = CrystalSearchPoint

    def map_vector_to_search_point(self, vector):
        args = uneven_chunker(vector, self.component_ndim)
        return self.search_point_classmethod(*args)

    @property
    def ndim(self):
        return sum(self.component_ndim)
