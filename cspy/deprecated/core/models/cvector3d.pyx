#cython: boundcheck=False, initializedcheck=False, wraparound=False, nonecheck=False
import math
from cpython cimport Py_buffer
from libcpp.vector cimport vector
from libcpp cimport bool
from libc.math cimport fabs as c_fabs
from libc.math cimport sin as c_sin
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

from libc.string cimport memset
cimport cython

cdef class cVector3D:
    def __cinit__(self):
        self.xyz = self.data
        self.index = -1

    def __init__(self, double x, double y, double z):
        self.xyz[0] = x
        self.xyz[1] = y
        self.xyz[2] = z


    def __reduce__(self):
        return (self.__class__, (self.x, self.y, self.z))

    property x:
       def __get__(self):
           return self.xyz[0]
       def __set__(self, double value):
           self.xyz[0] = value

    property y:
       def __get__(self):
           return self.xyz[1]
       def __set__(self, double value):
           self.xyz[1] = value

    property z:
       def __get__(self):
           return self.xyz[2]
       def __set__(self, double value):
           self.xyz[2] = value

    def __len__(self):
        return 3

    def __str__(self):
        return str(self.x)+" "+str(self.y)+" "+str(self.z)

    def __getitem__(self, x):
        if x == 0:
            return self.x
        elif x == 1:
            return self.y
        elif x == 2:
            return self.z
        elif isinstance(x, slice):
            return [self[a] for a in range(*x.indices(len(self)))]

    def __iter__(self):
        self.index = -1
        return self

    def __next__(self):
        if self.index == 2:
            raise StopIteration
        self.index += 1
        return self[self.index]

    def __setitem__(self, int x, double value):
        if x == 0:
            self.x = value
        elif x == 1:
            self.y = value
        elif x == 2:
            self.z = value

    def __str__(cVector3D self):
        return '{} {} {}'.format(*self)

    def __repr__(cVector3D self):
        return '<cVector3D x={} y={} z={}>'.format(*self)

    cpdef cVector3D copy(cVector3D self):
        cdef cVector3D r
        r = cVector3D.__new__(cVector3D)
        r.xyz[0] = self.xyz[0]
        r.xyz[1] = self.xyz[1]
        r.xyz[2] = self.xyz[2]
        return r

    cpdef void copy_from(cVector3D self, cVector3D other):
        self.xyz[0] = other.xyz[0]
        self.xyz[1] = other.xyz[1]
        self.xyz[2] = other.xyz[2]

    # Useful functions
    cdef cVector3D add(cVector3D self, cVector3D other):
        cdef cVector3D r
        r = self.copy()
        r += other
        return r

    def __add__(cVector3D self, cVector3D other):
        cdef cVector3D r
        r = self.copy()
        r += other
        return r

    def __iadd__(cVector3D self, cVector3D other):
        self.xyz[0] += other.xyz[0]
        self.xyz[1] += other.xyz[1]
        self.xyz[2] += other.xyz[2]
        return self

    cpdef cVector3D sub(self, cVector3D other):
        cdef cVector3D r
        r = self.copy()
        r.i_sub(other)
        return r

    cpdef void i_sub(self, cVector3D other):
        self.xyz[0] -= other.xyz[0]
        self.xyz[1] -= other.xyz[1]
        self.xyz[2] -= other.xyz[2]

    def __sub__(cVector3D self, cVector3D other):
        cdef cVector3D r
        r = self.copy()
        r -= other
        return r

    def __isub__(cVector3D self, cVector3D other):
        self.xyz[0] -= other.xyz[0]
        self.xyz[1] -= other.xyz[1]
        self.xyz[2] -= other.xyz[2]
        return self

    def __mul__(self, other):
        cdef cVector3D r
        if isinstance(self, float) or isinstance(self, int):
            r = other.copy()
            r *= self
        else:
            r = self.copy()
            r *= other
        return r

    def __rmul__(self, other):
        return self.__mul__(other)

    cdef void i_dmul(cVector3D self, double other):
        self.xyz[0] *= other
        self.xyz[1] *= other
        self.xyz[2] *= other
        
    cdef cVector3D dmul(cVector3D self, double other):
        cdef cVector3D r
        r = self.copy()
        r.xyz[0] *= other
        r.xyz[1] *= other
        r.xyz[2] *= other
        return r

    cpdef cVector3D vec_mat_mul(cVector3D self, m):
        cdef cVector3D r
        r = self.copy()
        r.i_vec_mat_mul(m)
        return r

    cpdef i_vec_mat_mul(cVector3D self, m):
        cdef double x,y,z
        x = self.xyz[0]*m[0][0]+self.xyz[1]*m[1][0]+self.xyz[2]*m[2][0]
        y = self.xyz[0]*m[0][1]+self.xyz[1]*m[1][1]+self.xyz[2]*m[2][1]
        z = self.xyz[0]*m[0][2]+self.xyz[1]*m[1][2]+self.xyz[2]*m[2][2]
        self.xyz[0] = x
        self.xyz[1] = y
        self.xyz[2] = z
        #return self


    #def __mul__(cVector3D self, double other):
    #    cdef cVector3D r
    #    r = self.copy()
    #    r *= other
    #    return r

    def __imul__(cVector3D self, double other):
        self.xyz[0] *= other
        self.xyz[1] *= other
        self.xyz[2] *= other
        return self

    def __div__(cVector3D self, double other):
        cdef cVector3D r
        r = self.copy()
        r /= other
        return r

    def __idiv__(cVector3D self, double other):
        self.xyz[0] /= other
        self.xyz[1] /= other
        self.xyz[2] /= other
        return self

    def __neg__(cVector3D self):
        cdef cVector3D r
        r = self.copy()
        r.ineg() 
        return r

    cpdef ineg(cVector3D self):
        self.xyz[0] = -self.xyz[0]
        self.xyz[1] = -self.xyz[1]
        self.xyz[2] = -self.xyz[2]
        return self

    def __pos__(cVector3D self):
        return self.copy()

    def __richcmp__(cVector3D self, cVector3D other, int op):
        if op == 2:
            return self.x == self.x and \
                   self.y == other.y and \
                   self.z == other.z
        else:
            return NotImplemented

    cpdef dot(cVector3D self, cVector3D other):
        return self.xyz[0]*other.xyz[0]+self.xyz[1]*other.xyz[1]+self.xyz[2]*other.xyz[2]

    cdef double cdot(cVector3D self, cVector3D other):
        return self.xyz[0]*other.xyz[0]+self.xyz[1]*other.xyz[1]+self.xyz[2]*other.xyz[2]


    cpdef double magnitude(cVector3D self):
        cdef double v
        v = (self.xyz[0]**2+self.xyz[1]**2+self.xyz[2]**2)**0.5
        return v

    cpdef cVector3D normalized(cVector3D self):
        cdef cVector3D r
        r = self.copy()
        r.i_normalized()
        return r

    cpdef cVector3D i_normalized(cVector3D self):
        cdef double m = (self.xyz[0]**2+self.xyz[1]**2+self.xyz[2]**2)**0.5
        self.xyz[0] /= m
        self.xyz[1] /= m
        self.xyz[2] /= m
        return self

    cpdef cross(cVector3D self, cVector3D other):
        cdef cVector3D r
        r = self.copy()
        r.i_cross(other)
        return r

    cpdef cVector3D i_cross(cVector3D self, cVector3D other):
        cdef double x,y,z
        x = self.xyz[1]*other.xyz[2] - self.xyz[2]*other.xyz[1]
        y = self.xyz[2]*other.xyz[0] - self.xyz[0]*other.xyz[2]
        z = self.xyz[0]*other.xyz[1] - self.xyz[1]*other.xyz[0]
        self.xyz[0] = x
        self.xyz[1] = y
        self.xyz[2] = z
        return self

    cpdef rotated(cVector3D self, double angle, cVector3D about):
        cdef cVector3D r
        r = self.copy()
        r.i_rotated(angle, about)
        return r

    cpdef i_rotated(cVector3D self, double angle, cVector3D about):
        ''' Return a new Vector3D when vector3d_in is rotated about vector3d_about by angle
    
        This is an optimised version of the code. Generated by Mathematica.
    
        :param vector3d_in: Input vector that should be rotated
        :type vector3d_in: :class:`.Vector3D` or list or tuple of length 3
        :param angle: Angle to rotate by in radians
        :type angle: float
        :param vector3d_about: Vector about which the rotation occurs
        :type vector3d_about: :class:`.Vector3D` or list or tuple of length 3
        :return: Returns a vector in new direction
        :rtype: :class:`.Vector3D`
        '''
        cdef double t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,t30
        try:
            t1=about.xyz[0]
            t2=t1*t1
            t3=about.xyz[1]
            t4=t3*t3
            t5=about.xyz[2]
            t6=t5*t5
            t7=t2+t4+t6
            t8=2.5e-1/t7
            t12=5.e-1*angle
            t13=c_sin(t12)
            t14=t13*t13
            t15=1.0/t14
            t9=4.*t14
            t10=c_sin(angle)
            t16=self.xyz[0]
            t19=self.xyz[1]
            t21=self.xyz[2]
            t18=t7**5.e-1
            t30=t19*t3
            self.xyz[0]=(t10*(t10*t7*t15*t16-4.*t18*(-t21*t3+t19*t5))+t9*(t16*t2+2.e0*t1*(t30+t21*t5)-t16*(t4+t6)))*t8
            self.xyz[1]=(t9*(-t19*t2+2.e0*t1*t16*t3+t19*(t4-t6)+2.*t21*t3*t5)+t10*(t10*t7*t15*t19+4.e0*t18*(-t1*t21+t16*t5)))*t8
            self.xyz[2]=(t10*(t10*t7*t15*t21-4.*t18*(-t1*t19+t16*t3))+t9*(-t21*(t2+t4-t6)+2.e0*(t1*t16+t30)*t5))*t8
        except ZeroDivisionError:
            pass
        return self

    cpdef is_collinear(cVector3D self, cVector3D a, cVector3D b):
        cdef cVector3D cp
        cp = (self-b).cross(a-b)
        return cp == cVector3D(0.,0.,0.)

    cpdef is_in_plane_func(cVector3D self, cVector3D a, cVector3D b, cVector3D c):
        cdef double dc
        dc = (self-c).dot((a-c).i_cross(b-c))
        return dc

    cpdef is_in_plane(cVector3D self, cVector3D a, cVector3D b, cVector3D c):
        return self.is_in_plane_func(a, b, c) == 0.

    cpdef element_mul(cVector3D self, cVector3D other):
        r = self.copy()
        r.i_element_mul(other)
        return r

    cpdef i_element_mul(cVector3D self, cVector3D other):
        self.xyz[0] *= other.xyz[0]
        self.xyz[1] *= other.xyz[1]
        self.xyz[2] *= other.xyz[2]

    cdef bool is_parallel(cVector3D self, cVector3D other, double threshold):
        cdef double d
        d = self.xyz[0]*other.xyz[0]+self.xyz[1]*other.xyz[1]+self.xyz[2]*other.xyz[2]
        return c_fabs(d) >= threshold

    cpdef bool is_close(cVector3D self, cVector3D other, double threshold):
        cdef double d
        d = (<cVector3D> (<cVector3D> self) - (<cVector3D> other)).magnitude()
        return d < threshold

    def as_Point(self):
        cdef Point p
        p.x = self.xyz[0]
        p.y = self.xyz[1]
        p.z = self.xyz[2]
        return p 

    @classmethod
    def from_Point(cls, Point p):
        cdef cVector3D obj
        obj = cVector3D.__new__(cVector3D) 
        obj.read_Point(p)
        return obj

    def read_Point(self, Point p):
        self.xyz[0] = p.x
        self.xyz[1] = p.y
        self.xyz[2] = p.z

    def as_Sphere(self):
        cdef Sphere s
        s.x = self.xyz[0]
        s.y = self.xyz[1]
        s.z = self.xyz[2]
        s.radius = 0
        return s


#cpdef sub(vector[double] a, vector[double] b):
#    return (a[0]-b[0], a[1]-b[1], a[2]-b[2])

ORIGIN = (0.,0.,0.)

cpdef dot(a, b, origin=ORIGIN):
    return (a[0]-origin[0])*(b[0]-origin[0]) + \
           (a[1]-origin[1])*(b[1]-origin[1]) + \
           (a[2]-origin[2])*(b[2]-origin[2]) 

cpdef magnitude(a, origin=ORIGIN):
    return ((a[0]-origin[0])*(a[0]-origin[0]) + \
            (a[1]-origin[1])*(a[1]-origin[1]) + \
            (a[2]-origin[2])*(a[2]-origin[2]))**0.5

cpdef normalize(a, origin=ORIGIN):
    m = magnitude(a, origin=origin)
    return (a[0]/m, a[1]/m, a[2]/m)


def reverse_enum(L):
   for index in reversed(range(len(L))):
      yield index, L[index]

cpdef minmax_cVector3D_onto_axis_iter(iter_vectors, cVector3D axis):
    cdef double mini, maxi, proj
    cdef cVector3D v
    #mini = maxi = proj = vectors[0].dot(axis)
    #for v in vectors:
    #    proj = v.dot(axis)
    #    maxi = max(maxi, proj)
    #    mini = min(mini, proj)
    #return mini, maxi
    v = next(iter_vectors)
    mini = maxi = proj = v.dot(axis)
    for v in iter_vectors:
        proj = v.dot(axis)
        maxi = max(maxi, proj)
        mini = min(mini, proj)
    return mini, maxi


cpdef minmax_cVector3D_onto_axis(list vectors, cVector3D axis):
    cdef double mini, maxi, proj
    cdef cSphere v
    mini = 9e9
    maxi = -9e9
    for v in vectors:
        proj = v.cdot(axis)
        mini = min(mini, proj-v.radius)
        maxi = max(maxi, proj+v.radius)
    return mini, maxi

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef non_parallel_vectors_two(list vectors1, list vectors2, double dot_threshold=0.99, int start=0):
    cdef cVector3D vj, vi
    cdef vector[bool] duplicate
    cdef double idotj
    cdef Py_ssize_t i=0,j=0
    cdef Py_ssize_t len_vecs
    cdef ndot_threshold = -dot_threshold
    i = start
    len_vecs1 = len(vectors1)
    len_vecs2 = len(vectors2)
    duplicate.reserve(len_vecs2)
    for j in range(len_vecs2):
        duplicate[j] = False
    for i in range(len_vecs1):
        #while j < len_vecs2:
        for j in range(len_vecs2):
            if duplicate[j]:
                continue
            idotj = (<cVector3D> vectors1[i]).cdot((<cVector3D> vectors2[j]))
            if idotj >= dot_threshold or idotj <= ndot_threshold:
                duplicate[j] = True
    return [vectors2[j] for j in range(len_vecs2) if not duplicate[j]]


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef non_parallel_vectors(list vectors, double dot_threshold=0.99, int start=0):
    cdef cVector3D vj, vi
    cdef double idotj
    cdef Py_ssize_t i=0,j=0
    cdef Py_ssize_t len_vecs
    cdef ndot_threshold = -dot_threshold
    i = start
    len_vecs = len(vectors)
    while i < len_vecs :
        #vi = vectors[i]
        j=i+1
        while j < len_vecs :
            #vj = vectors[j]
            idotj = (<cVector3D> vectors[i]).dot((<cVector3D> vectors[j]))
            if idotj >= dot_threshold or idotj <= ndot_threshold:
                del vectors[j]
                len_vecs -= 1
            else:
                j+=1
        i+=1

    return vectors[start:]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef cross_lists(list vecs1, list vecs2):
    cdef Py_ssize_t i, j, l1, l2
    cdef list crosses
    l1 = len(vecs1)
    l2 = len(vecs2)
    crosses = []
    for i in range(l1):
        for j in range(l2):
            try:
                crosses.append((<cVector3D> vecs1[i]).cross((<cVector3D> vecs2[j])).normalized())
            except ZeroDivisionError:
                pass
    return crosses

cpdef cVector3D cvec_mat_mul(cVector3D v, m):
    return cVector3D(v[0]*m[0][0]+v[1]*m[1][0]+v[2]*m[2][0],
                     v[0]*m[0][1]+v[1]*m[1][1]+v[2]*m[2][1],
                     v[0]*m[0][2]+v[1]*m[1][2]+v[2]*m[2][2])

cpdef ci_vec_mat_mul(self, m):
    x = self.xyz[0]*m[0][0]+self.xyz[1]*m[1][0]+self.xyz[2]*m[2][0]
    y = self.xyz[0]*m[0][1]+self.xyz[1]*m[1][1]+self.xyz[2]*m[2][1]
    z = self.xyz[0]*m[0][2]+self.xyz[1]*m[1][2]+self.xyz[2]*m[2][2]
    self.xyz[0] = x
    self.xyz[1] = y
    self.xyz[2] = z
    return self


cdef class cSphere(cVector3D):
    def __init__(self, double x, double y, double z, double r):
        self.xyz[0] = x
        self.xyz[1] = y
        self.xyz[2] = z
        self.radius = r

    def __str__(cSphere self):
        return '{} {} {} {}'.format(self.x, self.y, self.z, self.radius)

    def __repr__(cSphere self):
        return '<cSphere x={} y={} z={} radius={}>'.format(self.x, self.y, self.z, self.radius)

    def __reduce__(self):
        return (self.__class__, (self.x, self.y, self.z, self.radius))

    cpdef cSphere copy(cSphere self):
        return cSphere(self.xyz[0], self.xyz[1], self.xyz[2], self.radius)

    def as_Sphere(self):
        cdef Sphere s
        s.x = self.xyz[0]
        s.y = self.xyz[1]
        s.z = self.xyz[2]
        s.radius = self.radius
        return s

cpdef list cneighbouring_translations(Py_ssize_t n, m):
    cdef list trans
    cdef Py_ssize_t i, j, k, ind
    cdef double x,y,z
    cdef cVector3D tmp, ipj, ivv, ijj, ikk, iv=cVector3D(1,0,0), ij=cVector3D(0,1,0), ik=cVector3D(0,0,1)
    ind = 0
    trans=[cVector3D(0,0,0) for i in range((2*n+1)**3)]
    iv.i_vec_mat_mul(m) 
    ij.i_vec_mat_mul(m) 
    ik.i_vec_mat_mul(m) 
    for i in range(-n,n+1): 
        x = i
        ivv = iv*x
        for j in range(-n,n+1): 
            y = j
            ijj = ij*y
            ipj = ijj+ivv
            for k in range(-n,n+1): 
                z = k
                ikk = ik*z
                tmp = ipj+ikk
                trans[ind] =  tmp
                #(<cVector3D> trans[ind]).xyz[0] = x
                #(<cVector3D> trans[ind]).xyz[1] = y
                #(<cVector3D> trans[ind]).xyz[2] = z
                #(<cVector3D> trans[ind]).i_vec_mat_mul(m)
                ind+=1
    return trans

@cython.boundscheck(False)
@cython.wraparound(False)
cdef double ccdot(double[::1] a, double[::1] b):
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]


cdef class Spheres:
    def __init__(self, spheres=None):
        if spheres is None:
           self.N = 0
           return
        self.N = len(spheres)
        cdef Sphere s
        #self.vs = <Point*>self.mem.alloc(self.N, sizeof(Point))

        for i in range(self.N):
            s.x = spheres[i].x
            s.y = spheres[i].y
            s.z = spheres[i].z
            s.radius = spheres[i].radius
            self.vs.push_back(s)

    cdef SphereProjection projection(self, cVector3D vector):
        cdef SphereProjection proj
        cdef double d
        proj.mini = 9.e9
        proj.maxi = -9.e9
        for i in range(self.N):
            d = self.vs[i].x*vector.xyz[0] + \
                self.vs[i].y*vector.xyz[1] + \
                self.vs[i].z*vector.xyz[2]
            proj.mini = min(proj.mini, d-self.vs[i].radius)
            proj.maxi = max(proj.maxi, d+self.vs[i].radius)
        return proj

cdef class Points:
    def __init__(self, points=None):
        if points is None:
           self.N = 0
           return
        self.N = len(points)
        cdef Point p

        for i in range(self.N):
            p.x = points[i].x
            p.y = points[i].y
            p.z = points[i].z
            self.vs.push_back(p)

