from __future__ import division, absolute_import, print_function

# flake8: noqa
from .space_group_operation import SpaceGroupOperation as SymmetryOperation

# class SymmetryOperation(object):
#    def __init__(self, sym_op_string):
#        self.original_string_form = sym_op_string
