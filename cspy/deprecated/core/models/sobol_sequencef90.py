from cspy.deprecated.core.models.sobolf90 import i4_sobol
import numpy


class SobolSequence(object):
    def __init__(self, ndim):
        self.ndim = ndim

    def __getitem__(self, index):
        vector = numpy.asfortranarray(numpy.zeros(self.ndim), dtype=numpy.float32)
        vector, next_seed = i4_sobol(index, vector, dim_num=self.ndim)
        vector = numpy.ascontiguousarray(vector, dtype=numpy.float32)
        return vector
