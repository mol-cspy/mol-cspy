from libc.math cimport sin, cos
from math import pi
from cspy.deprecated.core.models.cvector3d cimport cVector3D
from cspy.deprecated.core.models.matrix import Matrix3D

cdef class Lattice:
    cdef double parameters[6], _vectors[3][3], _inv_vectors[3][3]
    cdef bint parameters_locked, vectors_set, inv_vectors_set
    cdef public object _crystal
    cdef object vecs, ivecs
    def __init__(self, a, b, c, alpha, beta, gamma, crystal=None):
        self.a = a 
        self.b = b 
        self.c = c 
        self.alpha = alpha 
        self.beta = beta
        self.gamma = gamma
        self._crystal = crystal
        self.vectors_set = False
        self.inv_vectors_set = False
        self.vecs = False
        self.ivecs = False

    cpdef copy(self):
        return self.__class__(self.a, self.b, self.c, self.alpha, self.beta, self.gamma)

    def __reduce__(self):
        return (self.__class__, (self.a, self.b, self.c, self.alpha, self.beta, self.gamma))

    cpdef set_by_index(self, i, value):
        self.parameters[i] = value

    property parameter_dict:
        def __get__(self):
            return {'a': self.a,
                    'b': self.b, 
                    'c': self.c, 
                    'alpha': self.alpha, 
                    'beta': self.beta, 
                    'gamma': self.gamma} 

    property lattice_parameters:
        def __get__(self):
            return list(self.parameters)
        def __set__(self, values):
            self.parameters = values

    property a:
        def __get__(self):
            return self.parameters[0]
        def __set__(self, value):
            self.parameters[0] = value
    property b:
        def __get__(self):
            return self.parameters[1]
        def __set__(self, value):
            self.parameters[1] = value
    property c:
        def __get__(self):
            return self.parameters[2]
        def __set__(self, value):
            self.parameters[2] = value
    property alpha:
        def __get__(self):
            return self.parameters[3]
        def __set__(self, value):
            self.parameters[3] = value
    property beta:
        def __get__(self):
            return self.parameters[4]
        def __set__(self, value):
            self.parameters[4] = value
    property gamma:
        def __get__(self):
            return self.parameters[5]
        def __set__(self, value):
            self.parameters[5] = value

    property angles:
        def __get__(self):
            return self.alpha, self.beta, self.gamma
        def __set__(self, angles):
            self.alpha = angles[0]
            self.beta = angles[1]
            self.gamma = angles[2]


    property lengths:
        def __get__(self):
            return self.a, self.b, self.c
        def __set__(self, lengths):
            self.a = lengths[0]
            self.b = lengths[1]
            self.c = lengths[2]

    property volume:
        def __get__(self):
            return self.angle_component_of_volume * self.length_component_of_volume

    property length_component_of_volume:
        def __get__(self):
            return self.a * self.b * self.c

    property angle_component_of_volume:
        def __get__(self):
            d2r = pi / 180.0
            VStar = (1.0 + 2.0 * cos(self.alpha * d2r)
                     * cos(self.beta  * d2r)
                     * cos(self.gamma * d2r)
                     - cos(self.alpha * d2r) ** 2
                     - cos(self.beta  * d2r) ** 2
                     - cos(self.gamma * d2r) ** 2) ** 0.5
            return VStar

    property lattice_vectors:
        def __get__(self):
            return self.vectors

    property vectors:
        def __get__(self):
            
            cdef double d2r, Az, Ay, Ax
            cdef cVector3D A, B, C
            cdef cVector3D tmp
            if self.vecs:
                return self.vecs
            d2r = pi / 180.0 
            C = cVector3D(0.0, 0.0, self.c)
            B = self.b * cVector3D(0.0, 0.0, 1.0).rotated(self.alpha * d2r, cVector3D(-1.0, 0.0, 0.0))
            Az = self.a * cos(self.beta * d2r)
            Ay = self.a * (cos(self.gamma * d2r) - cos(self.beta * d2r) * cos(self.alpha * d2r)) / sin(self.alpha * d2r)
            Ax = self.a * self.angle_component_of_volume / sin(self.alpha * d2r)
            A = cVector3D(Ax, Ay, Az)
            self._vectors[0][0] = A[0]
            self._vectors[0][1] = A[1]
            self._vectors[0][2] = A[2]
            self._vectors[1][0] = B[0]
            self._vectors[1][1] = B[1]
            self._vectors[1][2] = B[2]
            self._vectors[2][0] = C[0]
            self._vectors[2][1] = C[1]
            self._vectors[2][2] = C[2]
            v = Matrix3D(A, B, C)
            if self.parameters_locked:
                self.vecs = v
            return v

    property inv_lattice_vectors:
        def __get__(self):
            return self.inv_vectors

    property inv_vectors:
        def __get__(self):
            if self.ivecs:
                return self.ivecs
            m = self.vectors
            cdef double det = m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) -\
                              m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +\
                              m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0])
            cdef double invdet = 1 / det
            self._inv_vectors[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet
            self._inv_vectors[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet
            self._inv_vectors[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet
            self._inv_vectors[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet
            self._inv_vectors[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet
            self._inv_vectors[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet
            self._inv_vectors[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet
            self._inv_vectors[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet
            self._inv_vectors[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet
            iv = Matrix3D(cVector3D(self._inv_vectors[0][0], self._inv_vectors[0][1], self._inv_vectors[0][2]).copy(),
                    cVector3D(self._inv_vectors[1][0], self._inv_vectors[1][1], self._inv_vectors[1][2]).copy(),
                    cVector3D(self._inv_vectors[2][0], self._inv_vectors[2][1], self._inv_vectors[2][2]).copy(),)
            if self.parameters_locked:
                self.ivecs = iv
            return iv
 
    @staticmethod
    def anisotropic_grid(n_a,n_b,n_c):
        """
        Make a grid of integer-valued lattice point, defining how far to translate (in fractional space)
        along each given lattice vector. This is anisotrpic version which allows different lenghts
        of translations along each direction.

        :param int n_a: How far along the :math:`a` lattice direction to go.
        :param int n_b: How far along the :math:`b` lattice direction to go.
        :param int n_c: How far along the :math:`c` lattice direction to go.
        :return: A nested list of all the translation vectors in fractional sapce.
        """
        return [cVector3D(*[i,j,k]) for i in range(-1*n_a,n_a+1) for j in range(-1*n_b,n_b+1) for k in range(-1*n_c,n_c+1)]

    @staticmethod
    def isotropic_grid(n):
        """
        Make a grid of integer-valued lattice point, defining how far to translate (in fractional space)
        along each given lattice vector. This is the isotrpic version which the same length will be translated
        along each direction

        :param n: How far along each lattice direction to go.
        :return: A nested list of all the translation vectors in fractional sapce.
        """
        return Lattice.anisotropic_grid(n,n,n)

    def parameter_lock(self):
        self.parameters_locked = True
        pass
    def parameter_release(self):
        self.parameters_locked = False
        self.ivecs = False
        self.vecs = False
        pass
