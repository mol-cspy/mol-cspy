'''
Quaternion Class
======
'''

from libc.math cimport sin, cos, atan, asin, atan2, floor
from math import pi
cdef class Quaternion:
    '''
    Quaternion Class
    '''
    def __init__(self, double w, double x, double y, double z):
        self.w = w
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return "<Quaternion w={}, x={}, y={}, z={}>".format(self.w, self.x, self.y, self.z)

    def __richcmp__(self, other, int op):
        if op == 2:
            return self.w == other.w and self.xyz == other.xyz
        else:
            return NotImplemented


    property xyz:
        def __get__(self):
           return (self.data[1], self.data[2], self.data[3])
    property w:
        def __get__(self):
            return self.data[0]
        def __set__(self, value):
            self.data[0] = value
    property x:
        def __get__(self):
            return self.data[1]
        def __set__(self, value):
            self.data[1] = value
    property y:
        def __get__(self):
            return self.data[2]
        def __set__(self, value):
            self.data[2] = value
    property z:
        def __get__(self):
            return self.data[3]
        def __set__(self, value):
            self.data[3] = value

    def __mul__(Quaternion self, Quaternion other):
        '''
        Quaternion Multiplication

        :return: Quaternion 
        :rtype: Quaternion
        '''
        cdef double w, x, y, z
        w = self.data[0]*other.data[0]-self.data[1]*other.data[1]-self.data[2]*other.data[2]-self.data[3]*other.data[3]
        x = self.data[0]*other.data[1]+self.data[1]*other.data[0]+self.data[2]*other.data[3]-self.data[3]*other.data[2]
        y = self.data[0]*other.data[2]+self.data[2]*other.data[0]+self.data[3]*other.data[1]-self.data[1]*other.data[3]
        z = self.data[0]*other.data[3]+self.data[3]*other.data[0]+self.data[1]*other.data[2]-self.data[2]*other.data[1]
        return Quaternion(w, x, y, z)

    cpdef Quaternion conjugation(Quaternion self, Quaternion other):
        '''
        Quaterion conjugation
        a*b*inverse(a)

        :return: Quaternion
        :rtype: Quaterion
        '''
        return self*other*self.inverse()

    cpdef Quaternion inverse(self):
        '''
        Inverse of a Quaternion
        inverse(q,i,j,k)=(q,-i,-j,-k)
        
        :return: Quaternion
        :rtype: Quaternion
        '''
        return Quaternion(self.data[0], -self.data[1], -self.data[2], -self.data[3]) 

    cpdef rotate_vector(self, vector):
        vin_quaternion = Quaternion(0.0, vector[0], vector[1], vector[2])
        xyz = self.conjugation(vin_quaternion).xyz
        vector[0] = xyz[0]
        vector[1] = xyz[1]
        vector[2] = xyz[2]


    @classmethod
    def from_zero_to_one_vector(cls, vector):
        '''
        Map a vector to a point on a sphere using the Shoemaker equation

        Ref: K. Shoemake, Uniform random rotations, In D. Kirk, editor, Graphics Gems III, pages 124-132. Academic, New York, 1992
        Ref: http://planning.cs.uiuc.edu/node198.html    
    
        :return: Quaternion
        :rtype: Quaternion
        '''
        w = (1.0-vector[0])**0.5 * sin(2.0*pi*vector[1])
        x = (1.0-vector[0])**0.5 * cos(2.0*pi*vector[1])
        y = vector[0]**0.5 * sin(2.0*pi*vector[2])
        z = vector[0]**0.5 * cos(2.0*pi*vector[2])
        return cls(w, x, y, z)
        x = (1.0-vector[0])**0.5 * sin(2.0*pi*vector[1])
        y = (1.0-vector[0])**0.5 * cos(2.0*pi*vector[1])
        z = vector[0]**0.5 * sin(2.0*pi*vector[2])
        w = vector[0]**0.5 * cos(2.0*pi*vector[2])
        return cls(x, y, z, w)

    def to_zero_to_one_vector(self):
        v1 = atan2(self.w,self.x)/(pi*2.)
        if abs(self.w) > abs(self.x):
           v0 = 1-(self.w/sin(2*pi*v1))**2
        else:
           v0 = 1-(self.x/cos(2*pi*v1))**2
        v2 = atan2(self.y,self.z)/(pi*2.)
        v2 -= floor(v2)
        v1 -= floor(v1)
        v0 -= floor(v0)
        return v0, v1, v2
