from __future__ import print_function
from cspy.deprecated.core.io.gaussian.jobs import GaussianJob
import logging

gaussian_logger = logging.getLogger("CSPy.gaussian_utils")


def gaussian_add_arguments(parser):
    parser.add_argument(
        "-opt",
        "--perform_geometry_optimisation",
        action="store_true",
        help="Perform a geometry optimisation",
        default=False,
    )
    parser.add_argument(
        "-np",
        "--nprocs",
        type=int,
        help="Number of processors to use in each Gaussian run",
        default=1,
    )
    parser.add_argument(
        "-fun",
        "--functional",
        type=str,
        help="DFT functional to use in each Gaussian run",
        default="B3LYP",
    )
    parser.add_argument(
        "-bas",
        "--basis_set",
        type=str,
        help="Basis set to use in each Gaussian run",
        default="6-31G**",
    )
    parser.add_argument(
        "-basfile",
        "--basis_file",
        type=str,
        help="File contains the basis set information that are not standard in Gaussian",
        default="",
    )
    parser.add_argument(
        "-savechk",
        "--save_check_point",
        action="store_true",
        help="Save Gaussian checkpoint file",
        default=True,
    )
    parser.add_argument(
        "-mem",
        "--memory",
        type=str,
        help="Memory to use in each Gaussian run",
        default="1GB",
    )
    parser.add_argument(
        "--chelpg",
        action="store_true",
        help="Use CHelpG to calculate partial charges ",
        default=False,
    )
    parser.add_argument(
        "--gaussian_cleanup",
        action="store_true",
        help="Clean up gaussian output",
        default=False,
    )
    parser.add_argument(
        "-vol",
        "--molecular_volume",
        action="store_true",
        help="Calculate molecular volume",
        default=False,
    )
    parser.add_argument(
        "-pcm",
        "--polarizable_continuum",
        action="store_true",
        help="Use a Polarizable Continuum Model",
        default=False,
    )
    parser.add_argument(
        "-epcm",
        "--external_iteration_pcm",
        action="store_true",
        help="Use a Polarizable Continuum Model",
        default=False,
    )
    parser.add_argument(
        "-esp",
        "--dielectric_constant",
        type=str,
        help="Dielectric constant for Polarizable Continuum",
        default="3.0",
    )
    parser.add_argument(
        "-opt_opt",
        "--opt_options",
        type=str,
        help="Options within gaussian optimisation",
        default="ModRedundant,",
    )
    parser.add_argument(
        "--freq",
        type=str,
        default=False,
        help="Options for frequency, use a blank to turn on default",
    )
    parser.add_argument(
        "--gaussian_all_molecules",
        action="store_true",
        help="Run Gaussian on all molecules.",
        default=False,
    )
    parser.add_argument(
        "--additional_args",
        type=str,
        help="Additional arguments for gaussian",
        default="",
    )
    sp = parser.add_mutually_exclusive_group()
    sp.add_argument(
        "--force_rerun",
        action="store_true",
        default=True,
        dest="force_rerun",
        help="Force re-running of Gaussian - NOTE: not widely used.",
    )
    sp.add_argument(
        "--no-force_rerun", action="store_false", default=True, dest="force_rerun"
    )


def gaussian_parse_args(args=None):
    import argparse

    gaussian_parser = argparse.ArgumentParser(
        description="Arguments to control the Gaussian execution",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    gaussian_add_arguments(gaussian_parser)
    gaussian_args, unknown = gaussian_parser.parse_known_args(args=args)
    return gaussian_args


def add_gaussian_argument_group(parser):
    gaussian_parser = parser.add_argument_group(
        "Gaussian Arguments", "Arguments to control the Gaussian execution"
    )
    gaussian_add_arguments(gaussian_parser)


def package_gaussian_job(gaussian_args):
    from cspy.deprecated.core.io.gaussian.jobs import GaussianJob

    this_job = GaussianJob()
    gaussian_logger.debug(
        "before setting the job, job model is " + str(this_job.__dict__)
    )
    gaussian_logger.debug(
        "before setting the job, gaussian argument model is " + str(gaussian_args)
    )
    this_job.input_file_name = gaussian_args["input_file_name"]
    this_job.memory = gaussian_args["memory"]
    this_job.nprocs = gaussian_args["nprocs"]
    this_job.functional = gaussian_args["functional"]
    this_job.basis_set = gaussian_args["basis_set"]
    this_job.save_check_point = gaussian_args["save_check_point"]
    this_job.perform_geometry_optimisation = gaussian_args[
        "perform_geometry_optimisation"
    ]
    this_job.geometry_optimisation = gaussian_args["opt_options"]
    this_job.molecular_volume = gaussian_args["molecular_volume"]
    this_job.frequency_calculation = gaussian_args["freq"]
    this_job.normal_mode_calculation = gaussian_args["normal_mode_calculation"]
    this_job.additional_args = gaussian_args["additional_args"]
    this_job.comment = gaussian_args["cmt"]
    gaussian_logger.debug("after setting the job, " + str(this_job.__dict__))
    return this_job


def execute(
    filename, force_rerun, gaussexec
):  # not the correct way to pass in gaussian exec
    import os.path
    from subprocess import Popen
    import shutil

    if force_rerun or not os.path.isfile(str(filename).replace("com", "log")):
        gaussian_logger.info(
            "Execute Gaussian calculation on input file " + str(filename)
        )
        input_file = open(str(filename), "r")
        output_file = open(str(filename).replace("com", "log"), "w+")
        try:
            g09proc = Popen([gaussexec], stdin=input_file, stdout=output_file)
        except OSError as er:
            e = "There was a problem running gaussian executable : "
            e += gaussexec
            gaussian_logger.exception(e)
            exit()
        g09proc.wait()
        input_file.close()
        output_file.close()
        try:
            shutil.move("Test.FChk", str(filename).replace("com", "fchk"))
        except IOError:
            pass
    else:
        ifo = "Skipping gaussian call as output files already present"
        gaussian_logger.info(ifo)


def gaussian_calculation(
    molecule,
    gaussexec=None,
    filename="mol_gas_opt.com",
    gaussian_args={},
    calculate_normal_modes=False,
    force_rerun=False,
    geometry_optimisation=False,
    make_input_only=False,
    mod_redundant=[],
    cmt="Gaussian Calculation",
):
    # Defaults get replaced by anything in gaussian_args
    default = gaussian_parse_args([])

    _gaussian_args = dict(
        default.__dict__.items()
        + {
            "functional": "B3LYP",
            "basis_set": "6-31G(d,p)",
            "nprocs": "1",
            "memory": "1GB",
            "input_file_name": filename,
            "iso": "",
            "chk_filename": filename.split(".")[0],
            "opt_options": "ModRedundant,",
            "molecular_volume": False,
            "freq": False,
            "normal_mode_calculation": calculate_normal_modes,
            "perform_geometry_optimisation": geometry_optimisation,
            "cmt": cmt,
            "additional_args": "",
        }.items()
        + gaussian_args.items()
    )
    _semiempirical = ["AM1"]

    gaussian_job = package_gaussian_job(_gaussian_args)
    gaussian_job.molecule = molecule

    # call Gaussian input file writer to make the input com file
    from cspy.deprecated.core.io.gaussian.gaussian_input_writer import GaussianInputFileWriter
    from cspy.deprecated.core.io.gaussian.gaussian_output_reader import GaussianLogFileReader

    inputFileWriter = GaussianInputFileWriter(gaussian_job)
    inputFileWriter.write_out()
    if make_input_only:
        return gaussian_job

    # Run the calculation - will be refactored out a bit more
    filename = gaussian_job.execution_directory + "/" + gaussian_job.input_file_name
    execute(filename, force_rerun, gaussexec)

    # call output readers to get out information needed
    log_file_reader = GaussianLogFileReader(job=gaussian_job)

    if log_file_reader.get_termination_status():
        # read the content of Gaussian output files, exactly what got read in depends on the options specified by the job
        # and will be handled by the actual reader. See the actual Gaussian readers, should they need to cater for additional
        # options for your specific job, do not dump them in here!!!
        log_file_reader.read_in()
    else:
        raise CSPyException(
            "Error termination in Gaussian calculation, check Gaussian outputs for more info!"
        )
    return gaussian_job
