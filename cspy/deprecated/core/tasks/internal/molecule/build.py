from cspy.deprecated.core.models.molecule import Molecule
from .analysis import connectivity_matrix
from scipy.sparse.csgraph import connected_components, csgraph_from_dense


def fragment_geometry(molecule):
    cmat = connectivity_matrix(molecule)
    graph = csgraph_from_dense(cmat)
    num_mols, atom_id = connected_components(graph)
    molecules = [Molecule() for _ in range(num_mols)]
    for i in range(len(molecule.atoms)):
        molecules[atom_id[i]].atoms.append(molecule.atoms[i].copy())
        molecules[atom_id[i]].atoms[-1]._molecule = molecules[atom_id[i]]
    return molecules
