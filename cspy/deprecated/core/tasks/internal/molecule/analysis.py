from cspy.deprecated.core.models.element import _max_covalent_by_symbol
import cspy.deprecated.core.tasks.internal.vector3d.vector3d as vector3d
import numpy


def is_planar(molecule):
    """
    Method to test if a molecule is planar, i.e. where all atoms in the molecule lies within a plan.

    :param molecule: :class:`.Molecule`
    :rtype: boolean
    """
    sites = [atom.xyz for atom in molecule.atoms]
    return vector3d.is_list_planar(sites)


def is_linear(molecule):
    """
    Method to test if a molecule is linear, i.e. where all atoms in the molecule lies on a line.

    :param molecule: :class:`.Molecule`
    :rtype: boolean
    """
    # TODO - implement this!
    raise NotImplementedError


def centroid(molecule):
    """
    Method that calculate the geometric centre of a given molecule.

    :param molecule: :class:`.Molecule`
    :return: A :class:`.Vector3D` object for the geometric center of the molecule.
    """
    return vector3d.centroid([a.xyz for a in molecule.atoms])


def box_lengths(molecule):
    r"""
    Calculate the lengths of the molecule in three dimension. The i-th length :math:`\ell_{i}`
    is defined based on the extrema (plus or minus a vdW radii of a given atom) of all
    the coordinates of the constituent atoms onto the i-th principle moment of intertia :math:`\mathbf{I}_{i}`
    of this molecule.

    :param molecule: :class:`.Molecule`
    :return: A list of three lengths for the box enclosing the molecule.
    """
    cen = centroid(molecule)
    moi = vector3d.moment_of_inertia([a.xyz for a in molecule.atoms], origin=cen)
    box_lengths = []
    for moi_axis in moi:
        axis = moi_axis.normalized()
        axismax = -float("inf")
        axismin = float("inf")
        for atom in molecule.atoms:
            axis_proj = (atom.xyz - cen).dot(axis)
            axismax = max(axismax, axis_proj + atom.element.vdw_radius)
            axismin = min(axismin, axis_proj - atom.element.vdw_radius)
        box_lengths.append(axismax - axismin)
    return box_lengths


def box_volume(molecule):
    """
    Calculate the volume of the box that encloses the molecule, where the
    lengths of this box are calculated from the :ref:`.box_lengths` method.

    :param molecule: :class:`.Molecule`
    :return: A list of lengths for the box enclosing the molecule.
    :rtype: float
    """
    bl = box_lengths(molecule)
    return bl[0] * bl[1] * bl[2]


def rmsd(moleculeA, moleculeB, return_rotation=False, allow_inversion=False):
    npA = moleculeA.as_np_array()
    npB = moleculeB.as_np_array()
    assert npA.shape == npB.shape
    nAt = npA.shape[0]

    tA = -numpy.mean(npA, axis=0)
    npA += tA

    tB = -numpy.mean(npB, axis=0)
    npB += tB

    mR = numpy.dot(npA.T, npB)
    xx, yy, zz = mR[0, 0], mR[1, 1], mR[2, 2]
    xy, yz, zx = mR[0, 1], mR[1, 2], mR[2, 0]
    xz, yx, zy = mR[0, 2], mR[1, 0], mR[2, 1]
    N = [
        [xx + yy + zz, yz - zy, zx - xz, xy - yx],
        [yz - zy, xx - yy - zz, xy + yx, zx + xz],
        [zx - xz, xy + yx, yy - xx - zz, yz + zy],
        [xy - yx, zx + xz, yz + zy, zz - xx - yy],
    ]
    w, V = numpy.linalg.eig(N)
    max_i = numpy.argmax(w)
    min_i = numpy.argmin(w)

    if -w[min_i] > w[max_i] and allow_inversion:
        m = -w[min_i]
        q = V[:, min_i]
    else:
        m = w[max_i]
        q = V[:, max_i]
    q /= numpy.linalg.norm(q)
    msd = (
        sum([numpy.dot(npA[i, :], npA[i, :]) for i in range(npA.shape[0])])
        + sum([numpy.dot(npB[i, :], npB[i, :]) for i in range(npB.shape[0])])
        - 2 * m
    ) / float(nAt)
    rmsd = abs(msd) ** 0.5
    if return_rotation:
        return rmsd, q
    else:
        return rmsd


def distance_matrix(molecule):
    n = len(molecule.atoms)
    mat = numpy.zeros(shape=(n, n))
    for i in range(n - 1):
        for j in range(i + 1, n):
            d = (molecule.atoms[i].xyz - molecule.atoms[j].xyz).magnitude()
            mat[i][j] = d
            mat[j][i] = d
    return mat


def max_covalent_matrix(molecule):
    n = len(molecule.atoms)
    mat = numpy.zeros(shape=(n, n))
    for i in range(n - 1):
        symI = molecule.atoms[i].element.symbol
        for j in range(i + 1, n):
            symJ = molecule.atoms[j].element.symbol
            d = _max_covalent_by_symbol[(symI, symJ)]
            # try:
            #    d = _max_covalent_by_symbol(symI, symJ)
            # except Exception as exc:
            #    d = (molecule.atoms[i].element.vdw_radius + molecule.atoms[j].element.vdw_radius)
            mat[i][j] = d
            mat[j][i] = d
    return mat


def connectivity_matrix(molecule):
    dm = distance_matrix(molecule)
    mcm = max_covalent_matrix(molecule)

    n = len(molecule.atoms)
    mat = numpy.zeros(shape=(n, n), dtype=int)

    for i in range(n):
        for j in range(n):
            mat[i][j] = int(dm[i][j] < mcm[i][j])

    return mat
