"""
Molecule Transformations
======

"""

from cspy.deprecated.core.tasks.internal.vector3d import vector3d
from cspy.deprecated.core.tasks.internal.molecule.analysis import centroid
from cspy.deprecated.core.models import Vector3D
import math


def translate(mol, vec):
    """
    Translates a molecule along a vector.

    :param mol: molecule to translate
    :type mol: :class:`Molecule`
    :param vec: translation vector
    :type vec: :class:`cspy.core.models.vector.Vector3D`
    """

    for atom in mol.atoms:
        atom.xyz = atom.xyz + vec


def rotate(mol, rot, axis):
    """
    Rotates a molecule by a specified angle about a vector.

    :param mol: molecule to rotate 
    :type mol: :class:`Molecule`
    :param rot: angle of rotation (in radians)
    :type rot: float
    :param axis: vector about which to rotate
    :type axis: :class:`cspy.core.models.vector.Vector3D`
    """

    for atom in mol.atoms:
        atom.xyz = vector3d.rotated(atom.xyz, rot, axis)


def rotate_by_quaternion(molecule, quaternion, origin):
    """
    Rotates a molecule by a quaternion about a point.

    :param mol: molecule to rotate 
    :type mol: :class:`Molecule`
    :param quaternion: the rotation quaternion
    :type quaternion: Quaternion
    :param origin: coordinates
    :type origin: Vector3D

    """
    translate(molecule, -1 * origin)

    for atom in molecule.atoms:
        atom.xyz = vector3d.rotated_by_quaternion(atom.xyz, quaternion)

    translate(molecule, origin)


def align_along_axes_at_origin(molecule):
    # TODO - document this, why there's two blocks of similar looking codes?
    translate(molecule, -centroid(molecule))
    vals, vecs = vector3d.moment_of_inertia_numpy([a.xyz for a in molecule.atoms])
    m = vals.argmax()
    v = Vector3D(*vecs[:, m]).normalized()
    v2 = Vector3D(0, 0, 1)
    ang = math.acos(v.dot(v2))
    if abs(ang) > 1e-6:
        axis = v.cross(v2).normalized()
        rotate(molecule, ang, axis)
    cen = centroid(molecule)
    vals, vecs = vector3d.moment_of_inertia_numpy([a.xyz for a in molecule.atoms])
    m = vals.argmin()
    v = Vector3D(*vecs[m]).normalized()
    v2 = Vector3D(1, 0, 0)
    ang = math.acos(v.dot(v2))
    if abs(ang) > 1e-6:
        axis = v.cross(v2).normalized()
        rotate(molecule, ang, axis)
