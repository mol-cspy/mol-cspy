import unittest
import numpy
from mock import MagicMock

from cspy.deprecated.core.models import Vector3D, Atom, Molecule
from cspy.deprecated.core.models.quaternion import Quaternion

import cspy.deprecated.core.tasks.internal.vector3d.vector3d as vector3d

from cspy.deprecated.core.tasks.internal.molecule.analysis import (
    is_planar,
    is_linear,
    centroid,
    box_lengths,
    box_volume,
    rmsd,
)

from cspy.deprecated.core.resources.crystal_by_refcode import AABHTZ


class MoleculeGeometryAnalysisTestCase(unittest.TestCase):
    def test_box_lengths(self):
        a1 = Atom("C", Vector3D(1, 2, 3))
        a2 = Atom("C", Vector3D(1, 3, 3))
        a3 = Atom("C", Vector3D(1, 3, 4))
        a4 = Atom("C", Vector3D(2, 3, 4))
        m = Molecule([a1, a2, a3, a4])
        bl = box_lengths(m)
        numpy.testing.assert_almost_equal(bl, (5.10710678, 4.10710678, 4.10710678))
        bv = box_volume(m)
        self.assertAlmostEqual(bv, 86.14834267421129)

    def test_check_planar_method_called(self):
        molecule = MagicMock()
        old_is_list_planar = vector3d.is_list_planar
        vector3d.is_list_planar = MagicMock()
        is_planar(molecule)
        vector3d.is_list_planar.assert_called_with([])
        vector3d.is_list_planar = old_is_list_planar

    def test_is_planar(self):
        a1 = Atom("C", Vector3D(1, 0, 0))
        a2 = Atom("C", Vector3D(-1, 0, 0))
        a3 = Atom("C", Vector3D(0, 1, 0))
        a4 = Atom("C", Vector3D(0, -1, 0))
        m = Molecule([a1, a2, a3, a4])
        self.assertTrue(is_planar(m))

    def test_is_not_planar(self):
        a1 = Atom("C", Vector3D(1, 0, 0))
        a2 = Atom("C", Vector3D(-1, 0, 0))
        a3 = Atom("C", Vector3D(0, 1, 1))
        a4 = Atom("C", Vector3D(0, -1, 0))
        m = Molecule([a1, a2, a3, a4])
        self.assertFalse(is_planar(m))

    @unittest.skip("unimplemented test")
    def test_is_linear(self):
        # TODO implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_is_not_linear(self):
        # TODO implement this!
        pass

    def test_centroid(self):
        a1 = Atom("C", Vector3D(1, 0, 0))
        a2 = Atom("C", Vector3D(-1, 0, 0))
        a3 = Atom("C", Vector3D(0, 1, 0))
        a4 = Atom("C", Vector3D(0, -1, 0))
        m = Molecule([a1, a2, a3, a4])
        c = centroid(m)
        self.assertEqual(c, Vector3D(0, 0, 0))

    def test_box_lengths(self):
        a1 = Atom("C", Vector3D(1, 0, 0))
        a2 = Atom("C", Vector3D(-1, 0, 0))
        a3 = Atom("C", Vector3D(0, 1, 0))
        a4 = Atom("C", Vector3D(0, -1, 0))
        a5 = Atom("C", Vector3D(0, 0, 1))
        a6 = Atom("C", Vector3D(0, 0, -1))
        m = Molecule([a1, a2, a3, a4, a5, a6])
        bl = box_lengths(m)
        self.assertEqual(bl, [5.4, 5.4, 5.4])

    def test_box_volume(self):
        a1 = Atom("C", Vector3D(1, 0, 0))
        a2 = Atom("C", Vector3D(-1, 0, 0))
        a3 = Atom("C", Vector3D(0, 1, 0))
        a4 = Atom("C", Vector3D(0, -1, 0))
        a5 = Atom("C", Vector3D(0, 0, 1))
        a6 = Atom("C", Vector3D(0, 0, -1))
        m = Molecule([a1, a2, a3, a4, a5, a6])
        bv = box_volume(m)
        self.assertEqual(bv, 5.4 ** 3)

    def test_rmsd_identity(self):
        m = AABHTZ.asymmetric_unit[0]
        value, q = rmsd(m, m, return_rotation=True)
        self.assertAlmostEqual(value, 0.0, places=6)

    def test_rmsd_translate(self):
        m = AABHTZ.asymmetric_unit[0]
        mi = AABHTZ.asymmetric_unit[0].copy()
        for a in mi.atoms:
            a.xyz.copy_from(a.xyz - Vector3D(5.0, 2.0, 1.0))
        value, q = rmsd(m, mi, return_rotation=True)
        self.assertAlmostEqual(value, 0.0, places=6)

    @unittest.skip("Unimplemented")
    def test_rmsd_rotate(self):
        pass

    @unittest.skip("Inversion not working")
    def test_rmsd_with_inversion(self):
        m = AABHTZ.asymmetric_unit[0]
        mi = AABHTZ.asymmetric_unit[0].copy()
        for a in mi.atoms:
            a.xyz.copy_from(-1 * a.xyz)
        value, qi = rmsd(m, mi, return_rotation=True)
        self.assertAlmostEqual(value, 0.0, places=6)
