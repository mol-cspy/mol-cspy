"""
Transform molecule tests
=================

"""

import unittest
import os
import cspy.deprecated.core.models as models
from math import pi

from cspy.deprecated.core.tasks.internal.molecule.transform import (
    translate,
    rotate,
    rotate_by_quaternion,
)


class TranslateTestCase(unittest.TestCase):
    def test_mol_translate(self):
        """
        Translate test case

        Translate a water molecule by a vector

        """
        atoms = [
            models.Atom("O1", models.Vector3D(0.0, 0.0, 0.0)),
            models.Atom("H1", models.Vector3D(1.0, 0.0, 0.0)),
            models.Atom("H2", models.Vector3D(-0.241922, 0.970296, 0.0)),
        ]
        mol = models.Molecule(atoms=atoms)
        vec = models.Vector3D(0.5, -1.0, 10.0)
        translate(mol, vec)

        correct_pos = [
            [0.5, -1.0, 10.0],
            [1.5, -1.0, 10.0],
            [0.258078, -0.029704, 10.0],
        ]
        for i, atom in enumerate(mol.atoms):
            for j in range(0, 3):
                self.assertAlmostEqual(atom.xyz[j], correct_pos[i][j], 6)


class RotateTestCase(unittest.TestCase):
    def test_mol_rotate_simple(self):
        """
        Rotation test case

        Rotate a water molecule by a vector

        """
        atoms = [
            models.Atom("O1", models.Vector3D(0.0, 0.0, 0.0)),
            models.Atom("H1", models.Vector3D(1.0, 0.0, 0.0)),
            models.Atom("H2", models.Vector3D(-0.241922, 0.970296, 0.0)),
        ]
        mol = models.Molecule(atoms=atoms)
        ang = pi / 2.0
        axis = models.Vector3D(0.0, 0.0, 1.0)
        rotate(mol, ang, axis)

        correct_pos = [[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [-0.970296, -0.241922, 0.0]]
        for i, atom in enumerate(mol.atoms):
            for j in range(0, 3):
                self.assertAlmostEqual(atom.xyz[j], correct_pos[i][j], 6)

    def test_mol_rotate_offaxis(self):
        """
        Rotation test case

        Rotate a water molecule by a vector

        """
        atoms = [
            models.Atom("O1", models.Vector3D(0.0, 0.0, 0.0)),
            models.Atom("H1", models.Vector3D(1.0, 0.0, 0.0)),
            models.Atom("H2", models.Vector3D(-0.241922, 0.970296, 0.0)),
        ]
        mol = models.Molecule(atoms=atoms)
        ang = pi / 3.0
        axis = models.Vector3D(0.5, -1.0, 0.0)
        rotate(mol, ang, axis)

        correct_pos = [
            [0.0, 0.0, 0.0],
            [0.6, -0.2, 0.7745966692414832],
            [-0.33921239999999997, 0.9216508, 0.188402049472929],
        ]
        for i, atom in enumerate(mol.atoms):
            for j in range(0, 3):
                self.assertAlmostEqual(atom.xyz[j], correct_pos[i][j], 6)


class RotateByQuaternionTestCase(unittest.TestCase):
    """
    Rotation test case

    Rotate a water molecule by a vector

    """

    def test_rotate_by_quaternion(self):
        from cspy.deprecated.core.models.quaternion import Quaternion

        atoms = [
            models.Atom("O1", models.Vector3D(0.0, 0.0, 0.0)),
            models.Atom("H1", models.Vector3D(1.0, 0.0, 0.0)),
            models.Atom("H2", models.Vector3D(-0.241922, 0.970296, 0.0)),
        ]
        mol = models.Molecule(atoms=atoms)
        axis = Quaternion(
            0.0, 2.0 ** -0.5, 0.0, -(2.0 ** -0.5)
        )  # rotation around y-axis
        origin = models.Vector3D(0.0, 0.0, 0.0)
        rotate_by_quaternion(mol, axis, origin)

        correct_pos = [[0.0, 0.0, 0.0], [0.0, 0.0, -1.0], [0.0, -0.970296, 0.241922]]

        for i, atom in enumerate(mol.atoms):
            for j in range(0, 3):
                self.assertAlmostEqual(atom.xyz[j], correct_pos[i][j], 6)


class AlignAxisAlongOriginTestCase(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_align(self):
        # TODO - implement this!
        pass
