import copy

from cspy.deprecated.core.models import SpaceGroup, SymmetryOperation, Lattice, Vector3D


def factor_out_lattice_type(space_group):
    """
    Based on the `full` list of symmetry operations in the space group, this function works out the
    type of lattice system that this space group belongs to.

    :param: space_group: The crystal :class:`.SpaceGroup` which contains information of all symmetry operations
    :return: lattice_type: An integer value indicating the type of the crystal, which will be written out
                            under the LATT directive of the res file (1=P, 2=I, 3=rhombohedral obverse on hexagonal axes,
                            4=F, 5=A, 6=B, 7=C, with lattice_type>0 indicating the inclusion of an inversion center)
    :rtype: int
    :return: space_group: The space group with certain symmetry operations removed because of the information
                            redundancy based on lattice type.
    :rtype: :class:`.SpaceGroup`
    """
    space_group, centro_latt_type = factor_out_centrosymmetric_related_operations(
        space_group
    )
    space_group, centering_type = factor_out_translations(space_group)
    return space_group, centering_type * centro_latt_type


def is_centrosymmetric(space_group):
    """
    Based on the `full` list of symmetry operations in the space group, this function works out if there is
    a centre of inversion operation in this space group (i.e. whether this is a centrosymmetric space group).

    This function works in a brute force way, whereby given a random point in fractional space :math:`(x,y,z)`,
    it loops through all the symmetry operations and check if a point :math:`(-x,-y,-z)` will be generated.

    :param: space_group: The crystal :class:`.SpaceGroup` which contains information of all symmetry operations
    :return:  If this space group is centrosymmetric.
    :rtype: bool
    """
    # A random point in fractional space, as long as it does not sit on the special position.
    test_point = Vector3D(0.23424, 0.12348, 0.98566)
    inv_point = Vector3D(-0.23424, -0.12348, -0.98566)
    for sg_ops in space_group.symmetry_operations:
        transformed_point = sg_ops.apply(test_point)
        if __same_point_3D(transformed_point, inv_point):
            return True
    return False


def factor_out_centrosymmetric_related_operations(space_group):
    """
    Based on the `full` list of symmetry operations in the space group, this function remove symmetry operators
    in the space group that can be generated from other symmetry operations by inversion operator.

    This function works in a brute force way, whereby given a random point in fractional space :math:`(x,y,z)`,
    it loops through all the symmetry operations and check if a point :math:`(-(x+t_{x}),-(y+t_{y}),-(z+t_{z}))`,
    with :math:`t_{x,y,z}=[-1,0,1]` will be generated. (This is because, for instance, :math:`(\\bar{x},\\bar{y},\\bar{z}-1/2)`
    and :math:`(\\bar{x},\\bar{y},\\bar{z}+1/2)` differ by one translational element are both the result of
    applying inversion symmetry on :math:`(x,y,z+1/2)`. When found, the operation  :math:`(\\bar{x},\\bar{y},\\bar{z}-1/2)` or
    :math:`(\\bar{x},\\bar{y},\\bar{z}+1/2)`  will be removed, and we set the lattice type to be 1, as specified
    by SHELX convention. Otherwise return -1 for non-centrosymmetric systems.)

    :param space_group: The crystal :class:`.SpaceGroup` which contains information of all symmetry operations
    :return: space_group- The space group with symmetry operations related by inversion symmetry removed.
    :rtype: :class:`.SpaceGroup`
    :return: latt_type- 1 or -1, corresponding to centrosymmetric or non-centrosymmetric lattice type.
    :rtype: int
    """
    test_point = Vector3D(0.23424, 0.12348, 0.98566)
    sg_holder = copy.deepcopy(space_group.symmetry_operations)
    i1, i2 = 0, 0
    related_elements = []
    while i1 < len(sg_holder):
        test_point2 = sg_holder[i1].apply(test_point)
        i2 = i1 + 1
        while i2 < len(sg_holder):
            test_point3 = sg_holder[i2].apply(test_point)
            for nct in Lattice.isotropic_grid(1):
                test_point4 = test_point3 + nct
                if __same_point_3D(test_point2, -1.0 * test_point4):
                    related_elements.append(space_group.symmetry_operations[i2])
                    del sg_holder[i2]
                    continue
            i2 += 1
        i1 += 1

    if len(related_elements) == len(sg_holder):
        latt_type = 1
        space_group = SpaceGroup(sg_holder)
    else:
        latt_type = -1
    return space_group, latt_type


def factor_out_translations(space_group):
    """
    This method finds special translational operations in the space group operations, based on which the
    specific lattice type (LATT in SHELX directive) will be determine. Note, one should always use
    :func:`.factor_out_centrosymmetric_related_operations()` to factor out centrosymmetry element first.

    This function works in a brute force way, whereby given a random point in fractional space :math:`(x,y,z)`,
    two points :math:`(x_{1},y_{1},z_{1})` and :math:`(x_{2},y_{2},z_{2})` generated by operating two different
    symmetry operators on :math:`(x,y,z)` can be related to a special translation operations in a given lattice
    type.

    :param space_group: The crystal :class:`.SpaceGroup` which contains information of all symmetry operations
    :return: space_group- The space group with symmetry operations related by inversion symmetry removed.
    :rtype: :class:`.SpaceGroup`
    :return: latt_type- The lattice type in accordance to the SHELX convention. Note that here we ignore checking
                            the inversion part, and only gives out latt_type as positive number. One should combine
                            the result from :func:`.factor_out_centrosymmetric_related_operations()` to get
                            the proper lattice type.
    :rtype: int
    """
    # special translations in each lattice type
    pure_translations = {
        2: [Vector3D(0.5, 0.5, 0.5)],
        3: [
            Vector3D(2.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0),
            Vector3D(1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0),
        ],
        4: [Vector3D(0.5, 0.5, 0.0), Vector3D(0.5, 0.0, 0.5), Vector3D(0.0, 0.5, 0.5)],
        5: [Vector3D(0.0, 0.5, 0.5)],
        6: [Vector3D(0.5, 0.0, 0.5)],
        7: [Vector3D(0.5, 0.5, 0.0)],
    }

    test_point = Vector3D(0.23424, 0.12348, 0.98566)

    for translation_index in range(6):
        # Must do F before A,B,C as these are elements of {F}
        sg_holder = copy.deepcopy(space_group.symmetry_operations)
        for each_pt in pure_translations[translation_index + 2]:
            i1, i2 = 0, 0
            related_elements = []
            while i1 < len(sg_holder):
                test_point2 = sg_holder[i1].apply(test_point)
                i2 = i1 + 1
                while i2 < len(sg_holder):
                    test_point3 = sg_holder[i2].apply(test_point) + each_pt
                    for nct in Lattice.isotropic_grid(1):
                        test_point4 = test_point3 + nct
                        if __same_point_3D(test_point2, test_point4):
                            related_elements.append(space_group.symmetry_operations[i2])
                            del sg_holder[i2]
                            continue
                    i2 += 1
                i1 += 1

        if len(sg_holder) * (
            len(pure_translations[translation_index + 2]) + 1.0
        ) == len(space_group.symmetry_operations):
            latt_type = translation_index + 2
            space_group.symmetry_operations = sg_holder
            return space_group, latt_type
    # does not belong to anything above
    return space_group, 1


def __same_point_3D(pt1, pt2):
    return (
        __same_value(pt1[0], pt2[0])
        & __same_value(pt1[1], pt2[1])
        & __same_value(pt1[2], pt2[2])
    )


def __same_value(v1, v2):
    return (v1 + 0.001 > v2) and (v1 - 0.001 < v2)
