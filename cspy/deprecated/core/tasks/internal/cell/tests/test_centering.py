import unittest

from cspy.deprecated.core.models import Crystal, Molecule, Atom, Lattice, SpaceGroupOperation
from cspy.deprecated.core.models import SpaceGroup, SymmetryOperation
from cspy.deprecated.core.tasks.internal.cell.lattice_centering import *


class TestCentrosymmetric(unittest.TestCase):
    def test_non_centrosymmetric(self):
        sg = SpaceGroup(
            [SymmetryOperation("x,y,z"), SymmetryOperation("-x,y+1/2,-z+1/2")]
        )
        self.assertFalse(is_centrosymmetric(sg))

    def test_centrosymmetric(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x,1/2-y,-1.2-z"),
            ]
        )
        self.assertTrue(is_centrosymmetric(sg))

    def test_remove_centrosymmetric_related_operations_for_non_centrosymmetric(self):
        sg = SpaceGroup(
            [SymmetryOperation("x,y,z"), SymmetryOperation("-x,y+1/2,-z+1/2")]
        )
        new_sg, latt_type = factor_out_centrosymmetric_related_operations(sg)
        self.assertEqual(len(new_sg.symmetry_operations), 2)
        self.assertEqual(latt_type, -1)

    def test_remove_centrosymmetric_related_operations_for_centrosymmetric(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x,y+1/2,z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("-x,-y+1/2,-z-1/2"),
            ]
        )
        new_sg, latt_type = factor_out_centrosymmetric_related_operations(sg)
        self.assertEqual(len(new_sg.symmetry_operations), 2)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content, ["x,y,z", "x,y+1/2,z+1/2"]
        )
        self.assertEqual(latt_type, 1)


class TestFactoriseOutTranslations(unittest.TestCase):
    def test_factor_out_I(self):
        # using space group #71 for testing
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x+1/2,y+1/2,z+1/2"),
                SymmetryOperation("-x,y,z"),
                SymmetryOperation("-x+1/2,y+1/2,z+1/2"),
                SymmetryOperation("x,-y,z"),
                SymmetryOperation("x+1/2,-y+1/2,z+1/2"),
                SymmetryOperation("x,y,-z"),
                SymmetryOperation("x+1/2,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("-x+1/2,-y+1/2,-z+1/2"),
                SymmetryOperation("x,-y,-z"),
                SymmetryOperation("x+1/2,-y+1/2,-z+1/2"),
                SymmetryOperation("-x,y,-z"),
                SymmetryOperation("-x+1/2,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,z"),
                SymmetryOperation("-x+1/2,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            [
                "x,y,z",
                "-x,y,z",
                "x,-y,z",
                "x,y,-z",
                "-x,-y,-z",
                "x,-y,-z",
                "-x,y,-z",
                "-x,-y,z",
            ],
        )
        self.assertEqual(latt_type, 2)

    def test_factor_out_latt_type_3_for_sg_146_hexagonal_setting(self):
        """
        This method tests the correct setting for space group 146 which is compatiable with lattice type definiton.
        """
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-y,x-y,z"),
                SymmetryOperation("-x+y,-x,-z"),
                SymmetryOperation("x+2/3,y+1/3,z+1/3"),
                SymmetryOperation("-y+2/3,x-y+1/3,z+1/3"),
                SymmetryOperation("-x+y+2/3,-x+1/3,-z+1/3"),
                SymmetryOperation("x+1/3,y+2/3,z+2/3"),
                SymmetryOperation("-y+1/3,x-y+2/3,z+2/3"),
                SymmetryOperation("-x+y+1/3,-x+2/3,-z+2/3"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(latt_type, 3)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content, ["x,y,z", "-y,x-y,z", "-x+y,-x,-z"]
        )

    def test_factor_out_latt_type_4(self):
        # sg 22
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x,y+1/2,z+1/2"),
                SymmetryOperation("x+1/2,y,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("x,-y,-z"),
                SymmetryOperation("x,-y+1/2,-z+1/2"),
                SymmetryOperation("x+1/2,-y,-z+1/2"),
                SymmetryOperation("x+1/2,-y+1/2,-z"),
                SymmetryOperation("-x,y,-z"),
                SymmetryOperation("-x+1/2,y+1/2,-z"),
                SymmetryOperation("-x+1/2,y,-z+1/2"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,z"),
                SymmetryOperation("-x+1/2,-y+1/2,z"),
                SymmetryOperation("-x+1/2,-y,z+1/2"),
                SymmetryOperation("-x,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(latt_type, 4)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "x,-y,-z", "-x,y,-z", "-x,-y,z"],
        )

    def test_factor_out_latt_type_5(self):
        # sg 5 in A121 setting
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y,-z"),
                SymmetryOperation("x,y+1/2,z+1/2"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(latt_type, 5)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y,-z"])

    def test_factor_out_latt_type_6(self):
        # sg 6 in B1m1 setting
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x,-y,z"),
                SymmetryOperation("x+1/2,y,z+1/2"),
                SymmetryOperation("x+1/2,-y,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(latt_type, 6)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "x,-y,z"])

    def test_factor_out_latt_type_7(self):
        # sg 5 in C121 setting
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y,-z"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("-x+1/2,y+1/2,-z"),
            ]
        )
        new_sg, latt_type = factor_out_translations(sg)
        self.assertEqual(latt_type, 7)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y,-z"])


class TestOutFactorOutLatticeType(unittest.TestCase):
    # all tests assumes standard settings, here we tested all common space groups for Z'=1 systems
    def test_sg_1(self):
        sg = SpaceGroup([SymmetryOperation("x,y,z")])
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z"])

    def test_sg_2(self):
        sg = SpaceGroup([SymmetryOperation("x,y,z"), SymmetryOperation("-x,-y,-z")])
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z"])

    def test_sg_4(self):
        sg = SpaceGroup([SymmetryOperation("x,y,z"), SymmetryOperation("-x,y+1/2,-z")])
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y+1/2,-z"])

    def test_sg_5(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y,-z"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("-x+1/2,y+1/2,-z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -7)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y,-z"])

    def test_sg_7(self):
        sg = SpaceGroup([SymmetryOperation("x,y,z"), SymmetryOperation("x,-y,1/2+z")])
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "x,-y,1/2+z"])

    def test_sg_9(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x,-y,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("x+1/2,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -7)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "x,-y,z+1/2"])

    def test_sg_13(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x,-y,1/2+z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y,-z+1/2"])

    def test_sg_14(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y+1/2,-z+1/2"]
        )

    def test_sg_15(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,y,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x,-y,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("-x+1/2,y+1/2,-z+1/2"),
                SymmetryOperation("-x+1/2,-y+1/2,-z"),
                SymmetryOperation("x+1/2,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 7)
        self.assertEqual(new_sg.symmetry_list_for_res_content, ["x,y,z", "-x,y,-z+1/2"])

    def test_sg_18(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x+1/2,-y+1/2,-z"),
                SymmetryOperation("-x+1/2,y+1/2,-z"),
                SymmetryOperation("-x,-y,z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "x+1/2,-y+1/2,-z", "-x+1/2,y+1/2,-z", "-x,-y,z"],
        )

    def test_sg_19(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("x+1/2,y-1/2,-z"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x+1/2,-y,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "x+1/2,y-1/2,-z", "-x,y+1/2,-z+1/2", "-x+1/2,-y,z+1/2"],
        )

    def test_sg_29(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/2,-y,z+1/2"),
                SymmetryOperation("x+1/2,-y,z"),
                SymmetryOperation("-x,-y,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x+1/2,-y,z+1/2", "x+1/2,-y,z", "-x,-y,z+1/2"],
        )

    def test_sg_33(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/2,y+1/2,z+1/2"),
                SymmetryOperation("x+1/2,-y+1/2,z"),
                SymmetryOperation("-x,-y,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x+1/2,y+1/2,z+1/2", "x+1/2,-y+1/2,z", "-x,-y,z+1/2"],
        )

    def test_sg_43(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/4,y+1/4,z+1/4"),
                SymmetryOperation("x+1/4,-y+1/4,z+1/4"),
                SymmetryOperation("-x,-y,z"),
                SymmetryOperation("x,y+1/2,z+1/2"),
                SymmetryOperation("-x+1/4,y+3/4,z+3/4"),
                SymmetryOperation("x+1/4,-y+3/4,z+3/4"),
                SymmetryOperation("-x,-y+1/2,z+1/2"),
                SymmetryOperation("x+1/2,y,z+1/2"),
                SymmetryOperation("-x+3/4,y+1/4,z+3/4"),
                SymmetryOperation("x+3/4,-y+1/4,z+3/4"),
                SymmetryOperation("-x+1/2,-y,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,z"),
                SymmetryOperation("-x+3/4,y+3/4,z+1/4"),
                SymmetryOperation("x+3/4,-y+3/4,z+1/4"),
                SymmetryOperation("-x+1/2,-y+1/2,z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -4)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content, sg.symmetry_list_for_res_content
        )

    def test_sg_56(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/2,y,z+1/2"),
                SymmetryOperation("x,-y+1/2,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,-z"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x+1/2,-y,-z+1/2"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x+1/2,-y+1/2,z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x+1/2,y,z+1/2", "x,-y+1/2,z+1/2", "x+1/2,y+1/2,-z"],
        )

    def test_sg_60(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/2,y+1/2,z"),
                SymmetryOperation("x,-y,z+1/2"),
                SymmetryOperation("x+1/2,y+1/2,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x+1/2,-y+1/2,-z"),
                SymmetryOperation("-x,y,-z+1/2"),
                SymmetryOperation("-x+1/2,-y+1/2,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x+1/2,y+1/2,z", "x,-y,z+1/2", "x+1/2,y+1/2,-z+1/2"],
        )

    def test_sg_61(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x+1/2,y+1/2,z"),
                SymmetryOperation("x,-y+1/2,z+1/2"),
                SymmetryOperation("x+1/2,y,-z+1/2"),
                SymmetryOperation("-x,-y,-z"),
                SymmetryOperation("x+1/2,-y+1/2,-z"),
                SymmetryOperation("-x,y+1/2,-z+1/2"),
                SymmetryOperation("-x+1/2,-y,z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, 1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x+1/2,y+1/2,z", "x,-y+1/2,z+1/2", "x+1/2,y,-z+1/2"],
        )

    def test_sg_76(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,-y,z+1/2"),
                SymmetryOperation("-y,x,z+1/4"),
                SymmetryOperation("y,-x,z+3/4"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-x,-y,z+1/2", "-y,x,z+1/4", "y,-x,z+3/4"],
        )

    def test_sg_88(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,-y,z"),
                SymmetryOperation("-y,x+1/2,z+1/4"),
                SymmetryOperation("y,-x+1/2,z+1/4"),
                SymmetryOperation("-x,-y+1/2,-z+1/4"),
                SymmetryOperation("x,y+1/2,-z+1/4"),
                SymmetryOperation("y,-x,-z"),
                SymmetryOperation("-y,x,-z"),
                SymmetryOperation("x+1/2,y+1/2,z+1/2"),
                SymmetryOperation("-x+1/2,-y+1/2,z+1/2"),
                SymmetryOperation("-y+1/2,x,z+3/4"),
                SymmetryOperation("y+1/2,-x,z+3/4"),
                SymmetryOperation("-x+1/2,-y,-z+3/4"),
                SymmetryOperation("x+1/2,y,-z+3/4"),
                SymmetryOperation("y+1/2,-x+1/2,-z+1/2"),
                SymmetryOperation("-y+1/2,x+1/2,-z+1/2"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -2)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            [
                "x,y,z",
                "-x,-y,z",
                "-y,x+1/2,z+1/4",
                "y,-x+1/2,z+1/4",
                "-x,-y+1/2,-z+1/4",
                "x,y+1/2,-z+1/4",
                "y,-x,-z",
                "-y,x,-z",
            ],
        )

    def test_sg_92(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-x,-y,z+1/2"),
                SymmetryOperation("-y+1/2,x+1/2,z+1/4"),
                SymmetryOperation("y+1/2,-x+1/2,z+3/4"),
                SymmetryOperation("x+1/2,-y+1/2,-z+3/4"),
                SymmetryOperation("-x+1/2,y+1/2,-z+1/4"),
                SymmetryOperation("-y,-x,-z+1/2"),
                SymmetryOperation("y,x,-z"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            [
                "x,y,z",
                "-x,-y,z+1/2",
                "-y+1/2,x+1/2,z+1/4",
                "y+1/2,-x+1/2,z+3/4",
                "x+1/2,-y+1/2,-z+3/4",
                "-x+1/2,y+1/2,-z+1/4",
                "-y,-x,-z+1/2",
                "y,x,-z",
            ],
        )

    def test_sg_144(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-y,x-y,z+1/3"),
                SymmetryOperation("-x+y,-x,z+2/3"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            ["x,y,z", "-y,x-y,z+1/3", "-x+y,-x,z+2/3"],
        )

    def test_sg_146(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-y,x-y,z"),
                SymmetryOperation("-x+y,-x,-z"),
                SymmetryOperation("x+2/3,y+1/3,z+1/3"),
                SymmetryOperation("-y+2/3,x-y+1/3,z+1/3"),
                SymmetryOperation("-x+y+2/3,-x+1/3,-z+1/3"),
                SymmetryOperation("x+1/3,y+2/3,z+2/3"),
                SymmetryOperation("-y+1/3,x-y+2/3,z+2/3"),
                SymmetryOperation("-x+y+1/3,-x+2/3,-z+2/3"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -3)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content, ["x,y,z", "-y,x-y,z", "-x+y,-x,-z"]
        )

    def test_sg_169(self):
        sg = SpaceGroup(
            [
                SymmetryOperation("x,y,z"),
                SymmetryOperation("-y,x-y,z+1/3"),
                SymmetryOperation("-x+y,-x,z+2/3"),
                SymmetryOperation("-x,-y,z+1/2"),
                SymmetryOperation("x-y,x,z+1/6"),
                SymmetryOperation("y,-x+y,z+5/6"),
            ]
        )
        new_sg, latt_type = factor_out_lattice_type(sg)
        self.assertEqual(latt_type, -1)
        self.assertEqual(
            new_sg.symmetry_list_for_res_content,
            [
                "x,y,z",
                "-y,x-y,z+1/3",
                "-x+y,-x,z+2/3",
                "-x,-y,z+1/2",
                "x-y,x,z+1/6",
                "y,-x+y,z+5/6",
            ],
        )
