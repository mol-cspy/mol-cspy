import logging
import math
from collections import namedtuple
from cspy.deprecated.core.models.quaternion import Quaternion
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.crystal_landscape import CrystalLandscape
from cspy.deprecated.core.models.namespace import Namespace
from cspy.deprecated.core.models.search_space import CrystalSearchPoint
from cspy.deprecated.core.models.search_space import CrystalVectorMapper
from cspy.deprecated.core.models.sobol_sequence import SobolSequence
from cspy.deprecated.core.tasks.internal.convex_hull.analysis import OverlappingHulls
from cspy.deprecated.core.tasks.internal.crystal.analysis import box_volume
from cspy.deprecated.core.tasks.internal.crystal.analysis import (
    molecular_projection_on_to_lattice_vectors,
)
from cspy.deprecated.core.tasks.internal.crystal.cell_lengths import CellLengthGenerator
from cspy.deprecated.core.tasks.internal.crystal.sat_expand import CrystalExpansionBySAT
from cspy.deprecated.core.tasks.internal.landscape_generator.acceptance import (
    SearchPointAcceptanceChecker,
)
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import ConvergedLandscape
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import ExhaustedLandscape
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import IncompleteLandscape
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import LandscapeGenerator
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import ObjectAccepted
from cspy.deprecated.core.tasks.internal.landscape_generator.generic import ObjectRejected
from cspy.deprecated.core.tasks.internal.molecule.analysis import centroid
from cspy.deprecated.core.tasks.internal.molecule.analysis import rmsd
from cspy.deprecated.core.tasks.internal.molecule.transform import (
    rotate_by_quaternion,
    translate,
    align_along_axes_at_origin,
)


logger = logging.getLogger("cspy.deprecated.core.tasks.internal.landscape_generator.crystal")


Convergence = namedtuple("Convergence", ["number_valid_structures", "max_trials"])


class GeneratedCrystal:

    def __init__(self):
        self.crystal = None
        self.search_point = None
        self.has_been_made = False
        self.generate_tests_passed = None
        self.overlapping_hulls = False
        self.cell_gen_volume = False
        self.sat_tried = False
        self.target_volume = None
        self.max_volume = None


class CrystalLandscapeGenerator(LandscapeGenerator):

    def __init__(self, model, *args, **kwargs):
        super().__init__(model, *args, **kwargs)

        # crystal landscape specific setup
        for molecule in self.input_crystal.asymmetric_unit:
            align_along_axes_at_origin(molecule)

        if self.object_modifier_kwargs.get("local_target_volume", False):
            self.target_volume = self.input_crystal.lattice.volume
            logger.info("Local target Volume: {}".format(self.target_volume))
        else:
            self.target_volume = box_volume(self.input_crystal)
        self.max_volume = self.target_volume * 2.5
        logger.debug(
            "Target volume {} Max Volume {}".format(self.target_volume, self.max_volume)
        )
        self.sat_engine = CrystalExpansionBySAT(
            self.input_crystal,
            cell_cutoff=3,
            max_volume=self.max_volume,
            max_retries=50,
            use_volume_metric=False,
        )
        self.sat_engine._construct_hulls(self.input_crystal)

    @property
    def input_crystal(self):
        return self.input_model

    def VectorMapperFactory(self, model, *args, **kwargs):
        return CrystalVectorMapper(model)

    def VectorSequenceFactory(self, model, vector_mapper, *args, **kwargs):
        ndim = vector_mapper.ndim
        return SobolSequence(ndim)

    def ObjectInitialiser(self, search_point):
        """Create a new Crystal ready to have point applied"""
        obj = GeneratedCrystal()
        obj.crystal = self.input_crystal.copy()
        obj.crystal.lattice.parameter_release()

        self.sat_engine._reset_hulls()

        obj.crystal.lattice.a = 1.0
        obj.crystal.lattice.b = 1.0
        obj.crystal.lattice.c = 1.0

        obj.search_point = search_point
        for im in range(len(search_point.positions) // 3):
            obj.search_point.positions[
                im * 3
            ] *= obj.crystal.space_group.asymmetric_unit[0][1]
            obj.search_point.positions[
                im * 3 + 1
            ] *= obj.crystal.space_group.asymmetric_unit[1][1]
            obj.search_point.positions[
                im * 3 + 2
            ] *= obj.crystal.space_group.asymmetric_unit[2][1]

        obj.target_volume = self.target_volume
        obj.max_volume = self.max_volume

        return obj

    def ObjectModifier(self, obj):
        crystal = obj.crystal
        logger.debug("Starting generate_crystal")
        passed_test = SearchPointAcceptanceChecker(
            obj.search_point,
            obj.crystal,
            0.001,
            Namespace(
                {"min_angle": 45.0, "max_angle": 135.0, "dummy_symmetry_operation": []}
            ),
            assign_sobol=False,
        ).check_acceptance()
        if not passed_test:
            logger.debug("SearchPointAcceptanceChecker failed")
            obj.generate_tests_passed = False
            return

        if crystal.lattice.angle_component_of_volume < 0.5:
            logger.debug("Flat cell check failed")
            obj.generate_tests_passed = False
            return

        # TODO re-implement molecular flexibility
        # Molecular configuration modification
        # for im, molecule in enumerate(crystal.asymmetric_unit):
        #    passed_test = ModifyMolecularConfiguration(molecule, im, random_vector,
        #                                               flexible_molecule_handler)
        #    if not passed_test:
        #        obj.generate_tests_passed = False
        #        return

        # Rotation for molecules that contain more than one atom
        jm = 0
        for im, molecule in enumerate(crystal.asymmetric_unit):
            if len(molecule.atoms) == 1:
                continue
            if isinstance(obj.search_point.orientations[jm], Quaternion):
                rotation_quaternion = obj.search_point.orientations[jm]
            else:
                rotation_vector = obj.search_point.orientations[jm * 3: jm * 3 + 3]
                rotation_quaternion = Quaternion.from_zero_to_one_vector(
                    rotation_vector
                )
            jm += 1
            logger.debug("Rotation: %s", rotation_quaternion)
            rotate_by_quaternion(molecule, rotation_quaternion, centroid(molecule))
            self.sat_engine._rotate_hull(im, rotation_quaternion, centroid(molecule))

        # Cell Lengths
        length_bounds = molecular_projection_on_to_lattice_vectors(crystal)
        logger.debug("Length Bounds: " + str(length_bounds))
        unique_points_on_axis = crystal.space_group.symmetry_unique_points()
        logger.debug("Unique Sites on Axis: " + str(unique_points_on_axis))
        for ix in range(len(length_bounds)):
            length_bounds[ix][0] = (
                length_bounds[ix][0]
                * unique_points_on_axis[ix]
                * len(crystal.asymmetric_unit)
            )
        logger.debug("Length Bounds: " + str(length_bounds))

        cell_length_engine = CellLengthGenerator(
            length_bounds=length_bounds,
            angle_component_of_volume=crystal.lattice.angle_component_of_volume,
            target_volume=obj.target_volume,
            standard_deviation=1.0 / 0.15,
            crystal_system=crystal.space_group.crystal_system,
        )
        new_cell_lengths = cell_length_engine.cell_lengths(
            length_factors=obj.search_point.unit_cell_lengths,
            length_permutation=obj.search_point.seed,
        )
        logger.debug("Setting new cell lengths:" + str(new_cell_lengths))
        crystal.lattice.a = new_cell_lengths[0]
        crystal.lattice.b = new_cell_lengths[1]
        crystal.lattice.c = new_cell_lengths[2]
        if crystal.lattice.volume > obj.max_volume:
            obj.generate_tests_passed = False
            obj.cell_gen_volume = True
            return

        # Translations
        for im, molecule in enumerate(crystal.asymmetric_unit):
            translate(molecule, -1 * centroid(molecule))
            frac_translation_vector = Vector3D(
                *obj.search_point.positions[im * 3: im * 3 + 3]
            )
            cart_translation_vector = frac_translation_vector.vec_mat_mul(
                crystal.lattice.lattice_vectors
            )
            translate(molecule, cart_translation_vector)
            logger.debug(
                "Fractional Centre: %s",
                centroid(molecule).vec_mat_mul(crystal.lattice.inv_lattice_vectors),
            )
            self.sat_engine._translate_hull(im, cart_translation_vector)

        # SAT Expansion
        try:
            obj.sat_tried = True
            self.sat_engine.sat_all_overlaps(crystal)
            if crystal.lattice.volume > obj.max_volume:
                obj.generate_tests_passed = False
            else:
                obj.generate_tests_passed = True
        except OverlappingHulls:
            obj.generate_tests_passed = False
            obj.overlapping_hulls = True

        obj.has_been_made = True

    def ObjectAcceptor(self, obj):
        if obj.generate_tests_passed:
            raise ObjectAccepted
        else:
            raise ObjectRejected

    def ConvergedLandscapeAssessment(self):
        if self.landscape.n_accepted == self.convergence.number_valid_structures:
            raise ConvergedLandscape
        elif self.landscape.n_trials > self.convergence.max_trials:
            raise ExhaustedLandscape
        else:
            raise IncompleteLandscape

    def LandscapeFactory(self):
        return CrystalLandscape()


def input_vector_from_crystal(generator, crystal, seed=-1):
    sp = CrystalSearchPoint([], [], [], [], [], seed)
    for i, molecule in enumerate(crystal.asymmetric_unit):
        input_molecule = generator.input_crystal.asymmetric_unit[i]
        f = centroid(molecule).vec_mat_mul(crystal.lattice.inv_lattice_vectors)
        sp.positions.append(f[0])
        sp.positions.append(f[1])
        sp.positions.append(f[2])

        v, q = rmsd(input_molecule, molecule, return_rotation=True)
        Q = Quaternion(*q)
        f = Q.to_zero_to_one_vector()
        logger.debug("From RMSD: " + str(Q))
        logger.debug("Results Factors: " + str(f))
        logger.debug("From Factors: " + str(Quaternion.from_zero_to_one_vector(f)))
        sp.orientations.append(f[0])
        sp.orientations.append(f[1])
        sp.orientations.append(f[2])

    length_bounds = molecular_projection_on_to_lattice_vectors(crystal)
    logger.debug("Length Bounds: " + str(length_bounds))
    unique_points_on_axis = crystal.space_group.symmetry_unique_points()
    logger.debug("Unique Sites on Axis: " + str(unique_points_on_axis))
    for ix in range(len(length_bounds)):
        length_bounds[ix][0] = (
            length_bounds[ix][0]
            * unique_points_on_axis[ix]
            * len(crystal.asymmetric_unit)
        )
    cell_length_engine = CellLengthGenerator(
        length_bounds=length_bounds,
        angle_component_of_volume=crystal.lattice.angle_component_of_volume,
        target_volume=generator.target_volume,
        standard_deviation=1.0 / 0.15,
        crystal_system=crystal.space_group.crystal_system,
    )
    input_vector = cell_length_engine.invert_cell_lengths(
        crystal.lattice.lengths, length_permutation=sp.seed
    )
    sp = sp._replace(unit_cell_lengths=input_vector)

    angles = crystal.lattice.lattice_parameters[3:]
    mn = 45.0
    mx = 135.0
    for i in range(3):
        v = ((angles[i] - mn) / (mx - mn) - 0.5) * math.pi
        v = (math.sin(v) + 1.0) / 2.0
        if angles[i] < mn or angles[i] > mx:
            raise ValueError("Angle {} is outside of acceptable bounds".format(i))
        if crystal.space_group.crystal_system.is_index_restricted(3 + i):
            pass
        else:
            sp.unit_cell_angles.append(v)
    return sp
