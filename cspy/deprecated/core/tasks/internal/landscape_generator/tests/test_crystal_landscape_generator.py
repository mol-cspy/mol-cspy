from __future__ import print_function
import unittest
import copy
import math
import logging
import numpy as np
from cspy.deprecated.core.models import (
    Crystal,
    Molecule,
    Atom,
    Lattice,
    SpaceGroup,
    SpaceGroupOperation,
    Vector3D,
)
from cspy.deprecated.core.tasks.internal.landscape_generator.crystal import (
    CrystalLandscapeGenerator,
)
from cspy.deprecated.core.tasks.internal.landscape_generator.crystal import Convergence
from cspy.deprecated.core.tasks.internal.landscape_generator.crystal import (
    input_vector_from_crystal,
)
from cspy.deprecated.core.resources.crystallographic_space_groups import (
    CrystallographicSpaceGroups,
)
from cspy.deprecated.core.models.sobol_sequence import SobolSequence
from cspy.deprecated.core.io.res import load_crystal, res_string_crystal, write_crystal
from cspy.deprecated.core.resources.crystal_by_refcode import AABHTZ

LOG = logging.getLogger(__name__)


class CrystalLandscapeGeneratorTestCase(unittest.TestCase):
    def test_init(self):
        LOG.debug("Crystal res file AABHTZ:\n%s", res_string_crystal(AABHTZ))
        AABHTZ.space_group = CrystallographicSpaceGroups.get(2)
        AABHTZ.lattice_type = 1
        clg = CrystalLandscapeGenerator(
            model=AABHTZ,
            convergence=Convergence(number_valid_structures=2, max_trials=10),
        )
        clg.run()
        for aobj in clg.landscape.accepted_objs:
            print(res_string_crystal(aobj.crystal))
        self.assertEqual(2, len(clg.landscape.accepted_objs))

    def test_input_vector_from_crystal(self):
        LOG.debug("Crystal res file AABHTZ:\n%s", res_string_crystal(AABHTZ))
        AABHTZ.space_group = CrystallographicSpaceGroups.get(2)
        AABHTZ.lattice_type = 1
        clg = CrystalLandscapeGenerator(
            model=AABHTZ,
            convergence=Convergence(number_valid_structures=2, max_trials=10),
        )
        clg.run()
        c = clg.landscape.accepted_objs[-1].crystal
        c_sp = clg.landscape.accepted_objs[-1].search_point
        sp = input_vector_from_crystal(clg, c, c_sp.seed)
        np.testing.assert_almost_equal(sp.positions, c_sp.positions)
        np.testing.assert_almost_equal(sp.unit_cell_lengths, c_sp.unit_cell_lengths)
        np.testing.assert_almost_equal(sp.unit_cell_angles, c_sp.unit_cell_angles)
        np.testing.assert_almost_equal(
            sorted(sp.orientations), sorted(c_sp.orientations)
        )
        self.assertEqual(sp.seed, c_sp.seed)
