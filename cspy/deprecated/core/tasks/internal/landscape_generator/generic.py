import logging

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.landscape_generator.generic")


class ObjectAccepted(Exception):
    pass


class ObjectRejected(Exception):
    pass


class ConvergedLandscape(Exception):
    pass


class IncompleteLandscape(Exception):
    pass


class ExhaustedLandscape(Exception):
    pass


class LandscapeGenerator:

    def __init__(
        self,
        model,
        vector_generator_args=None,
        vector_mapper_args=None,
        vector_mapper_kwargs=None,
        object_initialiser_args=None,
        object_acceptor_args=None,
        object_modifier_args=None,
        object_modifier_kwargs=None,
        convergence=None,
        sampling_infrastructure_args=None,
    ):

        list_if_none = lambda x: [] if x is None else x
        dict_if_none = lambda x: {} if x is None else x

        self.vector_generator_args = list_if_none(vector_generator_args)
        self.vector_mapper_args = list_if_none(vector_mapper_args)
        self.vector_mapper_kwargs = dict_if_none(vector_mapper_kwargs)
        self.object_initialiser_args = list_if_none(object_initialiser_args)
        self.object_acceptor_args = list_if_none(object_acceptor_args)
        self.object_modifier_args = list_if_none(object_modifier_args)
        self.object_modifier_kwargs = dict_if_none(object_modifier_kwargs)
        self.convergence = list_if_none(convergence)
        self.sampling_infrastructure_args = list_if_none(sampling_infrastructure_args)

        self.index = -1

        self.landscape = self.LandscapeFactory()
        self.rejected_objs = []

        self.input_model = model

        self.vector_mapper = self.VectorMapperFactory(
            self.input_model, *self.vector_mapper_args, **self.vector_mapper_kwargs
        )
        self.vector_sequence = self.VectorSequenceFactory(
            self.input_model, self.vector_mapper, *self.vector_generator_args
        )

    def run(self, start_index=0):
        index = start_index
        remaining_attempts = self.convergence.number_valid_structures
        while True:
            if remaining_attempts == 0:
                try:
                    if index > 1000000:
                        raise ConvergedLandscape
                    self.ConvergedLandscapeAssessment()
                except ConvergedLandscape:
                    logger.info("Landscape is complete")
                    return
                except ExhaustedLandscape:
                    logger.debug("Landscape is exhausted")
                    break
                except IncompleteLandscape:
                    logger.debug("Landscape is incomplete")
                    remaining_attempts = (
                        self.convergence.number_valid_structures
                        - self.landscape.n_accepted
                    )
                    continue
            index += 1
            remaining_attempts -= 1
            self.run_index(index)

    def run_index(self, index):
        search_vector = self.vector_sequence[index]
        search_point = self.vector_mapper.map_vector_to_search_point(search_vector)
        search_point = search_point._replace(seed=index)
        obj = self.ObjectInitialiser(search_point, *self.object_initialiser_args)
        try:
            self.ObjectModifier(obj)
            self.ObjectAcceptor(obj)
        except ObjectAccepted:
            self.landscape.accepted_objs.append(obj)
        except ObjectRejected:
            self.landscape.rejected_objs.append(obj)
        else:
            logger.error("Seed did not produce an ObjectAccepted or ObjectRejected")
