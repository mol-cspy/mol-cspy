from __future__ import division, absolute_import, print_function
import math
import numpy
import logging

from cspy.deprecated.core.models import CrystalSearchPoint
from cspy.deprecated.core.models.space_group import CrystalSystems
from cspy.deprecated.core.models import Crystal
from cspy.deprecated.core.tasks.internal.crystal.neighbours import neighbouring_translations

logger = logging.getLogger("cspy.deprecated.core.models.random_vector")


class SearchPointAcceptanceChecker(object):
    def __init__(
        self, random_vector, crystal, critical_frac_distance, args, assign_sobol=True
    ):
        assert isinstance(random_vector, CrystalSearchPoint)
        assert isinstance(crystal, Crystal)
        self.random_vector = random_vector
        self.crystal = crystal
        self.critical_frac_distance = critical_frac_distance
        self.lattice_params_temp = list(crystal.lattice.lattice_parameters)
        self.args = args
        # initially the random number will be accepted until further tests showed that it is of no good.
        self.acceptable_values = True
        self.assign_sobol = assign_sobol

    def check_acceptance(self):
        # The while loop is here to overcome problems in the parallelization of structure generator, which always got broken out at the end (so the entire loop
        # will be only run once). This is to ensure that when the code was parallelized, every thread will get one seed and only work on one seed before it is given
        # another seed from the master. Whereas before when the code was run in serial, this method will also take care of incrementing the actual Sobol seed (hence
        # it was in a while loop). In long run this while loop is unnecessary and should be removed.

        self.acceptable_values = AcceptableCellAnglesChecker(
            self.random_vector, self.crystal, self.critical_frac_distance, self.args
        ).check_acceptance()

        if self.acceptable_values is not True:
            logger.debug("AcceptableCellAnglesChecker failed")
            return False

        self.acceptable_values = AcceptableFlexibleDegreeOfFreedomChecker(
            self.random_vector, self.crystal, self.critical_frac_distance, self.args
        ).check_acceptance()

        if self.acceptable_values is not True:
            logger.debug("AcceptableFlexibleDegreeOfFreedomChecker failed")
            return False

        self.acceptable_values = AcceptableOrientationChecker(
            self.random_vector, self.crystal, self.critical_frac_distance, self.args
        ).check_acceptance()

        if self.acceptable_values is not True:
            logger.debug("AcceptableOrientationChecker failed")
            return False

        self.acceptable_values = AcceptableMolecularFractionalCentersChecker(
            self.random_vector, self.crystal, self.critical_frac_distance, self.args
        ).check_acceptance()
        if self.acceptable_values is not True:
            logger.debug("AcceptableMolecularFractionalCentersChecker failed")
        return self.acceptable_values


class AcceptableMolecularFractionalCentersChecker(SearchPointAcceptanceChecker):
    def __init__(self, random_vector, crystal, critical_frac_distance, args):
        super(AcceptableMolecularFractionalCentersChecker, self).__init__(
            random_vector, crystal, critical_frac_distance, args
        )

    def check_acceptance(self):
        ################################################
        # Block to set the fractional coordinates of molecular centroids in the unit cell
        # It will do some check to make sure that no two molecular centers are close to each other
        # under some critial distance in fractional space (according to the critical_frac_distance
        # parameter), otherwise, reject the structure because the SAT expand method will work very hard
        # to expand the cell and it is not efficient.
        ################################################
        if self.random_vector.positions != []:
            #            try:
            if True:
                #                print self.args
                self._set_fractional_centres_for_each_unique_molecule()
            #            except Exception as exc:
            #                return False

            # TODO re-implement this test
            return self._no_symmetry_related_fractional_centres_overlaps()
        return True

    def _set_fractional_centres_for_each_unique_molecule(self):
        for im, molecule in enumerate(self.crystal.asymmetric_unit):
            if self.args.dummy_symmetry_operation and (
                im >= len(self.crystal.asymmetric_unit) / 2
            ):
                from cspy.deprecated.core.utils.listmathfast import list3apply_sym

                root_im = im % (len(self.crystal.asymmetric_unit) / 2)
                symmetry_point = [
                    x % 1
                    for x in list3apply_sym(
                        self.random_vector.position[root_im * 3 : (root_im + 1) * 3],
                        self.args.dummy_symmetry_operation[0],
                        self.args.dummy_symmetry_operation[1],
                    )
                ]
                molecule.set_fractional_centre(symmetry_point)
            else:
                pass
                # molecule.set_fractional_centre(self.random_vector.position[im * 3:(im + 1) * 3])

    def _no_symmetry_related_fractional_centres_overlaps(self):
        from cspy.deprecated.core.models import Vector3D

        # apply the symmetry operations to generate all symmetry related molecules. and test if any pair of them
        # has an inter-centroid distance less than a critial value, if so, discard the sobol number
        asym_pos = [
            Vector3D(*self.random_vector.positions[i : i + 3])
            for i in range(len(self.random_vector.positions) // 3)
        ]
        for op in self.crystal.space_group.full_symmetry:
            n_op = None
            for im1, m1pos in enumerate(asym_pos):
                for im2, m2pos in enumerate(asym_pos):
                    if self._is_identical_mol_index(im1, im2) and op.is_identity():
                        continue
                    if n_op is None:
                        n_op = neighbouring_translations(
                            n=1,
                            lattice=(
                                Vector3D(1, 0, 0),
                                Vector3D(0, 1, 0),
                                Vector3D(0, 0, 1),
                            ),
                        )
                    m2posRTo = op.apply(m2pos)
                    for t_op in n_op:
                        m2posRT = m2posRTo + t_op
                        if (m1pos - m2posRT).magnitude() < self.critical_frac_distance:
                            logger.debug(
                                "too close"
                                + str(op.operation_string)
                                + str(t_op)
                                + str(m1pos)
                                + str(m2posRT)
                            )
                            return False
        logger.debug("Molecular position compatible with space group operations")
        return True

    def _is_identical_mol_index(self, index1, index2):
        return index1 == index2

    def _is_identity_transformation(self, op):
        return op.original_string_form.replace(" ", "").lower() == "x,y,z"

    def _fractional_centres_too_close_in_space(self):
        from listmath import list3norm

        return (
            list3norm(self.frac_center_1, self.frac_center_2)
            < self.critical_frac_distance
        )


class AcceptableOrientationChecker(SearchPointAcceptanceChecker):
    def __init__(self, random_vector, crystal, critical_frac_distance, args):
        super(AcceptableOrientationChecker, self).__init__(
            random_vector, crystal, critical_frac_distance, args
        )

    def check_acceptance(self):
        ################################################
        # Block to handle the orientation of molecule inside the unit cell.
        ################################################
        if self.random_vector.orientations != []:
            try:
                self._set_molecular_orientation_sobol()
            except AttributeError:
                # This function doesnt do anything so we can care
                return True
            except Exception as exc:
                return False
        return True

    def _set_molecular_orientation_sobol(self):
        im = 0
        for molecule in self.crystal.asymmetric_unit:
            if len(molecule.atoms) > 1:
                molecule.set_orientation_sobol(
                    self.random_vector.orientation[im * 3 : (im + 1) * 3]
                )
                im += 1


class AcceptableFlexibleDegreeOfFreedomChecker(SearchPointAcceptanceChecker):
    def __init__(self, random_vector, crystal, critical_frac_distance, args):
        super(AcceptableFlexibleDegreeOfFreedomChecker, self).__init__(
            random_vector, crystal, critical_frac_distance, args
        )

    def check_acceptance(self):
        #################################################
        # Block to handle molecular flexibility in structure generation. Allow molecule to flex around the flexible degree of
        # freedoms. This needs to done before setting the positions of molecules in the unit cell, because it will affect
        # for example, whether molecules will overlap with each others.
        #################################################
        if self.random_vector.flex_dfs != []:
            # Should really test the energy of sum of displacements for given cutoff-- do this later
            try:
                self._set_molecular_flexible_DOF()
            except Exception as exc:
                return False
        return True

    def _set_molecular_flexible_DOF(self):
        """ Map the terms in random_vector for molecular flexibility to the molecule.

        The data structure here is horrible and needs re-factoring.
        """
        counter = 0
        for im, molecule in enumerate(self.crystal.asymmetric_unit):
            temp_dict = {}
            # Must maintain the order of the dictionary for repeatability
            for temp_key in iter(sorted(molecule.flex_df_sobol.iteritems())):
                temp_dict[temp_key[0]] = self.random_vector.flex_df[counter]
                counter += 1

            molecule.set_flex_df_sobol(temp_dict)


class AcceptableCellAnglesChecker(SearchPointAcceptanceChecker):
    def __init__(self, random_vector, crystal, critical_frac_distance, args):
        super(AcceptableCellAnglesChecker, self).__init__(
            random_vector, crystal, critical_frac_distance, args
        )

    def check_acceptance(self):
        ################################################
        # Block of code to update the unit cell angles based on the assigned random number and restrictions, if any, imposed
        # by the symmetry of the crystal system.
        ################################################
        # TODO: this iaa seems to be of the same value as ia in the following loop, and should be removed for clarity.
        iaa = 0
        # self._check_restricted_angles_set_properly()
        self._set_unit_cell_angles_for_rhombohedral_lattice_type()
        self._set_unit_cell_angles_for_non_rhombohedral_lattice_type()

        try:
            # given the unit cell angles, try to set it. The method will actually calculate the inverse lattice vectors and if things
            # doesnt work, then the angles are probably bad, so this random vector is bad and quit out.
            logger.debug(
                "Setting new lattice parameters" + str(self.lattice_params_temp)
            )
            self.crystal.lattice.lattice_parameters = self.lattice_params_temp
        except ZeroDivisionError:
            return False

        # another check to make sure the cell angles make sense
        if self._are_cell_angles_too_bad() is True:
            return False
        return True

    def _check_restricted_angles_set_properly(self):
        if len(self.crystal.lattice.restricted_angles) != 3:
            raise CSPyException(
                "Restriced angles list not set properly, should be of a list of length 3!"
            )

    def _set_unit_cell_angles_for_rhombohedral_lattice_type(self):
        if self.crystal.space_group.crystal_system == CrystalSystems.TRIGONAL:
            # the case where angles restricted to be same
            if len(self.random_vector.unit_cell_angles) != 1:
                raise CSPyException(
                    "Wrong number of unit cell angle dimensions set for Rhombohedral cell!"
                )

            self.lattice_params_temp[3:6] = [
                self.random_vector.unit_cell_angles[0]
                * (self.args.max_angle - self.args.min_angle)
                + self.args.min_angle
            ] * 3

    def _set_unit_cell_angles_for_non_rhombohedral_lattice_type(self):
        if self.crystal.space_group.crystal_system != CrystalSystems.TRIGONAL:

            iaa = 0
            for ia in range(0, 3):
                # if self.crystal.lattice.restricted_angles[ia]:
                if self.crystal.space_group.crystal_system.is_index_restricted(3 + ia):
                    self.lattice_params_temp[
                        3 + ia
                    ] = self.crystal.space_group.crystal_system.get_real_index(
                        3 + ia
                    ).value
                else:
                    asin_val = (
                        math.asin(2.0 * self.random_vector.unit_cell_angles[iaa] - 1.0)
                        / math.pi
                    )
                    delta = self.args.max_angle - self.args.min_angle
                    self.lattice_params_temp[3 + ia] = (
                        0.5 + asin_val
                    ) * delta + self.args.min_angle
                    iaa += 1

    def _are_cell_angles_too_bad(self):
        if self._flat_cell_parameter() < 0.0:
            return True
        else:
            return False

    def _flat_cell_parameter(self):
        fac = math.acos(-1.0) * 2.0 / 360.0
        alpha = self.crystal.lattice.lattice_parameters[3] * fac
        beta = self.crystal.lattice.lattice_parameters[4] * fac
        gamma = self.crystal.lattice.lattice_parameters[5] * fac
        return (
            1.0
            + 2.0 * math.cos(alpha) * math.cos(beta) * math.cos(gamma)
            - math.cos(alpha) ** 2
            - math.cos(beta) ** 2
            - math.cos(gamma) ** 2
        )
