from .crystal import *

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.landscape_generator.local_crystal")


class CrystalLocalVectorMapper(object):
    def __init__(self, model, input_search_point, scale=1.0, offset=None):
        self.model = model
        self.scale = scale
        self.input_search_point = input_search_point
        sp = self.input_search_point
        self.input_search_vector = sp[0] + sp[1] + sp[2] + sp[3]
        logger.info("Input Search Vector: {}".format(self.input_search_vector))
        if offset is None:
            self.offset = self.input_search_vector
        else:
            self.offset = offset
        self._vector_mapper = CrystalVectorMapper(model)

    def map_vector_to_search_point(self, search_vector):
        logger.debug("Search Vector: {}".format(search_vector))
        local_search_vector = self.input_search_vector + self.scale * (
            search_vector - self.offset
        )
        logger.debug("Local Search Vector: {}".format(local_search_vector))
        search_point = self._vector_mapper.map_vector_to_search_point(
            local_search_vector
        )
        for i, v in enumerate(search_point.orientations):
            search_point.orientations[i] = v - math.floor(v)
        for i, v in enumerate(search_point.unit_cell_angles):
            search_point.unit_cell_angles[i] = v - math.floor(v)
        for i, v in enumerate(search_point.unit_cell_lengths):
            search_point.unit_cell_lengths[i] = max(v, 0.0)
        return search_point

    @property
    def ndim(self):
        return self._vector_mapper.ndim


class CrystalLocalLandscapeGenerator(CrystalLandscapeGenerator):
    def __init__(self, model, *args, **kwargs):
        self.model_backup = copy.deepcopy(model)
        super(CrystalLocalLandscapeGenerator, self).__init__(model, *args, **kwargs)

        self.input_search_point_seed = 1

        self.input_search_point = input_vector_from_crystal(
            self, self.model_backup, seed=self.input_search_point_seed
        )

        super(CrystalLocalLandscapeGenerator, self).__init__(
            self.model_backup, *args, **kwargs
        )
        # self.input_search_point = input_vector_from_crystal(self, self.model_backup, seed=self.input_search_point_seed)

    def VectorMapperFactory(self, model, *args, **kwargs):
        if hasattr(self, "input_search_point"):
            return CrystalLocalVectorMapper(
                model, self.input_search_point, kwargs.get("locality", 0.5), 0.5
            )
        else:
            return CrystalVectorMapper(model)

    def ObjectModifier(self, obj):
        seed = obj.search_point.seed
        # unit_cell_lengths = obj.search_point.unit_cell_lengths
        obj.search_point = obj.search_point._replace(seed=self.input_search_point_seed)
        # obj.search_point = obj.search_point._replace(unit_cell_lengths=[0,0,0])
        super(CrystalLocalLandscapeGenerator, self).ObjectModifier(obj)
        obj.search_point = obj.search_point._replace(seed=seed)
        # obj.search_point = obj.search_point._replace(unit_cell_lengths=unit_cell_lengths)
