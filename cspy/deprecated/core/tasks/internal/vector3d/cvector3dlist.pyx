from cspy.deprecated.core.models.cvector3d cimport cVector3D
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free


cdef class cVector3DList:
    cdef int number
    cdef double* data
    cdef cVector3D vi, vj
    def __cinit__(self, items):
        self.number = len(items)
        self.data = <double*> PyMem_Malloc(3*self.number * sizeof(double))
        i=-1
        for j in range(self.number):
            for k in range(3):
                i+=1 
                self.data[i] = items[j][k]
                print(self.data[i])
        self.vi = cVector3D(0,0,0)
        self.vj = cVector3D(0,0,0)

    cpdef dot_loop(self):
        cdef int i,j
        for i in range(self.number):
            print(self.vi.x)
            self.vi.xyz = &self.data[i*3]
            print(self.vi.x)
            for j in range(self.number):
                self.vj.xyz = &self.data[j*3]
                print('dot',self.vi.dot(self.vj))

    def __dealloc__(self):
        PyMem_Free(self.data)
