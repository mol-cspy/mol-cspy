from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.cvector3d import cSphere
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import centroid, moment_of_inertia
import numpy

# 2-D convex hull requires just the non-zero dimensions, named points
# Jarvis March O(nh) - Tom Switzer <thomas.switzer@gmail.com>
# https://tixxit.wordpress.com/tag/convex-hull/
TURN_LEFT, TURN_RIGHT, TURN_NONE = (1, -1, 0)


def turn2D(p, q, r):
    """Returns -1, 0, 1 if p,q,r forms a right, straight, or left turn."""
    a = (q[0] - p[0]) * (r[1] - p[1]) - (r[0] - p[0]) * (q[1] - p[1])
    return (a > 0) - (a < 0)


def _dist2D(p, q):
    """Returns the squared Euclidean distance between p and q."""
    dx, dy = q[0] - p[0], q[1] - p[1]
    return dx * dx + dy * dy


def _next_hull_pt2D(points, p):
    """Returns the next point on the convex hull in CCW from p."""
    q = p
    for i, r in enumerate(points):
        t = turn2D(p, q, r)
        if t == TURN_RIGHT or t == TURN_NONE and _dist2D(p, r) > _dist2D(p, q):
            q = r
            j = i
    return j, q


def convex_hull2D(points):
    """Returns the points on the convex hull of points in CCW order."""
    hull = [min(points)]
    hull_indices = []
    for i, p in enumerate(hull):
        j, q = _next_hull_pt2D(points, p)
        if q != hull[0]:
            hull.append(q)
            hull_indices.append(j)
        if q == hull[0]:
            hull_indices = [j] + hull_indices
    return hull, hull_indices


ORIGIN = Vector3D(0.0, 0.0, 0.0)


def tetrahedron(o=ORIGIN, hl=0.25):
    """ Form a tetrahedron of sites around an origin """
    return [
        o + Vector3D(+hl, +hl, +hl),
        o + Vector3D(+hl, -hl, -hl),
        o + Vector3D(-hl, +hl, -hl),
        o + Vector3D(-hl, -hl, +hl),
    ]


# def sandwich(sites, thickness=0.5):
#    """ Make list of sites that sandwich the thinest proportion of a list of sites """
#    hl = thickness/2.
#    c = centroid(sites)
#    moi = moment_of_inertia([s-c for s in sites])
#    return [s+moi[2]*hl for s in sites]+[s-moi[2]*hl for s in sites]


def sandwich(sites, thickness=0.7):
    """ Make list of sites that enclose the planar molecule in a set of points either side """
    c = centroid(sites)
    dim = numpy.isclose(
        sites, c, rtol=1e-01, atol=1e-01, equal_nan=False
    )  # Find the non-zero dimension
    for i in range(1, len(dim)):  # for the other sites find the one planar dimension
        for j in range(3):
            if dim[i][j] == True:
                break
    # make a perfectly planar 2D convex hull, and then make a sandwich
    points = []
    for s in sites:
        a = []
        for k in range(3):
            if j != k:
                a.append(s[k])
        points.append(a)
    # ch are the 2D convex hull points, hi are the convex hull indices
    ch, hi = convex_hull2D(points)
    ch2 = []
    for ih in hi:
        ch2.append(sites[ih])  # recover the 3D points
    hl = thickness / 2.0
    new_sites = []
    mult = [-1.0, 1.0]
    for s in ch2:
        count = 0
        for m in mult:
            a = []
            for k in range(3):
                if k != j:
                    a.append(float(s[k]))
                else:
                    a.append(float(c[k] + (m * hl)))
            a.append(float(s.radius))
            new_sites += [cSphere(a[0], a[1], a[2], a[3])]
    return new_sites


def cuboid(sites, thickness=0.7):
    """ Make list of sites that enclose the thinest proportion of a list of sites in a cuboid """
    c = centroid(sites)
    dim = numpy.isclose(
        sites, c, rtol=1e-01, atol=1e-01, equal_nan=False
    )  # Find the non-zero dimension
    for i in range(
        1, len(dim)
    ):  # for the other sites find the one non-planar dimension
        for j in range(3):
            if dim[i][j] == False:
                break
    # set thickness
    hl = thickness / 2.0
    points = []
    for s in sites:
        a = []
        pdim = False
        for k in range(3):
            if pdim == False and j != k:
                a.append(s[k])
                pdim = True
            elif j == k:
                a.append(s[k])
        points.append(a)
    # ch are the 2D convex hull points, hi are the convex hull indices
    ch, hi = convex_hull2D(points)
    ch2 = []
    for ih in hi:
        ch2.append(sites[ih])  # recover the 3D points
    hl = thickness / 2.0
    new_sites = []
    mult = [[1.0, 1.0], [1.0, -1.0], [-1.0, -1.0], [-1.0, 1.0]]
    for s in ch2:
        for m in mult:
            count = 0
            a = []
            for k in range(3):
                if k == j:
                    a.append(float(s[k]))
                else:
                    a.append(float(c[k] + (m[count] * hl)))
                    count += 1
            a.append(float(s.radius))
            new_sites += [cSphere(a[0], a[1], a[2], a[3])]
    return new_sites


def octahedron(sites, thickness=0.7):
    """ Make list of sites with 4 points around the centroid """
    c = centroid(sites)
    # Find the non-zero dimension
    dim = numpy.isclose(sites, sites[0], rtol=1e-04, atol=1e-04, equal_nan=False)
    for i in range(
        1, len(dim)
    ):  # for the other sites find the one non-planar dimension
        for j in range(3):
            if dim[i][j] == False:
                break
    # set thickness
    hl = thickness / 2.0
    points = []
    for s in sites:
        a = []
        pdim = False
        for k in range(3):
            if pdim == False and j != k:
                a.append(s[k])
                pdim = True
            elif j == k:
                a.append(s[k])
        points.append(a)
    # ch are the 2D convex hull points, hi are the convex hull indices
    ch, hi = convex_hull2D(points)
    ch2 = []
    for ih in hi:
        ch2.append(sites[ih])  # recover the 3D points
    hl = thickness / 2.0
    new_sites = []
    cent = [cSphere(c[0], c[1], c[2], hl)]
    mult = [[1.0, 1.0], [1.0, -1.0], [-1.0, -1.0], [-1.0, 1.0]]
    for s in cent:
        for m in mult:
            count = 0
            a = []
            for k in range(3):
                if k == j:
                    a.append(float(s[k]))
                else:
                    a.append(float(c[k] + (m[count] * hl)))
                    count += 1
            a.append(float(s.radius))
            new_sites += [cSphere(a[0], a[1], a[2], a[3])]
    new_sites += ch2
    return new_sites


def solid(sites, thickness=0.7):
    """ Add points either side of the centroid """
    c = centroid(sites)
    dim = numpy.isclose(
        sites, c, rtol=1e-01, atol=1e-01, equal_nan=False
    )  # Find the non-zero dimension
    for i in range(1, len(dim)):  # for the other sites find the one planar dimension
        for j in range(3):
            if dim[i][j] == True:
                break
    # make a perfectly planar convex hull
    # if it's not perfectly planar we get bad structures
    # set thickness
    points = []
    for s in sites:
        a = []
        for k in range(3):
            if j != k:
                a.append(s[k])
        points.append(a)
    # ch are the 2D convex hull points, hi are the convex hull indices
    ch, hi = convex_hull2D(points)
    new_sites = []
    ch2 = []
    for ih in hi:
        new_sites.append(sites[ih])  # recover the 3D points
    hl = thickness / 2.0
    cent = [c]
    mult = [-1.0, 1.0]
    for s in cent:
        count = 0
        for m in mult:
            a = []
            for k in range(3):
                if k != j:
                    a.append(float(s[k]))
                else:
                    a.append(float(c[k] + (m * hl)))
            a.append(float(hl))
            new_sites += [cSphere(a[0], a[1], a[2], a[3])]
    return new_sites


def box(origin, length=1.0):
    # TODO - implement documentation, what does this mean? what's the difference to the
    #       box around molecule?
    return sandwich(
        [
            origin + Vector3D(+length / 2.0, +length / 2.0, 0.0),
            origin + Vector3D(+length / 2.0, -length / 2.0, 0.0),
            origin + Vector3D(-length / 2.0, -length / 2.0, 0.0),
            origin + Vector3D(-length / 2.0, +length / 2.0, 0.0),
        ],
        thickness=length,
    )
