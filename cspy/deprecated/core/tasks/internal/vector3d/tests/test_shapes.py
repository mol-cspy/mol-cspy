from __future__ import print_function
import unittest
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.tasks.internal.vector3d.shapes import tetrahedron, sandwich


class ShapesTestCase(unittest.TestCase):
    def test_tetrahedron(self):
        sites = tetrahedron()
        self.assertEqual(len(sites), 4)

    @unittest.skip("incorrect test/not working")
    def test_sandwich(self):
        filling = [
            Vector3D(0, 0, 0),
            Vector3D(1, 0, 0),
            Vector3D(1, 1, 0),
            Vector3D(0, 1, 0),
        ]
        sites = sandwich(filling)

        self.assertEqual(len(sites), 8)
        for f in filling:
            self.assertTrue(f + Vector3D(0, 0, 0.25) in sites)
            self.assertTrue(f + Vector3D(0, 0, -0.25) in sites)

    @unittest.skip("unimplemented test")
    def test_box(self):
        pass
