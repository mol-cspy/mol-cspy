import unittest

from cspy.deprecated.core.tasks.internal.vector3d.cvector3dlist import cVector3DList
from cspy.deprecated.core.models import Vector3D


class cVector3DListTestCase(unittest.TestCase):
    def test_init(self):
        cVector3DList([Vector3D(1, 2, 3), Vector3D(3, 4, 5)])

    def test_dot_loop(self):
        vl = cVector3DList([Vector3D(1, 2, 3), Vector3D(3, 4, 5)])
        vl.dot_loop()
