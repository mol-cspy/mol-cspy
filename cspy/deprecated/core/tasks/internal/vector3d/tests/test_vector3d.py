import unittest

import math
import numpy
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import is_list_planar
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import inertia_tensor, moment_of_inertia

# class Vector3DMathTestCase(unittest.TestCase):
#    def test_rotated(self):
#        r = rotated((1.,0.,0.), math.pi*90./180., (0.,0.,1.))
#        numpy.testing.assert_almost_equal(r,  (0., 1., 0.))
#
#    def test_dot(self):
#        a = Vector3D(1, 0, 0)
#        b = Vector3D(2, 0, 0)
#        d = dot(a, b)
#        self.assertEqual(d, 2)
#        d = dot(a, b, origin=Vector3D(-1, 0, 0))
#        self.assertEqual(d, 6)
#
#    def test_cross(self):
#
#        a = Vector3D(1, 0, 0)
#        b = Vector3D(2, 0, 0)
#        c = cross(a, b)
#        self.assertEqual(c, Vector3D(0,0,0))
#
#        c = cross(a, b, origin=Vector3D(1, -1, 0))
#        self.assertEqual(c, Vector3D(0,0,-1))
#
#        c = cross(b, a, origin=Vector3D(1, -1, 0))
#        self.assertEqual(c, Vector3D(0,0,+1))
#
#    def test_centroid(self):
#        a = [Vector3D(1., 0, 0),
#             Vector3D(2., 0, 0),]
#        self.assertEqual(centroid(a), (1.5, 0 ,0))
#
#    def test_is_collinear(self):
#        a = Vector3D(1., 0, 0)
#        b = Vector3D(2., 0, 0)
#        c = Vector3D(3., 0, 0)
#        self.assertTrue(is_collinear(a, b, c))
#
#        self.assertTrue(is_collinear(a, b, c+Vector3D(0.01, 0, 0)))
#        self.assertFalse(is_collinear(a, b, c+Vector3D(0, 0.01, 0)))
#
#    def test_is_planar(self):
#        a = Vector3D(1., 0, 0)
#        b = Vector3D(2., 0, 0)
#        c = Vector3D(3., 1, 0)
#        d = Vector3D(4., 1, 0)
#        self.assertEqual(planar_func(a, b, c, d), 0)
#        self.assertTrue(is_planar(a, b, c, d))
#
#        d = Vector3D(4., 0, 0)
#        self.assertTrue(is_planar(a, b, c, d))
#
#        d = Vector3D(4., 0, 1)
#        self.assertFalse(is_planar(a, b, c, d))
#
#
#    def test_is_list_planar(self):
#        a = [Vector3D(1., 0, 0),
#             Vector3D(2., 0, 0),
#             Vector3D(3., 1, 0),
#             Vector3D(4., 1, 0),]
#        self.assertTrue(is_list_planar(a))
#        a += [Vector3D(123, 456, 0)]
#        self.assertTrue(is_list_planar(a))
#
#        a += [Vector3D(123, 456, 789)]
#        self.assertFalse(is_list_planar(a))
#
#
#    def test_inertia_tensor(self):
#        a = [Vector3D(1., 0, 0),
#             Vector3D(2., 0, 0),
#             Vector3D(3., 1., 0),
#             Vector3D(4., 1., 0),]
#        it = inertia_tensor(a)
#        self.assertEqual(it, ((2., 7., 0.),(7., 30., 0.),(0., 0., 32.)))
#
#
#    def test_moment_of_inertia(self):
#        a = [Vector3D(1., 0, 0),
#             Vector3D(2., 0, 0),
#             Vector3D(3., 1., 0),
#             Vector3D(4., 1., 0),]
#        moi = moment_of_inertia(a)
#        numpy.testing.assert_almost_equal(moi, ((-0.97324899, -0.22975292, 0.),(0.22975292, -0.97324899, 0.),(0., 0., 1.)))
#
#        moi = moment_of_inertia(a, origin=centroid(a))
#        numpy.testing.assert_almost_equal(moi, ((-0.92387953, -0.38268343, 0.),(0.38268343, -0.92387953, 0.),(0., 0., 1.)))
#
