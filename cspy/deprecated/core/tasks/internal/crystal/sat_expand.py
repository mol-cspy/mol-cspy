import logging

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.crystal.sat_expand")

import copy

from cspy.deprecated.core.models import Vector3D

from cspy.deprecated.core.tasks.internal.convex_hull.analysis import (
    OverlappingHulls,
    SeparationEngine,
)
from cspy.deprecated.core.tasks.internal.crystal.neighbours import neighbouring_translations
from cspy.deprecated.core.tasks.internal.crystal.modify import expand_crystal
from cspy.deprecated.core.tasks.internal.convex_hull.generation import MolecularConvexHullEngine
from cspy.deprecated.core.models.cvector3d import Points
from cspy.deprecated.core.tasks.internal.convex_hull.transform import translate as hull_translate
from cspy.deprecated.core.tasks.internal.convex_hull.transform import (
    rotate_by_quaternion as hull_rotate_by_quaternion,
)
import pickle


class CrystalExpansionBySAT(object):
    def __init__(
        self,
        crystal,
        hulls=None,
        symmetry_hulls=None,
        cell_cutoff=3,
        max_retries=50,
        max_volume=float("inf"),
        use_volume_metric=False,
    ):
        """ Collection of functions to expand a crystal using SAT

        Initialised with a crystal, this class provide a set of functions that work
        on the convex hull of each molecule in the crystal to test for and remove overlap
        between molecules. If the convex hull has been previously generated this can be used.
        Also for speed, containers for generating symmetry related versions of the hulls can
        be passed to save initialising these objects.

        :param crystal:
        :param hulls:
        :param symmetry_hulls:
        :param cell_cutoff:
        :param max_retries:
        :param max_volume:
        """
        self.crystal = crystal
        if hulls is None:
            _hulls = [
                MolecularConvexHullEngine.generate_convex_hull(m.atoms)
                for m in crystal.asymmetric_unit
            ]
        else:
            _hulls = hulls
        self.hulls = _hulls
        if symmetry_hulls is None:
            self.symmetry_hulls = copy.deepcopy(self.hulls)
        else:
            self.symmetry_hulls = symmetry_hulls
        self.hull_translations = neighbouring_translations(
            3, self.crystal.lattice, as_points=True
        )
        self.max_retries = max_retries
        self.unit_cell_hulls = []
        self.max_volume = max_volume
        self.engine = SeparationEngine(
            hulls=self.hulls,
            lattice=self.crystal.lattice,
            symmetry_operations=self.crystal.space_group.full_symmetry,
            ch2_translations=self.hull_translations,
            stop_on_any_separation=True,
            symmetry_hulls=self.symmetry_hulls,
            volume_metric=use_volume_metric,
        )

    def _rotate_hull(self, hull_index, rotation_quaternion, rotation_centre):
        hull_rotate_by_quaternion(
            self.hulls[hull_index], rotation_quaternion, rotation_centre
        )

    def _translate_hull(self, hull_index, translation_vector):
        hull_translate(self.hulls[hull_index], translation_vector)

    def _construct_hulls(self, crystal):
        self.hulls = [
            MolecularConvexHullEngine.generate_convex_hull(m.atoms)
            for m in crystal.asymmetric_unit
        ]
        for hull in self.hulls:
            c = hull.centroid
            hull.max_r = max(
                [(vert.v - c).magnitude() + vert.v.radius for vert in hull.vertices]
            )
        self.hulls_vertices_v = [
            [vert.v.copy() for vert in hull.vertices] for hull in self.hulls
        ]
        self.hulls_np_faces = [
            [np_face.copy() for np_face in hull.np_faces] for hull in self.hulls
        ]
        self.hulls_edge_vs = [
            [edge_v.copy() for edge_v in hull.edge_vs] for hull in self.hulls
        ]
        self.hulls_centroids = [hull.centroid.copy() for hull in self.hulls]
        self.symmetry_hulls = copy.deepcopy(self.hulls)
        self.engine.hulls = self.hulls
        self.engine.symmetry_hulls = self.symmetry_hulls

    def _reset_hulls(self):
        for ih, hull in enumerate(self.hulls):
            for iv, vert in enumerate(hull.vertices):
                self.hulls[ih].vertices[iv].v.copy_from(self.hulls_vertices_v[ih][iv])
            for i, face in enumerate(hull.np_faces):
                self.hulls[ih].np_faces[i].copy_from(self.hulls_np_faces[ih][i])
            for i, edge_v in enumerate(hull.edge_vs):
                self.hulls[ih].edge_vs[i].copy_from(self.hulls_edge_vs[ih][i])
            hull._centroid.copy_from(self.hulls_centroids[ih])

    def sat_all_overlaps(self, crystal=None):
        """Iteratively try to remove all overlaps in convex hulls"""
        logger.debug("Start SAT Expand")
        if crystal is not None:
            self.crystal = crystal
            self.engine.lattice = crystal.lattice
        for retry in range(self.max_retries):
            no_overlap = self.test_for_sat_overlap()
            if self.crystal.lattice.volume > self.max_volume:
                logger.debug("sat failed volume")
                return
            if no_overlap:
                logger.debug("No overlap")
                return
            else:
                self.remove_detected_overlap()
        else:
            raise OverlappingHulls

    def test_for_sat_overlap(self):
        """ Use the engine to test for overlap """
        self.crystal.lattice.parameter_lock()
        self.engine.ch2_translations = neighbouring_translations(
            3, self.crystal.lattice, as_points=True
        )
        self.engine.reset_caches()
        self.engine.determine_best_separation_vector_for_crystal()
        self.crystal.lattice.parameter_release()
        return not self.engine.best_axis["initialised"]

    def remove_detected_overlap(self):
        """Single attempt to remove smallest overlap in hull

        Functions returns True if no overlap present. This often needs to be called iteratively.
        """
        ba = self.engine.best_axis
        mag = ba["magnitude"]
        axis = Vector3D.from_Point(ba["v"])
        # axis = Vector3D.from_Point(self.engine.best_axis['v'])
        cent_axis = self.engine.best_vector_cent

        logger.debug("Using displacement axis %s", axis)
        logger.debug("Magnitude %s", mag)
        logger.debug("Centroid Vector %s", cent_axis)

        if cent_axis.magnitude() < 0.1:
            raise OverlappingHulls

        # get axes in fractional coordinates
        frac_axis = (mag * axis).vec_mat_mul(self.crystal.lattice.inv_lattice_vectors)
        frac_cent_axis = cent_axis.vec_mat_mul(self.crystal.lattice.inv_lattice_vectors)
        logger.debug("Using axis in fractionals %s", frac_axis)
        logger.debug("Centroid Vector in fractions %s", frac_cent_axis)

        # translate into lattice length adjustement factors
        # factors to multiply lattice lengths
        latt_fac = Vector3D(1, 1, 1)
        # terms to add to lengths
        latt_add = Vector3D(0, 0, 0)
        for i in range(3):
            # only adjust lengths if centroids are sufficiently separated
            if abs(frac_cent_axis[i]) > 0.01:
                latt_fac[i] = latt_fac[i] + abs(frac_axis[i] / frac_cent_axis[i])
                latt_add[i] += 0.001
        logger.debug("Lattice length adjustment factors %s", latt_fac)

        # correct for symmetry of the lattice lengths (e.g if a=b=c)
        for i in range(3):
            x = self.crystal.space_group.crystal_system.get_linked_index(i)
            for j in x:
                latt_fac[j] = max([latt_fac[k] for k in x])
                # latt_add behaviour must mirror latt_fac to maintain symm
                latt_add[j] = max([latt_add[k] for k in x])
        logger.debug("Symmetry adjusted lattice length adjustment factors %s", latt_fac)

        # Change lattice lengths
        logger.debug(
            "Old cell Lengths: a=%s b=%s c=%s",
            *self.crystal.lattice.lattice_parameters[0:3]
        )

        expand_crystal(self.crystal, latt_fac, latt_add, self.engine.hulls)
        logger.debug(
            "New cell Lengths: a=%s b=%s c=%s",
            *self.crystal.lattice.lattice_parameters[0:3]
        )
