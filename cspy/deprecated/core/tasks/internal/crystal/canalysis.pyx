from cspy.deprecated.core.models.cvector3d cimport cVector3D
cpdef molecular_projection_on_to_lattice_vectors(crystal):
    '''

    molecular projection on to the lattice vectors

    :param crystal: crystal object
    :type crystal: :class:`cspy.deprecated.core.models.crystal`

    :return: mol_projs_min_max
    :rtype: list of lists

    '''
    cdef cVector3D ta, axis, Tvec, axn0, axn1, axn2
    cdef double proj, avdw
    cdef double mol_min_max[3][2]
    cdef double mol_projs[3][2]
    cdef Py_ssize_t i, iax
    cdef list axisn, asym
    mol_projs[0][0] = -9e9
    mol_projs[0][1] = 9e9
    mol_projs[1][0] = -9e9
    mol_projs[1][1] = 9e9
    mol_projs[2][0] = -9e9
    mol_projs[2][1] = 9e9
    axn0 = (<cVector3D> crystal.lattice.lattice_vectors[0]).normalized()
    axn1 = (<cVector3D> crystal.lattice.lattice_vectors[1]).normalized()
    axn2 = (<cVector3D> crystal.lattice.lattice_vectors[2]).normalized()
    asym = crystal.asymmetric_unit
    for op in crystal.space_group.full_symmetry:
        Rmat, Tvec = op.lattice_specific_symmetry_operation(crystal.lattice)
        for molecule in asym:
            mol_min_max[0][0] = 9e9
            mol_min_max[0][1] = -9e9
            mol_min_max[1][0] = 9e9
            mol_min_max[1][1] = -9e9
            mol_min_max[2][0] = 9e9
            mol_min_max[2][1] = -9e9
            for i in range(len(molecule.atoms)):
                axyz = (<cVector3D> molecule.atoms[i].xyz)
                ta = axyz.vec_mat_mul(Rmat)+(<cVector3D> Tvec)
                avdw = (<double> molecule.atoms[i].element.vdw_radius)
                proj = (<cVector3D> ta).cdot(axn0)
                mol_min_max[0][0] = min(mol_min_max[0][0], proj-avdw)
                mol_min_max[0][1] = max(mol_min_max[0][1], proj+avdw)
                proj = (<cVector3D> ta).cdot(axn1)
                mol_min_max[1][0] = min(mol_min_max[1][0], proj-avdw)
                mol_min_max[1][1] = max(mol_min_max[1][1], proj+avdw)
                proj = (<cVector3D> ta).cdot(axn2)
                mol_min_max[2][0] = min(mol_min_max[2][0], proj-avdw)
                mol_min_max[2][1] = max(mol_min_max[2][1], proj+avdw)
            mol_projs[0][0] = max(mol_projs[0][0], 0.75*(mol_min_max[0][1] - mol_min_max[0][0]))
            mol_projs[0][1] = min(mol_projs[0][1], 0.75*(mol_min_max[0][1] - mol_min_max[0][0]))
            mol_projs[1][0] = max(mol_projs[1][0], 0.75*(mol_min_max[1][1] - mol_min_max[1][0]))
            mol_projs[1][1] = min(mol_projs[1][1], 0.75*(mol_min_max[1][1] - mol_min_max[1][0]))
            mol_projs[2][0] = max(mol_projs[2][0], 0.75*(mol_min_max[2][1] - mol_min_max[2][0]))
            mol_projs[2][1] = min(mol_projs[2][1], 0.75*(mol_min_max[2][1] - mol_min_max[2][0]))
    return mol_projs
