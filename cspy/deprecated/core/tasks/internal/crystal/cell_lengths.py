"""
Cell Length Generator
=====================

This package is used to determine the unit cell lengths for the crystal.
"""
import logging
import itertools
from scipy.stats import norm
from cspy.deprecated.core.models.lattice import Lattice

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.crystal.cell_lengths")


class CellLengthGenerator(object):

    number_of_independently_variable_cell_length = {
        1: 2,
        2: 2,
        3: 2,
        4: 1,
        5: 0,
        6: 1,
        7: 0,
    }
    """
    Dictionary holding the number of cell lengths that are independently variable for each lattice system.
    Because the structure generator tries to generate crystals around a given target unit cell volume,
    this means that not all unit cell lengths can be varied independently by a given random number. For example,
    in a triclinic system, only two cell lengths can be varied independently, and the third unit cell
    length must be worked out based on the target volume and the other two lengths determined.
    """

    def __init__(
        self,
        length_bounds,
        angle_component_of_volume,
        target_volume,
        standard_deviation,
        crystal_system,
    ):
        """
        Initialise an object to determine what is the appropriate cell length for a given crystal.

        Configured with length bounds, target volume, angle_component of the volume,
        standard devision in the generated volume from unit grid, and the lattice type.
        After initialisation, :ref:`.cell_lengths` should be called to determine the actual cell length.

        :param length_bounds: a list of the max and min values for each cell length :math:`a`, :math:`b` and :math:`c`.
                              e.g. [(15.0, 5.0), (3.0, 1.0), (20.0, 1.0)]
        :type length_bounds: tuple, list
        :param angle_component_of_volume: Angle component of the cell volume. This would be 1.0 for cubic system.
        :type angle_component_of_volume: float
        :param target_volume: The mean volume acheived from even sampling.
        :type target_volume: float
        :param standard_deviation: Number of standard deviations where the max length bound lies
        :type standard_deviation: float
        :param crystal_system: The lattice type, 1=Monoclinic, etc..
        :type crystal_system: int
        """
        self.length_bounds = length_bounds
        self.angle_component_of_volume = angle_component_of_volume
        self.target_volume = target_volume
        self.standard_deviation = standard_deviation
        self.crystal_system = crystal_system
        self.set_length_permutation(None)
        self._lengths = Lattice(1, 1, 1, 0, 0, 0)

    @property
    def num_linear_lengths(self):
        return max(0, self.crystal_system.num_variable_lengths - 1)

    @property
    def num_ppf_lengths(self):
        return self.crystal_system.get_number_of_linked_parameters(
            self.length_order[-1]
        )

    def set_length_permutation(self, length_permutation_index):
        """ Set the length setting order.

        Currently un-used. Changes the order of setting the lattice
        lengths, so that the ppf scaled lengths are swapped.
        """
        variable_lengths = self.crystal_system.variable_lengths
        _perms = {i: v for i, v in enumerate(itertools.permutations(variable_lengths))}
        # print _perms
        # raise Exception
        # _perms = {0: (0, 1, 2), 1: (2, 0, 1), 2: (1, 2, 0)}
        # _perms = {0: (2, 0, 1), 1: (0, 1, 2), 2: (1, 0, 2)}
        # _perms = {0: (1, 0, 2), 1: (2, 1, 0), 2: (2, 0, 1)}
        if length_permutation_index is None:
            self.length_order = _perms[0]  # (0, 1, 2)
        else:
            self.length_order = _perms[length_permutation_index % len(variable_lengths)]

    def cell_lengths(self, length_factors, length_permutation=None):
        r""" Calculates cell lengths within a given length bound :math:`[l_{\min},l_{\max}]`.

        This is the main function to be called in order to get the cell lengths for the
        crystal to be generated.

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        :return: cell lengths
        :rtype: list of 3 floats
        :param length_permutation: Default
        """
        assert len(length_factors) == self.crystal_system.num_variable_lengths
        self._lengths = Lattice(1, 1, 1, 0, 0, 0)
        self.set_length_permutation(length_permutation)
        self.calc_linear_cell_lengths(length_factors)
        self.calc_remaining_lengths(length_factors)
        return self._lengths.lattice_parameters[:3]

    def calc_linear_cell_lengths(self, length_factors):
        """ Set the lengths for uniform distributed lengths.

        Based on the lattice type, n lengths will be set as
        a uniform distribution between min and max length bounds

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        """
        n = self.num_linear_lengths
        for f, i in enumerate(self.length_order[:n]):
            value = min(self.length_bounds[i]) + length_factors[f] * (
                max(self.length_bounds[i]) - min(self.length_bounds[i])
            )
            self.crystal_system.set_lattice_parameter(self._lengths, i, value)
            logger.debug(
                "Setting linear length %s to %s", i, self._lengths.lattice_parameters[i]
            )

    def calc_remaining_lengths(self, length_factors):
        """ Set the remaining unit cell length(s), which needs to be worked out from the
        target volume parameters.

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        """
        sd = self.standard_deviation
        n = self.num_linear_lengths
        n_ppf = float(self.num_ppf_lengths)

        mean_length = self.remaining_length_product_for_target_vol() ** (1.0 / n_ppf)
        logger.debug("Mean length %s", mean_length)
        i = self.length_order[-1]
        factor = length_factors[-1]
        logger.debug("Setting length %s with factor %s", i, factor)
        ppf_factor = self.bounded_ppf(factor)
        logger.debug("PPF factor %s", ppf_factor)
        value = mean_length + ppf_factor * (
            (1.0 / sd) * max(self.length_bounds[i])
        ) ** (1.0 / n_ppf)
        logger.debug("Generated length of %s", value)
        value = max(value, min(self.length_bounds[i]))
        self.crystal_system.set_lattice_parameter(self._lengths, i, value)

    def remaining_length_product_for_target_vol(self):
        r""" Method to calculate the remaining products of the undetermined cell lengths from the target volume.
        Given the cell volume is calculated based on

        .. math::
            V=l_{1}l_{2}l_{3}f(\\alpha,\\beta,\\gamma)

        where :math:`f(\\alpha,\\beta,\\gamma)` is the angle component of the volume. For systems where only one
        cell length can be varied independently, say :math:`l_1`, this method works out the product

        .. math::
            l_{2}l_{3} = \\frac{V}{f(\\alpha,\\beta,\\gamma)}

        from which :math:`l_2` and :math:`l_3` can be worked out subsequently.

        :return: remaining length products
        :rtype: float
        """
        return self.target_volume / (
            self.angle_component_of_volume
            * self._lengths.lattice_parameters[0]
            * self._lengths.lattice_parameters[1]
            * self._lengths.lattice_parameters[2]
        )

    @staticmethod
    def bounded_ppf(factor):
        """ Returns a bounded scipy.stats.norm.ppf factors"""
        return norm.ppf(max(0.01, min(0.99, factor)))

    def invert_cell_lengths(self, cell_lengths, length_permutation=None):
        r""" Calculates cell lengths within a given length bound :math:`[l_{\min},l_{\max}]`.

        This is the main function to be called in order to get the cell lengths for the
        crystal to be generated.

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        :return: cell lengths
        :rtype: list of 3 floats
        :param length_permutation: Default
        """
        assert len(cell_lengths) == self.crystal_system.num_variable_lengths
        self._lengths = Lattice(*(list(cell_lengths) + [0, 0, 0]))
        self._factors = [None for _ in range(self.crystal_system.num_variable_lengths)]
        self.set_length_permutation(length_permutation)
        self.invert_linear_cell_lengths(cell_lengths)
        self.invert_remaining_lengths(cell_lengths)
        return self._factors

    def invert_remaining_lengths(self, lengths):
        """ Set the remaining unit cell length(s), which needs to be worked out from the
        target volume parameters.

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        """
        sd = self.standard_deviation
        n = self.num_linear_lengths
        n_ppf = float(self.num_ppf_lengths)
        i = self.length_order[-1]

        value = self._lengths.lattice_parameters[i]
        self.crystal_system.set_lattice_parameter(self._lengths, i, 1)
        mean_length = self.remaining_length_product_for_target_vol() ** (1.0 / n_ppf)
        self.crystal_system.set_lattice_parameter(self._lengths, i, value)
        logger.debug("Mean length %s", mean_length)
        # factor = length_factors[-1]
        # logger.debug('Setting length %s with factor %s', i, factor)
        # ppf_factor = self.bounded_ppf(factor)
        # logger.debug('PPF factor %s', ppf_factor)
        # value = mean_length + \
        #        ppf_factor*((1./sd)*max(self.length_bounds[i]))**(1./n_ppf)
        # logger.debug('Generated length of %s', value)
        # value = max(value, min(self.length_bounds[i]))
        # self.crystal_system.set_lattice_parameter(self._lengths, i, value)

        value = lengths[i]
        value = max(value, min(self.length_bounds[i]))
        value -= mean_length
        value /= ((1.0 / sd) * max(self.length_bounds[i])) ** (1.0 / n_ppf)
        self._factors[-1] = norm.cdf(value)

    def invert_linear_cell_lengths(self, lengths):
        """ Set the lengths for uniform distributed lengths.

        Based on the lattice type, n lengths will be set as
        a uniform distribution between min and max length bounds

        :param length_factors: three values between 0 and 1 to determine
                               value between min and max of the length
                               bounds
        :type length_factors: list, tuple of 3 floats.
        """
        n = self.num_linear_lengths
        for f, i in enumerate(self.length_order[:n]):
            try:
                l = (lengths[i] - min(self.length_bounds[i])) / (
                    max(self.length_bounds[i]) - min(self.length_bounds[i])
                )
            except ZeroDivisionError:
                l = float("inf")
            self._factors[f] = l
