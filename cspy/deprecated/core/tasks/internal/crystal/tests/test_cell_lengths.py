import unittest
import numpy

from cspy.deprecated.core.tasks.internal.crystal.cell_lengths import CellLengthGenerator

from cspy.deprecated.core.models.space_group import CrystalSystems


class CellLengthGeneratorTestCase(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_init_with_proper_parameters(self):
        # TODO - implement this
        # Give an example of initialising this object with real values. This will show how this object can be initialized.
        pass

    def test_init_no_checking(self):
        CellLengthGenerator(
            length_bounds=None,
            angle_component_of_volume=None,
            target_volume=None,
            standard_deviation=None,
            crystal_system=CrystalSystems.TRICLINIC,
        )

    @unittest.skip("unimplemented test")
    def test_set_length_permutation_with_no_length_permutation_index(self):
        # TODO - implement this
        pass

    @unittest.skip("unimplemented test")
    def test_set_length_permutation_with_length_permutation_index(self):
        # TODO - implement this
        pass

    def test_cell_lengths(self):
        # TODO - more detailed tests, this method calls three other methods, need mocked test
        #       to ensure these three tests are called.
        app = CellLengthGenerator(
            [[19.0, 1.0], [19, 1.0], [19.0, 1.0]],
            1.0,
            1000.0,
            10.0,
            CrystalSystems.TRICLINIC,
        )
        lengths = app.cell_lengths([0.5, 0.5, 0.5])
        numpy.testing.assert_almost_equal(
            lengths,
            [10, 10, 10],
            err_msg="Mean of 10 for length for lattice type {}".format(
                CrystalSystems.TRICLINIC
            ),
        )

    @unittest.skip("unimplemented test")
    def test_calc_linear_lengths(self):
        # TODO - implement this
        pass

    @unittest.skip("unimplemented test")
    def test_calc_remaining_lengths(self):
        # TODO - implement this
        pass

    @unittest.skip("unimplemented test")
    def test_set_bounded_ppf(self):
        # TODO - implement this
        pass

    def test_volume_dist(self):
        # Terms here are computed from running the code
        correct = [
            1.88350675155,
            4.66973218737,
            6.67879675284,
            8.39546834680,
            10.0,
            11.6045316531,
            13.3212032472,
            15.3302678126,
            18.1164932484,
        ]
        app = CellLengthGenerator(
            [[19.0, 1.0], [19, 1.0], [19.0, 1.0]],
            1.0,
            1000.0,
            3.0,
            CrystalSystems.TRICLINIC,
        )
        for f, c in zip(range(1, 10), correct):
            lengths = app.cell_lengths([0.5, 0.5, float(f) / 10.0])
            self.assertAlmostEqual(lengths[2], c)

    def test_cell_lengths_tetragonal(self):
        app = CellLengthGenerator(
            [[19.0, 1.0], [19, 1.0], [19.0, 1.0]],
            1.0,
            1000.0,
            10.0,
            CrystalSystems.TETRAGONAL,
        )
        with self.assertRaises(AssertionError):
            lengths = app.cell_lengths([0.5, 0.5, 0.5])

        lengths = app.cell_lengths([0.5, 0.5])
        numpy.testing.assert_almost_equal(
            lengths,
            [10, 10, 10],
            err_msg="Mean of 10 for length for lattice type {}".format(
                app.crystal_system
            ),
        )
