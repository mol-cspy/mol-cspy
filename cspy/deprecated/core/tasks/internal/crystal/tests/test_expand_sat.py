import unittest
from cspy.deprecated.core.resources.crystal_by_refcode import AABHTZ
from cspy.deprecated.core.tasks.internal.crystal.sat_expand import CrystalExpansionBySAT


class SATExpandTestCase(unittest.TestCase):
    def test_simple(self):
        c = AABHTZ
        engine = CrystalExpansionBySAT(c)
        no_overlap = engine.test_for_sat_overlap()
        self.assertFalse(no_overlap)
        engine.sat_all_overlaps()
        no_overlap = engine.test_for_sat_overlap()
        self.assertTrue(no_overlap)
