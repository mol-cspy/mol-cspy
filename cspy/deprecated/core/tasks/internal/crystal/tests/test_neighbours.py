import unittest

from cspy.deprecated.core.tasks.internal.crystal.neighbours import (
    neighbouring_space_group_operations,
)
from cspy.deprecated.core.models import SpaceGroupOperation, Vector3D


class NeighboursTest(unittest.TestCase):
    def test_neighbouring_space_group_operations(self):
        old_ops = [SpaceGroupOperation("x,y,z")]
        new_ops = neighbouring_space_group_operations(old_ops)
        self.assertEqual(new_ops[0].operation_string, "x+0.0,y+0.0,z+0.0")
        test_point = Vector3D(0.1, 0.2, 0.3)
        self.assertEqual(old_ops[0].apply(test_point), new_ops[0].apply(test_point))

        new_ops = neighbouring_space_group_operations(old_ops, n=1)
        self.assertEqual(len(new_ops), 27)

    @unittest.skip("unimplemented test")
    def test_neighbouring_translations(self):
        # TODO - implement this!
        pass
