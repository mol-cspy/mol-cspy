import unittest
import numpy

from cspy.deprecated.core.models import Lattice, Crystal, Molecule, Atom, Vector3D
from cspy.deprecated.core.models.space_group import SpaceGroup
from cspy.deprecated.core.models.space_group_operation import SpaceGroupOperation

from cspy.deprecated.core.tasks.internal.crystal.modify import change_latt_params, expand_crystal
from cspy.deprecated.core.tasks.internal.crystal.modify import expand_crystal_by_sat


class ChangeLattParamsTestCase(unittest.TestCase):
    def test_change_latt_params(self):
        my_latt = Lattice(10.0, 10.0, 10.0, 90, 90, 90)
        new_params = (9.8, 10.0, 7.5, 90, 120, 109)
        change_latt_params(my_latt, new_params)

        for i, param in enumerate(my_latt.lattice_parameters):
            self.assertAlmostEqual(param, new_params[i], 15)

        return

    @unittest.skip("unimplemented test")
    def test_set_new_lattice_lengths(self):
        # TODO - implement this!
        pass

    def test_expand_crystal(self):
        l = Lattice(10, 10, 10, 90, 90, 90)
        m = Molecule([Atom("C", Vector3D(0, 0, 0)), Atom("O", Vector3D(0, 0, 1))])
        c = Crystal(asymmetric_unit=[m], lattice=l, space_group=None)
        expand_crystal(c, Vector3D(2, 2, 2))
        self.assertEqual(c.lattice.a, 20)
        self.assertEqual(c.lattice.b, 20)
        self.assertEqual(c.lattice.c, 20)
        self.assertEqual(c.lattice.alpha, 90)
        self.assertEqual(m.atoms[0].xyz, Vector3D(0, 0, 0.5))
        self.assertEqual(m.atoms[1].xyz, Vector3D(0, 0, 1.5))


#    def test_expand_crystal_by_sat(self):
#        l = Lattice(1,1,1,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0,0,0))])
#        m2 = Molecule([Atom('C', Vector3D(0,0,0.1))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z')])
#        c = Crystal(asymmetric_unit=[m1, m2], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        self.assertEqual(m1.atoms[0].xyz, Vector3D(0,0,0))
#        numpy.testing.assert_array_almost_equal(m2.atoms[0].xyz, Vector3D(0,0,0.5))
#        self.assertEqual(c.lattice.a, 1)
#        self.assertEqual(c.lattice.b, 1)
#        self.assertAlmostEqual(c.lattice.c, 5)
#        self.assertEqual(c.lattice.alpha, 90)
#
#    def test_expand_crystal_by_sat2(self):
#        l = Lattice(1,1,2,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0.0,0.0,0.0))])
#        m2 = Molecule([Atom('C', Vector3D(0.0,0.0,0.1))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z')])
#        c = Crystal(asymmetric_unit=[m1, m2], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        self.assertEqual(m1.atoms[0].xyz, Vector3D(0,0,0))
#        numpy.testing.assert_array_almost_equal(m2.atoms[0].xyz, Vector3D(0.0,0.0,0.5))
#        self.assertEqual(c.lattice.a, 1)
#        self.assertEqual(c.lattice.b, 1)
#        self.assertAlmostEqual(c.lattice.c, 5*2)
#        self.assertEqual(c.lattice.alpha, 90)
#
#        l = Lattice(1,1,2,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0.1,0.1,0.1))])
#        m2 = Molecule([Atom('C', Vector3D(0.2,0.2,0.2))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z')])
#        c = Crystal(asymmetric_unit=[m1, m2], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        numpy.testing.assert_array_almost_equal(m1.atoms[0].xyz, Vector3D(0.1,0.1,0.5))
#        numpy.testing.assert_array_almost_equal(m2.atoms[0].xyz, Vector3D(0.2,0.2,1.0))
#        self.assertEqual(c.lattice.a, 1)
#        self.assertEqual(c.lattice.b, 1)
#        self.assertAlmostEqual(c.lattice.c, 5*2)
#        self.assertEqual(c.lattice.alpha, 90)
#        l = Lattice(1,1,2,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0.1,0.1,0.1))])
#        m2 = Molecule([Atom('C', Vector3D(0.2,0.2,0.2))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z')])
#        c = Crystal(asymmetric_unit=[m2, m1], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        numpy.testing.assert_array_almost_equal(m1.atoms[0].xyz, Vector3D(0.1,0.1,0.5))
#        numpy.testing.assert_array_almost_equal(m2.atoms[0].xyz, Vector3D(0.2,0.2,1.0))
#        self.assertEqual(c.lattice.a, 1)
#        self.assertEqual(c.lattice.b, 1)
#        self.assertAlmostEqual(c.lattice.c, 5*2)
#        self.assertEqual(c.lattice.alpha, 90)
#
#    def test_sat_exand_sg(self):
#        l = Lattice(1,1,1,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0.0,0.0,0.1))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z'),
#                         SpaceGroupOperation('-x,-y,-z')])
#        c = Crystal(asymmetric_unit=[m1], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        numpy.testing.assert_array_almost_equal(m1.atoms[0].xyz, Vector3D(0.0,0.0,0.18660254))
#        self.assertAlmostEqual(c.lattice.a, 1.8660254037844388)
#        self.assertAlmostEqual(c.lattice.b, 1.8660254037844388)
#        self.assertAlmostEqual(c.lattice.c, 1.8660254037844388)
#        self.assertEqual(c.lattice.alpha, 90)
#
#        l = Lattice(1,1,1,90,90,90)
#        m1 = Molecule([Atom('C', Vector3D(0.05,0.05,0.05))])
#        sg = SpaceGroup([SpaceGroupOperation('x,y,z'),
#                         SpaceGroupOperation('-x,-y,-z')])
#        c = Crystal(asymmetric_unit=[m1], lattice=l, space_group=sg)
#        expand_crystal_by_sat(c)
#        numpy.testing.assert_array_almost_equal(m1.atoms[0].xyz, Vector3D(0.06924501,  0.06924501,  0.06924501))
#        self.assertAlmostEqual(c.lattice.a, 1.38490017946)
#        self.assertAlmostEqual(c.lattice.b, 1.38490017946)
#        self.assertAlmostEqual(c.lattice.c, 1.38490017946)
#        self.assertEqual(c.lattice.alpha, 90)
