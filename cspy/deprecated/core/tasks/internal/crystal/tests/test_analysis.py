"""
Crystal Analysis Tests
======

"""

import unittest
import numpy

from cspy.deprecated.core.models import (
    Vector3D,
    Atom,
    Molecule,
    Lattice,
    SpaceGroup,
    SpaceGroupOperation,
    Crystal,
)
from cspy.deprecated.core.tasks.internal.crystal.analysis import (
    molecular_projection_on_to_lattice_vectors,
)


class CrystalAnalyisTestCase(unittest.TestCase):
    """

    Crystal Analysis Test Class

    """

    def test_molecular_projection_on_to_lattice_vectors(self):
        """

        test molecular_projection_on_to_lattice_vectors

        make a molecule m


        """
        a1 = Atom("C", Vector3D(0.0, 0.0, 0.0))
        a2 = Atom("C", Vector3D(0.0, 0.0, 1.0))
        a3 = Atom("C", Vector3D(0.0, 1.0, 0.0))
        a4 = Atom("C", Vector3D(1.0, 0.0, 0.0))
        m = Molecule([a1, a2, a3, a4])
        l = Lattice(1, 1, 1, 90, 90, 90)
        s = SpaceGroup([SpaceGroupOperation("x,y,z")])
        c = Crystal(asymmetric_unit=[m], lattice=l, space_group=s)
        c.lattice_type = 1
        lb = molecular_projection_on_to_lattice_vectors(c)
        numpy.testing.assert_array_almost_equal(
            lb, [[3.3, 3.3], [3.3, 3.3], [3.3, 3.3]]
        )
