"""
Crystal Analysis
======
"""
from cspy.deprecated.core.tasks.internal.crystal.canalysis import (
    molecular_projection_on_to_lattice_vectors,
)
from cspy.deprecated.core.tasks.internal.molecule.analysis import (
    box_volume as molecular_box_volume,
)


def molecular_projection_on_to_lattice_vectors_old(crystal):
    """

    molecular projection on to the lattice vectors

    :param crystal: crystal object
    :type crystal: :class:`cspy.deprecated.core.models.crystal`

    :return: mol_projs_min_max
    :rtype: list of lists

    """
    mol_projs = [[], [], []]
    for op in crystal.space_group.full_symmetry:
        Rmat, Tvec = op.lattice_specific_symmetry_operation(crystal.lattice)
        for molecule in crystal.asymmetric_unit:
            mol_min_max = [
                [float("inf"), -float("inf")],
                [float("inf"), -float("inf")],
                [float("inf"), -float("inf")],
            ]
            for i, a in enumerate(molecule.atoms):
                ta = a.xyz.vec_mat_mul(Rmat) + Tvec
                for iax, axis in enumerate(crystal.lattice.lattice_vectors):
                    proj = ta.dot(axis.normalized())
                    mol_min_max[iax][0] = min(
                        mol_min_max[iax][0], proj - a.element.vdw_radius
                    )
                    mol_min_max[iax][1] = max(
                        mol_min_max[iax][1], proj + a.element.vdw_radius
                    )
            mol_projs[0].append(0.75 * (mol_min_max[0][1] - mol_min_max[0][0]))
            mol_projs[1].append(0.75 * (mol_min_max[1][1] - mol_min_max[1][0]))
            mol_projs[2].append(0.75 * (mol_min_max[2][1] - mol_min_max[2][0]))
    mol_projs_min_max = [
        [max(mol_projs[0]), min(mol_projs[0])],
        [max(mol_projs[1]), min(mol_projs[1])],
        [max(mol_projs[2]), min(mol_projs[2])],
    ]
    return mol_projs_min_max


def box_volume(crystal):
    """Computes volume of crystal from boxed molecules"""
    au_vol = sum([molecular_box_volume(m) for m in crystal.asymmetric_unit])
    uc_vol = au_vol * len(crystal.space_group.full_symmetry)
    return uc_vol
