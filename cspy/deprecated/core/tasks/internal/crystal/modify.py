import copy
import logging

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.crystal.modify")

from cspy.deprecated.core.tasks.internal.molecule.transform import translate
from cspy.deprecated.core.tasks.internal.molecule.analysis import centroid
from cspy.deprecated.core.models import Vector3D, Lattice, CrystalSearchPoint
from cspy.deprecated.core.tasks.internal.convex_hull.generation import MolecularConvexHullEngine

from cspy.deprecated.core.models import Vector3D

# from cspy.deprecated.core.tasks.internal.convex_hull.periodic_analysis import smallest_overlapping_axis


def set_cell_angles(crystal, crystal_search_point, crystal_search_space):
    """
    Method that set the unit cell angles of a given crystal, based on the random numbers
    in the search point vector :class:`.CrystalSearchPoint` that corresponding to the cell angle
    components, as well as the maximum and minimum range of angles set in the crystal search space

    :param crystal: A crystal to be made (:class:`.Crystal`).
    :param crystal_search_point: A search point object (:class:`.CrystalSearchPoint`) that contains all the random
                                    numbers to be converted to real lattice parameters.
    :param crystal_search_space: A search space object (:class:`.CrystalSearchSpace`) that defines the search degrees
                                    of freedoms.
    :return:
    """
    pass


def change_latt_params(lattice, new_params):
    r"""
    Change the lattice parameters of a given lattice to the new set of parameters.

    :param lattice: A fully constructed :class:`.Lattice` object.
    :param new_params: A list of six new parameters :math:`[a,b,c,\alpha,\beta,\gamma]` that the
            input lattice will be changed to.
    """
    lattice.a = new_params[0]
    lattice.b = new_params[1]
    lattice.c = new_params[2]
    lattice.alpha = new_params[3]
    lattice.beta = new_params[4]
    lattice.gamma = new_params[5]


def set_new_lattice_lengths(crystal, abc, hulls=None):
    # TODO - need to refactor out the part that move molecules and convex hull
    # TODO - implement documentations
    if hulls is None:
        hulls = []
    new_latt = Lattice(*crystal.lattice.lattice_parameters)
    new_latt.a = abc[0]
    new_latt.b = abc[1]
    new_latt.c = abc[2]
    for molecule in crystal.asymmetric_unit:
        old_cen_xyz = centroid(molecule)
        frac_cen = old_cen_xyz.vec_mat_mul(crystal.lattice.inv_lattice_vectors)
        new_cen_xyz = frac_cen.vec_mat_mul(new_latt.lattice_vectors)
        cart_trans = new_cen_xyz - old_cen_xyz
        translate(molecule, cart_trans)
    for hull in hulls:
        old_cen_xyz = hull.centroid
        frac_cen = old_cen_xyz.vec_mat_mul(crystal.lattice.inv_lattice_vectors)
        new_cen_xyz = frac_cen.vec_mat_mul(new_latt.lattice_vectors)
        cart_trans = new_cen_xyz - old_cen_xyz
        for iv in range(len(hull.vertices)):
            hull.vertices[iv].v += cart_trans
        hull._centroid += cart_trans
    crystal.lattice.a = abc[0]
    crystal.lattice.b = abc[1]
    crystal.lattice.c = abc[2]


def expand_crystal(crystal, abc_factors, abc_add=Vector3D(0, 0, 0), hulls=None):
    # TODO - implement documentations, what do abc_factors and abc_add mean???
    set_new_lattice_lengths(
        crystal,
        abc_add
        + Vector3D(*abc_factors).element_mul(
            Vector3D(*crystal.lattice.lattice_parameters[0:3])
        ),
        hulls=hulls,
    )


def expand_crystal_by_sat(crystal, cell_cutoff=3):
    # TODO - implement documentations
    # type: (object, object) -> object
    asym_ch = [
        MolecularConvexHullEngine.generate_convex_hull(m.atoms)
        for m in crystal.asymmetric_unit
    ]
    mag, axis, cent_axis = smallest_overlapping_axis(
        asym_ch, crystal.lattice, crystal.space_group, cell_cutoff=cell_cutoff
    )
    if axis is None:
        return True
    logger.debug("Using displacement axis %s", axis)
    logger.debug("Magnitude %s", mag)
    fac = Vector3D(1, 1, 1) + axis * (-mag) / cent_axis.dot(axis)
    logger.debug("Displacement fac %s", fac)
    expand_crystal(crystal, fac)
    return False
