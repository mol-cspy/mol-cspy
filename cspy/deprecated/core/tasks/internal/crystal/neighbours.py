"""
Computes neighbours for a crystal
=================================

"""


import itertools
import logging

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.crystal.neighbours")

from cspy.deprecated.core.models import SpaceGroupOperation
from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models import Lattice
from cspy.deprecated.core.models.cvector3d import cneighbouring_translations
from cspy.deprecated.core.models.cmatrix3d import cneighbouring_translations_points


def neighbouring_translations(n=0, lattice=None, as_points=False):
    # TODO - implement documentation!
    if lattice:
        if isinstance(lattice, Lattice):
            lv = lattice.lattice_vectors
        else:
            # TODO - why setting lattice to lattice vectors? explain
            lv = lattice
        if as_points:
            trans = cneighbouring_translations_points(n, lv)
            return trans
        else:
            trans = cneighbouring_translations(n, lv)
    else:
        trans = [Vector3D(*v) for v in itertools.product(range(-n, n + 1), repeat=3)]
    sorted_trans = sorted(trans, key=Vector3D.magnitude)
    return sorted_trans


def neighbouring_space_group_operations(space_group_operations, n=0):
    # TODO - implement documentation!
    trans = cneighbouring_translations(n, ((1, 0, 0), (0, 1, 0), (0, 0, 1)))
    # [Vector3D(*v) for v in itertools.product(range(-n,n+1), repeat=3)]
    sorted_trans = sorted(trans, key=Vector3D.magnitude)
    new_space_group_operations = []
    logger.debug("Adding %s translations", len(sorted_trans))
    # for sgo in space_group_operations:
    #    logger.debug('Operations:'+sgo.operation_string)

    for t in sorted_trans:
        for sgo in space_group_operations:
            s = sgo.operation_string.split(",")
            new_op = ",".join([s[i] + "+" + str(t[i]) for i in range(3)])
            new_sgo = SpaceGroupOperation(new_op)
            new_space_group_operations.append(new_sgo)

    return new_space_group_operations
