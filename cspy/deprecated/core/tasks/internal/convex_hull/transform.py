from cspy.deprecated.core.tasks.internal.vector3d import vector3d


def translate(hull, vector):
    """
    Method to translate a convex hull along the direction specified by a given vector.

    :param hull: A :class:`.Hull` object.
    :param vector: A :class:`.Vector3D` specifying the direction along which the
                    convex hull will be traslated.
    """
    for vert in hull.vertices:
        vert.v += vector
    hull._centroid += vector


def rotate_by_quaternion(hull, quaternion, origin):
    """
    Method to rotate the convex hull about a given origin by an angle specified by a quaternion.

    :param hull: A :class:`.Hull` object.
    :param quaternion: Specifying by how much the convex hull will be rotated.
    :param origin:  :class:`.Vector3D` specifying the rotational center.
    """
    translate(hull, -1 * origin)
    for vert in hull.vertices:
        vert.v.copy_from(vector3d.rotated_by_quaternion(vert.v, quaternion))
    for np_face in hull.np_faces:
        np_face.copy_from(vector3d.rotated_by_quaternion(np_face, quaternion))
    for edge_v in hull.edge_vs:
        edge_v.copy_from(vector3d.rotated_by_quaternion(edge_v, quaternion))
    hull._centroid.copy_from(vector3d.rotated_by_quaternion(hull._centroid, quaternion))
    translate(hull, origin)
