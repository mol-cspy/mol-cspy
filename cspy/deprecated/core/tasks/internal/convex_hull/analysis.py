"""
Convex Hull Analysis package
============================

The main feature in this package is the `:class:.SeparationEngine` for Hulls (ch1 and ch2).
The purpose is to analyse if 2 convex hulls overlap according to the separating axis theorem (SAT)
SeparationEngine allows the alleviation of overlap (to a point) by expansion of the cell (SAT expand)

"""
import itertools
from collections import namedtuple
import logging

logger = logging.getLogger("cspy.deprecated.core.tasks.internal.convex_hull.analyis")

from cspy.deprecated.core.models.cvector3d import cross_lists, non_parallel_vectors
from cspy.deprecated.core.models.cvector3d import minmax_cVector3D_onto_axis

from cspy.deprecated.core.tasks.internal.convex_hull.canalysis import (
    OverlappingHulls,
    SeparatedHulls,
)


class SeparationVector(object):
    """
    A SeparationVector object. Initialization

    :param v: the vector
    :type v: vector/cVector3D/cSphere
    :param magnitude: magnitude of the vector
    :type magnitude: float
    """

    def __init__(self, v, magnitude=None):
        """
        Initialization
        """
        self.v = v
        self.magnitude = magnitude

    def __copy__(self):
        """
        return a copy of the SeparationVector
        """
        return SeparationVector(self.v.copy(), self.magnitude)

    def __str__(self):
        """
        return the string form
        """
        return "<SeparationVector magnitude={}, v={}>".format(self.magnitude, self.v)


MinMax = namedtuple("MinMax", "minimum maximum")


def fromProjectionOfCHVerticesOnToVector(ch_vertices, vector):
    # TODO - implement documentation!
    mini, maxi = minmax_cVector3D_onto_axis([vert.v for vert in ch_vertices], vector)
    return MinMax(mini, maxi)


class MinMaxC(MinMax):
    """
    Minimimum/Maximum object. Initialization

    """

    @classmethod
    def fromProjectionOfPointsOnToVector(cls, points, vector):
        """
        Project a point onto an axis vector

        :param points: Point
        :type points: float
        :param vector: Vector
        :type vector: vector/cVector3D/cSphere
        :return: Min/Max points of line
        :rtype: (tuple,list)
        """
        mini, maxi = minmax_cVector3D_onto_axis(points, vector)
        # iter_vertices = iter(points)
        # v = next(iter_vertices)
        # proj = v.dot(vector)
        # mini = proj
        # maxi = proj
        # for vert in iter_vertices:
        #    proj = v.dot(vector)
        #    maxi = max(maxi, proj)
        #    mini = min(mini, proj)
        return cls(mini, maxi)

    @classmethod
    def fromProjectionOfCHVerticesOnToVector(cls, ch_vertices, vector):
        """
        Project a Convex Hull vertices onto an axis vector

        :param ch_vertices: convex hull vertices
        :type ch_vertices: list of vector/cVector3D/cSphere
        :param vector: Vector
        :type vector: vector/cVector3D/cSphere
        :return: Min/Max points of line
        :rtype: (tuple,list)
        """
        mini, maxi = minmax_cVector3D_onto_axis(
            [vert.v for vert in ch_vertices], vector
        )
        # iter_vertices = iter(ch_vertices)
        # vert = next(iter_vertices)
        # proj = vert.v.dot(vector)
        # mini = proj
        # maxi = proj
        # for vert in iter_vertices:
        #    proj = vert.v.dot(vector)
        #    maxi = max(maxi, proj)
        #    mini = min(mini, proj)
        return cls(mini, maxi)

    def absolute_overlap(self, other):
        # TODO - documentation out of date?
        """ Find the overlap between two lines, p1 and p2

        p1 and p2 are defined as end points (min and max) on an axis and this algorithm tells how these are overlapping
        Here are some example cases::
            |----p1---|=|-------p2------| returns +1
            |----p1-----|   |------p2------| returns -3
            |----p1-----|------p2------| returns 0
            |----p2-----| |------p1------| returns -1
            |----p2---|======|----p1------| returns +6

        :param p1: tuple containing the min and max points of line
        :type p1: tuple, list
        :param p2: tuple containing the min and max points of line
        :type p2: tuple, list
        :return: overlap or separation of the two lines
        :rtype: float
        """
        p1min, p1max = self.minimum, self.maximum
        p2min, p2max = other.minimum, other.maximum

        if p1max > p2max:
            if p1min < p2max:
                if p1min > p2max:
                    return abs(p2max - p1min)
                else:
                    return min(abs(p1max - p2min), abs(p1min - p2max))
            else:
                return p2max - p1min
        else:
            if p1max > p2min:
                if p1min < p2min:
                    return abs(p1max - p2min)
                else:
                    return min(abs(p1max - p2min), abs(p1min - p2max))
            else:
                return p1max - p2min


from cspy.deprecated.core.tasks.internal.convex_hull.canalysis import SeparationEngine


def degree_of_overlap(p1, p2):
    """ Returns the overlap between p1 and p2 or 0.0 if they are separated. See :func:`absolute_overlap`
    """
    return max(0.0, absolute_overlap(p1, p2))


def absolute_overlap(p1, p2):
    """ Find the overlap between two lines, p1 and p2

    p1 and p2 are defined as end points (min and max) on a axis and this algorithm tells how these are overlapping
    Here are some example cases::
        |----p1---|=|-------p2------| returns +1
        |----p1-----|   |------p2------| returns -3
        |----p1-----|------p2------| returns 0
        |----p2-----| |------p1------| returns -1
        |----p2---|======|----p1------| returns +6

    :param p1: tuple containing the min and max points of line
    :type p1: tuple, list
    :param p2: tuple containing the min and max points of line
    :type p2: tuple, list
    :return: overlap or separation of the two lines
    :rtype: float
    """
    p1min, p1max = sorted(p1)
    p2min, p2max = sorted(p2)

    if p1max > p2max:
        if p1min < p2max:
            if p1min > p2max:
                return abs(p2max - p1min)
            else:
                return min(abs(p1max - p2min), abs(p1min - p2max))
        else:
            return p2max - p1min
    else:
        if p1max > p2min:
            if p1min < p2min:
                return abs(p1max - p2min)
            else:
                return min(abs(p1max - p2min), abs(p1min - p2max))
        else:
            return p1max - p2min
