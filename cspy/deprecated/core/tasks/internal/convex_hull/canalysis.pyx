# cython: profile=True
import itertools
from collections import namedtuple


from cspy.deprecated.core.models.cvector3d import cross_lists, non_parallel_vectors, non_parallel_vectors_two
from cspy.deprecated.core.models.cvector3d cimport minmax_cVector3D_onto_axis


#from cspy.deprecated.core.models import Vector3D
from cspy.deprecated.core.models.cvector3d cimport cVector3D, cSphere
from cspy.deprecated.core.models.cvector3d cimport Spheres, SphereProjection
from cspy.deprecated.core.models.cvector3d cimport Points

from cspy.deprecated.core.models.point cimport Point


import copy
cimport cython
from libcpp cimport bool

cdef extern struct SeparationAxis:
    Point v
    double magnitude
    bool initialised

cdef class SeparationVector:
    cdef public object v, magnitude
    """
    A SeparationVector object. Initialization

    :param v: the vector
    :type v: vector/cVector3D/cSphere
    :param magnitude: magnitude of the vector
    :type magnitude: float
    """
    def __init__(self, v, magnitude=None):
        """
        Initialization
        """
        self.v = v
        self.magnitude = magnitude

    def __copy__(self):
        """
        return a copy of the SeparationVector
        """
        return SeparationVector(self.v.copy(), self.magnitude)

    def __str__(self):
        """
        return the string form
        """
        return '<SeparationVector magnitude={}, v={}>'.format(self.magnitude, self.v)


class SeparatedHulls(Exception): pass
class OverlappingHulls(Exception): pass



cdef class SeparationEngine:
    cdef public object hulls, symmetry_hulls # list of hulls in asymmetric unit and containers for unit cell
    cdef public object lattice, symmetry_operations
    cdef public object ch1, ch2 # hulls 1 and 2 used for comparison
    cdef public object ch2_nfaces, ch1ch2_edge_edge, remaining_v # list of separation vectors
    cdef public list ch1_nfaces
    cdef int ch1_hash, ch2_hash
    cdef public object best_vector
    cdef double ch1_max_r, ch2_max_r # typed storage of hulls max radius 
    cdef public cVector3D best_vector_cent, old_best_vector_cent
    cdef bool stop_on_any_separation, volume_metric # control functions
    cdef cVector3D ch2_origin_centroid, ch1_centroid, ch2_centroid, active_trans
    cdef Spheres ch1_spheres, ch2_spheres
    cdef public Points ch2_translations
    cdef public SeparationAxis best_axis, last_best_axis

    def __init__(self, hulls=None, lattice=None, symmetry_operations=None, ch1=None, ch2=None, ch2_translations=None,
                 stop_on_any_separation=False, symmetry_hulls=None, volume_metric=False):
        ''' Separation Engine for periodic convex hulls

        '''
        
        self.hulls = hulls
        self.lattice = lattice
        if symmetry_hulls is None:
            self.symmetry_hulls = copy.deepcopy(hulls)
        else:
            self.symmetry_hulls = symmetry_hulls
        self.symmetry_operations = symmetry_operations
        self.ch2_translations = ch2_translations
        self.stop_on_any_separation = stop_on_any_separation
        self.ch1 = ch1
        self.ch2 = ch2
        self.reset_caches()

    def reset_caches(self):
        ''' Reset any cached data for subsequent runs '''
        #TODO - implement documentation!
        self.ch1_nfaces = []
        self.ch1_hash = 0
        self.ch2_hash = 1
        self.ch2_nfaces = None
        self.ch1ch2_edge_edge = None
        self.remaining_v = None
        self.best_axis.magnitude = 9.e99
        self.best_axis.initialised = False
        self.best_vector = SeparationVector(v=None, magnitude=float('inf'))
        self.best_vector_cent = cVector3D.__new__(cVector3D)
        self.active_trans = cVector3D.__new__(cVector3D)
        self.ch2_centroid = cVector3D.__new__(cVector3D)

    @cython.cdivision(True)
    cdef SeparationAxis new_separation_axis(self, cVector3D normalized_separation_vector,
                                     double overlap_result,
                                     cVector3D centroid_vector):
        '''Construct separation axis according to original code by DHC'''
        cdef cVector3D tv # temp vector for testing
        cdef cVector3D cv_norm # centroid vector
        cdef double cv_proj, tv_norm_cond, tv_mag
        cdef bool b_mag
        cdef SeparationAxis axis
        
        # Construct the test vector
        tv = normalized_separation_vector.dmul(overlap_result)

        # its normalisation condition
        cv_norm = centroid_vector.normalized()
        cv_proj = normalized_separation_vector.cdot(cv_norm)
        tv_norm_cond = 1./(0.0000001 + cv_proj)
        tv.i_dmul(tv_norm_cond)

        # test if magnitude is smaller than previous
        tv_mag = tv.magnitude()
        tv_norm = tv.normalized()

        axis.v.x = tv_norm.xyz[0]
        axis.v.y = tv_norm.xyz[1]
        axis.v.z = tv_norm.xyz[2]
        axis.magnitude = tv_mag
        axis.initialised = True
        return axis
 
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void determine_if_best_separation(self,
                                     cVector3D normalized_separation_vector,
                                     double overlap_result,
                                     cVector3D centroid_vector):
        ''' Determine the best separation vector to use by choosing the separation with the
            smallest magnitude'''
        cdef SeparationAxis axis

        axis = self.new_separation_axis(normalized_separation_vector,
                                        overlap_result,
                                        centroid_vector)

        if axis.magnitude < self.best_axis.magnitude and\
           axis.magnitude > 0.05:
            self.best_axis = axis
            self.best_vector_cent.copy_from(centroid_vector)

    cdef int test_vector(self, cVector3D vector, cVector3D inter_hull_vector):
        '''Project all points on hull onto vector and test if overlapping'''
        cdef double mini, maxi, proj
        cdef cSphere v
        cdef double p1min, p1max, p2min, p2max, overlap
        cdef SphereProjection proj1
        cdef double proj_trans

        proj_trans = (<cVector3D> self.active_trans).cdot((<cVector3D> vector))

        proj1 = self.ch1_spheres.projection(vector)
        p1min = proj1.mini
        p1max = proj1.maxi

        p2min = 9e9
        p2max = -9e9
        proj2 = self.ch2_spheres.projection(vector)
        p2min = proj2.mini
        p2max = proj2.maxi

        p1min += 0.6
        p1max += -0.6
        p2min += proj_trans+0.6
        p2max += proj_trans-0.6

        if p1max > p2max: 
            if p1min < p2max:
                if p1min > p2min: 
                    overlap = abs(p2max-p1min)
                else: 
                    overlap = min(abs(p1max-p2min), abs(p1min-p2max))
            else: 
                overlap = p2max-p1min
        else:
            if p1max > p2min:
                if p1min < p2min:
                    overlap = abs(p1max-p2min)
                else: 
                    overlap = min(abs(p1max-p2min), abs(p1min-p2max))
            else:
                overlap = p1max-p2min

        if self.stop_on_any_separation and overlap <= 0.:
            return 1
        else:
            self.determine_if_best_separation(vector, overlap, inter_hull_vector)
            return 0


    @cython.nonecheck(False)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef int process_separation_vectors(self):
        ''' For the list of separation vectors determine the best separation vector '''
        cdef Py_ssize_t i
        cdef double vt, m, dn 
        cdef cVector3D inter_hull_vector, vector
        cdef int r, n, x, y, z

        #test centroid distance
        inter_hull_vector = cVector3D.__new__(cVector3D)
        inter_hull_vector.xyz[0] = self.ch2_centroid.xyz[0] - self.ch1_centroid.xyz[0]
        inter_hull_vector.xyz[1] = self.ch2_centroid.xyz[1] - self.ch1_centroid.xyz[1]
        inter_hull_vector.xyz[2] = self.ch2_centroid.xyz[2] - self.ch1_centroid.xyz[2]
        m = (<double> inter_hull_vector.magnitude())
        if m < 0.1:
            return -1

        # Inter hull vector test
        vector = inter_hull_vector.normalized()
        r = self.test_vector(vector, inter_hull_vector)
        if r == 1:
            return r

        # ch1 faces test
        for i in range(len(self.ch1_nfaces)):
            vector = self.ch1_nfaces[i]
            r = self.test_vector(vector, inter_hull_vector)
            if r == 1:
                return r

        # ch2 faces test
 
        # ch2 caching
        if self.ch2_nfaces is None:
            if self.ch2_hash == self.ch1_hash:
                self.ch2_nfaces = []
            else:
                self.ch2_nfaces = non_parallel_vectors_two(self.ch1_nfaces, self.ch2.np_faces)
        # ch2 testing
        for i in range(len(self.ch2_nfaces)):
            vector = self.ch2_nfaces[i]
            r = self.test_vector(vector, inter_hull_vector)
            if r == 1:
                return r

        # edge edge test

        # edge edge caching
        if self.ch1ch2_edge_edge is None:
            self.ch1ch2_edge_edge = cross_lists([e.vec() for e in self.ch1.edges], [e.vec() for e in self.ch2.edges])
        if self.remaining_v is None:
            self.remaining_v = non_parallel_vectors_two(self.ch1_nfaces, self.ch1ch2_edge_edge)
            self.remaining_v = non_parallel_vectors_two(self.ch2_nfaces, self.remaining_v)
            self.remaining_v = non_parallel_vectors(self.remaining_v)

        # edge edge testing
        for i in range(len(self.remaining_v)):
            vector = self.remaining_v[i]
            r = self.test_vector(vector, inter_hull_vector)
            if r == 1:
                return r

        # 0 if overlapping hulls
        return 0


    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void determine_best_separation_vector_for_dimer(self):
        ''' Determine the best separation vector for a pair of convex hulls '''
        cdef int return_status
        self.last_best_axis = self.best_axis
        self.old_best_vector_cent = self.best_vector_cent
        return_status = self.process_separation_vectors() #self.separation_vector_generator())
        if return_status == 1:
            self.best_axis = self.last_best_axis
            self.best_vector_cent = self.old_best_vector_cent

    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void determine_best_separation_vector_for_translations(self):
        ''' Determine the best separation vector for a pair of convex hulls (ch1 and ch2)
            while considering translated copies of ch2
        '''
        cdef cVector3D tmp, sep
        cdef double max_serparation
        tmp = cVector3D.__new__(cVector3D)
        sep = cVector3D.__new__(cVector3D)

        max_separation = (self.ch1_max_r+self.ch2_max_r)**2

        # determine if hulls are the same, if so skip the first translations which should be 000
        s = 0
        if self.ch1_hash == self.ch2_hash:
            s = 1

        tmp.xyz[0] = self.ch2_origin_centroid.xyz[0]-self.ch1_centroid.xyz[0]
        tmp.xyz[1] = self.ch2_origin_centroid.xyz[1]-self.ch1_centroid.xyz[1]
        tmp.xyz[2] = self.ch2_origin_centroid.xyz[2]-self.ch1_centroid.xyz[2]

        for i in range(s, self.ch2_translations.N):
            # checks if ch1 and ch2 are the same and no translations has been applied
            self.active_trans.xyz[0] = self.ch2_translations.vs[i].x
            self.active_trans.xyz[1] = self.ch2_translations.vs[i].y
            self.active_trans.xyz[2] = self.ch2_translations.vs[i].z
            sep.xyz[0] = tmp.xyz[0] + self.active_trans.xyz[0]
            sep.xyz[1] = tmp.xyz[1] + self.active_trans.xyz[1]
            sep.xyz[2] = tmp.xyz[2] + self.active_trans.xyz[2]
            
            if sep.cdot(sep) < max_separation:
                self.ch2_centroid.xyz[0] = self.ch2_origin_centroid.xyz[0]+self.active_trans.xyz[0]
                self.ch2_centroid.xyz[1] = self.ch2_origin_centroid.xyz[1]+self.active_trans.xyz[1]
                self.ch2_centroid.xyz[2] = self.ch2_origin_centroid.xyz[2]+self.active_trans.xyz[2]
                # test the dimer with this translation
                self.determine_best_separation_vector_for_dimer()

    def apply_symmetry_hull_i(self, i, Rmat, Tvec):
        ''' Return the symmetry generated version of convex hull i ''' 
        for iv, vertex in enumerate(self.hulls[i].vertices):
            self.symmetry_hulls[i].vertices[iv].v.copy_from(vertex.v.vec_mat_mul(Rmat)+Tvec)
        for iv, np_face in enumerate(self.hulls[i].np_faces):
            self.symmetry_hulls[i].np_faces[iv].copy_from(np_face.vec_mat_mul(Rmat))
        for iv, edge_v in enumerate(self.hulls[i].edge_vs):
            self.symmetry_hulls[i].edge_vs[iv] = edge_v.vec_mat_mul(Rmat)
        self.symmetry_hulls[i]._centroid.copy_from(self.hulls[i].centroid.vec_mat_mul(Rmat)+Tvec)

    def symmetry_hulls_generator(self):
        ''' Generate all symmetry generated copies of a convex hull '''
        # another generator to produce the ch2 hulls as needed
        for op in self.symmetry_operations:
            Rmat, Tvec = op.lattice_specific_symmetry_operation(self.lattice)
            for i in range(len(self.symmetry_hulls)):
                self.apply_symmetry_hull_i(i, Rmat, Tvec)
                yield self.symmetry_hulls[i]

 
    def determine_best_separation_vector_for_crystal(self):
        ''' Determine the best separation vector for a pair of convex hulls (ch1 and ch2)
            while considering both symmetry generated and translated copies of ch2
        '''
        for ich1, ch1 in enumerate(self.hulls):
            self.ch1 = ch1
            self.ch1_hash = ich1
            self.ch1_nfaces = self.ch1.np_faces
            self.ch1_centroid = (<cVector3D> self.ch1._centroid)
            self.ch1_spheres = Spheres([vert.v for vert in self.ch1.vertices])
            self.ch1_max_r = self.ch1.max_r
            for ich2, ch2 in enumerate(self.symmetry_hulls_generator()):
                self.ch2 = ch2
                self.ch2_spheres = Spheres([vertex.v for vertex in self.ch2.vertices])
                self.ch2_origin_centroid = self.ch2._centroid.copy()
                self.ch2_hash = ich2
                self.ch2_nfaces = None
                self.ch2_max_r = self.ch2.max_r
                self.ch1ch2_edge_edge = None
                self.remaining_v = None
                self.determine_best_separation_vector_for_translations()
        if self.best_axis.initialised:
            self.best_vector.v = cVector3D(self.best_axis.v.x,
                                           self.best_axis.v.y,
                                           self.best_axis.v.z)
            self.best_vector.magnitude = self.best_axis.magnitude


