"""

Convex Hull Generation
======================

"""

import logging

logger = logging.getLogger("cspy.core.tasks.internal.convex_hull.generation")

from cspy.deprecated.core.models.convex_hull import Hull
from cspy.deprecated.core.tasks.internal.vector3d.shapes import (
    tetrahedron,
    sandwich,
    box,
    cuboid,
    octahedron,
    solid,
)
from cspy.deprecated.core.tasks.internal.vector3d.vector3d import is_list_planar, is_list_collinear
from cspy.deprecated.core.models.cvector3d import cSphere


class MolecularConvexHullEngine(object):
    """
    Class: MolecularConvexHullEngine
    """

    def __init__(self):
        """
        Initialize a convex hull object
        """
        pass

    @staticmethod
    def generate_convex_hull(atoms):
        """
        Generate a convex hull based on atoms
        Input: atoms
        Output: Convex Hull
        """
        if len(atoms) == 0:
            raise NotImplementedError("Convex hull cannot be calculated with no atoms")
        logger.debug("There are %s atoms", len(atoms))
        tmpc = cSphere(
            atoms[0].xyz[0],
            atoms[0].xyz[1],
            atoms[0].xyz[2],
            atoms[0].element.vdw_radius,
        )
        ch_sites = [
            cSphere(atom.xyz[0], atom.xyz[1], atom.xyz[2], atom.element.vdw_radius)
            for atom in atoms
        ]
        if len(ch_sites) == 1:
            # make a tetrahedron
            ch_sites = tetrahedron(ch_sites[0])
            ch_type = "tetrahedron"
            ch_mod = 1
        elif is_list_collinear(ch_sites):
            # check if the molecule is linear
            # ch_sites = octahedron(ch_sites)
            ch_sites = cuboid(ch_sites)
            ch_mod = len(atoms)
        #elif is_list_planar(ch_sites):
        #    # make a sandwich
        #    ch_sites = sandwich(ch_sites)
        #    #ch_sites = solid(ch_sites)
        #    ch_mod = len(atoms)
        else:
            # just use ch_sites
            ch_sites = ch_sites
            ch_mod = len(atoms)

        ch = Hull(ch_sites)
        MolecularConvexHullEngine.set_vertex_atom_labels(
            ch.vertices, ch_sites, atoms, ch_mod=ch_mod
        )

        c = ch.centroid
        ch.max_r = max(
            [(vert.v - c).magnitude() + vert.v.radius for vert in ch.vertices]
        )

        return ch

    @staticmethod
    def set_vertex_atom_labels(vertices, all_sites, atoms, ch_mod=None):
        """
        Set convex hull atom labels
        Input: convex hull vertices, convex hull sites, atoms, convex hull modifier?
        """
        if ch_mod is None:
            ch_mod = len(atoms)
        atom_labels_on_hull = []
        for vert in vertices:
            v = vert.v
            ic = all_sites.index(v)
            ia = ic % ch_mod
            atom_label = atoms[ia].label
            vert.atom_label = atom_label
