import unittest

from cspy.deprecated.core.tasks.internal.convex_hull.generation import MolecularConvexHullEngine
from cspy.deprecated.core.models import Vector3D, Atom


class ConvexHullGenerationTestCase(unittest.TestCase):
    def test_init(self):
        eng = MolecularConvexHullEngine()
        assert isinstance(eng, MolecularConvexHullEngine)

    def test_generate_convex_hull(self):
        atoms = [Atom("H", Vector3D(1, 0, 0))]
        ch = MolecularConvexHullEngine.generate_convex_hull(atoms)
        self.assertEqual(len(ch.vertices), 4)
        self.assertTrue(all([v.atom_label == "H" for v in ch.vertices]))

        atoms = [
            Atom("H", Vector3D(1, 0, -0.5)),
            Atom("H", Vector3D(-0.5, -0.5, -0.5)),
            Atom("H", Vector3D(-0.5, +0.5, -0.5)),
            Atom("C", Vector3D(0, 0, 0)),
            Atom("H", Vector3D(0, 0, 1)),
        ]
        ch = MolecularConvexHullEngine.generate_convex_hull(atoms)
        self.assertEqual(len(ch.vertices), 4)
        self.assertTrue(all([v.atom_label == "H" for v in ch.vertices]))

        atoms = [
            Atom("H", Vector3D(1, 0, -0.5)),
            Atom("H", Vector3D(-0.5, -0.5, -0.5)),
            Atom("H", Vector3D(-0.5, +0.5, -0.5)),
            Atom("C", Vector3D(0, 0, 0)),
            Atom("C", Vector3D(0, 0, 1)),
            Atom("H", Vector3D(-1, 0, 1.5)),
            Atom("H", Vector3D(0.5, -0.5, 1.5)),
            Atom("H", Vector3D(0.5, +0.5, 1.5)),
        ]
        ch = MolecularConvexHullEngine.generate_convex_hull(atoms)
        self.assertEqual(len(ch.vertices), 6)
        self.assertTrue(all([v.atom_label == "H" for v in ch.vertices]))

    @unittest.skip("unimplemented test")
    def test_set_vertex_atom_labels(self):
        pass
