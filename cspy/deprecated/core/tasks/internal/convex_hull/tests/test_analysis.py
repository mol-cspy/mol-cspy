from __future__ import print_function
import unittest

# from cspy.deprecated.core.tasks.internal.convex_hull.generation import MolecularConvexHullEngine
# from cspy.deprecated.core.tasks.internal.convex_hull.analysis import degree_of_overlap
# from cspy.deprecated.core.tasks.internal.convex_hull.analysis import are_overlapping_hulls
# from cspy.deprecated.core.models import Vector3D, Atom


class TestSeparationVector(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_initialiation(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_copy_vector(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_string_form(self):
        # TODO - implement this!
        pass


class TestFromProjectionOfCHVerticesOnToVector(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_projection(self):
        # TODO - implement this! (probably do mock test to ensure two methods would be called)
        pass


class TestMinMaxC(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_fromProjectionOfPointsOnToVector(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_fromProjectionOfCHVerticesOnToVector(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_absolute_overlap(self):
        # TODO - implement this!
        pass


class TestSeperationEngine(unittest.TestCase):
    @unittest.skip("unimplemented test")
    def test_initialisation(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_initialisation_with_symmetry_hull(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_initialisation_with_no_symmetry_hull(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_reset_caches_called_upon_object_initialisation(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_reset_caches(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_simple_dimer(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_translate_dimer(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_symmetry_hulls(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_attempt_raise_on_stop_conditions(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_determine_if_best_separation(self):
        # TODO - implement this! (this might need to be done after the method being refactored a bit)
        pass

    @unittest.skip("unimplemented test")
    def test_process_separation_vectors(self):
        # TODO - implement this! (this might need to be done after the method being refactored a bit)
        pass

    @unittest.skip("unimplemented test")
    def test_determine_best_separation_vector_for_dimer(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_determine_best_separation_vector_for_translations(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_determine_best_separation_vector_for_crystal(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_separation_vector_generator(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_edge_edge_separation_vector_generator(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_symmetry_hull_generator(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_store_ch2_origin(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_apply_ch2_translation(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_reset_ch2_translation(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_apply_symmetry_hull_i(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_reset_symmetry_hull_i(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_degree_of_overlap(self):
        # TODO - implement this!
        pass

    @unittest.skip("unimplemented test")
    def test_absolute_overlap(self):
        # TODO - implement this!
        pass


# class ConvexHullAnalysisTestCase(unittest.TestCase):
#    def test_edge_separation(self):
#        atoms = [Atom('H', Vector3D(1, 0, 0))]
#        ch1 = MolecularConvexHullEngine.generate_convex_hull(atoms)
#
#        atoms = [Atom('H', Vector3D(1.1,0.1, 0))]
#        ch2 = MolecularConvexHullEngine.generate_convex_hull(atoms)
#
#        vectors = list(get_possible_separation_vectors_from_edges(ch1, ch2))
#        self.assertEqual(len(vectors), 30)
#
#        vectors = list(get_possible_separation_vectors(ch1, ch2))
#        self.assertEqual(len(vectors), 38)
#
#        vectors = get_unique_separation_vectors(ch1, ch2)
#        self.assertEqual(len(vectors), 7)
#
#        mini, maxi = minmax_vertices_projection_onto_vector(ch1.vertices, Vector3D(1, 0, 0))
#        self.assertEqual(mini, 0.75)
#        self.assertEqual(maxi, 1.25)
#
#        doo_list = [0.46188021535170065,0.5773502691896257,0.5773502691896257,0.46188021535170054]
#        for vector, doo_actual in zip(vectors, doo_list):
#            minmax1 = minmax_vertices_projection_onto_vector(ch1.vertices, vector)
#            minmax2 = minmax_vertices_projection_onto_vector(ch2.vertices, vector)
#
#            print(minmax1, minmax2)
#            doo = degree_of_overlap(minmax1, minmax2)
#            self.assertAlmostEqual(doo, doo_actual)
#
#    def test_are_overlapping_hulls(self):
#        atoms = [Atom('H', Vector3D(1, 0, 0))]
#        ch1 = MolecularConvexHullEngine.generate_convex_hull(atoms)
#        atoms = [Atom('H', Vector3D(1.1, 0, 0))]
#        ch2 = MolecularConvexHullEngine.generate_convex_hull(atoms)
#        self.assertTrue(are_overlapping_hulls(ch1,ch2), 'Hulls are shifted by 0.1')
#
#        atoms = [Atom('H', Vector3D(1.5, 0.0, 0))]
#        ch2 = MolecularConvexHullEngine.generate_convex_hull(atoms)
#        self.assertFalse(are_overlapping_hulls(ch1,ch2), 'Hulls are shifted by 0.5 (twice tetrahedron length)')
