import unittest
import numpy

from cspy.deprecated.core.models import Lattice, SpaceGroup, Vector3D
from cspy.deprecated.core.models.convex_hull import Hull
from cspy.deprecated.core.models.space_group_operation import SpaceGroupOperation

# from cspy.deprecated.core.tasks.internal.convex_hull.periodic_analysis import smallest_overlapping_axis
from cspy.deprecated.core.tasks.internal.vector3d.shapes import tetrahedron

# class CHPeriodicAnalysisTestCase(unittest.TestCase):
#    def test_smallest_overlapping_axis(self):
#        l = Lattice(1,1,1,90,90,90)
#        sg = SpaceGroup([SpaceGroupOperation('x+0.25,y,z')])
#        ch = Hull(tetrahedron())
#        mag, direction, cent_axis = smallest_overlapping_axis([ch], l, sg)
#
#        self.assertEqual(mag, 0.25)
#        numpy.testing.assert_array_almost_equal(direction, Vector3D(1., 0., 0.))
#
#
#        sg = SpaceGroup([SpaceGroupOperation('x+0.55,y,z')])
#        ch = Hull(tetrahedron())
#        mag, direction, cent_axis = smallest_overlapping_axis([ch], l, sg, cell_cutoff=0)
#
#        self.assertEqual(mag, float('inf'))
#        self.assertEqual(direction, None)
#
#        l = Lattice(1,1,1,90,90,90)
#        sg = SpaceGroup([SpaceGroupOperation('x+0.25,y,z'),
#                         SpaceGroupOperation('x+1.25,y,z')])
#        ch = Hull(tetrahedron())
#        mag, direction, cent_axis = smallest_overlapping_axis([ch], l, sg)
#
#        self.assertEqual(mag, 0.25)
#        numpy.testing.assert_array_almost_equal(direction, Vector3D(1., 0., 0.))
