import unittest
from cspy.deprecated.core.tasks.internal.vector3d.shapes import box
from cspy.deprecated.core.tasks.internal.convex_hull.analysis import SeparationEngine
from cspy.deprecated.core.tasks.internal.crystal.neighbours import neighbouring_translations
from cspy.deprecated.core.models.convex_hull import Hull
from cspy.deprecated.core.models import Vector3D, SymmetryOperation, Lattice


# class SeparationEngineTestCase(unittest.TestCase):
#     def test_simple(self):
#         sites = box(Vector3D(0.,0.,0.))
#         ch1 = Hull(box(Vector3D(0.0,0.,0.), length=1.0))
#         ch2 = Hull(box(Vector3D(0.5,0.,0.), length=1.0))
#
#         seng = SeparationEngine(ch1=ch1, ch2=ch2)
#         seng.determine_best_dimer_axis()
#         self.assertEqual(seng.best_vector.magnitude, 0.5)
#         self.assertEqual(seng.best_vector.v, Vector3D(1.0, 0.0, 0.0))
#
#     def test_simple_translated(self):
#         sites = box(Vector3D(0.,0.,0.))
#         ch1 = Hull(box(Vector3D(0.0,0.,0.), length=1.0))
#         ch2 = Hull(box(Vector3D(0.5,0.,0.), length=1.0))
#
#         seng = SeparationEngine(ch1=ch1, ch2=ch2, ch2_translations=[Vector3D(0.1,0,0)])
#         seng.determine_best_translated_dimer_axis()
#         self.assertEqual(seng.best_vector.magnitude, 0.4)
#         self.assertEqual(seng.best_vector.v, Vector3D(1.0, 0.0, 0.0))
#
#     def test_simple_periodic(self):
#         sites = box(Vector3D(0.,0.,0.))
#         ch1 = Hull(box(Vector3D(0.0,0.,0.), length=1.0))
#         ch2 = Hull(box(Vector3D(0.5,0.,0.), length=1.0))
#
#         l = Lattice(5, 5, 5, 90., 90., 90.)
#
#         so = [SymmetryOperation('x,y,z'),
#               SymmetryOperation('-x,-y,-z')]
#         trans = neighbouring_translations(3, l)
#
#         seng = SeparationEngine(hulls=[ch1, ch2], lattice=l, symmetry_operations=so, ch2_translations=trans, stop_on_any_separation=True)
#         seng.determine_best_periodic_dimer_axis()
#         self.assertEqual(seng.best_vector.magnitude, 0.5)
#         self.assertEqual(seng.best_vector.v, Vector3D(1.0, 0.0, 0.0))
#
#     def test_simple_periodic2(self):
#         sites = box(Vector3D(0.,0.,0.))
#         ch1 = Hull(box(Vector3D(0.25,0.,0.), length=1.0))
#
#         l = Lattice(5, 5, 5, 90., 90., 90.)
#
#         so = [SymmetryOperation('x,y,z'),
#               SymmetryOperation('-x,-y,-z')]
#         trans = neighbouring_translations(3, l)
#
#         seng = SeparationEngine(hulls=[ch1], lattice=l, symmetry_operations=so, ch2_translations=trans, stop_on_any_separation=True)
#         seng.determine_best_periodic_dimer_axis()
#         self.assertEqual(seng.best_vector.magnitude, 0.5)
#         self.assertEqual(seng.best_vector.v, Vector3D(1.0, 0.0, 0.0))
#
#     def test_simple_periodic3(self):
#         sites = box(Vector3D(0.,0.,0.))
#         ch1 = Hull(box(Vector3D(0.25,0.25,0.), length=1.0))
#
#         l = Lattice(5, 5, 5, 90., 90., 90.)
#
#         so = [SymmetryOperation('x,y,z'),
#               SymmetryOperation('-x,-y,-z')]
#         trans = neighbouring_translations(3, l)
#
#         l.parameter_lock()
#         seng = SeparationEngine(hulls=[ch1], lattice=l, symmetry_operations=so, ch2_translations=trans, stop_on_any_separation=True)
#         seng.determine_best_periodic_dimer_axis()
#         l.parameter_release()
#         self.assertAlmostEqual(seng.best_vector.magnitude, 0.5)
#         self.assertEqual(seng.best_vector.v, Vector3D(0.0, -1.0, 0.0))
