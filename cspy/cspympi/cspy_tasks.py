import copy
import logging
import time
from collections import namedtuple
from enum import IntEnum
from cspy import Molecule
from cspy.chem.multipole import DistributedMultipoles
from cspy.crystal import Crystal
from cspy.crystal.generate_crystal import CrystalGenerator
from cspy.db.key import CspDatabaseId
from cspy.deprecated.core.io.res import ResCrystalMapper
from cspy.deprecated.core.io.res import ResFileWriter
from cspy.deprecated.core.io.xyz import XYZFileReader
from cspy.deprecated.core.models import Crystal as CrystalOld
from cspy.deprecated.core.models import Lattice as LatticeOld
from cspy.deprecated.core.resources.crystallographic_space_groups \
    import CrystallographicSpaceGroups
from cspy.deprecated.core.tasks.internal.landscape_generator.crystal \
    import Convergence
from cspy.deprecated.core.tasks.internal.landscape_generator.crystal \
    import CrystalLandscapeGenerator
from cspy.minimize import CompositeMinimizer
from cspy.minimize import single_point_evaluation
from cspy.ml.descriptors import PowderPattern
from .worker import Worker


LOG = logging.getLogger(__name__)


GeneratedStructure = namedtuple(
    "GeneratedStructure", "name spacegroup trial_number file_content"
)
MinimizedStructure = namedtuple(
    "MinimizedStructure",
    "id spacegroup trial_number minimization_step energy density file_content xrd time",
)


class CSPyTaskTag(IntEnum):
    QR = 1
    OPT = 2
    PER_OPT = 3


class CSPyWorker(Worker):

    def __init__(self, data):
        """The CSP worker class.

        Parameters
        ----------
        data: dict
            Dictionary containing all the data required to run the
            structure generation or minimisation tasks.
        """
        super().__init__()
        self.data = data
        self.task_name = "QR"

    def calculate(self, data):
        """Override of the calculate method for CSPy tasks.

        Parameters
        ----------
        data: tuple
            Tuple containing the task tag to be done and args for
            that task.

        Returns
        -------
        tuple
            Tuple of the results which contains the task tag, results
            and time taken.
        """

        task, args, overrides = data
        results = None

        if task == CSPyTaskTag.QR and not self.data["clg_old"]:
            results = self.structure_generation(args)
        elif task == CSPyTaskTag.QR and self.data["clg_old"]:
            results = self.structure_generation_old(args)
        elif task == CSPyTaskTag.OPT:
            results = self.minimize_structure(args, overrides)

        return results

    def structure_generation(self, args):
        t1 = time.time()
        spacegroup, (min_seed, max_seed), name = args
        LOG.debug("min seed: %d, max seed: %d", min_seed, max_seed)
        molecules = [
            Molecule.from_xyz_string(x) for x in self.data["asymmetric_unit"]
        ]
        clg = CrystalGenerator(molecules, spacegroup, nudge=self.data["nudge"],  adaptcell=self.data["adaptcell"], asi=self.data["asi"])
        crystals = []
        for seed in range(min_seed, max_seed):
            generated = clg.generate(seed)
            if generated is None:
                crystals.append(None)
                continue
            res = generated.to_shelx_string(titl=str(
                CspDatabaseId.from_components(name, "QR", spacegroup, seed, 0)
            ))
            crystals.append(GeneratedStructure(
                    name=name, trial_number=seed, spacegroup=spacegroup,
                    file_content=res
            ))
        return CSPyTaskTag.QR, (spacegroup, crystals), time.time() - t1

    def structure_generation_old(self, args):
        t1 = time.time()
        spacegroup, (min_seed, max_seed), name = args
        LOG.debug("min seed: %d, max seed: %d", min_seed, max_seed)
        asymmetric_unit = [
            XYZFileReader(file_content=x).read() for x in
            self.data["asymmetric_unit"]
        ]
        crystal = CrystalOld(
            asymmetric_unit=asymmetric_unit,
            lattice=LatticeOld(20.0, 20.0, 20.0, 90, 90, 90),
            space_group=CrystallographicSpaceGroups.get(spacegroup),
        )
        mapper = ResCrystalMapper()
        crystals = []
        for seed in range(min_seed, max_seed):
            convergence = Convergence(number_valid_structures=1, max_trials=1)
            clg = CrystalLandscapeGenerator(
                model=copy.deepcopy(crystal), convergence=convergence
            )
            clg.run_index(seed)
            accepted = clg.landscape.accepted_objs
            if len(accepted) == 0:
                crystals.append(None)
                continue
            content = mapper.map_to_content(accepted[0].crystal)
            content._replace(TITL=str(
                CspDatabaseId.from_components(name, self.task_name, spacegroup, seed, 0)
            ))
            res = ResFileWriter(
                file_content=content, output_location="").res_string_form
            crystals.append(GeneratedStructure(
                name=name, trial_number=seed, spacegroup=spacegroup,
                file_content=res
            ))
        return CSPyTaskTag.QR, (spacegroup, crystals), time.time() - t1

    def minimize_structure(self, structure, overrides):
        from cspy.configuration import configure, CONFIG

        t1 = time.time()
        if isinstance(structure, list):
            structure = structure[0]
        name = structure.name
        sg = structure.spacegroup
        trial_number = structure.trial_number
        charges = DistributedMultipoles.from_dma_string(self.data["charges"])
        multipoles = DistributedMultipoles.from_dma_string(self.data["multipoles"])
        axis = self.data["axis"]
        bondlength_cutoffs = self.data["bondlength_cutoffs"]
        minimization_settings = self.data["minimization"]
        for key in overrides.keys():
            minimization_settings[key] = overrides[key]
        check_spe = self.data["check_single_point_energy"]
        configure(minimization_settings)
        crystal_info = {"spacegroup" : sg}

        minimizer = CompositeMinimizer.from_defaults(
            charges=charges, axis=axis, multipoles=multipoles,
            bondlength_cutoffs=bondlength_cutoffs, keep_files=self.data["keep_files"], 
            crystal_info = crystal_info
        )

        structure = Crystal.from_shelx_string(structure.file_content)
        structure_id = CspDatabaseId.from_components(name, self.task_name, sg, trial_number, 0)
        res = structure.to_shelx_string(titl=structure_id)
        crystals = [
            MinimizedStructure(
                id=structure_id,
                energy=float("nan"),
                density=float("nan"),
                spacegroup=sg,
                trial_number=trial_number,
                time=float("nan"),
                xrd=None,
                file_content=res,
                minimization_step=0,
            )
        ]
        minimized_crystals = minimizer(structure)
        valid = False

        for minimization_step, crystal in enumerate(minimized_crystals, start=1):
            if isinstance(crystal, str):
                valid = crystal
            else:
                structure_id = CspDatabaseId.from_components(
                    name, self.task_name, sg, trial_number, minimization_step
                )
                try:
                    energy = crystal.properties["lattice_energy"]
                except KeyError:
                    # Labelled as final_energy for dftb calculations - consider refactor
                    energy = crystal.properties["final_energy"]
                density = crystal.properties["density"]
                mtime = crystal.properties["minimization_time"]
                res = crystal.to_shelx_string(titl=f"{structure_id} {energy} {density}")
                xrd = None
                if minimization_step == minimizer.step_count:
                    check_through = True
                    if check_spe:
                        energy_check, _ = single_point_evaluation(
                            crystal,
                            multipoles,
                            axis,
                            bondlength_cutoffs=bondlength_cutoffs,
                            potential=self.data["minimization"]["neighcrys"]["potential"],
                            name=name,
                            **self.data["minimization"]["dmacrys"],
                        )
                        if abs(energy - energy_check) > 10.0:
                            check_through = False
                            LOG.info(
                                "Different energy from single point evaluation id %s, "
                                "energy %s, energy_check %s",
                                structure_id, energy, energy_check
                            )
                    if check_through:
                        valid = True
                        pp = PowderPattern.from_cif_string(crystal.to_cif_string())
                        if pp is not None:
                            xrd = pp.pattern

                crystals.append(
                    MinimizedStructure(
                        id=structure_id,
                        energy=energy,
                        density=density,
                        spacegroup=sg,
                        trial_number=trial_number,
                        time=mtime,
                        xrd=xrd,
                        file_content=res,
                        minimization_step=minimization_step,
                    )
            )
        return CSPyTaskTag.OPT, (valid, crystals), time.time() - t1
