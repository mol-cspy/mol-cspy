import logging
import numpy as np
import time
import logging
from ccdc.crystal import PackingSimilarity
from ccdc.crystal import Crystal as ccdc_Crystal
from cspy.crystal import Crystal as cspy_Crystal
#from cspy.datastore import CspDataStore
from cspy.db.datastore import CspDataStore
from multiprocessing import Pool
from collections import defaultdict


LOG = logging.getLogger(__name__)


def res_to_cif(res_string):
    """Change res string to cif string since ccdc could not read res string. If
    ccdc is installed in another conda environment rather than cspy, this part
    need to be changed"""
    c = cspy_Crystal.from_shelx_string(res_string)
    return c.to_cif_string()

def mercury_compack(
    reference_structure, 
    comparison_structures, 
    nmolecules=30, 
    start_batch=0,
):
    """Compare a list of structures in comparison_structures to single
    reference_struture"""
    similarity_engine = PackingSimilarity()
    BIG_RMS = 1e10
    n = 6

    results = []
    #for i, reference in enumerate(reference_structures):
    c_i = ccdc_Crystal.from_string(reference_structure, format='cif')
    for j, comparison in enumerate(comparison_structures, start=start_batch):
        c_j = ccdc_Crystal.from_string(comparison, format='cif')

        #Number of nmolecules to be matched is iterated from small to large
        #to rule out pairs structures with few number of molecules matched. This
        #increase the comparison efficiency by a lot.
        while n < nmolecules:
            similarity_engine.settings.packing_shell_size = n
            compare = similarity_engine.compare(c_i, c_j)
            if compare.nmatched_molecules < n:
                results.append((j, BIG_RMS))
                break
            #n = n * 2
            n = round(n * 1.618)
        else:
            similarity_engine.settings.packing_shell_size = nmolecules
            compare = similarity_engine.compare(c_i, c_j)
            if compare.nmatched_molecules >= nmolecules:
                results.append((j, compare.rmsd))
            else:
                results.append((j, BIG_RMS))
    return results


def read_equivalent_table(ds):
    """Read existing equivalent_table from previous clustering"""
    equivalent_table = defaultdict(list)
    for unique_id, equivalent_id in ds.query(
        "select unique_id, equivalent_id from equivalent_to"
    ):
        equivalent_table[unique_id].append(equivalent_id)
    if not equivalent_table:
        LOG.warning("%s: no existing equivalent table")
    return equivalent_table


def iterative_compack_batch(ref, comp, nprocs, nmolecules=30):
    """Multiprocessing pair comparison. Pool.map had some wired issue with a
    long list on iridis 5. One could change the code to use pool.map after
    testing pool.map."""
    length = len(comp)
    chunksize = min(10000, int(length / nprocs / nprocs) + 1)
    if length % chunksize == 0:
        chunknum = int(length / chunksize)
    else:
        chunknum = int(length / chunksize) + 1
    results = []
    pool = Pool(processes=nprocs)
    for i in range(chunknum):
        start = i * chunksize
        end = (i + 1) * chunksize
        LOG.debug("Start %s to %s structures", start, end)
        results.append(
            pool.apply_async(
                func=mercury_compack,
                args=(ref, comp[start:end], nmolecules, start,),
            )
        )
    pool.close()
    pool.join()
    del pool

    rmsds = {}
    for result in results:
        for j, rmsd in result.get():
            rmsds[j] = rmsd
    return rmsds


def structure_rows(ds):
    LOG.info("%s: loading crystals, descriptors", ds.filename)
    ids = []
    eds = []
    contents = []
    for cid, sg, density, energy, content in ds.final_minimizations():
        ids.append(cid)
        eds.append((energy, density))
        contents.append(res_to_cif(content))
    LOG.debug("%s: loaded %d descriptors", ds.filename, len(ids))

    if len(ids) == 0:
        LOG.warning("%s: no structures retrieved")
    return (
        ids,
        np.array(eds, dtype=np.float64),
        contents,
    )


def find_duplicates_compack(dbname, ids, eds, contents, args):
    duplicates = {}
    LOG.info("%s: clustering %d structures using compack", dbname, len(ids))
    equivalent_to = np.ones(len(ids), dtype=int) * -1
    for i in range(len(ids)):
        LOG.debug("Reference structure %s (%d)", ids[i], i)
        if equivalent_to[i] > -1:
            LOG.debug(
                "%s (%d) == %s (%d), skipping",
                ids[i],
                i,
                ids[equivalent_to[i]],
                equivalent_to[i],
            )
            continue
        ed = eds[i]
        ed_a = np.abs(np.array(eds) - ed)
        idxs = np.where(
            (ed_a[:, 0] < args.cluster_energy_threshold)
            & (ed_a[:, 1] < args.cluster_density_threshold)
            & (equivalent_to < 0)
        )[0]
        idxs = idxs[idxs > i]
        if len(idxs) == 0:
            LOG.debug(
                "%s (%d) had no structures in energy/density window for comparison",
                ids[i],
                i,
            )
            duplicates[i] = []
            continue
        LOG.info("Comparing %d structures to %s", len(idxs), ids[i])
        time1 = time.time()
        rmsds = iterative_compack_batch(
            contents[i],
            [contents[j] for j in idxs],
            args.nprocs,
            nmolecules=args.cluster_nmolecules,
        )
        LOG.debug("RMSD: %s", rmsds)
        dups = [idxs[j] for j, v in rmsds.items() if v < args.cluster_rms_threshold]
        if len(dups) > 0:
            equivalent_to[dups] = i
        LOG.info(
            "Found %d equivalent structures to %s by %s seconds", 
            len(dups), ids[i], time.time()-time1
        )
        duplicates[i] = dups
        LOG.info(
            "%d structures remaining", len(np.where(equivalent_to[i + 1:] < 0)[0])
        )

    LOG.info("%s: %d unique structures from compack", dbname, len(duplicates))
    duplicates = {ids[k]: [ids[x] for x in v] for k, v in duplicates.items()}
    return duplicates


def find_equivalent_structures(dbname, args, calculate_missing=True):
    ds = CspDataStore(dbname)
    t1 = time.time()
    ids, eds, contents = structure_rows(ds)
    if args.cluster_from_pxrd_results:
        equivalent_table = read_equivalent_table(ds) 
    t2 = time.time()
    LOG.debug("%s: loading data took %.3fs", dbname, t2 - t1)

    duplicates = find_duplicates_compack(dbname, ids, eds, contents, args)
    LOG.info("%s: %d unique structures total", dbname, len(duplicates))

    if args.cluster_from_pxrd_results:
        for unique_id, equivalent_ids in duplicates.items():
            equivalent_table[unique_id] += equivalent_ids
            for equivalent_id in equivalent_ids:
                equivalent_table[unique_id] += equivalent_table[equivalent_id]
                equivalent_table.pop(equivalent_id)
    else:
        equivalent_table = duplicates

    ds.add_equivalent_structures(equivalent_table)
    ds.commit()
    # Should move this out of the threads to happen at the end
    if dbname != args.output:
        LOG.info(
            "%s: copying %d structures to %s", dbname, len(duplicates), args.output
        )
        ds.copy_unique_structures_to(args.output)
    ds.close()
    return len(duplicates)


def main(sys_args=None):
    import argparse

    parser = argparse.ArgumentParser(
        description="Cluster with compack from csd python API. Need to load CSD "
        "module and install ccdc python package before using this scipt.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "databases",
        nargs="+",
        type=str,
        help="Databases to process given space separated."
    )
    parser.add_argument(
        "-p",
        "--cluster-from-pxrd-results",
        action="store_true",
        help="Cluster the unique structures from pxrd clustering and update the "
        "equivalent table with compack results.",
        default=False
    )
    # Clustering parameters
    parser.add_argument(
        "-cdt",
        "--cluster-density-threshold",
        type=float,
        help="The density threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=0.05
    )
    parser.add_argument(
        "-cet",
        "--cluster-energy-threshold",
        type=float,
        help="The energy threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=1.0
    )
    parser.add_argument(
        "-np",
        "--nprocs",
        type=int,
        help="Number of parallel processes/threads to use for "
        "compack clustering.",
        default=1
    )
    parser.add_argument(
        "--log-level",
        type=str,
        choices=("INFO", "DEBUG", "ERROR", "WARN"),
        default="INFO",
        help="Control level of logging output"
    )
    parser.add_argument(
        "-nmol",
        "--cluster-nmolecules",
        type=int,
        help="Number of molecules used in compack clustering.",
        default=30
    )
    parser.add_argument(
        "-rms",
        "--cluster-rms-threshold",
        type=float,
        help="RMS difference threshold used in compack clustering.",
        default=0.3
    )
    parser.add_argument(
        "-o", 
        "--output", 
        type=str, 
        default="output.db", 
        help="Database for output"
    )

    args = parser.parse_args(sys_args)
    logging.basicConfig(level=args.log_level)

    LOG.info("Creating output database: %s", args.output)
    output_db = CspDataStore(args.output)
    output_db.close()

    for dbname in args.databases:
        n_uniques = find_equivalent_structures(dbname, args)
        LOG.info("Found %d unique structures for database %s", n_uniques, dbname)

    if len(args.databases) == 1:
        return

    LOG.info("Clustering output database %s", args.output)
    find_equivalent_structures(args.output, args)

if __name__ == "__main__":
    main()
