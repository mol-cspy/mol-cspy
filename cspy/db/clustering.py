from cspy.db import CspDataStore
from cspy.db.clustering_loops import find_duplicates
from cspy.util.logging_config import FORMATS, DATEFMT
from cspy.crystal import Crystal
from concurrent.futures import ThreadPoolExecutor, as_completed
from scipy.integrate import simps
from collections import defaultdict
import logging
import numpy as np
import time
import os


LOG = logging.getLogger(__name__)

def calculate_missing_xrd(cid, res_content):
    LOG.info("trying to calculate missing powder pattern for %s", cid)
    c = Crystal.from_shelx_string(res_content)
    pp = c.calculate_powder_pattern()
    i = pp.pattern if pp is not None else None
    LOG.info("powder pattern for %s: %s", cid, "FAILED" if i is None else "SUCCESS")
    return i


def read_equivalent_table(ds):
    """Read existing equivalent_table from previous clustering"""
    equivalent_table = defaultdict(list)
    for unique_id, equivalent_id in ds.query(
        "select unique_id, equivalent_id from equivalent_to"
    ):
        equivalent_table[unique_id].append(equivalent_id)
    if not equivalent_table:
        LOG.warning("%s: no existing equivalent table")
    return equivalent_table


def process_row(row):
    try:
        cid, sg, density, energy, molid, content = row
    except ValueError:
        cid, sg, density, energy, content = row
        molid = 'None'
    return cid, sg, density, energy, molid, content


def structure_rows(ds, cluster_from_equivalent=False, kind="cdtw", calculate_missing=True):
    LOG.info("%s: loading crystals, descriptors", ds.filename)
    ids = []
    eds = []
    xs = []
    molids=[]
    contents = []
    missing_descriptors = {}
    unique_structures = []
    if cluster_from_equivalent:
        rows = ds.unique_structures().fetchall()
        descriptors = dict(ds.query(
            "select distinct(id), value from descriptor join equivalent_to on "
            "descriptor.id==equivalent_to.unique_id where name='xrd'"
        ))
    else:
        rows = ds.final_minimizations().fetchall()
        descriptors = dict(ds.query("select id, value from descriptor where name='xrd'"))
    LOG.debug("%s: loaded %d descriptors", ds.filename, len(descriptors))

    if kind == "compack":
        from cspy.db.compack_clustering import res_to_cif 
        for row in rows:
            cid, sg, density, energy, molid, content = process_row(row)
            ids.append(cid)
            eds.append((energy, density))
            molids.append(molid)  
            contents.append(res_to_cif(content))

    elif kind in ("cdtw_cos", "cdtw", "cos"):  
        for row in rows:
            cid, sg, density, energy, molid, content = process_row(row)
            if cid in descriptors:
                a = np.frombuffer(descriptors[cid])
                x = a / simps(a, dx=0.02) if max(a) != 1 else a
            else:
                x = calculate_missing_xrd(cid, content) if calculate_missing else None
                if x is not None:
                    missing_descriptors[cid] = x
                    x = x / simps(x, dx=0.02) 
            if x is None:
                unique_structures.append(cid)
            else:
                ids.append(cid)
                eds.append((energy, density))
                xs.append(x)
                molids.append(molid) 
                contents.append(content)                  
        if missing_descriptors:
            ds.add_descriptors(
                "xrd", missing_descriptors,
                metadata="{'two_theta': (0, 20), 'sep': 0.02}",
                replace=True,
                )
    else:
        raise NotImplementedError(
            f"Removing duplicates not supported for method='{kind}'"
        )

    if len(ids) == 0:
        LOG.warning("%s: no structures retrieved")
    LOG.debug(
        "%s: retrieved %d structures, %d had descriptors",
        ds.filename,
        len(ids),
        len(descriptors),
    )
    return (
        ids,
        np.array(eds, dtype=np.float64),
        np.asarray(xs, dtype=np.float64),
        contents,
        unique_structures,
    )


def find_duplicates_powder(dbname, ids, eds, xs, no_desc, args):
    nstructs = len(ids)
    LOG.info(
        "%s: finding unique structures by '%s', N=%d", dbname, args.method, nstructs
    )
    t1 = time.time()
    duplicates_array = find_duplicates(
        eds,
        xs,
        method=args.method,
        ethresh=args.cluster_energy_threshold,
        dthresh=args.cluster_density_threshold,
        cdtw_thresh=args.cluster_xrdcdtw_threshold,
        cos_thresh=args.cluster_xrdcos_threshold
    )
    t2 = time.time()
    LOG.debug("%s: clustering took %.3fs", dbname, t2 - t1)
    duplicates = {}
    for i, n in enumerate(duplicates_array):
        if n == -1:
            duplicates[i] = []
        else:
            duplicates[n].append(i)
    duplicates = {ids[k]: [ids[x] for x in v] for k, v in duplicates.items()}

    if args.method == "cdtw":
        process = "e->d->cdtw"
    elif args.method == "cdtw_cos":
        process = "e->d->cos->cdtw"
    elif args.method == "cos":
        process = "e->d->cos"
    LOG.info(
        "%s: %d unique structures from %s", dbname, len(duplicates), process
    )

    for cid in no_desc:
        duplicates[cid] = []
    if len(no_desc) > 0:
        LOG.info("%s: keeping %d stuctures w/o descriptors", dbname, len(no_desc))
    return duplicates


def find_duplicates_compack(dbname, ids, eds, contents, args):
    from cspy.db.compack_clustering import iterative_compack_batch

    duplicates = {}
    LOG.info("%s: clustering %d structures using compack", dbname, len(ids))
    equivalent_to = np.ones(len(ids), dtype=int) * -1
    for i in range(len(ids)):
        LOG.debug("Reference structure %s (%d)", ids[i], i)
        if equivalent_to[i] > -1:
            LOG.debug(
                "%s (%d) == %s (%d), skipping",
                ids[i],
                i,
                ids[equivalent_to[i]],
                equivalent_to[i],
            )
            continue
        ed = eds[i]
        ed_a = np.abs(np.array(eds) - ed)
        idxs = np.where(
            (ed_a[:, 0] < args.cluster_energy_threshold)
            & (ed_a[:, 1] < args.cluster_density_threshold)
            & (equivalent_to < 0)
        )[0]
        idxs = idxs[idxs > i]
        if len(idxs) == 0:
            LOG.debug(
                "%s (%d) had no structures in energy/density window for comparison",
                ids[i],
                i,
            )
            duplicates[i] = []
            continue
        LOG.info("Comparing %d structures to %s", len(idxs), ids[i])
        rmsds = iterative_compack_batch(
            contents[i],
            [contents[j] for j in idxs],
            args.jobs,
            nmolecules=args.cluster_nmolecules,
        )
        LOG.debug("RMSD: %s", rmsds)
        dups = [idxs[j] for j, v in rmsds.items() if v < args.cluster_rms_threshold]
        if len(dups) > 0:
            equivalent_to[dups] = i
        LOG.info("Found %d equivalent structures to %s", len(dups), ids[i])
        duplicates[i] = dups
        LOG.info(
            "%d structures remaining", len(np.where(equivalent_to[i + 1:] < 0)[0])
        )

    LOG.info("%s: %d unique structures from compack", dbname, len(duplicates))
    duplicates = {ids[k]: [ids[x] for x in v] for k, v in duplicates.items()}
    return duplicates


def find_equivalent_structures(dbname, args, calculate_missing=True):
    ds = CspDataStore(dbname)
    t1 = time.time()
    ids, eds, xs, contents, no_desc = structure_rows(
        ds, 
        cluster_from_equivalent=args.cluster_from_equivalent, 
        kind=args.method, 
        calculate_missing=calculate_missing,
    )
    if args.cluster_from_equivalent:
        equivalent_table = read_equivalent_table(ds) 
    t2 = time.time()
    LOG.debug("%s: loading data took %.3fs", dbname, t2 - t1)

    if args.method == "compack":
        duplicates = find_duplicates_compack(dbname, ids, eds, contents, args)
    elif args.method in ("cdtw_cos", "cdtw", "cos"):
        duplicates = find_duplicates_powder(dbname, ids, eds, xs, no_desc, args)
    else:
        raise NotImplementedError(
            f"Removing duplicates not supported for method='{args.method}'"
        )
    LOG.info("%s: %d unique structures total", dbname, len(duplicates))

    if args.cluster_from_equivalent:
        for unique_id, equivalent_ids in duplicates.items():
            equivalent_table[unique_id] += equivalent_ids
            for equivalent_id in equivalent_ids:
                equivalent_table[unique_id] += equivalent_table[equivalent_id]
                equivalent_table.pop(equivalent_id)
    else:
        equivalent_table = duplicates

    ds.add_equivalent_structures(equivalent_table)
    ds.commit()
    # Should move this out of the threads to happen at the end
    if dbname != args.output:
        LOG.info(
            "%s: copying %d structures to %s", dbname, len(duplicates), args.output
        )
        ds.copy_unique_structures_to(args.output)
    ds.close()
    return len(duplicates)


def main(sys_args=None):
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "databases",
        nargs="+",
        type=str,
        help="Databases to process given space separated."
    )
    # Clustering parameters
    parser.add_argument(
        "-cfe",
        "--cluster-from-equivalent",
        action="store_true",
        default=False,
        help="Cluster the unique structures from existing equivalent_table",
    )
    parser.add_argument(
        "-cdt",
        "--cluster-density-threshold",
        type=float,
        help="The density threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=0.05
    )
    parser.add_argument(
        "-cet",
        "--cluster-energy-threshold",
        type=float,
        help="The energy threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=1.0
    )
    parser.add_argument(
        "-ccdtwt",
        "--cluster-xrdcdtw-threshold",
        type=float,
        help="The difference in XRD patterns as "
        "measured by the constrained dynamic "
        "time warping method, within which "
        "structures are considered the same.",
        default=10.00
    )
    parser.add_argument(
        "-ccost",
        "--cluster-xrdcos-threshold",
        type=float,
        help="The similarity in XRD patterns as "
        "measured by a cosine difference within which "
        "structures are considered the same.",
        default=0.80
    )
    parser.add_argument(
        "-j",
        "--jobs",
        type=int,
        help="Number of parallel processes/threads to use for "
        "xrd/compack clustering",
        default=1
    )
    parser.add_argument(
        "--log-level",
        type=str,
        choices=("INFO", "DEBUG", "ERROR", "WARN"),
        default="INFO",
        help="Control level of logging output"
    )
    parser.add_argument(
        "-nmol",
        "--cluster-nmolecules",
        type=int,
        help="Number of molecules used in " "compack clustering.",
        default=30
    )
    parser.add_argument(
        "-rms",
        "--cluster-rms-threshold",
        type=float,
        help="RMS difference threshold used in " "compack clustering.",
        default=0.3
    )
    parser.add_argument(
        "--compack-timeout",
        type=float,
        help="Timeout for compack clustering in seconds.",
        default=30.0
    )
    parser.add_argument(
        "--skip-calculate-missing-pxrd",
        action="store_true",
        help="Skip the calculation of missing pxrd patterns.",
        default=False
    )
    parser.add_argument(
        "-o", 
        "--output", 
        type=str, 
        default="output.db", 
        help="Database for output"
    )
    parser.add_argument(
        "-m",
        "--method",
        type=str,
        default="cdtw_cos",
        choices=("cdtw_cos", "cdtw", "cos", "compack"),
        help="How to perform clustering"
    )

    args = parser.parse_args(sys_args)
    logging.basicConfig(
        level=args.log_level, format=FORMATS[args.log_level], datefmt=DATEFMT
    )
    if args.method == 'compack':
        try:
            from ccdc.crystal import PackingSimilarity
        except ImportError:
            LOG.error('ccdc module does not exist - compack method can not be used.')
            exit(-1)

    LOG.info("%d databases to process", len(args.databases))
    LOG.info("Creating output database: %s", args.output)
    output_db = CspDataStore(args.output)
    output_db.close()

    calculate_missing = False if args.skip_calculate_missing_pxrd else True
    n_uniques = 0
    with ThreadPoolExecutor(args.jobs) as e:
        futures = [
            e.submit(
                find_equivalent_structures, 
                dbname, 
                args, 
                calculate_missing=calculate_missing
            )
            for dbname in args.databases
        ]
        for future in as_completed(futures):
            n_uniques += future.result()
    LOG.info("Found %d unique structures in total", n_uniques)

    if len(args.databases) == 1:
        return

    LOG.info("Clustering output database %s", args.output)
    find_equivalent_structures(args.output, args, calculate_missing=False)


if __name__ == "__main__":
    main()
