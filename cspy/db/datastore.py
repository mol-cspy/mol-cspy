from collections import defaultdict
import logging
import json
import sqlite3
from .schemas import current as CURRENT_SCHEMA

LOG = logging.getLogger(__name__)


class DataStore:

    _SCHEMA = ""

    def __init__(self, filename, connect=True, execute_schema=False):
        """Open an sqlite3 database file, connecting if required"""
        assert isinstance(filename, str)
        self.filename = filename
        self._connection = None
        self._cursor = None
        self.table_names = set()
        self.schema_version = "unknown"

        if connect:
            self.connect(execute_schema=execute_schema)
            self.load_table_names()
            self.guess_schema_version()

    @property
    def connected(self):
        """Check if we have a connection to the database"""
        return self.connection is not None

    @property
    def connection(self):
        return self._connection

    def connect(self, execute_schema=False, **kwargs):
        """Establish a connection to the database,
        returning a cursor for that connection. If a
        connection already exists, use that one"""

        if self.connected:
            return self.connection.cursor()

        LOG.debug("Establishing connection to database %s", self.filename)
        conn = sqlite3.connect(self.filename, **kwargs)
        if execute_schema:
            try:
                conn.executescript(self._SCHEMA)
            except sqlite3.DatabaseError as e:
                LOG.error("Error establishing connection to %s: %s", self.filename, e)
                raise e
        self._connection = conn
        return self.cursor()

    def commit(self):
        """Force a commit on the database connection"""
        if self.connected:
            self.connection.commit()

    def table_exists(self, table_name):
        """Check if a table with the given name exists"""
        return table_name in self.table_names

    def cursor(self, new=False):
        assert self.connected, "Must connect to a database before acquiring cursor"
        if new:
            return self.connection.cursor()
        if not hasattr(self, "_cursor"):
            self._cursor = self.connection.cursor()
        elif self._cursor is None:
            self._cursor = self.connection.cursor()
        return self._cursor

    def load_table_names(self):
        """Load the set of current table names in the database"""
        cur = self.cursor()
        cur.execute("select name from sqlite_master where type='table'")
        self.table_names = {row[0] for row in cur.fetchall()}
        LOG.debug("Loaded table_names: %s", self.table_names)

    def guess_schema_version(self):
        """Figure out which version of cspy was used to create this database"""
        self.n_tables = len(self.table_names)
        if self.table_exists("meta"):
            self.schema_version = "cspy2"
            if not self.table_exists("crystal"):
                self.schema_version = "intermediate"
        LOG.debug("Guessed schema version: '%s'", self.schema_version)
        return self.schema_version

    def create_tables(self):
        """Connect to the database, create each table in the schema,
        then load the table names and guess the schema version"""
        cur = self.cursor()
        LOG.debug("Creating tables from schema")
        cur.executescript(self._SCHEMA)
        self.load_table_names()
        self.guess_schema_version()

    def close(self):
        """Close the database connection, clearing class variables"""
        self.disconnect()

    def disconnect(self):
        """Close the database connection, clearing class variables"""
        if self.connected:
            self.connection.close()
        self._cursor = None
        self._connection = None

    def insert_one(self, table, row, columns=None, commit=True, replace=False):
        stmt = "insert {replace} into {table} {columns} values ({placeholders})"
        if isinstance(row, dict):
            columns, values = zip(*row.items())
            placeholders = ",".join("?" for _ in values)
            columns = "({})".format(",".join(columns))
        else:
            values = row
            columns = "(" + ",".join(columns) + ")" if columns else ""
            placeholders = ",".join("?" for _ in values)

        return self.query(
            stmt.format(
                table=table,
                columns=columns,
                placeholders=placeholders,
                replace="or replace" if replace else "",
            ),
            values,
            commit=commit,
        )

    def insert_many(self, table, rows, columns=None, commit=True, replace=False):
        stmt = "insert {replace} into {table} {columns} values ({placeholders})"
        if isinstance(rows, dict):
            columns, values = zip(*rows.items())
            placeholders = ",".join("?" for _ in values)
            values = (x for x in zip(*values))
            columns = "({})".format(",".join(columns))
        else:
            values = list(rows)
            assert len(values) > 0
            columns = "(" + ",".join(columns) + ")" if columns else ""
            placeholders = ",".join("?" for _ in values[0])

        return self.query_many(
            stmt.format(
                table=table,
                columns=columns,
                placeholders=placeholders,
                replace="or replace" if replace else "",
            ),
            values,
            commit=commit,
        )

    def select(self, table, columns=None, additional=""):
        stmt = "select {columns} from {table} {additional}"
        if columns is not None:
            columns = ", ".join("{}.{}".format(table, col) for col in columns)
        else:
            columns = "*"
        return self.query(
            stmt.format(columns=columns, table=table, additional=additional)
        )

    def query(self, query_text, *args, commit=True, **kwargs):
        """Query the database with a custom query"""
        response = self.cursor().execute(query_text, *args, **kwargs)
        if commit:
            self.commit()
        return response

    def query_many(self, query_text, *args, commit=True):
        """Query the database with a custom query"""
        response = self.cursor().executemany(query_text, *args)
        if commit:
            self.commit()
        return response

    @staticmethod
    def create_and_connect(filename):
        """Create a new datastore object, establishing
        connection and creating tables"""
        dstore = DataStore(filename, connect=True, execute_schema=True)
        return dstore


class CspDataStore(DataStore):

    _SCHEMA = CURRENT_SCHEMA

    def __init__(self, *args, **kwargs):
        super().__init__(*args, connect=True, execute_schema=True, **kwargs)

    def update_unique_structures(self, equivalence):
        pass

    def unique_structures(self, with_trial_data=False, fallback=False):
        num_unique = self.query("select count(*) from equivalent_to").fetchone()[0]
        if num_unique == 0:
            LOG.info(
                "No data in equivalent_to table: run `cspy-db cluster` to populate this"
            )
            if fallback:
                LOG.info("Falling back to final minimizations")
                return self.final_minimizations(with_trial_data=with_trial_data)
            else:
                return []
        else:
            query_text = (
                "select C.* from crystal C join "
                "(select distinct unique_id from equivalent_to) "
                "on unique_id == C.id "
                "order by energy asc"
            )
            if with_trial_data:
                query_text = (
                    "select C.*, T.minimization_step, T.trial_number, T.minimization_time "
                    "from crystal C join "
                    "(select distinct unique_id from equivalent_to) "
                    "on unique_id == C.id"
                    " join trial_structure T on C.id = T.id "
                    "order by energy asc"
                )
        return self.query(query_text)

    def get_total_minimization_times(self):
        query_text = (
            "select trial_number, sum(minimization_time) from trial_structure "
            "group by trial_number"
        )
        return self.query(query_text)

    def equivalent_trial_numbers(self):
        query_text = (
            "select t1.trial_number, coalesce(t2.trial_number, -1)"
            "from equivalent_to "
            "left join trial_structure t1 on (t1.id = equivalent_to.unique_id) "
            "left join trial_structure t2 on (t2.id = equivalent_to.equivalent_id)"
        )
        return self.query(query_text)

    def final_minimization_step(self):
        return self.query(
            "select max(minimization_step) from trial_structure"
        ).fetchone()[0]

    def final_minimizations(self, with_trial_data=False):
        max_step = self.final_minimization_step()
        if max_step is None:
            return []
        query_text = (
            "select C.* from crystal C join "
            "(select id from trial_structure where minimization_step >= {max_step}) "
            "T on C.id = T.id "
            "order by energy asc"
        ).format(max_step=max_step)

        if with_trial_data:
            query_text = (
                "select C.*, T.minimization_step, T.trial_number, T.minimization_time "
                "from crystal C join "
                "(select * from trial_structure where minimization_step >= {max_step}) "
                "T on C.id = T.id "
            ).format(max_step=max_step)
        return self.query(query_text, commit=False)

    def number_of_final_minimizations(self):
        max_step = self.final_minimization_step()
        if max_step is None:
            return 0
        query_text = (
            "select count(C.id) from crystal C join "
            "(select id from trial_structure where minimization_step >= {max_step}) "
            "T on C.id = T.id "
        ).format(max_step=max_step)
        return list(self.query(query_text))[0][0]

    def copy_unique_structures_to(self, dbname):
        other = CspDataStore(dbname)
        other.close()
        self.query(f"attach '{dbname}' as tmp")
        self.query(
            "insert or replace into tmp.crystal select crystal.* from crystal join "
            "(select distinct unique_id from equivalent_to) "
            "on unique_id == crystal.id"
        )
        self.query(
            "insert or replace into tmp.descriptor select descriptor.* from descriptor join "
            "(select distinct unique_id from equivalent_to) "
            "on unique_id == descriptor.id"
        )
        self.query(
            "insert or replace into tmp.trial_structure select trial_structure.* from trial_structure join "
            "(select distinct unique_id from equivalent_to) "
            "on unique_id == trial_structure.id"
        )
        self.commit()
        self.query("detach tmp")

    def copy_equivalence_table_to(self, dbname):
        other = CspDataStore(dbname)
        other.close()
        self.query(f"attach '{dbname}' as tmp")
        self.query(
            "insert or replace into tmp.equivalent_to select * from equivalent_to"
        )
        self.commit()
        self.query("detach tmp")

    def crystals(self):
        return self.select("crystal")

    def add_equivalent_structures(self, equivalent_structures):
        rows = []
        for k, v in equivalent_structures.items():
            if len(v):
                rows += [(k, x) for x in set(v)]
            else:
                rows.append((k, None))
        self.query("delete from equivalent_to")
        self.insert_many("equivalent_to", rows, columns=("unique_id", "equivalent_id"))
        nuniques = self.query(
            "select count(distinct unique_id) from equivalent_to"
        ).fetchone()[0]
        ndups = self.query(
            "select count(distinct equivalent_id) from equivalent_to"
        ).fetchone()[0]
        LOG.debug("%s has %d uniques, %d duplicates", self.filename, nuniques, ndups)
        self.commit()

    def add_csp_results(self, results):
        crystals = defaultdict(list)
        trial_structures = defaultdict(list)
        descriptors = defaultdict(list)

        for r in results:
            crystals["id"].append(r.id)
            crystals["spacegroup"].append(r.spacegroup)
            crystals["energy"].append(r.energy)
            crystals["density"].append(r.density)
            crystals["file_content"].append(r.file_content)
            trial_structures["id"].append(r.id)
            trial_structures["minimization_step"].append(r.minimization_step)
            trial_structures["trial_number"].append(r.trial_number)
            trial_structures["valid"].append(True)
            trial_structures["minimization_time"].append(r.time)
            if r.xrd is not None:
                descriptors["id"].append(r.id)
                descriptors["name"].append("xrd")
                descriptors["value"].append(r.xrd)
                descriptors["metadata"].append("{'two_theta': (0, 20), 'sep': 0.02}")

        if crystals:
            self.insert_many("crystal", crystals, replace=True)
        if trial_structures:
            self.insert_many("trial_structure", trial_structures, replace=True)
        if descriptors:
            self.insert_many("descriptor", descriptors, replace=True)
        self.commit()

    def add_qrbh_results(self, results):
        crystals = defaultdict(list)
        trial_structures = defaultdict(list)
        descriptors = defaultdict(list)

        for r in results:
            crystals["id"].append(r.id)
            crystals["spacegroup"].append(r.spacegroup)
            crystals["energy"].append(r.energy)
            crystals["density"].append(r.density)
            crystals["file_content"].append(r.file_content)
            trial_structures["id"].append(r.id)
            trial_structures["minimization_step"].append(r.minimization_step)
            trial_structures["trial_number"].append(r.trial_number)
            trial_structures["valid"].append(r.accept)
            trial_structures["minimization_time"].append(r.time)
            meta = {"mc_step": r.mc_step, "unique_index": r.unique_index}
            trial_structures["metadata"].append(json.dumps(meta))
            if r.xrd is not None:
                descriptors["id"].append(r.id)
                descriptors["name"].append("xrd")
                descriptors["value"].append(r.xrd)
                descriptors["metadata"].append("{'two_theta': (0, 20), 'sep': 0.02}")

        if crystals:
            self.insert_many("crystal", crystals, replace=True)
        if trial_structures:
            self.insert_many("trial_structure", trial_structures, replace=True)
        if descriptors:
            self.insert_many("descriptor", descriptors, replace=True)
        self.commit()

    def add_trials(self, trials):
        trials_dict = defaultdict(list)
        for trial in trials:
            id, seed, iterations, state, meta = trial
            trials_dict["trial_id"].append(id)
            trials_dict["trial_number"].append(seed)
            trials_dict["iterations"].append(iterations)
            trials_dict["state"].append(state)
            trials_dict["metadata"].append(json.dumps(meta))
        if trials_dict:
            self.insert_many("trial", trials_dict, replace=True)

    def add_crystal_structures(self, structures, **kwargs):
        self.insert_many("crystal", structures, **kwargs)
        self.add_metadata("added {} crystals".format(len(structures)))

    def add_descriptors(self, kind, values, metadata="", **kwargs):
        descriptors = defaultdict(list)
        for i, val in values.items():
            descriptors["id"].append(i)
            descriptors["value"].append(sqlite3.Binary(val))
            descriptors["name"].append(kind)
            descriptors["metadata"].append(metadata)

        self.insert_many("descriptor", descriptors, **kwargs)
        self.add_metadata("added {} descriptors".format(len(values)))
        self.commit()

    def add_metadata(self, description):
        self.insert_one(
            "meta", {"version": self.schema_version, "description": description}
        )

    def has_unique_structure_information(self):
        return (
            self.query("select count(*) from equivalent_to limit 1").fetchone()[0] > 1
        )
