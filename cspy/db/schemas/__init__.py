from os.path import dirname, join

CURRENT_SCHEMA_VERSION = "cspy2"
_DIR = dirname(__file__)

with open(join(_DIR, "cspy2.sql")) as f:
    cspy2 = f.read()

current = cspy2

__all__ = ["cspy2", "current", "CURRENT_SCHEMA_VERSION"]
