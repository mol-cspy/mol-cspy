import argparse
import logging
from cspy.db import CspDataStore
from cspy.util.logging_config import FORMATS, DATEFMT
import pandas as pd
import numpy as np

LOG = logging.getLogger(__name__)


def print_database_info(dbfile, args):
    db = CspDataStore(dbfile)
    print("\n", dbfile)
    counts = {}
    for table in db.table_names:
        if table == "sqlite_sequence":
            continue
        query = f"select count(*) from {table}"
        num_rows = db.query(query).fetchone()[0]
        counts[table] = num_rows

    print("\n{:<18s} {:>10s}\n".format("table".upper(), "count".upper()))
    for k, v in counts.items():
        print(f"{k:<18s} {v:10d}")

    if counts["equivalent_to"] > 0:
        print("\nUnique structure information\n".upper())
        unique_structures = db.unique_structures(with_trial_data=True).fetchall()
        df = pd.DataFrame.from_records(
            unique_structures,
            columns=[
                "id",
                "Space Group",
                "Density (g/cm^3)",
                "Energy (kJ/mol)",
                "Molecule ID",
                "res",
                "step",
                "Sobol seed",
                "Final Minimization Time (s)",
            ],
        )
        print(df[["Energy (kJ/mol)", "Density (g/cm^3)"]].describe().round(2))
        equiv = np.array(db.equivalent_trial_numbers().fetchall())
        duplicates = {}
        for k, v in equiv:
            if k not in duplicates:
                duplicates[k] = []
            if v > -1:
                duplicates[k].append(v)
        N = len(df)

        print("\nCoupon collectors problem".upper())
        m = 1
        expected = int(np.log(N) * N + (m - 1) * np.log(np.log(N)) * N + N / 2 + 0.5)
        print(f"Expected trials to find {N} structures at least {m} times each ")
        print(f"    N log N + {m - 1} N log log N + N / 2 = {expected} trials")
        trial_numbers = np.sort(np.unique(equiv.ravel()))
        trial_numbers = trial_numbers[trial_numbers > 0][:expected]
        max_trial = np.max(trial_numbers)
        df["keep"] = True
        for k, v in duplicates.items():
            if k > max_trial:
                if all(x > max_trial for x in v):
                    df.loc[df["Sobol seed"] == k, "keep"] = False
        filtered = df[df.keep]
        print(
            f"No. unique structures if truncated at {expected} successful trials = {len(filtered)}"
        )
        print("\nRetained structures\n".upper())
        print(filtered[["Energy (kJ/mol)", "Density (g/cm^3)"]].describe().round(2))
        print("\nMissed structures\n".upper())
        print(
            df.loc[~df.keep, ["Energy (kJ/mol)", "Density (g/cm^3)"]]
            .describe()
            .round(2)
        )
    db.close()


def main(sys_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "databases",
        nargs="+",
        type=str,
        help="Databases to process given space separated.",
    )
    parser.add_argument(
        "--log-level",
        type=str,
        choices=("INFO", "DEBUG", "ERROR", "WARN"),
        default="INFO",
        help="Control level of logging output",
    )
    args = parser.parse_args(sys_args)
    logging.basicConfig(
        level=args.log_level, format=FORMATS[args.log_level], datefmt=DATEFMT
    )
    LOG.info("%d databases", len(args.databases))

    for db in args.databases:
        print_database_info(db, args)


if __name__ == "__main__":
    main()
