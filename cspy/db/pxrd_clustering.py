from cspy.db import CspDataStore
from cspy.db.clustering_loops import find_duplicates, find_duplicates_between_list
from cspy.util.logging_config import FORMATS, DATEFMT
from cspy.crystal import Crystal
from scipy.integrate import simps
from collections import defaultdict
from multiprocessing import Pool
import logging
import numpy as np
import time


LOG = logging.getLogger(__name__)


def calculate_missing_xrd(cid, res_content):
    LOG.info("trying to calculate missing powder pattern for %s", cid)
    c = Crystal.from_shelx_string(res_content)
    pp = c.calculate_powder_pattern()
    i = pp.pattern if pp is not None else None
    LOG.info("powder pattern for %s: %s", cid, "FAILED" if i is None else "SUCCESS")
    return i


def read_equivalent_table(ds):
    """Read existing equivalent_table from previous clustering"""
    equivalent_table = defaultdict(list)
    for unique_id, equivalent_id in ds.query(
        "select unique_id, equivalent_id from equivalent_to"
    ):
        if equivalent_id is not None:
            equivalent_table[unique_id].append(equivalent_id)
        else:
            equivalent_table[unique_id]
    if not equivalent_table:
        LOG.warning("%s: no existing equivalent table")
    return equivalent_table


def structure_rows(
    ds, 
    cluster_from_equivalent=False,
    calculate_missing=True
):
    LOG.info("%s: loading crystals, descriptors", ds.filename)
    ids = []
    eds = []
    xs = []
    missing_descriptors = {}
    unique_structures = []
    if cluster_from_equivalent:
        rows = ds.unique_structures().fetchall()
        descriptors = dict(ds.query(
            "select distinct(id), value from descriptor join equivalent_to on "
            "descriptor.id==equivalent_to.unique_id where name='xrd'"
        ))
    else:
        rows = ds.final_minimizations().fetchall()
        descriptors = dict(ds.query("select id, value from descriptor where name='xrd'"))
    LOG.debug("%s: loaded %d descriptors", ds.filename, len(descriptors))

    for cid, sg, density, energy, content in rows:
        if cid in descriptors:
            a = np.frombuffer(descriptors[cid])
            x = a / simps(a, dx=0.02)
        else:
            x = calculate_missing_xrd(cid, content) if calculate_missing else None
            if x is not None:
                missing_descriptors[cid] = x
                x = x / simps(x, dx=0.02)

        if x is None:
            unique_structures.append(cid)
        else:
            ids.append(cid)
            eds.append((energy, density))
            xs.append(x)

    if missing_descriptors:
        ds.add_descriptors("xrd", missing_descriptors,
                           metadata="{'two_theta': (0, 20), 'sep': 0.02}")

    if len(ids) == 0:
        LOG.warning("%s: no structures retrieved")
    LOG.debug(
        "%s: retrieved %d structures, %d had descriptors",
        ds.filename,
        len(ids),
        len(descriptors),
    )
    return (
        ids,
        np.array(eds, dtype=np.float64),
        np.asarray(xs, dtype=np.float64),
        unique_structures,
    )

def set_energy_flag(eds, cluster_energy_threshold, min_energy=None):
    if min_energy is None:
        energy_flag = eds[0][0]
    else:
        energy_flag = min_energy

    energy_flag_indice = [0]
    for i, info in enumerate(eds):
        energy, _ = info
        if energy - energy_flag <= cluster_energy_threshold:
            continue
        energy_flag += cluster_energy_threshold
        energy_flag_indice.append(i)

    if energy_flag_indice[-1] == len(eds) - 1:
        energy_flag_indice[-1] += 1
    else:
        energy_flag_indice.append(len(eds))
    return energy_flag_indice
    

def find_duplicate_within_window(
    ids, eds, xs, 
    method, 
    cluster_energy_threshold, 
    cluster_density_threshold, 
    cluster_xrdcdtw_threshold,
    cluster_xrdcos_threshold,
):
    equivalent_to = find_duplicates(
        eds, xs,
        method=method,
        ethresh=cluster_energy_threshold,
        dthresh=cluster_density_threshold,
        cdtw_thresh=cluster_xrdcdtw_threshold,
        cos_thresh=cluster_xrdcos_threshold,
    )
    duplicates = {}
    for i, n in enumerate(equivalent_to):
        if n == -1:
            duplicates[i] = []
        else:
            duplicates[n].append(i)
    duplicates = {ids[k]: [ids[x] for x in v] for k, v in duplicates.items()}
    return duplicates

def find_duplicate_between_window(
    index,
    ids_x, eds_x, xs_x, 
    ids_y, eds_y, xs_y, 
    method,
    cluster_energy_threshold, 
    cluster_density_threshold, 
    cluster_xrdcdtw_threshold,
    cluster_xrdcos_threshold,
):
    equivalent_to = find_duplicates_between_list(
        eds_x, xs_x,
        eds_y, xs_y,
        method=method,
        ethresh=cluster_energy_threshold,
        dthresh=cluster_density_threshold,
        cdtw_thresh=cluster_xrdcdtw_threshold,
        cos_thresh=cluster_xrdcos_threshold,
    )
    duplicates = defaultdict(list)
    for i, n in enumerate(equivalent_to):
        if n != -1:
            duplicates[n].append(i)
    duplicates = {ids_x[k]: [ids_y[x] for x in v] for k, v in duplicates.items()}
    return index, duplicates

def get_new_list(ids, eds, xs, results):
    new_ids = []
    new_eds = []
    new_xs = []
    equivalent_table = {}

    for result in results:
        for unique_id, equivalent_ids in result.get().items():
            equivalent_table[unique_id] = equivalent_ids
            index = ids.index(unique_id)
            new_ids.append(unique_id)
            new_eds.append(eds[index])
            new_xs.append(xs[index])

    return new_ids, new_eds, new_xs, equivalent_table

def merge_duplicates(equivalent_table, results):
    results_list = []
    for result in results:
        results_list.append(result.get())
    LOG.debug("Results_list in merge_duplicates %s", results_list)

    for i, equivalent_dict in sorted(results_list, key=lambda x:x[0], reverse=True):
        for unique_id, equivalent_ids in equivalent_dict.items():
            equivalent_table[unique_id] += equivalent_ids
            for equivalent_id in equivalent_ids:
                equivalent_table[unique_id] += equivalent_table.pop(equivalent_id)

    return equivalent_table

def find_duplicates_powder(dbname, ids, eds, xs, no_desc, args):
    nstructs = len(ids)
    LOG.info(
        "%s: finding unique structures by '%s', N=%d on %s processes",
        dbname, args.method, nstructs, args.nprocs
    )

    min_energy = eds[0][0]
    energy_flag_indice = set_energy_flag(
        eds, args.cluster_energy_threshold, min_energy=min_energy
    )
    LOG.info(
        "%s: %s energy flags from min_energy %s", dbname, len(energy_flag_indice), eds[0][0]
    )
    
    t1 = time.time()
    results = []
    pool = Pool(processes=args.nprocs)
    for i, energy_flag_index in enumerate(energy_flag_indice[:-1]):
        ids_x = ids[energy_flag_index:energy_flag_indice[i+1]]
        eds_x = np.array(eds[energy_flag_index:energy_flag_indice[i+1]], dtype=np.float64)
        xs_x = np.asarray(xs[energy_flag_index:energy_flag_indice[i+1]], dtype=np.float64)
        LOG.info(
            "%s: Clustering %s structures in energy window %s to %s",
            dbname, len(ids_x), eds_x[0][0], eds_x[-1][0]
        )
        results.append(pool.apply_async(
            func=find_duplicate_within_window,
            args=(
                ids_x, eds_x, xs_x, 
                args.method,
                args.cluster_energy_threshold, 
                args.cluster_density_threshold, 
                args.cluster_xrdcdtw_threshold, 
                args.cluster_xrdcos_threshold, 
            ),
        ))
    pool.close()
    pool.join()
    del pool

    new_ids, new_eds, new_xs, equivalent_table = get_new_list(ids, eds, xs, results)
    energy_flag_indice = set_energy_flag(
        new_eds, args.cluster_energy_threshold, min_energy=min_energy
    )
    LOG.info(
        "%s: Clustered within energy window %s %s structures left by %s seconds",
        dbname, args.cluster_energy_threshold, len(new_ids), time.time()-t1
    )
    
    results = []
    pool = Pool(processes=args.nprocs)
    for i, energy_flag_index in enumerate(energy_flag_indice[:-2]):
        ids_x = new_ids[energy_flag_index:energy_flag_indice[i+1]]
        eds_x = np.array(new_eds[energy_flag_index:energy_flag_indice[i+1]], dtype=np.float64)
        xs_x = np.asarray(new_xs[energy_flag_index:energy_flag_indice[i+1]], dtype=np.float64)

        ids_y = new_ids[energy_flag_indice[i+1]:energy_flag_indice[i+2]]
        eds_y = np.array(new_eds[energy_flag_indice[i+1]:energy_flag_indice[i+2]], dtype=np.float64)
        xs_y = np.asarray(new_xs[energy_flag_indice[i+1]:energy_flag_indice[i+2]], dtype=np.float64)
        LOG.info(
            "%s: Clustering %s structures in energy window %s to %s "
            "to %s structures in energy window %s to %s ",
            dbname, len(ids_x), eds_x[0][0], eds_x[-1][0],
            len(ids_y), eds_y[0][0], eds_y[-1][0]
        )
        LOG.debug("%s: ids_x in %s window %s", dbname, i, ids_x)
        LOG.debug("%s: ids_y in %s window %s", dbname, i, ids_y)
        results.append(pool.apply_async(
            func=find_duplicate_between_window,
            args=(
                i,
                ids_x, eds_x, xs_x, 
                ids_y, eds_y, xs_y, 
                args.method,
                args.cluster_energy_threshold, 
                args.cluster_density_threshold, 
                args.cluster_xrdcdtw_threshold, 
                args.cluster_xrdcos_threshold, 
            ),
        ))
    pool.close()
    pool.join()

    equivalent_table = merge_duplicates(equivalent_table, results)

    if args.method == "cdtw":
        process = "e->d->cdtw"
    elif args.method == "cdtw_cos":
        process = "e->d->cos->cdtw"
    elif args.method == "cos":
        process = "e->d->cos"
    LOG.info(
        "%s: %d unique structures from %s", dbname, len(equivalent_table), process
    )

    for cid in no_desc:
        equivalent_table[cid] = []
    if len(no_desc) > 0:
        LOG.info("%s: keeping %d stuctures w/o descriptors", dbname, len(no_desc))
    return equivalent_table


def find_equivalent_structures(dbname, args, calculate_missing=True):
    ds = CspDataStore(dbname)
    t1 = time.time()
    ids, eds, xs, no_desc = structure_rows(
        ds,
        cluster_from_equivalent=args.cluster_from_equivalent,
        calculate_missing=calculate_missing,
    )
    if args.cluster_from_equivalent:
        equivalent_table = read_equivalent_table(ds) 
    t2 = time.time()
    LOG.debug("%s: loading data took %.3fs", dbname, t2 - t1)

    if args.method in ("cdtw_cos", "cdtw", "cos"):
        duplicates = find_duplicates_powder(dbname, ids, eds, xs, no_desc, args)
    else:
        raise NotImplementedError(
            f"Removing duplicates not supported for method='{args.method}'"
        )
    LOG.info("%s: %d unique structures total", dbname, len(duplicates))

    if args.cluster_from_equivalent:
        for unique_id, equivalent_ids in duplicates.items():
            equivalent_table[unique_id] += equivalent_ids
            for equivalent_id in equivalent_ids:
                equivalent_table[unique_id] += equivalent_table.pop(equivalent_id)
    else:
        equivalent_table = duplicates

    ds.add_equivalent_structures(equivalent_table)
    ds.commit()
    # Should move this out of the threads to happen at the end
    if dbname != args.output:
        LOG.info(
            "%s: copying %d structures to %s", dbname, len(equivalent_table), args.output
        )
        ds.copy_unique_structures_to(args.output)
    ds.close()
    return len(equivalent_table)


def main(sys_args=None):
    import argparse

    parser = argparse.ArgumentParser(
        description="Cluster on XRD pattern by multiprocessing. Should be used "
        "for pure cdtw comparison and huge database",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "databases",
        nargs="+",
        type=str,
        help="Databases to process given space separated."
    )
    # Clustering parameters
    parser.add_argument(
        "-cfe",
        "--cluster-from-equivalent",
        action="store_true",
        default=False,
        help="Cluster the unique structures from existing equivalent_table",
    )
    parser.add_argument(
        "-cdt",
        "--cluster-density-threshold",
        type=float,
        help="The density threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=0.05
    )
    parser.add_argument(
        "-cet",
        "--cluster-energy-threshold",
        type=float,
        help="The energy threshold used in clustering, "
        "within which structures are considered the "
        "same.",
        default=1.0
    )
    parser.add_argument(
        "-ccdtwt",
        "--cluster-xrdcdtw-threshold",
        type=float,
        help="The difference in XRD patterns as "
        "measured by the constrained dynamic "
        "time warping method, within which "
        "structures are considered the same.",
        default=10.00
    )
    parser.add_argument(
        "-ccost",
        "--cluster-xrdcos-threshold",
        type=float,
        help="The similarity in XRD patterns as "
        "measured by a cosine difference within which "
        "structures are considered the same.",
        default=0.80
    )
    parser.add_argument(
        "-np",
        "--nprocs",
        type=int,
        help="Number of parallel processes/threads to use for "
        "xrd clustering",
        default=1
    )
    parser.add_argument(
        "--log-level",
        type=str,
        choices=("INFO", "DEBUG", "ERROR", "WARN"),
        default="INFO",
        help="Control level of logging output"
    )
    parser.add_argument(
        "--skip-calculate-missing-pxrd",
        action="store_true",
        help="Skip the calculation of missing pxrd patterns.",
        default=False
    )
    parser.add_argument(
        "-o", 
        "--output", 
        type=str, 
        default="output.db", 
        help="Database for output"
    )
    parser.add_argument(
        "-m",
        "--method",
        type=str,
        default="cdtw",
        choices=("cdtw_cos", "cdtw", "cos"),
        help="How to perform clustering"
    )

    args = parser.parse_args(sys_args)
    logging.basicConfig(
        level=args.log_level, format=FORMATS[args.log_level], datefmt=DATEFMT
    )

    LOG.info("Creating output database: %s", args.output)
    output_db = CspDataStore(args.output)
    output_db.close()

    calculate_missing = False if args.skip_calculate_missing_pxrd else True
    for dbname in args.databases:
        n_uniques = find_equivalent_structures(dbname, args, calculate_missing=calculate_missing)
        LOG.info("Found %d unique structures for database %s", n_uniques, dbname)

    if len(args.databases) == 1:
        return

    LOG.info("Clustering output database %s", args.output)
    find_equivalent_structures(args.output, args)


if __name__ == "__main__":
    main()
