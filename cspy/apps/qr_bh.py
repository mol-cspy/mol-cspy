import logging
import sys
from pprint import pformat
import numpy as np
from mpi4py import MPI
import random
from cspy import Molecule
from cspy.util.logging_config import FORMATS, DATEFMT
from cspy.apps.dma import generate_combined_name
from cspy.configuration import CspyConfiguration, CONFIG
from cspy.potentials import available_potentials
from cspy.util.path import Path
from cspy.apps.csp import check_multipoles_valid
from cspy.distributed.qrbh_manager import QRBHCSP
from cspy.cspympi.qrbh_tasks import QRBHWorker

LOG = logging.getLogger(__name__)

def get_mc_setting():
    config = CspyConfiguration()
    mc_para = {
        "temper": config.get("mc.initial_temper"),#Temperature for basin hopping
        "trial_step": config.get("mc.num_steps"),#Numebr of maximum steps for one basin hopping trial, would be infinite with continue_running=True
        "num_trials": config.get("mc.num_trials"),#Number of basin hopping trials
        "sat_expand": config.get("mc.sat_expand"),#Whether to sat-expand after perturbation
        "move_all": config.get("mc.move_all"),#Whether to apply all available types of move at one perturbation, don't use unless specific interest
        "auto_prob": config.get("mc.auto_prob"),#Calculate probability of choosing move type according to degrees of freedom
        "auto_cutoff": config.get("mc.auto_cutoff"),#Calculate cutoff of move type, currently only applied to volume expansion and contraction based on number of molecules in unit cell
        "continue_running": config.get("mc.continue_running"),#If ture, trial would keep running even after finish
        "cluster_s": config.get("mc.on_the_fly"),#Whether to use on-the-fly clustering, if Ture, continue_running will always be False
        "move": config.get("mc.move"),#Specified move details
        "raw_s": config.get("basin_hopping.raw_structure"),#Perturb from unminimized structure if True
        "opt_s": config.get("basin_hopping.basin_hopping"),#Minimize after perturbation, meaningless here
        "dump_accept": config.get("basin_hopping.dump_accept"),#Only dump accepted structures if True
        "niggli_s": config.get("basin_hopping.niggli_cell"),#Niggli_cell after perturbation
    }
    return mc_para

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "xyz_files",
        type=str,
        nargs="+",
        help="Xyz files containing molecules for generation",
    )
    parser.add_argument(
        "-c",
        "--charges",
        type=str,
        default=CONFIG.get("csp.charges_file"),
        help="Rank0 multipole file",
    )
    parser.add_argument(
        "-m",
        "--multipoles",
        type=str,
        default=CONFIG.get("csp.multipoles_file"),
        help="RankN multipole file",
    )
    parser.add_argument(
        "-g",
        "--spacegroups",
        type=str,
        default=CONFIG.get("csp.spacegroups"),
        help="Spacegroup set for structure generation",
    )
    parser.add_argument(
        "-a",
        "--axis",
        type=str,
        default=None,
        help="Axis filename for structure minimization",
    )
    parser.add_argument(
        "-n",
        "--number-structures",
        type=int,
        default=CONFIG.get("csp.number_structures"),
        help="Number of structures in total",
    )
    parser.add_argument(
        "-r",
        "--random-seed",
        type=int,
        default=None,
        help="Specify seed for random generator",
    )
    parser.add_argument(
        "-p",
        "--potential",
        default="fit",
        choices=available_potentials.keys(),
        help="intermolecular potential name",
    )
    parser.add_argument(
        "--cutoff",
        default=CONFIG.get("csp.cutoff"),
        help="dmacrys real space/repulsion-dispersion cutoff",
    )
    parser.add_argument(
        "--status-file",
        default=CONFIG.get("csp.status_file"),
        type=str,
        help="Specify output status file",
    )
    parser.add_argument(
        "--clg-old",
        action="store_true",
        help="Use the old version of the CLG",
    )
    parser.add_argument(
        "--log-level",
        default=CONFIG.get("csp.log_level"),
        help="Log level"
    )
    parser.add_argument(
        "--keep-files",
        action="store_true",
        default=False,
        help="Keep DMACRYS and NEIGHCRYS files which, for each structure, are stored in a new directory in the pwd."
    )

    args = parser.parse_args()
    logging.basicConfig(
        level=args.log_level,
        format='%(asctime)s - %(levelname)s - %(module)s %(lineno)d - '
               '%(message)s'
    )

    default_name = generate_combined_name(args.xyz_files)
    if args.axis is None:
        args.axis = default_name + ".mols"
        LOG.info("No axis file provided, trying %s", args.axis)
    if args.charges is None:
        args.charges = default_name + "_rank0.dma"
        LOG.info("No charges file provided, trying %s", args.charges)
    if args.multipoles is None:
        args.multipoles = default_name + ".dma"
        LOG.info("No multipole file provided, trying %s", args.multipoles)
    if not check_multipoles_valid(
        args.xyz_files, args.axis, args.charges, args.multipoles, args.potential
    ):
        LOG.error("No good matches for given multipoles! Exiting...")
        sys.exit(1)

    if args.cutoff == "calculate":
        cutoff = 15.0
        from scipy.spatial.distance import pdist
        from cspy import Molecule

        for x in args.xyz_files:
            mol = Molecule.load(x)
            cutoff = max(cutoff, 1.5 * np.max(pdist(mol.positions)))
    else:
        cutoff = float(args.cutoff)

    bonds = []
    bondlength_cutoffs = {}
    for x in args.xyz_files:
        mol = Molecule.load(x)
        mol.guess_bonds()
        bonds.append(mol.bonds.todense())
        for k, v in mol.neighcrys_bond_cutoffs().items():
            if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                bondlength_cutoffs[k] = v

    config = CspyConfiguration()
    config.set("neighcrys.potential", args.potential)
    mc_para = get_mc_setting()
    mc_para["bonds"] = bonds
    worker_data = {
        "charges": Path(args.charges).read_text(),
        "multipoles": Path(args.multipoles).read_text(),
        "axis": Path(args.axis).read_text(),
        "bondlength_cutoffs": bondlength_cutoffs,
        "asymmetric_unit": [Path(x).read_text() for x in args.xyz_files],
        "minimization": {
            "neighcrys": {
                "vdw_cutoff": cutoff,
                "potential": args.potential,
            },
            "pmin": {"timeout": config.get("pmin.timeout")},
            "dmacrys": {"timeout": config.get("dmacrys.timeout")},
            "minimization_steps": config.get("csp_minimization_step"),
        },
        "check_single_point_energy":
            config.get("csp.check_single_point_energy"),
        "nudge": args.nudge,
        "adaptcell" : args.adaptcell,
        "asi" : args.asi,
        "clg_old": args.clg_old,
        "mc": mc_para,
        "keep_files" : args.keep_files
    }
    LOG.info("Minimization settings: %s", pformat(worker_data["minimization"]))

    random.seed(args.random_seed)

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        comm.Split(0)
        csp_manager = QRBHCSP(
            workers=range(1, MPI.COMM_WORLD.Get_size()),
            name=default_name,
            spacegroups=args.spacegroups,
            mc_para=mc_para,
            number_structures=args.number_structures,
            status_file=args.status_file
        )
        csp_manager.run()
    else:
        comm.Split(1)
        QRBHWorker(worker_data).run()


if __name__ == "__main__":
    main()
