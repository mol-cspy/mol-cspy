import argparse
import sys
import logging

LOG = logging.getLogger("cspy-db")
USAGE = """cspy-db <command> [<args>]

Available commands are:
    prune       Remove duplicate structures from databases
    cluster     synonymous with 'prune'
    dump        Extract data from databases into other formats
"""


class DatabaseCLI:
    def __init__(self):
        parser = argparse.ArgumentParser(
            description="Process cspy databases", usage=USAGE
        )
        parser.add_argument("command", help="sub command to run")
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            LOG.error("Unrecognized command '%s'", args.command)
            parser.print_help()
            sys.exit(1)

        getattr(self, args.command)()

    def prune(self):
        self.cluster()

    def info(self):
        from cspy.db.info import main

        main(sys.argv[2:])

    def cluster(self):
        from cspy.db.clustering import main

        main(sys.argv[2:])

    def extract(self):
        from cspy.db.extract import main

        main(sys.argv[2:])

    def dump(self):
        from cspy.db.dump import main

        main(sys.argv[2:])


def main():
    DatabaseCLI()


if __name__ == "__main__":
    main()
