import logging
import sys
from pprint import pformat
import numpy as np
from mpi4py import MPI
from cspy import Molecule
from cspy.apps.dma import generate_combined_name
from cspy.chem.multipole import DistributedMultipoles
from cspy.configuration import CspyConfiguration
from cspy.crystal import Crystal
from cspy.cspympi import CSPyWorker
from cspy.distributed import QuasiRandomCSP
from cspy.potentials import available_potentials
from cspy.util.path import Path


LOG = logging.getLogger(__name__)


def check_multipoles_valid(xyz_files, axis, charges, multipoles, potential):
    """
    Checks whether the supplied multipole files are valid for the current calculation

    Args:
        xyz_files (string): XYZ filenames containing molecules for generation
        axis (string): DMACRYS axis filename for structure minimization
        charges (string): DMACRYS Rank 0 filename 
        multipoles (string): DMACRYS multipoles (Rank N) filename
        potential (string): Potential filename for structure minimization

    Returns:
        success (Boolean): True if the multipoles map onto the input molecules else False
    """
    c = DistributedMultipoles.from_dma_file(charges)
    if len(c.molecules) > 0:
        LOG.info("Succesfully read charges file: %s mols", len(c.molecules))
    m = DistributedMultipoles.from_dma_file(multipoles)
    if len(m.molecules) > 0:
        LOG.info(
            "Succesfully read higher order multipoles file: %s mols", len(m.molecules)
        )
    axis_contents = Path(axis).read_text()
    success = True
    for rank, multipoles in (("charges", c), ("multipoles", m)):
        crys = Crystal.from_xyz_files(xyz_files, titl="tmp")
        potential_type = available_potentials[potential][0]
        crys.neighcrys_setup(potential_type=potential_type, file_content=axis_contents)
        LOG.info("Mapping: %s", rank)
        reduction = 0.1 if potential_type == "W" else None
        mapping = crys.map_multipoles(multipoles, foreshorten_hydrogens=reduction)
        for i, (j, rmsd, inv) in enumerate(mapping):
            LOG.info(
                "%s: %5s idx=%d%s (%.3g): %s",
                xyz_files[i],
                rank,
                j,
                "'" if inv else "",
                rmsd,
                "ok" if rmsd < 1e-4 else "failed",
            )
            if rmsd > 1e-4:
                success = False
    return success


def main():
    import argparse
    from cspy.configuration import CONFIG
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "xyz_files",
        type=str,
        nargs="+",
        help="Xyz files containing molecules for generation",
    )
    parser.add_argument(
        "-c",
        "--charges",
        type=str,
        default=CONFIG.get("csp.charges_file"),
        help="Rank0 multipole file",
    )
    parser.add_argument(
        "-m",
        "--multipoles",
        type=str,
        default=CONFIG.get("csp.multipoles_file"),
        help="RankN multipole file",
    )
    parser.add_argument(
        "-g",
        "--spacegroups",
        type=str,
        default=CONFIG.get("csp.spacegroups"),
        help="Spacegroup set for structure generation",
    )
    parser.add_argument(
        "-a",
        "--axis",
        type=str,
        default=None,
        help="Axis filename for structure minimization",
    )
    parser.add_argument(
        "-n",
        "--number-structures",
        type=int,
        default=CONFIG.get("csp.number_structures"),
        help="Number of valid structures for structure generation",
    )
    parser.add_argument(
        "-p",
        "--potential",
        default=CONFIG.get("csp.potential"),
        choices=available_potentials.keys(),
        help="intermolecular potential name for struicture minimization",
    )
    parser.add_argument(
        "--cutoff",
        default=CONFIG.get("csp.cutoff"),
        help="DMACRYS real space/repulsion-dispersion cutoff",
    )
    parser.add_argument(
        "--status-file",
        default=CONFIG.get("csp.status_file"),
        type=str,
        help="Specify output status filename",
    )
    parser.add_argument(
        "--nudge",
        type=int,
        default=0,
        help="Nudge molecules in assymetric unit that fail QR step",
    )
    parser.add_argument(
        "--adaptcell",
        action="store_true",
        help="Adaptively optimise cell parameters",
    )
    parser.add_argument(
        "--asi",
        action="store_true",
        help="Allow molecules to have superimposed centroids (set to true for encapsulation)",
    )
    parser.add_argument(
        "--clg-old",
        action="store_true",
        help="Use the old version of the crystal landscape generator",
    )
    parser.add_argument(
        "--log-level",
        default=CONFIG.get("csp.log_level"),
        help="Log level"
    )
    parser.add_argument(
        "--keep-files",
        action="store_true",
        default=False,
        help="Keep DMACRYS and NEIGHCRYS files which, for each structure, are stored in a new directory in the pwd."
    )

    args = parser.parse_args()
    logging.basicConfig(
        level=args.log_level,
        format='%(asctime)s - %(levelname)s - %(module)s %(lineno)d - '
               '%(message)s'
    )

    default_name = generate_combined_name(args.xyz_files)
    if args.axis is None:
        args.axis = default_name + ".mols"
        LOG.info("No axis file provided, trying %s", args.axis)
    if args.charges is None:
        args.charges = default_name + "_rank0.dma"
        LOG.info("No charges file provided, trying %s", args.charges)
    if args.multipoles is None:
        args.multipoles = default_name + ".dma"
        LOG.info("No multipole file provided, trying %s", args.multipoles)
    if not check_multipoles_valid(
        args.xyz_files, args.axis, args.charges, args.multipoles,
        args.potential
    ):
        LOG.error("No good matches for given multipoles! Exiting...")
        sys.exit(1)

    if args.cutoff == "calculate":
        cutoff = 15.0
        from scipy.spatial.distance import pdist

        for x in args.xyz_files:
            mol = Molecule.load(x)
            if len(mol) == 1:
                continue
            cutoff = max(cutoff, 1.5 * np.max(pdist(mol.positions)))
    else:
        cutoff = float(args.cutoff)

    bondlength_cutoffs = {}
    for x in args.xyz_files:
        mol = Molecule.load(x)
        for k, v in mol.neighcrys_bond_cutoffs().items():
            if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                bondlength_cutoffs[k] = v

    config = CspyConfiguration()
    config.set("neighcrys.potential", args.potential)
    worker_data = {
        "charges": Path(args.charges).read_text(),
        "multipoles": Path(args.multipoles).read_text(),
        "axis": Path(args.axis).read_text(),
        "bondlength_cutoffs": bondlength_cutoffs,
        "asymmetric_unit": [Path(x).read_text() for x in args.xyz_files],
        "minimization": {
            "neighcrys": {
                "vdw_cutoff": cutoff,
                "potential": args.potential,
            },
            "pmin": {"timeout": config.get("pmin.timeout")},
            "dmacrys": {"timeout": config.get("dmacrys.timeout")},
            "minimization_steps": config.get("csp_minimization_step"),
        },
        "check_single_point_energy":
            config.get("csp.check_single_point_energy"),
        "nudge": args.nudge,
        "adaptcell" : args.adaptcell,
        "asi" : args.asi,
        "clg_old": args.clg_old,
        "keep_files" : args.keep_files
    }
    LOG.info("Minimization settings: %s", pformat(worker_data["minimization"]))

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        comm.Split(0)
        csp_manager = QuasiRandomCSP(
            workers=range(1, MPI.COMM_WORLD.Get_size()),
            name=default_name,
            spacegroups=args.spacegroups,
            number_structures=args.number_structures,
            status_file=args.status_file
        )
        csp_manager.run()
    else:
        comm.Split(1)
        CSPyWorker(worker_data).run()


if __name__ == "__main__":
    main()
