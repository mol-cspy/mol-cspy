import argparse
import logging
from pathlib import Path
from cspy import Crystal
from cspy.potentials import available_potentials
from cspy.util.logging_config import FORMATS, DATEFMT
from cspy.chem.multipole import DistributedMultipoles


LOG = logging.getLogger("cspy.opt")


def main():
    from cspy.configuration import CONFIG
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "crystal_structures", nargs="+", type=str, help="Crystal structures to minimize"
    )
    parser.add_argument(
        "--basis-set",
        "-b",
        default="6-311G**",
        help="Basis set for Gaussian calculation "
        "(a string passed verbatim into Gaussian "
        "input file, e.g. 6-311G**",
    )
    parser.add_argument(
        "--method",
        "-m",
        default="PBEPBE",
        help="Method for Gaussian calculation "
        "(a string passed verbatim into Gaussian "
        "input file, e.g. B3LYP",
    )
    parser.add_argument(
        "-p",
        "--potential",
        default=CONFIG.get("csp.potential"),
        choices=available_potentials.keys(),
        help="Intermolecular potential name",
    )
    parser.add_argument(
        "--charges",
        default=0,
        help='''Charges of each molecule to be read by Gaussian. 
        For example, a salt like NaCl would be specified as "+1 -1"''',
    )
    parser.add_argument(
        "--cutoff",
        default="calculate",
        help="DMACRYS real space/repulsion-dispersion cutoff",
    )
    parser.add_argument(
        "--multipole-rank",
        "-r",
        default=4,
        type=int,
        help="Maximum angular momenta for DMA",
    )
    parser.add_argument(
        "--mulfit-rank",
        default=None,
        type=int,
        help="Maximum angular momenta for MULFIT",
    )
    parser.add_argument(
        "--mulfit-method",
        default="cumulative",
        choices=("cumulative", "overall"),
        help="Method for fitting in MULFIT",
    )
    parser.add_argument(
        "--MAXI",
        default=CONFIG.get("neighcrys.max_iterations"),
        help="Max number of iterations to use in DMACRYS",
    )
    parser.add_argument(
        "--single-point",
        action="store_true",
        default=False,
        help="Calculate a single point energy (don't optimize) with DMACRYS",
    )
    parser.add_argument(
        "--multipole",
        default=None,
        help='''Multipoles file to be used in a DMACRYS calculation. 
        If not specified, a new axis will be created''',
    )
    parser.add_argument(
        "--axis",
        type=str,
        default=None,
        help='''Axis file to be used in a DMACRYS calculation. 
        If not specified, a new axis will be created''',
    )
    parser.add_argument(
        "--dmacrys-timeout",
        default=CONFIG.get("dmacrys.timeout"),
        type=float,
        help="Timeout for dmacrys calculations",
    )
    parser.add_argument(
        "--log-level",
        default="INFO",
        choices=("INFO", "DEBUG", "WARN", "ERROR"),
        help="Logging verbosity",
    )
    parser.add_argument(
        "--outputs",
        default=False,
        action="store_true",
        help="Write the final output files to the working directory",
    )
    parser.add_argument(
        "--cleanup",
        default=True,
        action="store_true",
        help="Remove intermediate files (fchk, individual dma files)",
    )
    parser.add_argument(
        "--no-cleanup",
        action="store_false",
        dest="cleanup",
        help="Don't remove intermediate files (fchk, individual dma files)",
    )
    parser.add_argument(
        "--gaussian-cpus", "-j", 
        default=2, 
        type=int, 
        help="Number of cpus to run Gaussian"
    )
    parser.add_argument(
        "--gaussian-mem", 
        default="2GB", 
        type=str, 
        help="Memory setting for Gaussian"
    )
    parser.add_argument(
        "--dma-switch", 
        default=4, 
        type=str, 
        help="dma switch",
    )
    parser.add_argument(
        "--pcm",
        default=None,
        type=str,
        help="Instructs Gaussian to use a PCM model with epsilon set to the specified value.",
    )
    parser.add_argument(
        "--dftb-calc",
        action="store_true",
        default=False,
        help="Perform a dftb calculation instead of using DMACRYS",
    )
    args = parser.parse_args()
    logging.basicConfig(
        format=FORMATS[args.log_level], datefmt=DATEFMT, level=args.log_level
    )
    kwargs = vars(args)
    bondlength_cutoffs = {}
    if args.multipole is not None:
        mols = DistributedMultipoles.from_dma_file(args.multipole).molecules
        for mol in mols:
            for k, v in mol.neighcrys_bond_cutoffs().items():
                if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                    bondlength_cutoffs[k] = v

    for crystal_file in args.crystal_structures:
        crystal = Crystal.load(crystal_file)
        if args.dftb_calc:
            LOG.info("Minimizing %s with dftb", crystal)
            result = crystal.minimize_with_dftb(**kwargs)
        else:
            if bondlength_cutoffs:
                crystal.properties["bondlength_cutoffs"] = bondlength_cutoffs
            if args.multipole is None:
                suffix = ".".join(
                    (args.method, args.basis_set, ("dma" + str(args.multipole_rank)))
                )
            else:
                suffix = ".".join((args.method, args.multipole))
                crystal.titl = Path(crystal_file).stem + "." + suffix
            LOG.info("Minimizing %s", crystal)
            result = crystal.minimize(**kwargs)
        if result:
            try:
                result.save(crystal.titl + ".opt.cif")
            except AttributeError as exc:
                LOG.error(f"{exc}: Cannot save structure from failed minimization.")


if __name__ == "__main__":
    main()
