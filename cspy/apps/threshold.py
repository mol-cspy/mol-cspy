import logging
import sys
import numpy as np
import random
from pprint import pformat
from mpi4py import MPI
from cspy.configuration import CspyConfiguration, CONFIG
from cspy.chem import Molecule
from cspy.apps.dma import generate_combined_name
from cspy.util.logging_config import FORMATS, DATEFMT
from cspy.apps.csp import check_multipoles_valid
from cspy.cspympi.threshold_tasks import ThresholdWorker
from cspy.distributed.threshold_manager import Threshold
from cspy.potentials import available_potentials
from cspy.util.path import Path

LOG = logging.getLogger(__name__)

def get_mc_setting():
    config = CspyConfiguration()
    mc_para = {
        "trial_step": config.get("mc.num_steps"),#Numebr of maximum steps for one threshold trial
        "sat_expand": config.get("mc.sat_expand"),#Whether to sat-expand after perturbation
        "move_all": config.get("mc.move_all"),#Whether to apply all available types of move at one perturbation, don't use unless specific interest
        "auto_prob": config.get("mc.auto_prob"),#Calculate probability of choosing move type according to degrees of freedom
        "auto_cutoff": config.get("mc.auto_cutoff"),#Calculate cutoff of move type, currently only applied to volume expansion and contraction based on number of molecules in unit cell
        "cluster_s": config.get("mc.on_the_fly"),#Whether to use on-the-fly clustering, if Ture, continue_running will always be False
        "move": config.get("mc.move"),#Specified move details
        "move_scale": config.get("mc.move_scale"),#Scale of move step size with lid states increasing
        "dump_accept": config.get("basin_hopping.dump_accept"),#Only dump accepted structures if True
        "interval_para": config.get("threshold.interval_para"),#Parameters for deciding intervals (number of steps) under each lid state
        "increase_para": config.get("threshold.increase_para"),#Parameters for deciding increase energy at each lid
        "minimize_s": config.get("threshold.minimize"),#Minimize after perturbation for basin information
        "min_energy": config.get("threshold.min_energy"),#Set minimal energy for threshold, would use minimized energy of initial structure if not
    }
    return mc_para

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "xyz_files",
        type=str,
        nargs="+",
        help="Xyz files containing molecules for generation",
    )
    parser.add_argument(
        "-r",
        "--res_files",
        type=str,
        nargs="+",
        help="Res files containing molecules for generation",
    )
    parser.add_argument(
        "-c",
        "--charges",
        type=str,
        default=CONFIG.get("csp.charges_file"),
        help="Rank0 multipole file",
    )
    parser.add_argument(
        "-m",
        "--multipoles",
        type=str,
        default=CONFIG.get("csp.multipoles_file"),
        help="RankN multipole file",
    )
    parser.add_argument(
        "-g",
        "--spacegroups",
        type=str,
        default=CONFIG.get("csp.spacegroups"),
        help="Spacegroup for structure generation",
    )
    parser.add_argument(
        "-a",
        "--axis",
        type=str,
        default=None,
        help="Axis filename for structure minimization",
    )
    parser.add_argument(
        "-t",
        "--nthreads",
        type=int,
        default=1,
        help="Number of worker threads per process to use for local scheduler",
    )
    parser.add_argument(
        "-rs",
        "--random-seed",
        type=int,
        default=None,
        help="Specify seed for random generator",
    )
    parser.add_argument(
        "-p",
        "--potential",
        default="fit",
        choices=available_potentials.keys(),
        help="intermolecular potential name",
    )
    parser.add_argument(
        "--cutoff",
        default=CONFIG.get("csp.cutoff"),
        help="dmacrys real space/repulsion-dispersion cutoff",
    )
    parser.add_argument(
        "--status-file",
        default=CONFIG.get("csp.status_file"),
        type=str,
        help="Specify output status file",
    )
    parser.add_argument(
        "--trials-per-res",
        default=1,
        type=int,
        help="number of trials/trajectories initiated for each res provided",
    )
    parser.add_argument(
        "--log-level",
        default=CONFIG.get("csp.log_level"),
        help="Log level"
    )

    args = parser.parse_args()
    logging.basicConfig(
        format=FORMATS[args.log_level], datefmt=DATEFMT, level=args.log_level
    )

    default_name = generate_combined_name(args.xyz_files)
    if args.axis is None:
        args.axis = default_name + ".mols"
        LOG.info("No axis file provided, trying %s", args.axis)
    if args.charges is None:
        args.charges = default_name + "_rank0.dma"
        LOG.info("No charges file provided, trying %s", args.charges)
    if args.multipoles is None:
        args.multipoles = default_name + ".dma"
        LOG.info("No multipole file provided, trying %s", args.multipoles)
    if not check_multipoles_valid(
        args.xyz_files, args.axis, args.charges, args.multipoles, args.potential
    ):
        LOG.error("No good matches for given multipoles! Exiting...")
        sys.exit(1)

    if args.cutoff == "calculate":
        cutoff = 15.0
        from scipy.spatial.distance import pdist
        from cspy import Molecule

        for x in args.xyz_files:
            mol = Molecule.load(x)
            cutoff = max(cutoff, 1.5 * np.max(pdist(mol.positions)))
    else:
        cutoff = float(args.cutoff)

    bonds = []
    bondlength_cutoffs = {}
    for x in args.xyz_files:
        mol = Molecule.load(x)
        mol.guess_bonds()
        bonds.append(mol.bonds.todense())
        for k, v in mol.neighcrys_bond_cutoffs().items():
            if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                bondlength_cutoffs[k] = v

    config = CspyConfiguration()
    config.set("neighcrys.potential", args.potential)

    elec_option = config.get("csp_minimization_step")[-1]["electrostatics"]
    if elec_option == "charges":
        electrostatics = Path(args.charges).read_text()
    elif elec_option == "multipoles":
        electrostatics = Path(args.multipoles).read_text()

    mc_para = get_mc_setting()
    mc_para["bonds"] = bonds
    worker_data = {
        "charges": Path(args.charges).read_text(),
        "multipoles": Path(args.multipoles).read_text(),
        "electrostatics": electrostatics,
        "axis": Path(args.axis).read_text(),
        "bondlength_cutoffs": bondlength_cutoffs,
        "minimization": {
            "neighcrys": {
                "vdw_cutoff": cutoff,
                "potential": args.potential,
            },
            "pmin": {"timeout": config.get("pmin.timeout")},
            "dmacrys": {"timeout": config.get("dmacrys.timeout")},
            "minimization_steps": config.get("csp_minimization_step"),
        },
        "check_single_point_energy":
            config.get("csp.check_single_point_energy"),
        "mc": mc_para,
    }
    LOG.info("Minimization settings: %s", pformat(worker_data["minimization"]))

    random.seed(args.random_seed)

    res_files = []
    for res in args.res_files:
        res_files.extend([res for i in range(args.trials_per_res)])

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        comm.Split(0)
        csp_manager = Threshold(
            workers=range(1, MPI.COMM_WORLD.Get_size()),
            name=default_name,
            spacegroups=args.spacegroups,
            res_files=res_files,
            mc_para=mc_para,
            status_file=args.status_file
        )
        csp_manager.run()
    else:
        comm.Split(1)
        ThresholdWorker(worker_data).run()


if __name__ == "__main__":
    main()
