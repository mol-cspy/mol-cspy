import copy
from .executable import AbstractExecutable, ReturnCodeError
from os import environ
import logging
from cspy.util.path import Path
from .locations import which


DFTB_EXEC = which("dftb+")
LOG = logging.getLogger("dftb")

class Dftb(AbstractExecutable):
    """An executable class for dftb+ calculations
    """
    _executable_location = DFTB_EXEC
    _OUT_LOGFILE_FMT = "{}_dftbplus.out"
    _OUT_GEOM_FMT = "{}.gen"

    def __init__(self, 
                 input_hsd: str,
                 input_coords: str,
                 crys_name: str = "crys",
                 working_directory: str = ".",
                 output_prefix: str = "geom.out",
                 timeout: float = 1200.0,
                 **kwargs: dict) -> None:
        """Initialises an executable class for dftb+ calculations
            
        Args:
            input_hsd (str): File name of the input hsd file.
            input_coords (str): File name of the input geometry file.
            crys_name (str): Name for the input structure. Defaults to "crys"
            working_directory (str): Path to the working directory. Defaults to "."
            output_prefix (str): Prefix for the output geometry file. Defaults to "geom.out"
            timeout (float): Timeout length in seconds for the calculation. Defaults to 1200.0
            kwargs (Dict): Dictionary of keyword arguments. (Not currently used)
        """
        self.timeout = timeout 
        self.crys_name = crys_name
        self.input_hsd = input_hsd
        self.input_coords = input_coords
        self.working_directory = working_directory
        self.output_prefix = output_prefix
        self.output_contents = None
        self.log_contents = None
        self.opt_coords = None
        self.error_contents = None
        
    @property
    def input_file(self) -> Path:
        """Path to the input file used for dftb+ calculations

        Returns:
            Path: A Path object for the input file
        """
        return Path(self.working_directory, self.input_hsd)
    
    @property
    def input_geometry(self) -> Path:
        """Path to the input geomery file used for dftb+ calculations

        Returns:
            Path: A Path object for the geometry input file
        """
        return Path(self.working_directory, self.input_coords)
    
    @property
    def output_file(self) -> Path:
        """Path to the output file from a dftb+ calculation

        Returns:
            Path: A Path object for the output file
        """
        return Path(self.working_directory, self._OUT_LOGFILE_FMT.format(self.crys_name))
    
    @property
    def output_geometry(self) -> Path:
        """Path to the output geomery file from a dftb+ calculations

        Returns:
            Path: A Path object for the output geometry file
        """
        return Path(self.working_directory, self._OUT_GEOM_FMT.format(self.output_prefix)) 
        
    def post_process(self) -> None:
        """Run post-processing
        """
        with open(self.output_file) as f:
            self.output_contents = f.read()
        with open(self.input_file) as f:
            self.input_contents = f.read()
        with open(self.input_geometry) as f:
            self.input_geometry_contents = f.read()

    def result(self) -> str:
        """Return output_contents content
        """    
        return self.output_contents
 
    def resolve_dependencies(self) -> None:
        """ Do whatever needs to be done before running
        the job (e.g. write input file etc.)"""
        pass
        
    def run(self, *args, **kwargs) -> None:
        """Run a dftb+ calculation

        Raises:
            e: A ReturnCodeError is raised if dftb+ fails.
        """
        env = copy.deepcopy(environ)
        env.update({"OMP_NUM_THREADS": "1"})
        try:
            self._run_raw_stdin()
        except ReturnCodeError as e:
            LOG.error("Dftb+ failed: %s", e)
            self.post_process()
            LOG.debug("Output log: %s", self.output_contents)
            LOG.debug("Input file: %s", self.input_contents)
            LOG.debug("Input geometry: %s", self.input_geometry_contents)
            raise e
            

