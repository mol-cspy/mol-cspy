TITL 10.largesalt_WEMGEK.res
CELL 1.0    6.0441    7.2056   18.5493   90.0000  103.6331   90.0000
ZERR    2 0.0 0.0 0.0 0.0 0.0 0.0
LATT  -1
SYMM -X , +Y +1/2, -Z 
SFAC C N O H 
C1          1   0.20850945   0.16221777   0.27673305
C2          1   0.09605728  -0.01296209   0.29908993
C3          1   0.15934546  -0.04562940   0.38211484
C4          1   0.01151408   0.00232295   0.42606455
C5          1   0.07406155  -0.02731262   0.50199283
C6          1   0.28364450  -0.10476426   0.53428093
C7          1   0.43110130  -0.15391696   0.49047516
C8          1   0.36865552  -0.12499998   0.41463843
C9          1   0.13340528   0.34426650   0.30509385
C10         1   0.35352984   0.22372342   0.16052438
C11         1   0.77622722   0.30163399   0.95610002
C12         1   0.86053715   0.18808265   0.89906339
C13         1   1.11091014   0.22277735   0.90218763
C14         1   1.20173536   0.09909711   0.84668353
N1          2   0.15774605   0.16923449   0.19209932
O1          3  -0.13859199   0.00568576   0.26901988
O2          3   0.61186594   0.39859705   0.94359715
O3          3   0.89788525   0.27532457   1.02526267
O4          3   0.72883662   0.22843596   0.82865565
O5          3   1.13544634   0.41475510   0.89335122
O6          3   1.27705920   0.18066251   0.79966495
O7          3   1.18633944  -0.06818793   0.85920235
H1          4   0.39129886   0.14704376   0.29484241
H2          4   0.16153641  -0.13007236   0.27313322
H3          4  -0.15222191   0.06038810   0.40103313
H4          4  -0.04141132   0.00944238   0.53575433
H5          4   0.33135595  -0.12800603   0.59315231
H6          4   0.59296661  -0.21592652   0.51516621
H7          4   0.48308355  -0.16666353   0.38095622
H8          4  -0.04851120   0.36451814   0.28527637
H9          4   0.17399111   0.34256986   0.36502542
H10         4   0.22161851   0.46121802   0.28782601
H11         4   0.42000960   0.35480416   0.18429705
H12         4   0.48277140   0.11775369   0.17442507
H13         4   0.29556665   0.23616653   0.10103435
H14         4   0.02590835   0.25643740   0.17400339
H15         4   0.10006363   0.04450653   0.17134313
H16         4  -0.20178133  -0.10278548   0.24394340
H17         4   0.82961430   0.34756028   1.05675695
H18         4   0.84681659   0.04259696   0.91307564
H19         4   0.57382265   0.21183065   0.82808931
H20         4   1.20388994   0.18026336   0.95738672
H21         4   1.29129389   0.43803051   0.89755937
END
