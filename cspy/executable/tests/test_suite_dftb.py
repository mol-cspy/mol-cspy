from cspy.crystal import Crystal
from cspy.chem.molecule import Molecule
from cspy.executable import Dftb
from cspy.configuration import CONFIG
from cspy.formats.dftb_input import write_dftb_inputs
from cspy.formats.dftb_output import DftbOutput
from os.path import dirname, join, abspath, splitext
from os import remove
import logging
import sys
import unittest
import pytest
from filecmp import cmp
from cspy.executable.locations import DFTB_EXEC

if sys.version.startswith("2"):
    from backports.tempfile import TemporaryDirectory
else:
    from tempfile import TemporaryDirectory

LOG = logging.getLogger(__name__)

class DftbTestCase(unittest.TestCase):

    def default_settings(self): 
        return {
                "skf_set":"3ob-3-1",
                "skf_path":join(dirname(__file__),'test_suite_dftb/parameter_sets/'),
                "disp_coeff":"3ob_brandenburg",
                "charge":0,
                "groups":1,
                "max_steps":2500,
                "max_force":0.00058,
                "alg":"LBFGS",
                "output_prefix":"geom.out",
                "kpoint_spacing":0.05,
                "scc_tol":1e-5,
                "timeout":600,
        }


    def run_dftb(self, input_file, settings):
        """Function that can be called by tests to run dftb+

        Args:
            input_file (str): Filename of a crystal structure (e.g. CIF/SHELX) or molecule (e.g. .XYZ)
            settings (Dict): Dictionary of settings for dftb+ calculation

        Returns:
            _type_: _description_
        """
        with TemporaryDirectory() as tmpdirname:
            LOG.debug("created temp directory: %s", tmpdirname)          
            self.writing_inputs(input_file, tmpdirname, settings) 
            exe = Dftb('dftb_in.hsd',
                       'geometry.gen',
                       working_directory=tmpdirname)
            exe.run()
        return exe


    def writing_inputs(self, input_file, working_directory, settings):
        """Function that can be called by tests to create input files

        Args:
            input_file (str): Filename of a crystal structure (e.g. CIF/SHELX) or molecule (e.g. .XYZ)
            working_directory (str): Path to the working directory
            settings (Dict): Dictionary of settings for dftb+ calculation
        """
        if splitext(input_file)[-1] == '.cif':
            structure = Crystal.load(input_file)
        else:
            structure = Molecule.load(input_file)
        write_dftb_inputs(structure, working_directory, settings)
    

    def combine_pytest_decorators(decorators):
        """Function for combining multiple pytest decorators that
        are reused on multiple tests
        """
        def combine(func):
            for dec in reversed(decorators):
                func = dec(func)
            return func
        return combine


    default_test_marks = combine_pytest_decorators([pytest.mark.external_binaries,
                                            pytest.mark.skipif(DFTB_EXEC == None, reason='dftb+ not found in $PATH')])
    slow_test_marks = combine_pytest_decorators([pytest.mark.external_binaries,
                                         pytest.mark.skipif(DFTB_EXEC == None, reason='dftb+ not found in $PATH'),
                                         pytest.mark.slow])

    @default_test_marks
    def test_dftb_01(self):
        """Runs a single-point energy calculation using dftb+ for ACETAC01
        """
        specific_settings = {
            "single_point": True,
            "atomic_pos_opt": False,
            "lattice_and_atoms_opt": False,
        }
        # combine default settings with specific test settings
        settings = {**self.default_settings(), **specific_settings}
        cif_file = join(dirname(__file__),'test_suite_dftb/01.singlepoint_ACETAC01_crystal/inputs/ACETAC01.cif')
        calc = self.run_dftb(cif_file, settings)
        output = DftbOutput(calc.output_contents)
        self.assertTrue(output._valid(single_point=True))
        properties = output._parse_file()
        # We use assertAlmostEqual because different software versions/compilations 
        # will likely give slightly different results
        self.assertAlmostEqual(-46.3853370962, 
                               float(properties['final_energy']), 
                               places=7)
        

    @slow_test_marks
    def test_dftb_02(self):
        """Runs a lattice and atomic positions optimisation using dftb+ for ACETAC01
        """
        specific_settings = {
            "single_point": False,
            "atomic_pos_opt": False,
            "lattice_and_atoms_opt": True,
        }
        # combine default settings with specific test settings
        settings = {**self.default_settings(), **specific_settings}
        LOG.debug(settings)
        cif_file = join(dirname(__file__),'test_suite_dftb/02.optimization_ACETAC01_crystal/inputs/ACETAC01.cif')
        calc = self.run_dftb(cif_file, settings)
        output = DftbOutput(calc.output_contents)
        self.assertTrue(output._valid())
        properties = output._parse_file()
        # We use assertAlmostEqual because different software versions/compilations 
        # will likely give slightly different results
        self.assertAlmostEqual(-46.4063421694, 
                         float(properties['final_energy']),
                         places=7)


    @slow_test_marks
    def test_dftb_03(self):
        """Runs an atomic positions optimisation using dftb+ for UREAXX
        """
        specific_settings = {
            "single_point": False,
            "atomic_pos_opt": True,
            "lattice_and_atoms_opt": False,
        }
        # combine default settings with specific test settings
        settings = {**self.default_settings(), **specific_settings}
        LOG.debug(settings)
        cif_file = join(dirname(__file__),'test_suite_dftb/03.optimization_UREAXX_crystal/inputs/UREAXX.cif')
        calc = self.run_dftb(cif_file, settings)
        output = DftbOutput(calc.output_contents)
        self.assertTrue(output._valid())
        properties = output._parse_file()
        # We use assertAlmostEqual because different software versions/compilations 
        # will likely give slightly different results
        self.assertAlmostEqual(-22.7701037337, 
                         float(properties['final_energy']),
                         places=7)


    @default_test_marks
    def test_dftb_04(self):
        """Runs a single-point energy calculation using dftb+ for ROY (molecule in QAXMEH)
        """
        specific_settings = {
            "single_point": True,
            "atomic_pos_opt": False,
            "lattice_and_atoms_opt": False,
        }
        # combine default settings with specific test settings
        settings = {**self.default_settings(), **specific_settings}
        LOG.debug(settings)
        xyz_file = join(dirname(__file__),'test_suite_dftb/04.singlepoint_QAXMEH_molecule/inputs/ROY_QAXMEH.xyz')
        calc = self.run_dftb(xyz_file, settings)
        output = DftbOutput(calc.output_contents)
        self.assertTrue(output._valid(single_point=True))
        properties = output._parse_file()
        # We use assertAlmostEqual because different software versions/compilations 
        # will likely give slightly different results
        self.assertAlmostEqual(-39.9102939255, 
                         float(properties['final_energy']),
                         places=7)
         

    @default_test_marks
    def test_dftb_05(self):
        """Runs an atomic positions optimisation using dftb+ for ROY (molecule in QAXMEH)
        """
        specific_settings = {
            "single_point": False,
            "atomic_pos_opt": True,
            "lattice_and_atoms_opt": False,
        }
        # combine default settings with specific test settings
        settings = {**self.default_settings(), **specific_settings}
        LOG.debug(settings)
        xyz_file = join(dirname(__file__),'test_suite_dftb/05.optimization_QAXMEH_molecule/inputs/ROY_QAXMEH.xyz')
        calc = self.run_dftb(xyz_file, settings)
        output = DftbOutput(calc.output_contents)
        self.assertTrue(output._valid())
        properties = output._parse_file()
        # We use assertAlmostEqual because different software versions/compilations 
        # will likely give slightly different results
        self.assertAlmostEqual(-40.0328213794, 
                         float(properties['final_energy']),
                         places=7)


