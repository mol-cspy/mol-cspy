import hashlib
import os
import toml
import logging
from cspy.potentials import available_potentials
from cspy.util import recursive_dict_update, nested_dict_delete

try:
    from collections import ChainMap
except ImportError:
    from chainmap import ChainMap

MOST_COMMON_SPACEGROUPS = {
    "single": [
        61,
        14,
        19,
        2,
        4,
        15,
        33,
        9,
        29,
        5,
        1,
        60,
        7,
        18,
        96,
        76,
        145,
        43,
        56,
        13,
        169,
        88,
        20,
        86,
        148,
    ],
    "co-crystal": [2, 14, 15, 4, 19, 61, 1, 33, 5, 9],
}

CONFIG = None
LOG = logging.getLogger(__name__)

DEFAULTS = {
    "TIMEOUT": "1500.0",
    "NUMBER_STRUCTURES": 10000,
    "VDW_CUTOFF": "15.0",
    "gaussian": {
        "nprocs": 1,
        "memory": "2GB",
        "method": "B3LYP",
        "basis": "6-311G**",
        "basis_file": None,
        "chelpg": None,
        "cleanup": None,
        "calculate_molecular_volume": None,
        "polarizable_continuum_model": None,
        "external_iteration_pcm": None,
        "dielectric_constant": None,
        "optimisation_options": None,
        "iso_value": None,
        "frequency_options": None,
        "all_molecules": None,
        "extra_arguments": None,
        "molecular_states": None,
        "force_rerun": None,
    },
    "neighcrys": {
        "extended_mode": True,
        "bondlength_filename": "bondlengths",
        "max_intermolecular_distance": 4.0,
        "insert_bond_centre_sites": False,
        "standardise_bonds": False,
        "wcal_ngcv": True,
        "max_iterations": 1000,
        "step_size": 0.5,
        "connectivity_search_max_cells": 3,
        "potential": "fit",
        "potential_type": available_potentials["fit"][0],
        "labels_filename": "",
        "foreshorten_hydrogens": False,
        "symmetry_subgroup": [0],
        "vdw_cutoff": 15.0,
        "multipole_filename": "",
        "polarizabilities_filename": "",
        "axis_filename": "",
        "paste_molecular_structure_filename": "",
        "potential_filename": available_potentials["fit"][1],
        "hessian_filename": ""
    },
    "pmin": {
        "iswitch": 2,
        "irosen": 2,
        "iuser_e": 0,
        "irot": 0,
        "isystem": 0,
        "nmol": 1,
        "sthl": 0.5,
        "ck": 0.25,
        "dmax": 10.0,
        "ddmax": 50,
        "icross": 0,
        "allow_change": 0,
        "coefftable": 0,
        "ls_cycles": 50,
        "rss_cycles": 50,
        "steps": [1e-5, 1e-5, 1e-5, 1e-5],
        "frac_changes": [1.0, 1.0, 1.0, 1.0],
        "timeout": 600.0,
    },
    "dmacrys": {
        "timeout": 1800.0,
        "timeout_auto": False,
        "timeout_auto_cap": 1800.0,
        "timeout_auto_sample": 1000.0,
    },
    "csp_minimization_step": [
        {"kind": "pmin", "electrostatics": "charges"},
        {
            "kind": "dmacrys",
            "CONP": True,
            "PRES": "0.1 GPa",
            "electrostatics": "charges",
        },
        {"kind": "dmacrys", "electrostatics": "multipoles"},
    ],
    "csp": {
        "spacegroups": "fine10",
        "number_structures": None,
        "potential": "fit",
        "cutoff": "calculate",
        "status_file": "status.txt",
        "charges_file": None,
        "multipoles_file": None,
        "log_level": "INFO",
        "check_single_point_energy": False,
        "backup_interval":10000
    },
    # current list of keywords that are accepted for running a dftb+ calculation
     "dftb": {
        "skf_set":"3ob-3-1",
        "skf_path":"TO_BE_ADDED_BY_USER",
        "disp_coeff":"3ob_brandenburg",
        "charge":0,
        "groups":1,
        "max_steps":2500,
        "max_force":0.00058,
        "alg":"LBFGS",
        "output_prefix":"geom.out",
        "kpoint_spacing":0.05,
        "scc_tol":1e-5,
        "timeout":1800,
        "single_point": False,
        "atomic_pos_opt": True,
        "lattice_and_atoms_opt": False,
    },
}

OPT_SCHEMES = {
    # DMACRYS optimisation schemes from fastest (top) to most accurate (bottom)
    "fast&loosest" : {
        "LIMI" : "0.01",
        "LIMG" : "0.01",
        "MAXD"  : "1.0",
        "UDTE" : "1000000000"
    },
    "fast&very_loose" : {
        "LIMI" : "0.001",
        "LIMG" : "0.001",
        "MAXD"  : "1.0",
        "UDTE" : "1000000000"
    },
    "fast&loose" : {
        "LIMI" : "0.0001",
        "LIMG" : "0.0001",
        "MAXD"  : "1.0",
        "UDTE" : "1000000000"
    },
    "loose" : {
        "LIMI" : "0.0001",
        "LIMG" : "0.0001",
        "MAXD"  : "0.5",
        "UDTE" : "1000000000"
    },
    "precise" : {
        "LIMI" : "0.00001",
        "LIMG" : "0.00001",
        "MAXD"  : "0.5",
        "UDTE" : "0"
    },
    "very_precise" : {
        "LIMI" : "0.000001",
        "LIMG" : "0.0000000001",
        "MAXD"  : "0.5",
        "UDTE" : "0"
    }
}

COMMON_SAMPLING_SETTINGS = {
    "efficientz1": {
        # efficient sampling for organic semiconductors Z'=1 
        "space_group": {14, 19, 2, 4, 61, 15, 33, 9, 29},
        "number_structures": {
            14: 5000,
            19: 1000,
            2: 1000,
            4: 1000,
            61: 1000,
            15: 1000,
            33: 1000,
            9: 1000,
            29: 1000
        },
    },
    "efficientz2": {
        # efficient sampling for organic semiconductors Z'=2 
        "space_group": {2, 14, 4},
        "number_structures": {
            2: 12500,
            14: 7500,
            4: 7500
        },
    },
    "coarse10": {
        # 10 most common spacegroups
        "space_group": MOST_COMMON_SPACEGROUPS["single"][:10],
        "number_structures": {x: 1000 for x in MOST_COMMON_SPACEGROUPS["single"]},
    },
    "coarse25": {
        # 25 most common spacegroups
        "space_group": MOST_COMMON_SPACEGROUPS["single"],
        "number_structures": {x: 1000 for x in MOST_COMMON_SPACEGROUPS["single"]},
    },
    "fine10": {
        # 10 most common spacegroups
        "space_group": MOST_COMMON_SPACEGROUPS["single"][:10],
        "number_structures": {x: 10000 for x in MOST_COMMON_SPACEGROUPS["single"][:10]},
    },
    "fine25": {
        # 25 most common spacegroups
        "space_group": MOST_COMMON_SPACEGROUPS["single"],
        "number_structures": {x: 10000 for x in MOST_COMMON_SPACEGROUPS["single"]},
    },
    "co-crystals_fine": {
        # 10 most common spacegroups for co-crystals
        "space_group": {2, 14, 15, 4, 19, 61, 1, 33, 5, 9},
        "number_structures": {
            2: 10000,
            19: 10000,
            4: 20000,
            61: 20000,
            14: 50000,
            15: 50000,
            1: 10000,
            33: 20000,
            5: 20000,
            9: 20000,
        },
    },
    "co-crystals_coarse": {
        # 6 most common spacegroups for co-crystals
        "space_group": {2, 14, 15, 4, 19, 61},
        "number_structures": {2: 2500, 14: 5000, 15: 5000, 4: 2500, 19: 3000, 61: 3000},
    },
}


OLD_ARGS_TO_NEW = {
    "gaussian": {
        "nprocs": "gaussian.nprocs",
        "functional": "gaussian.method",
        "basis_set": "gaussian.basis",
        "basis_file": "gaussian.basis_file",
        "memory": "gaussian.memory",
        "chelpg": "gaussian.chelpg",
        "gaussian_cleanup": "gaussian.cleanup",
        "molecular_volume": "gaussian.calculate_molecular_volume",
        "vol": "gaussian.calculate_molecular_volume",
        "polarizable_continuum": "gaussian.polarizable_continuum_model",
        "external_iteration_pcm": "gaussian.external_iteration_pcm",
        "dielectric_constant": "gaussian.dielectric_constant",
        "esp": "gaussian.dielectric_constant",
        "opt": "gaussian.optimisation_options",
        "gopt": "gaussian.optimisation_options",
        "iso": "gaussian.iso_value",
        "freq": "gaussian.frequency_options",
        "gaussian_all_molecules": "gaussian.all_molecules",
        "additional_args": "gaussian.extra_arguments",
        "set_molecular_states": "gaussian.molecular_states",
        "force_rerun": "gaussian.force_rerun",
    },
    "neighcrys": {
        "potential_file": "neighcrys.potential_filename",
        "bondlength_file": "neighcrys.bondlength_filename",
        "multipole_file": "neighcrys.multipole_filename",
        "axis_file": "neighcrys.axis_filename",
        "lattice_factor": "neighcrys.lattice_factor",
        "step_size": "neighcrys.step-size",
        "standardise_bonds": "neighcrys.standardise_bonds",
        "intermolecular_distance": "neighcrys.max_intermolecular_distance",
        "max_iterations": "neighcrys.max_iterations",
        "vdw_cutoff": "neighcrys.vdw_cutoff",
        "foreshorten_hydrogens": "neighcrys.foreshorten_hydrogens",
        "wcal_ngcv": "neighcrys.wcal_ngcv",
        "force_field_type": "neighcrys.forcefield_type",
        "neighcrys_input_mode": "neighcrys.input_mode",
        "max_search": "neighcrys.connectivity_search_max_cells",
        "check_anisotropy": "neighcrys.check_anisotropy",
        "custom_atoms_anisotropy": "neighcrys.custom_anisotropy",
        "custom_labels": "neighcrys.custom_labels",
        "pressure": "neighcrys.pressure",
        "limi": "neighcrys.limi",
        "limg": "neighcrys.limg",
        "gdma_dist_tol": "neighcrys.gdma_dist_tol",
        "paste_coords": "neighcrys.paste_coords",
        "paste_coords_file": "neighcrys.paste_coords_filename",
        "raise_before_neighcrys_call": "neighcrys.raise_before_call",
        "remove_symmetry_subgroup": "neighcrys.remove_symmetry_subgroup",
        "dummy_atoms": "neighcrys.dummy_atoms",
    },
    "dmacrys": {
        "assisted_convergence": "dmacrys.assisted_convergence",
        "clean_level": "dmacrys.clean_level",
        "zip_level": "dmacrys.zip_level",
        "zip_unpack": "dmacrys.zip_unpack",
        "constant_volume": "dmacrys.constant_volume",
        "dmacrys_timeout": "dmacrys.timeout",
        "remove_negative_eigens": "dmacrys.remove_negative_eigenvalues",
        "cutm": "dmacrys.cutm",
        "nbur": "dmacrys.nbur",
        "auto_neighbour_setting": "dmacrys.auto_neighbour",
        "check_z_value": "dmacrys.check_z",
        "new_ac": "dmacrys.use_new_assisted_convergence",
        "puc": "dmacrys.platon_unit_cell",
        "lj": "dmacrys.fit_lennard_jones",
        "lennard_jones": "dmacrys.fit_lennard_jones",
        "ljp": "dmacrys.lennard_jones_potential_filename",
        "lennard_jones_potential": "dmacrys.lennard_jones_potential_filename",
        "seig1": "dmacrys.seig1",
        "exact_prop": "dmacrys.exact_property",
        "raise_before_dmacrys_call": "dmacrys.raise_before_call",
        "setup_off": "dmacrys.setup_off",
        "setup_on": "dmacrys.setup_on",
        "spline_off": "dmacrys.spline_off",
        "spline_on": "dmacrys.spline_on",
        "set_real_space": "dmacrys.real_space",
        "platon_unit_cell": "dmacrys.platon_unit_cell",
        "timeout": 600.0,
    },
    "gdma": {"multipole_limit": "gdma.l_max", "multipole_switch": "gdma.version_flag"},
    "crystal_generator": {
        "space_group": "crystal_generator.spacegroup",
        "number_structures": "crystal_generator.structure_count",
        "input_file": "crystal_generator.input_filename",
        "output_file": "crystal_generator.output_filename",
        "min_packing": "crystal_generator.minimum_packing",
        "frozen_position": "crystal_generator.freeze_position",
        "frozen_rotation": "crystal_generator.freeze_rotation",
        "frozen_angles": "crystal_generator.freeze_angles",
        "dummy_symmetry_operation": "crystal_generator.dummy_symop",
        "sobol_seed": "crystal_generator.sobol_seed",
        "min_angle": "crystal_generator.minimum_unit_cell_angle",
        "max_angle": "crystal_generator.maximum_unit_cell_angle",
        "max_repulsion_energy": "crystal_generator.maximum_repulsion_energy",
        "separating_axis_theorem": "crystal_generator.separating_axis_theorem",
        "target_volume": "crystal_generator.target_volume",
        "boxes": "crystal_generator.boxes",
        "max_volume": "crystal_generator.max_volume",
        "expand_cell": "crystal_generator.expand_cell",
        "number_processors": "crystal_generator.nprocs",
        "automatic_sobol_seed": "crystal_generator.automatic_sobol_seed",
        "squashing_param": "crystal_generator.squashing_parameter",
        "zip_generated_structures": "crystal_generator.zip_structures",
        "zip_generated_structures_single": "crystal_generator.zip_structures_single",
        "zip_generated_structures_filename": "crystal_generator.zip_structures_filname",
        "z_prime": "crystal_generator.z_prime",
        "old_cell_bounds": "crystal_generator.old_cell_bounds",
        "target_volume_parameter": "crystal_generator.target_volume_parameter",
        "max_volume_parameter": "crystal_generator.max_volume_parameter",
    },
}

DEFAULT_CONFIG_LOCATIONS = (
    os.path.join(os.path.dirname(__file__), "cspy_defaults.toml"),
    os.path.expanduser("~/.cspy.toml"),
    "cspy.toml",
)


class CspyConfiguration(object):
    _settings = None
    _runtime_settings = {}

    def __init__(self, args_dict=None):

        if not args_dict:
            args_dict = {}

        self.config_locations = (x for x in DEFAULT_CONFIG_LOCATIONS if os.path.exists(x))
        self._defaults = DEFAULTS
        self._runtime_settings = recursive_dict_update(
            self._runtime_settings, args_dict
        )
        self.convert_to_new_setting_names()

        for loc in self.config_locations:
            with open(loc) as f:
                self._defaults = recursive_dict_update(self._defaults, toml.load(f))

        self._runtime_settings.update(self._defaults)
        self._settings = ChainMap(self._runtime_settings, os.environ)

    def save(self, filename="cspy.toml", settings="runtime"):
        """Save the current configuration to a file
        """
        if settings == "runtime":
            to_write = self._runtime_settings
        else:  # write all settings
            to_write = self._settings
        with open(filename, "w") as f:
            toml.dump(to_write, f)

    def __getitem__(self, key):
        return self._settings.__getitem__(key)

    def __setitem__(self, key, value):
        self._settings.__getitem__(key, value)

    def __delitem__(self, key):
        self._settings.__delitem__(key)

    def check_for_updates(self):
        defaults = DEFAULTS
        for loc in self.config_locations:
            with open(loc) as f:
                defaults = recursive_dict_update(defaults, toml.load(f))
        return defaults

    def set(self, setting_name, value):
        """ Set `setting_name` to be `value` in the underlying
        settings, where nesting levels are separated by `sep`,
        creating required levels as we go.

        >>> config = CspyConfiguration({})
        >>> config.set('tmp.level1.level2.val_bool', True)
        >>> config['tmp']['level1']['level2']['val_bool']
        True
        >>> config.set('tmp.level1.level2.val_float', 1.35)
        >>> config['tmp']['level1']['level2']['val_float']
        1.35
        """
        tmp = {}
        ref = tmp
        levels = setting_name.split(".")
        for x in levels[:-1]:
            ref[x] = {}
            ref = ref[x]
        ref[levels[-1]] = value
        self._runtime_settings = recursive_dict_update(self._runtime_settings, tmp)

    def get(self, setting_name, default_val=None, sep="."):
        """ Get the value for `setting_name` from the underlying
        settings, where levels are separated by `sep`. If at any
        level of nesting it is not defined, return `default_value`

        >>> config = CspyConfiguration({})
        >>> config.get('level.that.does.not.exist', 3.5)
        3.5
        >>> config.set('level.that.does.exist', 'sure does')
        >>> config.get('level.that.does.exist', 3.5)
        'sure does'
        >>> config['level']['that']['does']
        {'exist': 'sure does'}
        """
        tmp = self._settings
        levels = setting_name.split(sep)
        for level in levels:
            if level not in tmp:
                break
            tmp = tmp[level]
        else:
            return tmp
        return default_val

    def delete(self, setting_name, sep="."):
        """ Delete the value for `setting_name` from the underlying
        settings, where levels are separated by `sep`.

        >>> config = CspyConfiguration({})
        >>> config.set('level.that.does.exist', 'sure does')
        >>> config.get('level.that.does.exist')
        'sure does'
        >>> config.delete('level.that.does.exist')
        """
        nested_dict_delete(self._settings, setting_name, sep=sep)


    def convert_to_new_setting_names(self):
        for key in list(self._runtime_settings.keys()):
            key_done = False
            val = self._runtime_settings.pop(key)
            for section in OLD_ARGS_TO_NEW.keys():
                if key in OLD_ARGS_TO_NEW[section] and val is not None:
                    self.set(OLD_ARGS_TO_NEW[section][key], val)
                    key_done = True
            if not key_done:
                self.set(key, val)


if CONFIG is None:
    CONFIG = CspyConfiguration({})


def configure(args):
    global CONFIG
    if CONFIG is not None:
        args = recursive_dict_update(CONFIG._runtime_settings, args)
    else:
        CONFIG = CspyConfiguration(args)
    return CONFIG


def main():
    import argparse

    parser = argparse.ArgumentParser()
    from cspy.deprecated.gaussian_utils import gaussian_add_arguments
    from cspy.deprecated.neighcrys_utils import neighcrys_add_arguments
    from cspy.deprecated.dmacrys_utils import dmacrys_add_arguments
    from cspy.deprecated.gdma_utils import gdma_add_arguments

    gaussian_add_arguments(parser)
    neighcrys_add_arguments(parser)
    dmacrys_add_arguments(parser)
    gdma_add_arguments(parser)

    args = parser.parse_args()
    config = configure(vars(args))
    config.convert_to_new_setting_names()
    config.set_cluster("iridis5")
    print(toml.dumps(config._runtime_settings))


if __name__ == "__main__":
    main()
