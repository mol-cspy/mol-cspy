import logging
from collections import deque, defaultdict
from cspy.db import CspDataStore
import sqlite3
import json
import threading
import numpy as np
import time
import os
import sys
from cspy.util.path import Path
from cspy.util.time_estimate import strfdelta, timedelta
from cspy.cspympi import WorkQueue
from cspy.distributed.qrbh_manager import QRBHCSP
from cspy.cspympi.threshold_tasks import PerturbedStructure, ThresholdTaskTag
from cspy.similarity.unique_structures import Structure, UniqueStructures
from cspy.sample.mc.thre_increase import Increase_Fixed
from cspy.sample.mc.thre_interval import Interval_Fixed, Interval_Max_Ratio

LOG = logging.getLogger(__name__)

class TrialState:
    def __init__(
        self, 
        id, 
        res_old, 
        initial_energy, 
        interval_para, 
        increase_para, 
        iterations=0, 
        lid_state=1,
        state="R",
        restart_para=None,
        trajectory=None,

    ):
        self.id = id
        self._res_old = res_old
        self._initial_energy = initial_energy 
        self._lid_state = lid_state

        increase_energy = self._get_increase(increase_para)
        if restart_para is not None:
            increase_energy = None

        self._get_interval(interval_para, initial_energy, increase_energy, restart_para)
        self._iterations = iterations
        self._state = state
        if trajectory is None:
            datas = self.interval_tool.data
            datas.insert(0, 0)
            self._trajectory = [datas]
        else:
            self._trajectory = trajectory
    
    def _get_interval(
        self, interval_para, initial_energy, increase_energy, restart_para
    ):
        interval_type = interval_para[0]

        if interval_type == "fixed":
            self.interval_tool = Interval_Fixed(
                interval_para, initial_energy, increase_energy, restart_para
            )
        elif interval_type == "max_ratio":
            self.interval_tool = Interval_Max_Ratio(
                interval_para, initial_energy, increase_energy, restart_para
            )
        else:
            raise Exception("No threshold interval type ", interval_type)

    def _get_increase(self, increase_para):
        increase_type = increase_para[0]

        if increase_type == "fixed":
            self.increase_tool = Increase_Fixed(increase_para)
        else:
            raise Exception("No threshold increase type ", increase_type)

        return self.increase_tool.increase_energy

    def _get_id(self, name, sg, trial_number):
        return "-".join([name, str(sg), str(trial_number)])

    def update_step(self, res_new, energy):
        self._iterations += 1
        valid, lift_lid = self.interval_tool.update_step(energy)
        if valid:
            self._res_old = res_new

        if lift_lid:
            self._lid_state += 1
            LOG.info("Increase lid energy %s", self._lid_state)
            increase_energy = self.increase_tool.increase_energy
            datas = self.interval_tool.update_lid_energy(increase_energy)
            datas.insert(0, self._iterations)
            self._trajectory.append(datas)

        return valid

    @property
    def iterations(self):
        return self._iterations

    @property
    def res_old(self):
        return self._res_old

    @property
    def trial_state(self):
        return self._state

    @property
    def lid_state(self):
        return self._lid_state

    def finish_trial(self):
        self._state = "F"

    def check_duplicate(self, iterations):
        return iterations != (self._iterations + 1)or self._state == "F"

    def generate_restart(self):
        meta = {
            'res_old':self._res_old,
            'initial_energy':self._initial_energy,
            'trajectory':self._trajectory,
            'lid_state':self._lid_state,
            'increase': self.increase_tool.increase_energy,
            'restart_para':self.interval_tool.restart
        }
        return self.id, self._iterations, self._state, meta


class Threshold(QRBHCSP):
    def __init__(
        self,
        workers,
        name,
        spacegroups,
        res_files,
        mc_para,
        status_file="status.txt",
    ):
        self.start_time = time.time()
        self.n_workers = len(workers)
        self.work_queue = WorkQueue(workers)
        self.name = name
        LOG.info('Started Threshold algorithm for target: "%s"', self.name)

        self.initial_crystals = [Path(x).read_text() for x in res_files]
        self.num_trials = len(self.initial_crystals)

        self.interval_para = mc_para["interval_para"]
        self.increase_para = mc_para["increase_para"]
        self.minimize_s = mc_para["minimize_s"]
        self.min_energy = mc_para["min_energy"]
        self.cluster_s = mc_para["cluster_s"]
        self.trial_step = mc_para['trial_step']
        self.dump_accept = mc_para["dump_accept"]
        self.move_scale = mc_para["move_scale"]

        self.target = self.num_trials * self.trial_step
        self.unique_structures = {}
        self.unique_index = 0
        self.trial_states = {}
        self.finish_s = {}
        self.status_file = status_file
        self.initialize_spacegroups(spacegroups)
        self.successful_minimizations = {sg: deque() for sg in self.structures}
        for sg in self.structures:
            self.trial_states[sg] = {}
            self.unique_structures[sg] = UniqueStructures()
            self.finish_s[sg] = False

        self.status_file = status_file
        self.load_status_file()
        LOG.info("Initial structure table:\n%s", self.structure_table_string())

        self.db_thread = None
        self.db_writer = None
        self.create_databases()


    def initialize_spacegroups(self, spacegroups):
        try:
            spacegroups = [int(x) for x in spacegroups.split()]
        except ValueError as e:
            LOG.error("Error interpreting requested spacegroups: %s", e)
            raise ValueError("Invalid spacegroup") from e
        self.structures = {
            x: {
                "target": self.target,
                "perturb": 0,
                "accept": 0,
                "reject": 0,
                "valid": 0,
                "invalid": 0,
                "running": 0,
                "target_trial": self.num_trials,
                "active_trial": 0,
                "finish_trial": 0,
                "unique_min": 0,
                "ttime": 0.0,
                "mtime": 0.0,
            }
            for x in spacegroups
        }
        self.minimized_trials = defaultdict(set)

    def minimize_initial(self):
        for sg in self.structures:
            for i, res in enumerate(self.initial_crystals):
                structure = PerturbedStructure(
                    name=self.name, 
                    trial_number=i, 
                    spacegroup=sg, 
                    mc_step=0, 
                    file_content=res,
                )
                self.work_queue.prepend_job((ThresholdTaskTag.OPT, structure))

    def create_databases(self):
        from cspy.db.datastore_writer import Threshold_DatastoreWriter

        LOG.info("Setting up database files")
        for sg in self.structures:
            filename = f"{self.name}-{sg}.db"
            if not os.path.exists(filename):
                ds = CspDataStore.create_and_connect(filename)
                ds.disconnect()
            else:
                self.restart_from_database(sg, filename)
        self.db_writer = Threshold_DatastoreWriter(
            self.successful_minimizations,
            self.trial_states,
            filename_format=f"{self.name}-{{sg}}.db",
            interval=0.5,
        )
        self.db_thread = threading.Thread(
            target=self.db_writer.run, name=f"{self.name}-dbworker"
        )
        self.db_thread.start()

    def restart_from_database(self, sg, filename):
        ds = CspDataStore(filename)
        finish_trial = 0
        active_trial = 0
        ntrial = ds.query(
            "select max(trial_number) from trial_structure"
        ).fetchone()[0]
        max_min_step = ds.query(
            "select max(minimization_step) from trial_structure"
        ).fetchone()[0]
        if ntrial is not None:
            for trial_id, trial_number, iterations, state, metadata in ds.query(
                "select trial_id, trial_number, iterations, state, metadata from trial"
            ).fetchall():
                LOG.debug(
                    "SG(%d): getting trial %s %s state %s ",
                    sg, trial_number, iterations, state
                )

                meta = json.loads(metadata)
                lid_state=meta['lid_state']
                res_old = meta['res_old']
                self.trial_states[sg][trial_number] = TrialState(
                    id=trial_id,
                    res_old=res_old,
                    initial_energy=meta['initial_energy'],
                    interval_para=self.interval_para,
                    increase_para=self.increase_para,
                    iterations=iterations, 
                    lid_state=lid_state,
                    state=state, 
                    restart_para=meta['restart_para'],
                    trajectory=meta['trajectory'],
                )

                if state == "R":
                    LOG.info(
                        "SG(%d): restarting unfinished trial %s %s",
                        sg, trial_number, iterations
                    )
                    active_trial += 1
                    scale = self.move_scale ** lid_state
                    self.work_queue.append_job((
                        ThresholdTaskTag.PER_EN,
                        (sg, trial_number, iterations + 1, self.name, res_old, scale)
                    ))
                elif state == "F":
                    finish_trial += 1

            naccept = ds.query(
                "select count(*) from trial_structure where minimization_step=0 and valid=True"
            ).fetchone()[0]
            nreject = ds.query(
                "select count(*) from trial_structure where minimization_step=0 and valid=false"
            ).fetchone()[0]
            nvalid = ds.query(
                f"select count(*) from trial_structure where minimization_step={max_min_step}"
            ).fetchone()[0]


            self.structures[sg]["perturb"] = naccept + nreject
            self.structures[sg]["accept"] = naccept
            self.structures[sg]["reject"] = nreject
            self.structures[sg]["valid"] = nvalid
            self.structures[sg]["active_trial"] = active_trial
            self.structures[sg]["finish_trial"] = finish_trial

        #Start new trials that didn't initiate
        for i, res in enumerate(self.initial_crystals[int(ntrial)+1:]):
            structure = PerturbedStructure(
                name=self.name, 
                trial_number=i, 
                spacegroup=sg, 
                iterations=0, 
                file_content=res,
            )
            self.work_queue.prepend_job((ThresholdTaskTag.OPT, structure))

        ds.disconnect()

    def process_result(self, result):
        task_type, result, ttime = result
        if task_type == ThresholdTaskTag.PER_EN:
            #Get perturbed structures
            crystal = result
            sg = crystal.spacegroup
            trial_number = crystal.trial_number
            mc_step = crystal.mc_step

            if crystal.energy is not None:
                self.unique_index += 1
                crystal = crystal._replace(unique_index=self.unique_index)
                sg = crystal.spacegroup
                trial_number = crystal.trial_number
                mc_step = crystal.mc_step
                self.structures[sg]["perturb"] += 1
                self.structures[sg]["ttime"] += ttime
                energy = crystal.energy

                if trial_number in self.trial_states[sg]:
                    if self.trial_states[sg][trial_number].check_duplicate(mc_step):
                        LOG.warning(
                            "Duplicate result! sg %s, seed %s, mc_step %s",
                            sg, trial_number, mc_step,
                        )
                        return

                LOG.info(
                    "Processing per_en sg %s trial_number %s mc_step %s energy %s",
                    sg, trial_number, mc_step, energy
                )

                new_s = False
                if self.trial_states[sg][trial_number].update_step(
                    crystal.file_content, energy
                ):
                    self.structures[sg]["accept"] += 1
                    new_s = True
                    crystal = crystal._replace(accept=True)
                    self.successful_minimizations[sg].append(crystal)
                    if self.minimize_s:
                        res_in = crystal.file_content
                        LOG.debug(
                            "Submit new min sg %s trial_number %s mc_step %s",
                            sg, trial_number, mc_step
                        )
                        structure = PerturbedStructure(
                            name=self.name, 
                            spacegroup=sg, 
                            trial_number=trial_number, 
                            mc_step=mc_step,
                            file_content=res_in,
                        )
                        self.work_queue.append_job((ThresholdTaskTag.OPT, structure))
                else:
                    self.structures[sg]["reject"] += 1
                    if not self.dump_accept:
                        self.successful_minimizations[sg].append(crystal)

                mc_step += 1
                if mc_step > self.trial_step:
                    self.trial_states[sg][trial_number].finish_trial()
                    self.structures[sg]["active_trial"] -= 1
                    self.structures[sg]["finish_trial"] += 1
                    return

            res_in = self.trial_states[sg][trial_number].res_old
            lid_state = self.trial_states[sg][trial_number].lid_state
            scale = self.move_scale ** lid_state
            LOG.debug(
                "Submit new perturb sg %s trial_number %s mc_step %s scale %s",
                sg, trial_number, mc_step, scale
            )
            self.work_queue.append_job((
                ThresholdTaskTag.PER_EN,
                (sg, trial_number, mc_step, self.name, res_in, scale)
            ))
        elif task_type == ThresholdTaskTag.OPT:
            #Get minimized structures
            valid, crystals = result
            sg = crystals[0].spacegroup
            trial_number = crystals[0].trial_number
            mc_step = crystals[0].mc_step
            self.structures[sg]["ttime"] += ttime

            LOG.info(
                "Processing min sg %s trial_number %s mc_step %s",
                sg, trial_number, mc_step,
            )

            if valid:
                self.structures[sg]["valid"] += 1
                new_s = False
                crystals = self.set_u_index(crystals)

                if mc_step == 0:
                    trial_id = "-".join([self.name, str(sg), str(trial_number)])
                    res_old = crystals[-1].file_content
                    if self.min_energy is None: 
                        initial_energy = crystals[-1].energy
                    else:
                        initial_energy = self.min_energy
                    self.trial_states[sg][trial_number] = TrialState(
                        trial_id,
                        res_old, 
                        initial_energy, 
                        self.interval_para, 
                        self.increase_para,
                    )
                    new_s = True
                    self.structures[sg]["active_trial"] += 1

                    for c in crystals:
                        if c.minimization_step < 0:
                            continue
                        self.successful_minimizations[c.spacegroup].append(c)
                        if c.time == c.time:
                            self.structures[c.spacegroup]["mtime"] += c.time

                    self.work_queue.append_job((
                        ThresholdTaskTag.PER_EN,
                        (sg, trial_number, 1, self.name, res_old, 1.0)
                    ))
                else:
                    for c in crystals:
                        if c.minimization_step <= 0:
                            continue
                        self.successful_minimizations[c.spacegroup].append(c)
                        if c.time == c.time:
                            self.structures[c.spacegroup]["mtime"] += c.time

                if self.cluster_s:
                    duplicate_s = self.fly_cluster(crystals[-1])
                    self.structures[sg]["unique_min"] = self.unique_structures[
                        sg
                    ].number_structures

            else:
                self.structures[sg]["invalid"] += 1
                if mc_step == 0:
                    LOG.error(
                        "Failed initialization sg %s, trial_number %s",
                        sg,
                        trial_number,
                    )
                    return

    def run(self):
        self.minimize_initial()

        # keeping running if there are jobs to do or if there are
        # completed results to gather
        while not self.work_queue.done:

            self.work_queue.do_work()
            LOG.debug('MPI idle: %s', sorted(self.work_queue.idle))
            LOG.debug('MPI running: %s', sorted(self.work_queue.running))
            LOG.debug('jobs in queue: %s', self.work_queue.num_jobs)
            time.sleep(1)

            for result in self.work_queue.results:
                self.process_result(result)

            self.update_status()

        self.shutdown()

    def shutdown(self):
        LOG.info("Waiting to finish writing database...")
        if self.db_thread is not None:
            self.db_writer.complete = True
            self.db_thread.join()
        LOG.info("Done writing databases")
        LOG.info("%d QR valid minimizations", self.valid_structure_count())
        LOG.info("%d QR invalid minimizations", self.invalid_structure_count())
        LOG.info("%d finished BH trials", self.finish_trial_count())
        LOG.info("%d unfinished BH trials", self.active_trial_count())

        LOG.info("Final structure table\n%s", self.structure_table_string())
        endtime = time.time()
        time_fmt = "{d}d {h}h {m}m {s}s"
        LOG.info("Threshold complete!")
        LOG.info(
            "Walltime: %s",
            strfdelta(timedelta(seconds=endtime - self.start_time), time_fmt),
        )
        ttime = self.total_cpu_time()
        mtime = self.total_minimization_time()
        LOG.info("Total CPU time: %s", strfdelta(timedelta(seconds=ttime), time_fmt))
        LOG.info(
            "Minimization CPU time: %s", strfdelta(timedelta(seconds=mtime), time_fmt)
        )
        self.work_queue.terminate_workers()
