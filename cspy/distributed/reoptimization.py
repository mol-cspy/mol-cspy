import logging
import os
import sys
import threading
import time
from collections import deque, namedtuple
import pandas as pd
from pprint import pformat
import numpy as np
from mpi4py import MPI
from cspy.apps.csp import check_multipoles_valid
from cspy.apps.dma import generate_combined_name
from cspy.chem import Molecule
from cspy.configuration import CspyConfiguration
from cspy.db import CspDataStore
from cspy.potentials import available_potentials
from cspy.util.path import Path
from cspy.cspympi import ReoptWorker
from cspy.cspympi import ReoptTaskTag
from cspy.cspympi import WorkQueue
from sqlite3 import OperationalError

LOG = logging.getLogger(__name__)

MinimizationStructure = namedtuple(
    "MinimizationStructure", "id spacegroup trial_number filename file_content"
)


def get_unminimized_key(min_id):
    contents = min_id.split('-')
    contents[4] = '0'
    return "-".join(contents)


class ReoptimizationManager:

    def __init__(
        self,
        database_files,
        workers,
        name,
        restart_minimize=False,
        minimize_unique=False,
        errors_file="errors.txt",
    ):
        self.name = name
        self.restart_minimize = restart_minimize
        self.minimize_unique = minimize_unique
        self.database_files = database_files

        self.start_time = time.time()
        self.work_queue = WorkQueue(workers)
        LOG.info('Starting ReoptimizationManager for target: "%s"', self.name)
        self.db_thread = None
        self.db_writer = None
        self.errors_file = errors_file
        self.load_errors_file()

        self.successful_minimizations = {}
        self.structures = {}
        self.errors = {
                'summary' : {
                    "timeout": 0,
                    "max_its": 0,
                    "buck_cat": 0,
                    "mol_clash": 0,
                    "unknown": 0,
                    "total": 0,
                }
            }
        for filename in self.database_files:
            structures = deque()
            db = CspDataStore(filename)
            name = Path(filename).stem
            if self.restart_minimize:
                contents = db.final_minimizations(with_trial_data=True).fetchall()
                for id, sg, _density, energy, _mol_id, _file_content, _min_step, trial_number, _min_time in contents:
                    unmin_id = get_unminimized_key(id)
                    file_content = db.query(
                        "select file_content from crystal where id='{}'".format(unmin_id)
                    ).fetchone()[0]
                    structures.append(
                        MinimizationStructure(
                            id=unmin_id,
                            filename=name,
                            file_content=file_content,
                            spacegroup=sg,
                            trial_number=trial_number,
                        )
                    )
            else:
                if self.minimize_unique:
                    contents = db.unique_structures(
                        with_trial_data=True, fallback=True
                    ).fetchall()
                else:
                    contents = db.final_minimizations(with_trial_data=True).fetchall()
                for row in contents:
                    id, sg, _density, energy, _molecule_id, file_content, _min_step, trial_number, _min_time = row
                    structures.append(
                        MinimizationStructure(
                            id=id,
                            filename=name,
                            file_content=file_content,
                            spacegroup=sg,
                            trial_number=trial_number,
                        )
                    )
            db.close()
            self.successful_minimizations[name] = deque()
            self.structures[name] = structures
        self.create_databases()
        self.num_minimizations = sum(len(x) for x in self.structures.values())
        LOG.info("%d structures to re-optimize", self.num_minimizations)

    def restart_from_database(self, filename):
        ds = CspDataStore(filename)
        name = Path(filename).stem.split(".")[0]
        if name not in self.structures:
            return
        LOG.info("Pruning structures found in database: %s", filename)
        complete_ids = {x.id: i for i, x in enumerate(self.structures[name])}
        to_remove = set()
        if self.restart_minimize:
            chosen_step = 0
        else:
            chosen_step = ds.final_minimization_step()
        try:
            for crystal_id, trial_number in ds.query(
                "select id, trial_number from trial_structure where minimization_step={}"
                .format(chosen_step)
            ):
                crystal_id = "-".join(crystal_id.split("-")[:-2])
                if crystal_id in complete_ids:
                    to_remove.add(complete_ids[crystal_id])
        except OperationalError as e:
            LOG.info(f'{filename} is empty after restart. Skipping pruning step.')
            
        for idx in sorted(to_remove, reverse=True):
            del self.structures[name][idx]
        ds.close()

    def create_databases(self):
        from cspy.db.datastore_writer import DatastoreWriter

        LOG.info("Setting up database files")
        for filename in self.structures:
            filename = f"{filename}.opt.db"
            if not os.path.exists(filename):
                ds = CspDataStore.create_and_connect(filename)
                ds.disconnect()
            else:
                self.restart_from_database(filename)
        self.db_writer = DatastoreWriter(
            self.successful_minimizations,
            filename_format=f"{{db_id}}.opt.db",
            interval=0.5,
        )
        self.db_thread = threading.Thread(
            target=self.db_writer.run, name=f"{self.name}-dbworker"
        )
        self.db_thread.start()

    def run(self):
        if self.num_minimizations == 0:
            LOG.info("Nothing to do!")
            self.shutdown()
            return

        for k, v in self.structures.items():
            for structure in v:
                self.work_queue.append_job((ReoptTaskTag.OPT, structure))

        while not self.work_queue.done:
            self.work_queue.do_work()
            LOG.debug('MPI idle: %s', sorted(self.work_queue.idle))
            LOG.debug('MPI running: %s', sorted(self.work_queue.running))
            LOG.debug('jobs in queue: %s', self.work_queue.num_jobs)
            time.sleep(1)

            # if a known exception occurs, valid will be a string, otherwise 
            for task, (valid, crystals), ttime in self.work_queue.results:
                LOG.debug("Successful %s: %s s %d", task, ttime, len(crystals))
                if valid == True:
                    for c in crystals:
                        self.successful_minimizations[c.filename].append(c)
                else:
                    c = crystals[0]
                    self.successful_minimizations[c.filename].append(c)
                    self.errors["summary"]["total"] += 1
                    if valid == "timeout":
                        self.errors["total"]["timeout"] += 1
                    elif valid == 'MaxIts':
                        self.errors["total"]["max_its"] += 1
                    elif valid == 'BuckCat':
                        self.errors["total"]["buck_cat"] += 1
                    elif valid == 'MolClash':
                        self.errors["total"]["mol_clash"] += 1
                    else:
                        self.errors["total"]["unknown"] += 1
            self.update_errors()
        
        LOG.info("All minimizations complete!")
        self.shutdown()
            
    def update_errors(self):
        errors_string = self.errors_table_string()
        LOG.debug("Current status:\n%s", errors_string)
        with open(self.errors_file, "w") as f:
            f.write(errors_string + "\n")

    def errors_table_string(self):
        df = pd.DataFrame.from_dict(self.errors, orient="index")
        df = df.applymap(str)
        return df.to_string()
    
    def load_errors_file(self):
        if os.path.exists(self.errors_file):
            LOG.info("Loading structure table from %s", self.errors_file)
            err_table_data = pd.read_table(
                self.errors_file, skipfooter=1, sep="\s+", engine="python"
            )
            for r in err_table_data.itertuples():
                print(r)
                sg = r.Index
                try:
                    sg = int(sg)
                except:
                    pass
                if sg not in self.errors:
                    continue
                self.errors[sg]["total"] = r.total
                self.errors[sg]["timeout"] = r.timeout
                self.errors[sg]["max_its"] = r.max_its
                self.errors[sg]["buck_cat"] = r.buck_cat
                self.errors[sg]["unknown"] = r.unknown

    def shutdown(self):
        LOG.info("Shutting down...")
        if self.db_thread is not None:
            self.db_writer.complete = True
            self.db_thread.join()
        self.work_queue.terminate_workers()
        LOG.info("Shutdown complete")


def main():
    import argparse
    from cspy.configuration import CONFIG

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "database_files",
        type=str,
        nargs="+",
        help="Database files containing crystals for optimization",
    )
    parser.add_argument(
        "-x",
        "--xyz-files",
        type=str,
        nargs="+",
        help="Xyz files containing molecules for generation",
    )
    parser.add_argument(
        "-c",
        "--charges",
        type=str,
        default=CONFIG.get("csp.charges_file"),
        help="Rank0 multipole file",
    )
    parser.add_argument(
        "-m",
        "--multipoles",
        type=str,
        default=CONFIG.get("csp.multipoles_file"),
        help="RankN multipole file",
    )
    parser.add_argument(
        "-a",
        "--axis",
        type=str,
        default=None,
        help="Axis filename for structure minimization",
    )
    parser.add_argument(
        "-p",
        "--potential",
        default=CONFIG.get("csp.potential"),
        choices=available_potentials.keys(),
        help="intermolecular potential name",
    )
    parser.add_argument(
        "--cutoff",
        default=CONFIG.get("csp.cutoff"),
        help="dmacrys real space/repulsion-dispersion cutoff",
    )
    parser.add_argument(
        "-r",
        "--restart-minimize",
        action="store_true",
        default=False,
        help="Reminimize the structure from generated structure",
    )
    parser.add_argument(
        "-mq",
        "--minimize-unique",
        action="store_true",
        default=False,
        help="Only minimize the unique structures",
    )
    parser.add_argument(
        "--log-level",
        default=CONFIG.get("csp.log_level"),
        help="Log level"
    )
    parser.add_argument(
        "--keep-files",
        action="store_true",
        default=False,
        help="Keep DMACRYS and NEIGHCRYS files which, for each structure, are stored in a new directory in the pwd."
    )

    args = parser.parse_args()
    logging.basicConfig(
        level=args.log_level,
        format='%(asctime)s - %(levelname)s - %(module)s %(lineno)d - '
               '%(message)s'
    )
    config = CspyConfiguration()
    reopt_steps = [step.get('kind') for step in config.get("csp_minimization_step")]
    # Check if we are not running a calculation with DMACRYS or PMIN
    # also added vasp keyword for when vasp minimizer added to mol-cspy
    if any(item in ["dftb", "vasp"] for item in reopt_steps):
        default_name = "reopt_structures"
        worker_data = {
            "charges": None,
            "multipoles": None,
            "axis": None,
            "bondlength_cutoffs": None,
            "minimization": {
                "minimization_steps": config.get("csp_minimization_step"),
            },
            "check_single_point_energy":
                config.get("csp.check_single_point_energy")
        }
        LOG.info("Minimization settings: %s", pformat(worker_data["minimization"]))
    else:
        # This block is triggered if we are using DMACRYS or PMIN for reoptimisations
        if not args.xyz_files:
            LOG.error(f'args.xyz_files has the following value: {args.xyz_files}')
            LOG.error('Please specify the XYZ files containing molecules that were used to generate the crystal structures. Exiting program.')
            exit(-1)
        default_name = generate_combined_name(args.xyz_files)
        if args.axis is None:
            args.axis = default_name + ".mols"
            LOG.info("No axis file provided, trying %s", args.axis)
        if args.charges is None:
            args.charges = default_name + "_rank0.dma"
            LOG.info("No charges file provided, trying %s", args.charges)
        if args.multipoles is None:
            args.multipoles = default_name + ".dma"
            LOG.info("No multipole file provided, trying %s", args.multipoles)
        if not check_multipoles_valid(
            args.xyz_files, args.axis, args.charges, args.multipoles,
            args.potential
        ):
            LOG.error("No good matches for given multipoles! Exiting...")
            sys.exit(1)

        if args.cutoff == "calculate":
            cutoff = 15.0
            from scipy.spatial.distance import pdist

            for x in args.xyz_files:
                mol = Molecule.load(x)
                if len(mol) == 1:
                    continue
                cutoff = max(cutoff, 1.5 * np.max(pdist(mol.positions)))
        else:
            cutoff = float(args.cutoff)

        bondlength_cutoffs = {}
        for x in args.xyz_files:
            mol = Molecule.load(x)
            for k, v in mol.neighcrys_bond_cutoffs().items():
                if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                    bondlength_cutoffs[k] = v

        config = CspyConfiguration()
        config.set("neighcrys.potential", args.potential)
        worker_data = {
            "charges": Path(args.charges).read_text(),
            "multipoles": Path(args.multipoles).read_text(),
            "axis": Path(args.axis).read_text(),
            "bondlength_cutoffs": bondlength_cutoffs,
            "asymmetric_unit": [Path(x).read_text() for x in args.xyz_files],
            "minimization": {
                "neighcrys": {
                    "vdw_cutoff": cutoff,
                    "potential": args.potential,
                },
                "pmin": {"timeout": config.get("pmin.timeout")},
                "dmacrys": {"timeout": config.get("dmacrys.timeout")},
                "minimization_steps": config.get("csp_minimization_step"),
            },
            "check_single_point_energy":
                config.get("csp.check_single_point_energy"),
            "keep_files" : args.keep_files
        }
        LOG.info("Minimization settings: %s", pformat(worker_data["minimization"]))
    LOG.debug(f'worker data: %s', pformat(worker_data))
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        comm.Split(0)
        csp_manager = ReoptimizationManager(
            database_files=args.database_files,
            workers=range(1, MPI.COMM_WORLD.Get_size()),
            name=default_name,
            restart_minimize=args.restart_minimize,
            minimize_unique=args.minimize_unique
        )
        csp_manager.run()
    else:
        comm.Split(1)
        ReoptWorker(worker_data).run()


if __name__ == "__main__":
    main()
