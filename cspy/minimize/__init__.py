__all__ = [
    "DmacrysMinimizer",
    "dmacrys_minimizer",
    "GaussianMinimizer",
    "gaussian_minimizer",
    "PminMinimizer",
    "pmin_minimizer",
    "CompositeMinimizer",
    "DftbMinimizer",
    "dftb_minimizer",
    "single_point_evaluation",
    "calculate_multipoles_and_minimize",
    "dftb_calculator",
]
from .dmacrys_minimizer import DmacrysMinimizer
from .gaussian_minimizer import GaussianMinimizer
from .pmin_minimizer import PminMinimizer
from .composite_minimizer import CompositeMinimizer
from .dftb_minimizer import DftbMinimizer
from cspy.crystal import Crystal
from cspy.chem.molecule import Molecule
from typing import Tuple, Union


def single_point_evaluation(
    crystal: Crystal,
    mults: str,
    axis: str,
    potential: str,
    bondlength_cutoffs: dict = None,
    **kwargs: dict,
) -> Tuple:
    """Run a single point energy evaluation using DMACRYS

    Args:
        crystal (Crystal): An input crystal structure.
        mults (str): Contents of the multipoles file.
        axis (str): Contents of the axis file.
        potential (str): Name of the potential to use.
        bondlength_cutoffs (Dict, optional): A dictionary containing the bondlengths
                                             cutoff for NEIGHCRYS. Defaults to None.

    Returns:
        Tuple: The energy and density of the crystal if successful, else returns nan and nan.
    """
    if bondlength_cutoffs is not None:
        crystal.properties["bondlength_cutoffs"] = bondlength_cutoffs
    evaluater = DmacrysMinimizer(
        mults,
        axis,
        potential=potential,
        MAXI=0,
        rmsd_error_threshold=0.5,
        **kwargs,
    )
    nan = float("nan")

    try:
        evaluater(crystal)
    except:
        return nan, nan

    dm = evaluater.summary
    try:
        energy = dm.initial_energy
        density = dm.initial_density
        return energy, density
    except AttributeError:
        return nan, nan


def calculate_multipoles_and_minimize(
    crystal: Crystal, **kwargs
) -> Union[Crystal, None]:
    """Calculate the multipoles of a crystal and optimise using DMACRYS

    Args:
        crystal (:obj: Crystal): An input crystal structure

    Returns:
        Crystal or None: Returns a crystal object if a minimization is
                         successful otherwise returns None.
    """
    import logging
    import numpy as np
    from cspy.apps.dma import generate_multipoles
    from cspy.configuration import CONFIG
    from cspy.potentials import available_potentials
    from cspy.chem.multipole import DistributedMultipoles
    from pathlib import Path
    import shutil

    LOG = logging.getLogger(__name__)

    # setup settings used in multiple programs
    potential = kwargs.get("potential", "fit")
    foreshorten_hydrogens = kwargs.get("foreshorten_hydrogens", None)
    potential_type, potential_filename = available_potentials[potential]
    if potential_type == "W" and foreshorten_hydrogens is None:
        foreshorten_hydrogens = 0.1
        LOG.info("Reducing hydrogen bond lengths by: %.2f", foreshorten_hydrogens)

    # setup gaussian settings
    g09_kwargs = {
        "method": kwargs.get("method", CONFIG.get("gaussian.method")),
        "basis_set": kwargs.get("basis_set", CONFIG.get("gaussian.basis")),
        "nprocshared": kwargs.get("gaussian_cpus", 2),
        "mem": kwargs.get("gaussian_mem", "2GB"),
    }

    # setup gdma and mulfit settings
    mulfit_method = kwargs.get("mulfit_method", "cumulative")
    mulfit_rank = kwargs.get("mulfit_rank", None)

    ranks = []
    mf_suffix = ""
    if mulfit_rank is not None:
        ranks.append(mulfit_rank)
        mf_suffix = "_rank{}{}".format(
            mulfit_rank, "o" if mulfit_method == "overall" else ""
        )
    mf_format = crystal.titl + "{}.dma"
    multipole_filename = mf_format.format(mf_suffix)

    dma_kwargs = {
        "potential_type": potential_type,
        "mulfit_method": mulfit_method,
        "dma_switch": kwargs.get("dma_switch", 4),
        "foreshorten_hydrogens": foreshorten_hydrogens,
        "hydrogen_radius": kwargs.get("hydrogen_radius", None),
    }

    # run gaussian and gdma/mulfit if multipole file doesn't exist
    multipole = kwargs.get("multipole", None)
    cleanup_files = []
    if multipole is None and not Path(multipole_filename).exists():
        LOG.info("Calculating multipoles (ranks=%s)", ranks)
        cleanup_files = generate_multipoles(
            crystal, ranks, mf_format, **dma_kwargs, **g09_kwargs
        )
    elif multipole is not None and not Path(multipole_filename).exists():
        LOG.info("Using multipoles in %s", multipole)
        shutil.copy(multipole, multipole_filename)
    else:
        LOG.info("Using multipoles in %s", multipole_filename)
    mults = DistributedMultipoles.from_dma_file(multipole_filename)

    # setup neighcrys/dmacrys settings
    cutoff = kwargs.get("cutoff", "calculate")
    if cutoff == "calculate":
        cutoff = 15.0
        from scipy.spatial.distance import pdist

        for mol in crystal.symmetry_unique_molecules():
            if len(mol) == 1:
                continue
            cutoff = max(cutoff, 1.5 * np.max(pdist(mol.positions)))
    else:
        cutoff = float(cutoff)
    LOG.info("Using cutoff: %f", cutoff)

    dmacrys_kwargs = {
        "potential": potential,
        "timeout": kwargs.get("dmacrys_timeout", CONFIG.get("dmacrys.timeout")),
        "MAXI": kwargs.get("MAXI", CONFIG.get("neighcrys.max_iterations")),
        "vdw_cutoff": cutoff,
        "bondlength_cutoffs": crystal.properties["bondlength_cutoffs"],
    }
    if kwargs.get("single_point", False):
        LOG.info(
            "Calculating single point energy for crystal %s, l_max=%d",
            crystal,
            mults.max_rank,
        )
        dmacrys_kwargs["MAXI"] = 0
        dmacrys_kwargs["CONV"] = True
    elif kwargs.get("symmetry_reduction", False):
        LOG.info("Minimizing crystal %s with SEIG", crystal)
        dmacrys_kwargs["SEIG"] = kwargs.get("SEIG", 1)
        dmacrys_kwargs["NOPR"] = False
    elif kwargs.get("phonon", False):
        LOG.info("Minimizing crystal %s with PROP calculation", crystal)
        dmacrys_kwargs["ACCM"] = kwargs.get("ACCM", 100000000)
        dmacrys_kwargs["STAR"] = "PROP"
        dmacrys_kwargs["WCAL"] = False
        dmacrys_kwargs["NOPR"] = False
    else:
        LOG.info("Minimizing crystal %s", crystal)

    # setup axis file
    axis_file = kwargs.get("axis", None)
    axis_filename = crystal.titl + ".mols"
    if axis_file is None and not Path(axis_filename).exists():
        LOG.info("creating axis file using neighcrys")
        crystal.neighcrys_setup(potential_type)
        axis_file = Path(axis_filename)
        axis_file.write_text(crystal.neighcrys_axis.to_string())
    elif axis_file is not None and not Path(axis_filename).exists():
        LOG.info("using axes in {}".format(axis_file))
        shutil.copy(axis_file, axis_filename)
    else:
        LOG.info("using axes in {}".format(axis_filename))
    axis = Path(axis_filename).read_text()

    # run neighcrys/dmacrys
    minimizer = DmacrysMinimizer(mults, axis, **dmacrys_kwargs)
    result = minimizer(crystal)

    # process results and cleanup
    dmacrys_summary_file = crystal.titl + mf_suffix + ".dmacrys_summary"
    dmacrys_output_file = crystal.titl + mf_suffix + ".dmacrys_stdout"
    dm = minimizer.summary
    LOG.info(
        "Initial energy: %.3f, initial density: %.3f",
        dm.initial_energy,
        dm.initial_density,
    )
    LOG.info(
        "Final energy: %.3f, final density: %.3f", dm.final_energy, dm.final_density
    )
    crystal.properties["lattice_energy"] = dm.initial_energy
    crystal.properties["density"] = dm.initial_density
    if result is not None:
        e = result.properties["lattice_energy"]
        d = result.properties["density"]
        result.titl = f"{crystal.titl}.{e}.{d}"
    if kwargs.get("cleanup", True):
        for fname in cleanup_files:
            Path(fname).unlink()
    if kwargs.get("outputs", False):
        Path(dmacrys_summary_file).write_text(dm.contents)
        Path(dmacrys_output_file).write_text(minimizer.dmaout_contents)
    if kwargs.get("single_point", False):
        result = crystal
    return result


def dftb_error_check(mol_check: bool, settings: dict) -> None:
    """Check the dftb+ calculation settings for common errors
    (May be refactored in future versions if necessary)

    Args:
        mol_check (bool): True if calculation is on a Molecule
                          False if on a Crystal
        settings (dict): A dictionary containing the keywords used
                         to set up the dftb+ calculation

    Raises:
        ValueError: If there is an issue with any of the settings
    """
    for value in ["single_point", "atomic_pos_opt", "lattice_and_atoms_opt"]:
        if not isinstance(settings[value], bool):
            raise ValueError(f"{value} keyword must be a boolean")

    if not any(
        [
            settings["single_point"],
            settings["atomic_pos_opt"],
            settings["lattice_and_atoms_opt"],
        ]
    ):
        raise ValueError(
            "Cannot set single_point, atomic_pos_opt and lattice_and_atoms_opt all to False"
        )

    if settings["single_point"] and (
        settings["atomic_pos_opt"] or settings["lattice_and_atoms_opt"]
    ):
        raise ValueError(
            "Cannot set single_point and either atomic_pos_opt or lattice_and_atoms_opt keywords to True"
        )

    if settings["atomic_pos_opt"] and settings["lattice_and_atoms_opt"]:
        raise ValueError(
            "Cannot set both atomic_pos_opt and lattice_and_atoms_opt keywords to True."
        )

    if mol_check and settings["lattice_and_atoms_opt"]:
        raise ValueError(
            "Cannot set lattice_and_atoms_opt keywords to True for a molecular calculation."
        )

    if settings["skf_path"] == "TO_BE_ADDED_BY_USER":
        raise ValueError(
            "SKF path not specified. Default value from cspy/configuration.py has not been set"
        )


def dftb_calculator(
    structure: Union[Crystal, Molecule], **kwargs: dict
) -> Union[Union[Crystal, Molecule], None]:
    """Calculator for setting up and executing dftb+ calculations

    Args:
        structure (Crystal or Molecule): An input crystal or molecule

    Raises:
        ValueError: Raised if there is an issue with the dftb+ keywords
                    that are read from a TOML file or configuration.py.

    Returns:
        Molecule, Crystal or None: Returns a crystal or molecule (depending on input)
        if a minimization occurs otherwise returns None for a single-point energy
        evaluation.
    """
    import logging
    from pathlib import Path
    from cspy.configuration import CONFIG
    from cspy.chem.molecule import Molecule
    import pprint

    LOG = logging.getLogger(__name__)

    mol_calc = isinstance(structure, Molecule)
    dftb_keywords = CONFIG.get("dftb")
    LOG.debug(f"DFTB settings:\n{pprint.pformat(dftb_keywords)}")
    # check keywords for common errors
    dftb_error_check(mol_calc, dftb_keywords)

    minimizer = DftbMinimizer(mol_calc=mol_calc, **dftb_keywords)
    result = minimizer(structure)
    dftb_log_file = "dftb_stdout.txt"
    dftb_input_hsd = "dftb_in.txt"

    if result is not None:
        e = float(result.properties["final_energy"])
        LOG.info(f"Final energy = {e} kJ/mol")
        if not mol_calc:
            dens = result.density
            result.properties["density"] = dens
            LOG.info(f"Final density = {dens} cm^3/g")
            result.titl = f"{structure.titl} {e} {dens}"
    if kwargs.get("outputs", False):
        LOG.debug("Saving DFTB output files")
        Path(dftb_log_file).write_text(minimizer.log_summary)
        Path(dftb_input_hsd).write_text(minimizer.input_summary)

    return result if not dftb_keywords["single_point"] else None


class MinimizerFactory:
    """Factory for initialising different minimizer classes"""

    kind_mapping = {
        "pmin": PminMinimizer,
        "dmacrys": DmacrysMinimizer,
        "gaussian": GaussianMinimizer,
        "dftb": DftbMinimizer,
    }

    def __init__(self, kind: str, *args, **kwargs) -> None:
        self.kind = kind.lower()
        self.args = args
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs) -> "Minimizer":
        """Calling the minimizer factory will create a minimizer

        Returns:
            Minimizer: One of the minimizer classes depending on the input args
        """
        return self.create_minimizer(*args, **kwargs)

    def create_minimizer(self, *args, **kwargs) -> "Minimizer":
        """Create an instance of a minimizer class

        Returns:
            Minimizer: Returns an instance of a specified minimizer class.
        """
        args = args + self.args
        cls = self.kind_mapping[self.kind]
        return cls(*args, **self.kwargs, **kwargs)

    @property
    def required_args(self) -> Tuple:
        """Lists the required arguments for different types of minimizer

        Returns:
           Tuple: A tuple of requirements or an empty tuple
        """
        if self.kind in ("pmin", "dmacrys"):
            return "multipoles", "axis", "potential"
        else:
            return ()

    def __repr__(self) -> str:
        return (
            f"<MinimizerFactory: kind='{self.kind}' required_args={self.required_args}>"
        )
