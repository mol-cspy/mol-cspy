from cspy.crystal import Crystal
from cspy.chem.molecule import Molecule
from cspy.executable.dftb import Dftb
from cspy.executable import ReturnCodeError, TimeoutExpired
import os
import logging
from tempfile import TemporaryDirectory
from os.path import exists
from cspy.formats.dftb_input import write_dftb_inputs
from cspy.formats.dftb_output import DftbOutput
from cspy.util.timer import Timer
from typing import Union

LOG = logging.getLogger(__name__)


class DftbMinimizer:
    """A minimizer class created for dftb+ calculations"""

    def __init__(self, mol_calc: bool = None, **kwargs: dict) -> None:
        """Intialise an instance of the DftbMinimizer class

        Args:
            mol_calc (bool or None, optional): True if calculation to be performed on a molecule,
            else False or None. Defaults to None.
            kwargs (dict): keyword arguments used in this function to pass dftb+ keywords from the
            TOML file/configuration.py to class attributes.

        Raises:
            NotImplementedError: Raised if SKF parameter set other than 3ob-3-1 is used.
            RuntimeError: Raised if SKF parameters can not be located.
            KeyError: Raised if there are some keyword arguments missing - these are normally read from
            a TOML file or configuration.py into a dictionary.
        """
        LOG.debug("Initializing DftbMinimizer.")
        self.mol_calc = mol_calc
        self.kwargs = kwargs
        try:
            self.skf_set = self.kwargs.get("skf_set")
            self.skf_path = self.kwargs.get("skf_path")
            self.output_prefix = self.kwargs.get("output_prefix")
            self.single_point = self.kwargs.get("single_point")
            # Ensures lattice optimisation is false for molecule calculations regardless of kwargs
            self.lattice_opt = (
                self.kwargs.get("lattice_and_atoms_opt") if not self.mol_calc else False
            )
            self.atomic_positions_opt = self.kwargs.get("atomic_pos_opt")
            self.timeout = self.kwargs.get("timeout")
        except KeyError as e:
            raise KeyError(f"Some keyword arguments are missing: {e}")
        self.last_log_contents = None

        # This check can be removed if other parameter sets are added.
        if self.skf_set != "3ob-3-1":
            LOG.error(
                "%s parameter set not yet implemented for use in mol-CSPy DFTB minimisation.",
                self.skf_set,
            )
            raise NotImplementedError

        if self.skf_path is not None:
            param_path = os.path.join(self.skf_path, self.skf_set)
            path_check = exists(param_path)
            if not path_check:
                LOG.error("No %s parameters found.", self.skf_set)
                raise RuntimeError("Missing %s parameters.", self.skf_set)
            LOG.debug("Found %s parameter set: %s", self.skf_set, param_path)

        else:
            LOG.error("No slater-koster file path has been provided.")
            raise RuntimeError(
                "Config is missing a path to the slater-koster parameters."
            )

    def create_inputs(
        self, structure: Union[Crystal, Molecule], working_directory: str
    ) -> None:
        """Creates the input HSD and geometry files for dftb+ calculation

        Args:
            structure (:obj: Crystal or :obj: Molecule): An input crystal structure or molecule
            working_directory (str): Path of the working directory
        """
        write_dftb_inputs(
            structure, working_directory=working_directory, settings=self.kwargs
        )

    def run_executable(
        self, name: str, working_directory: str, output_prefix: str, timeout: float
    ) -> Dftb:
        """Wrapper function for initialising a dftb+ executable class that can
        run the requested calculation.

        Args:
            name (str): Name of the input structure
            working_directory (str): Path of the working directory
            output_prefix (str): Prefix for the output geometry file.
            timeout (float): Timeout length in seconds for the calculation.

        Returns:
            Dftb: Returns the instance of the dftb+ executable class
            which can be processed to get output content.
        """
        # initialise an instance of the dftb executable class
        exe = Dftb(
            input_hsd="dftb_in.hsd",
            input_coords="geometry.gen",
            crys_name=name,
            working_directory=working_directory,
            output_prefix=output_prefix,
            timeout=timeout,
        )
        timing = Timer()
        with timing:
            # run method of the dftb executable class
            return_code = exe.run()
        success = exe.output_contents is not None
        LOG.debug(
            "DFTB+ calculation %s in %.2fs",
            "succeeded" if success else "failed",
            timing.elapsed,
        )
        if not success:
            LOG.debug(
                "Last 20 lines in DFTB output: %s\n",
                "\n".join(exe.output_contents.splitlines()[-20:]),
            )
            LOG.debug("DFTB input:\n%s", exe.hsd_file.read())
        return exe

    def calculate(
        self, structure: Union[Crystal, Molecule]
    ) -> Union[Crystal, Molecule]:
        """Performs a dftb+ calculation

        Args:
            structure (Crystal or Molecule): An input crystal structure or molecule

        Returns:
            Crystal or Molecule: Returns the input crystal structure if a single-point energy
            evaluation is performed, else return the optimised crystal structure if a
            minimization is performed.
        """
        name = structure.molecular_formula if self.mol_calc else structure.titl
        if self.single_point:
            LOG.debug("Performing single point energy calculation on %s", name)
        elif self.lattice_opt:
            LOG.debug(
                "Performing lattice and atomic positions minimization on %s", name
            )
        else:
            LOG.debug("Performing atomic positions minimization on %s", name)
        timing = Timer()
        with timing:
            with TemporaryDirectory(prefix="/dev/shm/") as tmpdirname:
                self.create_inputs(
                    structure,
                    working_directory=tmpdirname,
                )
                try:
                    LOG.debug("Starting dftb calculation in %s", tmpdirname)
                    d = self.run_executable(
                        name=name,
                        working_directory=tmpdirname,
                        output_prefix=self.output_prefix,
                        timeout=self.timeout,
                    )
                except (ReturnCodeError, TimeoutExpired) as exc:
                    LOG.exception("Error in DFTB+: %s", exc)
                    return None
                except Exception as exc:
                    LOG.exception("Unknown error in DFTB+: %s", exc)
                    return None

                # post-process
                self.last_log_contents = d.output_contents
                # LOG.debug(self.last_log_contents)
                self.last_input_contents = d.input_contents
                self._job_output = DftbOutput(self.last_log_contents)
                if not self.single_point and not self._job_output._valid():
                    LOG.debug("Invalid calculation.")
                    LOG.debug("Output contents:\n%s", self.last_log_contents)
                    return None

                self._job_output_properties = self._job_output._parse_file()
                structure.properties["energy"] = self._job_output_properties[
                    "initial_energy"
                ]
                if self.single_point and not self.mol_calc:
                    structure.properties["final_energy"] = (
                        float(self._job_output_properties["final_energy"]) * 2625.5002
                    ) / len(structure.unit_cell_molecules())

                if not self.single_point:
                    if self.mol_calc:
                        from cspy.chem.molecule import Molecule

                        new_structure = Molecule.from_gen_file(d.output_geometry)
                        new_structure.properties["initial_energy"] = (
                            float(self._job_output_properties["initial_energy"])
                            * 2625.5002
                        )
                        new_structure.properties["final_energy"] = (
                            float(self._job_output_properties["final_energy"])
                            * 2625.5002
                        )
                    else:
                        new_structure = Crystal.from_gen_file(d.output_geometry)
                        new_structure.properties["initial_energy"] = (
                            float(self._job_output_properties["initial_energy"])
                            * 2625.5002
                        ) / len(new_structure.unit_cell_molecules())
                        new_structure.properties["final_energy"] = (
                            float(self._job_output_properties["final_energy"])
                            * 2625.5002
                        ) / len(new_structure.unit_cell_molecules())
                        new_structure.properties["density"] = float(
                            new_structure.density
                        )

        LOG.info('Minimization of "%s" complete in %.2fs', name, timing.elapsed)

        return structure if self.single_point else new_structure

    def __call__(self, structure: Union[Crystal, Molecule]) -> Union[Crystal, Molecule]:
        """Calling the DFTBMinimizer instance runs the calculation

        Args:
            structure (Union[Crystal,Molecule]): A Crystal or Molecule

        Returns:
            Union[Crystal,Molecule]: _description_
        """
        return self.calculate(structure)

    @property
    def log_summary(self) -> str:
        """Get the output file contents

        Returns:
            str: The output file contents
        """
        return self.last_log_contents

    @property
    def input_summary(self) -> str:
        """Get the input file contents

        Returns:
            str: The input file contents
        """
        return self.last_input_contents
