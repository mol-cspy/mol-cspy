__all__ = ["PotentialData", "available_potentials"]

import os

_DIRNAME = os.path.dirname(__file__)
from .potential_data import PotentialData

available_potentials = {
    "fit": ("F", os.path.join(_DIRNAME, "fit.pots")),
    # In 'fit_boron', the boron atom has the same non-bonded
    # parameters as nitrogen.
    "fit_boron": ("F", os.path.join(_DIRNAME, "fit_boron.pots")),
    "w99": ("W", os.path.join(_DIRNAME, "w99.pots")),
    "fit_water_X": ("F", os.path.join(_DIRNAME, "fit_water_X.pots")),
    "Day_halobenzenes": ("C", os.path.join(_DIRNAME, "Day_halobenzenes.pots")),
    "w99_orig_Halogens": ("W", os.path.join(_DIRNAME, "w99_orig_Halogens.pots")),
    "w99_orig_H": ("W", os.path.join(_DIRNAME, "w99_orig_H.pots")),
    "w99rev_6311": ("W", os.path.join(_DIRNAME, "w99rev_6311.pots")),
    "w99rev_6311_s": ("W", os.path.join(_DIRNAME, "w99rev_6311_s.pots")),
    "w99rev_631": ("W", os.path.join(_DIRNAME, "w99rev_631.pots")),
    "w99rev_pcm_6311": ("W", os.path.join(_DIRNAME, "w99rev_pcm_6311.pots")),
    "w99_s_cl": ("W", os.path.join(_DIRNAME, "w99_s_cl.pots")),
    "w99sp": ("W", os.path.join(_DIRNAME, "w99sp.pots")),
    "w99rev_pcm_6311_and_Halides": ("W", os.path.join(_DIRNAME, "w99rev_pcm_6311_and_Halides.pots")),
}
