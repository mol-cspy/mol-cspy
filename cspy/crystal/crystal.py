import logging
from collections import defaultdict
import os
from scipy.spatial import cKDTree as KDTree
import numpy as np
from scipy.sparse import dok_matrix
import scipy.sparse.csgraph as csgraph
from cspy.util.path import Path
from cspy.formats.cif import Cif
from cspy.formats.pmin_save import PminSave
from cspy.formats.xyz import parse_xyz_file
from cspy.formats.shelx import (
    parse_shelx_file_content,
    to_res_contents,
)
from .unit_cell import UnitCell
from .space_group import SpaceGroup
from .symmetry_operation import SymmetryOperation
from cspy.linalg import cartesian_product
from cspy.chem import Element, chemical_formula, Molecule
from typing import TypeVar

np.set_printoptions(threshold=np.inf)

LOG = logging.getLogger(__name__)


class AsymmetricUnit:
    """Storage class for the coordinates and labels in a crystal
    asymmetric unit
    Create an asymmetric unit object from a list of Elements and 
    an array of fractional coordinates.

    Parameters
    ----------
    elements : :obj:`list` of :obj:`Element`
        N length list of elements associated with the sites in this asymmetric
        unit
    positions : array_like
        (N, 3) array of site positions in fractional coordinates
    labels : array_like
        N length array of string labels for each site
    **kwargs
        Additional properties (will populate the properties member)
        to store in this asymmetric unit
    """

    def __init__(self, elements, positions, labels=None, **kwargs):
        self.elements = elements
        self.atomic_numbers = np.asarray([x.atomic_number for x in elements])
        self.positions = np.asarray(positions)
        self.properties = {}
        self.properties.update(kwargs)
        if labels is None:
            self.labels = []
            label_index = defaultdict(int)
            for el in self.elements:
                label_index[el] += 1
                self.labels.append("{}{}".format(el, label_index[el]))
        else:
            self.labels = labels
        self.labels = np.array(self.labels)

    @classmethod
    def from_records(cls, records):
        """Initialize an AsymmetricUnit from a list of dictionary like objects
        
        Parameters
        ----------
        records : iterable
            An iterable containing dict_like objects with `label`,
            `element`, `position` and optionally `occupation` stored.
        """
        labels = []
        elements = []
        positions = []
        occupation = []
        for r in records:
            labels.append(r["label"])
            elements.append(Element[r["element"]])
            positions.append(r["position"])
            occupation.append(r.get("occupation", 1.0))
        positions = np.asarray(positions)
        return cls(elements, positions, labels=labels, occupation=occupation)

    @property
    def formula(self):
        """Molecular formula for this asymmetric unit

        Returns
        -------
        str
            The chemical formula of this asymmetric unit e.g.
            H24O12 or similar.
        """
        return chemical_formula(self.elements, subscript=False)

    def __len__(self):
        return len(self.elements)

    def __repr__(self):
        return "<{}>".format(self.formula)

Self = TypeVar("Self", bound="Crystal")
class Crystal:
    """Storage class for a crystal structure, consisting of
    an asymmetric unit, a unit cell and space group information.

    Parameters
    ----------
    unit_cell : :obj:`UnitCell`
        The unit cell for this crystal i.e. the translational symmetry
        of the crystal structure.
    space_group : :obj:`SpaceGroup`
        The space group symmetry of this crystal i.e. the generators
        for populating the unit cell given the asymmetric unit.
    asymmetric_unit : :obj:`AsymmetricUnit`
        The asymmetric unit of this crystal. The sites of this
        combined with the space group will generate all translationally
        equivalent positions.
    kwargs : :obj:`dict`
        Optional properties to (will populate the properties member) store
        about the the crystal structure.
    """

    space_group: SpaceGroup
    unit_cell: UnitCell
    asymmetric_unit: AsymmetricUnit
    properties: dict

    def __init__(self, unit_cell, space_group, asymmetric_unit, **kwargs):
        self.space_group = space_group
        self.unit_cell = unit_cell
        self.asymmetric_unit = asymmetric_unit
        self.properties = {}
        self.properties.update(kwargs)

    @property
    def site_positions(self):
        """Row major array of asymmetric unit atomic positions

        Returns
        -------
        array_like
            The positions in fractional coordinates of the asymmetric unit.
        """
        return self.asymmetric_unit.positions

    @property
    def site_atoms(self):
        """Array of asymmetric unit atomic numbers

        Returns
        -------
        array_like
            The atomic numbers of the asymmetric unit.
        """
        return self.asymmetric_unit.atomic_numbers

    @property
    def nsites(self):
        """The number of sites in the asymmetric unit.

        Returns
        -------
        int
            The number of sites in the asymmetric unit.
        """
        return len(self.site_atoms)

    @property
    def symmetry_operations(self):
        """Symmetry operations that generate this crystal.

        Returns
        -------
        :obj:`list` of :obj:`SymmetryOperation`
            List of SymmetryOperation objects belonging to the space group
            symmetry of this crystal.
        """
        return self.space_group.symmetry_operations

    def to_cartesian(self, coords):
        """Convert coordinates (row major) from fractional to cartesian coordinates.

        Parameters
        ----------
        coords : array_like
            (N, 3) array of positions assumed to be in fractional coordinates

        Returns
        -------
        array_like
            (N, 3) array of positions transformed to cartesian (orthogonal) coordinates
            by the unit cell of this crystal.
        """
        return self.unit_cell.to_cartesian(coords)

    def to_fractional(self, coords):
        """Convert coordinates (row major) from cartesian to fractional coordinates.

        Parameters
        ----------
        coords : array_like
            (N, 3) array of positions assumed to be in cartesian (orthogonal) coordinates

        Returns
        -------
        array_like
            (N, 3) array of positions transformed to fractional coordinates
            by the unit cell of this crystal.
        """
        return self.unit_cell.to_fractional(coords)

    def unit_cell_atoms(self, tolerance=1e-3):
        """Generate all atoms in the unit cell (i.e. with 
        fractional coordinates in [0, 1]) along with associated
        information about symmetry operations, occupation, elements
        related asymmetric_unit atom etc.
        
        Will merge atom sites within tolerance of each other, and
        sum their occupation numbers. A warning will be logged if
        any atom site in the unit cell has > 1.0 occupancy after
        this.

        Sets the `_unit_cell_atom_dict` member as this is an expensive
        operation and is worth caching the result. Subsequent calls
        to this function will be a no-op.

        Parameters
        ----------
        tolerance : float, optional
            Minimum separation of sites in the unit cell, below which 
            atoms/sites will be merged and their (partial) occupations
            added.

        Returns
        -------
        dict
            A dictionary of arrays associated with all sites contained
            in the unit cell of this crystal, members are:

            asym_atom: corresponding asymmetric unit atom indices for all sites.

            frac_pos: (N, 3) array of fractional positions for all sites.

            cart_pos: (N, 3) array of cartesian positions for all sites.

            element: (N) array of atomic numbers for all sites.

            symop: (N) array of indices corresponding to the generator symmetry
            operation for each site.

            label: (N) array of string labels corresponding to each site
            occupation: (N) array of occupation numbers for each site. Will
            warn if any of these are greater than 1.0
        """
        if hasattr(self, "_unit_cell_atom_dict"):
            return getattr(self, "_unit_cell_atom_dict")
        pos = self.site_positions
        atoms = self.site_atoms
        natom = self.nsites
        nsymops = len(self.space_group.symmetry_operations)
        occupation = np.tile(
            self.asymmetric_unit.properties.get("occupation", np.ones(natom)), nsymops
        )
        labels = np.tile(self.asymmetric_unit.labels, nsymops)
        uc_nums = np.tile(atoms, nsymops)
        asym = np.arange(len(uc_nums)) % natom
        sym, uc_pos = self.space_group.apply_all_symops(pos)
        translated = np.fmod(uc_pos + 7.0, 1)
        tree = KDTree(translated)
        dist = tree.sparse_distance_matrix(tree, max_distance=tolerance)
        mask = np.ones(len(uc_pos), dtype=bool)

        # because crystals may have partially occupied sites
        # on special positions, we need to merge some sites
        expected_natoms = np.sum(occupation)
        for (i, j), d in dist.items():
            if not (i < j):
                continue
            occupation[i] += occupation[j]
            mask[j] = False
        occupation = occupation[mask]
        if not np.isclose(np.sum(occupation), expected_natoms):
            LOG.warning("invalid total occupation after merging sites")
        if np.any(occupation > 1.0):
            LOG.warning("Some unit cell site occupations are > 1.0")
        setattr(
            self,
            "_unit_cell_atom_dict",
            {
                "asym_atom": asym[mask],
                "frac_pos": translated[mask],
                "element": uc_nums[mask],
                "symop": sym[mask],
                "label": labels[mask],
                "occupation": occupation,
                "cart_pos": self.to_cartesian(translated[mask]),
            },
        )
        return self._unit_cell_atom_dict

    def unit_cell_connectivity(self, tolerance=0.4, neighbouring_cells=1):
        """Periodic connectiviy for the unit cell, populates _uc_graph
        with a networkx.Graph object, where nodes are indices into the
        _unit_cell_atom_dict arrays and the edges contain the translation
        (cell) for the image of the corresponding unit cell atom with the
        higher index to be bonded to the lower

        Bonding is determined by interatomic distances being less than the
        sum of covalent radii for the sites plus the tolerance (provided 
        as a parameter)
        
        Parameters
        ----------
        tolerance : float, optional
            Bonding tolerance (bonded if d < cov_a + cov_b + tolerance)
        neighbouring_cells : int, optional
            Number of neighbouring cells in which to look for bonded atoms.
            We start at the (0, 0, 0) cell, so a value of 1 will look in the
            (0, 0, 1), (0, 1, 1), (1, 1, 1) i.e. all 26 neighbouring cells.
            1 is typically sufficient for organic systems.

        Returns
        -------
        :obj:`tuple` of (sparse_matrix in dict of keys format, dict)
            the (i, j) value in this matrix is the bond length from i,j
            the (i, j) value in the dict is the cell translation on j which
            bonds these two sites
        """
        if hasattr(self, "_uc_graph"):
            return getattr(self, "_uc_graph")

        slab = self.slab(bounds=((-1, -1, -1), (1, 1, 1)))
        n_uc = slab["n_uc"]
        uc_pos = slab["frac_pos"][:n_uc]
        uc_nums = slab["element"][:n_uc]
        neighbour_pos = slab["frac_pos"][n_uc:]
        cart_uc_pos = self.to_cartesian(uc_pos)
        unique_elements = {x: Element.from_atomic_number(x) for x in np.unique(uc_nums)}

        # first establish all connections in the unit cell
        covalent_radii = np.array([unique_elements[x].cov for x in uc_nums])
        symbols = np.array([unique_elements[x].symbol for x in uc_nums])
        max_cov = np.max(covalent_radii)

        # TODO this needs to be sped up for large cells, tends to slow for > 1000 atoms
        # and the space storage will become a problem
        tree = KDTree(cart_uc_pos)
        dist = tree.sparse_distance_matrix(tree, max_distance=2 * max_cov + tolerance)

        uc_edges = []
        for (i, j), d in dist.items():
            if not (i < j):
                continue
            if "bondlength_cutoffs" in self.properties:
                key = tuple(sorted((symbols[i], symbols[j])))
                bond_cutoff = self.properties["bondlength_cutoffs"].get(key, 0)
            else:
                bond_cutoff = covalent_radii[i] + covalent_radii[j] + tolerance
            if 1e-3 < d <= bond_cutoff:
                uc_edges.append((i, j, d, (0, 0, 0)))

        cart_neighbour_pos = self.unit_cell.to_cartesian(neighbour_pos)
        tree2 = KDTree(cart_neighbour_pos)
        dist = tree.sparse_distance_matrix(tree2, max_distance=2 * max_cov + tolerance)
        # could be sped up if done outside python
        cells = slab["cell"][n_uc:]
        for (uc_atom, neighbour_atom), d in dist.items():
            uc_idx = neighbour_atom % n_uc
            if not (uc_atom < uc_idx):
                continue
            if "bondlength_cutoffs" in self.properties:
                key = tuple(sorted((symbols[uc_atom], symbols[uc_idx])))
                bond_cutoff = self.properties["bondlength_cutoffs"].get(key, 0)
            else:
                bond_cutoff = covalent_radii[uc_atom] + covalent_radii[uc_idx] + tolerance
            if 1e-3 < d <= bond_cutoff:
                cell = cells[neighbour_atom]
                uc_edges.append((uc_atom, uc_idx, d, tuple(cell)))

        properties = {}
        uc_graph = dok_matrix((n_uc, n_uc))
        for i, j, d, cell in uc_edges:
            uc_graph[i, j] = d
            properties[(i, j)] = cell

        setattr(self, "_uc_graph", (uc_graph, properties))
        return self._uc_graph

    @property
    def titl(self):
        """The titl (i.e. name) of this crystal

        Returns
        -------
        str
            the value of "titl" in properties (if set) otherwise the chemical formula
            of the asymmetric_unit
        """
        return self.properties.get("titl", self.asymmetric_unit.formula)

    @titl.setter
    def titl(self, value):
        """Set the titl (i.e. name) of this crystal

        Parameters 
        -------
        value : str
            The name of this crystal.
        """
        self.properties["titl"] = value

    def unit_cell_molecules(self):
        """Calculate the molecules for all sites in the unit cell,
        where the number of molecules will be equal to number of
        symmetry unique molecules times number of symmetry operations.
        
        Returns
        -------
        :obj:`list` of :obj:`Molecule`
            List of all connected molecules in this crystal, which
            when translated by the unit cell would produce the full crystal.
            If the asymmetric is molecular, the list will be of length
            num_molecules_in_asymmetric_unit * num_symm_operations
        """
        if hasattr(self, "_unit_cell_molecules"):
            return getattr(self, "_unit_cell_molecules")
        uc_graph, edge_cells = self.unit_cell_connectivity()
        n_uc_mols, uc_mols = csgraph.connected_components(
            csgraph=uc_graph, directed=False, return_labels=True
        )
        uc_frac = self._unit_cell_atom_dict["frac_pos"]
        uc_cartesian = self._unit_cell_atom_dict["cart_pos"]
        uc_elements = self._unit_cell_atom_dict["element"]
        uc_asym = self._unit_cell_atom_dict["asym_atom"]
        uc_symop = self._unit_cell_atom_dict["symop"]

        molecules = []

        n_uc = len(uc_frac)
        LOG.debug("%d molecules in unit cell", n_uc_mols)
        for i in range(n_uc_mols):
            nodes = np.where(uc_mols == i)[0]
            root = nodes[0]
            elements = uc_elements[nodes]
            shifts = np.zeros((n_uc, 3))
            ordered, pred = csgraph.breadth_first_order(
                csgraph=uc_graph, i_start=root, directed=False
            )
            for j in ordered[1:]:
                i = pred[j]
                if j < i:
                    shifts[j, :] = shifts[i, :] - edge_cells[(j, i)]
                else:
                    shifts[j, :] = shifts[i, :] + edge_cells[(i, j)]
            positions = self.to_cartesian((uc_frac + shifts)[nodes])
            asym_atoms = uc_asym[nodes]
            reorder = np.argsort(asym_atoms)
            asym_atoms = asym_atoms[reorder]

            mol = Molecule.from_arrays(
                elements=elements[reorder],
                positions=positions[reorder],
                guess_bonds=True,
                unit_cell_atoms=np.array(nodes)[reorder],
                asymmetric_unit_atoms=asym_atoms,
                asymmetric_unit_labels=self.asymmetric_unit.labels[asym_atoms],
                generator_symop=uc_symop[np.asarray(nodes)[reorder]],
            )
            molecules.append(mol)
        setattr(self, "_unit_cell_molecules", molecules)
        return molecules

    def asym_mols(self):
        """Alias for symmetry_unique_molecules()"""
        return self.symmetry_unique_molecules()

    def unique_components(self, rmsd_tol=0.2):
        """Gets the unqiue components of the crystal

        Returns:
        :list of cspy.chem.Molecules: list containing the unique 
        components 
        :list of lists of int: list of lists containing indices of 
        equivalent molecules in asym_mols

        """
        mols = self.asym_mols()
        equiv_mols = { i: [i] for i, _ in enumerate(mols)}

        for i, mol in enumerate(mols):
            if i not in equiv_mols.keys():
                continue
            comps = [ j for j in equiv_mols.keys() if j > i ]
            for j in comps:
                comp_mol = mols[j]
                try:
                    _, _, rmsd = mol.overlay(comp_mol)
                except:
                    continue
                if rmsd < rmsd_tol:
                    equiv_mols[i].extend(equiv_mols[j])
                    equiv_mols.pop(j)

        return (
            [ mols[i] for i in equiv_mols.keys() ],
            [ x for x in equiv_mols.values() ], 
        )

    def replace_molecules(self, molecules, how="best_match", reorder_to="self"):
        """Attempt to replace the molecules in the asymmetric_unit with
        those provided -- akin to 'pasting' in different molecular
        geometries.

        Parameters
        ----------
        molecules : :obj:`list` of :obj:`Molecule`
            The candidate molecules for replacing those in this crystal
        how : str, optional
            The method for determining which molecules to replace with
            those in the list
        reorder_to : str, optional
            Reorder the atoms of the overlap to match self or other.

        Returns
        -------
        :obj:`Crystal`
            Modified version of this crystal with the molecules replaced.
        
        """
        if hasattr(molecules, "positions"):
            molecules = [molecules]
        if how != "best_match":
            raise NotImplementedError(
                "no other methods implemented for molecule replacement"
            )
        from copy import deepcopy

        reference_mols = self.asym_mols()
        best_matches = [(-1, None, 1e100)] * len(reference_mols)
        for i, ref in enumerate(reference_mols):
            for j, mol in enumerate(molecules):
                try:
                    overlayed, order, rmsd = ref.overlay(
                        mol, reorder_to=reorder_to)
                except NotImplementedError:
                    continue
                if best_matches[i][2] > rmsd:
                    best_matches[i] = (j, overlayed, rmsd)
        LOG.info(
            "RMSDs of best matches: \n%s",
            "\n".join(
                f"old {i} -> new {x[0]} {x[2]:.3f}" for i, x in enumerate(best_matches)
            ),
        )
        new_asym_mols = [x[1] for x in best_matches]
        asym_pos = np.vstack([x.positions for x in new_asym_mols])
        asym_nums = np.hstack([x.atomic_numbers for x in new_asym_mols])
        asymmetric_unit = AsymmetricUnit(
            [Element[x] for x in asym_nums], self.to_fractional(asym_pos)
        )
        return Crystal(
            deepcopy(self.unit_cell), deepcopy(self.space_group), asymmetric_unit
        )

    def replace_molecules_by_substructure_overlay(
        self, molecules, crys_substructure=None,
        mols_substructure=None, how="best_match"
    ):
        """Attempt to replace the molecules in the asymmetric_unit with
        those provided by substructure overlay. 
        
        !! If multicomponent crystal will have to do each seperately !!

        Parameters
        ----------
        molecule : cspy.chem.Molecule
            The candidate molecule for replacing those in this crystal
        crys_substructure : list of int
            The atom indices of the crystal molecules to be overlayed with
        mols_substructure : list of int
            The atom indices of the molecule to overlay with.
        how : str, optional
            The method for determining which molecules to replace with
            those in the list

        Returns
        -------
        :obj:`Crystal`
            Modified version of this crystal with the molecules replaced.
        
        """
        if hasattr(molecules, "positions"):
            molecules = [molecules]
        if how != "best_match":
            raise NotImplementedError(
                "no other methods implemented for molecule replacement"
            )
        from copy import deepcopy

        # if provide only one substructure, assumes indices are same
        # in other. Can make this so by using overlay().
        if mols_substructure is None and crys_substructure is None:
            raise ValueError('must provide substructure to overlay')
        elif mols_substructure is None:
            mols_substructure = crys_substructure
        elif crys_substructure is None:
            crys_substructure = mols_substructure

        reference_mols = self.asym_mols()
        best_matches = [(-1, None, 1e100)] * len(reference_mols)
        for i, ref in enumerate(reference_mols):
            for j, mol in enumerate(molecules):
                try:
                    overlayed, rmsd = ref.overlay_substructure(
                        mol, crys_substructure, mols_substructure)
                except NotImplementedError:
                    continue
                if best_matches[i][2] > rmsd:
                    best_matches[i] = (j, overlayed, rmsd)
        LOG.info(
            "RMSDs of best matches: \n%s",
            "\n".join(
                f"old {i} -> new {x[0]} {x[2]:.3f}" for i, x in enumerate(best_matches)
            ),
        )
        replaced_mols = [ 0 if x[1] is None else 1 for x in best_matches ]
        if sum(replaced_mols) == 0:
            LOG.info('no matching molecules to replace')
            return None

        LOG.info(
            'replacing %d molecules', sum(replaced_mols)
        )
        new_asym_mols = [
            x[1] if x[1] is not None else reference_mols[i] 
            for i, x in enumerate(best_matches) 
        ]
        asym_pos = np.vstack([x.positions for x in new_asym_mols])
        asym_nums = np.hstack([x.atomic_numbers for x in new_asym_mols])
        asymmetric_unit = AsymmetricUnit(
            [Element[x] for x in asym_nums], self.to_fractional(asym_pos)
        )
        return Crystal(
            deepcopy(self.unit_cell), deepcopy(self.space_group), asymmetric_unit
        )

    def symmetry_unique_molecules(self, bond_tolerance=0.3):
        """Calculate a list of connected molecules which contain
        every site in the asymmetric_unit

        Populates the _symmetry_unique_molecules member, subsequent
        calls to this function will be a no-op.
       
        Parameters
        ----------
        bond_tolerance : float, optional
            Bonding tolerance (bonded if d < cov_a + cov_b + bond_tolerance)

        Returns
        -------
        :obj:`list` of :obj:`Molecule`
            List of all connected molecules in the asymmetric_unit of this
            crystal, i.e. the minimum list of connected molecules which contain
            all sites in the asymmetric unit.
            If the asymmetric is molecular, the list will be of length
            num_molecules_in_asymmetric_unit and the total number of atoms
            will be equal to the number of atoms in the asymmetric_unit
        """
        if hasattr(self, "_symmetry_unique_molecules"):
            return getattr(self, "_symmetry_unique_molecules")
        uc_molecules = self.unit_cell_molecules()
        asym_atoms = np.zeros(len(self.asymmetric_unit), dtype=bool)
        molecules = []
        # sort by % of identity symop
        order = lambda x: len(np.where(x.asym_symops == 16484)[0]) / len(x)
        for i, mol in enumerate(sorted(uc_molecules, key=order, reverse=True)):
            asym_atoms_in_g = np.unique(mol.properties["asymmetric_unit_atoms"])
            if np.all(asym_atoms[asym_atoms_in_g]):
                continue
            asym_atoms[asym_atoms_in_g] = True
            molecules.append(mol)
            if np.all(asym_atoms):
                break
        LOG.debug("%d symmetry unique molecules", len(molecules))
        setattr(self, "_symmetry_unique_molecules", molecules)
        return molecules

    def molecular_shell(self, mol_idx=0, radius=3.8, method="nearest_atom"):
        mol = self.symmetry_unique_molecules()[mol_idx]
        frac_origin = self.to_fractional(mol.center_of_mass)
        frac_radius = radius / np.array(self.unit_cell.lengths)
        hmax, kmax, lmax = np.ceil(frac_radius + frac_origin).astype(int) + 1
        hmin, kmin, lmin = np.floor(frac_origin - frac_radius).astype(int) - 1
        uc_mols = self.unit_cell_molecules()
        shifts = self.to_cartesian(
            cartesian_product(
                np.arange(hmin, hmax), np.arange(kmin, kmax), np.arange(lmin, lmax)
            )
        )
        neighbours = []
        for uc_mol in uc_mols:
            for shift in shifts:
                uc_mol_t = uc_mol.translated(shift)
                dist = mol.distance_to(uc_mol_t, method=method)
                if (dist < radius) and (dist > 1e-2):
                    neighbours.append(uc_mol_t)
        return neighbours

    def slab(self, bounds=((-1, -1, -1), (1, 1, 1))):
        """Calculate the atoms and associated information
        for a slab consisting of multiple unit cells.
        
        If unit cell atoms have not been calculated, this populates
        their information."""
        uc_atoms = self.unit_cell_atoms()
        (hmin, kmin, lmin), (hmax, kmax, lmax) = bounds
        h = np.arange(hmin, hmax + 1)
        k = np.arange(kmin, kmax + 1)
        l = np.arange(lmin, lmax + 1)
        cells = cartesian_product(
            h[np.argsort(np.abs(h))], k[np.argsort(np.abs(k))], l[np.argsort(np.abs(l))]
        )
        ncells = len(cells)
        uc_pos = uc_atoms["frac_pos"]
        n_uc = len(uc_pos)
        pos = np.empty((ncells * n_uc, 3), dtype=np.float64)
        slab_cells = np.empty((ncells * n_uc, 3), dtype=np.float64)
        for i, cell in enumerate(cells):
            pos[i * n_uc: (i + 1) * n_uc, :] = uc_pos + cell
            slab_cells[i * n_uc: (i + 1) * n_uc] = cell
        slab_dict = {
            k: np.tile(v, ncells) for k, v in uc_atoms.items() if not k.endswith("pos")
        }
        slab_dict["frac_pos"] = pos
        slab_dict["cell"] = slab_cells
        slab_dict["n_uc"] = n_uc
        slab_dict["n_cells"] = ncells
        slab_dict["cart_pos"] = self.to_cartesian(pos)
        return slab_dict

    def atoms_in_radius(self, radius, origin=(0, 0, 0), farthest_atom=True):
        #mol = self.asym_mols()[0]
        #centroid = mol.centroid
        centroids = []
        for mol in self.asym_mols():
            centroids.append(mol.centroid)
        #print(centroids)
        centroid = np.average(centroids, axis=0)
        farthest_atom_xyz = np.max(
            np.linalg.norm(
                mol.positions - mol.centroid, axis=1
            )
        )
        frac_origin = self.to_fractional(centroid)
        #frac_origin = np.mean(self.asymmetric_unit.positions, axis=0)
        if farthest_atom:
            radius = radius + farthest_atom_xyz
        frac_radius = radius / np.array(self.unit_cell.high())
        hmax, kmax, lmax = np.ceil(frac_radius + frac_origin).astype(int) + 1
        hmin, kmin, lmin = np.floor(frac_origin - frac_radius).astype(int) - 1
        slab = self.slab(bounds=((hmin, kmin, lmin), (hmax, kmax, lmax)))
        #print(hmax, kmax, lmax, hmin, kmin, lmin, frac_origin)
        #tree = KDTree(slab["cart_pos"])
        #idxs = sorted(tree.query_ball_point(origin, radius))
        #result = {k: v[idxs] for k, v in slab.items() if isinstance(v, np.ndarray)}
        #result["uc_atom"] = np.tile(np.arange(slab["n_uc"]), slab["n_cells"])[idxs]
        #return result
        return slab

    @property
    def site_labels(self):
        """array of labels for sites in the asymmetric_unit"""
        return self.asymmetric_unit.labels

    def __repr__(self):
        if "lattice_energy" in self.properties and "density" in self.properties:
            return "<Crystal {} {} ({:.3f}, {:.3f})>".format(
                self.asymmetric_unit.formula,
                self.space_group.symbol,
                self.properties["density"],
                self.properties["lattice_energy"],
            )
        return "<Crystal {} {}>".format(
            self.asymmetric_unit.formula, self.space_group.symbol
        )

    def standardized(self):
        """Calculate a standardized versio of this crystal using
        the Niggli cell"""
        from cspy.crystal.util import standardize_crystal

        return standardize_crystal(self)

    def as_primitive_P1(self):
        """calculate smallest primitive P1 cell"""
        from cspy.crystal.util import niggli_primitive_crystal

        return niggli_primitive_crystal(self)

    @property
    def density(self):
        if "density" in self.properties:
            return self.properties["density"]
        uc_mass = sum(Element[x].mass for x in self.unit_cell_atoms()["element"])
        uc_vol = self.unit_cell.volume()
        return uc_mass / uc_vol / 0.6022

    @property
    def energy(self):
        return self.properties.get("lattice_energy", np.nan)

    @classmethod
    def load(cls, filename: str, **kwargs: dict) -> Self:
        """Load a crystal structure from file (.res, .cif)"""
        extension_map = {
            ".res": cls.from_shelx_file,
            ".cif": cls.from_cif_file,
            ".xyz": cls.from_xyz_file,
            ".exyz": cls.from_extended_xyz_file,
        }
        extension = os.path.splitext(filename)[-1].lower()
        return extension_map[extension](filename, **kwargs)

    def save(self, filename):
        """Save this crystal structure to file (.res)"""
        extension_map = {".res": self.to_shelx_file, ".cif": self.to_cif_file}
        extension = os.path.splitext(filename)[-1].lower()
        return extension_map[extension](filename)

    def to_shelx_file(self, filename):
        """Write this crystal structure as a shelx .res file"""
        with open(filename, "w") as f:
            f.write(self.to_shelx_string())

    def to_shelx_string(self, titl=None):
        """Represent this crystal structure as a shelx .res string"""
        fn = lambda x: x.replace("H", "Z")
        sfac = sorted([x.symbol for x in set(self.asymmetric_unit.elements)], key=fn)
        atom_sfac = [sfac.index(x.symbol) + 1 for x in self.asymmetric_unit.elements]
        shelx_data = {
            "TITL": self.titl if titl is None else titl,
            "CELL": self.unit_cell.parameters,
            "SFAC": sfac,
            "SYMM": [
                str(s)
                for s in self.space_group.reduced_symmetry_operations()
                if not s.is_identity()
            ],
            "LATT": self.space_group.latt,
            "ATOM": [
                "{:3} {:3} {: 20.12f} {: 20.12f} {: 20.12f}".format(l, s, *pos)
                for l, s, pos in zip(
                    self.asymmetric_unit.labels, atom_sfac, self.site_positions
                )
            ],
        }
        return to_res_contents(shelx_data)

    @classmethod
    def from_shelx_file(cls, filename, **kwargs):
        """Initialize a crystal structure from a shelx .res file"""
        p = Path(filename)
        titl = p.stem
        return cls.from_shelx_string(p.read_text(), titl=titl, **kwargs)

    @classmethod
    def from_shelx_string(cls, file_content, **kwargs):
        """Initialize a crystal structure from a shelx .res string"""
        shelx_dict = parse_shelx_file_content(file_content)
        asymmetric_unit = AsymmetricUnit.from_records(shelx_dict["ATOM"])
        space_group = SpaceGroup.from_symmetry_operations(
            shelx_dict["SYMM"], expand_latt=shelx_dict["LATT"]
        )
        unit_cell = UnitCell.from_lengths_and_angles(
            shelx_dict["CELL"]["lengths"], shelx_dict["CELL"]["angles"], unit="degrees"
        )
        return cls(
            unit_cell,
            space_group,
            asymmetric_unit,
            titl=kwargs.pop("titl", shelx_dict["TITL"]),
            **kwargs,
        )

    @classmethod
    def from_turbomole_string(cls, coord_content):
        """Initialize from an xtb coord string resulting from optimization"""
        data = {}
        sections = coord_content.split("$")
        for section in sections:
            if not section or section.startswith("end"):
                continue
            lines = section.strip().splitlines()
            label = lines[0].strip()
            data[label] = [x.strip() for x in lines[1:]]
        lattice = []
        for line in data["lattice bohr"]:
            lattice.append([float(x) for x in line.split()])
        elements = []
        positions = []
        for line in data["coord"]:
            x, y, z, el = line.split()
            positions.append((float(x), float(y), float(z)))
            elements.append(Element[el])
        direct = np.array(lattice) * 0.529177249
        pos_cart = np.array(positions) * 0.52917749
        uc = UnitCell(direct)
        pos_frac = uc.to_fractional(pos_cart)
        asym = AsymmetricUnit(elements, pos_frac)
        return cls(uc, SpaceGroup(1), asym)

    def to_turbomole_string(self):
        tmol_template = (
            "$periodic 3\n"
            "$cell\n"
            "{a} {b} {c} {alpha} {beta} {gamma}\n"
            "$coord frac\n"
            "{coords}\n"
        )
        uc_atoms = self.unit_cell_atoms()
        pos = uc_atoms["frac_pos"]
        el = [Element[x].symbol for x in uc_atoms["element"]]
        coords = "\n".join(
            f"    {x:10.6f} {y:10.6f} {z:10.6f} {e}" for (x, y, z), e in zip(pos, el)
        )
        bohr = 1 / 0.529177249
        a, b, c, alpha, beta, gamma = self.unit_cell.parameters
        return tmol_template.format(
            a=a * bohr,
            b=b * bohr,
            c=c * bohr,
            alpha=alpha,
            beta=beta,
            gamma=gamma,
            coords=coords,
        )

    def to_gen_file(self, filename: str, **kwargs: dict) -> None:
        gen_template = (
            "{num_atoms} S\n"
            "{elements}\n"
            "{coords}\n"
            "0.000000 0.000000 0.000000\n"
            "{cell_matrix}\n"
        )
        P1 = self.as_P1()
        uc_atoms = P1.unit_cell_atoms()
        pos = uc_atoms["cart_pos"]
        num_atoms = len(uc_atoms["cart_pos"])
        ele = [Element[x].symbol for x in uc_atoms["element"]]
        unique_ele = set([Element[x].symbol for x in sorted(uc_atoms["element"])])
        ele_order_dict = {item:i+1 for i, item in enumerate(unique_ele)}
        temp_coords = np.column_stack(([ele_order_dict[item] for item in ele], pos))
        sorted_temp_coords = temp_coords[temp_coords[:,0].argsort()]
        index = np.linspace(1, num_atoms, num_atoms)
        genfile_elements =" ".join(item for item in unique_ele)
        genfile_coords ="\n".join("{:.10g} {:.10g} {:.10f} {:.10f} {:.10f}".format(*item)
                                  for item in np.column_stack((index, sorted_temp_coords)))
        genfile_cell ="\n".join("{:.10f} {:.10f} {:.10f}".format(*item)
                                for item in P1.unit_cell.lattice)

        input_contents = gen_template.format(
            num_atoms=num_atoms,
            elements=genfile_elements,
            coords=genfile_coords,
            cell_matrix=genfile_cell,
            )
        with open(filename, "w") as f:
            f.write(input_contents)

    def to_cif_data(self, data_block_name=None,**kwargs):
        if data_block_name is None:
            data_block_name = self.titl
        if "cif_data" in self.properties:
            cif_data = self.properties["cif_data"]
        else:
            cif_data = {
                "audit_creation_method": "generated by cspy2.0",
                "symmetry_equiv_pos_site_id": list(
                    range(1, len(self.symmetry_operations) + 1)
                ),
                "symmetry_equiv_pos_as_xyz": [str(x) for x in self.symmetry_operations],
                "cell_length_a": self.unit_cell.a,
                "cell_length_b": self.unit_cell.b,
                "cell_length_c": self.unit_cell.c,
                "cell_angle_alpha": self.unit_cell.alpha_deg,
                "cell_angle_beta": self.unit_cell.beta_deg,
                "cell_angle_gamma": self.unit_cell.gamma_deg,
                "atom_site_label": self.asymmetric_unit.labels,
                "atom_site_type_symbol": [
                    x.symbol for x in self.asymmetric_unit.elements
                ],
                "atom_site_fract_x": self.asymmetric_unit.positions[:, 0],
                "atom_site_fract_y": self.asymmetric_unit.positions[:, 1],
                "atom_site_fract_z": self.asymmetric_unit.positions[:, 2],
                "atom_site_occupancy": self.asymmetric_unit.properties.get(
                    "occupation", np.ones(len(self.asymmetric_unit))
                ),
            }
        if "wavelength" in kwargs:
            cif_data["diffrn_radiation_wavelength"] = float(kwargs["wavelength"])
        return {data_block_name: cif_data}

    def to_cif_file(self, filename, **kwargs):
        cif_data = self.to_cif_data(**kwargs)
        return Cif(cif_data).to_file(filename)

    def to_cif_string(self, **kwargs):
        cif_data = self.to_cif_data(**kwargs)
        return Cif(cif_data).to_string()

    @classmethod
    def from_cif_data(cls, cif_data, titl=None):
        """Initialize a crystal structure from a dictionary
        of CIF data"""
        labels = cif_data.get("atom_site_label", None)
        symbols = cif_data.get("atom_site_type_symbol", labels)
        elements = [Element[x] for x in symbols]
        x = np.asarray(cif_data.get("atom_site_fract_x", []))
        y = np.asarray(cif_data.get("atom_site_fract_y", []))
        z = np.asarray(cif_data.get("atom_site_fract_z", []))
        occupation = np.asarray(cif_data.get("atom_site_occupancy", [1] * len(x)))
        frac_pos = np.array([x, y, z]).T
        asym = AsymmetricUnit(
            elements=elements, positions=frac_pos, labels=labels, occupation=occupation
        )
        lengths = [cif_data[f"cell_length_{x}"] for x in ("a", "b", "c")]
        angles = [cif_data[f"cell_angle_{x}"] for x in ("alpha", "beta", "gamma")]
        unit_cell = UnitCell.from_lengths_and_angles(lengths, angles, unit="degrees")
        if "symmetry_equiv_pos_as_xyz" in cif_data:
            space_group = SpaceGroup.from_symmetry_operations(
                [
                    SymmetryOperation.from_string_code(x)
                    for x in cif_data["symmetry_equiv_pos_as_xyz"]
                ]
            )
        elif "symmetry_Int_Tables_number" in cif_data:
            space_group = SpaceGroup(cif_data["symmetry_Int_Tables_number"])

        return Crystal(unit_cell, space_group, asym, cif_data=cif_data, titl=titl)

    @classmethod
    def from_cif_file(cls, filename, data_block_name=None):
        """Initialize a crystal structure from a CIF file"""
        cif = Cif.from_file(filename)
        if data_block_name is not None:
            return cls.from_cif_data(cif.data[data_block_name])

        crystals = {
            name: cls.from_cif_data(data, titl=name) for name, data in cif.data.items()
        }
        keys = list(crystals.keys())
        if len(keys) == 1:
            return crystals[keys[0]]
        return crystals

    @classmethod
    def from_cif_string(cls, file_content, **kwargs):
        cif = Cif.from_string(file_content)
        crystals = {
            name: cls.from_cif_data(data, titl=name) for name, data in cif.data.items()
        }
        keys = list(crystals.keys())
        if len(keys) == 1:
            return crystals[keys[0]]
        return crystals

    @classmethod
    def from_gen_file(cls, filename: str, **kwargs: dict) -> Self:
        """Initialize a crystal structure from a GEN file"""
        p = Path(filename)
        return cls.from_gen_string(p.read_text(), **kwargs)
        
    @classmethod
    def from_gen_string(cls, file_content: str, **kwargs: dict) -> Self:
        """Initialize a crystal structure from the GEN file contents"""
        from cspy.formats.gen import parse_gen_file_content
        gen_dict = parse_gen_file_content(file_content)
        unit_cell = UnitCell(gen_dict['cell_vectors'])
        space_group = gen_dict['space_group']
        asym = AsymmetricUnit(elements=gen_dict['elements'],
                              positions=unit_cell.to_fractional(gen_dict['positions']))
        
        return cls(unit_cell, space_group, asym, **kwargs)

    @classmethod
    def from_molecules(cls, molecules, **kwargs):
        sort = kwargs.get("sort_atoms", False)
        if not isinstance(molecules, list):
            molecules = list(molecules)
        assert (len(molecules)) > 0, "Require at least 1 molecule"
        if sort:
            Molecule.group_atoms_by_element(molecules)
        boxa, boxb, boxc = 0, 0, 0
        for i, mol in enumerate(molecules):
            m_bbox = mol.bbox_size + 4.0
            c = -mol.centroid
            if i > 0:
                c[0] += boxa
            mol.translate(c)
            boxa += m_bbox[0] + 3.0
            boxb += m_bbox[1]
            boxc += m_bbox[2]
            boxa, boxb, boxc = max(5.0, boxa), max(5.0, boxb), max(5.0, boxc)

        unit_cell = UnitCell.orthorhombic(boxa, boxb, boxc)
        elements = np.hstack([x.elements for x in molecules])
        positions = np.vstack([x.positions for x in molecules])
        asym = AsymmetricUnit(elements, unit_cell.to_fractional(positions))
        return cls(unit_cell, SpaceGroup(1), asym, **kwargs)
        
    @classmethod
    def from_CONTCAR(cls, filename, **kwargs):
        with open(filename, 'r') as f:
            lines = [line.strip('\n') for line in f]

        scaling_factor = float(lines[1])
        unscaled_vectors = np.vstack([np.array([vector.split() for vector in lines[2:5]])]).astype(float)
        scaled_vectors = scaling_factor * unscaled_vectors
        elements_dict = {elem:lines[6].split()[idx] for idx, elem in enumerate(lines[5].split())}
        elements = []
        num_atoms = 0
        for key, value in elements_dict.items():
            elements += int(value) * [Element[key]]
            num_atoms += int(value)

        positions = np.vstack([np.array([pos.split() for pos in lines[8:num_atoms+8]])]).astype(float) # assumes Direct keyword

        unit_cell = UnitCell(scaled_vectors)
        asym = AsymmetricUnit(elements=elements,
                              positions=positions,
                              )

        return cls(unit_cell, SpaceGroup(1), asym, **kwargs)

    @classmethod
    def from_unit_cell_sg_molecules(cls, unit_cell, space_group, molecules, **kwargs):
        elements = []
        positions = []
        labels = []
        for mol in molecules:
            elements += mol.elements
            positions += list(mol.positions)
            labels += list(mol.properties["asymmetric_unit_labels"])
        positions = np.asarray(positions)
        asymmetric_unit = AsymmetricUnit(
            elements, unit_cell.to_fractional(positions), labels
        )
        return cls(unit_cell, space_group, asymmetric_unit)

    @classmethod
    def from_xyz_file(cls, filename, **kwargs):
        molecules = [Molecule.from_xyz_file(filename)]
        return cls.from_molecules(molecules, **kwargs)

    @classmethod
    def from_xyz_files(cls, filenames, **kwargs):
        molecules = [Molecule.from_xyz_file(filename) for filename in filenames]
        return cls.from_molecules(molecules, **kwargs)

    @classmethod
    def from_extended_xyz_file(cls, filename, **kwargs):
        xyz_dict = parse_xyz_file(filename, extended_xyz=True)
        lattice = [float(x) for x in xyz_dict["Lattice"].split()]
        if len(lattice) == 9:
            latt = np.asarray(lattice).reshape((3, 3))
            unit_cell = UnitCell(latt)
        elif len(lattice) == 6:
            unit_cell = UnitCell.from_lengths_and_angles(lattice)

        elements = []
        positions = []
        for sym, pos in xyz_dict["atoms"]:
            elements.append(Element[sym])
            positions.append(pos)
        positions = np.asarray(positions)
        asymmetric_unit = AsymmetricUnit(elements, unit_cell.to_fractional(positions))
        space_group = SpaceGroup(xyz_dict.get("space_group", 1))
        titl = xyz_dict.get("phase", None)
        return cls(unit_cell, space_group, asymmetric_unit, titl=titl)

    @classmethod
    def from_pmin_save(cls, pmin_save, space_group=SpaceGroup(1)):
        a, b, c, ca, cb, cg = pmin_save.params
        unit_cell = UnitCell.from_lengths_and_angles(
            np.array([a, b, c]), np.arccos([ca, cb, cg])
        )
        labels = []
        idxs = []
        frac = []

        for label, idx, frac_coord in pmin_save.atoms:
            labels.append(label)
            idxs.append(idx)
            frac.append(frac_coord)
        asym = AsymmetricUnit(
            elements=[Element[x] for x in labels],
            positions=np.array(frac),
            labelidx=np.array(idxs),
        )
        return cls(unit_cell, space_group, asym)

    @classmethod
    def from_pmin_save_string(cls, pmin_save_contents, space_group=SpaceGroup(1)):
        return cls.from_pmin_save(PminSave(pmin_save_contents), space_group=space_group)

    def neighcrys_setup(
        self, potential_type="F", file_content=None, bond_tolerance=0.4
    ):
        """Convenience method to add neighcrys axis information for this crystal"""
        from .neighcrys_axis import NeighcrysAxis

        self.neighcrys_axis = NeighcrysAxis(
            self, potential_type, file_content=file_content, alter_crystal=True
        )
        labels = []
        for m in self.symmetry_unique_molecules():
            labels += [x[:4] for x in m.properties["neighcrys_label"]]
        self.properties["unique_potential_labels"] = set(labels)
        elements = set(self.asymmetric_unit.elements)
        if "bondlength_cutoffs" not in self.properties:
            bondlength_cutoffs = {}
            for m in self.symmetry_unique_molecules():
                for k, v in m.neighcrys_bond_cutoffs().items():
                    if k not in bondlength_cutoffs or v > bondlength_cutoffs[k]:
                        bondlength_cutoffs[k] = v
            self.properties["bondlength_cutoffs"] = bondlength_cutoffs
        self.properties["neighcrys_potential_type"] = potential_type

    def map_multipoles(self, mults, foreshorten_hydrogens=None):
        """Attempt to map the multipoles in mults to the symmetry unique molecules
        in this crystal"""
        if not hasattr(self, "neighcrys_axis"):
            self.neighcrys_setup()

        # We assume the order of the atoms is exactly the same for now
        LOG.debug("Attempting to map multipoles in %s", self)
        mapping = []
        for mol in self.symmetry_unique_molecules():
            pos = mol.positions_in_molecular_axis_frame(
                foreshorten_hydrogens=foreshorten_hydrogens
            )
            diff = mol.positions - pos
            rmsd_rotated = np.sqrt(np.vdot(diff, diff) / diff.shape[0])
            LOG.debug(
                "RMSD change from rotation to molecular_axis_frame: %.3f", rmsd_rotated
            )
            pos_inv = pos * [1, 1, -1]
            best_idx = -1
            best_rmsd = 1e100
            best_multipoles = None
            inv = False
            for i, mult_mol in enumerate(mults.molecules):
                if len(pos) != len(mult_mol.positions):
                    continue
                diff = pos - mult_mol.positions
                rmsd = np.sqrt(np.vdot(diff, diff) / diff.shape[0])
                LOG.debug("mol %d no inversion: %.5g", i, rmsd)
                if rmsd < best_rmsd:
                    best_idx = i
                    best_rmsd = rmsd
                    inv = False
                diff = pos_inv - mult_mol.positions
                rmsd = np.sqrt(np.vdot(diff, diff) / diff.shape[0])
                LOG.debug("mol %d with inversion: %.5g", i, rmsd)
                if rmsd < best_rmsd:
                    best_idx = i
                    best_rmsd = rmsd
                    inv = True
            if best_rmsd > 1e-3:
                LOG.warning(
                    "High best RMSD in Crystal.%s mol = %s, match=%d%s RMSD= %.5g",
                    self.map_multipoles.__name__,
                    mol,
                    best_idx,
                    "i" if inv else "",
                    best_rmsd,
                )
            LOG.debug(
                "Best matching molecule %d%s in mults, RMSD=%.5g",
                best_idx,
                "i" if inv else "",
                best_rmsd,
            )
            if inv:
                mol.properties["multipoles"] = [
                    x.reflected("xy") for x in mults.molecules[best_idx].multipoles
                ]
            else:
                mol.properties["multipoles"] = [
                    x for x in mults.molecules[best_idx].multipoles
                ]
            mapping.append((best_idx, best_rmsd, inv))
        return mapping

    def convert_to_molecular_asymmetric_unit(self):
        LOG.warning(
            "Space group subgroups are not properly "
            "implemented, this will not work in many cases, so "
            "check the results carefully."
        )
        mols = self.asym_mols()
        n_uc_mols = len(self.unit_cell_molecules())
        elements = []
        positions = []
        for mol in mols:
            elements += mol.elements
            positions += self.to_fractional(mol.positions).tolist()

        asym = AsymmetricUnit(elements, positions)

        if len(asym) == len(self.asymmetric_unit):
            LOG.warning(
                "Crystal is already has a molecular asymmetric unit:" " returning self"
            )
            return self

        generator_symops = np.unique(
            np.hstack([x.properties["generator_symop"] for x in mols])
        )
        generator_symops = [
            SymmetryOperation.from_integer_code(x)
            for x in generator_symops
            if x != 16484
        ]
        subgroups = self.space_group.subgroups()
        crystal = None
        for subgroup in sorted(
            subgroups, key=lambda x: x.international_tables_number, reverse=True
        ):
            sub_symops = subgroup.symmetry_operations
            if any(symop in sub_symops for symop in generator_symops):
                continue
            else:
                new_space_group = subgroup
                crystal = Crystal(self.unit_cell, new_space_group, asym)
                if len(crystal.unit_cell_molecules()) == n_uc_mols:
                    break

        return crystal

    def choose_trigonal_lattice(self, choice="H"):
        """Change the choice of lattice for this crystal to either
        rhombohedral or hexagonal cell

        Parameters
        ----------
        choice: str, optional
            The choice of the resulting lattice, either 'H' for hexagonal
            or 'R' for rhombohedral (default 'H').
        """
        if not self.space_group.has_hexagonal_rhombohedral_choices():
            raise ValueError("Invalid space group for choose_trigonal_lattice")
        if self.space_group.choice == choice:
            return
        cart_asym_pos = self.to_cartesian(self.asymmetric_unit.positions)
        assert choice in ("H", "R"), "Valid choices are H, R"
        if self.space_group.choice == "R":
            T = np.array(((-1, 1, 0), (1, 0, -1), (1, 1, 1)))
        else:
            T = 1 / 3 * np.array(((-1, 1, 1), (2, 1, 1), (-1, -2, 1)))
        new_uc = UnitCell(np.dot(T, self.unit_cell.direct))
        self.unit_cell = new_uc
        self.asymmetric_unit.positions = self.to_fractional(cart_asym_pos)
        self.space_group = SpaceGroup(
            self.space_group.international_tables_number, choice=choice
        )

    def as_P1(self):
        """Create a copy of this crystal in space group P 1, with the new
        asymmetric_unit consisting of self.unit_cell_molecules()"""
        return self.as_P1_supercell((1, 1, 1))

    def as_P1_supercell(self, size):
        """Create a supercell of this crystal in space group P 1.

        Parameters
        ----------
        size: tuple of int
            Size of the P 1 supercell to be created.

        Returns
        -------
        cspy.crystal.Crystal
            Crystal object of a supercell in space group P 1.
        """
        import itertools as it

        umax, vmax, wmax = size
        a, b, c = self.unit_cell.lengths
        sc = UnitCell.from_lengths_and_angles(
            (umax * a, vmax * b, wmax * c), self.unit_cell.angles
        )
        u = np.arange(umax)
        v = np.arange(vmax)
        w = np.arange(wmax)

        trans = np.array(list(it.product(u, v, w))) @ self.unit_cell.lattice
        positions = []
        atomic_nums = []
        for uc_mol in self.unit_cell_molecules():
            for tran in trans:
                positions.append(uc_mol.positions + tran)
                atomic_nums.append(uc_mol.atomic_numbers)

        asym_pos = np.vstack(positions)
        asym_nums = np.hstack(atomic_nums)
        asymmetric_unit = AsymmetricUnit(
            [Element[x] for x in asym_nums], sc.to_fractional(asym_pos)
        )
        new_crystal = Crystal(sc, SpaceGroup(1), asymmetric_unit)
        new_crystal.titl = self.titl + "-P1-{}-{}-{}".format(*size)
        return new_crystal

    def calculate_lattice_energy(self, **kwargs):
        """Calculate multipoles, then calculate a single point energy
        for this structure

        Returns
        _______
        this crystal, with the energy set by the minimizer
        """
        kwargs["single_point"] = True
        from cspy.minimize import calculate_multipoles_and_minimize

        return calculate_multipoles_and_minimize(self, **kwargs)

    def minimize(self, **kwargs):
        """Calculate multipoles, then optimize this structure
        
        Returns a minimized version of this crystal.
        """
        from cspy.minimize import calculate_multipoles_and_minimize

        return calculate_multipoles_and_minimize(self, **kwargs)
    
    def minimize_with_dftb(self, **kwargs: dict) -> Self:
        """Optimizes the structure using dftb+

        Returns:
            a minimized version of this crystal as a Crystal object
        """
        from cspy.minimize import dftb_calculator

        optimised_crystal = dftb_calculator(self, **kwargs) 
        return optimised_crystal

    def minimize_with_symmetry_reduction(self, size, **kwargs):
        """Reduce to P1 supercell, calculate multipoles, then optimize
        the structure

        Returns a minimized version of this crystal.
        """
        kwargs["symmetry_reduction"] = True
        from cspy.minimize import calculate_multipoles_and_minimize

        crystal = self.as_P1_supercell(size)
        return calculate_multipoles_and_minimize(crystal, **kwargs)

    def minimize_and_calculate_phonons(self, **kwargs):
        """Calculate multipoles, then optimize this structure with the
        phonons calculations turned on, it is recommended that the
        crystal structure is optimize with dmacrys with
        minimize_with_symmetry_reduction before using this function

        Returns a minimized version of this crystal, with phonon
        properties in the crystal properties dictionary
        """
        kwargs["phonon"] = True
        from cspy.minimize import calculate_multipoles_and_minimize

        if self.space_group.symbol != "P1":
            raise NotImplementedError(
                "Phonon code in cspy only tested for crystals in P1, "
                "it is therefore recommended that you minimize with "
                "minimize_with_symmetry_reduction before using "
                "this function"
            )

        return calculate_multipoles_and_minimize(self, **kwargs)

    def create_jmol_phonon_animation_file(self, filename, **kwargs):
        from cspy.crystal.phonon import jmol_phonon_xyz_file
        jmol_phonon_xyz_file(self, filename, **kwargs)

    def calculate_powder_pattern(
        self, method="platon", wavelength=None, **kwargs
    ):
        """Calculate the powder pattern for this crystal using platon"""
        if method != "platon":
            raise NotImplementedError
        from cspy.ml.descriptors import PowderPattern

        if wavelength:
            try:
                pattern = PowderPattern.from_cif_string(
                    self.to_cif_string(wavelength=float(wavelength)), **kwargs
                )
            except ValueError:
                pattern = PowderPattern.from_cif_string(
                    self.to_cif_string(), wavelength=wavelength, **kwargs
                )
        else:
            pattern = PowderPattern.from_cif_string(self.to_cif_string())

        return pattern

    def calculate_structure_factors(self, method="platon"):
        """Calculate the structure factors for this crystal using platon"""
        if method != "platon":
            raise NotImplementedError
        from cspy.ml.descriptors import StructureFactors

        return StructureFactors.from_cif_string(self.to_cif_string())

    def calculate_symmetry_functions(self, **kwargs):
        """Calculate the symmetry functions for this crystal"""
        from cspy.ml.descriptors import SymmetryFunctions

        return SymmetryFunctions.from_crystal(self, **kwargs)
