import logging
import math
import numpy as np
from spglib import standardize_cell, get_symmetry_dataset, find_primitive, niggli_reduce
from cspy.chem.element import Element
from .space_group import SpaceGroup


LOG = logging.getLogger(__name__)

def niggli_primitive_crystal(crystal, **kwargs):
    from cspy.crystal import Crystal, AsymmetricUnit, UnitCell

    lattice = crystal.unit_cell.direct
    uc_dict = crystal.unit_cell_atoms()
    positions = uc_dict["frac_pos"]
    elements = uc_dict["element"]
    asym_atoms = uc_dict["asym_atom"]
    asym_labels = uc_dict["label"]
    cell = lattice, positions, elements

    # reduced_cell = find_primitive(cell, **kwargs)
    reduced_cell = standardize_cell(cell, to_primitive=1, no_idealize=1, **kwargs)

    if reduced_cell is None:
        LOG.warning("Could not find reduced cell for crystal %s", crystal)
        return None
    sg = SpaceGroup(1)
    reduced_lattice, positions, elements = reduced_cell
    unit_cell = UnitCell(reduced_lattice)
    cart_positions = unit_cell.to_cartesian(positions)

    niggli_lattice = niggli_reduce(reduced_lattice)
    if niggli_lattice is None:
        LOG.warning("Could not find niggli lattice for crystal %s", crystal)
        return None
    unit_cell = UnitCell(niggli_lattice)
    positions = unit_cell.to_fractional(cart_positions)
    positions -= np.floor(positions)
    
    unit_cell = UnitCell(niggli_lattice)
    asym = AsymmetricUnit(
        [Element[x] for x in elements],
        positions,
        labels=asym_labels[:len(positions)],
    )
    return Crystal(unit_cell, sg, asym, titl=crystal.titl + "_primitive")

def standardize_crystal(crystal, **kwargs):
    from cspy.crystal import Crystal, AsymmetricUnit, UnitCell

    lattice = crystal.unit_cell.direct
    uc_dict = crystal.unit_cell_atoms()
    positions = uc_dict["frac_pos"]
    elements = uc_dict["element"]
    asym_atoms = uc_dict["asym_atom"]
    asym_labels = uc_dict["label"]
    cell = lattice, positions, elements

    reduced_cell = standardize_cell(cell, **kwargs)

    if reduced_cell is None:
        LOG.warning("Could not find reduced cell for crystal %s", crystal)
        return None
    dataset = get_symmetry_dataset(reduced_cell)
    asym_idx = np.unique(dataset["equivalent_atoms"])
    asym_idx = asym_idx[np.argsort(asym_atoms[asym_idx])]
    sg = SpaceGroup(dataset["number"], choice=dataset["choice"])

    reduced_lattice, positions, elements = reduced_cell
    unit_cell = UnitCell(reduced_lattice)
    asym = AsymmetricUnit(
        [Element[x] for x in elements[asym_idx]],
        positions[asym_idx],
        labels=asym_labels[asym_idx],
    )
    return Crystal(unit_cell, sg, asym)


def detect_symmetry(crystal, **kwargs):
    from cspy.crystal import Crystal, AsymmetricUnit, UnitCell

    lattice = crystal.unit_cell.direct
    uc_dict = crystal.unit_cell_atoms()
    positions = uc_dict["frac_pos"]
    elements = uc_dict["element"]
    asym_atoms = uc_dict["asym_atom"]
    asym_labels = uc_dict["label"]
    cell = lattice, positions, elements
    dataset = get_symmetry_dataset(cell, **kwargs)
    if dataset["number"] == crystal.space_group.international_tables_number:
        return None
    asym_idx = np.unique(dataset["equivalent_atoms"])
    asym_idx = asym_idx[np.argsort(asym_atoms[asym_idx])]
    sg = SpaceGroup(dataset["number"], choice=dataset["choice"])
    asym = AsymmetricUnit(
        [Element[x] for x in dataset["std_types"][asym_idx]],
        dataset["std_positions"][asym_idx],
    )
    unit_cell = UnitCell(dataset["std_lattice"])
    return Crystal(unit_cell, sg, asym)


def crystal_expansions(crystal, target_num_uc_mols, ratios=(1, 1, 1)):
    """Returns the expansions required to generate a supercell with a
    target number of unit cell molecules. This method tries to
    increase each cell axis length equally.

    Parameters
    ----------
    crystal : cspy.crystal.Crystal
        The input crystal to generate the supercell from.
    target_num_uc_mols : int
        The target number of unit cell molecules that the supercell
        should have.
    ratios : tuple
        The ratio the unit cell should be expanded with.
    Returns
    -------
        A tuple of the expansions.
    """
    num_uc_mols = len(crystal.unit_cell_molecules())
    if target_num_uc_mols <= num_uc_mols:
        return 1, 1, 1

    if target_num_uc_mols % num_uc_mols != 0:
        new_target = math.ceil(target_num_uc_mols / num_uc_mols) * num_uc_mols
        LOG.debug("Unable to create an expanded crystal structure "
                  "with %s molecules in the unit cell, will expand "
                  "to a crystal structure with %s molecules in the "
                  "unit cell instead", target_num_uc_mols, new_target)
        target_num_uc_mols = new_target

    a, b, c = crystal.unit_cell.lengths
    r_a, r_b, r_c = ratios
    a = np.inf if r_a <= 1e-10 else a / r_a
    b = np.inf if r_b <= 1e-10 else b / r_b
    c = np.inf if r_c <= 1e-10 else c / r_c
    i, j, k = 1, 1, 1
    for prime in prime_factors(target_num_uc_mols // num_uc_mols):
        if a == min(a, b, c):
            i *= prime
            a *= prime
        elif b == min(a, b, c):
            j *= prime
            b *= prime
        elif c == min(a, b, c):
            k *= prime
            c *= prime
    return i, j, k


def prime_factors(n):
    """Return a list of prime factors of n not including 1.

    Parameters
    ----------
    n : int
        An integer that you want to break down into its prime factors.

    Returns
    -------
    factors : list of int
        A list of prime factors of n.
    """
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    factors.sort(reverse=True)
    return factors
