class Increase_Generic():
    def __init__(self, para, restart):
        self._get_para(para)
        self._initiate(restart)

    def _get_para(self, para):
        return

    def _initiate(self, restart):
        return

    def _reset(self):
        return

    @property
    def increase_energy(self):
        return self._increase_energy

class Increase_Fixed(Increase_Generic):
    def __init__(self, para, restart=None):
        super().__init__(para, restart)

    def _get_para(self, para):
        self._increase_energy = float(para[1])
