import numpy as np
import logging

LOG = logging.getLogger(__name__)

class Interval_Generic():
    def __init__(self, para, initial_energy, increase_energy, restart):
        self._get_para(para)
        self._initiate(initial_energy, increase_energy, restart)

    def _get_para(self, para):
        return

    def _initiate(self, initial_energy, increase_energy, restart):
        self.iteration_tmp = 0
        return

    def _reset(self):
        self.iteration_tmp = 0
        return

    def update_step(self, energy):
        valid = False
        return valid

    def update_lid_energy(self, increase):
        self.increase_energy += increase
        return self.data

    @property
    def data(self):
        return [self.lid_energy]

    @property
    def lid_energy(self):
        return self.initial_energy + self.increase_energy

    @property
    def restart(self):
        _restart = {
            'increase_energy':self.increase_energy,
            'iteration_tmp':self.iteration_tmp,
        }
        return _restart
 

class Interval_Fixed(Interval_Generic):
    def __init__(self, para, initial_energy, increase_energy=None, restart=None):
        if increase_energy is None and restart is None:
            raise Exception("No initial increase_energy")
        super().__init__(para, initial_energy, increase_energy, restart)

    def _get_para(self, para):
        self.interval = int(para[1])

    def _initiate(self, initial_energy, increase_energy=None, restart=None):
        self.initial_energy = initial_energy
        if restart is None:
            self.increase_energy = float(increase_energy)
            self.iteration_tmp = 0
        else:
            self.increase_energy = float(restart['increase_energy'])
            self.iteration_tmp = int(restart['iteration_tmp'])

    def update_step(self, energy):
        self.iteration_tmp += 1
        valid = False
        if energy < self.lid_energy:
            valid = True

        lift_lid = False
        if self.iteration_tmp >= self.interval:
            self._reset()
            lift_lid = True
        return valid, lift_lid
 

class Interval_Max_Ratio(Interval_Generic):
    def __init__(self, para, initial_energy, increase_energy=None, restart=None):
        if increase_energy is None and restart is None:
            raise Exception("No initial increase_energy")
        super().__init__(para, initial_energy, increase_energy, restart)

    def _get_para(self, para):
        if para[1] == "fix":
            self._calculate_energy_bin = self._fix_energy_bin
        elif para[1] == "linear":
            self._calculate_energy_bin = self._linear_energy_bin
        elif para[1] == "exp":
            self._calculate_energy_bin = self._exp_energy_bin
        elif para[1] == "sqrt":
            self._calculate_energy_bin = self._sqrt_energy_bin
        else:
            raise Exception("No energy bin calculation method", para[1])

        self.step_min = int(para[2])
        self.step_max = int(para[3])
        self.step_interval = int(para[4])
        self.bin_para = float(para[5])
        self.tolerance = float(para[6])

    def _initiate(self, initial_energy, increase_energy=None, restart=None):
        self.initial_energy = initial_energy
        if restart is None:
            self.increase_energy = float(increase_energy)
            self.iteration_tmp = 0
            self.num_max = 0
            self.max_ratio = []
        else:
            self.increase_energy = float(restart['increase_energy'])
            self.iteration_tmp = int(restart['iteration_tmp'])
            self.num_max = int(restart['num_max'])
            self.max_ratio = []
            for i in restart['max_ratio']:
                self.max_ratio.append(float(i))
        self._get_energy_bin()
        self._data = [
            self.lid_energy, self.energy_bin, self.iteration_tmp, self.num_max, 0.0
        ]

    def update_step(self, energy):
        self.iteration_tmp += 1
        valid = False
        if energy < self.lid_energy:
            valid = True
            if (self.lid_energy - energy) < self.energy_bin:
                self.num_max += 1

        self.max_ratio.append(float(self.num_max) / float(self.iteration_tmp))

        lift_lid = False
        if self.iteration_tmp >= self.step_min:
            variance = np.var(self.max_ratio[-self.step_interval:])
            if self.iteration_tmp >= self.step_max or variance < self.tolerance:
                self._data = [
                    self.lid_energy, 
                    self.energy_bin,
                    self.iteration_tmp, 
                    self.num_max, 
                    variance,
                ]
                self._reset()
                lift_lid = True
        return valid, lift_lid

    def _reset(self):
        self.iteration_tmp = 0
        self.num_max = 0
        self.max_ratio = []

    def _get_energy_bin(self):
        self._calculate_energy_bin()
        LOG.info("Increase_energy %s, energy_bin %s", self.increase_energy, self.energy_bin)
        
    def _fix_energy_bin(self):
        self.energy_bin = 1.0

    def _linear_energy_bin(self):
        self.energy_bin = self.increase_energy / self.bin_para / self.bin_para

    def _sqrt_energy_bin(self):
        self.energy_bin = np.sqrt(self.increase_energy) / self.bin_para

    def _exp_energy_bin(self):
        self.energy_bin = np.exp(self.increase_energy / self.bin_para / self.bin_para - 1)

    def update_lid_energy(self, increase):
        self.increase_energy += increase
        self._get_energy_bin()
        self._data[0] = self.lid_energy
        self._data[1] = self.energy_bin
        return self.data

    @property
    def data(self):
        return self._data

    @property
    def restart(self):
        _restart = {
            'increase_energy':self.increase_energy,
            'iteration_tmp':self.iteration_tmp,
            'num_max':self.num_max,
            'max_ratio':self.max_ratio[-self.step_interval:],
        }
        return _restart
