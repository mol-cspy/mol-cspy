FORMATS = {
    "DEBUG": "[%(filename)s:%(funcName)s:%(lineno)d] %(message)s",
    "INFO": "[%(levelname)1.1s %(asctime)s] %(message)s",
    "WARN": "[%(levelname)1.1s] %(message)s",
    "ERROR": "[%(levelname)1.1s] %(message)s",
}

DATEFMT = "%H:%M:%S"
