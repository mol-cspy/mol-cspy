.. _csd-python-api-installation:

CSD Python API
==============

mol-CSPy makes use of the CSD Python API for useful tools such as its crystal packing similarity API (via the COMPACK algorithm).

In order to make use of these features in mol-CSPy, **the user must have a valid CSD license.**

Installation notes for the CSD Python API are listed through the following `link <https://downloads.ccdc.cam.ac.uk/documentation/API/installation_notes.html>`_



