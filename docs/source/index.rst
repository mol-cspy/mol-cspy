.. mol-cspy documentation master file, created by
   sphinx-quickstart on Thu Feb 15 14:57:39 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mol-CSPy's documentation!
====================================

This is the documentation page for mol-CSPy, the Day group crystal structure prediction software library and programs.


.. toctree::
   :maxdepth: 1
   :caption: Quickstart
   
   installation
   csd-python-api
   bb_wikipages/Advanced configuration


.. toctree::
   :maxdepth: 1
   :caption: mol-CSPy Commands
   :glob:

   commands/*
   


.. toctree::
   :maxdepth: 1
   :caption: Worked Examples
   
   examples/acetic_acid
   examples/co-crystal
   examples/threshold.rst
   examples/qrbh.rst


.. toctree::
   :maxdepth: 1
   :caption: Misc

   bb_wikipages/FAQ.rst
   sql_schema.rst
   bb_wikipages/Papers
   bb_wikipages/Scripts for CSPy
   license
