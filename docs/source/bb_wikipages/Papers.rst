Useful Literature
=================

GDMA
----

-  Stone, A. J., Chem. Phys. Lett., 1981, 83, 233−239.
-  Stone, A. J., Alderton, M. Mol. Phys., 1985, 56, 1047−1064
-  Stone, A.J., J. Chem. Theory Comput., 2005, 1, 1128.


MULFIT
------

-  Winn, P. J., Ferenczy, G. G., and Reynolds, C. A., J. Phys. Chem. A, 1997. 101, 5437.
-  Winn, P. J., Ferenczy, G. G., and Reynolds, C. A., J. Phys. Chem. A, 1997. 101, 5446.

Quasirandom Sampling
--------------------

-  Case, D. H., Campbell, J. E., Bygrave, P. J., & Day, G. M. (2016).
   `Convergence Properties of Crystal Structure Prediction by
   Quasi-Random Sampling. <https://doi.org/10.1021/acs.jctc.5b01112>`__
   Journal of Chemical Theory and Computation, 12(2), 910–924

DMACRYS/NEIGHCRYS
-----------------

-  Price, S. L., Leslie, M., Welch, G. W. A., Habgood, M., Price, L. S.,
   Karamertzanis, P. G., & Day, G. M. (2010). `Modelling organic crystal
   structures using distributed multipole and polarizability-based model
   intermolecular potentials. <https://doi.org/10.1039/C004164E>`__
   Physical Chemistry Chemical Physics, 12(30), 8478
-  Willock, D. J., Price, S. L., Leslie, M., & Catlow, C. R. A. (1995).
   `The relaxation of molecular crystal structures using a distributed
   multipole electrostatic
   model. <https://doi.org/10.1002/jcc.540160511>`__ Journal of
   Computational Chemistry, 16(5), 628–647

FIT
---

-  D. S. Coombes, S. L. Price, D. J. Willock and M. Leslie (1996), `Role
   of Electrostatic Interactions in Determining the Crystal Structures
   of Polar Organic Molecules. A Distributed Multipole
   Study <https://pubs.acs.org/doi/full/10.1021/jp960333b>`__ J. Phys.
   Chem. 100, 7352-7360
-  D. E. Williams and D. J. Houpt (1986). `Fluorine nonbonded potential
   parameters derived from crystalline
   perfluorocarbons <https://doi.org/10.1107/S010876818609821X>`__ Acta
   Cryst. B42, 286-295
-  Williams, D. E.; Cox, S. R. (1984). `Nonbonded potentials for
   azahydrocarbons: the importance of the Coulombic
   interaction. <https://doi.org/10.1107/S010876818400238X>`__ Acta
   Crystallographica Section B Structural Science, 40(4), 404–417.
-  S. R. Cox, L.-Y. Hsu and D. E. Williams (1981). `Nonbonded potential
   function models for crystalline
   oxohydrocarbons. <https://doi.org/10.1107/S0567739481000703>`__ Acta
   Cryst. A37, 293-301

W99
---

-  Williams, D. E. (2001). `Improved intermolecular force field for
   molecules containing H, C, N, and O atoms, with application to
   nucleoside and peptide
   crystals. <https://doi.org/10.1002/jcc.1074>`__ Journal of
   Computational Chemistry, 22(11), 1154–1166.
-  Williams, D. E. (2000). `Improved intermolecular force field for
   crystalline oxohydrocarbons including O-H…O hydrogen
   bonding. <https://doi.org/10.1002/1096-987X(20010115)22:1%3C1::AID-JCC2%3E3.0.CO;2-6>`__
   Journal of Computational Chemistry, 22(1), 1–20.
-  Williams, D. E. (1999). `Improved intermolecular force field for
   crystalline hydrocarbons containing four- or three-coordinated
   carbon. <https://doi.org/10.1016/S0022-2860(99)00092-7>`__ Journal of
   Molecular Structure, 485-486, 321–347.

W99rev
------

-  Pyzer-Knapp, E. O., Thompson, H. P. G., & Day, G. M. (2016). `An
   optimized intermolecular force field for hydrogen-bonded organic
   molecular crystals using atomic multipole
   electrostatics. <https://doi.org/10.1107/S2052520616007708>`__ Acta
   Crystallographica Section B Structural Science, Crystal Engineering
   and Materials, 72(4), 477–487.

Other
-----

-  Starr, T. L., & Williams, D. E. (1977). `Comparison of models for
   H2–H2 and H2–He anisotropic intermolecular
   repulsion. <https://doi.org/10.1063/1.434165>`__ The Journal of
   Chemical Physics, 66(5), 2054–2057. doi:10.1063/1.434165
